//
//  DispatchQueueExtension.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 25/02/2019.
//

import Foundation

extension DispatchQueue {
    
    static var background = DispatchQueue.global(qos: .background)

}
