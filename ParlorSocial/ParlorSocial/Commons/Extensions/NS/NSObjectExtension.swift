//
//  NSObjectExtension.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 22/03/2019.
//

import Foundation

protocol HasApply { }

extension HasApply {

    @discardableResult
    func apply(closure: (Self) -> Void) -> Self {
        closure(self)
        return self
    }

}

extension NSObject: HasApply { }
