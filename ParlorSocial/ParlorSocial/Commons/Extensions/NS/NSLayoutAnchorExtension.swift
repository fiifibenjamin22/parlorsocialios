//
//  NSLayoutAnchorExtension.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 26/02/2019.
//  Copyright © 2019 KISS DIGITAL SP Z O O. All rights reserved.
//

import UIKit

extension NSLayoutAnchor {

    @discardableResult
    @objc func equal(
        to other: NSLayoutAnchor<AnchorType>,
        constant: CGFloat = 0.0,
        withPriority priority: UILayoutPriority = .required
    ) -> NSLayoutConstraint {
        let newConstraint = constraint(equalTo: other, constant: constant)
        newConstraint.priority = priority
        newConstraint.isActive = true
        return newConstraint
    }

    @discardableResult
    @objc func greaterThanOrEqual(
        to other: NSLayoutAnchor<AnchorType>,
        constant: CGFloat = 0.0,
        withPriority priority: UILayoutPriority = .required
    ) -> NSLayoutConstraint {
        let newConstraint = constraint(greaterThanOrEqualTo: other, constant: constant)
        newConstraint.priority = priority
        newConstraint.isActive = true
        return newConstraint
    }

    @discardableResult
    @objc func lessThanOrEqual(
        to other: NSLayoutAnchor<AnchorType>,
        constant: CGFloat = 0.0,
        withPriority priority: UILayoutPriority = .required
    ) -> NSLayoutConstraint {
        let newConstraint = constraint(lessThanOrEqualTo: other, constant: constant)
        newConstraint.priority = priority
        newConstraint.isActive = true
        return newConstraint
    }

}
