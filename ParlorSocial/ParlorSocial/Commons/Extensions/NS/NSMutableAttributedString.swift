//
//  NSMutableAttributedString.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 26/02/2019.
//  Copyright © 2019 KISS DIGITAL SP Z O O. All rights reserved.
//

import UIKit

extension NSMutableAttributedString {

    func decorate(
        partOfText: String,
        fontWeight: UIFont.Weight,
        fontColor: UIColor = .appText,
        fontSize: CGFloat = Constants.FontSize.body) {

        let attributes = [
            NSAttributedString.Key.font: UIFont.app(ofSize: fontSize, weight: fontWeight),
            NSAttributedString.Key.foregroundColor: fontColor
        ]
        
        addAttributes(attributes, range: (self.string as NSString).range(of: partOfText))
    }

    func withAppFont(
        size: CGFloat? = nil,
        weight: UIFont.Weight) -> Self {

        enumerateAttributes(
            in: NSRange(location: 0, length: self.length),
            using: { (keys, range, _) in
                guard let oldFont = keys[.font] as? UIFont else { return }
                self.addAttribute(.font, value: UIFont.app(
                    ofSize: size ?? oldFont.pointSize,
                    weight: weight)
                        .withTraits(traits: oldFont.fontDescriptor.symbolicTraits) ?? UIFont.systemFont(ofSize: oldFont.pointSize), range: range)

            })
        
        return self
    }

    func with(breakMode: NSLineBreakMode) -> Self {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = breakMode
        let range = NSRange(location: 0, length: self.length)
        self.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: range)
        
        return self
    }
}
