//
//  NSObjectProtocolExtension.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 26/02/2019.
//  Copyright © 2019 KISS DIGITAL SP Z O O. All rights reserved.
//

import Foundation

extension NSObjectProtocol {
    
    static var name: String {
        return String(describing: self)
    }
    
}
