//
//  NSLayoutDimensionExtension.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 04.03.2019.
//

import UIKit

extension NSLayoutDimension {
    
    @discardableResult
    @objc func equalTo(constant: CGFloat, withPriority priority: UILayoutPriority = .required) -> NSLayoutConstraint {
        let newConstraint = constraint(equalToConstant: constant)
        newConstraint.priority = priority
        newConstraint.isActive = true
        return newConstraint
    }
    
}
