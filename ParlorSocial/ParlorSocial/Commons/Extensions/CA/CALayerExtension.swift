//
//  CALayerExtension.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 01/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

extension CALayer {
    func set(shadow: Shadow?, rasterize: Bool = true) {
        if let shadow = shadow {
            shadowColor = shadow.color.cgColor
            shadowOffset = CGSize(width: shadow.offset.x, height: shadow.offset.y)
            shadowOpacity = Float(shadow.opacity)
            shadowRadius = shadow.radius

            if rasterize {
                shouldRasterize = true
                rasterizationScale = UIScreen.main.scale
            }
        } else {
            shadowColor = nil
            shadowOpacity = 0.0
        }
    }
}
