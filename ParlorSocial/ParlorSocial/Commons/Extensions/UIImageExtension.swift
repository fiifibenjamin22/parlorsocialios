//
//  UIImageExtension.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 11/04/2019.
//

import Foundation
import UIKit

extension UIImage {
    
    func imageWith(newSize: CGSize) -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: newSize)
        let image = renderer.image { _ in
            self.draw(in: CGRect.init(origin: CGPoint.zero, size: newSize))
        }
        
        return image
    }
    
    func rotated(byRadians radians: Float) -> UIImage? {
        var newSize = CGRect(origin: CGPoint.zero, size: self.size).applying(CGAffineTransform(rotationAngle: CGFloat(radians))).size
        // Trim off the extremely small float value to prevent core graphics from rounding it up
        newSize.width = floor(newSize.width)
        newSize.height = floor(newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, self.scale)
        let context = UIGraphicsGetCurrentContext()!
        
        // Move origin to middle
        context.translateBy(x: newSize.width / 2, y: newSize.height / 2)
        // Rotate around middle
        context.rotate(by: CGFloat(radians))
        // Draw the image at its center
        self.draw(in: CGRect(x: -self.size.width / 2, y: -self.size.height / 2, width: self.size.width, height: self.size.height))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    var base64Encoded: String {
        let data: Data = {
            let sizeInBytes = Double((self.jpegData(compressionQuality: 1) ?? Data()).count)
            let maxSize = 1e+6 // 1 mb
            var ratio: CGFloat = 1.0
            if sizeInBytes > maxSize {
                let oversizedRatio = maxSize / sizeInBytes
                ratio = CGFloat(oversizedRatio)
            }
            return self.jpegData(compressionQuality: ratio) ?? Data()
        }()
        let baseString = "data:image/jpeg;base64," + data.base64EncodedString(options: [])
        return baseString
    }
    
    func scale(toMaxSize maxSize: CGSize) -> UIImage? {
        let oldWidth = self.size.width
        let oldHeight = self.size.height
        let scaleFactor = (oldWidth > oldHeight) ? maxSize.width / oldWidth : maxSize.height / oldHeight
        let newHeight = oldHeight * scaleFactor
        let newWidth = oldWidth * scaleFactor
        let newSize = CGSize(width: newWidth, height: newHeight)
        return scale(to: newSize)
    }
    
    func scale(to size: CGSize) -> UIImage? {
        var scaledImageRect = CGRect.zero
        
        let aspectWidth: CGFloat = size.width / self.size.width
        let aspectHeight: CGFloat = size.height / self.size.height
        let aspectRatio: CGFloat = min(aspectWidth, aspectHeight)
        
        scaledImageRect.size.width = self.size.width * aspectRatio
        scaledImageRect.size.height = self.size.height * aspectRatio
        scaledImageRect.origin.x = (size.width - scaledImageRect.size.width) / 2.0
        scaledImageRect.origin.y = (size.height - scaledImageRect.size.height) / 2.0
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        
        self.draw(in: scaledImageRect)
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return scaledImage
    }
    
}
