//
//  UIControlExtension.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 30/04/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

extension UIControl {
    func setTarget(_ target: Any?, action: Selector, for events: UIControl.Event) {
        removeTarget(nil, action: nil, for: .allEvents)
        addTarget(target, action: action, for: events)
    }
}
