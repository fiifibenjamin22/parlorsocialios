//
//  DoubleExtension.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 26/02/2019.
//  Copyright © 2019 KISS DIGITAL SP Z O O. All rights reserved.
//

import Foundation

extension Double {
    
    func precised(_ decimalPlaces: Int = 1) -> Double {
        let offset = pow(10, Double(decimalPlaces))
        return (self * offset).rounded() / offset
    }
    
    func getCurrencyString(currency: Currency?) -> String? {
        guard let currency = currency else { return nil }
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        // MARK: - This app is currently used only in United States
        formatter.locale = Locale(identifier: "en_US")
        formatter.currencySymbol = currency.symbol
        formatter.minimumFractionDigits = self == floor(self) ? 0 : 2
        formatter.maximumFractionDigits = 2
        return formatter.string(from: self as NSNumber) ?? ""
    }
    
    func getCurrencyStringWithFreeOption(currency: Currency?) -> String? {
        guard let currency = currency else { return nil }
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        // MARK: - This app is currently used only in United States
        formatter.locale = Locale(identifier: "en_US")
        formatter.currencySymbol = currency.symbol
        formatter.minimumFractionDigits = self == floor(self) ? 0 : 2
        formatter.maximumFractionDigits = 2
        if self == 0 {
            return Strings.MembershipPlans.free.localized
        } else {
            return formatter.string(from: self as NSNumber) ?? ""
        }
    }
    
}
