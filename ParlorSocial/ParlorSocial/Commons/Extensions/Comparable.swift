//
//  Comparable.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 30/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

extension Comparable {
    func clamped(to range: ClosedRange<Self>) -> Self {
        return min(max(self, range.lowerBound), range.upperBound)
    }
}
