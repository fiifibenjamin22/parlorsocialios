//
//  NSLayoutPriorityExtension.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 08/03/2019.
//

import UIKit

extension UILayoutPriority {
    
    func increase(by value: Float = 1) -> UILayoutPriority {
        return UILayoutPriority(self.rawValue + value)
    }
    
    func decrease(by value: Float = 1) -> UILayoutPriority {
        return UILayoutPriority(self.rawValue - value)
    }
    
}
