//
//  UIDeviceExtension.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 23/07/2019.
//

import UIKit

extension UIScreen {
    enum SizeType: CGFloat {
        case unknown = 0.0
        case iPhones_5_5s_5c_SE = 1136.0
        case iPhones_6_6s_7_8 = 1334.0
        case iPhone_XR = 1792
        case iPhones_6Plus_6sPlus_7Plus_8Plus = 1920.0
        case iPhones_X_XS = 2436
        case iPhone_XSMax = 2688
    }
    
    var sizeType: SizeType {
        let height = nativeBounds.height
        guard let sizeType = SizeType(rawValue: height) else { return .unknown }
        return sizeType
    }
}
