//
//  UITextViewExtension.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 29/03/2019.
//

import UIKit
import RxSwift

extension UITextField {

    func addBottomBorder(withColor color: UIColor, size: CGFloat = 1) {
        let view = UIView().manualLayoutable()
        self.addSubview(view)
        view.backgroundColor = color
        view.edgesToParent(anchors: [.bottom, .leading, .trailing], insets: .zero)
        view.heightAnchor.equalTo(constant: size)
    }

    func setLeftPadding(_ amount: CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPadding(_ amount: CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }

}

extension ParlorTextField {

    func withLightBackgroundStyle() -> Self {
        self.font = UIFont.custom(ofSize: Constants.FontSize.body, font: UIFont.Roboto.regular)
        self.textColor = UIColor.appTextBright
        self.placeholderColor = UIColor.appGreyLight
        self.tintColor = UIColor.appSeparator
        self.addBottomBorder(withColor: UIColor.appSeparator)
        return self
    }

}

extension UITextField {
    
    func bindToStringCardField(
        ofType type: CardUploadStringField,
        to destination: PublishSubject<(CardUploadStringField, String)>
        ) -> Disposable {
        return self.rx.text
            .emptyOnNil()
            .map { (type, $0) }
            .bind(to: destination)
    }
    
    func bindToIntCardField(
        ofType type: CardUploadIntField,
        to destination: PublishSubject<(CardUploadIntField, Int?)>
        ) -> Disposable {
        return self.rx.text
            .emptyOnNil()
            .map { Int($0) }
            .map { (type, $0) }
            .bind(to: destination)
    }
    
}
