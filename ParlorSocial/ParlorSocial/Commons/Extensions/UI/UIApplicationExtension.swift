//
//  UIApplicationExtension.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 26/02/2019.
//  Copyright © 2019 KISS DIGITAL SP Z O O. All rights reserved.
//

import UIKit

extension UIApplication {
    
    var statusBar: UIView? {
        if #available(iOS 13.0, *) {
            if let statusBar = self.keyWindow?.viewWithTag(Config.statusBarTag),
                keyWindow?.subviews.last?.tag == Config.statusBarTag {
                return statusBar
            } else {
                keyWindow?.viewWithTag(Config.statusBarTag)?.removeFromSuperview()
                let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
                statusBarView.tag = Config.statusBarTag
                
                self.keyWindow?.addSubview(statusBarView)
                return statusBarView
            }
        } else {
            if responds(to: Selector(("statusBar"))) {
                return value(forKey: "statusBar") as? UIView
            }
        }
        return nil
    }
    
}
