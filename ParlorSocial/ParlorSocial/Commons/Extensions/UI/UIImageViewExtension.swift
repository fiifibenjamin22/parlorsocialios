//
//  UIImageViewExtension.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 26/02/2019.
//  Copyright © 2019 KISS DIGITAL SP Z O O. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {

    func getImage(
        from url: URL?,
        placeholder: UIImage? = nil,
        success: @escaping (UIImage) -> Void = { _ in },
        failure: @escaping (FailureResponse) = { _ in },
        ifFailedOrNilSet failureImage: UIImage? = nil) {
        
        let failureImg = failureImage ?? placeholder
        
        guard let url = url else {
            self.image = failureImg
            return
        }
        
        kf.cancelDownloadTask()
        kf.setImage(with: url, placeholder: placeholder) { [weak self] result in
            switch result {
            case .success(let result):
                self?.image = result.image
                success(result.image)
            case .failure(let error):
                self?.image = failureImg
                let appError = AppError(with: error)
                failure(appError)
            }
        }

    }
}
