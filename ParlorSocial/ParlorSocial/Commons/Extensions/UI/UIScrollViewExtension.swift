//
//  UIScrollViewExtensions.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 30/04/2019.
//

import Foundation
import UIKit
import RxSwift

extension UIScrollView {
    
    var currentPage: Int {
        return Int( (self.contentOffset.x + (0.5 * self.frame.size.width) ) / self.frame.width)
    }
    
    var isInAroundOfTop: Bool {
        return contentOffset.y >= -(contentInset.top + 10) && contentOffset.y <= -(contentInset.top - 10)
    }
    
    func stopScrolling() {
        self.setContentOffset(self.contentOffset, animated: false)
    }
    
    func changeNavigationBarAlpha(using imageView: UIImageView, to navBar: UINavigationBar?) {
        let maxAlpha: CGFloat = 0.3
        guard let navBar = navBar else { return }
        let heightWhenOpaque = imageView.frame.height
        let currentAlpha = imageView.image != nil ? max(min(maxAlpha, contentOffset.y / heightWhenOpaque), 0) : maxAlpha
        let color = UIColor.black.withAlphaComponent(currentAlpha)
        navBar.backgroundColor = color
        UIApplication.shared.statusBar?.backgroundColor = color
    }
    
    func changeNavigationBarAlpha(using view: UIView, to navBar: UINavigationBar?) {
        let maxAlpha: CGFloat = 0.3
        guard let navBar = navBar else { return }
        let heightWhenOpaque = view.frame.height
        let currentAlpha = max(min(maxAlpha, contentOffset.y / heightWhenOpaque), 0)
        let color = UIColor.black.withAlphaComponent(currentAlpha)
        navBar.backgroundColor = color
        UIApplication.shared.statusBar?.backgroundColor = color
    }
    
}
