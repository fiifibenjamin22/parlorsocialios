//
//  UILabelExtension.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 26/02/2019.
//  Copyright © 2019 KISS DIGITAL SP Z O O. All rights reserved.
//

import UIKit

extension UILabel {
    
    static func styled(
        textColor: UIColor = .appText,
        withFont font: UIFont = UIFont.app(ofSize: Constants.FontSize.body, weight: .regular),
        alignment: NSTextAlignment = .center,
        numberOfLines: Int = 0) -> UILabel {

        let label = ParlorLabel()
        label.textColor = textColor
        label.font = font
        label.textAlignment = alignment
        label.numberOfLines = numberOfLines
        label.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 998), for: .vertical)
        label.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 998), for: .horizontal)
        label.manualLayoutable()

        return label
    }
    
    static func withMainMessageStyle() -> UILabel {
        return styled(
            textColor: UIColor.appTextBright,
            withFont: UIFont.custom(ofSize: Constants.FontSize.subtitle, font: UIFont.Roboto.medium),
            alignment: .center,
            numberOfLines: 0)
    }

}

extension ParlorLabel {
    
    func withText(_ text: String?) -> ParlorLabel {
        return self.apply {
            $0.text = text
        }
    }
    
    static func styled(
        withTextColor textColor: UIColor = .appText,
        withFont font: UIFont = UIFont.app(ofSize: Constants.FontSize.body, weight: .regular),
        alignment: NSTextAlignment = .center,
        numberOfLines: Int = 0,
        letterSpacing: CGFloat = 0) -> ParlorLabel {
        let parlorLabel = (styled(textColor: textColor, withFont: font, alignment: alignment, numberOfLines: numberOfLines) as? ParlorLabel) ?? ParlorLabel()
        parlorLabel.letterSpacing = letterSpacing
        return parlorLabel
    }
    
    static func withSubMessageStyle() -> ParlorLabel {
        return ParlorLabel().apply {
            $0.numberOfLines = 0
            $0.textColor = UIColor.textFieldUnderline
            $0.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.regular)
            $0.textAlignment = .center
            $0.lineHeight = 18
        }
    }
    
    static func withSectionHeaderLabelStyle(
        insets: UIEdgeInsets = UIEdgeInsets(top: 25, left: 16, bottom: 16, right: 0)
    ) -> ParlorLabel {
        return ParlorLabel.styled(
            withTextColor: PremiumColorManager.shared.currentTheme.profileTabHeaderLabelColor,
            withFont: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Helvetica.regular),
            alignment: .left,
            numberOfLines: 1
        ).apply {
            $0.insets = insets
        }
    }
}
