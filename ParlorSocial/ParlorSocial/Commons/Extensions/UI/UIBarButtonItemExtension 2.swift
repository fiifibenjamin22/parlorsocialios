//
//  UIBarButtonItemExtension.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 26/02/2019.
//  Copyright © 2019 KISS DIGITAL SP Z O O. All rights reserved.
//

import UIKit

extension UIBarButtonItem {
    
    convenience init(image: UIImage?) {
        self.init(image: image, style: .plain, target: nil, action: nil)
    }

    static func createCloseButton(target: Any?, action: Selector?) -> UIBarButtonItem {
        return UIBarButtonItem(image: Icons.Share.close.withRenderingMode(.alwaysOriginal), style: .plain, target: target, action: action)
    }

    static func createSaveItem(target: Any, action: Selector?) -> UIBarButtonItem {
        return UIBarButtonItem(title: Strings.Interests.save.localized, style: .done, target: target, action: action).apply {
            $0.setTitleTextAttributes(
                [NSAttributedString.Key.font: UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.regular),
                 NSAttributedString.Key.foregroundColor: UIColor.rsvpBorder], for: .normal)
        }
    }

    static func createSearchButton(target: Any?, action: Selector?) -> UIBarButtonItem {
        let search = UIBarButtonItem(
            image: Icons.NavigationBar.search,
            style: .plain,
            target: target,
            action: action)

        return search
    }

    static func createCalendarButton(target: Any?, action: Selector?) -> UIBarButtonItem {
        let calendar = UIBarButtonItem(
            image: Icons.NavigationBar.calendar,
            style: .plain,
            target: target,
            action: action)

        return calendar
    }
}
