//
//  UIButtonExtension.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 14/05/2019.
//

import Foundation
import UIKit

extension UIButton {
    
    func alignVertically(withSpacing spacing: CGFloat = 6.0) {
        guard let imageSize = self.imageView?.image?.size,
            let text = self.titleLabel?.text,
            let font = self.titleLabel?.font
            else { return }
        self.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -imageSize.width, bottom: -(imageSize.height + spacing), right: 0.0)
        let labelString = NSString(string: text)
        let titleSize = labelString.size(withAttributes: [NSAttributedString.Key.font: font])
        self.imageEdgeInsets = UIEdgeInsets(top: -(titleSize.height + spacing), left: 0.0, bottom: 0.0, right: -titleSize.width)
        let edgeOffset = abs(titleSize.height - imageSize.height) / 2.0
        self.contentEdgeInsets = UIEdgeInsets(top: edgeOffset, left: 0.0, bottom: edgeOffset, right: 0.0)
    }
    
    func setEnabledWithAlpha(_ isEnabled: Bool, disabledAlpha: CGFloat = 0.4) {
        self.isEnabled = isEnabled
        self.alpha = isEnabled ? 1 : disabledAlpha
    }
    
}
