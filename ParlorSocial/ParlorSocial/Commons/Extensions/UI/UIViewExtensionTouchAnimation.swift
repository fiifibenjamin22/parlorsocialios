//
//  UIViewExtensionTouchAnimation.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 26/02/2019.
//  Copyright © 2019 KISS DIGITAL SP Z O O. All rights reserved.
//

import UIKit

extension UIView {
    
    func applyTouchAnimation() {
        let gestureRec = TouchGestureRecognizer()
        gestureRec.addAction(
            beginAction: { [unowned self] _ in
                self.animateTouchedDown()
        }, endAction: { [unowned self] in
            self.animateTouchedUp()
        })
        addGestureRecognizer(gestureRec)
    }
    
    @objc private func animateTouchedDown(completion: (() -> Void)? = nil) {
        UIView.animate(withDuration: 0.1, animations: {
            if self.hasShadow {
                self.layer.shadowOpacity = 0
            }
            self.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
        }) {_ in
            if self is UIControl {
                UIImpactFeedbackGenerator().impactOccurred()
            }
            completion?()
        }
    }
    
    @objc private func animateTouchedUp(completion: (() -> Void)? = nil) {
        UIView.animate(withDuration: 0.1, animations: {
            if self.hasShadow {
                self.layer.shadowOpacity = UIView.defaultShadowOpacity
            }
            self.transform = CGAffineTransform(scaleX: 1, y: 1)
        }) {_ in
            completion?()
        }
    }
}
