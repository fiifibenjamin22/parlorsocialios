//
//  UIStackViewExtension.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 26/02/2019.
//  Copyright © 2019 KISS DIGITAL SP Z O O. All rights reserved.
//

import UIKit

extension UICollectionView {
    
    func registerCell(_ cellClass: UICollectionViewCell.Type) {
        self.register(cellClass, forCellWithReuseIdentifier: cellClass.name)
    }
    
    func registerHeaderView(_ headerClass: UICollectionReusableView.Type) {
        self.register(headerClass, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerClass.name)
    }
    
    func registerFooterView(_ footerClass: UICollectionReusableView.Type) {
        self.register(footerClass, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: footerClass.name)
    }

    func dequeueReusableCell<T: UICollectionViewCell>(
        with identifier: String? = nil,
        for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withReuseIdentifier: identifier ?? T.name, for: indexPath) as? T else {
            fatalError("Cannot dequeue: \(T.self) with identifier: \(T.name)")
        }
        
        return cell
    }
    
}
