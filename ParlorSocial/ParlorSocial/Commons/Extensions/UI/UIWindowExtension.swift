//
//  UIWindowExtension.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 26/02/2019.
//  Copyright © 2019 KISS DIGITAL SP Z O O. All rights reserved.
//

import UIKit

extension UIWindow {
    
    func setRootViewController(_ controller: UIViewController, options: TransitionOptions = TransitionOptions()) {
        var transitionWindow: UIWindow?
        if let background = options.background {
            transitionWindow = UIWindow(frame: UIScreen.main.bounds)
            switch background {
            case .customView(let view):
                transitionWindow?.rootViewController = UIViewController.newController(withView: view, frame: transitionWindow!.bounds)
            case .color(let color):
                transitionWindow?.backgroundColor = color
            }
            transitionWindow?.makeKeyAndVisible()
        }
        
        // Make animation
        self.layer.add(options.animation, forKey: kCATransition)
        self.rootViewController = controller
        self.makeKeyAndVisible()
        
        if let window = transitionWindow {
            DispatchQueue.main.asyncAfter(deadline: (.now() + 1 + options.duration), execute: window.removeFromSuperview)
        }
    }
    
    struct TransitionOptions {
        
        enum Curve {
            case linear
            case easeIn
            case easeOut
            case easeInOut
            
            var function: CAMediaTimingFunction {
                switch self {
                case .linear: return CAMediaTimingFunction(name: .linear)
                case .easeIn: return CAMediaTimingFunction(name: .easeIn)
                case .easeOut: return CAMediaTimingFunction(name: .easeOut)
                case .easeInOut: return CAMediaTimingFunction(name: .easeInEaseOut)
                }
            }
        }
        
        enum Direction {
            case fade
            case fromTop
            case fromBottom
            case fromLeft
            case fromRight
            
            func transition() -> CATransition {
                let transition = CATransition()
                transition.type = CATransitionType.push
                switch self {
                case .fade:
                    transition.type = CATransitionType.fade
                    transition.subtype = nil
                case .fromTop: transition.subtype = CATransitionSubtype.fromTop
                case .fromBottom: transition.subtype = CATransitionSubtype.fromBottom
                case .fromLeft: transition.subtype = CATransitionSubtype.fromLeft
                case .fromRight: transition.subtype = CATransitionSubtype.fromRight
                }
                
                return transition
            }
        }
        
        enum Background {
            case color(UIColor)
            case customView(UIView)
        }
        
        public var duration: TimeInterval = Constants.transitionAnimationDuration
        public var direction: TransitionOptions.Direction = .fromLeft
        public var style: TransitionOptions.Curve = .linear
        public var background: TransitionOptions.Background?
        
        init(direction: Direction = .fromRight, style: Curve = .linear) {
            self.direction = direction
            self.style = style
        }
        
        init() { }
        
        var animation: CATransition {
            let transition = self.direction.transition()
            transition.duration = self.duration
            transition.timingFunction = self.style.function
            return transition
        }
    }
    
    static var safeAreaTopInset: CGFloat {
        return UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0
    }
    
    static var safeAreaBottomInset: CGFloat {
        return UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
    }
}

internal extension UIViewController {
    
    static func newController(withView view: UIView, frame: CGRect) -> UIViewController {
        view.frame = frame
        let controller = UIViewController()
        controller.view = view
        return controller
    }
    
}
