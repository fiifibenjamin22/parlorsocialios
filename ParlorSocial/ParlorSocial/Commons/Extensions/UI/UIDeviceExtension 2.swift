//
//  UIDeviceExtension.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 25/07/2019.
//

import UIKit

extension UIDevice {
    var hasNotch: Bool {
        return UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0 > 0
    }
}
