//
//  SequenceExtension.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 10/05/2019.
//

import Foundation

public extension Sequence where Element: Equatable {
    var uniqueElements: [Element] {
        return self.reduce(into: []) { uniqueElements, element in
            if !uniqueElements.contains(element) {
                uniqueElements.append(element)
            }
        }
    }
}
