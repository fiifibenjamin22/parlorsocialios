//
//  DecodableExtension.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 25/02/2019.
//

import Foundation

extension Decodable {
    
    static func make(from json: JSON) throws -> Self {
        let data = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
        return try decoder.decode(Self.self, from: data)
    }
    
    static func make(from data: Data) throws -> Self {
        return try decoder.decode(Self.self, from: data)
    }
    
    static func makeSafe(from data: Data) -> Self? {
        do {
            return try Self.make(from: data)
        } catch {
            return nil
        }
    }
    
    private static var decoder: JSONDecoder {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        decoder.dateDecodingStrategy = .custom({ decoder -> Date in
            let container = try decoder.singleValueContainer()
            let dateString: String = try container.decode(String.self)
            
            return DateFormatter.apiFormatter.date(from: dateString) ?? Date()
        })
        
        return decoder
    }
    
}
