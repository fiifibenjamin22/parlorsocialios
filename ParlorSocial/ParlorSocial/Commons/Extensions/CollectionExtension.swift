//
//  CollectionExtension.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 25/02/2019.
//

import Foundation

extension Collection {
    
    subscript(safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
    
}
