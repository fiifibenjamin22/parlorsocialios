//
//  DateExtension.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 25/02/2019.
//

import Foundation

extension Date {
    
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }

    enum Format {
        static let api = "yyyy-MM-dd'T'HH:mm:ssXXXXX"
        static let allActivations = "h:mm a"
        static let activationDetails = "h:mma"
        static let nonUpcomingRsvpsDate = "EEE, MMM d"
        static let historyRsvpsDate = "MMM d, YYYY"
        static let dateOfBirth = "yyyy-MM-dd"
        static let dateOfBirthEdit = "MM-dd-yyyy"
        static let membershipRenewal = "MM/dd/yyyy"
    }

    enum TimePeriod {
        case today, tonight, tomorrow, other

        init(dateToFormat: Date, currentDate: Date) {
            if currentDate.isTodayEarlyMorning {
                self = .other
            } else if dateToFormat.isToday {
                self = dateToFormat.isEvening ? .tonight : .today
            } else if dateToFormat.isTomorrow {
                self = .tomorrow
            } else {
                self = .other
            }
        }
    }

    func toString(withFormat format: String = Format.api) -> String {
        let formatter = dateFormatter
        formatter.dateFormat = format
        
        return formatter.string(from: self)
    }

    func toAllActivationsHeaderFormat(isApproximatedStartTime: Bool, currentDate: Date = Date()) -> String {
        guard !isApproximatedStartTime else { return Strings.Activation.dateTBD.localized.uppercased() }

        let period = TimePeriod(dateToFormat: self, currentDate: currentDate)

        switch period {
        case .today, .tonight: return Strings.Activation.today.localized.uppercased()
        case .tomorrow: return Strings.Activation.tomorrow.localized.uppercased()
        case .other:
            let formatter = dateFormatter
            formatter.dateFormat = "EEEE, MMM d'\(daySuffix)'"

            return formatter.string(from: self).uppercased()
        }
    }

    func toActivationUpdateFormat() -> String {
        let formatter = dateFormatter
        formatter.dateFormat = "EEEE, MMM d'\(daySuffix)', h:mma"
        return formatter.string(from: self)
    }

    func toAnnouncementsFormat() -> String {
        let now = Date()
        let difference = Calendar.current.dateComponents([.hour, .day], from: self, to: now)
        guard let hoursDiff = difference.hour,
            let daysDiff = difference.day else {
                return ""
        }
        
        switch (daysDiff, hoursDiff) {
        case (daysDiff, _) where daysDiff > 1:
            return String(format: Strings.Announcements.daysAgo.localized, daysDiff)
        case (daysDiff, _) where daysDiff == 1:
            return Strings.Announcements.singleDayAgo.localized
        case (_, hoursDiff) where hoursDiff >= 1:
            return Strings.Announcements.today.localized
        default:
            return Strings.Announcements.justNow.localized
        }
    }

    func toCalendarHeaderDate() -> String {
        let formatter = dateFormatter
        formatter.dateFormat = "EEEE, MMMM d'\(daySuffix)'"
        
        return formatter.string(from: self).uppercased()
    }

    func toMixerStartDateFormat(isApproximatedStartTime: Bool, currentDate: Date = Date()) -> String {
        guard !isApproximatedStartTime else {
            return Strings.Activation.dateTBD.localized
        }

        let period = TimePeriod(dateToFormat: self, currentDate: currentDate)
        let nonUpcomingDateFormat = "EEEE, MMM d'\(daySuffix)' h:mma"
        let upcomingDateFormat = "h:mma"
        let formatter = dateFormatter
        formatter.dateFormat = period == .other ? nonUpcomingDateFormat : upcomingDateFormat

        let formattedDate = formatter.string(from: self)

        switch period {
        case .today: return "\(Strings.Activation.today.localized) " + formattedDate
        case .tonight: return "\(Strings.Activation.tonight.localized) " + formattedDate
        case .tomorrow: return "\(Strings.Activation.tomorrow.localized) " + formattedDate
        case .other: return formattedDate
        }
    }

    func toActivationDetailsStartDateFormat(isApproximatedStartTime: Bool) -> String {
        let formatter = dateFormatter
        formatter.dateFormat = "EEEE, MMM d'\(daySuffix)' h:mma"

        if isApproximatedStartTime {
            return Strings.Activation.dateTBD.localized
        } else {
            return formatter.string(from: self)
        }
    }
    
    func toFullDescriptiveDateFormat() -> String {
        let formatter = dateFormatter
        formatter.dateFormat = "EEEE, MMMM d'\(daySuffix)' h:mma"
        
        return formatter.string(from: self)
    }
    
    func toWelcomeMessageFormat() -> String {
        let formatter = dateFormatter
        formatter.dateFormat = "MMMM d'\(daySuffix)', EEEE"
        
        return formatter.string(from: self)
    }

    func toMonthAndDayFormat(isApproximatedStartTime: Bool) -> String {
        let formatter = dateFormatter
        formatter.dateFormat = "MMM d'\(daySuffix)'"

        if isApproximatedStartTime {
            return Strings.Activation.tbd.localized
        } else {
            return formatter.string(from: self).uppercased()
        }
    }
    
    func toDetailsFormat(withEndDate endDate: Date?, isApproximatedStartTime: Bool) -> String {
        let startDate = self.toMixerStartDateFormat(isApproximatedStartTime: isApproximatedStartTime)
        guard let endDate = endDate, !isApproximatedStartTime else {
            return startDate
        }
        let endDateText = endDate.toString(withFormat: Format.activationDetails)
        return "\(startDate) - \(endDateText)"
    }

    func toAttendingFormat(withEndDate endDate: Date?) -> String {
        let startDate = self.toString(withFormat: Format.activationDetails)
        guard let endDate = endDate else { return startDate }
        let endDateText = endDate.toString(withFormat: Format.activationDetails)
        return "\(startDate)-\(endDateText)"
    }

    func toCalendarItemFormat(withEndDate endDate: Date?) -> String {
        let startHourText = self.toString(withFormat: Format.activationDetails)
        guard let endDate = endDate else {
            return startHourText.uppercased()
        }
        let endHourText = endDate.toString(withFormat: Format.activationDetails)
        return "\(startHourText) - \(endHourText)".uppercased()
    }
    
    func toUpcomingRsvpsDate(currentDate: Date = Date()) -> String {
        let period = TimePeriod(dateToFormat: self, currentDate: currentDate)

        switch period {
        case .today: return Strings.MyRsvps.upcomingToday.localized
        case .tonight: return Strings.MyRsvps.upcomingTonight.localized
        case .tomorrow: return Strings.MyRsvps.upcomingTomorrow.localized
        case .other:
            let formatter = dateFormatter
            formatter.dateFormat = Format.nonUpcomingRsvpsDate

            let formattedDate = formatter.string(from: self)
            return formattedDate
        }
    }

    func toUpcomingActivationDate() -> String {
        let formatter = dateFormatter
        formatter.dateFormat = Format.allActivations
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"

        let formattedDate = formatter.string(from: self)
        return formattedDate
    }
    
    func toHistoryRsvpsDate() -> String {
        let formatter = dateFormatter
        formatter.dateFormat = Format.historyRsvpsDate
        return formatter.string(from: self)
    }

    func isTheSameDay(as otherDate: Date) -> Bool {
        return Calendar.current.isDate(self, inSameDayAs: otherDate)
    }
    
    func countOfDaysFrom(date: Date?) -> Int? {
        guard let date = date else { return nil }
        let calendar = Calendar.current

        let startDate = calendar.startOfDay(for: date)
        let endDate = calendar.startOfDay(for: self)

        let components = calendar.dateComponents([.day], from: startDate, to: endDate)
        return components.day
    }
    
    func countOfMonthsFrom(date: Date?) -> Int? {
        guard let date = date else { return nil }
        let calendar = Calendar.current

        let startDate = calendar.startOfDay(for: date)
        let endDate = calendar.startOfDay(for: self)

        let components = calendar.dateComponents([.month], from: startDate, to: endDate)
        return components.month
    }
    
    var countOfDaysFromToday: Int? {
        let calendar = Calendar.current

        let currentDate = calendar.startOfDay(for: Date())
        let endDate = calendar.startOfDay(for: self)

        let components = calendar.dateComponents([.day], from: currentDate, to: endDate)
        return components.day
    }
    
    var isFuture: Bool {
        return self > Date()
    }
    
    var isToday: Bool {
        return Calendar.current.isDateInToday(self)
    }

    var isTodayEarlyMorning: Bool {
        guard self.isToday else { return false }
        // Early moring is range between 12AM and 4AM
        let earlyMorningEndHour = 4
        let hour = Calendar.current.component(.hour, from: self)

        return hour < earlyMorningEndHour
    }

    var isEvening: Bool {
        // Bussiness logic, "tonight" time is defined by client and begins at 18:00
        let tonightHour = 18
        let hour = Calendar.current.component(.hour, from: self)

        return hour >= tonightHour
    }

    var isTomorrow: Bool {
        return Calendar.current.isDateInTomorrow(self)
    }
}

extension Date {

    static func formatTimeInterval(from startDate: Date, to endDate: Date?) -> String {
        let formatter = startDate.dateFormatter
        formatter.dateFormat = "h:mma"

        let startString = formatter.string(from: startDate).uppercased()

        guard let endDate = endDate else { return startString }
        return "\(formatter.string(from: startDate)) - \(formatter.string(from: endDate))".uppercased()
    }
    
    static func yearsBetweenDate(startDate: Date, endDate: Date) -> Int? {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year], from: startDate, to: endDate)
        return components.year
    }
    
    static func date(from expMonth: Int?, and expYear: Int?) -> Date? {
        var dateComponents = DateComponents()
        dateComponents.year = (expYear ?? 0) + 2000
        if let strongExpMonth = expMonth {
            dateComponents.month = strongExpMonth + 1
        }
        return Calendar.current.date(from: dateComponents)
    }
}

extension Date {

    private var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.amSymbol = "am"
        formatter.pmSymbol = "pm"

        return formatter
    }

    private var daySuffix: String {
        let calendar = Calendar.current
        let dayOfMonth = calendar.component(.day, from: self)

        switch dayOfMonth {
        case 1, 21, 31: return "st"
        case 2, 22: return "nd"
        case 3, 23: return "rd"
        default: return "th"
        }
    }

}
