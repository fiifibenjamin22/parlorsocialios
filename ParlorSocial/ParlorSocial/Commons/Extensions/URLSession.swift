//
//  URLSession.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 07/01/2020.
//

import Foundation

extension URLSession {
    
    class func downloadImage(atURL url: URL, withCompletionHandler completionHandler: @escaping (Data?, Error?) -> Void) {
        let dataTask = URLSession.shared.dataTask(with: url) { (data, _, error) in
            completionHandler(data, error)
        }
        dataTask.resume()
    }
}
