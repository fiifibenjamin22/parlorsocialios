//
//  CGFloatExtension.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 02/04/2019.
//

import UIKit

extension CGFloat {
    
    /// The least positive number for TableView.
    ///
    /// * leastNormalMagnitude and leastNonzeroMagnitude will be treated as minus (hence the crash).
    /// * Zero will make the TableView return the default height as header / footer.
    /// * Anything between zero and one will be treated as a minus.
    /// * One will make the TableView return the default height.
    static var leastPositiveForTableView: CGFloat {
        return CGFloat.leastNormalMagnitude
    }
}
