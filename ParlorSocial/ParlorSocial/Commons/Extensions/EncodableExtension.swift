//
//  EncodableExtension.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 26/02/2019.
//  Copyright © 2019 KISS DIGITAL SP Z O O. All rights reserved.
//

import Foundation

extension Encodable {
    
    var asDictionary: JSON {
        do {
            return try asDictionaryThrowable()
        } catch {
            #if DEBUG
            fatalError(error.localizedDescription)
            #endif
        }
        return [:]
    }
    
    func asDictionaryThrowable() throws -> JSON  {
        let data = try Self.encoder.encode(self)
        let serializedObject = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
        
        if let dictionary = serializedObject as? JSON {
            return dictionary
        } else if let singleObjectArray = serializedObject as? NSArray,
            let json = singleObjectArray.firstObject,
            let dictionary = json as? JSON {
            return dictionary
        } else {
            throw AppError(with: Strings.encodingError.localized + ": \(self)")
        }
    }
    
    var asData: Data? {
        do {
            let json = try self.asDictionaryThrowable()
            return try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
        } catch {
            return nil
        }
    }
    
    private static var encoder: JSONEncoder {
        let encoder = JSONEncoder()
        encoder.keyEncodingStrategy = .convertToSnakeCase
        encoder.dateEncodingStrategy = .custom({ date, encoder -> Void in
            let dateString = DateFormatter.apiFormatter.string(from: date)
            var container = encoder.singleValueContainer()
            try container.encode(dateString)
        })
        return encoder
    }
}
