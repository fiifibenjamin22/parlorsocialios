//
//  DictionaryExtension.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 27/04/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

extension Dictionary {
    func merging(_ other: [Key: Value]) -> [Key: Value] {
        return merging(other, uniquingKeysWith: { leftValue, _ in leftValue })
    }
}
