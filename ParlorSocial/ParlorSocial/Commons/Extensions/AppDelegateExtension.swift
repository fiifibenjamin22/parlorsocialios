//
//  AppDelegateExtension.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 25/02/2019.
//

import UIKit
import IQKeyboardManagerSwift
import Crashlytics
import Firebase
import AlamofireNetworkActivityLogger
import UserNotifications
import GoogleMaps
import RxSwift
import AVFoundation
import FBSDKCoreKit
import Stripe

extension AppDelegate {
    
    func setupApp(notificationOption: Any?, activityDictionary: [AnyHashable: Any]?) {
        AnalyticService.shared.refreshSessionId()
        setupWindow()
        setupFirebase()
        setupKeyboardHandler()
        setupDesign()
        setupNetworkLogger()
        setupGlobalSettings()
        setAudioMix()
        setupInitialScene(notificationOption: notificationOption, deepLink: getDeepLinkFromLaunching(activityDictionary: activityDictionary))
        setupGoogleMaps()
        setupStripe()
        doForAppWillEnterForegroundAndLaunching()
    }
    
    func appWillEnterForeground() {
        sendAnalyticEvents(inBackground: false)
        AnalyticService.shared.refreshSessionId()
        updateProfile()
        doForAppWillEnterForegroundAndLaunching()
    }
    
    func appDidEnterBackground() {
        sendAnalyticEvents(inBackground: true)
    }
    
    func appDidBecomeActive() {
        UIApplication.shared.applicationIconBadgeNumber = 0
        AppEvents.activateApp()
        checkAppUpdate()
    }

}

extension AppDelegate {
    
    private func setupFirebase() {
        FirebaseApp.configure()
        #if !DEBUG
        Fabric.with([Crashlytics.self])
        #endif
    }
    
    private func setupGoogleMaps() {
        GMSServices.provideAPIKey(Config.mapsApiKey)
    }
    
    private func setupStripe() {
        Stripe.setDefaultPublishableKey(Config.stripePublishableKey)
    }

    private func setupWindow() {
        self.window = UIWindow()
    }
    
    private func setupKeyboardHandler() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
    }
    
    private func setupDesign() {
        self.window?.tintColor = .appTintColor
        self.window?.backgroundColor = .appBackground
        UINavigationBar.appearance().tintColor = .appTintColor
        UINavigationBar.appearance().backgroundColor = .white
    }
    
    private func setupInitialScene(notificationOption: Any?, deepLink: URL?) {
        let initialSceneVC: UIViewController = {
            if !Config.notShowWelcomeMovie, notificationOption == nil, deepLink == nil {
                return setupWelcomeMovieFlow()
            }
            switch KeychainWorker().userToken != nil {
            case true: return setupMainAppFlow()
            case false: return setupAuthenticationScene()
            }
        }()
        self.window?.rootViewController = initialSceneVC
        self.window?.makeKeyAndVisible()
    }
    
    private func setupNetworkLogger() {
        #if DEBUG
        NetworkActivityLogger.shared.level = .debug
        NetworkActivityLogger.shared.startLogging()
        #endif
    }
    
    private func setupAuthenticationScene() -> UIViewController {
        return LoginViewController().embedInNavigationController()
    }

    private func setupMainAppFlow() -> UIViewController {
        if isAppUpdateRequired() {
            return AppUpdateRequiredViewController()
        } else if Config.shouldInterceptWithPaymentScreen {
            return InitialViewControllerUtility.getViewControllerWithLoginAsRoot(viewController: PremiumPaymentViewController())
        } else if Config.shouldInterceptWithMembershipPlansScreen {
            return InitialViewControllerUtility.getViewControllerWithLoginAsRoot(viewController: MembershipPlansViewController())
        } else if Config.shouldInterceptWithDashboardScreen {
            return AppTabBarController().embedInNavigationController()
        } else if Config.shouldInterceptWithUserNotApprovedScreen {
            return UserNotApprovedViewController()
        } else {
            return setupAuthenticationScene()
        }
    }
    
    private func isAppUpdateRequired() -> Bool {
        guard let latestAppUpdateInfo = Config.appUpdateInfo else { return false }
        return latestAppUpdateInfo.appUpdateStrategy == .required
    }

    private func setupWelcomeMovieFlow() -> UIViewController {
        return WelcomeMovieViewController()
    }
    
    private func doForAppWillEnterForegroundAndLaunching() {
        updateNotificationsReminderDataManager.updateDataOnDidBecomeActive()
        incrementAppEntryCountHelper.incrementAppEntryCounters()
        CalendarUtilities.shared.checkIfPermissionsAreChangedInDeviceSetting(settingsChangeSource: .manual)
    }
    
    private func handlePasswordReset(withUrl url: URL) {
        let parameters = url.queryParameters
        if let email = parameters[Config.emailQueryKey], let token = parameters[Config.tokenQueryKey] {
            self.window?.rootViewController = ResetPasswordViewController.fromResetPasswordDeepLink(forUserWithEmail: email, token: token)
            self.window?.makeKeyAndVisible()
        }
    }
    
    private func setupGlobalSettings() {
        ProfileRepository.shared
            .getSettingsInfo()
            .subscribe()
            .disposed(by: disposeBag)
    }

    private func checkAppUpdate() {
        appUpdateChecker.loadAppVersionInformationIfNeeded()
            .subscribe(onNext: { [unowned self] required in
                guard required else { return }
                guard !(self.window?.rootViewController is AppUpdateRequiredViewController) else { return }

                let controller = AppUpdateRequiredViewController()
                self.window?.setRootViewController(controller, options: .init(direction: .fade, style: .easeInOut))
                self.window?.makeKeyAndVisible()
            })
            .disposed(by: disposeBag)
    }

    private func setAudioMix() {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default, options: [.mixWithOthers])
            try AVAudioSession.sharedInstance().setActive(true)
        } catch {
            print("something went wrong")
        }
    }
    
    func restartMainAppFlow() -> AppViewController? {
        self.window?.rootViewController = AppTabBarController().embedInNavigationController()
        self.window?.makeKeyAndVisible()
        guard let topVC = self.window?.rootViewController?.top,
            let topAppVC = topVC as? AppViewController else { return nil }
        return topAppVC
    }
    
    private func sendAnalyticEvents(inBackground: Bool) {
        guard Config.userProfile != nil else { return }
        if inBackground {
            AnalyticService.shared.sendEventsInBackground()
        } else {
            AnalyticService.shared.sendEvents()
        }
    }
    
    private func updateProfile() {
        guard Config.userProfile != nil else { return }
        ProfileRepository.shared.refreshProfileData()
    }
    
    private func getDeepLinkFromLaunching(activityDictionary: [AnyHashable: Any]?) -> URL? {
        guard let activityDictionary = activityDictionary else { return nil }
        for key in activityDictionary.keys {
            if let userActivity = activityDictionary[key] as? NSUserActivity {
                return userActivity.webpageURL
            }
        }
        return nil
    }
    
}
