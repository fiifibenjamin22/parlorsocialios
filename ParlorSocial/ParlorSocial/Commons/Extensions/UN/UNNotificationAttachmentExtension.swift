//
//  UNNotificationAttachmentExtension.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 07/01/2020.
//

import Foundation
import UserNotifications

extension UNNotificationAttachment {
    static func saveNotificationImageToDisk(fileIdentifier: String, data: NSData, options: [NSObject: AnyObject]?) -> UNNotificationAttachment? {
        let fileManager = FileManager.default
        let folderName = "NotificationAttachments"
        let folderURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(folderName, isDirectory: true)
        
        do {
            if !fileManager.fileExists(atPath: folderURL.path) {
                try fileManager.createDirectory(at: folderURL, withIntermediateDirectories: true, attributes: nil)
            }
            let fileURL = folderURL.appendingPathComponent(fileIdentifier)
            try data.write(to: fileURL, options: [])
            let attachment = try UNNotificationAttachment(identifier: fileIdentifier, url: fileURL, options: options)
            return attachment
        } catch let error {
            print("Notification Attachment Error: \(error)")
        }
        
        return nil
    }
}
