//
//  ObservableTypeExtension.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 02/04/2019.
//

import Foundation
import Alamofire
import RxSwift
import RxCocoa
import RxSwiftExt

extension ObservableType {

    func setLoadingStateOnSubscribe(with realy: BehaviorRelay<RepositoryPaginatedDataState<TableSection>>) -> Observable<Element> {
        return self.do(onSubscribe: { realy.accept(.loading) })
    }
    
    func changeSearchState(on relay: PublishRelay<MainSearchState>, searchState: MainSearchState) -> Observable<Element> {
        return self.do(onNext: {_ in relay.accept(searchState) })
    }

    func withLatestMetadata(from relay: BehaviorRelay<PaginatedData>) -> Observable<ListMetadata> {
        return self.withLatestFrom(relay, resultSelector: { return $1.metadata })
    }

    func skipOnListPopulated(with relay: BehaviorRelay<PaginatedData>) -> Observable<ListMetadata> {
        return withLatestMetadata(from: relay)
            .filter { !$0.isLastPage }
    }
    
    func filterMapWithErrorHandling<T: Decodable> (errorSubject: PublishSubject<AppError>) -> Observable<T>
        where Element == ApiResponse<DataResponse<T>> {
        return self.filterMap { [unowned errorSubject] response -> FilterMap<T> in
            switch response {
            case .success(let item):
                return .map(item.data)
            case .failure(let error):
                errorSubject.onNext(error)
                return .ignore
            }
        }
    }
    
    func mapResponseToTableSection<T: Decodable & PaginatableResponse>(
        with dataRelay: BehaviorRelay<PaginatedData>,
        sectionStateRelay: BehaviorRelay<RepositoryPaginatedDataState<TableSection>>,
        sectionMaker: @escaping ([T.ListElement]) -> [TableSection]) -> Observable<PaginatedData> where Element == ApiResponse<T> {

        return self.filter { apiResponse in
            switch apiResponse {
            case .success:
                return true
            case .failure(let error):
                sectionStateRelay.accept(.error(error))
                return false
            }
        }.withLatestFrom(dataRelay, resultSelector: { return ($0, $1.sections) }).map({ (apiResponse, sections) -> PaginatedData in
                switch apiResponse {
                case .success(let response):
                    let previousElements: [T.ListElement] = sections.flatMap { $0.getItems() ?? [] }
                    let elements: [T.ListElement] = previousElements + response.data
                    return PaginatedData(sectionMaker(elements), response.meta)
                default:
                    return dataRelay.value
                }
            })
    }
    
    func mergeWithCurrentPaginatableData<T: Decodable & PaginatableResponse>(
        from relay: BehaviorRelay<[T]>,
        errorSubject: PublishSubject<AppError>
    ) -> Observable<[T]> where Element == ApiResponse<T> {
        return self.withLatestFrom(relay) { ($0, $1) }
            .map { (apiResponse, currentData) in
                switch apiResponse {
                case .success(let newData):
                    return currentData + [newData]
                case .failure(let error):
                    errorSubject.onNext(error)
                    return currentData
                }
            }
    }

    func doOnNext(_ block: @escaping (Element) -> Void) -> Observable<Element> {
        return self.do(onNext: block)
    }

}

extension ObservableType where Element == ApiResponse<MessageResponse> {
    
    func filterMapWithErrorHandling(errorSubject: PublishSubject<AppError>) -> Observable<MessageResponse> {
        return self.filterMap { [unowned errorSubject] response -> FilterMap<MessageResponse> in
            switch response {
            case .success(let item):
                return .map(item)
            case .failure(let error):
                errorSubject.onNext(error)
                return .ignore
            }
        }
    }
    
}

public extension ObservableType where Element == String? {

    func emptyOnNil() -> Observable<String> {
        return self.map {
            $0 ?? ""
        }
    }

}

extension ObservableType where Element == DataRequest {

    func validateUserLoggedInAndPaidForSubscription() -> Observable<DataRequest> {
        let acceptableStatuses = Array(0...400) + Array(403...500)
        return self.validate(statusCode: acceptableStatuses)
    }

}

extension ObservableType where Element == (HTTPURLResponse, Data) {
    
    func asApiResponseObs<E: Decodable>() -> Observable<ApiResponse<E>> {
        return self.flatMap({ (tuple) -> Observable<ApiResponse<E>> in
            let (response, data) = tuple
            guard Array(200..<206).contains(response.statusCode) else {
                return Observable.just(ApiResponse.failure(AppError(from: data, code: response.statusCode, errorSource: .apiError)))
            }
            do {
                let decoded = try E.make(from: data)
                return Observable.just(ApiResponse.success(decoded))
            } catch {
                print(error)
                return Observable.just(ApiResponse.failure(AppError(with: error, errorSource: .apiError)))
            }
        }).catchError { error in
            return Observable.just(ApiResponse.failure(AppError(with: error, errorSource: .apiError)))
        }
        
    }

    func asEmptyResposne() -> Observable<EmptyResponse> {
        return self.flatMap({ (tuple) -> Observable<EmptyResponse> in
            let (reponse, data) = tuple
            guard Array(200..<206).contains(reponse.statusCode) else {
                return Observable.just(EmptyResponse.failure(AppError(from: data, code: reponse.statusCode, errorSource: .apiError)))
            }
            return Observable.just(EmptyResponse.success)
        }).catchError { error in
            return Observable.just(EmptyResponse.failure(AppError(with: error, errorSource: .apiError)))
        }
    }

}

extension PublishSubject where Element == Void {

    func emitElement() {
        self.onNext(())
    }

}

extension ObservableType where Element == Void {
    
    func startWithElement() -> Observable<Void> {
        return self.startWith(Void())
    }
    
}

extension ObservableType where Element == AppError {

    func handleWithAlerts(using viewController: AppViewControllerClass) -> Disposable {
        return subscribe(onNext: { [weak viewController] appError in
            if appError.shouldShowAlertPopup {
                viewController?.present(ParlorAlertViewController(alertData: AlertData.fromAppError(appError: appError)), animated: true, completion: nil)
            }
        })
    }

}

extension ObservableType where Element == PaginatedData {

    func changePaginatedState(with relay: BehaviorRelay<RepositoryPaginatedDataState<TableSection>>) -> Observable<Element> {
        return self.do(onNext: { (sections, metadata) in
            if metadata.isLastPage {
                relay.accept(.populated(elements: sections))
            } else {
                relay.accept(.paging(elements: sections))
            }
        })
    }

}

extension ObservableType where Element == AlertData {
    
    func show(using viewController: AppViewControllerClass) -> Disposable {
        return subscribe(onNext: { [weak viewController] data in
            if data.shouldShowAlertPopup {
                viewController?.present(ParlorAlertViewController(alertData: data), animated: true, completion: nil)
            }
        })
    }
    
}

extension ObservableType where Element == Bool {
    
    func showProgressBar(using viewController: UIViewController, inside view: UIView? = nil, isDark: Bool = false) -> Disposable {
        return subscribe(onNext: { [weak viewController, view] isVisible in
            guard let viewController = viewController else { return }
            
            if isVisible {
                guard viewController.children.first(where: { $0 is ProgressIndicatorViewController }) == nil else { return }
                viewController.addChild(viewController: ProgressIndicatorViewController(isDark: isDark), inside: view ?? viewController.view)
            } else {
                if let progressIndicatorVC = viewController.children.first(where: { $0 is ProgressIndicatorViewController }) {
                    UIView.animate(withDuration: 0.2, animations: { [weak progressIndicatorVC] in
                        progressIndicatorVC?.view.alpha = 0
                    }, completion: { [weak progressIndicatorVC, viewController] _ in
                        guard let progressIndicatorVC = progressIndicatorVC else { return }
                        viewController.removeChild(viewController: progressIndicatorVC)
                    })
                }
            }
        })
    }
    
}

extension ObservableType {
    
    func flatMapWithConfirmation(using subject: PublishSubject<AlertData>, alertData: AlertData) -> Observable<Element> {
        return self.flatMap { (item: Element) -> Observable<Element> in
            return Observable<Element>.create { (observer: AnyObserver<Element>) -> Disposable in
                subject.onNext(alertData.withButtonAction(sourceName: alertData.analyticAlertSourceName, buttonAction: {
                    observer.onNext(item)
                    observer.onCompleted()
                }))
                return Disposables.create()
            }
        }
    }
    
    func showingProgressBar(with logic: BaseViewModel) -> Observable<Element> {
        return self.do(
            onNext: { _ in
                logic.progressBarSubject.onNext(false) },
            onSubscribe: {
                logic.progressBarSubject.onNext(true) }
        )
    }

}

// MARK: Helper for filtering nil values and return non optional value in onSubscribe
public protocol OptionalType {
    associatedtype Wrapped
    
    var optional: Wrapped? { get }
}

extension Optional: OptionalType {
    public var optional: Wrapped? { return self }
}

extension ObservableType where Element: OptionalType {
    func ignoreNil() -> Observable<Element.Wrapped> {
        return flatMap { value in
            value.optional.map { Observable<Element.Wrapped>.just($0) } ?? Observable<Element.Wrapped>.empty()
        }
    }
}
