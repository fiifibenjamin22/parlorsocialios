//
//  DateFormatterExtension.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 01/03/2019.
//

import Foundation

extension DateFormatter {
    
    static let apiFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = Date.Format.api
        return formatter
    }()
    
    static let dateOfBirthEditFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = Date.Format.dateOfBirth
        return formatter
    }()
    
}
