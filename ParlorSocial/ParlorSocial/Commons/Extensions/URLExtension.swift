//
//  URLExtension.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 06/05/2019.
//

import Foundation

extension URL {
    
    init?(string: String?) {
        guard let string = string else { return nil }
        self.init(string: string)
    }
    
    var queryParameters: [String: String] {
        guard let components = URLComponents(url: self, resolvingAgainstBaseURL: true),
            let queryItems = components.queryItems else { return [:] }
        return queryItems.reduce(into: [String: String]()) { (result, item) in
            result[item.name] = item.value
        }
    }
    
}
