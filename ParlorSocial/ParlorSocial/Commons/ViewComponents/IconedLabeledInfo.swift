//
//  IconedLabeledInfo.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 15/04/2019.
//

import UIKit

class IconedLabeledInfo: UIStackView {
    
    private let iconSize: CGFloat = 19
    
    private(set) var iconImage: UIImageView = UIImageView().manualLayoutable().apply {
        $0.contentMode = .scaleAspectFit
    }
    
    private(set) var infoTitleLabel: ParlorLabel = ParlorLabel.styled(withTextColor: UIColor.eventTime, withFont: UIFont.custom(ofSize: Constants.FontSize.xTiny, font: UIFont.Roboto.medium), alignment: .left, numberOfLines: 1)
    
    private(set) var infoContentLabel: ParlorLabel = ParlorLabel.styled(withTextColor: UIColor.white, withFont: UIFont.custom(ofSize: Constants.FontSize.hintSize, font: UIFont.Roboto.regular), alignment: .left, numberOfLines: 1)

    init(withIcon icon: UIImage, title: String) {
        super.init(frame: .zero)
        initialize()
        self.iconImage.image = icon
        self.infoTitleLabel.text = title
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        self.spacing = 5
        self.alignment = .center
        addArrangedSubviews([iconImage, infoTitleLabel, infoContentLabel])
        iconImage.widthAnchor.equalTo(constant: iconSize)
        iconImage.heightAnchor.equalTo(constant: iconSize)
    }
}


class CustomColouredIconedLabeledInfo: UIStackView {
    
    private let iconSize: CGFloat = 19
    
    private(set) var iconImage: UIImageView = UIImageView().manualLayoutable().apply {
        $0.contentMode = .scaleAspectFit
    }
    
    private(set) var infoTitleLabel: ParlorLabel = ParlorLabel.styled(withTextColor: UIColor.eventTime, withFont: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium), alignment: .left, numberOfLines: 1)
    
    private(set) var infoContentLabel: ParlorLabel = ParlorLabel.styled(withTextColor: UIColor.white, withFont: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.regular), alignment: .left, numberOfLines: 1)

    init(withIcon icon: UIImage, title: String, color: UIColor) {
        super.init(frame: .zero)
        initialize()
        self.iconImage.image = icon
        self.infoTitleLabel.text = title
        self.infoTitleLabel.textColor = color
        self.iconImage.tintColor = color
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        self.spacing = 5
        self.alignment = .center
        addArrangedSubviews([iconImage, infoTitleLabel, infoContentLabel])
        iconImage.widthAnchor.equalTo(constant: iconSize)
        iconImage.heightAnchor.equalTo(constant: iconSize)
    }
}
