//
//  GuestsNumberView.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 29/04/2019.
//

import UIKit

enum GuestNumberViewType {
    case canRsvpWithGuests
    case canRsvpWithoutGuests
    
    static func getType(baseOn bottomBarType: ActivationDetailsBottomBarType) -> Self {
        switch bottomBarType {
        case .canRsvpWithGuests: return canRsvpWithGuests
        case .canRsvpWithoutGuests: return canRsvpWithoutGuests
        default:
            fatalError("There is bad ActivationDetailsBottomBarType for creating GuestNumberViewType")
        }
    }
}

class GuestNumberView: UIView {
    
    // MARK: - Views
    lazy var rsvpButton: TrackableButton = {
        return TrackableButton().manualLayoutable().apply {
            $0.setTitleColor(.appText, for: .normal)
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.small, font: UIFont.Editor.medium)
            $0.backgroundColor = .appGreen
            $0.setAttributedTitle(NSAttributedString(
                string: Strings.Activation.rsvp.localized,
                attributes: [.kern: Constants.LetterSpacing.medium]), for: .normal)
            $0.trackingId = AnalyticEventClickableElementName.ActivationDetails.rsvp.rawValue
        }
    }()
    
    lazy var guestsNumberButton: GuestNumberButton = {
        return GuestNumberButton().manualLayoutable().apply {
            $0.trackingId = AnalyticEventClickableElementName.ActivationDetails.guestsNumber.rawValue
        }
    }()
    
    lazy var bottomSpaceGuestsNumberButton: UIView = {
        return UIView().manualLayoutable().apply {
            $0.backgroundColor = .white
        }
    }()
    
    lazy var bottomSpaceRsvpButton: UIView = {
        return UIView().manualLayoutable().apply {
            $0.backgroundColor = .appGreen
        }
    }()

    weak var guestsPickerView: GuestPickerView? {
        didSet {
            handlePickerViewUpdate()
        }
    }
    
    // MARK: - Properties
    private var isEnabled: Bool = true {
        didSet {
            handleEnabled()
        }
    }
    var type: GuestNumberViewType = .canRsvpWithGuests {
        didSet {
            updateView(by: type)
        }
    }
    
    init() {
        super.init(frame: .zero)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Initialization
    private func initialize() {
        addSubviews([guestsNumberButton, rsvpButton, bottomSpaceGuestsNumberButton, bottomSpaceRsvpButton])
        guestsNumberButton.edgesToParent(
            anchors: [.leading, .top, .bottom],
            insets: UIEdgeInsets.margins(bottom: UIDevice.current.hasNotch ? 10 : 0))
        guestsNumberButton.trailingAnchor.equal(to: rsvpButton.leadingAnchor)
        guestsNumberButton.addTarget(self, action: #selector(didTapNumberPickerButton), for: .touchUpInside)
        
        bottomSpaceGuestsNumberButton.edgesToParent(anchors: [.leading, .bottom])
        bottomSpaceGuestsNumberButton.trailingAnchor.equal(to: rsvpButton.leadingAnchor)
        bottomSpaceGuestsNumberButton.heightAnchor.equalTo(constant: UIDevice.current.hasNotch ? 10 : 0)
        
        rsvpButton.edgesToParent(anchors: [.top, .bottom, .trailing], insets: UIEdgeInsets.margins(bottom: UIDevice.current.hasNotch ? 10 : 0))
        rsvpButton.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.377).activate()
        
        bottomSpaceRsvpButton.edgesToParent(anchors: [.bottom, .trailing])
        bottomSpaceRsvpButton.leadingAnchor.equal(to: rsvpButton.leadingAnchor)
        bottomSpaceRsvpButton.heightAnchor.equalTo(constant: UIDevice.current.hasNotch ? 10 : 0)
    }
    
    // MARK: - Private methods
    private func handlePickerViewUpdate() {
        guard let pickerView = guestsPickerView else { return }
        pickerView.isHidden = true
        pickerView.doneButton.addTarget(self, action: #selector(didTapDoneButton), for: .touchUpInside)
    }
    
    private func handleEnabled() {
        guestsNumberButton.showArray = isEnabled
        guestsNumberButton.isUserInteractionEnabled = isEnabled
    }
    
    private func updateView(by type: GuestNumberViewType) {
        switch type {
        case .canRsvpWithGuests:
            isEnabled = true
        case .canRsvpWithoutGuests:
            isEnabled = false
        }
    }
    
    @objc private func didTapNumberPickerButton() {
        guestsPickerView?.isHidden = false
    }
    
    @objc private func didTapDoneButton() {
        guard let pickerView = guestsPickerView else { return }
        guestsPickerView?.isHidden = true
        guestsNumberButton.additionalGuests = pickerView.showingGuestsNumber
    }
}
