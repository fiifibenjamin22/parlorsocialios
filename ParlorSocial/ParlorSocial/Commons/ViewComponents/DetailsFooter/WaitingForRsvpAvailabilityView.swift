//
//  WaitingForRsvpAvailabilityView.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 23/07/2019.
//

import UIKit

enum WaitingForRsvpAvailabilityViewType {
    case stayTuned
    case rsvpIsClosed

    static func getType(
        basedOn bottomBarType: ActivationDetailsBottomBarType) -> Self {
        switch bottomBarType {
        case .stayTuned: return stayTuned
        case .rsvpIsClosed: return rsvpIsClosed
        default:
            fatalError("There is bad ActivationDetailsBottomBarType for creating WaitingForRsvpAvailabilityViewType")
        }
    }
}

class WaitingForRsvpAvailabilityView: UIView {
    
    // MARK: - Views
    
    lazy var stayTunedImageView: UIImageView = {
        return UIImageView().manualLayoutable()
    }()
    
    lazy var stayTunedLabel: ParlorLabel = {
        return ParlorLabel.styled(
            withTextColor: .appTextBright,
            withFont: UIFont.custom(ofSize: Constants.FontSize.hintSize, font: UIFont.Roboto.medium),
            alignment: .left,
            numberOfLines: 1)
            .manualLayoutable().apply {
                $0.adjustsFontSizeToFitWidth = true
        }
    }()
    
    lazy var horizontalStackView: UIStackView = {
        return UIStackView(
            axis: .horizontal,
            with: [stayTunedImageView, stayTunedLabel],
            spacing: 7,
            alignment: .center,
            distribution: .fill)
    }()
    
    // MARK: - Properties
    
    var type: WaitingForRsvpAvailabilityViewType = .rsvpIsClosed {
        didSet {
            updateView(by: type)
        }
    }
    
    init() {
        super.init(frame: .zero)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        backgroundColor = .white
        layoutViews()
    }
    
    private func layoutViews() {
        addSubview(horizontalStackView)
        
        stayTunedImageView.apply {
            $0.setContentHuggingPriority(.required, for: .horizontal)
            $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        }
        
        horizontalStackView.apply {
            $0.edgesToParent(insets: UIEdgeInsets.margins(left: 23, bottom: UIDevice.current.hasNotch ? 10 : 0, right: 23))
        }
    }

    private func updateView(by type: WaitingForRsvpAvailabilityViewType) {
        switch type {
        case .stayTuned:
            stayTunedImageView.image = Icons.ActivationDetails.stayTuned
            stayTunedLabel.text = Strings.ActivationDetails.stayTuned.localized
        case .rsvpIsClosed:
            stayTunedImageView.image = Icons.ActivationDetails.closedRsvp
            stayTunedLabel.text = Strings.ActivationDetails.rsvpClosed.localized
        }
    }
}
