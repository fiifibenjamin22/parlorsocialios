//
//  NotifyMeBottomBarView.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 29/11/2019.
//  Copyright © 2019 KISSdigital. All rights reserved.
//

import UIKit

class NotifyMeBottomBarView: UIView {
    
    // MARK: - Views
    lazy var notifyMeButton: TrackableButton = {
        return TrackableButton().manualLayoutable().apply {
            $0.setTitleColor(.appText, for: .normal)
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.small, font: UIFont.Roboto.medium)
            $0.backgroundColor = .appYellow
            $0.setTitle(Strings.Activation.notifyMe.localized.uppercased(), for: .normal)
            $0.titleLabel?.adjustsFontSizeToFitWidth = true
        }
    }()

    lazy var rsvpBeginsAtLabel: ParlorLabel = {
        return ParlorLabel.styled(
            withTextColor: .appTextBright,
            withFont: UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.medium),
            alignment: .left,
            numberOfLines: 1)
            .manualLayoutable()
    }()
    
    lazy var bottomSpaceLeftView: UIView = {
        return UIView().manualLayoutable().apply {
            $0.backgroundColor = .white
        }
    }()
    
    lazy var bottomSpaceButton: UIView = {
        return UIView().manualLayoutable().apply {
            $0.backgroundColor = .appYellow
        }
    }()
    
    // MARK: - Properties
    var dateText: String? {
        didSet {
            rsvpBeginsAtLabel.text = dateText
        }
    }
    
    init() {
        super.init(frame: .zero)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Initialization
    private func initialize() {
        setupHierarchy()
        setupViews()
        setupProperties()
    }
    
    private func setupHierarchy() {
        addSubviews([rsvpBeginsAtLabel, notifyMeButton, bottomSpaceLeftView, bottomSpaceButton])
    }
    
    private func setupViews() {
        rsvpBeginsAtLabel.apply {
            $0.edgesToParent(anchors: [.top, .leading], insets: .margins(left: 20))
            $0.bottomAnchor.equal(to: bottomSpaceLeftView.topAnchor)
            $0.trailingAnchor.equal(to: notifyMeButton.leadingAnchor, constant: -20)
        }
        
        notifyMeButton.apply {
            $0.edgesToParent(anchors: [.top, .trailing])
            $0.bottomAnchor.equal(to: bottomSpaceButton.topAnchor)
            $0.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.377).activate()
        }
        
        bottomSpaceLeftView.apply {
            $0.edgesToParent(anchors: [.leading, .bottom, .trailing])
            $0.heightAnchor.equalTo(constant: UIDevice.current.hasNotch ? 10 : 0)
        }
        
        bottomSpaceButton.apply {
            $0.edgesToParent(anchors: [.bottom, .trailing])
            $0.heightAnchor.equalTo(constant: UIDevice.current.hasNotch ? 10 : 0)
            $0.widthAnchor.equal(to: notifyMeButton.widthAnchor)
        }
        
    }
    
    private func setupProperties() {
        backgroundColor = .white
    }
}
