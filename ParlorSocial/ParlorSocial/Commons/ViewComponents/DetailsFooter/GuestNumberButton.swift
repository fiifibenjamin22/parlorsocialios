//
//  GuestNumberButton.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 29/04/2019.
//

import UIKit

class GuestNumberButton: TrackableButton {

    init() {
        super.init(frame: .zero)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var additionalGuests: Int = 0 {
        didSet {
            handleAadditionalGuestsChanged()
        }
    }
    
    var showArray: Bool = true {
        didSet {
            setImage(showArray ? Icons.GuestsForm.arrow : nil, for: .normal)
        }
    }
    
    private func initialize() {
        showArray = true
        setTitle(Strings.GuestForm.justMe.localized, for: .normal)
        backgroundColor = .white
        titleLabel?.font = UIFont.custom(ofSize: 14, font: UIFont.Roboto.medium)
        setTitleColor(UIColor.appTextBright, for: .normal)
        contentHorizontalAlignment = .left
        semanticContentAttribute = .forceRightToLeft
        titleEdgeInsets = UIEdgeInsets(top: 0, left: 28, bottom: 0, right: 0)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if imageView != nil {
            imageEdgeInsets = UIEdgeInsets(top: 0, left: self.bounds.width - titleLabel!.bounds.width - CGFloat(45), bottom: 0, right: 0)
        }
    }
    
    private func handleAadditionalGuestsChanged() {
        if additionalGuests > 0 {
            setTitle(String(format: Strings.GuestForm.moreGuestsPlaceholder.localized, additionalGuests), for: .normal)
        } else {
            setTitle(Strings.GuestForm.justMe.localized, for: .normal)
        }
    }

}
