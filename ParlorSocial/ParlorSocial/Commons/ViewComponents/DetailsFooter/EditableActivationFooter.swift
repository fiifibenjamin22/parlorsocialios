//
//  EditableActivationFooter.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 02/05/2019.
//

import UIKit

enum EditableFooterState {
    case rsvp(withStatus: RsvpStatus)
    case edited
}

extension EditableFooterState {
    var text: String {
        switch self {
        case .edited:
            return Strings.ActivationDetails.updated.localized
        case .rsvp(let status):
            return status.labelText
        }
    }
}

class EditableActivationFooter: UIView {

    lazy var editButton: TrackableButton = {
        return TrackableButton().manualLayoutable().apply {
            $0.setTitleColor(.appText, for: .normal)
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.medium)
            $0.setTitle(Strings.edit.localized.uppercased(), for: .normal)
            $0.applyTouchAnimation()
            $0.trackingId = AnalyticEventClickableElementName.ActivationDetails.editRsvp.rawValue
        }
    }()
    
    lazy var iconImage: UIImageView = {
        return UIImageView(image: Icons.Common.checkedWhite).apply { $0.contentMode = .scaleAspectFit }
    }()
    
    lazy var infoLabel: ParlorLabel = {
        return ParlorLabel.styled(withTextColor: .appTextBright, withFont: UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.medium), alignment: .left, numberOfLines: 1)
    }()
    
    lazy var labelStack: UIStackView = {
        return UIStackView(axis: .horizontal, with: [iconImage, infoLabel], spacing: 7, alignment: .center, distribution: .equalSpacing)
    }()
    
    lazy var seperatorView: UIView = {
        return UIView().manualLayoutable().apply { $0.backgroundColor = UIColor.appSeparator }
    }()
    
    var state: EditableFooterState? {
        didSet {
            updateStateData()
        }
    }
    
    init() {
        super.init(frame: .zero)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        backgroundColor = .white
        
        addSubviews([labelStack, seperatorView, editButton])
        
        labelStack.leadingAnchor.equal(to: leadingAnchor, constant: 23)
        labelStack.centerYAnchor.equal(to: centerYAnchor, constant: UIDevice.current.hasNotch ? -5 : 0)
        
        editButton.edgesToParent(anchors: [.top, .bottom, .trailing], insets: UIEdgeInsets.margins(bottom: UIDevice.current.hasNotch ? 10 : 0))
        editButton.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.377).activate()
        
        seperatorView.edgesToParent(anchors: [.top, .bottom])
        seperatorView.widthAnchor.equalTo(constant: 1)
        seperatorView.trailingAnchor.equal(to: editButton.leadingAnchor)
        
        iconImage.widthAnchor.equalTo(constant: 15)
        iconImage.heightAnchor.equalTo(constant: 15)
        
        updateStateData()
    }
    
    private func updateStateData() {
        guard let state = state else { return }
        infoLabel.text = state.text
    }

}

fileprivate extension RsvpStatus {
    
    var labelText: String {
        switch self {
        case .guestList:
            return Strings.ActivationDetails.confirmed.localized
        case .waitList, .transferableToGuestList:
            return Strings.ActivationDetails.onWaitlist.localized
        default:
            return ""
        }
    }
    
}
