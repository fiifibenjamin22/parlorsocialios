//
//  GuestPickerView.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 29/04/2019.
//

import UIKit

protocol GuestPickerViewDelegate: class {
    func guestsNumberValueChanged(value: Int)
}

class GuestPickerView: UIView {
    
    // MARK: - Views

    lazy var guestLabel: ParlorLabel = {
       return ParlorLabel.styled(withTextColor: .black, withFont: UIFont.custom(ofSize: 12, font: UIFont.Roboto.bold), alignment: .center, numberOfLines: 1, letterSpacing: Constants.LetterSpacing.small)
    }().manualLayoutable()
    
    lazy var guestsCounter: ParlorLabel = {
        return ParlorLabel.styled(withTextColor: .appText, withFont: UIFont.custom(ofSize: 83, font: UIFont.Editor.bold), alignment: .center, numberOfLines: 1)
    }().manualLayoutable()
    
    lazy var minusButton: TrackableButton = {
        return TrackableButton().apply {
            $0.setImage(Icons.ActivationDetails.minus, for: .normal)
            $0.contentEdgeInsets = UIEdgeInsets(top: 40, left: 40, bottom: 40, right: 40)
            $0.trackingId = AnalyticEventClickableElementName.ActivationDetails.guestsPickerMinus.rawValue
        }.manualLayoutable()
    }()
    
    lazy var plusButton: TrackableButton = {
        return TrackableButton().apply {
            $0.setImage(Icons.ActivationDetails.plus, for: .normal)
            $0.contentEdgeInsets = UIEdgeInsets(top: 40, left: 40, bottom: 40, right: 40)
            $0.trackingId = AnalyticEventClickableElementName.ActivationDetails.guestsPickerPlus.rawValue
        }.manualLayoutable()
    }()
    
    lazy var doneButton: TrackableButton = {
        return TrackableButton().apply {
            $0.backgroundColor = .white
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.medium)
            $0.setTitleColor(UIColor.appText, for: .normal)
            $0.setTitle(Strings.done.localized.uppercased(), for: .normal)
            $0.trackingId = AnalyticEventClickableElementName.ActivationDetails.guestsPickerDone.rawValue
        }.manualLayoutable()
    }()
    
    // MARK: - Properties
    
    weak var delegate: GuestPickerViewDelegate?
    
    var showingGuestsNumber: Int = 0 {
        didSet {
            updateCounterLabel()
            updateButtonsEnabled()
            delegate?.guestsNumberValueChanged(value: showingGuestsNumber)
        }
    }
    
    var maxPossibleGuests: Int = Int.max {
        didSet {
            updateButtonsEnabled()
        }
    }
    
    init() {
        super.init(frame: .zero)
        initialize()
        updateButtonsEnabled()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        backgroundColor = .white
        updateCounterLabel()
        guestLabel.text = Strings.GuestForm.guestHowMany.localized.uppercased()
        addSubviews([guestLabel, guestsCounter, minusButton, plusButton, doneButton])
        
        guestLabel.topAnchor.equal(to: self.topAnchor, constant: 30)
        guestLabel.centerXAnchor.equal(to: self.centerXAnchor)
        
        guestsCounter.centerXAnchor.equal(to: self.centerXAnchor)
        guestsCounter.centerYAnchor.equal(to: self.centerYAnchor)
        
        minusButton.centerYAnchor.equal(to: guestsCounter.centerYAnchor)
        minusButton.leadingAnchor.equal(to: self.leadingAnchor)
        
        plusButton.centerYAnchor.equal(to: guestsCounter.centerYAnchor)
        plusButton.trailingAnchor.equal(to: self.trailingAnchor)
        
        doneButton.centerXAnchor.equal(to: self.centerXAnchor)
        doneButton.bottomAnchor.equal(to: self.bottomAnchor, constant: -30)
        
        minusButton.addTarget(self, action: #selector(didTapMinusButton), for: .touchUpInside)
        plusButton.addTarget(self, action: #selector(didTapPlusButton), for: .touchUpInside)
    }
    
    private func updateCounterLabel() {
        guestsCounter.text = String(showingGuestsNumber)
    }
    
    private func updateButtonsEnabled() {
        minusButton.setEnabledWithAlpha(showingGuestsNumber > 0)
        plusButton.setEnabledWithAlpha(showingGuestsNumber < maxPossibleGuests)
    }
    
    @objc private func didTapPlusButton() {
        if showingGuestsNumber < maxPossibleGuests {
            showingGuestsNumber = showingGuestsNumber + 1
        }
    }
    
    @objc private func didTapMinusButton() {
        if showingGuestsNumber > 0 {
            showingGuestsNumber = showingGuestsNumber - 1
        }
    }
    
}
