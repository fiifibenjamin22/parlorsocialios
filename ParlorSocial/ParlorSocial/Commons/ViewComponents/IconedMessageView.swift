//
//  IconedMessageView.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 04/04/2019.
//

import UIKit

class IconedMessageView: UIView {
    
    let titleTopMargin: CGFloat = 34
    let messageTopMargin: CGFloat = 17

    let roundedIcon =  RoundedIconView()
    let titleLabel = ParlorLabel.withMainMessageStyle()
    let messageLabel = ParlorLabel.withSubMessageStyle()
    
    convenience init(icon: UIImage?, title: String?, message: String?) {
        self.init(frame: .zero)
        roundedIcon.iconImage = icon
        titleLabel.text = title
        messageLabel.text = message
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        let subviews = [roundedIcon, titleLabel, messageLabel]
        subviews.forEach { $0.manualLayoutable() }
        roundedIcon.backgroundColor = UIColor.appBackground
        addSubviews(subviews)
        setupLayout()
    }
    
    private func setupLayout() {
        roundedIcon.topAnchor.equal(to: self.topAnchor)
        roundedIcon.centerXAnchor.equal(to: self.centerXAnchor)

        titleLabel.edgesToParent(anchors: [.leading, .trailing])
        titleLabel.topAnchor.equal(to: roundedIcon.bottomAnchor, constant: titleTopMargin)
        
        messageLabel.centerXAnchor.equal(to: self.centerXAnchor)
        messageLabel.topAnchor.equal(to: titleLabel.bottomAnchor, constant: messageTopMargin)
        messageLabel.edgesToParent(anchors: [.leading, .trailing, .bottom])
    }

}
