//
//  SelectableButton.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 23/05/2019.
//

import UIKit

class SelectableButton: TrackableButton {
    
    // MARK: - Views
    
    private(set) lazy var progressBar: UIActivityIndicatorView = {
        return UIActivityIndicatorView().manualLayoutable()
    }()
    
    init() {
        super.init(frame: .zero)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        applyTouchAnimation()
        setupProgressIndicator()
    }
    
    var selectedFont: UIFont? {
        didSet {
            updateFont()
        }
    }
    
    var unselectedFont: UIFont? {
        didSet {
            updateFont()
        }
    }
    
    var selectedColor: UIColor? {
        didSet {
            updateFont()
        }
    }
    
    var unselectedColor: UIColor? {
        didSet {
            updateFont()
        }
    }
    
    override var isSelected: Bool {
        didSet {
            updateFont()
        }
    }
    
    private func updateFont() {
        guard let usedFont = isSelected ? selectedFont : unselectedFont,
            let usedFontColor = isSelected ? selectedColor : unselectedColor else { return }
        titleLabel?.font = usedFont
        setTitleColor(usedFontColor, for: .normal)
    }
    
    private func setupProgressIndicator() {
        addSubview(progressBar)
        progressBar.apply {
            $0.edgesToParent(anchors: [.top, .bottom])
            $0.centerXAnchor.equal(to: self.centerXAnchor)
        }
    }
    
    func progressBar(isHidden: Bool) {
        isHidden ? progressBar.stopAnimating() : progressBar.startAnimating()
    }

}
