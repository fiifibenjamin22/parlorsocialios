//  TrackableButton.swift
//  ParlorSocialClub
//
//  Created on 13/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

class TrackableButton: UIButton {
    
    // MARK: - Properties
    
    var trackingId: String?
    
    // MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        addButtonClickTrackingAction()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setup
    
    private func addButtonClickTrackingAction() {
        addTarget(self, action: #selector(saveButtonClickEvent), for: .touchUpInside)
    }
    
    // MARK: - Actions
    
    @objc private func saveButtonClickEvent() {
        guard let name = trackingId else { return }
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: name)
    }
}
