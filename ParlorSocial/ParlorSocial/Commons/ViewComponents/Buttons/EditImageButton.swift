//
//  EditImageButton.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 27/05/2019.
//

import UIKit

class EditImageButton: UIView {
    
    // MARK: - Views

    lazy var imageView: UIImageView = {
        return UIImageView(image: Icons.EditProfile.editPhoto).manualLayoutable().apply {
            $0.contentMode = .scaleAspectFill
        }
    }()
    
    lazy var editImageLabel: ParlorLabel = {
        return ParlorLabel.styled(withTextColor: .appGreyLight,
                                  withFont: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.regular),
                                  alignment: .center, numberOfLines: 1).apply {
            $0.text = Strings.edit.localized
        }
    }()
    
    // MARK: - Initalization
    
    init() {
        super.init(frame: .zero)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        applyTouchAnimation()
        addSubviews([imageView, editImageLabel])
        setupAutoLayout()
    }
    
    private func setupAutoLayout() {
        imageView.apply {
            $0.edgesToParent(anchors: [.leading, .trailing, .top])
            $0.heightAnchor.equalTo(constant: 84)
            $0.widthAnchor.equalTo(constant: 84)
            $0.layer.cornerRadius = 42
            $0.clipsToBounds = true
            $0.edgesToParent(anchors: [.leading, .trailing, .top])
        }
        
        editImageLabel.apply {
            $0.topAnchor.equal(to: imageView.bottomAnchor, constant: 13)
            $0.edgesToParent(anchors: [.bottom])
            $0.centerXAnchor.equal(to: self.centerXAnchor)
        }
    }

}
