//
//  StripeButton.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 25/07/2019.
//

import UIKit

class StripeButton: UIButton {
    
    // MARK: - Properties
    private lazy var stripeURL: URL = {
        if let url =  URL(string: "https://stripe.com/") {
            return url
        }
        fatalError()
    }()

    // MARK: - Initalization
    init() {
        super.init(frame: .zero)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        applyTouchAnimation()
        setImage(Icons.Common.poweredByStripe, for: .normal)
        addTarget(self, action: #selector(didTapStripeButton), for: .touchUpInside)
    }

    @objc private func didTapStripeButton() {
        UIApplication.shared.open(stripeURL)
    }
}
