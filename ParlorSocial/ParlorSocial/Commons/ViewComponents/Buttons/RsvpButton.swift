//
//  RsvpButton.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 27/03/2019.
//

import UIKit

final class RsvpButton: UIButton {

    var responded: Bool = true {
        didSet {
            responded ? setupForResponded() : setupForRsvpAvailable()
        }
    }

    var width: CGFloat {
        set { widthConstraint.constant = newValue }
        get { return widthConstraint.constant }
    }

    var height: CGFloat {
        set { heightConstraint.constant = newValue }
        get { return heightConstraint.constant }
    }

    private var widthConstraint: NSLayoutConstraint!
    private var heightConstraint: NSLayoutConstraint!

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupBasicProperties()
        setupForRsvpAvailable()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height / 2
    }
}

extension RsvpButton {

    private func setupBasicProperties() {
        manualLayoutable()
        heightConstraint = heightAnchor.equalTo(constant: 50)
        widthConstraint = widthAnchor.equalTo(constant: 164)

        layer.borderColor = UIColor.rsvpBorder.cgColor
        setTitleColor(.white, for: .normal)
        titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Editor.medium)
    }

    private func setupForRsvpAvailable() {
        layer.borderWidth = 1
        setImage(nil, for: .normal)
        setAttributedTitle(NSAttributedString(
            string: Strings.Activation.rsvp.localized,
            attributes: [.kern: Constants.LetterSpacing.medium, .foregroundColor: UIColor.white]), for: .normal)

        titleEdgeInsets = .zero
        imageEdgeInsets = .zero
    }

    private func setupForResponded() {
        layer.borderWidth = 0
        setImage(Icons.AllActivations.check, for: .normal)
        setAttributedTitle(NSAttributedString(
            string: Strings.Activation.rsvpd.localized,
            attributes: [.kern: Constants.LetterSpacing.medium, .foregroundColor: UIColor.white]), for: .normal)

        let spacing: CGFloat = Constants.Margin.tiny / 2
        titleEdgeInsets = UIEdgeInsets(top: 0, left: spacing, bottom: 0, right: -spacing)
        imageEdgeInsets = UIEdgeInsets(top: 0, left: -spacing + 2, bottom: 0, right: spacing)
    }
}
