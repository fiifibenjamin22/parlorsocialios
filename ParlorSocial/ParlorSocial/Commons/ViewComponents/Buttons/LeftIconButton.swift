//
//  LeftIconButton.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 22/03/2019.
//

import UIKit

class LeftIconButton: UIButton {
    
    private let buttonPadding: CGFloat = Constants.Margin.standard
    
    private let spacing: CGFloat = 4
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        manualLayoutable()
        applyTouchAnimation()
        
        contentHorizontalAlignment = .left
        titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.body, font: UIFont.Roboto.regular)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentEdgeInsets = UIEdgeInsets(padding: buttonPadding).insetBy(dx: spacing, dy: 0)
        titleEdgeInsets = UIEdgeInsets(top: 0, left: spacing, bottom: 0, right: -spacing)
        imageEdgeInsets = UIEdgeInsets(top: 0, left: -spacing, bottom: 0, right: spacing)
    }
}
