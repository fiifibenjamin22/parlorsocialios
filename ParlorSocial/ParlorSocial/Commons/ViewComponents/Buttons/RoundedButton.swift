//
//  RoundedButton.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 29/03/2019.
//

import UIKit

class RoundedButton: TrackableButton {
    
    override var isEnabled: Bool {
        get {
            return super.isEnabled
        }
        set {
            super.isEnabled = newValue
            self.alpha = newValue ? 1 : 0.7
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        self.clipsToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = min(self.bounds.height, self.bounds.width) / 2
    }
    
}

extension RoundedButton {
    
    static func withBackgroundStyle() -> RoundedButton {
        return RoundedButton().apply {
            $0.backgroundColor = UIColor(red: 255 / 255, green: 203 / 255, blue: 42 / 255, alpha: 1.0)//.white
            $0.applyTouchAnimation()
            $0.layer.borderWidth = 1
            $0.layer.borderColor = UIColor.textVeryLight.cgColor
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium)
            $0.setTitleColor(.appText, for: .normal)
            $0.heightAnchor.equalTo(constant: Constants.bigButtonHeight)
        }
    }
    
}
