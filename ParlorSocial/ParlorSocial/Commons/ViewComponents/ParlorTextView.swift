//  ParlorTextView.swift
//  ParlorSocialClub
//
//  Created on 27/04/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

class ParlorTextView: UITextView {
    
    // MARK: - Properties
    
    var letterSpacing: CGFloat = 0 {
        didSet {
             updateTextAttributes()
        }
    }
    
    var lineHeight: CGFloat = 0 {
        didSet {
            updateTextAttributes()
        }
    }
    
    var linkHandler: TextViewLinkHandler = DefaultTextViewLinkHandler()
    var linkClickTrackingId: String?
    
    override var text: String? {
        didSet {
             updateTextAttributes()
        }
    }
    
    override var attributedText: NSAttributedString? {
        didSet {
            updateTextAttributes()
        }
    }
    
    // MARK: - Init
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // If text dont't have a link it will allow to recognize gesture on view under text view
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        var tapLocation = point
        tapLocation.x -= textContainerInset.left
        tapLocation.y -= textContainerInset.top
        
        let tappedCharacterIndex = layoutManager.characterIndex(for: tapLocation, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        if tappedCharacterIndex < textStorage.length {
            if textStorage.attribute(.link, at: tappedCharacterIndex, effectiveRange: nil) != nil {
                return self
            }
        }
        
        return nil
    }
    
    // MARK: - Setup
    
    private func setup() {
        isScrollEnabled = false
        backgroundColor = .clear
        dataDetectorTypes = [.link]
        isEditable = false
        tintColor = .appGreen
        textContainerInset = .zero
        textContainer.lineFragmentPadding = 0
        linkTextAttributes = [.underlineStyle: 1]
        delegate = self
    }
    
    private func updateTextAttributes() {
        var attributes: [NSAttributedString.Key: Any] = [:]
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.minimumLineHeight = lineHeight
        paragraphStyle.lineBreakMode = .byWordWrapping
        paragraphStyle.alignment = self.textAlignment
        attributes[.paragraphStyle] = paragraphStyle
        
        if let attributedText = self.attributedText {
            let mutable = NSMutableAttributedString(attributedString: attributedText)
            mutable.addAttributes(attributes, range: NSRange(location: 0, length: mutable.length))
            super.attributedText = mutable
        } else {
            attributes[.kern] = letterSpacing
            super.attributedText = NSAttributedString(string: self.text ?? "", attributes: attributes)
        }
    }
}

// MARK: - UITextViewDelegate

extension ParlorTextView: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        saveLinkClickAnalyticEvent()
        linkHandler.openUrl(URL)
        return false
    }
    
    private func saveLinkClickAnalyticEvent() {
        guard let trackingId = linkClickTrackingId else { return }
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: trackingId)
    }
}
