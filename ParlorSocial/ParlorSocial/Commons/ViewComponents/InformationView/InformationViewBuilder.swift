//
//  InformationViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 04.03.2019.
//

import UIKit

final class InformationViewBuilder {

    private unowned let view: InformationView

    init(view: InformationView) {
        self.view = view
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

extension InformationViewBuilder {

    private func setupProperties() {
        view.autoresizingMask = .flexibleHeight
        view.informationLabel.setContentCompressionResistancePriority(.required, for: .horizontal)
    }

    private func setupHierarchy() {
        view.contentStackView.addArrangedSubviews([view.iconContainer, view.informationLabel])
        view.addSubview(view.contentStackView)
        view.iconContainer.addSubview(view.informationIcon)
    }

    private func setupAutoLayout() {
        view.contentStackView.edges(equalTo: view)

        view.informationIcon.edgesToParent(anchors: [.leading, .trailing])
        view.informationIcon.bottomAnchor.lessThanOrEqual(to: view.iconContainer.bottomAnchor)

        view.informationIcon.heightAnchor.equal(to: view.informationIcon.widthAnchor)
    }

    func setupIconCenterYConstraint() -> NSLayoutConstraint {
        return view.informationIcon.centerYAnchor.equal(to: view.iconContainer.centerYAnchor)
    }
    
    func setupIconWidthConstraint() -> NSLayoutConstraint {
        return view.informationIcon.widthAnchor.equalTo(constant: Constants.InforamtionView.iconSize)

    }

    func setupIconTopConstraint(clipToTop: Bool) -> NSLayoutConstraint {
        if clipToTop {
            return view.informationIcon.topAnchor.equal(to: view.iconContainer.topAnchor)
        } else {
            return view.informationIcon.topAnchor.greaterThanOrEqual(to: view.iconContainer.topAnchor)
        }
    }

}

extension InformationViewBuilder {

    func buildView() -> UIView {
        let view = UIView().manualLayoutable()

        return view
    }

    func buildIconImageView() -> UIImageView {
        let imageView = UIImageView().manualLayoutable()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true

        return imageView
    }

    func buildInformationLabel() -> ParlorLabel {
        return ParlorLabel.styled(
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.small,
                font: UIFont.Roboto.regular),
            alignment: .left)
    }

    func buildStackView() -> UIStackView {
        let stackView = UIStackView(axis: .horizontal, spacing: 5)

        return stackView
    }
}

private extension Constants {

    enum InforamtionView {
        static let iconSize: CGFloat = 16
    }

}
