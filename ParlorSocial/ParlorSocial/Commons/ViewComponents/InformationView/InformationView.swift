//
//  InformationView.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 04.03.2019.
//

import UIKit

final class InformationView: UIView {

    private(set) var informationIcon: UIImageView!
    private(set) var informationLabel: ParlorLabel!
    private(set) var iconContainer: UIView!
    private(set) var contentStackView: UIStackView!
    private(set) var iconTopConstraint: NSLayoutConstraint!
    private(set) var iconCenterYConstraint: NSLayoutConstraint!
    private(set) var iconWidthConstraint: NSLayoutConstraint!

    var icon: UIImage? {
        set {
            informationIcon.image = newValue
            iconContainer.isHidden = newValue == nil
        }
        get { return informationIcon.image }
    }

    var title: String? {
        set {
            informationLabel.text = newValue
            informationLabel.isHidden = newValue == nil
        }
        get { return informationLabel.text }
    }

    var font: UIFont {
        set { informationLabel.font = newValue }
        get { return informationLabel.font }
    }

    var clipIconToTop: Bool = true {
        didSet {
            let builder = InformationViewBuilder(view: self)

            iconTopConstraint.secondItem?.removeConstraint(iconTopConstraint)

            iconTopConstraint = builder.setupIconTopConstraint(clipToTop: clipIconToTop)
            iconCenterYConstraint.isActive = !clipIconToTop
        }
    }

    var iconSize: CGFloat {
        set { iconWidthConstraint.constant = newValue }
        get { return iconWidthConstraint.constant }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override var tintColor: UIColor! {
        didSet {
            informationLabel.textColor = tintColor
            informationIcon.tintColor = tintColor
        }
    }
}

// MARK: - Private methods
extension InformationView {

    private func initialize() {
        manualLayoutable()
        setupViews()
    }

    private func setupViews() {
        let builder = InformationViewBuilder(view: self)

        informationIcon = builder.buildIconImageView()
        informationLabel = builder.buildInformationLabel()
        contentStackView = builder.buildStackView()
        iconContainer = builder.buildView()

        builder.setupViews()
        iconTopConstraint = builder.setupIconTopConstraint(clipToTop: clipIconToTop)
        iconCenterYConstraint = builder.setupIconCenterYConstraint()
        iconWidthConstraint = builder.setupIconWidthConstraint()

        clipIconToTop = true
    }
}
