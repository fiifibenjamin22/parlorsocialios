//
//  ParlorTextField.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 29/03/2019.
//

import UIKit

class LoginTextField: ParlorTextField {
    
    private let passwordIconImage: UIImageView = {
        return UIImageView(image: Icons.Login.passwordEye).manualLayoutable()
    }()
    
    var isPasswordField = false {
        didSet {
            adjustToPasswordFieldIfNeeded()
        }
    }
    
    override var padding: UIEdgeInsets {
        didSet {
            adjustToPasswordFieldIfNeeded()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        self.font = UIFont.custom(ofSize: Constants.FontSize.body, font: UIFont.Roboto.light)
        self.textColor = UIColor.appTextWhite
        self.passwordIconImage.isUserInteractionEnabled = true
        self.passwordIconImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapRightIcon)))
    }
    
    private func adjustToPasswordFieldIfNeeded() {
        self.isSecureTextEntry = isPasswordField
        self.rightViewMode = isPasswordField ? .always : .never
        self.rightView = isPasswordField ? prepareRightView() : nil
    }

    @objc private func didTapRightIcon() {
        togglePasswordMode()
    }
    
    private func togglePasswordMode() {
        guard isPasswordField else { return }
        self.isSecureTextEntry = !isSecureTextEntry
        passwordIconImage.image = isSecureTextEntry ? Icons.Login.passwordEye : Icons.Login.passwordEyeClosed
    }
    
    private func prepareRightView() -> UIView {
        passwordIconImage.removeFromSuperview()
        let iconSuperview = UIView().manualLayoutable().apply {
            $0.addSubview(passwordIconImage)
        }
        passwordIconImage.contentMode = .right
        passwordIconImage.edgesToParent(insets: .margins(right: self.padding.right))
        passwordIconImage.heightAnchor.equalTo(constant: 40)
        passwordIconImage.widthAnchor.equalTo(constant: 40)
        return iconSuperview
    }
    
}
