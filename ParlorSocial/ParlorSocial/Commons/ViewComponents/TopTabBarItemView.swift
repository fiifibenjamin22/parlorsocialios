//
//  TopTabBarItemView.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 07/05/2019.
//

import UIKit

enum BottomLineStyle {
    case automatic
    case fixedWidthMultiplier(multiplier: CGFloat)
}

protocol TopTabBarItemViewDelegate: class {
    func topTabBarItemView(_ topTabBarItemView: TopTabBarItemView, selected: Bool)
}

class TopTabBarItemView: UIControl {
    var title: String? {
        didSet {
            titleLabel.text = title
        }
    }

    override var isSelected: Bool {
        didSet {
            selectedChange()
        }
    }

    weak var delegate: TopTabBarItemViewDelegate?

    private(set) var titleLabel: UILabel!
    private(set) var bottomLine: UIView!

    private let bottomLineStyle: BottomLineStyle
    
    // MARK: - Initialization
    init(bottomLineStyle: BottomLineStyle) {
        self.bottomLineStyle = bottomLineStyle

        super.init(frame: .zero)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private functions
    private func initialize() {
        self.backgroundColor = UIColor.white
        
        self.titleLabel = ParlorLabel.styled(
            withTextColor: .closeTintColor,
            withFont: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium),
            letterSpacing: Constants.LetterSpacing.small).manualLayoutable()
        
        self.bottomLine = UIView().manualLayoutable().apply {
            $0.backgroundColor = UIColor.black
            $0.isHidden = true
        }
        
        layoutViews()
        
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapView)))
    }
    
    private func layoutViews() {
        self.addSubviews([titleLabel, bottomLine])
        
        self.titleLabel.apply {
            $0.centerXAnchor.equal(to: self.centerXAnchor)
            $0.centerYAnchor.equal(to: self.centerYAnchor)
        }
        
        self.bottomLine.apply {
            $0.edgesToParent(anchors: [.bottom])
            
            switch bottomLineStyle {
            case .automatic:
                $0.leadingAnchor.equal(to: titleLabel.leadingAnchor, constant: -Constants.TopTabBarItemView.bottomLineMargin)
                $0.trailingAnchor.equal(to: titleLabel.trailingAnchor, constant: Constants.TopTabBarItemView.bottomLineMargin)
            case .fixedWidthMultiplier(let multiplier):
                $0.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: multiplier).activate()
                $0.centerXAnchor.equal(to: self.centerXAnchor)
            }

            $0.heightAnchor.equalTo(constant: 2)
        }
    }
    
    @objc private func didTapView() {
        guard !isSelected else { return }
        isSelected = !isSelected
        delegate?.topTabBarItemView(self, selected: isSelected)
    }
    
    private func selectedChange() {
        titleLabel.textColor = isSelected ? UIColor.black : UIColor.closeTintColor
        bottomLine.isHidden = !isSelected
    }
}

// MARK: - Constants
fileprivate extension Constants {
    enum TopTabBarItemView {
        static let bottomLineMargin: CGFloat = 5
    }
}
