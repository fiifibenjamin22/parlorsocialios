//
//  HistoryStackItem.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 22/07/2019.
//

import UIKit

final class HistoryStackItem: UIView {
    
    // MARK: - Properties
    private(set) var searchData: String!
    var selectionCallback: ((String) -> Void)?
    
    // MARK: - Views
    private(set) lazy var mainLabel: UILabel = {
        return UILabel.styled(textColor: UIColor.appTextSemiLight,
                              withFont: UIFont.custom(
                                ofSize: Constants.FontSize.subbody,
                                font: UIFont.Roboto.regular),
                              alignment: .left,
                              numberOfLines: 1)
    }()
    
    // MARK: - Initialization
    init(searchData: String) {
        super.init(frame: .zero)
        initialize()
        setup(with: searchData)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - Public functions
extension HistoryStackItem {
    
    func setup(with data: String) {
        searchData = data
        mainLabel.text = data
    }
    
}

// MARK: - Private functions
extension HistoryStackItem {
    
    private func initialize() {
        self.setupTapGestureRecognizer(target: self, action: #selector(didTapView))
        layoutViews()
    }
    
    private func layoutViews() {
        self.addSubview(mainLabel)

        mainLabel.apply {
            $0.edgesToParent(insets: UIEdgeInsets.margins(
                left: Constants.Margin.standard,
                right: Constants.Margin.standard)
            )
        }
    }
    
    @objc private func didTapView() {
        selectionCallback?(searchData)
    }
}
