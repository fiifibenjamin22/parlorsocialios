//
//  SearchHistoryBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 22/07/2019.
//

import UIKit

final class SearchHistoryBuilder {
    
    private unowned let view: SearchHistoryView
    
    init(view: SearchHistoryView) {
        self.view = view
    }
    
    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

extension SearchHistoryBuilder {
    
    private func setupProperties() {
        view.backgroundColor = .white
        view.searchHistoryLabel.text = Strings.Search.searchHistory.localized.uppercased()
        view.bottomLine.backgroundColor = .textVeryLight
        view.clearHistoryButton.apply {
            $0.setTitle(Strings.Search.clearRecentSearches.localized, for: .normal)
            $0.trackingId = AnalyticEventClickableElementName.SearchScreen.clearHistory.rawValue
        }
    }
    
    private func setupHierarchy() {
        view.apply {
            $0.addSubviews([$0.scrollView])
            
            $0.scrollView.addSubview($0.contentScrollView)
            
            $0.contentScrollView.addSubviews([$0.searchHistoryLabel, $0.verticalStackView, $0.bottomLine, $0.clearHistoryButton])
        }
    }
    
    private func setupAutoLayout() {
        view.scrollView.apply {
            $0.edgesToParent()
        }
        
        view.contentScrollView.apply {
            $0.edgesToParent()
            $0.widthAnchor.equal(to: view.scrollView.widthAnchor)
        }
        
        view.searchHistoryLabel.apply {
            $0.edgesToParent(
                anchors: [.top, .leading, .trailing],
                insets: UIEdgeInsets.margins(top: 27, left: Constants.Margin.standard, right: Constants.Margin.standard)
            )
        }
        
        view.verticalStackView.apply {
            $0.edgesToParent(anchors: [.leading, .trailing])
            $0.topAnchor.equal(to: view.searchHistoryLabel.bottomAnchor, constant: 25)
        }
        
        view.bottomLine.apply {
            $0.heightAnchor.equalTo(constant: 1)
            $0.edgesToParent(
                anchors: [.leading, .trailing],
                insets: UIEdgeInsets.margins(left: Constants.Margin.standard, right: Constants.Margin.standard)
            )
            $0.topAnchor.equal(to: view.verticalStackView.bottomAnchor, constant: 25)
        }
        
        view.clearHistoryButton.apply {
            $0.topAnchor.equal(to: view.bottomLine.bottomAnchor, constant: 25)
            $0.heightAnchor.equalTo(constant: 16)
            $0.bottomAnchor.equal(to: view.contentScrollView.bottomAnchor, constant: -25)
            $0.centerXAnchor.equal(to: view.centerXAnchor)
        }
        
    }
    
}

extension SearchHistoryBuilder {
    func scrollView() -> UIScrollView {
        return UIScrollView().manualLayoutable()
    }
    
    func buildSearchHistoryLabel() -> ParlorLabel {
        return ParlorLabel.styled(
            withTextColor: .appTextBright,
            withFont: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium),
            alignment: .left,
            numberOfLines: 1)
            .manualLayoutable()
    }
    
    func buildVerticalStackView() -> UIStackView {
        return UIStackView(axis: .vertical, spacing: 17, alignment: .fill, distribution: .fill)
    }
    
    func buildView() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildClearHistoryButton() -> TrackableButton {
        return TrackableButton().manualLayoutable().apply {
            $0.applyTouchAnimation()
            $0.setTitleColor(.appTextBright, for: .normal)
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.medium)
        }
    }
}
