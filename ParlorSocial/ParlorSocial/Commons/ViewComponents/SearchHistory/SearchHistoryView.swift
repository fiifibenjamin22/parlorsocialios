//
//  SearchHistoryView.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 22/07/2019.
//

import UIKit

protocol SearchHistoryViewDelegate: class {
    func didTapItem(withQuery query: String)
}

final class SearchHistoryView: UIView {
    
    // MARK: - Views
    private(set) var scrollView: UIScrollView!
    private(set) var contentScrollView: UIView!
    private(set) var searchHistoryLabel: ParlorLabel!
    private(set) var verticalStackView: UIStackView!
    private(set) var bottomLine: UIView!
    private(set) var clearHistoryButton: TrackableButton!
    private(set) var stackViewItems: [HistoryStackItem]!
    
    // MARK: - Properties
    weak private var delegate: SearchHistoryViewDelegate?
    
    init(delegate: SearchHistoryViewDelegate?) {
        super.init(frame: .zero)
        initialize(delegate: delegate)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Public methods
extension SearchHistoryView {
    func setup(with items: [String]) {
        self.isHidden = items.isEmpty
        stackViewItems.removeAll()
        verticalStackView.removeArrangedSubviews()
        items.forEach {
            let historyStackItem = HistoryStackItem(searchData: $0)
            historyStackItem.selectionCallback = delegate?.didTapItem
            stackViewItems.append(historyStackItem)
            verticalStackView.addArrangedSubview(historyStackItem)
        }
    }
}

// MARK: - Private methods
extension SearchHistoryView {
    
    private func initialize(delegate: SearchHistoryViewDelegate?) {
        self.delegate = delegate
        manualLayoutable()
        setupViews()
    }
    
    private func setupViews() {
        let builder = SearchHistoryBuilder(view: self)
        scrollView = builder.scrollView()
        contentScrollView = builder.buildView()
        searchHistoryLabel = builder.buildSearchHistoryLabel()
        verticalStackView = builder.buildVerticalStackView()
        bottomLine = builder.buildView()
        clearHistoryButton = builder.buildClearHistoryButton()
        stackViewItems = []
        
        builder.setupViews()
        clearHistoryButton.addTarget(self, action: #selector(didTabClearRecentSearches), for: .touchUpInside)
    }
    
    @objc private func didTabClearRecentSearches() {
        Config.cleanSearchHistory()
        setup(with: [])
    }
    
}
