//
//  InterestGroupView.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 09/05/2019.
//

import UIKit

class InterestGroupView: UIView {

    lazy var interestImageView: UIImageView = {
        return UIImageView().manualLayoutable().apply {
            $0.contentMode = .scaleAspectFill
            $0.clipsToBounds = true
        }
    }()
    
    lazy var checkedIconImage: UIImageView = {
        return UIImageView(image: Icons.InterestGroupos.checkedIcon).manualLayoutable()
    }()
    
    lazy var interestLabel: ParlorLabel = {
        return ParlorLabel.styled(withTextColor: .appTextMediumBright, withFont: UIFont.custom(ofSize: Constants.FontSize.hintSize, font: UIFont.Roboto.medium), alignment: .center, numberOfLines: 0).apply {
            $0.adjustsFontSizeToFitWidth = true
            $0.minimumScaleFactor = 0.2
        }
    }()
    
    lazy var alphaView: UIView = {
        return UIView().manualLayoutable().apply {
            $0.backgroundColor = UIColor.appText.withAlphaComponent(0.3)
        }
    }()
    
    init() {
        super.init(frame: .zero)
        backgroundColor = .white
        setupAutoLayout()
        updateCheckedState()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var showingInterest: InterestGroup? {
        didSet {
            updateInterest()
        }
    }
    
    var isChecked: Bool = false {
        didSet {
            updateCheckedState()
        }
    }

    private func setupAutoLayout() {
        addSubviews([interestImageView, interestLabel, checkedIconImage])
        interestImageView.edgesToParent()
        interestLabel.edgesToParent(anchors: [.leading, .trailing], padding: Constants.Margin.tiny)
        interestLabel.centerYAnchor.equal(to: self.centerYAnchor)
        interestLabel.topAnchor.greaterThanOrEqual(to: topAnchor, constant: Constants.Margin.small)
        interestLabel.bottomAnchor.greaterThanOrEqual(to: bottomAnchor, constant: -Constants.Margin.small)
        checkedIconImage.edgesToParent(anchors: [.top, .trailing], padding: Constants.Margin.small)
    }
    
    private func updateInterest() {
        guard let showingInterest = showingInterest else { return }
        interestLabel.text = showingInterest.name
        interestLabel.font = showingInterest.isAddMoreData ?
            .custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium) :
            .custom(ofSize: Constants.FontSize.hintSize, font: UIFont.Roboto.medium)
        interestImageView.getImage(from: showingInterest.imageURL, success: { [weak self] _ in
            self?.interestLabel.textColor = .white
        })
    }
    
    private func updateCheckedState() {
        guard !(showingInterest?.isAddMoreData ?? false) else { return }
        self.interestImageView.isHidden = isChecked
        self.interestLabel.textColor = isChecked || interestImageView.image == nil ? .appTextMediumBright : .white
        self.checkedIconImage.isHidden = !isChecked
    }
}
