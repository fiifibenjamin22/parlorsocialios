//
//  HasActivateMembershipView.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 16/01/2020.
//

import Foundation
import UIKit

@objc protocol HasActivateMembershipView: class {
    var activateMembershipView: ActivateMembershipView! { get set }
    @objc func didTapActivateMembershipMoreButton()
}

extension HasActivateMembershipView where Self: UIViewController {
    func setupActivateMembershipView() {
        view.addSubview(activateMembershipView)
        activateMembershipView.topAnchor.equal(to: view.safeAreaLayoutGuide.topAnchor)
        activateMembershipView.leadingAnchor.equal(to: view.leadingAnchor)
        activateMembershipView.trailingAnchor.equal(to: view.trailingAnchor)
        activateMembershipView.moreButton.addTarget(self, action: #selector(didTapActivateMembershipMoreButton), for: .touchUpInside)
    }
}
