//
//  ActivateMembershipView.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 09/01/2020.
//

import UIKit

class ActivateMembershipView: UIView {
    
    // MARK: - Views
    private(set) lazy var horizontalLabelsStackView: UIStackView = {
        return UIStackView(axis: .horizontal, spacing: 3, alignment: .center, distribution: .fill).manualLayoutable()
    }()
    
    private(set) lazy var daysLabel: ParlorLabel = {
        return ParlorLabel.styled(
            withTextColor: .white,
            withFont: UIFont.custom(ofSize: Constants.FontSize.small, font: UIFont.Roboto.bold),
            alignment: .left,
            numberOfLines: 1
        ).manualLayoutable()
    }()
    
    private(set) lazy var activateMembershipLabel: ParlorLabel = {
        return ParlorLabel.styled(
            withTextColor: .white,
            withFont: UIFont.custom(ofSize: Constants.FontSize.xxTiny, font: UIFont.Roboto.medium),
            alignment: .left,
            numberOfLines: 1
        ).manualLayoutable().apply { $0.lineHeight = 13 }
    }()
    
    private(set) lazy var moreButton: RoundedButton = {
        return RoundedButton().manualLayoutable().apply {
            $0.setTitle(Strings.MembershipPlans.moreButtonTitle.localized.uppercased(), for: .normal)
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.xxTiny, font: UIFont.Roboto.medium)
            $0.setTitleColor(.white, for: .normal)
            $0.layer.borderColor = UIColor.white.cgColor
            $0.layer.borderWidth = 1
            $0.applyTouchAnimation()
        }
    }()
    
    // MARK: - Properties
    var heightAnchorContraint: NSLayoutConstraint!
    
    override var isHidden: Bool {
        get {
            return super.isHidden
        }
        set {
            super.isHidden = newValue
            heightAnchorContraint.constant = newValue ? 0 : 43
        }
    }

    // MARK: - Initialize and setup
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(with data: ActivateMembershipViewModel) {
        isHidden = data.isHidden
        activateMembershipLabel.text = data.toActivateMembershipString
        daysLabel.text = data.daysString
    }
    
    // MARK: - Private methods
    private func initialize() {
        self.clipsToBounds = true
        setupView()
    }
    
    private func setupView() {
        setupHierarchy()
        setupProperties()
        setupAutoLayout()
    }
    
    private func setupProperties() {
        backgroundColor = .appGreen
        activateMembershipLabel.text = Strings.MembershipPlans.toActivateMembership.localized
        daysLabel.apply {
            $0.adjustsFontSizeToFitWidth = true
            $0.minimumScaleFactor = 0.6
        }
    }
    
    private func setupHierarchy() {
        addSubviews([horizontalLabelsStackView, moreButton])
        horizontalLabelsStackView.addArrangedSubviews([daysLabel, activateMembershipLabel])
    }
    
    private func setupAutoLayout() {
        heightAnchorContraint = heightAnchor.equalTo(constant: 43)
        daysLabel.setContentHuggingPriority(.required, for: .horizontal)
        horizontalLabelsStackView.apply {
            $0.edgesToParent(anchors: [.leading], insets: .margins(left: 20))
            $0.trailingAnchor.equal(to: moreButton.leadingAnchor, constant: -5)
            $0.centerYAnchor.equal(to: self.centerYAnchor)
        }
        moreButton.apply {
            $0.edgesToParent(anchors: [.trailing], insets: .margins(right: 15))
            $0.heightAnchor.equalTo(constant: 28)
            $0.widthAnchor.equalTo(constant: 90)
            $0.centerYAnchor.equal(to: self.centerYAnchor)
        }
        activateMembershipLabel.setContentCompressionResistancePriority(.required, for: .horizontal)
    }
    
}
