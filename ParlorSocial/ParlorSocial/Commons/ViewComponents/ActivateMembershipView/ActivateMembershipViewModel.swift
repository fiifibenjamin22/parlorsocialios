//
//  ActivateMembershipViewModel.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 09/01/2020.
//

struct ActivateMembershipViewModel {
    let isHidden: Bool
    let daysString: String
    let toActivateMembershipString: String?
}
