//
//  HasActivateMembershipViewLogic.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 16/01/2020.
//

import Foundation
import RxSwift

protocol HasActivateMembershipViewLogic {
    var activateMembershipViewLogic: ActivateMembershipViewLogic? { get set }
    var setupActivateMembershipViewSubject: PublishSubject<ActivateMembershipViewModel> { get set }
    func didTapActivateMembershipMoreButton()
    func initActivateMembershipViewLogic()
}
