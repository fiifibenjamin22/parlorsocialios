//
//  ActivateMembershipViewLogic.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 09/01/2020.
//

import Foundation
import RxSwift
import RxCocoa

class ActivateMembershipViewLogic {
    // MARK: - Properties
    private let disposeBag = DisposeBag()
    private let profileRepository: ProfileRepositoryProtocol
    private let countOfDaysForComplimentaryWhenShowView = 14
    let setupViewRelay = BehaviorRelay<ActivateMembershipViewModel?>(value: nil)
    let buttonClickedSubject = PublishSubject<Void>()
    
    // MARK: - Initialization
    init(profileRepository: ProfileRepositoryProtocol) {
        self.profileRepository = profileRepository
        setup()
    }
    
    func buttonClicked() {
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.ActivateMembership.more.rawValue)
        buttonClickedSubject.emitElement()
    }
    
    // MARK: - Private methods
    private func setup() {
        profileRepository.currentProfileObs
            .subscribe(onNext: { [weak self] profile in
                self?.handleProfileResponse(profile: profile)
            }).disposed(by: disposeBag)
    }
    
    private func handleProfileResponse(profile: Profile) {
        guard let daysCount = profile.membershipExpiryReminder?.expiryDate?.countOfDaysFromToday else {
            setupViewRelay.accept(ActivateMembershipViewModel(isHidden: true, daysString: "", toActivateMembershipString: nil))
            return
        }
        setupViewRelay.accept(ActivateMembershipViewModel(
            isHidden: profile.membershipExpiryReminder == nil,
            daysString: daysString(by: daysCount),
            toActivateMembershipString: profile.membershipExpiryReminder?.type == .preview ?
                Strings.MembershipPlans.toActivateMembership.localized :
                Strings.MembershipPlans.toExtendMembership.localized)
        )
    }
    
    private func daysString(by daysCount: Int) -> String {
        if daysCount > 1 {
            return String(format: Strings.MembershipPlans.daysCount.localized.uppercased(), daysCount)
        } else if daysCount == 1 {
            return Strings.MembershipPlans.oneDayCount.localized.uppercased()
        } else {
            return Strings.MembershipPlans.lastDay.localized.uppercased()
        }
    }
    
}
 
