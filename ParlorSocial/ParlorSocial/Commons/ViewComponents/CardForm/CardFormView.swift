//  CardFormView.swift
//  ParlorSocialClub
//
//  Created on 15/04/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

class CardFormView: UIView {
    
    // MARK: - Views
    
    private(set) var stackView: UIStackView!
    private(set) var cardHorizontalStack: UIStackView!
    private(set) var cityHorizontalStack: UIStackView!
    private(set) var numberTextField: ParlorTextField!
    private(set) var monthTextField: ParlorTextField!
    private(set) var yearTextField: ParlorTextField!
    private(set) var cvvTextField: ParlorTextField!
    private(set) var nameTextField: ParlorTextField!
    private(set) var address1TextField: ParlorTextField!
    private(set) var address2TextField: ParlorTextField!
    private(set) var cityTextField: ParlorTextField!
    private(set) var stateTextField: ParlorTextField!
    private(set) var zipTextField: ParlorTextField!
    
    // MARK: - Properties
    
    lazy var stringFieldsToTypeMap: [ParlorTextField: CardUploadStringField] = {
        return [
            numberTextField: CardUploadStringField.number,
            cvvTextField: CardUploadStringField.cvc,
            nameTextField: CardUploadStringField.name,
            address1TextField: CardUploadStringField.address1,
            address2TextField: CardUploadStringField.address2,
            cityTextField: CardUploadStringField.city,
            stateTextField: CardUploadStringField.state,
            zipTextField: CardUploadStringField.zip
        ]
    }()
    
    lazy var intFieldsToTypeMap: [ParlorTextField: CardUploadIntField] = {
        return [
            monthTextField: CardUploadIntField.expMonth,
            yearTextField: CardUploadIntField.expYear
        ]
    }()
    
    // MARK: - Init
    
    init() {
        super.init(frame: .zero)
        
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setup
    
    private func setupViews() {
        let builder = CardFormViewBuilder(view: self)
        self.stackView = builder.buildMainStackView()
        self.cardHorizontalStack = builder.buildHorizontalStack()
        self.cityHorizontalStack = builder.buildHorizontalStack()
        self.numberTextField = builder.buildEditableField()
        self.monthTextField = builder.buildEditableField()
        self.yearTextField = builder.buildEditableField()
        self.cvvTextField = builder.buildEditableField()
        self.nameTextField = builder.buildEditableField()
        self.address1TextField = builder.buildEditableField()
        self.address2TextField = builder.buildEditableField()
        self.cityTextField = builder.buildEditableField()
        self.stateTextField = builder.buildEditableField()
        self.zipTextField = builder.buildEditableField()
        builder.setupViews()
    }
}
