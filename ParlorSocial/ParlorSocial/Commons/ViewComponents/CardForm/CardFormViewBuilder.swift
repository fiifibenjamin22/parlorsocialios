//  CardFormViewBuilder.swift
//  ParlorSocialClub
//
//  Created on 15/04/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

class CardFormViewBuilder {
    
    // MARK: - Properties
    
    private let view: CardFormView
    
    // MARK: - Init
    
    init(view: CardFormView) {
        self.view = view
    }
    
    // MARK: - Setup
    
    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
    
    private func setupProperties() {
        view.backgroundColor = .white
        view.apply {
            $0.numberTextField.placeholder = Strings.Cards.Add.number.localized
            $0.monthTextField.placeholder = Strings.Cards.Add.mm.localized
            $0.yearTextField.placeholder = Strings.Cards.Add.yy.localized
            $0.cvvTextField.placeholder = Strings.Cards.Add.cvv.localized
            $0.nameTextField.placeholder = Strings.Cards.Add.name.localized
            $0.address1TextField.placeholder = Strings.Cards.Add.address1.localized
            $0.address2TextField.placeholder = Strings.Cards.Add.address2.localized
            $0.cityTextField.placeholder = Strings.Cards.Add.city.localized
            $0.zipTextField.placeholder = Strings.Cards.Add.zip.localized
            $0.stateTextField.placeholder = Strings.Cards.Add.state.localized
            
            $0.numberTextField.maxCharacterCount = 16
            $0.monthTextField.maxCharacterCount = 2
            $0.yearTextField.maxCharacterCount = 2
            $0.cvvTextField.maxCharacterCount = 4
            $0.nameTextField.maxCharacterCount = 128
            [$0.address1TextField, $0.address2TextField, $0.cityTextField, $0.stateTextField].forEach {
                $0?.maxCharacterCount = 200
            }
            $0.zipTextField.maxCharacterCount = 10

            [$0.numberTextField, $0.monthTextField, $0.yearTextField, $0.cvvTextField].forEach {
                $0?.keyboardType = .numberPad
            }
            $0.zipTextField.keyboardType = .numbersAndPunctuation
            $0.nameTextField.keyboardType = .namePhonePad
        }
    }

    private func setupHierarchy() {
        view.apply {
            view.addSubview($0.stackView)
            $0.stackView.addArrangedSubviews([
                $0.numberTextField,
                $0.cardHorizontalStack,
                $0.nameTextField,
                $0.address1TextField,
                $0.address2TextField,
                $0.cityHorizontalStack
            ])
            
            $0.cardHorizontalStack.addArrangedSubviews([
                $0.monthTextField,
                $0.yearTextField,
                $0.cvvTextField
            ])
            
            $0.cityHorizontalStack.addArrangedSubviews([
                $0.cityTextField,
                $0.stateTextField,
                $0.zipTextField
            ])
        }
    }

    private func setupAutoLayout() {
        view.apply {
            $0.stackView.edgesToParent()
            
            $0.monthTextField.widthAnchor.equal(to: $0.yearTextField.widthAnchor)
            $0.cvvTextField.widthAnchor.constraint(equalTo: $0.monthTextField.widthAnchor, multiplier: 2).activate()

            $0.stateTextField.widthAnchor.equal(to: $0.zipTextField.widthAnchor)
            $0.cityTextField.widthAnchor.constraint(equalTo: $0.zipTextField.widthAnchor, multiplier: 2).activate()
        }
    }
    
    // MARK: - Public methods
    
    func buildEditableField() -> ParlorTextField {
        return ParlorTextField().apply {
            $0.placeholderColor = .appGreyLight
            $0.font = UIFont.custom(ofSize: Constants.FontSize.hintSize, font: UIFont.Roboto.regular)
            $0.textColor = .appTextBright
            $0.tintColor = .textVeryLight
            $0.heightAnchor.equalTo(constant: 60)
            $0.layer.borderColor = UIColor.textVeryLight.cgColor
            $0.layer.borderWidth = 1
            $0.padding = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        }
    }
    
    func buildMainStackView() -> UIStackView {
        return UIStackView(
            axis: .vertical,
            spacing: 10,
            alignment: .fill,
            distribution: .equalSpacing
        ).manualLayoutable()
    }
    
    func buildHorizontalStack() -> UIStackView {
        return UIStackView(
            axis: .horizontal,
            with: [],
            spacing: Constants.Margin.tiny,
            alignment: .fill, distribution: .fill
            )
    }
}
