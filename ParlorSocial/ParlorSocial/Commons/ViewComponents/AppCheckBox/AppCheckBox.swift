//
//  AppCheckBox.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 16/04/2019.
//

import UIKit

protocol AppCheckBoxDelegate: class {
    func appCheckboxDidChangeStatus(_ appCheckbox: AppCheckBox)
}

class AppCheckBox: UIView {

    weak var delegate: AppCheckBoxDelegate?
    var checkedImage: UIImage = Icons.CheckBox.selected {
        didSet {
            button.setImage(isChecked ? checkedImage : uncheckedImage, for: .normal)
        }
    }
    
    var uncheckedImage: UIImage = Icons.CheckBox.default {
        didSet {
            button.setImage(isChecked ? checkedImage : uncheckedImage, for: .normal)
        }
    }

    var isChecked: Bool = false {
        didSet {
            button.setImage(isChecked ? checkedImage : uncheckedImage, for: .normal)
        }
    }

    var title: String? {
        set { label.text = newValue }
        get { return label.text }
    }

    private let button = UIButton().manualLayoutable()

    private let label = ParlorLabel.styled(
        withTextColor: .appGreyLight,
        withFont: UIFont.custom(
            ofSize: Constants.FontSize.xTiny,
            font: UIFont.Roboto.regular),
        alignment: .left)

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func initialize() {
        manualLayoutable()
        setupView()

        button.tintColor = .eventTime
        button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapButton)))
        self.isChecked = false
    }

    private func setupView() {
        [button, label].addTo(parent: self)
        let margin: CGFloat = 8
        button.apply {
            $0.edgesToParent(anchors: [.leading, .top], insets: UIEdgeInsets.margins(top: margin, bottom: margin))
            $0.bottomAnchor.lessThanOrEqual(to: bottomAnchor, constant: -margin)
        }

        label.apply {
            $0.topAnchor.greaterThanOrEqual(to: topAnchor, constant: margin)
            $0.leadingAnchor.equal(to: button.trailingAnchor, constant: margin)
            $0.bottomAnchor.lessThanOrEqual(to: bottomAnchor, constant: -margin)
            $0.trailingAnchor.equal(to: trailingAnchor)
            $0.centerYAnchor.equal(to: centerYAnchor)
        }
    }

    @objc private func didTapButton() {
        isChecked = !isChecked
        delegate?.appCheckboxDidChangeStatus(self)
    }

}
