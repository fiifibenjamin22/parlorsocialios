//
//  HostView.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 15/04/2019.
//

import UIKit

class HostView: UIView {

    private let imageSize: CGFloat = 42
    private let viewHeight: CGFloat = 80
    private let horizontalMargin: CGFloat = 17
    private let hostNameMargin: CGFloat = 15
    private let detailsButtonSize: CGFloat = 12
    
    private(set) var hostIconView: UIImageView!
    private(set) var hostNameLabel: UILabel!
    private(set) var detailsButton: UIButton!
    
    var selectionCallback: ((ParlorHost) -> Void) = { _ in }
    
    var host: ParlorHost? {
        didSet {
            guard let host = host else { return }
            populate(with: host)
        }
    }

    init() {
        super.init(frame: .zero)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.rsvpBorder.cgColor
        self.applyTouchAnimation()
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapView)))
        
        self.hostIconView = UIImageView().manualLayoutable().apply {
            $0.contentMode = .scaleAspectFill
            $0.clipsToBounds = true
            $0.layer.cornerRadius = imageSize / 2
        }
        
        self.hostNameLabel = UILabel.styled(textColor: UIColor.appBackground, withFont: UIFont.custom(ofSize: Constants.FontSize.hintSize, font: UIFont.Editor.bold), alignment: .left, numberOfLines: 1)
        
        self.detailsButton = UIButton().manualLayoutable().apply {
            $0.setImage(Icons.Common.backIcon, for: .normal)
            $0.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        }
    
        self.layoutViews()
    }
    
    private func layoutViews() {
        self.addSubviews([hostIconView, hostNameLabel, detailsButton])
        
        self.heightAnchor.equalTo(constant: viewHeight)
        
        self.hostIconView.apply {
            $0.centerYAnchor.equal(to: self.centerYAnchor)
            $0.widthAnchor.equalTo(constant: self.imageSize)
            $0.heightAnchor.equalTo(constant: self.imageSize)
            $0.leadingAnchor.equal(to: self.leadingAnchor, constant: horizontalMargin)
        }
        
        self.hostNameLabel.apply {
            $0.centerYAnchor.equal(to: self.centerYAnchor)
            $0.leadingAnchor.equal(to: self.hostIconView.trailingAnchor, constant: hostNameMargin)
            $0.trailingAnchor.equal(to: self.detailsButton.leadingAnchor, constant: -hostNameMargin)
        }
        
        self.detailsButton.apply {
            $0.centerYAnchor.equal(to: self.centerYAnchor)
            $0.trailingAnchor.equal(to: self.trailingAnchor, constant: -20)
            $0.widthAnchor.equalTo(constant: detailsButtonSize)
            $0.heightAnchor.equalTo(constant: detailsButtonSize)
        }
    }
    
    private func populate(with host: ParlorHost) {
        hostIconView.getImage(from: host.imageUrlGreyscale)
        hostNameLabel.text = host.name
    }
    
    @objc private func didTapView() {
        guard let host = host else { return }
        selectionCallback(host)
    }
}
