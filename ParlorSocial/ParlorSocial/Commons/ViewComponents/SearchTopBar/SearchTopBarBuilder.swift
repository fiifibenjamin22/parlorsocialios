//
//  SearchTopBarBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 18/07/2019.
//

import UIKit

final class SearchTopBarBuilder {
    
    private unowned let view: SearchTopBarView
    
    init(view: SearchTopBarView) {
        self.view = view
    }
    
    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

extension SearchTopBarBuilder {
    
    private func setupProperties() {
        view.backgroundColor = .white
        view.cleanTextButton.apply {
            $0.isHidden = true
            $0.setImage(Icons.SearchBar.clean, for: .normal)
            $0.trackingId = AnalyticEventClickableElementName.SearchScreen.clearText.rawValue
        }
        view.cancelButton.apply {
            $0.setTitle(Strings.cancel.localized.capitalizingFirstLetter(), for: .normal)
            $0.trackingId = AnalyticEventClickableElementName.SearchScreen.cancel.rawValue
        }
        view.searchTextField.placeholder = Strings.Search.searchPlaceholder.localized
        view.searchTextField.attributedPlaceholder = NSAttributedString(
            string: Strings.Search.searchPlaceholder.localized,
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.appGreyLight]
        )
        view.searchTextField.text = ""
    }
    
    private func setupHierarchy() {
        view.apply {
            $0.addSubviews([$0.searchImageView, $0.searchTextField, $0.cleanTextButton, $0.cancelButton])
        }
    }
    
    private func setupAutoLayout() {
        view.searchImageView.apply {
            $0.edgesToParent(anchors: [.leading], insets: UIEdgeInsets.margins(left: Constants.Margin.standard))
            $0.centerYAnchor.equal(to: view.centerYAnchor)
            $0.heightAnchor.equalTo(constant: 16)
            $0.widthAnchor.equalTo(constant: 16)
        }
        
        view.searchTextField.apply {
            $0.leadingAnchor.equal(to: view.searchImageView.trailingAnchor, constant: 16)
            $0.trailingAnchor.equal(to: view.cleanTextButton.leadingAnchor, constant: -10)
            $0.centerYAnchor.equal(to: view.centerYAnchor)
        }
        
        view.cleanTextButton.apply {
            $0.trailingAnchor.equal(to: view.cancelButton.leadingAnchor, constant: -10)
            $0.heightAnchor.equalTo(constant: 21)
            $0.widthAnchor.equalTo(constant: 21)
            $0.centerYAnchor.equal(to: view.centerYAnchor)
            $0.setContentHuggingPriority(.required, for: .horizontal)
        }
        
        view.cancelButton.apply {
            $0.edgesToParent(anchors: [.trailing], insets: UIEdgeInsets.margins(right: Constants.Margin.standard))
            $0.centerYAnchor.equal(to: view.centerYAnchor)
            $0.setContentCompressionResistancePriority(.required, for: .horizontal)
            $0.setContentHuggingPriority(.required, for: .horizontal)
        }
    }
    
}

extension SearchTopBarBuilder {
    
    func buildSearchImageView() -> UIImageView {
        return UIImageView(image: Icons.SearchBar.search).manualLayoutable()
    }
    
    func buildSearchTextField() -> UITextField {
        return UITextField().manualLayoutable().apply {
            $0.backgroundColor = .white
            $0.font = UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.regular)
            $0.textColor = .appTextMediumBright
            $0.autocorrectionType = .no
            $0.tintColor = .lightBackgroundSeperator
        }
    }
    
    func buildCleanTextButton() -> TrackableButton {
        return TrackableButton().manualLayoutable().apply {
            $0.applyTouchAnimation()
        }
    }
    
    func buildCancelButton() -> TrackableButton {
        return TrackableButton().manualLayoutable().apply {
            $0.applyTouchAnimation()
            $0.contentEdgeInsets = UIEdgeInsets(padding: 5)
            $0.setTitleColor(.appTextMediumBright, for: .normal)
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.regular)
        }
    }
}
