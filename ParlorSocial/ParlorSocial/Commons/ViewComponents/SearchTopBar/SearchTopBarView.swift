//
//  SearchTopBarView.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 18/07/2019.
//

import UIKit
import RxSwift
import RxCocoa

protocol SearchTopBarViewDelegate: class {
    func didTapCancel()
    func didSearchTextChange(text: String)
}

final class SearchTopBarView: UIView {
    
    // MARK: - Views
    private(set) var searchImageView: UIImageView!
    private(set) var searchTextField: UITextField!
    private(set) var cleanTextButton: TrackableButton!
    private(set) var cancelButton: TrackableButton!
    
    // MARK: - Properties
    private let disposeBag = DisposeBag()
    weak private var delegate: SearchTopBarViewDelegate?
    
    init(delegate: SearchTopBarViewDelegate?) {
        super.init(frame: .zero)
        initialize(delegate: delegate)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Private methods
extension SearchTopBarView {
    
    private func initialize(delegate: SearchTopBarViewDelegate?) {
        self.delegate = delegate
        manualLayoutable()
        setupViews()
        setupObservables()
    }
    
    private func setupViews() {
        let builder = SearchTopBarBuilder(view: self)
        
        searchImageView = builder.buildSearchImageView()
        searchTextField = builder.buildSearchTextField()
        cleanTextButton = builder.buildCleanTextButton()
        cancelButton = builder.buildCancelButton()
        
        builder.setupViews()
        cancelButton.addTarget(self, action: #selector(didTapCancel), for: .touchUpInside)
        cleanTextButton.addTarget(self, action: #selector(didTapCleanText), for: .touchUpInside)
        searchTextField.becomeFirstResponder()
    }
    
    private func setupObservables() {
        bindSearchTextField()
    }
    
    private func bindSearchTextField() {
        searchTextField
            .rx
            .controlEvent(.editingChanged)
            .withLatestFrom(searchTextField.rx.text.orEmpty)
            .doOnNext { [unowned self] in
                self.cleanTextButton.isHidden = $0.isEmpty
            }
            .debounce(.milliseconds(500), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [unowned self] text in
                self.handleTextEditing(response: text)
            }).disposed(by: disposeBag)
        
        searchTextField
            .rx
            .controlEvent(.editingDidBegin)
            .withLatestFrom(searchTextField.rx.text.orEmpty)
            .subscribe(onNext: { [unowned self] in
                self.cleanTextButton.isHidden = $0.isEmpty
            }).disposed(by: disposeBag)
    }
    
    private func handleTextEditing(response: String) {
        delegate?.didSearchTextChange(text: response)
    }
    
    @objc private func didTapCancel() {
        delegate?.didTapCancel()
    }
    
    @objc private func didTapCleanText() {
        cleanTextButton.isHidden = true
        searchTextField.text = ""
        delegate?.didSearchTextChange(text: "")
    }
}

