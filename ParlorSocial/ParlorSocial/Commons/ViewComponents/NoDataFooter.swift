//
//  NoDataFooter.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 18/06/2019.
//

import UIKit

class NoDataFooter: UITableViewHeaderFooterView {
    
    // MARK: - Properties
    
    private let horizontalTextMargin: CGFloat = 15
    private let verticalTextMargin: CGFloat = 15
    
    // MARK: - Views
    private(set) lazy var cardView: UIView = {
        return UIView().manualLayoutable().apply {
            $0.layer.borderWidth = 0.5
        }
    }()
    
    lazy var noDataTitle: ParlorLabel = {
        return ParlorLabel.styled(
            withFont: UIFont.custom(ofSize: Constants.FontSize.hintSize, font: UIFont.Roboto.medium),
            alignment: .left,
            numberOfLines: 0
        )
    }()
    
    lazy var noDataMessage: ParlorLabel = {
        return ParlorLabel.styled(
            withFont: UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.regular),
            alignment: .left,
            numberOfLines: 0
            ).apply { $0.lineHeight = 19 }
    }()
    
    private lazy var verticalStackView: UIStackView = {
        return UIStackView(
            axis: .vertical,
            spacing: 10,
            alignment: .fill,
            distribution: .fill
        )
    }()
    
    private lazy var topSpaceView: UIView = {
        return UIView().manualLayoutable()
    }()
    
    private lazy var bottomSpaceView: UIView = {
        return UIView().manualLayoutable()
    }()
    
    var isVisible: Bool = true {
        didSet {
            [topSpaceView, noDataTitle, noDataMessage, bottomSpaceView, self].forEach { $0.isHidden = !isVisible }
        }
    }

    // MARK: - Initialization
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        setupAutoLayout()
    }
    
    private func setupAutoLayout() {
        addSubview(cardView)
        
        cardView.addSubview(verticalStackView)
        
        cardView.apply {
            $0.edgesToParent(insets: UIEdgeInsets.margins(left: Constants.Margin.standard, right: Constants.Margin.standard))
        }
        
        verticalStackView.addArrangedSubviews([topSpaceView, noDataTitle, noDataMessage, bottomSpaceView])
        
        verticalStackView.apply {
            $0.edgesToParent(
                anchors: [.top, .leading, .bottom, .trailing],
                insets: UIEdgeInsets.margins(left: horizontalTextMargin, right: horizontalTextMargin)
            )
        }
        
        topSpaceView.heightAnchor.equalTo(constant: verticalTextMargin)
        bottomSpaceView.heightAnchor.equalTo(constant: verticalTextMargin)

    }

}
