//
//  ParlorButtonStyle.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 27/04/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

struct ParlorButtonStyleAppearance {
    let backgroundColor: UIColor
    let titleColor: UIColor
    let selectedTitleColor: UIColor
    let font: UIFont
    let letterSpacing: CGFloat
    let borderWidth: CGFloat
    let borderColor: UIColor
}

enum ParlorButtonStyle {
    case plain
    case selectable
    case white
    case outlined(titleColor: UIColor, borderColor: UIColor, font: UIFont)

    var appearance: ParlorButtonStyleAppearance {
        switch self {
        case .plain:
            return ParlorButtonStyleAppearance(
                backgroundColor: UIColor.clear,
                titleColor: UIColor.black,
                selectedTitleColor: UIColor.black,
                font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.bold),
                letterSpacing: Constants.LetterSpacing.small,
                borderWidth: 0,
                borderColor: UIColor.clear
            )
        case .white:
            return ParlorButtonStyleAppearance(
                backgroundColor: UIColor.white,
                titleColor: UIColor.appTextMediumBright,
                selectedTitleColor: UIColor.appTextMediumBright,
                font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium),
                letterSpacing: Constants.LetterSpacing.small,
                borderWidth: 0,
                borderColor: UIColor.clear
            )
        case .selectable:
            return ParlorButtonStyleAppearance(
                backgroundColor: UIColor.clear,
                titleColor: UIColor.closeTintColor,
                selectedTitleColor: UIColor.black,
                font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.bold),
                letterSpacing: Constants.LetterSpacing.small,
                borderWidth: 0,
                borderColor: UIColor.clear
            )
        case .outlined(let titleColor, let borderColor, let font):
            return ParlorButtonStyleAppearance(
                backgroundColor: .clear,
                titleColor: titleColor,
                selectedTitleColor: titleColor,
                font: font,
                letterSpacing: Constants.LetterSpacing.none,
                borderWidth: 1,
                borderColor: borderColor
            )
        }
    }
}
