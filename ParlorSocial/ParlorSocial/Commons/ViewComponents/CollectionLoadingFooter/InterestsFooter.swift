//
//  CollectionLoadingFooter.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 10/05/2019.
//

import UIKit

class InterestsFooter: UICollectionReusableView {
    
    // MARK: - Views
    private(set) lazy var loadingIndicator: UIActivityIndicatorView = {
        return UIActivityIndicatorView(style: .gray).manualLayoutable().apply {
            $0.tintColor = .appGrey
            $0.startAnimating()
        }
    }()
    
    private(set) lazy var suggestInterestButton: UIButton = {
        return RoundedButton().manualLayoutable().apply {
            $0.backgroundColor = .white
            $0.applyTouchAnimation()
            $0.addShadow(withOpacity: 0.05, radius: 1.5)
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium)
            $0.setTitleColor(.appText, for: .normal)
        }
    }()
    
    // MARK: - Properties
    var isLoadingFooterVisible: Bool = true {
        didSet {
            updateState()
        }
    }
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        suggestInterestButton.setTitle(Strings.Interests.suggest.localized.uppercased(), for: .normal)
        setupAutoLayout()
        updateState()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupAutoLayout() {
        addSubviews([loadingIndicator, suggestInterestButton])
        
        loadingIndicator.apply {
            $0.centerXAnchor.equal(to: self.centerXAnchor)
            $0.centerYAnchor.equal(to: self.centerYAnchor)
        }
        
        suggestInterestButton.apply {
            $0.centerXAnchor.equal(to: self.centerXAnchor)
            $0.centerYAnchor.equal(to: self.centerYAnchor)
            $0.edgesToParent(anchors: [.leading, .trailing], insets: UIEdgeInsets.margins(left: 2, right: 2))
            $0.heightAnchor.equalTo(constant: Constants.bigButtonHeight)
        }
    }
    
    private func updateState() {
        self.loadingIndicator.isHidden = !isLoadingFooterVisible
        self.suggestInterestButton.isHidden = isLoadingFooterVisible
    }
}
