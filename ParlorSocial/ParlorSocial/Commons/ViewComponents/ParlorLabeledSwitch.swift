//
//  ParlorLabeledSwitch.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 22/07/2019.
//

import UIKit

class ParlorLabeledSwitch: UIView {
    
    // MARK: Views
    lazy var label: ParlorLabel = {
       return ParlorLabel.styled(
        withFont: UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.medium)
        )
    }()
    
    lazy var uiSwitch: UISwitch = {
        return UISwitch().manualLayoutable()
    }()
    
    // MARK: Properties
    var isOn: Bool {
        get {
            return uiSwitch.isOn
        }
        set {
            uiSwitch.isOn = newValue
        }
    }

    // MARK: Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        addSubviews([label, uiSwitch])
        
        layer.borderColor = UIColor.textVeryLight.cgColor
        layer.borderWidth = 1
        
        setupAutoLayout()
    }
    
    private func setupAutoLayout() {
        self.heightAnchor.equalTo(constant: 51)
        
        uiSwitch.apply {
            $0.centerYAnchor.equal(to: self.centerYAnchor)
            $0.trailingAnchor.equal(to: self.trailingAnchor, constant: -15)
        }
        
        label.apply {
            $0.leadingAnchor.equal(to: self.leadingAnchor, constant: 20)
            $0.centerYAnchor.equal(to: self.centerYAnchor)
        }
    }

}
