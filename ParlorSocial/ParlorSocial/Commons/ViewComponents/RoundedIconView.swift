//
//  RoundedIconView.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 04/04/2019.
//

import UIKit

class RoundedIconView: UIView {
    
    private let usesDefaultSize: Bool
    private let imageView: UIImageView = UIImageView()
    
    var iconImage: UIImage? {
        didSet {
            imageView.image = iconImage
        }
    }
    
    init(image: UIImage? = nil, usesDefaultSize: Bool = true) {
        self.usesDefaultSize = usesDefaultSize
        self.iconImage = image
        super.init(frame: .zero)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        self.clipsToBounds = true
        self.initImageView()
        if usesDefaultSize {
            self.heightAnchor.equalTo(constant: Constants.roundedIconSize)
            self.widthAnchor.equalTo(constant: Constants.roundedIconSize)
        }
        
    }
    
    private func initImageView() {
        addSubview(imageView)
        imageView.apply {
            $0.manualLayoutable()
            $0.centerXAnchor.equal(to: self.centerXAnchor)
            $0.centerYAnchor.equal(to: self.centerYAnchor)
            $0.clipsToBounds = true
            $0.image = iconImage
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = self.bounds.height / 2
    }

}
