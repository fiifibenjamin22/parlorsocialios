//
//  Shadow.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 01/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

struct Shadow {
    let color: UIColor
    let offset: CGPoint
    let opacity: CGFloat
    let radius: CGFloat

    init(color: UIColor = .black, offset: CGPoint = .zero, opacity: CGFloat = 0.5, radius: CGFloat = 5.0) {
        self.color = color
        self.offset = offset
        self.opacity = opacity
        self.radius = radius
    }
}
