//
//  HostInfoItemView.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 17/04/2019.
//

import UIKit

class HostInfoItemView: UIStackView {

    private let outerInset: CGFloat = 20
    private let innerInset: CGFloat = 5
    
    private(set) var itemName: ParlorLabel!
    private(set) var itemText: ParlorTextView!
    
    init() {
        super.init(frame: .zero)
        initialize()
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        itemName = ParlorLabel.styled(withTextColor: UIColor.appTextSemiLight, withFont: UIFont.custom(ofSize: Constants.FontSize.xTiny, font: UIFont.Roboto.regular), alignment: .left, numberOfLines: 1).apply {
            $0.insets.top = outerInset
            $0.insets.bottom = innerInset
        }
        
        itemText = ParlorTextView().manualLayoutable().apply {
            $0.font = UIFont.custom(ofSize: Constants.FontSize.hintSize, font: UIFont.Roboto.regular)
            $0.textColor = .appTextMediumBright
            $0.tintColor = .appTextMediumBright
            $0.lineHeight = Constants.lineHeightBig
            $0.textContainerInset = UIEdgeInsets.margins(top: innerInset, left: 0, bottom: outerInset, right: 0)
        }
        
        axis = .vertical
        distribution = .equalSpacing
        spacing = 0
        alignment = .leading
        addArrangedSubviews([itemName, itemText])
    }
}
