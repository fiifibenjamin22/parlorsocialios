//
//  HomeWelcomeView.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 24/04/2019.
//

import UIKit

protocol HomeWelcomeViewDelegate: class {
    func didTapSearch()
    func didTapCalendar()
}

class HomeWelcomeView: UIView {
    
    // MARK: - Views
    
    private(set) var noContentView: UIView! = UIView().apply {
        $0.backgroundColor = .white
    }.manualLayoutable()
    
    private(set) var progressIndicator: UIActivityIndicatorView = UIActivityIndicatorView().apply {
        $0.color = .black
        $0.startAnimating()
    }.manualLayoutable()
    
    private(set) var backgroundImage: UIImageView! = UIImageView().apply {
        $0.contentMode = .scaleAspectFill
        }.manualLayoutable()
    
    private(set) var searchImageView: UIImageView = {
        return UIImageView(image: Icons.NavigationBar.search.withRenderingMode(.alwaysTemplate)).manualLayoutable().apply {
            $0.tintColor = .white
        }
    }()
    
    private(set) var nameLabel: ParlorLabel = ParlorLabel.styled(
        withTextColor: .white,
        withFont: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.bold),
        alignment: .center,
        numberOfLines: 1,
        letterSpacing: 1.1)
    
    private(set) var calendarImageView: UIImageView = {
        return UIImageView(image: Icons.NavigationBar.calendar.withRenderingMode(.alwaysTemplate)).manualLayoutable().apply {
            $0.tintColor = .white
        }
    }()
    
    private(set) var topSeperator: UIView = UIView().manualLayoutable().apply {
        $0.backgroundColor = .white
        $0.alpha = 0.14
    }.manualLayoutable()
    
    private(set) var welcomeLabel: ParlorLabel = ParlorLabel.styled(
        withTextColor: .white,
        withFont: UIFont.custom(ofSize: 30, font: UIFont.Dalmatins.regular),
        alignment: .center,
        numberOfLines: 1)
    
    private(set) var dateLabel: ParlorLabel = ParlorLabel.styled(
        withTextColor: .white,
        withFont: UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.regular),
        alignment: .center,
        numberOfLines: 1)
    
    private(set) var starIcon: UIImageView = UIImageView(image: Icons.GuestListRsvp.premiumIcon).manualLayoutable()
    
    private(set) var bottomMessage: ParlorLabel = ParlorLabel.styled(
        withTextColor: .white,
        withFont: UIFont.custom(ofSize: Constants.FontSize.xxTiny, font: UIFont.Roboto.regular),
        alignment: .center,
        numberOfLines: 1)
    
    private(set) var bottomIcon: UIImageView = UIImageView(image: Icons.Welcome.homeArrowDown).manualLayoutable()
    
    // MARK: - Properties
    private let timeIntervalToHide: TimeInterval = 5
    
    private var isAnimating: Bool = false
    
    private weak var delegate: HomeWelcomeViewDelegate?
    
    var didStartFadingOut: () -> Void = { }
    
    var welcomeData: WelcomeScreenData? {
        didSet {
            updateContent()
        }
    }
    
    var displayProfile: Profile? {
        didSet {
            updateProfileContent()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
        isExclusiveTouch = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        animateFadeOutIfNeeded()
        return nil
    }
    
    func hide() {
        animateFadeOutIfNeeded()
    }
    
    private func initialize() {
        addSubviews([backgroundImage, searchImageView, nameLabel, calendarImageView, topSeperator,
                     welcomeLabel, dateLabel, starIcon, bottomMessage, bottomIcon, noContentView])
        noContentView.addSubview(progressIndicator)
        
        setupAutoLayout()
        welcomeLabel.adjustsFontSizeToFitWidth = true
        welcomeLabel.minimumScaleFactor = 0.01
        backgroundColor = UIColor.black.withAlphaComponent(0.4)
    }
    
    private func setupAutoLayout() {
        backgroundImage.edgesToParent()
        noContentView.edgesToParent()
        
        progressIndicator.apply {
            $0.centerXAnchor.equal(to: noContentView.centerXAnchor)
            $0.centerYAnchor.equal(to: noContentView.centerYAnchor)
        }
        
        searchImageView.apply {
            $0.edgesToParent(anchors: [.leading], insets: UIEdgeInsets.margins(left: Constants.Margin.standard))
            $0.topAnchor.equal(to: self.safeAreaLayoutGuide.topAnchor, constant: 27)
            $0.heightAnchor.equalTo(constant: 16)
            $0.widthAnchor.equalTo(constant: 16)
        }
        
        nameLabel.leadingAnchor.equal(to: searchImageView.trailingAnchor, constant: 10)
        nameLabel.trailingAnchor.equal(to: calendarImageView.leadingAnchor, constant: -10)
        nameLabel.topAnchor.equal(to: self.safeAreaLayoutGuide.topAnchor, constant: 30)
        
        calendarImageView.apply {
            $0.edgesToParent(anchors: [.trailing], insets: UIEdgeInsets.margins(right: Constants.Margin.standard))
            $0.topAnchor.equal(to: self.safeAreaLayoutGuide.topAnchor, constant: 27)
            $0.heightAnchor.equalTo(constant: 16)
            $0.widthAnchor.equalTo(constant: 16)
        }
        
        topSeperator.heightAnchor.equalTo(constant: 1)
        topSeperator.edgesToParent(anchors: [.leading, .trailing])
        topSeperator.topAnchor.equal(to: nameLabel.bottomAnchor, constant: 20)
        
        starIcon.centerXAnchor.equal(to: self.centerXAnchor)
        starIcon.bottomAnchor.equal(to: bottomMessage.topAnchor, constant: -15)

        bottomMessage.centerXAnchor.equal(to: self.centerXAnchor)
        bottomMessage.bottomAnchor.equal(to: bottomIcon.topAnchor, constant: -Constants.Margin.tiny)
        
        welcomeLabel.edgesToParent(anchors: [.leading, .trailing], padding: Constants.Margin.small)
        welcomeLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: -Constants.Margin.big).activate()
        
        dateLabel.topAnchor.equal(to: welcomeLabel.bottomAnchor, constant: Constants.Margin.tiny)
        dateLabel.edgesToParent(anchors: [.leading, .trailing])
        dateLabel.bottomAnchor.lessThanOrEqual(to: bottomMessage.topAnchor)
        
        bottomIcon.centerXAnchor.equal(to: self.centerXAnchor)
        bottomIcon.bottomAnchor.equal(to: self.bottomAnchor, constant: -24)
    }
    
    private func animateFadeOutIfNeeded() {
        if !isAnimating && !isHidden {
            animateFadeOut()
        }
    }
    
    private func animateFadeOut() {
        isAnimating = true
        didStartFadingOut()
        UIView.animate(withDuration: 0.25, animations: {
            self.alpha = 0
        }, completion: { _ in
            self.isHidden = true
            self.isAnimating = false
            self.alpha = 1
        })
    }
    
    private func updateContent() {
        guard let data = welcomeData,
            let imageUrl = URL(
                string: data.imageUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed
            )) else { return }
        isHidden = false
        noContentView.isHidden = false
        startAutoHideTimer()
        backgroundImage.getImage(from: imageUrl, success: { _ in
            self.noContentView.isHidden = true
        }, failure: { _ in
            self.animateFadeOut()
        })
        welcomeLabel.text = data.text
        dateLabel.text = Date().toWelcomeMessageFormat()
    }
    
    private func updateProfileContent() {
        guard let profile = displayProfile else { return }
        let isPremium = profile.isPremium
        nameLabel.text = String(format: Strings.Welcome.namePlaceholder.localized, profile.firstName).uppercased()
        bottomMessage.text = isPremium ?
            Strings.Welcome.yourePremium.localized.uppercased() :
            Strings.Welcome.youreInvited.localized.uppercased()
        bottomMessage.textColor = isPremium ? .appTextGold : .white
        starIcon.isHidden = !profile.isPremium
    }
    
    private func startAutoHideTimer() {
        Timer.scheduledTimer(withTimeInterval: timeIntervalToHide, repeats: false) { _ in
            if self.backgroundImage.image == nil {
                self.animateFadeOut()
            }
        }
    }

}
