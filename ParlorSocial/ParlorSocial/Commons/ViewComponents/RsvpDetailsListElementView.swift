//
//  RsvpDetailsListElementView.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 13/05/2019.
//

import UIKit

class RsvpDetailsListElementView: UIView {
    
    // MARK: - Properties
    private(set) var host: ParlorHost?
    var selectionCallback: ((ParlorHost?) -> Void)?
    
    // MARK: - Views
    private(set) lazy var mainLabelLeftIconView: UIView = {
        return UIView().manualLayoutable()
    }()
    
    private(set) lazy var mainLabel: ParlorLabel = {
        return ParlorLabel.styled(
            withTextColor: UIColor.appTextBright,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.subbody,
                font: UIFont.Roboto.regular),
            alignment: .left,
            numberOfLines: 0)
            .apply {
                $0.lineHeight = 20
        }
    }()
    
    private(set) lazy var leftIconImageView: UIImageView = {
        return UIImageView().manualLayoutable().apply {
            $0.contentMode = .top
            $0.setContentHuggingPriority(.required, for: .horizontal)
            $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        }
    }()
    
    private(set) var rightIconImageView: UIImageView = {
        return UIImageView(image: Icons.RsvpDetails.rightArrow.scale(to: CGSize(width: 16, height: 27)))
            .manualLayoutable().apply {
                $0.contentMode = .center
                $0.setContentHuggingPriority(.required, for: .horizontal)
                $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        }
    }()
    
    private(set) var rightLabel: UILabel = {
        return UILabel.styled(textColor: UIColor.appTextSemiLight,
                       withFont: UIFont.custom(
                        ofSize: Constants.FontSize.xTiny,
                        font: UIFont.Roboto.regular),
                       alignment: .right).apply {
                        $0.isHidden = true
        }
    }()
    
    private(set) var horizontalStackView: UIStackView = {
        return UIStackView(axis: .horizontal, spacing: 16, alignment: .center, distribution: .fill).apply {
            $0.layoutMargins = UIEdgeInsets(top: 3, left: 0, bottom: 0, right: 0)
            $0.isLayoutMarginsRelativeArrangement = true
        }
    }()

    // MARK: - Initialization
    init() {
        super.init(frame: .zero)
        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

// MARK: - Public functions
extension RsvpDetailsListElementView {
    
    func setup(with data: RsvpDetailsListElementModel) {
        host = data.host
        if let callback = data.tapCallback, data.isClickable {
            selectionCallback = callback
            self.applyTouchAnimation()
        }

        rightIconImageView.isHidden = !data.isClickable
        rightLabel.isHidden = data.isClickable
        rightLabel.text = data.rightText
        
        leftIconImageView.image = data.leftIcon
        mainLabel.text = data.text
    }
    
}

// MARK: - Private functions
extension RsvpDetailsListElementView {

    private func initialize() {
        self.backgroundColor = UIColor.appBackground
        self.isUserInteractionEnabled = true
        self.setupTapGestureRecognizer(target: self, action: #selector(didTapView))
        
        layoutViews()
    }
    
    private func layoutViews() {
        self.addSubview(horizontalStackView)
        
        horizontalStackView.apply {
            $0.addArrangedSubviews([mainLabelLeftIconView, rightIconImageView, rightLabel])
            $0.edgesToParent()
        }
        
        mainLabelLeftIconView.addSubviews([leftIconImageView, mainLabel])
        
        mainLabel.apply {
            $0.edgesToParent(anchors: [.top, .trailing, .bottom], insets: UIEdgeInsets.margins(top: -4))
            $0.leadingAnchor.equal(to: leftIconImageView.trailingAnchor, constant: 16)
        }
        
        leftIconImageView.apply {
            $0.edgesToParent(anchors: [.top, .leading], insets: UIEdgeInsets.margins(top: -2))
            $0.widthAnchor.equalTo(constant: Constants.RsvpDetailsListElementView.leftIconSize)
            $0.heightAnchor.equalTo(constant: Constants.RsvpDetailsListElementView.leftIconSize)
        }
    }
    
    @objc private func didTapView() {
        if let callback = selectionCallback {
            callback(host)
        }
    }
    
}

// MARK: - RsvpDetailsListElementModel
struct RsvpDetailsListElementModel {
    let leftIcon: UIImage?
    let host: ParlorHost?
    let text: String?
    var tapCallback: ((ParlorHost?) -> Void)?
    var isClickable: Bool
    let rightText: String?
}

// MARK: - Constants

extension Constants {
    enum RsvpDetailsListElementView {
        static let leftIconSize: CGFloat = 19.0
    }
}
