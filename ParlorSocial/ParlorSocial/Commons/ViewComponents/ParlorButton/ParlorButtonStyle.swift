//
//  ParlorButtonStyle.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 27/04/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

struct ParlorButtonStyleAppearance {
    let backgroundColor: UIColor
    let titleColor: UIColor
    let selectedTitleColor: UIColor
    let font: UIFont
    let disabledStateAlpha: CGFloat = 0.5
    let letterSpacing: CGFloat
    let borderWidth: CGFloat
    let borderColor: UIColor
}

enum ParlorButtonStyle {
    case plain(titleColor: UIColor, font: UIFont, letterSpacing: CGFloat)
    case selectable
    case white(font: UIFont)
    case outlined(titleColor: UIColor, borderColor: UIColor, font: UIFont, letterSpacing: CGFloat)
    case filledWith(color: UIColor, titleColor: UIColor, font: UIFont, letterSpacing: CGFloat)

    var appearance: ParlorButtonStyleAppearance {
        switch self {
        case .plain(let titleColor, let font, let letterSpacing):
            return ParlorButtonStyleAppearance(
                backgroundColor: UIColor.clear,
                titleColor: titleColor,
                selectedTitleColor: titleColor,
                font: font,
                letterSpacing: letterSpacing,
                borderWidth: 0,
                borderColor: UIColor.clear
            )
        case .selectable:
            return ParlorButtonStyleAppearance(
                backgroundColor: UIColor.clear,
                titleColor: UIColor.closeTintColor,
                selectedTitleColor: UIColor.black,
                font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.bold),
                letterSpacing: Constants.LetterSpacing.small,
                borderWidth: 0,
                borderColor: UIColor.clear
            )
        case .white(let font):
            return ParlorButtonStyleAppearance(
                backgroundColor: UIColor.white,
                titleColor: UIColor.appText,
                selectedTitleColor: UIColor.appText,
                font: font,
                letterSpacing: Constants.LetterSpacing.none,
                borderWidth: 0,
                borderColor: UIColor.clear
            )
        case .outlined(let titleColor, let borderColor, let font, let letterSpacing):
            return ParlorButtonStyleAppearance(
                backgroundColor: .clear,
                titleColor: titleColor,
                selectedTitleColor: titleColor,
                font: font,
                letterSpacing: letterSpacing,
                borderWidth: 1,
                borderColor: borderColor
            )
        case .filledWith(let color, let titleColor, let font, let letterSpacing):
            return ParlorButtonStyleAppearance(
                backgroundColor: color,
                titleColor: titleColor,
                selectedTitleColor: titleColor,
                font: font,
                letterSpacing: letterSpacing,
                borderWidth: 0,
                borderColor: UIColor.clear
            )
        }
    }
}
