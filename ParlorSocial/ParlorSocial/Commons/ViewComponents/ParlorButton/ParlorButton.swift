//
//  ParlorButton.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 27/04/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class ParlorButton: TrackableButton {
    var title: String? {
        get { return title(for: []) }
        set {
            guard let appearance = style?.appearance else { return setTitle(newValue, for: []) }

            newValue.map {
                let attributes: [NSAttributedString.Key: Any] = [.kern: appearance.letterSpacing]
                let normalAttributes = attributes.merging([.foregroundColor: appearance.titleColor])
                let selectedAttributes = attributes.merging([.foregroundColor: appearance.selectedTitleColor])

                setAttributedTitle(NSAttributedString(string: $0, attributes: normalAttributes), for: [])
                setAttributedTitle(NSAttributedString(string: $0, attributes: selectedAttributes), for: .selected)
            }
        }
    }

    override var isEnabled: Bool {
        didSet {
            guard let disabledAlpha = style?.appearance.disabledStateAlpha else { return }

            let targetAlpha = isEnabled ? 1.0 : disabledAlpha

            UIView.animate(
                withDuration: 0.1,
                animations: {
                    self.alpha = targetAlpha
            },
                completion: { _ in // We set alpha again on completion to make sure alpha state is correct even if the animation was interrupted
                    self.alpha = targetAlpha
            }
            )
        }
    }

    var style: ParlorButtonStyle? {
        didSet { setupStyle() }
    }

    convenience init(style: ParlorButtonStyle) {
        defer { self.style = style }

        self.init()
    }

    func resetLayout() {
        setImage(nil, for: .normal)
        titleEdgeInsets = .zero
        imageEdgeInsets = .zero
        semanticContentAttribute = .forceLeftToRight
    }

    private func setupStyle() {
        guard let appearance = style?.appearance else { return }

        backgroundColor = appearance.backgroundColor
        titleLabel?.font = appearance.font
        layer.borderColor = appearance.borderColor.cgColor
        layer.borderWidth = appearance.borderWidth
    }
}
