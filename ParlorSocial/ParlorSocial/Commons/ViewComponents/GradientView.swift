//
//  GradientView.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 26/03/2019.
//

import UIKit

class GradientView: UIView {
    
    private lazy var gradient: CAGradientLayer = {
        let gradient = CAGradientLayer()
        
        gradient.colors = [UIColor.darkBackground.cgColor,
                           UIColor.darkBackground.withAlphaComponent(0.7).cgColor,
                           UIColor.clear.cgColor]
        
        gradient.locations = [0.0, 0.5, 1.0]
        gradient.startPoint = CGPoint(x: 0, y: 1)
        gradient.endPoint = CGPoint(x: 0, y: 0)
        gradient.frame = self.frame
        
        return gradient
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        gradient.frame = self.bounds
    }
}

extension GradientView {
    
    private func initialize() {
        manualLayoutable()
        layer.insertSublayer(gradient, at: 0)
    }
    
}
