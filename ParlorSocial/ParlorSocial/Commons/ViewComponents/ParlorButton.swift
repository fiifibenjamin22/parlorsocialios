//
//  ParlorButton.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 27/04/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class ParlorButton: UIButton {
    var title: String? {
        get { return title(for: []) }
        set {
            guard let appearance = style?.appearance else { return setTitle(newValue, for: []) }

            newValue.map {
                let attributes: [NSAttributedString.Key: Any] = [.kern: appearance.letterSpacing]
                let normalAttributes = attributes.merging([.foregroundColor: appearance.titleColor])
                let selectedAttributes = attributes.merging([.foregroundColor: appearance.selectedTitleColor])

                setAttributedTitle(NSAttributedString(string: $0, attributes: normalAttributes), for: [])
                setAttributedTitle(NSAttributedString(string: $0, attributes: selectedAttributes), for: .selected)
            }
        }
    }

    var style: ParlorButtonStyle? {
        didSet { setupStyle() }
    }

    convenience init(style: ParlorButtonStyle) {
        defer { self.style = style }

        self.init()
    }

    private func setupStyle() {
        guard let appearance = style?.appearance else { return }

        backgroundColor = appearance.backgroundColor
        titleLabel?.font = appearance.font
        layer.borderColor = appearance.borderColor.cgColor
        layer.borderWidth = appearance.borderWidth
    }
}
