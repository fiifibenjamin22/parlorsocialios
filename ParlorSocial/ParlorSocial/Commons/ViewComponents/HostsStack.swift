//
//  HostsStack.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 15/04/2019.
//

import UIKit

protocol HostStackDelegate: class {
    func hostsStack(_ hostStack: HostsStack, didSelectHost host: ParlorHost)
}

class HostsStack: UIStackView {
    
    weak var delegate: HostStackDelegate?
    
    private lazy var selectionCallback: ((ParlorHost) -> Void) = { [unowned self] host in
        self.delegate?.hostsStack(self, didSelectHost: host)
    }

    init() {
        super.init(frame: .zero)
        initialize()
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        self.axis = .vertical
        self.distribution = .equalSpacing
        self.alignment = .fill
    }
    
    private func createStyledLabel(withText text: String) -> ParlorLabel {
        return ParlorLabel.styled(withTextColor: UIColor.eventTime, withFont: UIFont.custom(ofSize: Constants.FontSize.xTiny, font: UIFont.Roboto.medium), alignment: .left, numberOfLines: 1).apply {
            $0.insets.top = 30
            $0.insets.bottom = 19
            $0.text = text
        }
    }
    
    func populate(with allHosts: [ParlorHost]) {
        removeArrangedSubviews()
        let roles = [HostRole.parlorHost, HostRole.guestHost]
        roles.forEach { role in
            addSingleTypeHosts(type: role, hosts: allHosts
                .filter { $0.role.shouldBeCombinedWith(other: role) }
                .sorted(by: { $0.role.sortIndex < $1.role.sortIndex })
            )
        }
    }
    
    private func addSingleTypeHosts(type: HostRole, hosts: [ParlorHost]) {
        guard !hosts.isEmpty else { return }
        var displayName = type.displayName.uppercased()
        displayName = hosts.count > 1 ? "\(displayName)S:" : "\(displayName):"
        let label = createStyledLabel(withText: displayName)
        let parlorViews = hosts.map { host in HostView().apply {
            $0.host = host
            $0.selectionCallback = selectionCallback
            }
        }
        addArrangedSubviews([label] + parlorViews.embedInContainersIfNeeded())
    }

}

private extension Array where Element == HostView {
    
    func embedInContainersIfNeeded() -> [UIView] {
        guard count > 1, let firstElement = first else { return self }
        let tail = Array(self.dropFirst())
        let embeddedViews = tail.map { hostView -> UIView in
            let container = UIView()
            container.addSubview(hostView)
            hostView.manualLayoutable()
            hostView.edgesToParent(insets: UIEdgeInsets(top: -1, left: 0, bottom: 0, right: 0))
            return container
        }
        return [firstElement] + embeddedViews
    }
    
}
