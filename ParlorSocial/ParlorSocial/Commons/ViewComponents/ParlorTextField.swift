//
//  ParlorTextField.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 11/04/2019.
//

import Foundation
import UIKit

class ParlorTextField: UITextField {
    
    private var rightBorderView: UIView?
    private var leftBorderView: UIView?
    
    var maxCharacterCount: Int = .max
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return rect(forBounds: bounds)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return rect(forBounds: bounds)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return rect(forBounds: bounds)
    }
    
    var sideBorderColor: UIColor? {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0) {
        didSet {
            setNeedsLayout()
        }
    }
    
    var placeholderColor: UIColor? {
        didSet {
            updatePlaceholder()
        }
    }
    
    override var placeholder: String? {
        didSet {
            updatePlaceholder()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        addTarget(self, action: #selector(limitText), for: .editingChanged)
        tintColor = .textVeryLight
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateLayers()
    }
    
    private func updatePlaceholder() {
        guard let color = self.placeholderColor else { return }
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "", attributes: [
            .foregroundColor: color])
    }
    
    private func updateLayers() {
        leftBorderView?.removeFromSuperview()
        rightBorderView?.removeFromSuperview()
        guard let color = sideBorderColor else { return }
        leftBorderView = setupConstrainedView(withColor: color).apply { $0.leadingAnchor.equal(to: self.leadingAnchor) }
        rightBorderView = setupConstrainedView(withColor: color).apply { $0.trailingAnchor.equal(to: self.trailingAnchor) }
    }
    
    private func setupConstrainedView(withColor color: UIColor) -> UIView {
        return UIView().manualLayoutable().apply {
            self.addSubview($0)
            $0.backgroundColor = color
            $0.edgesToParent(anchors: [.top, .bottom])
            $0.widthAnchor.equalTo(constant: 1)
        }
    }
    
    private func rect(forBounds bounds: CGRect) -> CGRect {
        if let rightViewWidth = self.rightView?.frame.width {
            return bounds.inset(by: UIEdgeInsets(top: padding.top, left: padding.left, bottom: padding.bottom, right: rightViewWidth))
        }
        return bounds.inset(by: padding)
    }
    
    @objc private func limitText() {
        text = text?.safelyLimitedTo(length: maxCharacterCount)
    }
    
}
