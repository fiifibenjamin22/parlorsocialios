//
//  FriendsView.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 27/04/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

class FriendsView: UIView {
    private enum Constants {
        static let imageSpacing: CGFloat = 5
    }

    // MARK: - Views

    private var imageViews: [UIImageView] = []

    // MARK: - Initialization
    init(frame: CGRect = .zero, images: [UIImage]? = nil) {
        super.init(frame: frame)

        if let images = images { prepareToLayout(images) }

        updateConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func updateConstraints() {
        super.updateConstraints()

        setupInitialLayout()
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        imageViews.forEach { $0.layer.cornerRadius = $0.bounds.height / 2 }
    }

    // MARK: - Public methods
    func set(images: [UIImage]) {
        imageViews = []
        subviews.forEach { $0.removeFromSuperview() }
        prepareToLayout(images)
        updateConstraints()
    }

    func set(imageUrls: [URL]) {
        imageViews = []
        subviews.forEach { $0.removeFromSuperview() }
        prepareToLayout(imageUrls)
        updateConstraints()
    }

    // MARK: - Private methods

    private func prepareToLayout(_ images: [UIImage]) {
        for (index, image) in images.enumerated() {
            let imageView = buildCircularImageView(from: image).manualLayoutable()

            if imageViews.isEmpty {
                addSubview(imageView)
            } else {
                insertSubview(imageView, belowSubview: imageViews[imageViews.index(before: index)])
            }
            self.imageViews.append(imageView)
        }
    }

    private func prepareToLayout(_ urls: [URL]) {
        for (index, url) in urls.enumerated() {
            let imageView = buildCircularImageView(from: url).manualLayoutable()

            if imageViews.isEmpty {
                addSubview(imageView)
            } else {
                insertSubview(imageView, belowSubview: imageViews[imageViews.index(before: index)])
            }
            self.imageViews.append(imageView)
        }
    }

    private func setupInitialLayout() {
        imageViews.enumerated().forEach { index, imageView in
            imageView.centerYAnchor.equal(to: centerYAnchor)
            imageView.widthAnchor.equal(to: heightAnchor)
            imageView.heightAnchor.equal(to: imageView.widthAnchor)

            if index == 0 {
                imageView.leadingAnchor.equal(to: leadingAnchor)
            } else {
                let previousView = imageViews[index - 1]
                imageView.leadingAnchor.equal(to: previousView.centerXAnchor, constant: Constants.imageSpacing)

                if index == imageViews.endIndex - 1 {
                    imageView.trailingAnchor.equal(to: trailingAnchor)
                }
            }
        }
    }

    private func buildCircularImageView(from image: UIImage) -> UIImageView {
        let imageView = UIImageView(image: image)

        return imageView
    }

    private func buildCircularImageView(from url: URL) -> UIImageView {
        let imageView = UIImageView()
        imageView.getImage(from: url)
        imageView.clipsToBounds = true

        return imageView
    }
}
