//
//  FriendsListView.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 28/04/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

class FriendsListView: UIView {

    // MARK: - Views
    private lazy var friendsView: FriendsView = {
        return FriendsView().manualLayoutable()
    }()

    private lazy var comingAlongLabel: UILabel = {
        return UILabel.styled(
            textColor: UIColor.appGreyLight,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.xxTiny,
                font: UIFont.Roboto.regular
            ),
            alignment: .left
        ).manualLayoutable().apply {
            $0.numberOfLines = 2
        }
    }()

    // MARK: - Initalization
    override init(frame: CGRect = .zero) {
        super.init(frame: .zero)

        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public methods
    func set(images: [UIImage], title: String?) {
        comingAlongLabel.text = title
        friendsView.set(images: images)
    }

    func set(imageUrls: [URL], title: String?) {
        comingAlongLabel.text = title
        friendsView.set(imageUrls: imageUrls)
    }

    // MARK: - Private methods
    private func setup() {
        addSubviews([friendsView, comingAlongLabel])

        friendsView.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        friendsView.edgesToParent(anchors: [.top, .bottom, .leading])

        comingAlongLabel.apply {
            $0.centerYAnchor.equal(to: friendsView.centerYAnchor)
            $0.trailingAnchor.equal(to: trailingAnchor)
            $0.leadingAnchor.equal(to: friendsView.trailingAnchor, constant: 5.0)
        }
    }
}
