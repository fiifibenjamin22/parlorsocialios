//
//  RoundedImageView.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 23/05/2019.
//

import UIKit

class RoundedImageView: UIImageView {
    
    override init(image: UIImage? = nil) {
        super.init(image: image)
        initialize()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        self.clipsToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = min(self.bounds.height, self.bounds.width) / 2
    }
    
}
