//
//  AppDelegate.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 25/02/2019.
//

import UIKit
import Firebase
import RxSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    // MARK: - Properties
    var window: UIWindow?
    let disposeBag = DisposeBag()
    let updateNotificationsReminderDataManager: UpdateNotificationsReminderDataManager = .init()
    let incrementAppEntryCountHelper: IncrementAppEntryCountHelper = .init()
    let appUpdateChecker: AppUpdateRequirementChecker = .init()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let notificationOption = launchOptions?[.remoteNotification]
        let activityDictionary = launchOptions?[.userActivityDictionary] as? [AnyHashable: Any] 
        setupApp(notificationOption: notificationOption, activityDictionary: activityDictionary)
        return true
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        DeepLinksHandler.shared.handleDeepLinkCallback(for: userActivity, appState: application.applicationState)
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        appDidEnterBackground()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        appWillEnterForeground()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        appDidBecomeActive()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Config.deviceToken = deviceToken.map { data in String(format: "%02.2hhx", data) }.joined()
        GlobalNotificationsService.shared.registerDeviceToken()
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        Config.deviceToken = nil
        print("Failed to register: \(error)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        GlobalNotificationsService.shared.handlePushNotification(with: userInfo, appState: application.applicationState)
        completionHandler(.newData)
    }
}
