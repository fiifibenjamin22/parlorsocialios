//
//  GlobalNotificationsService.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 24/06/2019.
//

import Foundation
import RxSwift

/// *Manages push notifications*
class GlobalNotificationsService {
    // MARK: - Properties
    static let shared: GlobalNotificationsService = GlobalNotificationsService()
    private let notificationCenter: UNUserNotificationCenter
    private let authRepository = AuthRepository.shared
    private let disposeBag = DisposeBag()
    private let notificationsAnalyticService: NotificationsAnalyticService
    
    /// Stores push notification details when push notification is intercepted when the app is not active. It is used after the app becomes active.
    var storedNotificationData: PushNotificationData?
    
    // MARK: - Initialization
    private init(notificationsAnalyticService: NotificationsAnalyticService = NotificationsAnalyticService()) {
        self.notificationsAnalyticService = notificationsAnalyticService
        self.notificationCenter = UNUserNotificationCenter.current()
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(
            self,
            selector: #selector(handlePushNotificationWhenDidBecomeActive),
            name: UIApplication.didBecomeActiveNotification,
            object: nil
        )
    }
    
    func arePermissionsAuthorized(completionHandler: @escaping (Bool) -> Void) {
        notificationCenter.getNotificationSettings { settings in
            completionHandler(settings.authorizationStatus == .authorized)
        }
    }
    
    func configurePushNotifications(completionHandler: @escaping (Bool, AlertData?) -> Void) {
        notificationCenter.getNotificationSettings { [weak self] settings in
            switch settings.authorizationStatus {
            case .authorized:
                self?.setEnabledPushNotifications()
                completionHandler(true, nil)
            case .denied:
                completionHandler(false, AlertData.noNotificationsPermissions())
            case .notDetermined:
                self?.setPushNotificationsWhenNotDetermined(completionHandler: completionHandler)
            default:
                break
            }
        }
    }
    
    func setEnabledPushNotifications() {
        DispatchQueue.main.async {
            UIApplication.shared.registerForRemoteNotifications()
        }
        let photoCategory = UNNotificationCategory(identifier: "PhotoCategory", actions: [], intentIdentifiers: [], options: [])
        notificationCenter.setNotificationCategories(Set([photoCategory]))
    }
    
    /**
     *Sends device token to API*
     
     Method gets device token from Config class, gets langauge code and sends them to API. Method is called when getting device token is available.
     */
    func registerDeviceToken() {
        guard let deviceToken = Config.deviceToken,
            let language = Locale.current.languageCode else { return }
        let request = RegisterDeviceRequest(token: deviceToken, language: language)
        authRepository.registerDevice(using: request)
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    /**
     *Deletes device token from database in API*
     
     - parameter completionHandler: Callback that is called after success or fail unregistering.
     
     Method is called when user logs out.
     */
    func unregisterDeviceToken(completionHandler: @escaping (Bool) -> Void ) {
        guard let deviceToken = Config.deviceToken else {
            completionHandler(false)
            return
        }
        let request = UnregisterDeviceRequest(deviceToken: deviceToken)
        authRepository.unregisterDevice(using: request)
            .subscribe(onNext: { response in
                switch response {
                case .success:
                    completionHandler(true)
                case .failure(let error):
                    completionHandler(false)
                    print(error)
                }
            }).disposed(by: disposeBag)
    }
    
    /**
     *Intercepts push notification and chooses method depending on application state is active or not.*
     
     - parameter notificationOption: A dictionary that contains information related to the remote notification, potentially including a badge number for the app icon, an alert sound, an alert message to display to the user, a notification identifier, and custom data. The provider originates it as a JSON-defined dictionary that iOS converts to an NSDictionary object; the dictionary may contain only property-list objects plus NSNull. For more information about the contents of the remote notification dictionary, see Generating a Remote Notification.
     - parameter appState: The running state of an app
     
     Method shows notification alert when app state is active or update `currentNotificationDetail` that will be intercept when the application will be active.
     */
    func handlePushNotification(with notificationOption: [AnyHashable: Any], appState: UIApplication.State) {
        guard let notificationData = getNotificationDetails(notificationOption: notificationOption),
            let topViewController = UIApplication.shared.windows.first?.rootViewController?.top,
            let topAppViewController = topViewController as? AppViewController else { return }
        if appState == .active {
            showNotificationAlert(notificationData: notificationData, topAppViewController: topAppViewController)
        } else {
            storedNotificationData = notificationData
        }
    }
    
    // MARK: - Private methods
    private func setPushNotificationsWhenNotDetermined(completionHandler: @escaping (Bool, AlertData?) -> Void) {
        notificationCenter.requestAuthorization(options: [.alert, .sound, .badge]) { [weak self] isGranted, _ in
            if isGranted {
                self?.setEnabledPushNotifications()
                completionHandler(true, nil)
            } else {
                completionHandler(false, nil)
            }
        }
    }
    
    /**
     *Gets notification details from notification option*
     
     - parameter notificationOption: A dictionary that contains information related to the remote notification, potentially including a badge number for the app icon, an alert sound, an alert message to display to the user, a notification identifier, and custom data. The provider originates it as a JSON-defined dictionary that iOS converts to an NSDictionary object; the dictionary may contain only property-list objects plus NSNull. For more information about the contents of the remote notification dictionary, see Generating a Remote Notification.
     
     - returns: It returns `PushNotificationData` object created from `notificationOption`.
     
     Method decodes `userInfo` received from `AppDelegate` and returns `PushNotificationData` object.
     */
    private func getNotificationDetails(notificationOption: [AnyHashable: Any]) -> PushNotificationData? {
        guard let json = notificationOption as? JSON else { return nil }
        guard let detail = try? PushNotificationDetail.make(from: json) else { return nil }
        return PushNotificationData(from: detail)
    }
    
    /**
    *Shows notification alert when app is active and gets push notification*
    
    - parameter notificationData: `PushNotificationData` object created from `notificationOption`.
    - parameter topAppViewController: `AppViewController` object.

    Method shows notification alert when app is opened and gets push notification.
    When you add new destination, you should save opened event when click ok button on alert. See `NotificationAnalyticService` class.
    */
    private func showNotificationAlert(notificationData: PushNotificationData, topAppViewController: AppViewController) {
        guard let destinationId = notificationData.destinationId,
            let destination = notificationData.destination else { return }
        
        notificationsAnalyticService.savePushNotificationOpenedEvent(id: notificationData.id)
        notifyAboutNewActivationIfNeeded(forDestination: destination)
        
        switch destination {
        case .transferFromWaitlist:
            topAppViewController.showAlertFromPushNotification(basedOn: notificationData) { [weak topAppViewController] in
                topAppViewController?.showVCAfterPushNotificationOrLinkClicked(
                    for: AppViewControllerDestination.waitListInfo(activationId: destinationId))
            }
        case .guestListAvailable:
            topAppViewController.showAvailableGuestListPopup(activationId: destinationId, showGuestListWhenAvailable: false)
        case .announcement:
            AnnouncementsRepository.shared.setUnreadAnnouncementsSubject(true)
        case .rsvpOpen:
            let openingType = ActivationDetailsNavigationProvider.getOpeningActivationDetailsType()
            topAppViewController.showAlertFromPushNotification(
            basedOn: notificationData, showMessageInTitle: true) { [weak topAppViewController] in
                let controller = AppViewControllerDestination.activationDetails(activationId: destinationId, openingType: openingType, navigationSource: .pushNotification)
                topAppViewController?.showVCAfterPushNotificationOrLinkClicked(for: controller)
            }
        case .newActivationPublished, .upcomingActivationReminder:
            let openingType = ActivationDetailsNavigationProvider.getOpeningActivationDetailsType()
            topAppViewController.showAlertFromPushNotification(basedOn: notificationData) { [weak topAppViewController] in
                topAppViewController?.showVCAfterPushNotificationOrLinkClicked(
                    for: AppViewControllerDestination.activationDetails(activationId: destinationId, openingType: openingType, navigationSource: .pushNotification))
            }
        case .previewActivateMembership, .complimentaryMembershipExpirationWarning:
            topAppViewController.showAlertFromPushNotification(basedOn: notificationData) { [weak topAppViewController] in
                topAppViewController?.showVCAfterPushNotificationOrLinkClicked(
                    for: AppViewControllerDestination.membershipPlans)
            }
        case .premiumMembershipAccepted:
            break // No action needed
        case .memberApplicationApproved:
            let userId = destinationId
            guard Config.userProfile?.id == userId else { return }
            topAppViewController.showAlertFromPushNotification(basedOn: notificationData) { [weak self] in
                self?.replaceCurrentScreenAfterUserApprove()
            }
        }
    }
    
    /**
    *Shows destination view controller when app is not active and gets push notification*
    
    - parameter notificationDetails: `PushNotificationData` object created from `notificationOption`.
    - parameter topAppViewController: `AppViewController` object.

    Method shows destination view controller when app is opened and gets push notification.
    When you add new destination, you should save opened event to analytic. See `NotificationAnalyticService` class.
    */
    private func showNotificationDestination(notificationData: PushNotificationData, topAppViewController: AppViewController) {
        guard KeychainWorker().userToken != nil else {
            if let app = UIApplication.shared.delegate as? AppDelegate,
                let window = app.window {
                window.rootViewController = LoginViewController().embedInNavigationController()
                window.makeKeyAndVisible()
            }
            return
        }
        guard let destinationId = notificationData.destinationId,
            let destination = notificationData.destination else { return }
        
        notificationsAnalyticService.savePushNotificationOpenedEvent(id: notificationData.id)
        notifyAboutNewActivationIfNeeded(forDestination: destination)

        switch destination {
        case .transferFromWaitlist:
            topAppViewController.showVCAfterPushNotificationOrLinkClicked(
                for: AppViewControllerDestination.waitListInfo(activationId: destinationId))
        case .guestListAvailable:
            topAppViewController.showAvailableGuestListPopup(
                activationId: destinationId, showGuestListWhenAvailable: true)
        case .announcement:
            topAppViewController.showVCAfterPushNotificationOrLinkClicked(for: .announcementDetails(announcementId: destinationId))
        case .newActivationPublished, .upcomingActivationReminder, .rsvpOpen:
            let openingType = ActivationDetailsNavigationProvider.getOpeningActivationDetailsType()
            let controller = AppViewControllerDestination.activationDetails(activationId: destinationId, openingType: openingType, navigationSource: .pushNotification)
            topAppViewController.showVCAfterPushNotificationOrLinkClicked(for: controller)
        case .previewActivateMembership, .complimentaryMembershipExpirationWarning:
            topAppViewController.showVCAfterPushNotificationOrLinkClicked(for: AppViewControllerDestination.membershipPlans)
        case .premiumMembershipAccepted:
            break
        case .memberApplicationApproved:
            break // app should redirect to dashboard after user profile refresh when become active
        }
    }
    
    /**
     *Intercepts push notification stored in `storedNotificationDetail` when application becomes active.*
     
     Method is called when app was not active and was opened by click on push notification. The application waits until it becomes active. After that it moves to specific view controller.
     */
    @objc private func handlePushNotificationWhenDidBecomeActive() {
        guard let data = storedNotificationData,
            let destination = data.destination,
            let topViewController = UIApplication.shared.windows.first?.rootViewController?.top,
            let topAppViewController = topViewController as? AppViewController else { return }
        switch destination {
        default:
            showNotificationDestination(notificationData: data, topAppViewController: topAppViewController)
        }
        storedNotificationData = nil
    }
    
    /// Replace current screen for logged in user when their application has approved
    private func replaceCurrentScreenAfterUserApprove() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate, let window = appDelegate.window else { return }
        window.rootViewController = AppTabBarController().embedInNavigationController()
    }
    
    private func notifyAboutNewActivationIfNeeded(forDestination destination: NotificationDestination) {
        guard destination == .newActivationPublished else { return }
        HomeRepository.shared.newActivationsNotificationSubject.onNext(true)
    }
}
