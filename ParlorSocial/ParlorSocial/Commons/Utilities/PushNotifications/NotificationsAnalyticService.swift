//
//  NotificationsAnalyticService.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 17/03/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

class NotificationsAnalyticService {
    /**
    *Save enable notifications settings in user defaults*
    
    - parameter checkIfChanged: Set true if you want to save enable notifications settings only if value is another than the late.
    */
    func saveNotificationsSettingsChangeAnalyticEvent(withCheckingIfChanged checkIfChanged: Bool, source: NotificationsSettingsChangedEventSource) {
        guard let sessionId = AnalyticService.shared.sessionId,
            let userId = Config.userProfile?.id else { return }
        GlobalNotificationsService.shared.arePermissionsAuthorized { isGranted in
            let latestNotificationsEnableSettings = NotificationsConfig.latestNotificationsEnableSettings
            guard latestNotificationsEnableSettings != isGranted || !checkIfChanged else { return }
            let event = AnalyticEvent(
                userId: userId,
                deviceToken: Config.deviceToken,
                timestamp: Int64(Date().timeIntervalSince1970 * 1000),
                type: .notificationsSettingsChanged,
                data: .createForSpecialType(isEnabled: isGranted, source: source.rawValue),
                sessionId: sessionId
            )
            AnalyticService.shared.saveEventInDatabase(event)
            NotificationsConfig.latestNotificationsEnableSettings = isGranted
        }
    }
    
    func savePushNotificationOpenedEvent(id: Int?) {
        guard let sessionId = AnalyticService.shared.sessionId,
            let userId = Config.userProfile?.id,
            let pushNotificationId = id else { return }
        let event = AnalyticEvent(
            userId: userId,
            deviceToken: Config.deviceToken,
            timestamp: Int64(Date().timeIntervalSince1970 * 1000),
            type: .pushNotificationOpened,
            data: .createForSpecialType(referencedId: pushNotificationId),
            sessionId: sessionId
        )
        AnalyticService.shared.saveEventInDatabase(event)
    }
}
