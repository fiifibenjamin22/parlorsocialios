//
//  UpdateNotificationsReminderDataManager.swift
//  ParlorSocialTests
//
//  Created by Benjamin Acquah on 11/03/2020.
//

import Foundation

final class UpdateNotificationsReminderDataManager {
    // MARK: - Properties
    private let profileRepository: ProfileRepositoryProtocol
    private let notificationsAnalyticService: NotificationsAnalyticService
    
    // MARK: - Initialization
    init(profileRepository: ProfileRepositoryProtocol = ProfileRepository.shared,
         notificationsAnalyticService: NotificationsAnalyticService = NotificationsAnalyticService()) {
        self.profileRepository = profileRepository
        self.notificationsAnalyticService = notificationsAnalyticService
    }
    
    func updateDataOnEnableNotificationsReminderVC(isGranted: Bool, notificationsSettingsChangeSource: NotificationsSettingsChangedEventSource) {
        if isGranted {
            NotificationsConfig.enableNotificationsRemindLevel = .zero
            resetCounters()
        } else {
            NotificationsConfig.lastRejectNotificationsPermissionsDate = Date()
            incrementEnableNotificationsRemindLevel()
        }
        notificationsAnalyticService.saveNotificationsSettingsChangeAnalyticEvent(withCheckingIfChanged: true, source: notificationsSettingsChangeSource)
        printUpdatingData()
    }
    
    func updateDataOnDidBecomeActive() {
        profileRepository.getEnableNotificationsSettingsRemindModel { [weak self] model in
            guard NotificationsConfig.latestNotificationsEnableSettings != (model.notificationsPermissions == .authorized) else { return }
            if model.notificationsPermissions == .denied && Config.userProfile?.type != .notApproved {
                NotificationsConfig.lastRejectNotificationsPermissionsDate = Date()
                self?.incrementEnableNotificationsRemindLevel()
            } else if model.notificationsPermissions == .authorized {
                NotificationsConfig.enableNotificationsRemindLevel = .zero
                self?.resetCounters()
                self?.enableNotificationsIfUserLoggedIn()
            }
            self?.notificationsAnalyticService.saveNotificationsSettingsChangeAnalyticEvent(withCheckingIfChanged: true, source: .manual)
            self?.printUpdatingData()
        }
    }
    
    // MARK: - Private methods
    private func incrementEnableNotificationsRemindLevel() {
        let remindLevel = NotificationsConfig.enableNotificationsRemindLevel ?? .zero
        guard remindLevel != .third else { return }
        NotificationsConfig.enableNotificationsRemindLevel = EnableNotificationsSettingsRemindLevel(rawValue: remindLevel.rawValue + 1)
    }
    
    private func resetCounters() {
        NotificationsConfig.appEntryCount = 0
        NotificationsConfig.rsvpsCount = 0
    }
    
    private func enableNotificationsIfUserLoggedIn() {
        guard Config.userProfile != nil else { return }
        GlobalNotificationsService.shared.setEnabledPushNotifications()
    }
    
    // MARK: - Debug methods
    
    private func printUpdatingData() {
        #if DEBUG
        print("NOTIFICATIONS_ENABLE_SETTINGS: \(NotificationsConfig.latestNotificationsEnableSettings)")
        print("LAST_REJECT_NOTIFICATIONS_PERMISSIONS_DATE: \(String(describing: NotificationsConfig.lastRejectNotificationsPermissionsDate))")
        print("ENABLE_NOTIFICATIONS_REMIND_LEVEL: \(String(describing: NotificationsConfig.enableNotificationsRemindLevel))")
        #endif
    }
}
