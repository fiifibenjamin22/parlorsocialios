//
//  MapRouter.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 14/06/2019.
//

import Foundation
import UIKit

/// *Manages routing to choosed map.*
final class RouteMapService {
    
    /**
     *Deliver map app deep link*
     
     - parameter lat: Latitude
     - parameter lng: Longitude
     - parameter address: Address of searched place.
     - returns: Deep link to specified map app.
     */
    static func getMapUrl(lat: Double, lng: Double, address: String) -> URL? {
        guard let chosenMapAppString = Config.chosenMapApp else { return nil }
        let chosenMapEnum = MapProviderEnum(stringUrl: chosenMapAppString)
        guard let chosenMapUrl = URL(string: chosenMapEnum.appUrlString),
            UIApplication.shared.canOpenURL(chosenMapUrl),
            let encodedAddress = address.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return nil }
        
        var mapUrl: URL?
        switch chosenMapEnum {
        case .appleMap, .googleMap:
            mapUrl = URL(string: "\(chosenMapEnum.appUrlString)?q=\(encodedAddress)&ll=\(lat),\(lng)")
        case .mapQuest:
            mapUrl = URL(string: "https://www.mapquest.com/latlng/\(lat),\(lng)")
        case .wazeMap:
            mapUrl = URL(string: "https://waze.com/ul?ll=\(lat),\(lng)")
        }
        return mapUrl
    }
    
}

enum MapProviderEnum: CaseIterable {
    case appleMap
    case googleMap
    case wazeMap
    case mapQuest
}

extension MapProviderEnum {
    init(stringUrl: String) {
        switch stringUrl {
        case Strings.MapProvider.appleMapUrl.localized:
            self = .appleMap
        case Strings.MapProvider.googleMapUrl.localized:
            self = .googleMap
        case Strings.MapProvider.wazeMapUrl.localized:
            self = .wazeMap
        case Strings.MapProvider.mapQuestUrl.localized:
            self = .mapQuest
        default:
            self = .appleMap
        }
    }
    
    var appName: String {
        switch self {
        case .appleMap:
            return Strings.MapProvider.appleMapAppName.localized
        case .googleMap:
            return Strings.MapProvider.googleMapAppName.localized
        case .wazeMap:
            return Strings.MapProvider.wazeMapAppName.localized
        case .mapQuest:
            return Strings.MapProvider.mapQuestAppName.localized
        }
    }
    
    var appUrlString: String {
        switch self {
        case .appleMap:
            return Strings.MapProvider.appleMapUrl.localized
        case .googleMap:
            return Strings.MapProvider.googleMapUrl.localized
        case .wazeMap:
            return Strings.MapProvider.wazeMapUrl.localized
        case .mapQuest:
            return Strings.MapProvider.mapQuestUrl.localized
        }
    }

}

