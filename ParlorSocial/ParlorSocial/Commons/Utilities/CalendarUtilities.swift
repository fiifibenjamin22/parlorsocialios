//
//  CalendarUtilities.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 22/07/2019.
//

import Foundation
import EventKit
import RxSwift

class CalendarUtilities {
    
    static let shared: CalendarUtilities = CalendarUtilities()
    
    private let eventStore = EKEventStore()
    /// The subject that emmits when calendar sync status is changed.
    private let isCalendarSyncEnabledRelay: BehaviorSubject<Bool> = BehaviorSubject(value: isSyncOn)
    
    private init() {}
    
    /// The flag that informs about permissions to the calendar access.
    static var arePermissionsRejected: Bool {
        let status = EKEventStore.authorizationStatus(for: .event)
        return status == .restricted || status == .denied
    }
    
    static var arePermissionsAuthorized: Bool {
        let status = EKEventStore.authorizationStatus(for: .event)
        return status == .authorized
    }
    
    /// The flag that informs about calendar sync is enabled or disabled.
    static var isSyncOn: Bool {
        return CalendarSyncConfig.isSyncEnabled == true
    }
    
    /// The observable that emmits an information about calendar sync is enabled or disabled.
    var isCalendarSyncEnabledObs: Observable<Bool> {
        return isCalendarSyncEnabledRelay.asObservable()
    }
    
    /**
     *Asks for calendar permissions*
     
     - parameter callback: Method that is called after getting perrmissions.
     */
    func requestPermissions(_ callback: @escaping (Bool) -> Void) {
        eventStore.requestAccess(to: .event) { isGranted, error in
            DispatchQueue.main.async {
                callback(isGranted && error == nil)
            }
        }
    }
    
    /**
     *Adds rsvp to calendar.*
     
     - parameter rsvp: An reservation of activation
     - parameter activation: An activation that user want to reservation for.
     */
    func addRsvp(_ rsvp: Rsvp, forActivation activation: Activation) {
        eventStore.requestAccess(to: .event) { (granted, error) in
            guard granted && error == nil,
                let selectedSegment = activation.segments.first(where: { $0.id == rsvp.timeSegmentId })
                else { return }
            
            let event = EKEvent(eventStore: self.eventStore).apply {
                $0.title = String(format: Strings.Calendar.titleForSystemCalendar.localized, activation.name)
                $0.startDate = selectedSegment.startAt
                $0.endDate = selectedSegment.endAt
                $0.calendar = self.eventStore.defaultCalendarForNewEvents
                
                guard let locationTitle = activation.location?.detailedOneLineLocation,
                    let lat = activation.location?.lat,
                    let lng = activation.location?.lng else { return }
                $0.structuredLocation = EKStructuredLocation(title: locationTitle)
                $0.structuredLocation?.geoLocation = CLLocation(latitude: lat, longitude: lng)
            }
            do {
                try self.eventStore.save(event, span: .thisEvent)
            } catch {
                print("failed to save event with error : \(error)")
            }
        }
    }
    
    /**
     *Changes calendar sync state*
     
     - parameter isEnabled: Calendar sync state can be enabled or disabled.
     - parameter callback: Callback that is called after calendar sync is changed.
     */
    func changeSyncState(isEnabled: Bool, changeSource: CalendarSyncChangedEventSource, callback: @escaping (Bool, AlertData?) -> Void) {
        if !isEnabled {
            setCalendarSyncDisabledWithRejectionDateUpdate()
            callback(false, nil)
            self.updateRelay(changeSource: changeSource)
        } else if CalendarUtilities.arePermissionsRejected {
            setCalendarSyncDisabledWithRejectionDateUpdate()
            callback(false, AlertData.noCalendarPermissions())
            self.updateRelay(changeSource: changeSource)
        } else {
            CalendarUtilities.shared.requestPermissions { [weak self] isGranted in
                if isGranted {
                    CalendarSyncConfig.isSyncEnabled = true
                    callback(true, nil)
                } else {
                    self?.setCalendarSyncDisabledWithRejectionDateUpdate()
                    callback(false, nil)
                }
                self?.updateRelay(changeSource: changeSource)
            }
        }
    }
    
    func checkIfPermissionsAreChangedInDeviceSetting(settingsChangeSource: CalendarSyncChangedEventSource) {
        guard let isSyncEnabled = CalendarSyncConfig.isSyncEnabled else {
            return
        }
        guard isSyncEnabled, CalendarUtilities.arePermissionsRejected else { return }
        CalendarSyncConfig.isSyncEnabled = false
        self.updateRelay(changeSource: settingsChangeSource)
    }
    
    private func setFirstPermissionRejectionDateIfNeeded() {
        guard CalendarSyncConfig.firstRejectCalendarSyncPermissionsDate == nil else { return }
        CalendarSyncConfig.firstRejectCalendarSyncPermissionsDate = Date()
    }
    
    private func updateRelay(changeSource: CalendarSyncChangedEventSource) {
        let isSyncOn = CalendarUtilities.isSyncOn
        saveCalendarSyncAnalyticEvent(isSyncOn: isSyncOn, changeSource: changeSource)
        self.isCalendarSyncEnabledRelay.onNext(isSyncOn)
    }
    
    private func setCalendarSyncDisabledWithRejectionDateUpdate() {
        CalendarSyncConfig.isSyncEnabled = false
        setFirstPermissionRejectionDateIfNeeded()
    }
    
    private func saveCalendarSyncAnalyticEvent(isSyncOn: Bool, changeSource: CalendarSyncChangedEventSource) {
        guard let sessionId = AnalyticService.shared.sessionId,
            let userId = Config.userProfile?.id else { return }
        let event = AnalyticEvent(
            userId: userId,
            deviceToken: Config.deviceToken,
            timestamp: Int64(Date().timeIntervalSince1970 * 1000),
            type: .calendarSyncSettingsChanged,
            data: .createForSpecialType(isEnabled: isSyncOn, source: changeSource.rawValue),
            sessionId: sessionId
        )
        AnalyticService.shared.saveEventInDatabase(event)
    }
}
