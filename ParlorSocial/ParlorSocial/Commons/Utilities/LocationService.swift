//
//  LocationService.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 05/06/2019.
//

import MapKit
import CoreLocation

protocol LocationServiceDelegate: class {
    func didGetLocationError(errorType: GetLocationErrorType)
    func didGetCoordinate(coordinate: CLLocationCoordinate2D)
}

/// *Helps to manage operations on location. Delivers coordinates or returns any errors.*
class LocationService: NSObject {
    // MARK: - Properties
    weak var delegate: LocationServiceDelegate?
    private let locationManager = CLLocationManager()
    private var locationTrackingType: LocationTrackingType = .continuously
}

// MARK: - Public methods
extension LocationService {
    
    /**
     *Config location service, asks for location permission and starts updating location.*
     - parameter type: Type of tracking location. It can be continuously or only once.
     - parameter desiredAccuracy: The accuracy of a geographical coordinate.
     
     If location service is disabled or authorization status is denied, calls specified error method from delegate.
     */
    public func getLocation(type: LocationTrackingType, desiredAccuracy: CLLocationAccuracy = kCLLocationAccuracyBest) {
        setupLocationService(type: type, desiredAccuracy: desiredAccuracy)
        locationManager.requestWhenInUseAuthorization()
        guard CLLocationManager.locationServicesEnabled() else {
            delegate?.didGetLocationError(errorType:
                .locationDisbled(error: AppError(with: Strings.LocationService.locationIsDisabled.localized)
                    .setErrorType(errorType: .location)))
            return
        }
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.denied {
            delegate?.didGetLocationError(errorType:
                .accessDenied(error: AppError(with: Strings.LocationService.locationAccessDenied.localized)
                    .setErrorType(errorType: .location)))
            return
        }
        locationManager.startUpdatingLocation()
    }
    
    /**
     *Stops updating location.*
     */
    public func stopUpdatingLocation() {
        locationManager.delegate = nil
        locationManager.stopUpdatingLocation()
    }
    
}

// MARK: - Private methods
private extension LocationService {
    private func setupLocationService(type: LocationTrackingType, desiredAccuracy: CLLocationAccuracy) {
        locationManager.delegate = self
        locationTrackingType = type
        locationManager.desiredAccuracy = desiredAccuracy
    }
}

// MARK: - CLLocationManagerDelegate
extension LocationService: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        switch locationTrackingType {
        case .once:
            stopUpdatingLocation()
        case .continuously:
            break
        }
        delegate?.didGetCoordinate(coordinate: locValue)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        delegate?.didGetLocationError(errorType:
            .findLocationFailed(error: AppError(with: Strings.LocationService.locationCannotBeDetermined.localized)
            .setErrorType(errorType: .location)))
        stopUpdatingLocation()
    }
}

// MARK: - Get location type
enum LocationTrackingType {
    case once
    case continuously
}

enum GetLocationErrorType {
    case accessDenied(error: AppError)
    case locationDisbled(error: AppError)
    case findLocationFailed(error: AppError)
    case canNotGetCoordinate
}
