//
//  Config.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 26/02/2019.
//

import Foundation
import UIKit

/// *This class contains all properties from User Defaults and Keychains. There are also global properties used for config app.*
enum Config {
    
    static let emailQueryKey = "email"
    static let tokenQueryKey = "token"
    static let unauthorizedCode = 401
    static let paymentRequiredCode = 402
    static let conflictCode = 409
    static let searchHistoryMaxCount = 5
    static let statusBarTag = 12131415
    
    /// *It contains api url and it depends on build configuration*
    static var landingDomain: String {
        guard let urlPath = Bundle.main.infoDictionary!["PARLOR_LANDING_URL"] as? String else {
            fatalError("PARLOR_LANDING_URL have to be placed in plist file")
        }
        
        return urlPath
    }

    static var signInURL: URL {
        guard let url = URL(string: "\(landingDomain)/application/sign-up") else {
            fatalError()
        }
        return url
    }
    
    static var privacyPolicyURL: URL {
        guard let url = URL(string: "\(landingDomain)/privacy-policy") else {
            fatalError()
        }
        return url
    }
    
    static var termsAndConditionsURL: URL {
        guard let url = URL(string: "\(landingDomain)/terms-and-conditions") else {
            fatalError()
        }
        return url
    }
    
    static var appStoreUrl: URL {
        guard let stringUrl = Bundle.main.infoDictionary?["APP_STORE_URL"] as? String, let url = URL(string: stringUrl) else {
            fatalError("APP_STORE_URL not found in plist")
        }
        return url
    }

    static var instagramProfileUrl: URL {
        guard let stringUrl = Bundle.main.infoDictionary?["INSTAGRAM_PROFILE_URL"] as? String, let url = URL(string: stringUrl) else {
            fatalError("INSTAGRAM_PROFILE_URL not found in plist")
        }
        return url
    }

    static var appVersion: String {
        guard let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String else { return "" }
        return version
    }
    
    static var buildNumber: String? {
        #if !AppStore
        guard let buildNumber = Bundle.main.infoDictionary?["CFBundleVersion"] as? String else { return nil }
        return buildNumber
        #else
        return nil
        #endif
    }
    
    /// *If this value is true, payment screen should be showed after login*
    static var shouldInterceptWithPaymentScreen: Bool {
        return Config.userProfile?.destinationAfterLogin == .payment
    }
    
    static var shouldInterceptWithMembershipPlansScreen: Bool {
        return Config.userProfile?.destinationAfterLogin == .selectMembership
    }
    
    static var shouldInterceptWithDashboardScreen: Bool {
        return Config.userProfile?.destinationAfterLogin == .dashboard
    }
    
    static var shouldInterceptWithUserNotApprovedScreen: Bool {
        return Config.userProfile?.destinationAfterLogin == .userNotApproved
    }
    
    /// *It is api key of Google map*
    static var mapsApiKey: String {
        guard let key = Bundle.main.infoDictionary!["MAPS_API_KEY"] as? String else {
            fatalError()
        }
        return key
    }
    
    static var mapStyle: URL {
        guard let mapStyle = Bundle.main.url(forResource: "parlor-map-style", withExtension: "json") else {
            fatalError()
        }
        return mapStyle
    }
    
    /// - Apple pay
    static var applePayMerchantId: String {
        guard let key = Bundle.main.infoDictionary!["APPLE_PAY_MERCHANT_ID"] as? String else {
            fatalError()
        }
        return key
    }
    
    static var stripePublishableKey: String {
        guard let key = Bundle.main.infoDictionary!["STRIPE_PUBLISHABLE_KEY"] as? String else {
            fatalError()
        }
        return key
    }
    
    static var keychainWorker: KeychainWorker = {
        return KeychainWorker()
    }()
    
    static var userDefaultsWorker: UserDefaultsWorker = {
        return UserDefaultsWorker()
    }()
    
    static func clearTokenData() {
        keychainWorker.removeTokenFromKeychain()
    }
    
    static func performLogoutClear() {
        DispatchQueue.main.async {
            UIApplication.shared.unregisterForRemoteNotifications()
        }
        keychainWorker.removeTokenFromKeychain()
        userProfile = nil
        deviceToken = nil
    }
    
    static func updateTokenData(withAccessToken accessToken: String, refreshToken: String) {
        let worker = KeychainWorker()
        worker.saveUserToken(accessToken)
        worker.saveRefreshToken(refreshToken)
    }

    /**
     *Count of done rsvps by user*
    
     Maximum value is `IncrementRsvpCountHelper.maxRsvpCount`. To increment value use `IncrementRsvpCountHelper` class.
     */
    static var rsvpsCount: Int {
        get {
            guard let id = userProfile?.id else { return 0 }
            return userDefaultsWorker.rsvpsCount?[id] ?? 0
        }
        set {
            guard let id = userProfile?.id else { return }
            var rsvpsCountDict = userDefaultsWorker.rsvpsCount ?? [:]
            rsvpsCountDict[id] = newValue
            userDefaultsWorker.rsvpsCount = rsvpsCountDict
        }
    }
    
    /**
     *Count of user entries to app*
    
     Maximum value is `IncrementAppEntryCountHelper.maxAppEntryCount`. To increment value use `IncrementAppEntryCountHelper` class.
     */
    static var appEntryCount: Int {
        get {
            guard let id = userProfile?.id else { return 0 }
            return userDefaultsWorker.appEntryCount?[id] ?? 0
        }
        set {
            guard let id = userProfile?.id else { return }
            var appEntryCountDict = userDefaultsWorker.appEntryCount ?? [:]
            appEntryCountDict[id] = newValue
            userDefaultsWorker.appEntryCount = appEntryCountDict
        }
    }
    
    static var globalSettings: GlobalSettings? {
        get {
            return userDefaultsWorker.globalSettings
        }
        set {
            userDefaultsWorker.globalSettings = newValue
        }
    }

    static var token: String? {
        return keychainWorker.userToken
    }
    
    static var refreshToken: String? {
        return keychainWorker.refreshToken
    }
    
    static var userProfile: Profile? {
        get {
            return userDefaultsWorker.userProfile
        }
        set {
            userDefaultsWorker.userProfile = newValue
        }
    }
    
    static var isPremiumUser: Bool {
        return userProfile?.isPremium ?? false
    }
    
    static var chosenMapApp: String? {
        get {
            return userDefaultsWorker.chosenMapApp
        }
        set {
            userDefaultsWorker.chosenMapApp = newValue
        }
    }
    
    static var deviceToken: String? {
        get {
            return userDefaultsWorker.deviceToken
        }
        set {
            userDefaultsWorker.deviceToken = newValue
        }
    }
    
    static var notShowWelcomeMovie: Bool {
        return !(globalSettings?.showWelcomeVideo ?? true)
    }
    
    static var searchHistory: [String] {
        get {
            return userDefaultsWorker.searchHistory
        }
        set {
            userDefaultsWorker.searchHistory = newValue
        }
    }
    
    static var appUpdateInfo: AppUpdateInfoData? {
        get {
            return userDefaultsWorker.appUpdateInfo
        }
        set {
            userDefaultsWorker.appUpdateInfo = newValue
        }
    }

    static func addToSearchHistory(item: String) {
        searchHistory = Array(([item] + searchHistory.filter { $0 != item }).prefix(50))
    }

    static func cleanSearchHistory() {
        searchHistory = []
    }
    
    static func wasOnboardingShown(for type: OnboardingType) -> Bool {
        return userDefaultsWorker.wasOnboardingShown(for: type)
    }
    
    static func setOnboardingShown(for type: OnboardingType) {
        userDefaultsWorker.setOnboardingShown(for: type)
    }
    
    static func setOnboardingAllTypesUnshown() {
        OnboardingType.allCases.forEach { type in
            userDefaultsWorker.setOnboardingUnshown(for: type)
        }
    }
}
