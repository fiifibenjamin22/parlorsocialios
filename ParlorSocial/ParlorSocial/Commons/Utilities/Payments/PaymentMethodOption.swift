//  PaymentMethodOption.swift
//  ParlorSocialClub
//
//  Created on 17/04/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

protocol PaymentMethodOption: DropDownOption {
    var type: PaymentMethodType { get }
    var isDefault: Bool { get }
}

enum PaymentMethodType {
    case card
    case applePay
}
