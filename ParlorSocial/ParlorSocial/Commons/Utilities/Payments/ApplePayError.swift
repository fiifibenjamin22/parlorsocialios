//  ApplePayError.swift
//  ParlorSocialClub
//
//  Created on 28/04/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

enum ApplePayError: Error {
    case notAvailable
    
    var localizedDescription: String {
        switch self {
        case .notAvailable: return Strings.ApplePay.notAvailable.localized
        }
    }
}
