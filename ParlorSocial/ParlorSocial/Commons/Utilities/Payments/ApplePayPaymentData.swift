//  ApplePayPaymentData.swift
//  ParlorSocialClub
//
//  Created on 20/04/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import PassKit

struct ApplePayPaymentData {
    let countryCode: String = "US" /// app only available in USA and stripe account configured for USA
    let currencyCode: String
    let paymentItems: [PKPaymentSummaryItem]
    
    init(currencyCode: String, paymentItems: [PKPaymentSummaryItem]) {
        self.currencyCode = currencyCode
        self.paymentItems = paymentItems
    }
}
