//  ApplePayManager.swift
//  ParlorSocialClub
//
//  Created on 17/04/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import PassKit
import Stripe
import Crashlytics

typealias StripeTokenResponse = (token: STPToken?, error: Error?)

protocol ApplePayManagerDelegate: class {
    func applePayManager(_ manager: ApplePayManager, didFinishWith response: StripeTokenResponse, paymentCompletionHandler completion: @escaping (PKPaymentAuthorizationStatus) -> Void)
    func applePayManagerDidDismissPaymentController(_ manager: ApplePayManager)
}

class ApplePayManager: NSObject {

    // MARK: - Public properties
    
    var paymentErrorToPresent: AppError?
    
    // MARK: - Properties
    
    private unowned var presentationController: UIViewController
    private unowned var delegate: ApplePayManagerDelegate
    
    // MARK: - Init
    
    init(presentationController: UIViewController, delegate: ApplePayManagerDelegate) {
        self.presentationController = presentationController
        self.delegate = delegate
        super.init()
    }

    // MARK: - Public methods
    
    func makePayment(usingData data: ApplePayPaymentData) throws {
        let paymentRequest = Stripe.paymentRequest(withMerchantIdentifier: Config.applePayMerchantId, country: data.countryCode, currency: data.currencyCode)
        paymentRequest.paymentSummaryItems = data.paymentItems
        paymentRequest.requiredBillingContactFields = []
        
        guard Stripe.canSubmitPaymentRequest(paymentRequest), let controller = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest) else {
            logApplePayUnavailable()
            throw ApplePayError.notAvailable
        }
        
        controller.delegate = self
        presentationController.present(controller, animated: true, completion: nil)
    }
    
    // MARK: - Logging
    
    private func logApplePayUnavailable() {
        let error = NSError(domain: "Apple Pay unavailable", code: -1, userInfo: nil)
        // Logging non fatal in firebase for statistics purpose
        Crashlytics.sharedInstance().recordError(error)
    }
}

// MARK: - PKPaymentAuthorizationViewControllerDelegate

extension ApplePayManager: PKPaymentAuthorizationViewControllerDelegate {
    
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void) {
        STPAPIClient.shared().createToken(with: payment, completion: { [unowned self] token, error in
            self.delegate.applePayManager(self, didFinishWith: StripeTokenResponse(token: token, error: error), paymentCompletionHandler: completion)
        })
    }
    
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        controller.dismiss(animated: true, completion: { [unowned self] in
            self.delegate.applePayManagerDidDismissPaymentController(self)
        })
    }
}
