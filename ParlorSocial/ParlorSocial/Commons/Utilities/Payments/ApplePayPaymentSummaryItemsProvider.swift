//  ApplePayPaymentSummaryItemsProvider.swift
//  ParlorSocialClub
//
//  Created on 21/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import PassKit

final class ApplePayPaymentSummaryItemsProvider {
    
    // MARK: - Properties
    
    private static let businessMerchantName: String = "parlor.social"

    // MARK: - Factory methods
    
    static func createPaymentItemsForActivation(usingName name: String, price: Double) -> [PKPaymentSummaryItem] {
        let decimalPrice = NSDecimalNumber(value: price)
        let productItem = PKPaymentSummaryItem(label: name, amount: decimalPrice)
        let totalItem = createTotalPaymentItem(usingPrice: decimalPrice)
        return [productItem, totalItem]
    }
    
    static func createPaymentItemsForPayment(usingPlanData data: PlanData, price: Double) -> [PKPaymentSummaryItem] {
        var items: [PKPaymentSummaryItem] = []
        
        let priceDecimal = NSDecimalNumber(value: price)
        let productItem = PKPaymentSummaryItem(label: data.name, amount: priceDecimal)
        items.append(productItem)
        
        var totalPrice: NSDecimalNumber = priceDecimal

        if let proratedPrice = data.proratedPrice, let proratedDiscount = data.proratedDiscount {
            let decimalProratedPrice = NSDecimalNumber(value: proratedPrice)
            let decimalProratedDiscount = NSDecimalNumber(value: proratedDiscount)
            
            let discountItem = PKPaymentSummaryItem(label: Strings.Premium.proratedDiscount.localized, amount: decimalProratedDiscount) // item with discount
            items.append(discountItem)
            
            totalPrice = decimalProratedPrice // prorated price is final payment price
        }
        
        let totalItem = createTotalPaymentItem(usingPrice: totalPrice)
        items.append(totalItem)
        
        return items
    }
    
    // MARK: - Private helpers
    
    private static func createTotalPaymentItem(usingPrice price: NSDecimalNumber) -> PKPaymentSummaryItem {
        let merchantName = Config.globalSettings?.applePayBusinessMerchantName ?? Self.businessMerchantName
        return PKPaymentSummaryItem(label: merchantName, amount: price)
    }
}
