//  ApplePayDropDownItem.swift
//  ParlorSocialClub
//
//  Created on 17/04/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

struct ApplePayPaymentMethodOption: PaymentMethodOption {
    
    // MARK: - Constant
    
    static let applePayOptionId: Int = -1
    
    // MARK: - Properties
    
    let id: Int = applePayOptionId
    let icon: UIImage? = Icons.Cards.applePay
    let title: String = Strings.ApplePay.applePay.localized
    let type: PaymentMethodType = .applePay
    let isDefault: Bool = false
}
