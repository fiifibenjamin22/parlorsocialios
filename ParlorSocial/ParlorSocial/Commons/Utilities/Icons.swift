//
//  Icons.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 01/03/2019.
//

import UIKit

enum Icons {
    
    static func fromAppError(appError: AppError) -> UIImage {
        guard let errorType = appError.errorType else { return Error.general }
        switch errorType {
        case .notConnectedToInternet:
            return Error.notConnectedToInternet
        case .login:
            return Error.login
        case .premium:
            return Error.premium
        case .guestData:
            return Error.guestData
        case .payment:
            return Error.payment
        case .introRequest:
            return Error.introRequest
        case .location:
            return Error.location
        case .changeEmail:
            return Error.changeEmail
        }
    }
    
    enum Error {
        static let general = UIImage(named: "IconGeneralError")!
        static let notConnectedToInternet = UIImage(named: "IconNotConnectedToInternet")!
        static let login = UIImage(named: "IconLoginError")!
        static let premium = UIImage(named: "IconPremiumError")!
        static let guestData = UIImage(named: "IconGuestDataError")!
        static let payment = UIImage(named: "remove")!
        static let introRequest = UIImage(named: "IconIntroRequest")!
        static let location = UIImage(named: "IconLocationError")!
        static let changeEmail = UIImage(named: "IconChangeEmailError")!
    }
    
    enum AllActivations {
        static let clock = UIImage(named: "IconClock")!
        static let location = UIImage(named: "IconLocation")!
        static let premium = UIImage(named: "IconPremium")!
        static let check = UIImage(named: "IconCheck")!
        static let host = UIImage(named: "IconHost")!
        static let guestList = UIImage(named: "IconGuestList")!
    }

    enum Home {
        static let happeningsEmptyData = UIImage(named: "happenings-empty-data")
        static let mixersEmptyData = UIImage(named: "mixers-empty-data")
    }
    
    enum ActivationType {
        static let all = UIImage(named: "IconAll")
        static let `private` = UIImage(named: "IconPrivate")!
        static let event = UIImage(named: "IconEvent")!
        static let venue = UIImage(named: "IconVenue")!
        static let forYou = UIImage(named: "IconForYou")!
        static let privateDark = UIImage(named: "IconPrivateDark")!
        static let eventDark = UIImage(named: "IconEventDark")!
        static let venueDark = UIImage(named: "IconVenueDark")!
        static let privateBlack = UIImage(named: "IconPrivateBlack")!
        static let eventBlack = UIImage(named: "IconEventBlack")!
        static let venueBlack = UIImage(named: "IconVenueBlack")!
    }
    
    enum TabBar {
        static let home = UIImage(named: "IconHome")!
        static let homeSelected = UIImage(named: "IconHomeSelected")!
        static let myRsvps = UIImage(named: "IconRsvps")!
        static let myRsvpsSelected = UIImage(named: "IconRsvpsSelected")!
        static let announcements = UIImage(named: "IconAnnouncements")!
        static let announcementsSelected = UIImage(named: "IconAnnouncementsSelected")!
        static let profile = UIImage(named: "IconProfile")!
        static let profileSelected = UIImage(named: "IconProfileSelected")!
        static let unreadAnnouncements = UIImage(named: "IconUnreadAnnouncements")!
        static let unreadAnnouncementsSelected = UIImage(named: "IconUnreadAnnouncementsSelected")!
        static let unreadParlorPass = UIImage(named: "IconUnreadParlorPass")!
        static let unreadParlorPassSelected = UIImage(named: "IconUnreadParlorPassSelected")!
        static let unreadHome = UIImage(named: "IconUnreadHome")!
        static let unreadHomeSelected = UIImage(named: "IconUnreadHomeSelected")!
        static let community = UIImage(named: "IconCommunity")!
        static let communitySelected = UIImage(named: "IconCommunitySelected")!
    }
    
    enum NavigationBar {
        static let search = UIImage(named: "IconSearch")!
        static let calendar = UIImage(named: "IconCalendar")!
        static let arrowDown = UIImage(named: "IconArrowDown")!
        static let close = UIImage(named: "IconClose")!
    }
    
    enum Login {
        static let background = UIImage(named: "login-bg")!
        static let passwordEye = UIImage(named: "password-eye")!
        static let passwordEyeClosed = UIImage(named: "password-eye-closed")!
        static let loginLogo = UIImage(named: "login-logo")
    }
    
    enum ResetPassword {
        static let resetIcon = UIImage(named: "reset-icon")!
        static let createNew = UIImage(named: "create-new")!
        static let tick = UIImage(named: "tick")!
    }
    
    enum Common {
        static let backIcon = UIImage(named: "back")!
        static let logo = UIImage(named: "logo")!
        static let moreImages = UIImage(named: "moreImages")!
        static let checkedWhite = UIImage(named: "checked-white")!
        static let warning = UIImage(named: "warning")!
        static let hostPlaceholder = UIImage(named: "hostPlaceholder")!
        static let placeholderPhoto = UIImage(named: "placeholderPhoto")!
        static let placeholderPhotoBlack = UIImage(named: "placeholderPhotoBlack")!
        static let poweredByStripe = UIImage(named: "poweredByStripe")!
        static let checkedBig = UIImage(named: "checked-big")!
        static let cancelButton = UIImage(named: "cancelButton")!
    }
    
    enum ActivationDetails {
        static let entry = UIImage(named: "entry")!
        static let time = UIImage(named: "timeIcon")!
        static let location = UIImage(named: "locationIcon")!
        static let plus = UIImage(named: "plus")!
        static let minus = UIImage(named: "minus")!
        static let mapMarker = UIImage(named: "mapMarker")!
        static let stayTuned = UIImage(named: "IconStayTuned")!
        static let closedRsvp = UIImage(named: "IconClosedRsvp")!
        static let share = UIImage(named: "IconShare")!
        static let guestList = UIImage(named: "IconGuestList")!
    }
    
    enum ActivationDetailsInfo {
        static let event = UIImage(named: "IconInfoEvent")!
        static let link = UIImage(named: "IconInfoLink")!
        static let guestList = UIImage(named: "IconInfoGuestList")!
        static let rightArrow = UIImage(named: "IconInfoRightArrow")!
        static let share = UIImage(named: "IconInfoShare")!
    }

    enum Share {
        static let close = UIImage(named: "IconCloseButton")!
        static let parlor = UIImage(named: "IconParlor")!
        static let messages = UIImage(named: "IconMessages")!
        static let mail = UIImage(named: "IconMail")!
        static let messenger = UIImage(named: "IconMessenger")!
        static let whatsapp = UIImage(named: "IconWhatsapp")!
        static let copy = UIImage(named: "IconCopy")!
        static let more = UIImage(named: "IconMore")!
    }
    
    enum GuestsForm {
        static let timePeriod = UIImage(named: "IconTimePeriod")!
        static let card = UIImage(named: "IconCard")!
        static let guest = UIImage(named: "IconGuest")!
        static let arrow = UIImage(named: "IconSelectArrow")!
        static let add = UIImage(named: "add")!
        static let info = UIImage(named: "info")!
        static let guestSlotsNotAvailable = UIImage(named: "GuestSlotsNotAvailableIcon")!
    }
    
    enum CheckBox {
        static let `default` = UIImage(named: "CheckBox")!
        static let selected = UIImage(named: "CheckBoxSelected")!
    }
    
    enum Onboarding {
        static let rsvps = UIImage(named: "onboarding-rsvps")!
        static let home = UIImage(named: "onboarding-home")!
        static let announcements = UIImage(named: "onboarding-announcements")!
        static let profile = UIImage(named: "onboarding-profile")!
        static let guestList = UIImage(named: "onboarding-guest-list")!
        static let mixers = UIImage(named: "onboarding-mixers")!
        static let happenings = UIImage(named: "onboarding-happenings")!
        static let community = UIImage(named: "onboarding-community")!
    }
    
    enum Welcome {
        static let background = UIImage(named: "welcome-bg")!
        static let homeArrowDown = UIImage(named: "arrow-down")!
    }
    
    enum Register {
        static let addEmail = UIImage(named: "register-mail")!
        static let birthDate = UIImage(named: "birth-date")!
    }
    
    enum Waitlist {
        static let mainIcon = UIImage(named: "waitlist")!
    }
    
    enum InterestGroupos {
        static let checkedIcon = UIImage(named: "interest-checked")!
    }
    
    enum RsvpDetails {
        static let rightArrow = UIImage(named: "right-arrow")!
        static let externalTicketIcon = UIImage(named: "external-ticket-icon")!
        static let guestListDark = UIImage(named: "icon-guest-list-dark")!
        static let hostDark = UIImage(named: "icon-host-dark")!
        static let locationDark = UIImage(named: "icon-location-dark")!
        static let time = UIImage(named: "IconTimePeriod")!
    }

    enum EditProfile {
        static let editPhoto = UIImage(named: "placeholderPhoto")!
    }
    
    enum MyRsvps {
        static let waitlistIcon = UIImage(named: "waitlist-icon")!
        static let guestListIcon = UIImage(named: "guest-list-icon")!
    }
    
    enum GuestListRsvp {
        static let premiumIcon = UIImage(named: "IconPremiumGold")!
        static let checkBoxSelectedIcon = UIImage(named: "BigIconCheckBoxSelected")!
        static let checkBoxIcon = UIImage(named: "BigIconCheckBox")!
        static let introRequestSuccess = UIImage(named: "IconIntroRequestSuccess")!
    }
    
    enum Profile {
        static let calendarSetttings = UIImage(named: "calendarSettings")!
        static let referFriend = UIImage(named: "refer")!
        static let upgradeToPremium = UIImage(named: "upgrade")!
        static let emailUs = UIImage(named: "email")!
        static let referFriendSuccess = UIImage(named: "referFriendSuccessIcon")!
        static let removeFromCircle = UIImage(named: "removeFromCircleIcon")!
    }
    
    enum ReferFriend {
        static let friend = UIImage(named: "personalIconOutline")!
    }
    
    enum Cards {
        static let masterCard = UIImage(named: "mastercard")!
        static let visa = UIImage(named: "visa")!
        static let amex = UIImage(named: "amex")!
        static let discover = UIImage(named: "discover")!
        static let union = UIImage(named: "union")!
        static let diners = UIImage(named: "diners")!
        static let jcb = UIImage(named: "jcb")!
        static let remove = UIImage(named: "remove")!
        static let add = UIImage(named: "IconAddCard")!
        static let applePay = UIImage(named: "applePay")!
    }
    
    enum ChangePassword {
        static let changePasswordSuccess = UIImage(named: "IconChangePasswordSuccess")!
    }
    
    enum ChangeEmail {
        static let changeEmailSuccess = UIImage(named: "IconChangeEmailSuccess")!
    }
    
    enum PushNotifications {
        static let waitListTransfer = UIImage(named: "IconWaitlistTransfer")!
        static let rsvpOpen = UIImage(named: "IconRsvpOpen")!
        static let generalPremium = UIImage(named: "premiumGeneral")!
    }
    
    enum SearchBar {
        static let search = UIImage(named: "IconSearchBlack")!
        static let clean = UIImage(named: "IconSearchClean")!
    }
    
    enum AvailableGuestList {
        static let closeButton = UIImage(named: "IconCloseCrossSmall")!
    }
    
    enum WelcomeMovie {
        static let splash = UIImage(named: "SplashImage")!
    }
    
    enum AddProfilePhoto {
        static let addProfileAvatar = UIImage(named: "IconAddProfileAvatar")!
    }
    
    enum EnableNotificationsSettingsReminder {
        static let phoneNotificationIcon = UIImage(named: "IconPhoneNotification")!
    }

    enum UserNotApproved {
        static let backgroundImage = UIImage(named: "UserNotApprovedBackground")!
        static let instagramIcon = UIImage(named: "instagramIcon")!
    }

    enum AppUpdateRequired {
        static let appUpdateLogo = UIImage(named: "AppUpdateLogo")!
    }
    
    enum Connections {
        static let rejectIcon = UIImage(named: "rejectIcon")!
        static let moreConnectionsIcon = UIImage(named: "moreConnections")!
    }
}
