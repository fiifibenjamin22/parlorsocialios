//
//  ConfigurableCell.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 28/03/2019.
//

import UIKit

protocol ConfigurableCell {
    associatedtype Model
    func configure(with model: Model)
}
