//
//  EnumDecodable.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 19/06/2019.
//

import Foundation

protocol EnumDecodable: RawRepresentable, Decodable {
    static var defaultDecoderValue: Self { get }
}

extension EnumDecodable where RawValue: Decodable {
    init(from decoder: Decoder) throws {
        let value = try decoder.singleValueContainer().decode(RawValue.self)
        self = Self(rawValue: value) ?? Self.defaultDecoderValue
    }
}
