//  CalendarSyncConfig.swift
//  ParlorSocialClub
//
//  Created on 29/04/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

struct CalendarSyncConfig {
    
    // MARK: - Private properties
    
    private static var userDefaultsWorker: UserDefaultsWorker = {
        return UserDefaultsWorker()
    }()
    
    // MARK: - Public properties
    
    static let minDaysAfterFirstReminderRejection: Int = 1
    
    //For changins state is CelendarUtilities class
    static var isSyncEnabled: Bool? {
        get {
            guard let id = Config.userProfile?.id else { return nil }
            return userDefaultsWorker.isSyncEnabledDict?[id]
        }
        set {
            guard let id = Config.userProfile?.id else { return }
            var isSyncEnabledDict = userDefaultsWorker.isSyncEnabledDict ?? [:]
            isSyncEnabledDict[id] = newValue
            userDefaultsWorker.isSyncEnabledDict = isSyncEnabledDict
        }
    }
    
    /// Date of first permission rejection for calendar sync
    static var firstRejectCalendarSyncPermissionsDate: Date? {
        get {
            guard let id = Config.userProfile?.id else { return nil }
            return userDefaultsWorker.firstRejectCalendarSyncPermissionsDate?[id]
        }
        set {
            guard let id = Config.userProfile?.id else { return }
            var lastRejectNotificationsPermissionsDateDict = userDefaultsWorker.firstRejectCalendarSyncPermissionsDate ?? [:]
            lastRejectNotificationsPermissionsDateDict[id] = newValue
            userDefaultsWorker.firstRejectCalendarSyncPermissionsDate = lastRejectNotificationsPermissionsDateDict
        }
    }
    
    /**
     *Count of done rsvps by user for calendar sync*
    
     Maximum value is `IncrementRsvpCountHelper.maxRsvpCount`. To increment value use `IncrementRsvpCountHelper` class.
     */
    static var rsvpsCount: Int {
        get {
            guard let id = Config.userProfile?.id else { return 0 }
            return userDefaultsWorker.rsvpsForCalendarSyncCount?[id] ?? 0
        }
        set {
            guard let id = Config.userProfile?.id else { return }
            var rsvpsCountDict = userDefaultsWorker.rsvpsForCalendarSyncCount ?? [:]
            rsvpsCountDict[id] = newValue
            userDefaultsWorker.rsvpsForCalendarSyncCount = rsvpsCountDict
        }
    }
}
