//
//  InitialViewControllerUtility.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 22/01/2020.
//

import UIKit

final class InitialViewControllerUtility {
    static func getViewControllerWithLoginAsRoot<T: AppViewController>(viewController: T) -> UIViewController {
        let loginNavigationViewController = LoginViewController().embedInNavigationController()
        let currentControllers = loginNavigationViewController.viewControllers
        loginNavigationViewController.setViewControllers(currentControllers + [viewController], animated: true)
        return loginNavigationViewController
    }
}
