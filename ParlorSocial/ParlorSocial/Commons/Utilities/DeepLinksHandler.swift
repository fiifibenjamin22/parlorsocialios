//
//  DeepLinksHandler.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 05/09/2019.
//

import UIKit

/** This class is used to handle universal links. When user open specific link in browser, application will be opened on specific view.
 */
class DeepLinksHandler {
    static let shared: DeepLinksHandler = DeepLinksHandler()
    
    /// Stores universal link when it is intercepted when the app is not active. It is used after the app becomes active.
    var storedUniversalLinkUserActivity: NSUserActivity?
    var topAppVC: AppViewController? {
        let topVC = UIApplication.shared.windows.first?.rootViewController?.top
        return topVC as? AppViewController
    }
    
    private init() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(
            self,
            selector: #selector(handleDeepLinkWhenDidBecomeActive),
            name: UIApplication.didBecomeActiveNotification,
            object: nil
        )
    }
    
    /**
     *Intercepts universal links and chooses appropriate operation depending on the link.*
     - parameter userActivity: A representation of the state of your app at a moment in time.
     - returns: `true` to indicate that your app handled the activity or false to let iOS know that your app did not handle the activity.
     */
    func handleDeepLinkCallback(for userActivity: NSUserActivity, appState: UIApplication.State) {
        if appState == .active {
            handleDeepLinkByType(for: userActivity)
        } else {
            storedUniversalLinkUserActivity = userActivity
        }
    }
    
    private func handleDeepLinkByType(for userActivity: NSUserActivity) {
        storedUniversalLinkUserActivity = nil
        guard let url = userActivity.webpageURL,
            let deepLinkType = userActivity.deepLinkType else { return }
        switch deepLinkType {
        case .createPassword:
            handlePasswordReset(with: url)
        case .changeEmail:
            handleChangeEmail(with: url)
        case .activationDetails:
            handleActivationDetails(with: url)
        case .guestList:
            handleGuestList(with: url)
        case .selectMembership:
            handleSelectMembership(with: url)
        case .waitList:
            handleWaitList(with: url)
        case .announcementDetails:
            handleAnnouncementDetailsLinkClick(with: url)
        }
    }
    
    /**
     *Intercepts universal link from resetting password and shows reset password view.*
     - parameter url: Intercepted universal link.
     */
    private func handlePasswordReset(with url: URL) {
        let parameters = url.queryParameters
        if let email = parameters[Config.emailQueryKey], let token = parameters[Config.tokenQueryKey] {
            UIApplication.shared.windows.first?.rootViewController =
                ResetPasswordViewController.fromResetPasswordDeepLink(forUserWithEmail: email, token: token)
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        }
    }
    
    /**
     *Intercepts universal link from changing email and shows login view.*
     - parameter url: Intercepted universal link.
     */
    private func handleChangeEmail(with url: URL) {
        let urlString = url.absoluteString
        guard let range = urlString.range(of: "\(DeepLinkType.changeEmail.rawValue)") else { return }
        let parameters = urlString[range.upperBound...]
        UIApplication.shared.windows.first?.rootViewController =
            LoginViewController(forUrlParameters: String(parameters))
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
    
    /**
    *Intercepts universal link and shows activation details view.*
    - parameter url: Intercepted universal link.
    */
    private func handleActivationDetails(with url: URL) {
        guard let activationId = getItemId(from: url, forDeepLinkType: .activationDetails) else { return }
        let openingType = ActivationDetailsNavigationProvider.getOpeningActivationDetailsType()
        let controller =  AppViewControllerDestination.activationDetails(activationId: activationId, openingType: openingType, navigationSource: .deeplink)
        topAppVC?.showVCAfterPushNotificationOrLinkClicked(for: controller)
    }
    
    /**
    *Intercepts universal link and shows guest list of an activation.*
    - parameter url: Intercepted universal link.
    */
    private func handleGuestList(with url: URL) {
        guard let activationId = getItemId(from: url, forDeepLinkType: .activationDetails) else { return }
        topAppVC?.showAvailableGuestListPopup(activationId: activationId, showGuestListWhenAvailable: true, animated: false)
    }
    
    private func handleWaitList(with url: URL) {
        guard let activationId = getItemId(from: url, forDeepLinkType: .activationDetails) else { return }
        topAppVC?.showVCAfterPushNotificationOrLinkClicked(
            for: AppViewControllerDestination.waitListInfo(activationId: activationId))
    }
    
    private func getItemId(from url: URL, forDeepLinkType type: DeepLinkType) -> Int? {
        let urlString = url.absoluteString
        guard let range = urlString.range(of: "\(type.rawValue)/") else { return nil }
        let parameters = urlString[range.upperBound...].components(separatedBy: "/")
        guard let activationIdString = parameters.first,
            let activationId = Int(activationIdString),
            Config.userProfile != nil else { return nil }
        return activationId
    }
    
    /**
    *Intercepts universal link and shows select membership plan.*
    - parameter url: Intercepted universal link.
    */
    private func handleSelectMembership(with url: URL) {
        topAppVC?.showVCAfterPushNotificationOrLinkClicked(
            for: AppViewControllerDestination.membershipPlans)
    }
    
    /**
    *Intercepts universal link and shows announcement details view.*
    - parameter url: Intercepted universal link.
    */
    private func handleAnnouncementDetailsLinkClick(with url: URL) {
        guard let announcementId = getItemId(from: url, forDeepLinkType: .announcementDetails) else { return }
        topAppVC?.showVCAfterPushNotificationOrLinkClicked(for: .announcementDetails(announcementId: announcementId))
    }

    /**
     *Intercepts deep link stored in `storedUniversalLinkUserActivity` when application becomes active.*
     
     Method is called when app was not active and was opened by click on deep link. The application waits until it becomes active. After that it moves to specific view controller.
     */
    @objc private func handleDeepLinkWhenDidBecomeActive() {
        guard let userActivity = storedUniversalLinkUserActivity else { return }
        handleDeepLinkByType(for: userActivity)
    }
    
    enum DeepLinkType: String, CaseIterable {
        // The order matters
        case createPassword = "create-password"
        case changeEmail = "change-email"
        case guestList = "/guestlist"
        case waitList = "/waitlist"
        case activationDetails = "/activations"
        case selectMembership = "select-membership"
        case announcementDetails = "/announcements"

        static var inAppOpenableDeepLinks: [Self] {
            return [.guestList, .waitList, .activationDetails, .selectMembership]
        }
    }
}

fileprivate extension NSUserActivity {
    typealias LinkType = DeepLinksHandler.DeepLinkType
    
    var deepLinkType: LinkType? {
        guard let url = self.webpageURL else { return nil }
        return LinkType.allCases.first {
            url.absoluteString.contains($0.rawValue)
        }
    }
}
