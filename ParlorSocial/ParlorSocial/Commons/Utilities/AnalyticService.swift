//
//  AnalyticService.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 03/10/2019.
//  Copyright © 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxSwift

class AnalyticService {
    
    static let shared: AnalyticService = .init()
    let minRandomSessionId: Int64 = 1000000000
    let maxRandomSessionId: Int64 = 9999999999
    let sendEventsTimeInterval = 60.0
    var backgroundTask: UIBackgroundTaskIdentifier?
    var sessionId: Int64?
    
    private let databaseRepository: AnalyticsDatabaseRepositoryProtocol
    private let disposeBag: DisposeBag = .init()
    
    private init() {
        databaseRepository = AnalyticsDatabaseRepository()
        Timer.scheduledTimer(timeInterval: sendEventsTimeInterval, target: self, selector: #selector(sendEvents), userInfo: nil, repeats: true)
    }
        
}

// MARK: - Public methods
extension AnalyticService {
    func saveOpenScreenEventToUserDefaults(screenData: AnalyticEventViewControllerData, referenceId: Int?, navigationSource: String?, navigationSourceId: Int?) {
        guard let sessionId = self.sessionId, let userId = Config.userProfile?.id else { return }
        let data = AnalyticEventData.createForScreenOpenType(
            name: screenData.screenName,
            referencedId: referenceId,
            source: navigationSource,
            sourceId: navigationSourceId
        )
        let event = AnalyticEvent(
            userId: userId,
            deviceToken: Config.deviceToken,
            timestamp: screenData.timestamp,
            type: screenData.eventType,
            data: data,
            sessionId: sessionId
        )
        saveEventInDatabase(event)
    }
    
    func saveCloseScreenEventToUserDefaults(
        openScreenData: AnalyticEventViewControllerData,
        closeScreenData: AnalyticEventViewControllerData,
        referenceId: Int?,
        navigationSource: String?,
        navigationSourceId: Int?
    ) {
        guard let sessionId = self.sessionId, let userId = Config.userProfile?.id else { return }
        let duration = closeScreenData.timestamp - openScreenData.timestamp
        let data = AnalyticEventData.createForScreenCloseType(
            name: closeScreenData.screenName,
            duration: duration,
            referencedId: referenceId,
            source: navigationSource,
            sourceId: navigationSourceId
        )
        let event = AnalyticEvent(
            userId: userId,
            deviceToken: Config.deviceToken,
            timestamp: closeScreenData.timestamp,
            type: closeScreenData.eventType,
            data: data,
            sessionId: sessionId
        )
        saveEventInDatabase(event)
    }
    
    func saveTabSelectedAnalyticEvent(type: AnalyticEventTabSelected) {
        guard let sessionId = AnalyticService.shared.sessionId,
            let userId = Config.userProfile?.id else { return }
        let event = AnalyticEvent(
            userId: userId,
            deviceToken: Config.deviceToken,
            timestamp: Int64(Date().timeIntervalSince1970 * 1000),
            type: .tabSelected,
            data: .createForSpecialType(name: type.rawValue, duration: nil, referencedId: nil),
            sessionId: sessionId
        )
        saveEventInDatabase(event)
    }
    
    func saveViewClickAnalyticEvent(withName name: String, referenceId: Int? = nil) {
        guard let sessionId = AnalyticService.shared.sessionId,
            let userId = Config.userProfile?.id else { return }
        let event = AnalyticEvent(
            userId: userId,
            deviceToken: Config.deviceToken,
            timestamp: Int64(Date().timeIntervalSince1970 * 1000),
            type: .buttonClicked,
            data: .createForButtonTapAction(withName: name, referenceId: referenceId),
            sessionId: sessionId
        )
        saveEventInDatabase(event)
    }

    func saveNativeAnalyticShareActivation(with type: UIActivity.ActivityType) {
        guard let sessionId = AnalyticService.shared.sessionId,
            let userId = Config.userProfile?.id else { return }
        let event = AnalyticEvent(
            userId: userId,
            deviceToken: Config.deviceToken,
            timestamp: Int64(Date().timeIntervalSince1970 * 1000),
            type: .nativeShare,
            data: .createForButtonTapAction(withName: type.rawValue),
            sessionId: sessionId
        )
        saveEventInDatabase(event)
    }
    
    func sendEventsInBackground() {
        let events = databaseRepository.fetchAllEvents()
        guard !events.isEmpty else { return }
        
        self.backgroundTask = UIApplication.shared.beginBackgroundTask(withName: "backgroundTask") { [unowned self] in
            UIApplication.shared.endBackgroundTask(self.backgroundTask!)
            self.backgroundTask = UIBackgroundTaskIdentifier.invalid
        }
        
        AnalyticRepository.shared.sendEvents(with: AnalyticEventRequest(usingData: events))
            .subscribe(onNext: { [unowned self] response in
                self.handleSendEventsResponse(response: response, events: events)
                UIApplication.shared.endBackgroundTask(self.backgroundTask!)
                self.backgroundTask = UIBackgroundTaskIdentifier.invalid
            }).disposed(by: self.disposeBag)
    }
    
    @objc func sendEvents() {
        let events = databaseRepository.fetchAllEvents()
        guard !events.isEmpty && Config.userProfile != nil else { return }

        AnalyticRepository.shared.sendEvents(with: AnalyticEventRequest(usingData: events))
            .subscribe(onNext: { [unowned self] response in
                self.handleSendEventsResponse(response: response, events: events)
            }).disposed(by: self.disposeBag)
    }
    
    func refreshSessionId() {
        sessionId = Int64.random(in: minRandomSessionId..<maxRandomSessionId)
    }
    
    func saveEventInDatabase(_ event: AnalyticEvent) {
        print("[EVENT ANALYTIC] Save analytic event to user defaults - event type: \(event.type), event data: \(String(describing: event.data))")
        databaseRepository.saveEvent(event)
    }
}

// MARK: - Private methods
private extension AnalyticService {
    
    private func handleSendEventsResponse(response: ApiResponse<MessageResponse>, events: [AnalyticEventEntity]) {
        switch response {
        case .success:
            databaseRepository.deleteEvents(events)
        case .failure(let error):
            print(error)
        }
    }
    
}
