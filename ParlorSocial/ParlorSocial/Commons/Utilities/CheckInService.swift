//
//  CheckInService.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 20/01/2020.
//

import Foundation
import RxSwift
import CoreLocation

protocol CheckInServiceDelegate: class {
    var checkInService: CheckInService { get set }
    func didStartedCheckIn(_ checkInService: CheckInService)
    func didGetLocationError(_ checkInService: CheckInService, errorType: GetLocationErrorType)
    func didEndCheckInSuccess(_ checkInService: CheckInService, activationId: Int)
    func didCheckInError(_ checkInService: CheckInService, error: AppError)
}

extension CheckInServiceDelegate where Self: BaseViewModel {
    func didStartedCheckIn(_ checkInService: CheckInService) {
        progressBarSubject.onNext(true)
    }
    
    func didGetLocationError(_ checkInService: CheckInService, errorType: GetLocationErrorType) {
        progressBarSubject.onNext(false)
        switch errorType {
        case .locationDisbled(let error):
            errorSubject.onNext(error)
        case .findLocationFailed(let error):
            showLocationErrorTryAgainAlert(with: error)
        case .accessDenied(let error):
            alertDataSubject.onNext(AlertData.locationErrorAlert(error: error))
        case .canNotGetCoordinate:
            break
        }
    }
    
    func didCheckInError(_ checkInService: CheckInService, error: AppError) {
        progressBarSubject.onNext(false)
        showLocationErrorTryAgainAlert(with: error)
    }
    
    private func showLocationErrorTryAgainAlert(with error: AppError) {
        alertDataSubject.onNext(AlertData.locationErrorTryAgainAlertData(error: error, buttonAction: { [weak self] in
            guard let strongActivationId = self?.checkInService.activationId,
                let strongCheckInStrategy = self?.checkInService.checkInStrategy else { return }
            self?.checkInService.navigateToGuestList(
                activationId: strongActivationId,
                checkInStrategy: strongCheckInStrategy
            )
        }))
    }
}

final class CheckInService {
    // MARK: - Properties
    var activationId: Int?
    var checkInStrategy: CheckInStrategy?

    weak var delegate: CheckInServiceDelegate?

    let disposeBag = DisposeBag()

    private let locationService: LocationService
    private let guestListRepository: GuestListRsvpRepositoryProtocol
    private let activationRepository: ActivationsRepositoryProtocol
    
    // MARK: - Initialization
    init(locationService: LocationService, guestListRepository: GuestListRsvpRepositoryProtocol, activationRepository: ActivationsRepositoryProtocol) {
        self.locationService = locationService
        self.guestListRepository = guestListRepository
        self.activationRepository = activationRepository
        setup()
    }
    
    // MARK: - Methods
    func navigateToGuestList(activationId: Int, checkInStrategy: CheckInStrategy?) {
        self.activationId = activationId
        self.checkInStrategy = checkInStrategy

        if checkInStrategy == .required {
            delegate?.didStartedCheckIn(self)
            locationService.getLocation(type: .once)
        } else {
            delegate?.didEndCheckInSuccess(self, activationId: activationId)
        }
    }

    func checkInOnGuestListView(activationId: Int, checkInStrategy: CheckInStrategy?) {
        self.activationId = activationId
        self.checkInStrategy = checkInStrategy
        
        if checkInStrategy == .optional {
            delegate?.didStartedCheckIn(self)
            locationService.getLocation(type: .once)
        } else {
            delegate?.didEndCheckInSuccess(self, activationId: activationId)
        }
    }
    
    // MARK: - Private methods
    private func setup() {
        locationService.delegate = self
    }
    
    private func handleCheckInResponse(_ response: RsvpResponse) {
        switch response {
        case .success:
            guard let activationId = activationId else { print("Activation id in CheckInService is nil"); return }
            delegate?.didEndCheckInSuccess(self, activationId: activationId)
        case .failure(let error):
            failureCheckIn(error: error)
        }
    }
    
    private func handleIsAlreadyCheckedIn(_ response: SingleActivationResposne) {
        switch response {
        case .success(let response):
            self.activationId = response.data.id
            self.checkInStrategy = response.data.checkInStrategy
            delegate?.didEndCheckInSuccess(self, activationId: response.data.id)
        case .failure(let error):
            delegate?.didCheckInError(self, error: AppError(with: error))
        }
    }
    
    private func failureCheckIn(error: AppError) {
        if error.isAlreadyCheckedInError {
            guard let strongActivationId = activationId else { print("Activation id in CheckInService is nil"); return }
            activationRepository.getActivation(withId: strongActivationId)
                .subscribe(onNext: { [weak self] response in
                    self?.handleIsAlreadyCheckedIn(response)
                }).disposed(by: disposeBag)
        } else {
            delegate?.didCheckInError(self, error: error)
        }
    }
}

// MARK: - Location service
extension CheckInService: LocationServiceDelegate {
    func didGetLocationError(errorType: GetLocationErrorType) {
        delegate?.didGetLocationError(self, errorType: errorType)
    }
    
    func didGetCoordinate(coordinate: CLLocationCoordinate2D) {
        guard let lat = Double(String(format: "%.7f", coordinate.latitude)),
            let lng = Double(String(format: "%.7f", coordinate.longitude)) else {
                delegate?.didGetLocationError(self, errorType: .canNotGetCoordinate)
                return
        }

        guard let strongActivationId = activationId else { print("Activation id in CheckInService is nil"); return }

        let checkInRequest = CheckInRequest(coordinates: Coordinates(lat: lat, lng: lng))
        guestListRepository.checkInGuestList(forActivationId: strongActivationId, data: checkInRequest)
            .subscribe(onNext: { [weak self] response in
                guard let strongSelf = self else { return }
                strongSelf.handleCheckInResponse(response)
            }).disposed(by: disposeBag)
    }
}
