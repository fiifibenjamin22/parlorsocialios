//  FirebaseAnalyticService.swift
//  ParlorSocialClub
//
//  Created on 26/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import FirebaseAnalytics

final class FirebaseAnalyticService: MembershipAnalyticService {
    
    enum CustomParameter: String {
        case membershipName = "membership_name"
    }
    
    func logMembershipSelectedEvent(membershipPlan: PlanData) {
        let currencyCode = membershipPlan.currency?.code ?? defaultCurrency
        let price = membershipPlan.proratedPrice ?? defaultPrice

        let parameters: [String: Any] = [
            AnalyticsParameterCurrency: currencyCode,
            AnalyticsParameterValue: price,
            AnalyticsParameterItemID: "\(membershipPlan.id)",
            AnalyticsParameterQuantity: 1,
            CustomParameter.membershipName.rawValue: membershipPlan.membershipPlanAnalyticContentType
        ]
        
        Analytics.logEvent(AnalyticsEventAddToCart, parameters: parameters)
    }
    
    func logMembershipPurchasedEvent(membershipPlan: PlanData) {
        let currencyCode = membershipPlan.currency?.code ?? defaultCurrency
        let price = membershipPlan.proratedPrice ?? defaultPrice

        let parameters: [String: Any] = [
          AnalyticsParameterCurrency: currencyCode,
          AnalyticsParameterValue: price,
          AnalyticsParameterItemID: "\(membershipPlan.id)",
          AnalyticsParameterItemName: membershipPlan.membershipPlanAnalyticContentType,
          AnalyticsParameterQuantity: 1
        ]

        Analytics.logEvent(AnalyticsEventPurchase, parameters: parameters)
    }
    
    func logPaymentMethodEvent(isSuccess: Bool) {
        Analytics.logEvent(AnalyticsEventAddPaymentInfo, parameters: [
            AnalyticsParameterSuccess: isSuccess ? 1 : 0
        ])
    }
}
