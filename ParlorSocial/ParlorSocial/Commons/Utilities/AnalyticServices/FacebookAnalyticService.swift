//
//  FacebookAnalyticService.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 03/03/2020.
//

import Foundation
import FBSDKCoreKit

final class FacebookAnalyticService {
    func logSelectMembershipScreenOpened() {
        let params = [AppEvents.ParameterName.contentType.rawValue: MembershipPlanAnalyticContentType.selectMembershipScreen.rawValue]
        AppEvents.logEvent(.viewedContent, parameters: params)
    }
}

extension FacebookAnalyticService: MembershipAnalyticService {
    func logMembershipSelectedEvent(membershipPlan: PlanData) {
        let currencyCode = membershipPlan.currency?.code ?? defaultCurrency
        let price = membershipPlan.proratedPrice ?? defaultPrice
        
        AppEvents.logEvent(.addedToCart, valueToSum: price, parameters: [
            AppEvents.ParameterName.contentID.rawValue: membershipPlan.id,
            AppEvents.ParameterName.contentType.rawValue: membershipPlan.membershipPlanAnalyticContentType,
            AppEvents.ParameterName.currency.rawValue: currencyCode
        ])
    }
    
    func logMembershipPurchasedEvent(membershipPlan: PlanData) {
        let currencyCode = membershipPlan.currency?.code ?? defaultCurrency
        let price = membershipPlan.proratedPrice ?? defaultPrice
        
        AppEvents.logPurchase(price, currency: currencyCode, parameters: [
            AppEvents.ParameterName.contentID.rawValue: membershipPlan.id,
            AppEvents.ParameterName.contentType.rawValue: membershipPlan.membershipPlanAnalyticContentType
        ])
    }
    
    func logPaymentMethodEvent(isSuccess: Bool) {
        AppEvents.logEvent(.addedPaymentInfo, parameters: [AppEvents.ParameterName.success.rawValue: isSuccess ? 1 : 0])
    }
}
