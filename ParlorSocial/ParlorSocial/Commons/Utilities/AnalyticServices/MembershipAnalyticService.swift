//  MembershipAnalyticService.swift
//  ParlorSocialClub
//
//  Created on 26/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

protocol MembershipAnalyticService: class {
    var defaultCurrency: String { get }
    var defaultPrice: Double { get }
    
    func logMembershipSelectedEvent(membershipPlan: PlanData)
    func logMembershipPurchasedEvent(membershipPlan: PlanData)
    func logPaymentMethodEvent(isSuccess: Bool)
}

extension MembershipAnalyticService {
    var defaultCurrency: String { "USD" }
    var defaultPrice: Double { 0.0 }
}
