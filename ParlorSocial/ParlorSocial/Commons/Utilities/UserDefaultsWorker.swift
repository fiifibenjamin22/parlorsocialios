//
//  UserDefaultsWorker.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 18/04/2019.
//

import Foundation

private let globalSettingsKey: String = "globalSettingsKey"
private let profileKey: String = "us_profile"
private let mapAppKey: String = "chosen_map_app"
private let deviceTokenKey: String = "token_device"
private let welcomeMovieKey: String = "welcome_movie"
private let searchHistoryKey: String = "search_history"
private let isCalendarSyncEnabledKey: String = "isSyncEnabled"
private let latestNotificationsEnableSettingsKey: String = "latest_notifications_enable_settings"
private let enableNotificationsRemindLevelKey: String = "enable_notifications_remind_level"
private let rsvpsCountKey: String = "rsvps_count_key"
private let lastRejectNotificationsPermissionsDateKey: String = "last_reject_notifications_permissions_date"
private let appEntryCountKey: String = "app_entry_count"
private let appEntryForNotificationsCountKey: String = "app_entry_for_notifications_count"
private let rsvpsForNotificationsCountKey: String = "rsvps_for_notifications_count"
private let firstRejectCalendarSyncPermissionsDateKey: String = "first_reject_calendar_sync_permissions_date"
private let rsvpsForCalendarSyncCountKey: String = "rsvps_for_calendar_sync_count"
private let appUpdateInfoKey: String = "app_update_info"

class UserDefaultsWorker {
    private let jsonDecoder = JSONDecoder()
    private let jsonEncoder = JSONEncoder()
    
    var globalSettings: GlobalSettings? {
        get {
            guard let savedSettings = UserDefaults.standard.object(forKey: globalSettingsKey) as? Data else { return nil }
            return try? jsonDecoder.decode(GlobalSettings.self, from: savedSettings)
        }
        set {
            if newValue == nil {
                UserDefaults.standard.removeObject(forKey: globalSettingsKey)
            }
            if let data = try? jsonEncoder.encode(newValue) {
                UserDefaults.standard.set(data, forKey: globalSettingsKey)
            }
        }
    }
        
    var userProfile: Profile? {
        get {
            guard let savedProfile = UserDefaults.standard.object(forKey: profileKey) as? Data else { return nil }
            return try? jsonDecoder.decode(Profile.self, from: savedProfile)
        }
        set {
            if newValue == nil {
                UserDefaults.standard.removeObject(forKey: profileKey)
            }
            if let data = try? jsonEncoder.encode(newValue) {
                UserDefaults.standard.set(data, forKey: profileKey)
            }
        }
    }
    
    var searchHistory: [String] {
        get {
            return UserDefaults.standard.stringArray(forKey: searchHistoryKey) ?? []
        }
        set {
            UserDefaults.standard.set(newValue, forKey: searchHistoryKey)
        }
    }
    
    var appUpdateInfo: AppUpdateInfoData? {
        get {
            guard let updateInfo = UserDefaults.standard.object(forKey: appUpdateInfoKey) as? Data else { return nil }
            return try? jsonDecoder.decode(AppUpdateInfoData?.self, from: updateInfo)
        }
        set {
            if newValue == nil {
                UserDefaults.standard.removeObject(forKey: appUpdateInfoKey)
            }
            if let data = try? jsonEncoder.encode(newValue) {
                UserDefaults.standard.set(data, forKey: appUpdateInfoKey)
            }
        }
    }
    
    var chosenMapApp: String? {
        get {
            return UserDefaults.standard.string(forKey: mapAppKey)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: mapAppKey)
        }
    }
    
    var deviceToken: String? {
        get {
            return UserDefaults.standard.string(forKey: deviceTokenKey)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: deviceTokenKey)
        }
    }
    
    var notShowWelcomeMovie: Bool {
        get {
            return UserDefaults.standard.bool(forKey: welcomeMovieKey)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: welcomeMovieKey)
        }
    }

    var isSyncEnabledDict: [Int: Bool]? {
        get {
            guard let calendarSyncDict = UserDefaults.standard.object(forKey: isCalendarSyncEnabledKey) as? Data else { return nil }
            return try? jsonDecoder.decode([Int: Bool].self, from: calendarSyncDict)
        }
        set {
            if let data = try? jsonEncoder.encode(newValue) {
                UserDefaults.standard.set(data, forKey: isCalendarSyncEnabledKey)
            }
        }
    }
    
    /**
    *Enable notifications remind level*
   
     `zero` it is set when user has not set permissions yet or has set on authorized
     `first` it is set when user reject notifications perrminssions first time
     `second` it is set when user reject notifications perrminssions second time
     `third` it is set when user reject notifications perrminssions third time
    */
    var enableNotificationsRemindLevel: [Int: EnableNotificationsSettingsRemindLevel]? {
        get {
            guard let enableNotificationsRemindLevelDict = UserDefaults.standard.object(forKey: enableNotificationsRemindLevelKey) as? Data else { return nil }
            return try? jsonDecoder.decode([Int: EnableNotificationsSettingsRemindLevel].self, from: enableNotificationsRemindLevelDict)
        }
        set {
            if let data = try? jsonEncoder.encode(newValue) {
                UserDefaults.standard.set(data, forKey: enableNotificationsRemindLevelKey)
            }
        }
    }
    
    /**
     *Count of done rsvps by user*
    
     Maximum value is `IncrementRsvpCountHelper.maxRsvpCount`. To increment value use `IncrementRsvpCountHelper` class.
     */
    var rsvpsCount: [Int: Int]? {
        get {
            guard let rsvpsCount = UserDefaults.standard.object(forKey: rsvpsCountKey) as? Data else { return nil }
            return try? jsonDecoder.decode([Int: Int].self, from: rsvpsCount)
        }
        set {
            if let data = try? jsonEncoder.encode(newValue) {
                UserDefaults.standard.set(data, forKey: rsvpsCountKey)
            }
        }
    }
    
    var lastRejectNotificationsPermissionsDate: [Int: Date]? {
        get {
            guard let lastRejectNotificationsPermissionsDate = UserDefaults.standard.object(forKey: lastRejectNotificationsPermissionsDateKey) as? Data else { return nil }
            return try? jsonDecoder.decode([Int: Date].self, from: lastRejectNotificationsPermissionsDate)
        }
        set {
            if let data = try? jsonEncoder.encode(newValue) {
                UserDefaults.standard.set(data, forKey: lastRejectNotificationsPermissionsDateKey)
            }
        }
    }
    
    /**
     *Count of user entries to app*
    
     Maximum value is `IncrementAppEntryCountHelper.maxAppEntryCount`. To increment value use `IncrementAppEntryCountHelper` class.
     */
    var appEntryCount: [Int: Int]? {
        get {
            guard let appEntryCount = UserDefaults.standard.object(forKey: appEntryCountKey) as? Data else { return nil }
            return try? jsonDecoder.decode([Int: Int].self, from: appEntryCount)
        }
        set {
            if let data = try? jsonEncoder.encode(newValue) {
                UserDefaults.standard.set(data, forKey: appEntryCountKey)
            }
        }
    }
    
    var latestNotificationsEnableSettings: Bool {
        get {
            return UserDefaults.standard.bool(forKey: latestNotificationsEnableSettingsKey)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: latestNotificationsEnableSettingsKey)
        }
    }
    
    func wasOnboardingShown(for type: OnboardingType) -> Bool {
        return UserDefaults.standard.bool(forKey: type.rawValue)
    }
    
    func setOnboardingShown(for type: OnboardingType) {
        UserDefaults.standard.set(true, forKey: type.rawValue)
    }
    
    func setOnboardingUnshown(for type: OnboardingType) {
        UserDefaults.standard.set(false, forKey: type.rawValue)
    }
        
    /**
     *Count of user entries to app, stats counting when notifications allowed or day after last permission rejection date*

     Maximum value is `IncrementRsvpCountHelper.maxRsvpCount`. To increment value use `IncrementRsvpCountHelper` class.
     */
    var rsvpsForNotificationsCount: [Int: Int]? {
        get {
            guard let count = UserDefaults.standard.object(forKey: rsvpsForNotificationsCountKey) as? Data else { return nil }
            return try? jsonDecoder.decode([Int: Int].self, from: count)
        }
        set {
            if let data = try? jsonEncoder.encode(newValue) {
                UserDefaults.standard.set(data, forKey: rsvpsForNotificationsCountKey)
            }
        }
    }
    
    /**
     *Count of user entries to app, stats counting when notifications allowed or day after last permission rejection date*

     Maximum value is `IncrementAppEntryCountHelper.maxAppEntryCount`. To increment value use `IncrementAppEntryCountHelper` class.
     */
    var appEntryForNotificationsCount: [Int: Int]? {
        get {
            guard let count = UserDefaults.standard.object(forKey: appEntryForNotificationsCountKey) as? Data else { return nil }
            return try? jsonDecoder.decode([Int: Int].self, from: count)
        }
        set {
            if let data = try? jsonEncoder.encode(newValue) {
                UserDefaults.standard.set(data, forKey: appEntryForNotificationsCountKey)
            }
        }
    }
    
    var firstRejectCalendarSyncPermissionsDate: [Int: Date]? {
        get {
            guard let date = UserDefaults.standard.object(forKey: firstRejectCalendarSyncPermissionsDateKey) as? Data else { return nil }
            return try? jsonDecoder.decode([Int: Date].self, from: date)
        }
        set {
            if let data = try? jsonEncoder.encode(newValue) {
                UserDefaults.standard.set(data, forKey: firstRejectCalendarSyncPermissionsDateKey)
            }
        }
    }
    
    /**
     *Count of done rsvps by user for calendar sync*

     Maximum value is `IncrementRsvpCountHelper.maxRsvpCount`. To increment value use `IncrementRsvpCountHelper` class.
     */
    var rsvpsForCalendarSyncCount: [Int: Int]? {
        get {
            guard let count = UserDefaults.standard.object(forKey: rsvpsForCalendarSyncCountKey) as? Data else { return nil }
            return try? jsonDecoder.decode([Int: Int].self, from: count)
        }
        set {
            if let data = try? jsonEncoder.encode(newValue) {
                UserDefaults.standard.set(data, forKey: rsvpsForCalendarSyncCountKey)
            }
        }
    }
}
