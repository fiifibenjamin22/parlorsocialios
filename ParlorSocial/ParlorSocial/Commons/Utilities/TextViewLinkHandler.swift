//  TextViewLinkHandler.swift
//  ParlorSocialClub
//
//  Created on 11/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

protocol TextViewLinkHandler {
    func openUrl(_ url: URL)
}

final class DefaultTextViewLinkHandler: TextViewLinkHandler {
    
    // MARK: - Public methods
    
    func openUrl(_ url: URL) {
        if canOpenDeepLinkInApp(usingStringUrl: url.absoluteString) {
            let activity = NSUserActivity(activityType: "InAppDeepLink")
            activity.webpageURL = url
            DeepLinksHandler.shared.handleDeepLinkCallback(for: activity, appState: .active)
        } else {
            openUrlInWebView(url: url)
        }
    }
    
    // MARK: - Private methods
    
    private func openUrlInWebView(url: URL) {
        guard let topViewController = UIApplication.shared.windows.first?.rootViewController?.top, let topAppViewController = topViewController as? AppViewController else {
            return
        }
        let data = BaseWebViewData(navBarTitle: nil, url: url)
        let controller = BaseWebViewController(data: data).embedInNavigationController()
        controller.modalPresentationStyle = .currentContext
        
        topAppViewController.present(controller, animated: true)
    }
    
    private func canOpenDeepLinkInApp(usingStringUrl stringUrl: String) -> Bool {
        guard stringUrl.contains(Config.landingDomain) else { return false }
        
        for openableLink in DeepLinksHandler.DeepLinkType.inAppOpenableDeepLinks {
            if stringUrl.contains(openableLink.rawValue) {
                return true
            }
        }
        
        return false
    }
}
