//
//  ValidatedOutput.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 03/02/2020.
//

import Foundation

struct ValidatedOutput {
    let name: String
    let errorMessages: [String]
    let isValid: Bool
}
