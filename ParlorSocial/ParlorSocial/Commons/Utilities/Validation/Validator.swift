//
//  Validator.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 03/02/2020.
//

import Foundation

class Validator {
    enum Result {
        case success
        case failure(appError: AppError)
    }
    
    func validate<T>(validatedInput: [ValidatedInput<T>]) -> [ValidatedOutput] {
        var validatedOutputs: [ValidatedOutput] = []
        validatedInput.forEach { validatedInput in
            let errorMessages = validatedInput.rules.filter { !$0.check(validatedInput.value) }.map { $0.errorMessage }
            validatedOutputs.append(ValidatedOutput(name: validatedInput.name, errorMessages: errorMessages, isValid: errorMessages.isEmpty))
        }
        return validatedOutputs
    }
    
    func validateWithResult<T>(for validatedInput: [ValidatedInput<T>]) -> Result {
        let validatedOutputs = validate(validatedInput: validatedInput)
        return getResultFrom(validatedOutputs: validatedOutputs)
    }
    
    func getResultFrom(validatedOutputs: [ValidatedOutput]) -> Result {
        let notValidOutputs = validatedOutputs.filter { !$0.isValid }
        if notValidOutputs.isEmpty {
            return .success
        } else {
            return .failure(appError: AppError(from: validatedOutputs))
        }
    }
}
