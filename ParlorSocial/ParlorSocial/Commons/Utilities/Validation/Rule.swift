//
//  Rule.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 03/02/2020.
//

import Foundation

class Rule<T> {
    var check: (T) -> Bool = { _ in false }
    var errorMessage: String = ""
}

class StringNotBlank: Rule<String?> {
    override init() {
        super.init()
        check = { !$0.isBlank }
        errorMessage = Strings.Validation.notBeEmpty.localized
    }
}

class StringMaxLength: Rule<String?> {
    init(_ length: Int) {
        super.init()
        check = { return $0?.count ?? 0 <= length }
        errorMessage = String(format: Strings.Validation.maxLength.localized, length)
    }
}

class StringMinLength: Rule<String?> {
    init(_ length: Int) {
        super.init()
        check = { return $0?.count ?? 0 >= length }
        errorMessage = String(format: Strings.Validation.minLength.localized, length)
    }
}

class StringPasswordPower: Rule<String?> {
    override init() {
        super.init()
        let regex = "^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,}$"
        check = {
            return $0?.range(of: regex, options: .regularExpression) != nil
        }
        errorMessage = Strings.Validation.passwordPower.localized
    }
}

class IntNotNull: Rule<Int?> {
    override init() {
        super.init()
        check = { $0 != nil }
        errorMessage = Strings.Validation.notBeEmpty.localized
    }
}

class DateMinAge: Rule<Date?> {
    init(_ value: Int) {
        super.init()
        check = {
            guard let startDate = $0 else { return false }
            let years = Date.yearsBetweenDate(startDate: startDate, endDate: Date())
            return years ?? 0 >= value
        }
        errorMessage = String(format: Strings.Validation.dateMustBeLeastYears.localized, value)
    }
}

class CardMonthIsAfterCurrent: Rule<Int?> {
    init(year: Int?) {
        super.init()
        check = { month in
            let components = Calendar.current.dateComponents([.month, .year], from: Date())
            guard let currentYear = components.year,
                let currentMonth = components.month,
                let validatedMonth = month else { return false }
            guard var validatedYear = year else { return true }
            validatedYear += 2000
            if currentYear != validatedYear {
                return true
            } else {
                return validatedMonth >= currentMonth
            }
        }
        errorMessage = Strings.Validation.monthCardInvalid.localized
    }
}

class CardYearIsAfterCurrent: Rule<Int?> {
    override init() {
        super.init()
        check = { year in
            let components = Calendar.current.dateComponents([.year], from: Date())
            guard let currentYear = components.year,
                var validatedYear = year else { return false }
            validatedYear += 2000
            return validatedYear >= currentYear
        }
        errorMessage = Strings.Validation.yearCardInvalid.localized
    }
}
