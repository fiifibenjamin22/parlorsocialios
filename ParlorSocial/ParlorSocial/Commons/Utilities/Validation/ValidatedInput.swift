//
//  ValidatedInput.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 03/02/2020.
//

import Foundation

struct ValidatedInput<T> {
    let rules: [Rule<T>]
    let value: T
    let name: String
}
