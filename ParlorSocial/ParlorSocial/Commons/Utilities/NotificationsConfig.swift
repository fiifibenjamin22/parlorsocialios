//  NotificationsConfig.swift
//  ParlorSocialClub
//
//  Created on 29/04/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

struct NotificationsConfig {
    
    // MARK: - Private properties
    
    private static var userDefaultsWorker: UserDefaultsWorker = {
        return UserDefaultsWorker()
    }()
    
    // MARK: - Public properties
    
    static let minDaysAfterLastReminderRejection: Int = 1
    
    /**
     *Enable notifications remind level*
    
      `zero` it is set when user has not set permissions yet or has set on authorized
      `first` it is set when user reject notifications permissions first time
      `second` it is set when user reject notifications permissions second time
      `third` it is set when user reject notifications permissions third time
     */
    static var enableNotificationsRemindLevel: EnableNotificationsSettingsRemindLevel? {
        get {
            guard let id = Config.userProfile?.id else { return nil }
            return userDefaultsWorker.enableNotificationsRemindLevel?[id]
        }
        set {
            guard let id = Config.userProfile?.id else { return }
            var enableNotificationsRemindLevelDict = userDefaultsWorker.enableNotificationsRemindLevel ?? [:]
            enableNotificationsRemindLevelDict[id] = newValue
            userDefaultsWorker.enableNotificationsRemindLevel = enableNotificationsRemindLevelDict
        }
    }

    /// Date of last permission rejection for remote notifcations
    static var lastRejectNotificationsPermissionsDate: Date? {
        get {
            guard let id = Config.userProfile?.id else { return nil }
            return userDefaultsWorker.lastRejectNotificationsPermissionsDate?[id]
        }
        set {
            guard let id = Config.userProfile?.id else { return }
            var lastRejectNotificationsPermissionsDateDict = userDefaultsWorker.lastRejectNotificationsPermissionsDate ?? [:]
            lastRejectNotificationsPermissionsDateDict[id] = newValue
            userDefaultsWorker.lastRejectNotificationsPermissionsDate = lastRejectNotificationsPermissionsDateDict
        }
    }
    
    /**
     *Count of done rsvps by user, stats counting when notifications allowed or day after last permission rejection date*
    
     Maximum value is `IncrementRsvpCountHelper.maxRsvpCount`. To increment value use `IncrementRsvpCountHelper` class.
     */
    static var rsvpsCount: Int {
        get {
            guard let id = Config.userProfile?.id else { return 0 }
            return userDefaultsWorker.rsvpsForNotificationsCount?[id] ?? 0
        }
        set {
            guard let id = Config.userProfile?.id else { return }
            var rsvpsCountDict = userDefaultsWorker.rsvpsForNotificationsCount ?? [:]
            rsvpsCountDict[id] = newValue
            userDefaultsWorker.rsvpsForNotificationsCount = rsvpsCountDict
        }
    }
    
    /**
     *Count of user entries to app, stats counting when notifications allowed or day after last permission rejection date*
    
     Maximum value is `IncrementAppEntryCountHelper.maxAppEntryCount`. To increment value use `IncrementAppEntryCountHelper` class.
     */
    static var appEntryCount: Int {
        get {
            guard let id = Config.userProfile?.id else { return 0 }
            return userDefaultsWorker.appEntryForNotificationsCount?[id] ?? 0
        }
        set {
            guard let id = Config.userProfile?.id else { return }
            var appEntryCountDict = userDefaultsWorker.appEntryForNotificationsCount ?? [:]
            appEntryCountDict[id] = newValue
            userDefaultsWorker.appEntryForNotificationsCount = appEntryCountDict
        }
    }
    
    static var latestNotificationsEnableSettings: Bool {
        get {
            return userDefaultsWorker.latestNotificationsEnableSettings
        }
        set {
            userDefaultsWorker.latestNotificationsEnableSettings = newValue
        }
    }
}
