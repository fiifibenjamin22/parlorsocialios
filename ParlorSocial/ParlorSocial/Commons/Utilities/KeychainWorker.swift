//
//  KeychainWorker.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 26/02/2019.
//

import KeychainAccess

private var TOKEN_KEY: String { return "token" }
private var REFRESH_TOKEN_KEY: String { return "rtoken" }
private var ENC_KEY: String { return "enc_key" }

final class KeychainWorker {
    
    var keychainKey: String {
        return "pl.kissdigital.ParlorSocial"
    }
    
    lazy var keychain = Keychain(service: keychainKey)
    
    var isTokenPresent: Bool {
        return userToken != nil
    }
    
    func saveUserToken(_ token: String) {
        save(token, atKey: TOKEN_KEY)
    }
    
    func saveRefreshToken(_ token: String) {
        save(token, atKey: REFRESH_TOKEN_KEY)
    }
    
    func save(_ item: String, atKey key: String) {
        DispatchQueue.global(qos: .utility).sync {
            do {
                try self.keychain.set(item, key: key)
            } catch {
                fatalError(error.localizedDescription)
            }
        }
    }
    
    func removeTokenFromKeychain() {
        do {
            try keychain.remove(TOKEN_KEY)
            try keychain.remove(REFRESH_TOKEN_KEY)
        } catch {
            print("Unable to remove token from keychain: \(error)")
        }
    }
    
    var userToken: String? {
        do {
            return try keychain.get(TOKEN_KEY)
        } catch {
            return nil
        }
    }
    
    var refreshToken: String? {
        do {
            return try keychain.get(REFRESH_TOKEN_KEY)
        } catch {
            return nil
        }
    }
}
