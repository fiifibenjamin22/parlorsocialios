//
//  Typealiases.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 26/02/2019.
//

import Foundation
import RxSwift

typealias JSON = [String: Any]
typealias FailureResponse = (AppError) -> Void
typealias Function = () -> Void

typealias MessageApiResponse = Observable<ApiResponse<MessageResponse>>

typealias ActivationsResponseObs = Observable<ApiResponse<Activations.Get.Response>>

typealias MyConnectionsResponseObs = Observable<ApiResponse<MyConnections.Get.Response>>
typealias ConnectionRequestsResponseObs = Observable<ApiResponse<ConnectionRequests.Get.Response>>

typealias HomeActivationsApiResposne = ApiResponse<HomeActivations.Get.Response>
typealias HomeActivationsResponseObs = Observable<ApiResponse<HomeActivations.Get.Response>>
typealias SingleHomeActivationResposne = ApiResponse<DataResponse<HomeActivation>>

typealias HomeRsvpsResponseObs = Observable<ApiResponse<HomeRsvps.Get.Response>>
typealias SingleHomeRsvpResposne = ApiResponse<DataResponse<HomeRsvp>>

typealias LocationsResponseObs = Observable<ApiResponse<Locations.Get.Response>>

typealias SingleActivationResposne = ApiResponse<DataResponse<Activation>>
typealias ActivationInfoResposne = ApiResponse<DataResponse<ActivationInfo>>
typealias ActivationUpdatesResponseObs = Observable<ApiResponse<ActivationUpdates.Get.Response>>
typealias ShareDataResponse = ApiResponse<DataResponse<ShareData>>
typealias AvailableSlotsResponse = ApiResponse<DataResponse<AvailableSlots>>
typealias WelcomeScreensResposne = ApiResponse<DataResponse<[WelcomeScreenData]>>

typealias ProfileResponse = ApiResponse<DataResponse<Profile>>
typealias ProfileMessageResponse = ApiResponse<DataMessageResponse<Profile>>
typealias ChangePasswordResponse = ApiResponse<ChangePasswordResponseData>
typealias SettingsResponse = ApiResponse<DataResponse<GlobalSettings>>
typealias PlanInfoResponse = ApiResponse<DataResponse<PlanData>>
typealias MembershipPlansResponse = ApiResponse<DataResponse<[PlanData]>>

typealias PaginatedData = (sections: [TableSection], metadata: ListMetadata)
typealias SectionPaginatedRepositoryState = RepositoryPaginatedDataState<TableSection>

typealias SingleCardResponse = ApiResponse<DataResponse<CardData>>
typealias CardsResponse = ApiResponse<DataResponse<[CardData]>>
typealias CardDetailsResponse = ApiResponse<DataResponse<CardDataDetails>>
typealias InterestGroupsResposne = ApiResponse<PaginatedResponse<InterestGroup>>

typealias GuestListRsvpResponse = ApiResponse<DataResponse<[GuestListGuest]>>
typealias GuestListRsvpMetaResponse = ApiResponse<GetGuestListMetaResponse>

typealias RsvpResponse = ApiResponse<DataResponse<Rsvp>>
typealias CirclesResposne = ApiResponse<DataResponse<[Circle]>>
typealias MyInterestGroupsResponse = ApiResponse<DataResponse<[InterestGroup]>>

typealias AnnouncementsApiResponse = ApiResponse<PaginatedResponse<Announcement>>
typealias AnnouncementsInfoApiResponse = ApiResponse<DataResponse<AnnouncementsInfo>>
typealias AnnouncementApiResponse = ApiResponse<DataResponse<Announcement>>

typealias SearchApiResponse = ApiResponse<DataResponse<[SearchData]>>
typealias DatesApiResponse = ApiResponse<DataResponse<[Date]>>

typealias AppUpdateInfoResponse = ApiResponse<DataResponse<AppUpdateInfoData>>

typealias ConnectionChangeApiResponseObs = Observable<ApiResponse<DataResponse<ConnectionResponse>>>
