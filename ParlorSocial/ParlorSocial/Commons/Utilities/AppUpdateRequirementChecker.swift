//  AppUpdateRequirementChecker.swift
//  ParlorSocialClub
//
//  Created on 08/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import RxSwift

final class AppUpdateRequirementChecker {
    
    // MARK: - Properties
    
    private let repository: AppSettingsRepositoryProtocol
    
    // MARK: - Init
    
    init(repository: AppSettingsRepositoryProtocol = AppSettingsRepository()) {
        self.repository = repository
    }
    
    // MARK: - Public methods
    
    func loadAppVersionInformationIfNeeded() -> Observable<Bool> {
        return repository.getAppVersionInfo()
            .compactMap { response -> AppUpdateInfoData? in
                switch response {
                case .success(let data): return data.data
                case .failure: return nil
                }
            }
            .doOnNext { Config.appUpdateInfo = $0 }
            .map { $0?.appUpdateStrategy == .required }
    }
}
