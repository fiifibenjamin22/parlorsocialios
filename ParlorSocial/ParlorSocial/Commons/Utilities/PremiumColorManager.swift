//
//  PremiumColorManager.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 19/06/2019.
//

import UIKit
import RxSwift

final class PremiumColorManager {
    
    static let shared = PremiumColorManager()
    
    private let disposeBag = DisposeBag()
    var currentTheme: Theme = (Config.userProfile?.isPremium ?? false) ? .premium : .nonPremium
    
    private init() {}
    
    func theme(from profile: Profile) -> Theme {
        if profile.isPremium {
            currentTheme = .premium
        } else {
            currentTheme = .nonPremium
        }
        return currentTheme
    }
}

extension PremiumColorManager {
    enum Theme {
        case premium
        case nonPremium
        
        // MARK: - Images
        
        var placeholderPhoto: UIImage {
            switch self {
            case .nonPremium:
                return Icons.Common.placeholderPhoto
            case .premium:
                return Icons.Common.placeholderPhotoBlack
            }
        }
        
        // MARK: - Styles
        
        var footerTableViewStyle: FooterStyle {
            switch self {
            case .nonPremium:
                return .light
            default:
                return .dark
            }
        }
        
        // MARK: - Colors
        
        var statusBarColor: UIColor {
            switch self {
            case .nonPremium:
                return .white
            case .premium:
                return .black
            }
        }
        
        var progressBarColor: UIColor {
            switch self {
            case .nonPremium:
                return .black
            case .premium:
                return .white
            }
        }
        
        var profileBackgroundColor: UIColor {
            switch self {
            case .nonPremium:
                return .white
            case .premium:
                return .appText
            }
        }
        
        var profileNameTextColor: UIColor {
            switch self {
            case .nonPremium:
                return .appTextMediumBright
            case .premium:
                return .white
            }
        }
        
        var profilePositionTextColor: UIColor {
            switch self {
            case .nonPremium:
                return .appTextMediumBright
            case .premium:
                return .appSeparator
            }
        }
        
        var roundedButtonBorderColor: UIColor {
            switch self {
            case .nonPremium:
                return .appSeparator
            case .premium:
                return .appTextBright
            }
        }
        
        var roundedButtonTextColor: UIColor {
            switch self {
            case .nonPremium:
                return .appText
            case .premium:
                return .white
            }
        }
        
        var separatorColor: UIColor {
            switch self {
            case .nonPremium:
                return .textVeryLight
            case .premium:
                return .backgroundMediumDark
            }
        }
        
        var tabButtonsSeparator: UIColor {
            switch self {
            case .nonPremium:
                return .appSeparator
            case .premium:
                return .backgroundMediumDark
            }
        }
        
        var selectedTabButtonColor: UIColor {
            switch self {
            case .nonPremium:
                return .appTextMediumBright
            case .premium:
                return .white
            }
        }
        
        var unselectedTabButtonColor: UIColor {
            switch self {
            case .nonPremium:
                return .appTextMediumBright
            case .premium:
                return .eventTime
            }
        }
        
        var profileTabHeaderLabelColor: UIColor {
            switch self {
            case .nonPremium:
                return .appTextSemiLight
            case .premium:
                return .eventTime
            }
        }
        
        var profileMainTableViewBackgroundColor: UIColor {
            switch self {
            case .nonPremium:
                return .appBackground
            case .premium:
                return .backgroundMediumDark
            }
        }
        
        var profileItemBackgroundColor: UIColor {
            switch self {
            case .nonPremium:
                return .white
            case .premium:
                return .appTextBright
            }
        }
        
        var profileItemContentTextColor: UIColor {
            switch self {
            case .nonPremium:
                return .appTextMediumBright
            case .premium:
                return .appTextWhite
            }
        }
        
        var profileEmptyCircleTextColor: UIColor {
            switch self {
            case .nonPremium:
                return .appTextSemiLight
            case .premium:
                return .appTextWhite
            }
        }
        
        var profileItemNameTextColor: UIColor {
            switch self {
            case .nonPremium:
                return .textFieldUnderline
            case .premium:
                return .eventTime
            }
        }
        
        var progressBarBackground: UIColor {
            switch self {
            case .nonPremium:
                return UIColor.white.withAlphaComponent(0.8)
            case .premium:
                return UIColor.black.withAlphaComponent(0.5)
            }
        }
        
        var indicatorColor: UIColor {
            switch self {
            case .nonPremium:
                return .black
            case .premium:
                return .white
            }
        }
        
    }
}
