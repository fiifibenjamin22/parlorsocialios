//
//  HasFilterableNavigationBar.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 12/03/2019.
//

import UIKit

protocol HasFilterableNavigationBar {
    func createNavigationBar(searchAction: Selector?, calendarAction: Selector?, categoriesAction: Selector?)
    func change(title: String?)
    var navigationTitle: String? { get }
    var navigationTitleClickable: Bool { get }
    var isNavigationBottomShadowEnable: Bool { get }
}

extension HasFilterableNavigationBar where Self: UIViewController {

    func createNavigationBar(
        searchAction: Selector? = nil,
        calendarAction: Selector? = nil,
        categoriesAction: Selector? = nil) {

        navigationItem.leftBarButtonItem = .createSearchButton(target: self, action: searchAction)
        navigationItem.rightBarButtonItem = .createCalendarButton(target: self, action: calendarAction)

        if let categoriesAction = categoriesAction {
            navigationItem.titleView = buildTitleView(with: categoriesAction)
        }

        if isNavigationBottomShadowEnable {
            navigationController?.navigationBar.addShadow(withOpacity: 0.1, radius: 10)
            navigationController?.navigationBar.shadowImage = UIImage()
        } else {
            navigationController?.navigationBar.shadowImage = UIImage()
        }
    }

    func change(title: String?) {
        titleLabel?.text = title
    }

    private var titleLabel: UILabel? {
        return titleStackView?.arrangedSubviews.first { $0 is UILabel } as? UILabel
    }

    private var titleIcon: UIImageView? {
        return titleStackView?.arrangedSubviews.first { $0 is UIImageView } as? UIImageView
    }

    private var titleStackView: UIStackView? {
        guard let titleView = navigationItem.titleView else { return nil }
        return titleView.subviews.first { $0 is UIStackView } as? UIStackView
    }

    private var titleButton: UIButton? {
        guard let titleView = navigationItem.titleView else { return nil }
        return titleView.subviews.first { $0 is UIButton } as? UIButton
    }
}

extension HasFilterableNavigationBar {

    private func buildTitleView(with clickAction: Selector) -> UIView {

        // Container View
        let titleView = UIView().manualLayoutable()
        titleView.applyTouchAnimation()

        // Title Label
        let titleLabel = ParlorLabel.styled(
            withTextColor: .black,
            withFont: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.bold),
            numberOfLines: 1,
            letterSpacing: Constants.LetterSpacing.small
        ).manualLayoutable()
        titleLabel.text = navigationTitle

        // Arrow Icon
        let icon = UIImageView(image: Icons.NavigationBar.arrowDown).manualLayoutable()
        icon.contentMode = .scaleAspectFit
        icon.isHidden = !navigationTitleClickable

        // Stack View containing title and icon
        let stackView = UIStackView(axis: .horizontal, with: [titleLabel, icon], spacing: 2)

        // Button on top of stack view
        let button = UIButton().manualLayoutable()
        button.addTarget(self, action: clickAction, for: .touchUpInside)

        // Add views to title view and add constraints
        [stackView, button].addTo(parent: titleView)
        stackView.edgesToParent(insets: .margins(top: 4))
        button.edges(equalTo: stackView)
        icon.widthAnchor.equalTo(constant: 12)

        return titleView
    }
}
