//  RefreshableView.swift
//  ParlorSocialClub
//
//  Created on 04/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

protocol RefreshableView {
    func refreshData()
}
