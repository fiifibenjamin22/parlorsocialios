//
//  ScrollableContent.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 03/04/2019.
//

import Foundation

protocol ScrollableContent {
    
    func scrollToTop()
    
}
