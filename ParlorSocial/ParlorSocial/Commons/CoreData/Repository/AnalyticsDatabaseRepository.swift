//  AnalyticsDatabaseRepository.swift
//  ParlorSocialClub
//
//  Created on 29/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation
import Crashlytics

protocol AnalyticsDatabaseRepositoryProtocol {
    func saveEvent(_ event: AnalyticEvent)
    func fetchAllEvents() -> [AnalyticEventEntity]
    func deleteEvents(_ events: [AnalyticEventEntity])
}

final class AnalyticsDatabaseRepository: AnalyticsDatabaseRepositoryProtocol {
    
    // MARK: - Properties
    
    private let database: DatabaseCRUDProtocol
    
    // MARK: - Init
    
    init(withDatabase database: DatabaseCRUDProtocol = DatabaseController()) {
        self.database = database
    }
    
    // MARK: - AnalyticsDatabaseRepositoryProtocol
    
    func saveEvent(_ event: AnalyticEvent) {
        do {
            let entity = AnalyticEventEntity(usingData: event, context: database.managedObjectContext)
            try database.insert(object: entity)
        } catch {
            logError(error, message: "Can't save analytic event: \(event)")
        }
    }
    
    func fetchAllEvents() -> [AnalyticEventEntity] {
        do {
            return try database.fetchObjects(usingFetchRequest: AnalyticEventEntity.fetchRequest())
        } catch {
            logError(error, message: "Can't fetch analytic events")
            return []
        }
    }
    
    func deleteEvents(_ events: [AnalyticEventEntity]) {
        do {
            try database.delete(objects: events)
        } catch {
            logError(error, message: "Can't fetch analytic events")
        }
    }
    
    // MARK: - Private methods
    
    private func logError(_ error: Error, message: String) {
        #if DEBUG
        print("\(message)\n\(error)")
        #else
        Crashlytics.sharedInstance().recordError(error)
        #endif
    }
}
