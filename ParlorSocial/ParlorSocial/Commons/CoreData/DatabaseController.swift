//  DatabaseController.swift
//  ParlorSocialClub
//
//  Created on 29/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import CoreData

protocol DatabaseCRUDProtocol {
    var managedObjectContext: NSManagedObjectContext { get }
    
    func insert<T: NSManagedObject>(object: T) throws
    func fetchObjects<T: NSManagedObject>(usingFetchRequest request: NSFetchRequest<T>) throws -> [T]
    func delete<T: NSManagedObject>(objects: [T]) throws
}

final class DatabaseController {
    
    // MARK: - Properties
    
    private let persistentContainer: NSPersistentContainer = NSPersistentContainer(name: "ParlorDatabaseModel")
    
    // MARK: - Init
    
    init() {
        setupPersistentContainer()
    }
    
    // MARK: - Private methods
    
    private func setupPersistentContainer() {
        persistentContainer.loadPersistentStores { _, error in
            if let error = error as NSError? {
                fatalError("Can't loadPersistentStores: \(error), \(error.userInfo)")
            }
        }
    }
}

// MARK: - CRUD

extension DatabaseController: DatabaseCRUDProtocol {
    var managedObjectContext: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    func insert<T: NSManagedObject>(object: T) throws {
        managedObjectContext.insert(object)
        try managedObjectContext.save()
    }

    func fetchObjects<T: NSManagedObject>(usingFetchRequest request: NSFetchRequest<T>) throws -> [T] {
        let objects = try managedObjectContext.fetch(request)
        return objects
    }
    
    func delete<T: NSManagedObject>(objects: [T]) throws {
        objects.forEach { managedObjectContext.delete($0) }
        try managedObjectContext.save()
    }
}
