//  AnalyticEventDataEntity.swift
//  
//
//  Created on 29/06/2020.
//  

import Foundation
import CoreData

@objc(AnalyticEventDataEntity)
public class AnalyticEventDataEntity: NSManagedObject {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<AnalyticEventDataEntity> {
        return NSFetchRequest<AnalyticEventDataEntity>(entityName: "AnalyticEventDataEntity")
    }

    @NSManaged public var name: String?
    @NSManaged public var duration: NSNumber?
    @NSManaged public var referencedId: NSNumber?
    @NSManaged public var isEnabled: Bool
    @NSManaged public var source: String?
    @NSManaged public var sourceId: NSNumber?
    @NSManaged public var event: AnalyticEventEntity?
    
    convenience init(usingData data: AnalyticEventData?, context: NSManagedObjectContext) {
        self.init(context: context)
        
        if let duration = data?.duration {
            self.duration = NSNumber(value: duration)
        }
        if let isEnabled = data?.isEnabled {
            self.isEnabled = isEnabled
        }
        if let name = data?.name {
            self.name = name
        }
        if let referencedId = data?.referencedId {
            self.referencedId = NSNumber(value: referencedId)
        }
        if let source = data?.source {
            self.source = source
        }
        if let sourceId = data?.sourceId {
            self.sourceId = NSNumber(value: sourceId)
        }
    }
}
