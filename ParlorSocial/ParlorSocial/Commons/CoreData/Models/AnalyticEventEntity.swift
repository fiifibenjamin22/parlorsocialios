//  AnalyticEventEntity.swift
//  
//
//  Created on 29/06/2020.
//

import Foundation
import CoreData

@objc(AnalyticEventEntity)
public class AnalyticEventEntity: NSManagedObject {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<AnalyticEventEntity> {
        return NSFetchRequest<AnalyticEventEntity>(entityName: "AnalyticEventEntity")
    }

    @NSManaged public var userId: NSNumber
    @NSManaged public var deviceToken: String?
    @NSManaged public var timestamp: NSNumber
    @NSManaged public var type: String
    @NSManaged public var sessionId: NSNumber
    @NSManaged public var eventData: AnalyticEventDataEntity?
    
    convenience init(usingData data: AnalyticEvent, context: NSManagedObjectContext) {
        self.init(context: context)
        
        userId = NSNumber(value: data.userId)
        deviceToken = data.deviceToken
        timestamp = NSNumber(value: data.timestamp)
        type = data.type.rawValue
        sessionId = NSNumber(value: data.sessionId)
        eventData = AnalyticEventDataEntity(usingData: data.data, context: context)
    }
}
