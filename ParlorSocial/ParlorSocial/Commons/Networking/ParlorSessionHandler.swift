//
//  ParlorOauthHandler.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 03/04/2019.
//

import Foundation
import Alamofire

class ParlorSessionHandler {
    
    private typealias RefreshCompletion = (_ succeeded: Bool, _ accessToken: String?, _ refreshToken: String?) -> Void
    
    private let sessionManager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        return SessionManager(configuration: configuration)
    }()
    
    private let authRepository = AuthRepository.shared
    private let lock = NSLock()
    private var requestsToRetry: [RequestRetryCompletion] = []
    private var isRefreshing = false
    
    // MARK: - Initialization
    
    public init() {}
    
    // MARK: - Private - Refresh Tokens
    
    private func refreshToken(using refreshToken: String, completion: @escaping RefreshCompletion) {
        guard !isRefreshing else { return }
        isRefreshing = true
        _ = authRepository.login(using: LoginRequest.usingRefreshToken(refreshToken: refreshToken))
            .subscribe(onNext: { response in
                switch response {
                case .success(let data):
                    completion(true, data.accessToken, data.refreshToken)
                case .failure:
                    completion(false, nil, nil)
                }
            })
    }
    
    private func moveToLoginScreen(with errorCode: Int) {
        DispatchQueue.main.async { [unowned self] in
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            guard let topVC = appDelegate?.window?.rootViewController?.top else { return }
            if self.canMoveToLoginScreen(topVC: topVC) {
                Config.clearTokenData()
                UIApplication.shared.keyWindow?.rootViewController?.dismiss()
                UIApplication.shared.keyWindow?.setRootViewController(LoginViewController(with: errorCode).embedInNavigationController())
            }
        }
    }
    
    private func canMoveToLoginScreen(topVC: UIViewController) -> Bool {
        guard let top = topVC as? ParlorAlertViewController else {
            return !(topVC is LoginViewController)
        }
        guard let navigationVC = top.presentingViewController as? UINavigationController else { return false }
        return (navigationVC.viewControllers.first(where: {$0 is LoginViewController}) == nil)
    }
}

// MARK: - RequestAdapter

extension ParlorSessionHandler: RequestAdapter {
    
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var request = urlRequest
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue(Config.appVersion, forHTTPHeaderField: "App-Version")
        request.setValue("ios", forHTTPHeaderField: "App-Platform")
        if let accessToken = Config.token {
            request.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        }
        if let deviceToken = Config.deviceToken {
            request.setValue(deviceToken, forHTTPHeaderField: "App-Device-Token")
        }
        return request
    }
    
}

// MARK: - RequestRetrier
extension ParlorSessionHandler: RequestRetrier {
    
    func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
        lock.lock() ; defer { lock.unlock() }
        
        guard let response = request.task?.response as? HTTPURLResponse else {
            completion(false, 0.0)
            return
        }
        
        guard response.statusCode != Config.paymentRequiredCode else {
            completion(false, 0.0) // Error connected with not paid subscription
            moveToLoginScreen(with: Config.paymentRequiredCode)
            return
        }
        
        guard response.statusCode == Config.unauthorizedCode else {
            completion(false, 0.0) // Error not connected with authorization move forward
            return
        }
        
        guard let currentRefreshToken = Config.refreshToken else {
            completion(false, 0.0)
            moveToLoginScreen(with: Config.unauthorizedCode) // no refresh token in keychain proceed to lgoin
            return
        }
        
        requestsToRetry.append(completion)
        refreshToken(using: currentRefreshToken) {  [weak self] succeeded, accessToken, refreshToken in
            guard let self = self else { return }
            self.isRefreshing = false
            self.lock.lock() ; defer { self.lock.unlock() }
            
            if let accessToken = accessToken, let refreshToken = refreshToken {
                Config.updateTokenData(withAccessToken: accessToken, refreshToken: refreshToken)
            } else {
                self.moveToLoginScreen(with: Config.unauthorizedCode)
            }
            self.requestsToRetry.forEach { $0(succeeded, 0.0) }
            self.requestsToRetry.removeAll()
        }
    }
    
}
