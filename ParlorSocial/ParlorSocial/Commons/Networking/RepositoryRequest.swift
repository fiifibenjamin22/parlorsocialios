//
//  ApiRequest.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 11/03/2019.
//

import Foundation
import Alamofire

final class RepositoryRequest {
    
    let dataRequest: DataRequest
    
    init(dataRequest: DataRequest) {
        self.dataRequest = dataRequest
    }
    
    func cancel() {
        dataRequest.cancel()
    }
}
