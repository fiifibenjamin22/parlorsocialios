//
//  SearchApiService.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 31/07/2019.
//

import Alamofire
import AlamofireNetworkActivityLogger
import RxSwift

protocol SearchApiService: class {
    func search(withPhrase query: String) -> Observable<SearchApiResponse>
}

extension ApiService: SearchApiService {
    
    func search(withPhrase query: String) -> Observable<SearchApiResponse> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.search(["q": query]))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
}
