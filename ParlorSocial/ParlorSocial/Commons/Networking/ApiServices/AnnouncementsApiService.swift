//
//  AnnouncementsApiService.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 03/07/2019.
//

import Foundation
import Alamofire
import AlamofireNetworkActivityLogger
import RxSwift

protocol AnnouncementsApiService: class {
    func getAnnouncements(atPage page: Int) -> Observable<AnnouncementsApiResponse>
    func getAnnouncement(withId id: Int) -> Observable<AnnouncementApiResponse>
    func getUnreadAnnouncementsInfo() -> Observable<AnnouncementsInfoApiResponse>
}

extension ApiService: AnnouncementsApiService {
    
    func getAnnouncement(withId id: Int) -> Observable<AnnouncementApiResponse> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.getAnnouncement(id))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func getAnnouncements(atPage page: Int) -> Observable<AnnouncementsApiResponse> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.getAnnouncements(["page": page]))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func getUnreadAnnouncementsInfo() -> Observable<AnnouncementsInfoApiResponse> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.getUnreadAnnouncementsInfo)
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
}
