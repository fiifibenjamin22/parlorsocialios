//
//  ApiService.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 27.02.2019.
//

import Alamofire

final class ApiService {
    
    static let shared: ApiService = ApiService()
    
    private init() {}
    
    lazy var manager: SessionManager = {
        let manager = Alamofire.SessionManager(configuration: URLSessionConfiguration.default)
        let handler = ParlorSessionHandler()
        manager.retrier = handler
        manager.adapter = handler
        return manager
    }()
    
    lazy var authManager: SessionManager = {
        let manager = Alamofire.SessionManager(configuration: URLSessionConfiguration.default)
        let handler = ParlorSessionHandler()
        manager.adapter = handler
        return manager
    }()
    
    var searchRequestInProgress: Bool = false
    
}
