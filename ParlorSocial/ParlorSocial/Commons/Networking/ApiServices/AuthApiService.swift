//
//  AuthApiService.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 02/04/2019.
//

import Foundation
import Alamofire
import RxSwift
import RxAlamofire

protocol AuthApiService: class {
    
    func login(using request: LoginRequest) -> Observable<ApiResponse<TokenData>>
    func registerDevice(using request: RegisterDeviceRequest) -> MessageApiResponse
    func unregisterDevice(using request: UnregisterDeviceRequest) -> MessageApiResponse
    func changeEmail(using request: String) -> MessageApiResponse
}

extension ApiService: AuthApiService {
    
    func login(using request: LoginRequest) -> Observable<ApiResponse<TokenData>> {
        return authManager
            .rx
            .request(urlRequest: ApiRouter.login(request.asDictionary))
            .responseData()
            .asApiResponseObs()
    }
    
    func registerDevice(using request: RegisterDeviceRequest) -> MessageApiResponse {
        return authManager
            .rx
            .request(urlRequest: ApiRouter.registerDevice(request.asDictionary))
            .responseData()
            .asApiResponseObs()
    }
    
    func unregisterDevice(using request: UnregisterDeviceRequest) -> MessageApiResponse {
        return authManager
            .rx
            .request(urlRequest: ApiRouter.unregisterDevice(request.asDictionary))
            .responseData()
            .asApiResponseObs()
    }
    
    func changeEmail(using request: String) -> MessageApiResponse {
        return authManager
            .rx
            .request(urlRequest: ApiRouter.confirmChangeEmail(request))
            .responseData()
            .asApiResponseObs()
    }
}
