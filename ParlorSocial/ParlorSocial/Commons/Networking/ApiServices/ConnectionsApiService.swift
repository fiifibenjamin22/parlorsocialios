//  ConnectionsApiService.swift
//  ParlorSocialClub
//
//  Created on 13/08/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import RxSwift

protocol ConnectionsApiService: class {
    func getMyConnections(using model: MyConnections.Get.Request) -> MyConnectionsResponseObs
    func getConnectionRequests(using model: ConnectionRequests.Get.Request) -> ConnectionRequestsResponseObs
    func createConnection(using data: CreateConnectionRequest) -> ConnectionChangeApiResponseObs
    func updateConnection(with connectionId: Int, using data: UpdateConnectionRequest) -> ConnectionChangeApiResponseObs
    func deleteConnection(with connectionId: Int) -> MessageApiResponse
}

extension ApiService: ConnectionsApiService {
    func getMyConnections(using model: MyConnections.Get.Request) -> MyConnectionsResponseObs {
        return manager
            .rx
            .request(urlRequest: ApiRouter.getMyConnections(model.asDictionary))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }

    func getConnectionRequests(using model: ConnectionRequests.Get.Request) -> ConnectionRequestsResponseObs {
        return manager
            .rx
            .request(urlRequest: ApiRouter.getConnectionRequests(model.asDictionary))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func createConnection(using data: CreateConnectionRequest) -> ConnectionChangeApiResponseObs {
        return manager
            .rx
            .request(urlRequest: ApiRouter.createConnection(data.asDictionary))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func deleteConnection(with connectionId: Int) -> MessageApiResponse {
        return manager
            .rx
            .request(urlRequest: ApiRouter.deleteConnection(connectionId))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }

    func updateConnection(with connectionId: Int, using data: UpdateConnectionRequest) -> ConnectionChangeApiResponseObs {
        return manager
            .rx
            .request(urlRequest: ApiRouter.updateConnection(connectionId, data.asDictionary))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
}
