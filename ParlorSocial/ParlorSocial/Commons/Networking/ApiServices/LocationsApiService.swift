//
//  LocationsApiService.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 04/04/2019.
//

import Foundation
import Alamofire
import AlamofireNetworkActivityLogger
import RxSwift

protocol LocationsApiService: class {
    func getLocations(using model: Locations.Get.Request) -> LocationsResponseObs
}

extension ApiService: LocationsApiService {
    
    func getLocations(using model: Locations.Get.Request) -> LocationsResponseObs {
        return manager
            .rx
            .request(urlRequest: ApiRouter.getLocations(model.asDictionary))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
}
