//
//  AnalyticApiService.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 07/10/2019.
//  Copyright © 2019 KISSdigital. All rights reserved.
//

import Alamofire
import AlamofireNetworkActivityLogger
import RxSwift

protocol AnalyticApiService: class {
    func sendEvents(with events: AnalyticEventRequest) -> MessageApiResponse
}

extension ApiService: AnalyticApiService {
    
    func sendEvents(with events: AnalyticEventRequest) -> MessageApiResponse {
        return manager
            .rx
            .request(urlRequest: ApiRouter.sendEvents(events.asDictionary))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
}
