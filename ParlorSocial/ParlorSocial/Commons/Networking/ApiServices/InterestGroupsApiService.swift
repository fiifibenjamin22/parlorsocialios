//
//  InterestGroupsApiService.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 09/05/2019.
//

import Foundation
import Alamofire
import AlamofireNetworkActivityLogger
import RxSwift

protocol InterestGroupsApiService: class {
    func getInterestGroups(atPage page: Int) -> Observable<InterestGroupsResposne>
    func updateInterestGroups(_ data: InterestsGroupUpdateRequest) -> MessageApiResponse
    func proposeInterestGroup(withName name: String) -> MessageApiResponse
}

extension ApiService: InterestGroupsApiService {
    
    func proposeInterestGroup(withName name: String) -> MessageApiResponse {
        let data = ProposeInterestGroupRequest(name: name).asDictionary
        return manager
            .rx
            .request(urlRequest: ApiRouter.proposeInterestGroup(data))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func getInterestGroups(atPage page: Int) -> Observable<InterestGroupsResposne> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.getInterestGroups(["page": page]))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func updateInterestGroups(_ data: InterestsGroupUpdateRequest) -> MessageApiResponse {
        return manager
            .rx
            .request(urlRequest: ApiRouter.updateInterestGroups(data.asDictionary))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
}
