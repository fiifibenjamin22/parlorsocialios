//
//  GuestListRsvpApiService.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 23/05/2019.
//

import Foundation
import Alamofire
import AlamofireNetworkActivityLogger
import RxSwift

protocol GuestListRsvpApiService: class {
    func getGuestList(forActivationWithId id: Int) -> Observable<GuestListRsvpMetaResponse>
    func sendIntroRequest(forActivationWithId: Int, data: SendIntroRequest) -> Observable<GuestListRsvpResponse>
    func checkInGuestList(forActivationWithId id: Int, data: CheckInRequest) -> Observable<RsvpResponse>
}

extension ApiService: GuestListRsvpApiService {
    func getGuestList(forActivationWithId id: Int) -> Observable<GuestListRsvpMetaResponse> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.getGuestList(id))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func sendIntroRequest(forActivationWithId id: Int, data: SendIntroRequest) -> Observable<GuestListRsvpResponse> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.introRequest(id, data.asDictionary))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func checkInGuestList(forActivationWithId id: Int, data: CheckInRequest) -> Observable<RsvpResponse> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.checkInGuestList(id, data.asDictionary))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
}
