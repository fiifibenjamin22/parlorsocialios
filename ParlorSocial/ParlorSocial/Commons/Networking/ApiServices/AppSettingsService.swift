//  AppSettingsService.swift
//  ParlorSocialClub
//
//  Created on 08/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import RxSwift

protocol AppSettingsService: class {
    func getAppVersionInfo() -> Observable<AppUpdateInfoResponse>
}

extension ApiService: AppSettingsService {
    func getAppVersionInfo() -> Observable<AppUpdateInfoResponse> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.getAppUpdateInfo)
            .responseData()
            .asApiResponseObs()
    }
}
