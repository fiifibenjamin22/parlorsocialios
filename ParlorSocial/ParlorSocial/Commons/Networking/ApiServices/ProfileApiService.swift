//
//  ProfileApiService.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 05/04/2019.
//

import Foundation
import RxSwift

protocol ProfileApiService {
    func createNewPassword(forUserWithEmail email: String) -> Observable<ApiResponse<MessageResponse>>
    func resetPassword(using requestData: ResetPasswordRequest) -> Observable<ApiResponse<MessageResponse>>
    func getWelcomeScreens() -> Observable<WelcomeScreensResposne>
    func getUserCards() -> Observable<CardsResponse>
    func changeDefaultCard(to newCardId: Int) -> Observable<SingleCardResponse>
    func getMyProfileData() -> Observable<ProfileResponse>
    func updateProfile(withData data: ProfileUpdateModel) -> Observable<ProfileResponse>
    func uploadNewPhoto(withData data: ImageUploadModel) -> Observable<ProfileResponse>
    func getMyCircles() -> Observable<CirclesResposne>
    func removeMyCircle(withId id: Int) -> Observable<EmptyResponse>
    func removeCard(withId id: Int) -> Observable<EmptyResponse>
    func getMyInterestGroups() -> Observable<MyInterestGroupsResponse>
    func getSettingsInfo() -> Observable<SettingsResponse>
    func changePassword(using data: ChangePasswordRequest) -> Observable<ChangePasswordResponse>
    func changeEmail(using data: ChangeEmailRequest) -> MessageApiResponse
    func addCard(withData data: CardUploadData) -> Observable<SingleCardResponse>
    func addCard(usingToken token: CardTokenRequest) -> Observable<SingleCardResponse>
    func inviteFriend(with data: ReferFriendData) -> MessageApiResponse
    func getCardDetails(withId id: Int) -> Observable<CardDetailsResponse>
    func editCard(withData data: EditCardRequest, cardId: Int) -> Observable<SingleCardResponse>
    func getPlanData() -> Observable<PlanInfoResponse>
    func subscribeToPremium(using request: SubscriptionRequest) -> MessageApiResponse
    func getMembershipPlans(using model: MembershipPlans.Get.Request) -> Observable<MembershipPlansResponse>
    func getPremiumPlanInfo() -> Observable<PlanInfoResponse>
}

extension ApiService: ProfileApiService {
    
    func subscribeToPremium(using request: SubscriptionRequest) -> MessageApiResponse {
        return manager
            .rx
            .request(urlRequest: ApiRouter.createPremiumSubscription(request.asDictionary))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func getPlanData() -> Observable<PlanInfoResponse> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.getPlanInfo)
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func addCard(withData data: CardUploadData) -> Observable<SingleCardResponse> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.addCard(data.asDictionary))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func addCard(usingToken token: CardTokenRequest) -> Observable<SingleCardResponse> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.addCard(token.asDictionary))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func removeCard(withId id: Int) -> Observable<EmptyResponse> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.deleteCard(id))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asEmptyResposne()
    }
    
    func changeDefaultCard(to newCardId: Int) -> Observable<SingleCardResponse> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.changeDefaultCard(newCardId))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func changePassword(using data: ChangePasswordRequest) -> Observable<ChangePasswordResponse> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.changePassword(data.asDictionary))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func changeEmail(using data: ChangeEmailRequest) -> MessageApiResponse {
        return manager
            .rx
            .request(urlRequest: ApiRouter.changeEmail(data.asDictionary))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func removeMyCircle(withId id: Int) -> Observable<EmptyResponse> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.removeMyCircle(id))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asEmptyResposne()
    }
    
    func getMyInterestGroups() -> Observable<MyInterestGroupsResponse> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.getMyInterestGroups)
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func getMyCircles() -> Observable<CirclesResposne> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.getMyCircles)
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func uploadNewPhoto(withData data: ImageUploadModel) -> Observable<ProfileResponse> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.uploadPhoto(data.asDictionary))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func updateProfile(withData data: ProfileUpdateModel) -> Observable<ProfileResponse> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.updateProfile(data.asDictionary))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func getMyProfileData() -> Observable<ProfileResponse> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.myProfile)
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func getUserCards() -> Observable<CardsResponse> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.getCards)
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func getWelcomeScreens() -> Observable<WelcomeScreensResposne> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.getWelcomeScreens([:]))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func resetPassword(using requestData: ResetPasswordRequest) -> Observable<ApiResponse<MessageResponse>> {
        return authManager
            .rx
            .request(urlRequest: ApiRouter.resetPassword(requestData.asDictionary))
            .responseData()
            .asApiResponseObs()
    }
    
    func createNewPassword(forUserWithEmail email: String) -> Observable<ApiResponse<MessageResponse>> {
        return authManager
            .rx
            .request(urlRequest: ApiRouter.createNewPassword(CreateNewPasswordRequest(email: email).asDictionary))
            .responseData()
            .asApiResponseObs()
    }
    
    func getSettingsInfo() -> Observable<SettingsResponse> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.getSettingsInfo)
            .responseData()
            .asApiResponseObs()
    }
    
    func inviteFriend(with data: ReferFriendData) -> MessageApiResponse {
        return manager
            .rx
            .request(urlRequest: ApiRouter.inviteFriend(data.asDictionary))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func getCardDetails(withId id: Int) -> Observable<CardDetailsResponse> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.getCardDetails(id))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func editCard(withData data: EditCardRequest, cardId: Int) -> Observable<SingleCardResponse> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.editCard(data.asDictionary, cardId))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func getMembershipPlans(using data: MembershipPlans.Get.Request) -> Observable<MembershipPlansResponse> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.getSubscriptionPlans(data.asDictionary))
            .responseData()
            .asApiResponseObs()
    }
    
    func getPremiumPlanInfo() -> Observable<PlanInfoResponse> {
        return manager
        .rx
        .request(urlRequest: ApiRouter.getPremiumPlanInfo)
        .responseData()
        .asApiResponseObs()
    }
}
