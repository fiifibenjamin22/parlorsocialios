//
//  ActivationsService.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 27.02.2019.
//

import Foundation
import Alamofire
import RxSwift

protocol HomeApiService: class {
    func getHomeActivations(using model: HomeActivations.Get.Request) -> HomeActivationsResponseObs
    func getHomeActivation(withId id: Int) -> Observable<SingleHomeActivationResposne>
    func getRsvps(using model: HomeRsvps.Get.Request) -> HomeRsvpsResponseObs
    func getRsvp(withId id: Int) -> Observable<SingleHomeRsvpResposne>
}

protocol ActivationsApiService: class {
    func getActivations(using model: Activations.Get.Request) -> ActivationsResponseObs
    func getActivation(withId id: Int) -> Observable<SingleActivationResposne>
    func getActivationInfo(forActivationWithId activationId: Int) -> Observable<ActivationInfoResposne>
    func getActivationUpdates(forActivationWithId activationId: Int, using model: ActivationUpdates.Get.Request) -> ActivationUpdatesResponseObs
    func getShareData(forActivationWithId id: Int) -> Observable<ShareDataResponse>
    func createRsvp(forActivationWithId activationId: Int, data: RsvpRequest) -> Observable<RsvpResponse>
    func updateRsvp(forActivationWithId activationId: Int, data: RsvpRequest) -> Observable<RsvpResponse>
    func cancelRsvp(forActivationWithId activationId: Int) -> MessageApiResponse
    func transferRsvpToGuestList(forActivationWithId activationId: Int, data: RsvpTransferRequest) -> MessageApiResponse
    func notifyMeRsvpAvailability(forActivationWithId activationId: Int) -> MessageApiResponse
    func getRsvpsStartDates() -> Observable<DatesApiResponse>
    func getAmountOfSlots(forActivationId: Int) -> Observable<AvailableSlotsResponse>
}

extension ApiService: HomeApiService {
    func getHomeActivations(using model: HomeActivations.Get.Request) -> HomeActivationsResponseObs {
        return manager
            .rx
            .request(urlRequest: ApiRouter.getHomeActivations(model.asDictionary))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }

    func getHomeActivation(withId id: Int) -> Observable<SingleHomeActivationResposne> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.getHomeActivation(id))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }

    func getRsvps(using model: HomeRsvps.Get.Request) -> HomeRsvpsResponseObs {
        return manager
            .rx
            .request(urlRequest: ApiRouter.getHomeRsvps(model.asDictionary))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }

    func getRsvp(withId id: Int) -> Observable<SingleHomeRsvpResposne> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.getHomeRsvp(id))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
}

extension ApiService: ActivationsApiService {
    
    func getRsvpsStartDates() -> Observable<DatesApiResponse> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.rsvpsStartDates)
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func updateRsvp(forActivationWithId activationId: Int, data: RsvpRequest) -> Observable<RsvpResponse> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.updateRsvp(activationId, data.asDictionary))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func transferRsvpToGuestList(forActivationWithId activationId: Int, data: RsvpTransferRequest) -> MessageApiResponse {
        return manager
            .rx
            .request(urlRequest: ApiRouter.transferToGuestList(activationId, data.asDictionary))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func createRsvp(forActivationWithId activationId: Int, data: RsvpRequest) -> Observable<RsvpResponse> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.newRsvp(activationId, data.asDictionary))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }

    func getActivations(using model: Activations.Get.Request) -> ActivationsResponseObs {
        return manager
            .rx
            .request(urlRequest: ApiRouter.getActivations(model.asDictionary))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func getActivation(withId id: Int) -> Observable<SingleActivationResposne> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.getActivation(id))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }

    func getShareData(forActivationWithId id: Int) -> Observable<ShareDataResponse> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.getActivationShareData(id))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func getActivationInfo(forActivationWithId activationId: Int) -> Observable<ActivationInfoResposne> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.getActivationInfo(activationId))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func getActivationUpdates(forActivationWithId activationId: Int, using model: ActivationUpdates.Get.Request) -> ActivationUpdatesResponseObs {
        return manager
            .rx
            .request(urlRequest: ApiRouter.getActivationUpdates(activationId, model.asDictionary))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func cancelRsvp(forActivationWithId activationId: Int) -> MessageApiResponse {
        return manager
            .rx
            .request(urlRequest: ApiRouter.cancelRsvp(activationId))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func notifyMeRsvpAvailability(forActivationWithId activationId: Int) -> MessageApiResponse {
        return manager
            .rx
            .request(urlRequest: ApiRouter.notifyMeRsvpAvailability(activationId))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
    
    func getAmountOfSlots(forActivationId: Int) -> Observable<AvailableSlotsResponse> {
        return manager
            .rx
            .request(urlRequest: ApiRouter.getAvailableSlots(forActivationId))
            .validateUserLoggedInAndPaidForSubscription()
            .responseData()
            .asApiResponseObs()
    }
}
