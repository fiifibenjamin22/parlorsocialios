//
//  ApiRouter.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 27.02.2019.
//

import Alamofire

enum ApiRouter: URLRequestConvertible {

    // Activations
    case getActivations(Parameters)
    case getHomeActivations(Parameters)
    case getHomeActivation(Int)
    case getHomeRsvps(Parameters)
    case getHomeRsvp(Int)
    case getActivation(Int)
    case getActivationInfo(Int)
    case getActivationUpdates(Int, Parameters)
    case getActivationShareData(Int)
    case newRsvp(Int, Parameters)
    case updateRsvp(Int, Parameters)
    case cancelRsvp(Int)
    case transferToGuestList(Int, Parameters)
    case notifyMeRsvpAvailability(Int)
    case rsvpsStartDates
    case getAvailableSlots(Int)
    // Connections
    case getMyConnections(Parameters)
    case getConnectionRequests(Parameters)
    case createConnection(Parameters)
    case updateConnection(Int, Parameters)
    case deleteConnection(Int)
    // Auth
    case login(Parameters)
    case registerDevice(Parameters)
    case unregisterDevice(Parameters)
    case confirmChangeEmail(String)
    // Locations
    case getLocations(Parameters)
    //Profile
    case createNewPassword(Parameters)
    case resetPassword(Parameters)
    case getWelcomeScreens(Parameters)
    case getCards
    case addCard(Parameters)
    case myProfile
    case changeDefaultCard(Int)
    case deleteCard(Int)
    case updateProfile(Parameters)
    case uploadPhoto(Parameters)
    case getMyCircles
    case removeMyCircle(Int)
    case getMyInterestGroups
    case getSettingsInfo
    case changePassword(Parameters)
    case changeEmail(Parameters)
    case inviteFriend(Parameters)
    case getCardDetails(Int)
    case editCard(Parameters, Int)
    case createPremiumSubscription(Parameters)
    case getSubscriptionPlans(Parameters)
    case getPremiumPlanInfo
    //Interest groups
    case getInterestGroups(Parameters)
    case updateInterestGroups(Parameters)
    case proposeInterestGroup(Parameters)
    //Guest list rsvp
    case getGuestList(Int)
    case introRequest(Int, Parameters)
    case checkInGuestList(Int, Parameters)
    //Announcements
    case getAnnouncements(Parameters)
    case getAnnouncement(Int)
    case getUnreadAnnouncementsInfo
    //Application
    case getPlanInfo
    //Search
    case search(Parameters)
    //Analytic
    case sendEvents(Parameters)
    //App settings
    case getAppUpdateInfo

    var baseOauthUrl: URL { return URL(string: "\(apiDomain)/oauth/")! }
    var baseUrl: URL { return URL(string: "\(apiDomain)/api/\(ApiRouter.apiVersion)/")! }
    static let apiVersion = "v2"

    var method: HTTPMethod {
        switch self {
        case .getActivations: return .get
        case .getHomeActivations: return .get
        case .getHomeRsvps: return .get
        case .login: return .post
        case .getLocations: return .get
        case .createNewPassword: return .post
        case .resetPassword: return .post
        case .getActivation: return .get
        case .getActivationInfo: return .get
        case .getActivationUpdates: return .get
        case .getHomeActivation: return .get
        case .getHomeRsvp: return .get
        case .getActivationShareData: return .get
        case .getMyConnections: return .get
        case .getConnectionRequests: return .get
        case .createConnection: return .post
        case .updateConnection: return .patch
        case .deleteConnection: return .delete
        case .getWelcomeScreens: return .get
        case .newRsvp: return .post
        case .getCards: return .get
        case .myProfile: return .get
        case .getInterestGroups: return .get
        case .updateInterestGroups: return .patch
        case .cancelRsvp: return .patch
        case .transferToGuestList: return .patch
        case .getGuestList: return .get
        case .updateProfile: return .patch
        case .uploadPhoto: return .post
        case .updateRsvp: return .patch
        case .getMyCircles: return .get
        case .introRequest: return .patch
        case .getMyInterestGroups: return .get
        case .proposeInterestGroup: return .post
        case .removeMyCircle: return .delete
        case .checkInGuestList: return .patch
        case .getSettingsInfo: return .get
        case .changePassword: return .put
        case .changeEmail: return .patch
        case .changeDefaultCard: return .patch
        case .deleteCard: return .delete
        case .addCard: return .post
        case .registerDevice: return .post
        case .unregisterDevice: return .post
        case .getAnnouncements: return .get
        case .getAnnouncement: return .get
        case .inviteFriend: return .post
        case .getCardDetails: return .get
        case .editCard: return .patch
        case .getUnreadAnnouncementsInfo: return .get
        case .notifyMeRsvpAvailability: return .post
        case .getPlanInfo: return .get
        case .createPremiumSubscription: return .post
        case .search: return .get
        case .rsvpsStartDates: return .get
        case .confirmChangeEmail: return .post
        case .sendEvents: return .post
        case .getSubscriptionPlans: return .get
        case .getAvailableSlots: return .get
        case .getPremiumPlanInfo: return .get
        case .getAppUpdateInfo: return .get
        }
    }

    var requestUrl: URL {
        return baseUrl
    }

    private var path: String {
        switch self {
        case .getActivations: return "activations"
        case .getHomeActivations: return "activations/home"
        case .getHomeActivation(let id): return "activations/home/\(id)"
        case .getHomeRsvps: return "activations/rsvps"
        case .getHomeRsvp(let id): return "activations/rsvps/\(id)"
        case .createNewPassword: return "password/create"
        case .login: return "login"
        case .getLocations: return "locations"
        case .resetPassword: return "password/reset"
        case .getActivation(let id): return "activations/\(id)"
        case .getActivationInfo(let id): return "activations/\(id)/info"
        case .getActivationUpdates(let id, _): return "activations/\(id)/updates"
        case .getActivationShareData(let id): return "activations/\(id)/share"
        case .getMyConnections: return "connections/my-connections"
        case .getConnectionRequests: return "connections/requested"
        case .createConnection: return "connections"
        case .updateConnection(let id, _): return "connections/\(id)"
        case .deleteConnection(let id): return "connections/\(id)"
        case .getWelcomeScreens: return "welcome-screens"
        case .newRsvp(let activationId, _): return "activations/\(activationId)/rsvps"
        case .updateRsvp(let activationId, _): return "activations/\(activationId)/rsvps"
        case .cancelRsvp(let activationId): return "activations/\(activationId)/rsvps/cancel"
        case .transferToGuestList(let activationId, _): return "activations/\(activationId)/rsvps/transfer"
        case .rsvpsStartDates: return "activations/rsvps-start-time"
        case .getCards: return "cards"
        case .myProfile: return "users/me"
        case .updateProfile: return "users/me/profile"
        case .getInterestGroups, .updateInterestGroups: return "interest-groups"
        case .getGuestList(let id): return "activations/\(id)/guests"
        case .uploadPhoto: return "users/me/photo"
        case .getMyCircles: return "users/me/circles"
        case .introRequest(let activationId, _): return "activations/\(activationId)/intros"
        case .getMyInterestGroups: return "users/me/interest-groups"
        case .proposeInterestGroup: return "interest-groups/propose"
        case .removeMyCircle(let id): return "users/me/circles/\(id)"
        case .checkInGuestList(let activationId, _): return "activations/\(activationId)/rsvps/checkin-v2"
        case .getSettingsInfo: return "settings"
        case .changePassword: return "users/me/password"
        case .changeEmail: return "users/me/change-email"
        case .changeDefaultCard(let id): return "cards/\(id)/default"
        case .deleteCard(let id): return "cards/\(id)"
        case .addCard: return "cards"
        case .registerDevice: return "push/register"
        case .unregisterDevice: return "push/unregister"
        case .getAnnouncements: return "announcements"
        case .getAnnouncement(let id): return "announcements/\(id)"
        case .inviteFriend: return "invite-friend"
        case .getCardDetails(let id),
             .editCard(_, let id):
            return "cards/\(id)"
        case .getUnreadAnnouncementsInfo: return "announcements/unread"
        case .notifyMeRsvpAvailability(let id): return "activations/\(id)/notify"
        case .getPlanInfo: return "subscriptions/plan"
        case .createPremiumSubscription: return "subscriptions"
        case .search: return "search"
        case .confirmChangeEmail(let subUrl): return "change-email\(subUrl)"
        case .sendEvents: return "analytics/events"
        case .getSubscriptionPlans: return "application/plans"
        case .getAvailableSlots(let id): return "activations/\(id)/available/slots"
        case .getPremiumPlanInfo: return "application/plans/premium"
        case .getAppUpdateInfo: return "app/check-update"
        }
    }

    private var apiDomain: String {
        guard let urlPath = Bundle.main.infoDictionary!["API_PATH"] as? String else {
            fatalError("API_PATH have to be placed in plist file")
        }

        return urlPath
    }

    func asURLRequest() throws -> URLRequest {
        let url = URL(string: requestUrl.appendingPathComponent(path).absoluteString.removingPercentEncoding)
            ?? requestUrl.appendingPathComponent(path)
        let urlRequest = try URLRequest(url: url, method: method)

        switch self {
        case .getActivations(let parameters),
             .getHomeActivations(let parameters),
             .getHomeRsvps(let parameters),
             .getActivationUpdates(_, let parameters),
             .getMyConnections(let parameters),
             .getConnectionRequests(let parameters),
             .getLocations(let parameters),
             .getInterestGroups(let parameters),
             .getAnnouncements(let parameters),
             .search(let parameters),
             .getWelcomeScreens(let parameters),
             .getSubscriptionPlans(let parameters):
            return try URLEncoding.default.encode(urlRequest, with: parameters)
        case .login(let parameters),
             .createNewPassword(let parameters),
             .resetPassword(let parameters),
             .newRsvp(_, let parameters),
             .updateRsvp(_, let parameters),
             .createConnection(let parameters),
             .updateConnection(_, let parameters),
             .updateProfile(let parameters),
             .uploadPhoto(let parameters),
             .updateInterestGroups(let parameters),
             .introRequest(_, let parameters),
             .checkInGuestList(_, let parameters),
             .proposeInterestGroup(let parameters),
             .changePassword(let parameters),
             .changeEmail(let parameters),
             .addCard(let parameters),
             .registerDevice(let parameters),
             .unregisterDevice(let parameters),
             .inviteFriend(let parameters),
             .createPremiumSubscription(let parameters),
             .transferToGuestList(_, let parameters),
             .editCard(let parameters, _),
             .sendEvents(let parameters):
            return try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .getActivation:
            return try URLEncoding.default.encode(urlRequest, with: ["with": "hosts.interestGroups"])
        case .getActivationInfo,
             .getCards,
             .getActivationShareData,
             .myProfile,
             .getHomeRsvp,
             .getHomeActivation,
             .cancelRsvp,
             .getMyCircles,
             .getGuestList,
             .getMyInterestGroups,
             .removeMyCircle,
             .getSettingsInfo,
             .changeDefaultCard,
             .deleteCard,
             .getCardDetails,
             .notifyMeRsvpAvailability,
             .getPlanInfo,
             .getAnnouncement,
             .rsvpsStartDates,
             .confirmChangeEmail,
             .getUnreadAnnouncementsInfo,
             .getAvailableSlots,
             .getPremiumPlanInfo,
             .getAppUpdateInfo,
             .deleteConnection:
            return try URLEncoding.default.encode(urlRequest, with: [:])
        }
    }

}
