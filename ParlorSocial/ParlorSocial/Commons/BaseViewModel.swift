//
//  BaseViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 15/05/2019.
//

import Foundation
import RxSwift

protocol BaseViewModelLogic: ObservableSources {
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? { get }
}

protocol BaseViewModelProtocol {
    var analyticEventReferencedId: Int? { get }
    var analyticEventNavigationSource: String? { get }
    var analyticEventNavigationSourceId: Int? { get }
}

typealias BaseViewModel = BaseViewModelClass & BaseViewModelProtocol

class BaseViewModelClass: NSObject, BaseViewModelLogic {
    
    // MARK: - Properties
    
    let disposeBag = DisposeBag()
    let alertDataSubject: PublishSubject<AlertData> = PublishSubject()
    let errorSubject: PublishSubject<AppError> = PublishSubject()
    let progressBarSubject: BehaviorSubject<Bool> = BehaviorSubject(value: false)
    let closeViewSubject: PublishSubject<Void> = PublishSubject()
    let analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? = PublishSubject()
    var analyticEventNavigationSource: String?
    var analyticEventNavigationSourceId: Int?
    
    var alertDataObs: Observable<AlertData> {
        return alertDataSubject.asObservable()
    }
    
    var errorObs: Observable<AppError> {
        return errorSubject.asObservable()
    }
    
    var progressBarObs: Observable<Bool> {
        return progressBarSubject.asObservable()
    }
    
    var closeObs: Observable<Void> {
        return closeViewSubject.asObservable()
    }
    
    // MARK: - Initialization
    
    override init() {
        super.init()
        initFlow()
    }
}

// MARK: - Private methods

private extension BaseViewModelClass {
    
    private func initFlow() {
        analyticEventScreenSubject?
            .filter { $0.eventType == .screenOpened }
            .subscribe(onNext: { [unowned self] screenData in
                let baseViewModel = self as? BaseViewModel
                AnalyticService.shared.saveOpenScreenEventToUserDefaults(
                    screenData: screenData,
                    referenceId: baseViewModel?.analyticEventReferencedId,
                    navigationSource: baseViewModel?.analyticEventNavigationSource,
                    navigationSourceId: baseViewModel?.analyticEventNavigationSourceId
                )
            }).disposed(by: disposeBag)
        
        analyticEventScreenSubject?
            .pairwise()
            .filter { $0.0.eventType == .screenOpened && $0.1.eventType == .screenClosed }
            .subscribe(onNext: { [unowned self] (openedScreenData, closedScreenData) in
                let baseViewModel = self as? BaseViewModel
                AnalyticService.shared.saveCloseScreenEventToUserDefaults(
                    openScreenData: openedScreenData,
                    closeScreenData: closedScreenData,
                    referenceId: baseViewModel?.analyticEventReferencedId,
                    navigationSource: baseViewModel?.analyticEventNavigationSource,
                    navigationSourceId: baseViewModel?.analyticEventNavigationSourceId
                )
            }).disposed(by: disposeBag)
    }
}
