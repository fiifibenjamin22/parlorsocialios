//
//  AddToGuestListHelper.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 14/11/2019.
//  Copyright © 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxSwift

protocol AddToGuestListHelperProtocol {
    func checkIsAvailablePlacesInActivation(activationId: Int, guestsWithUserCount: Int)
    var successCallback: (() -> Void) { get set }
    var alertButtonCallback: (() -> Void) { get set }
}

final class AddToGuestListHelper: AddToGuestListHelperProtocol {
    
    // MARK: - Properties
    let disposeBag = DisposeBag()
    weak var viewModel: BaseViewModel?
    var checkingGuestCountIsNeeded: Bool
    var successCallback: (() -> Void) = {}
    var alertButtonCallback: (() -> Void) = {}
    
    private var guestsWithUserCount: Int?
    
    // MARK: - Initialization
    init(viewModel: BaseViewModel, checkingGuestCountIsNeeded: Bool = true) {
        self.viewModel = viewModel
        self.checkingGuestCountIsNeeded = checkingGuestCountIsNeeded
    }
    
    // MARK: - Public methods
    func checkIsAvailablePlacesInActivation(activationId: Int, guestsWithUserCount: Int) {
        guard checkingGuestCountIsNeeded else { successCallback(); return }
        self.guestsWithUserCount = guestsWithUserCount
        getAmountSlots(activationId: activationId)
    }
}

// MARK: - Private methods
private extension AddToGuestListHelper {
    
    private func handleAvailableSlots(_ response: AvailableSlotsResponse) {
        switch response {
        case .success(let response):
            checkIfOpenRsvpForm(availableSlots: response.data.availableSlots)
        case .failure(let error):
            viewModel?.progressBarSubject.onNext(false)
            viewModel?.errorSubject.onNext(error)
        }
    }
    
    private func areEnoughPlaces(availableSlots: Int) -> Bool {
        guard let guestsWithUserCount = guestsWithUserCount else { return false }
        return availableSlots >= guestsWithUserCount
    }
    
    private func checkIfOpenRsvpForm(availableSlots: Int) {
        if areEnoughPlaces(availableSlots: availableSlots) {
            successCallback()
        } else {
            viewModel?.progressBarSubject.onNext(false)
            var title: String
            switch availableSlots {
            case 0:
                title = Strings.GuestForm.fullGuestListAlert.localized
            case 1:
                title = Strings.GuestForm.fullGuestListOnlyYouAlert.localized
            default:
                title = String(format: Strings.GuestForm.fullGuestListNumberOfAvailablePlacesAlert.localized, availableSlots - 1)
            }
            viewModel?.alertDataSubject.onNext(
                AlertData.fullGuestListAlert(
                    title: title,
                    buttonAction: { [unowned self] in self.alertButtonCallback() }
            ))
        }
    }
    
    private func getAmountSlots(activationId: Int) {
        ActivationsRepository.shared.getAmountOfSlots(activationId: activationId)
        .subscribe(onNext: { [unowned self] response in
            self.handleAvailableSlots(response)
        }).disposed(by: disposeBag)
    }
    
}
