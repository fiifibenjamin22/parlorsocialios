//
//  IncrementAppEntryCountHelper.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 12/03/2020.
//

import Foundation

final class IncrementAppEntryCountHelper {
    // MARK: - Properties
    private let maxAppEntryCount = Int.max
    
    // MARK: - Public methods
    func incrementAppEntryCounters() {
        incrementMainAppEntryCounter()
        incrementAppEntryForNotificationsCounter()
    }
    
    // MARK: - Private methods
    
    private func incrementMainAppEntryCounter() {
        let currentCount = Config.appEntryCount
        guard currentCount < maxAppEntryCount else { return }
        Config.appEntryCount = currentCount + 1
        #if DEBUG
        print("INCREMENT APP ENTRY COUNT: \(currentCount + 1)")
        #endif
    }
    
    private func incrementAppEntryForNotificationsCounter() {
        let currentCount = NotificationsConfig.appEntryCount
        guard canIncrementAppEntryCounter(currentCount: currentCount) else { return }
        
        NotificationsConfig.appEntryCount = currentCount + 1
        #if DEBUG
        print("INCREMENT APP ENTRY FOR NOTIFICATIONS COUNTER: \(currentCount + 1)")
        #endif
    }
    
    private func canIncrementAppEntryCounter(currentCount: Int) -> Bool {
        let currentDate = Date()
        let daysAfterLastRejectionDate = currentDate.countOfDaysFrom(date: NotificationsConfig.lastRejectNotificationsPermissionsDate) ?? 0
        let currentLevel = NotificationsConfig.enableNotificationsRemindLevel ?? .zero
        let isMinDaysAfterFirstReminderRejection = daysAfterLastRejectionDate >= NotificationsConfig.minDaysAfterLastReminderRejection && currentLevel == .first

        return currentCount < maxAppEntryCount && (isMinDaysAfterFirstReminderRejection || currentLevel == .second || currentLevel == .third)
    }
}
