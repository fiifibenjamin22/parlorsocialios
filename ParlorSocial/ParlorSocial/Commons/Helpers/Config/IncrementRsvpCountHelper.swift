//
//  IncrementRsvpCountHelper.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 11/03/2020.
//

import Foundation

final class IncrementRsvpCountHelper {
    // MARK: - Properties
    private let maxRsvpCount = Int.max
    
    // MARK: - Public methods
    func incrementRsvpCountersAfterRsvp() {
        incrementMainRsvpCounter()
        incrementRsvpCounterForNotifications()
        incrementRsvpCounterForCalendarSync()
    }
    
    // MARK: - Private methods
    
    private func incrementMainRsvpCounter() {
        let currentCount = Config.rsvpsCount
        guard currentCount < maxRsvpCount else { return }
        Config.rsvpsCount = currentCount + 1
        #if DEBUG
        print("INCREMENT RSVP COUNT: \(currentCount + 1)")
        #endif
    }
    
    private func incrementRsvpCounterForNotifications() {
        let currentCount = NotificationsConfig.rsvpsCount
        guard canIncrementRsvpCounterForNotifications(currentCount: currentCount) else { return }
        
        NotificationsConfig.rsvpsCount = currentCount + 1
        #if DEBUG
        print("INCREMENT RSVP COUNTER FOR NOTIFICATIONS: \(currentCount + 1)")
        #endif
    }
    
    private func canIncrementRsvpCounterForNotifications(currentCount: Int) -> Bool {
        let currentDate = Date()
        let daysAfterLastRejectionDate = currentDate.countOfDaysFrom(date: NotificationsConfig.lastRejectNotificationsPermissionsDate) ?? 0
        let currentLevel = NotificationsConfig.enableNotificationsRemindLevel ?? .zero
        let isMinDaysAfterFirstReminderRejection = (daysAfterLastRejectionDate >= NotificationsConfig.minDaysAfterLastReminderRejection && currentLevel == .first)
        
        return currentCount < maxRsvpCount && (isMinDaysAfterFirstReminderRejection || currentLevel == .second || currentLevel == .third)
    }
    
    private func incrementRsvpCounterForCalendarSync() {
        let currentCount = CalendarSyncConfig.rsvpsCount
        guard canIncrementRsvpCounterForCalendarSync(currentCount: currentCount) else { return }
        
        CalendarSyncConfig.rsvpsCount = currentCount + 1
        #if DEBUG
        print("INCREMENT RSVP COUNTER FOR NOTIFICATIONS: \(currentCount + 1)")
        #endif
    }
    
    private func canIncrementRsvpCounterForCalendarSync(currentCount: Int) -> Bool {
        let currentDate = Date()
        let daysAfterFirstRejectionDate = currentDate.countOfDaysFrom(date: CalendarSyncConfig.firstRejectCalendarSyncPermissionsDate) ?? 0
        let isAfterFirstRsvp = currentCount == 0
        let isMinDaysAfterFirstReminderRejection = daysAfterFirstRejectionDate >= CalendarSyncConfig.minDaysAfterFirstReminderRejection
        
        return currentCount < maxRsvpCount && (isMinDaysAfterFirstReminderRejection || isAfterFirstRsvp)
    }
}
