//
//  ProgressIndicatorViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 02/07/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

class ProgressIndicatorViewController: AppViewController {
   
    // MARK: - Properties
    let analyticEventScreen: AnalyticEventScreen? = nil
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return nil
    }
    
    // MARK: - Views
    private(set) var progressIndicator: UIActivityIndicatorView!
    
    // MARK: - Initialization
    init(isDark: Bool = false) {
        super.init(nibName: nil, bundle: nil)
        setup(isDark: isDark)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }

    private func setup(isDark: Bool) {
        modalPresentationStyle = .overFullScreen
        modalTransitionStyle = .crossDissolve
        if isDark {
            view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            progressIndicator.color = .white
        } else {
            view.backgroundColor = UIColor.white.withAlphaComponent(0.8)
            progressIndicator.color = .black
        }
    }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        showingAnimationView()
    }
    
}

// MARK: - Private methods
extension ProgressIndicatorViewController {

    private func setupViews() {
        let builder = ProgressIndicatorViewBuilder(controller: self)
        progressIndicator = builder.buildProgressIndicator()

        builder.setupViews()
    }

    private func showingAnimationView() {
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.view.alpha = 1
        }
    }
    
}
