//
//  ProgressIndicatorViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 02/07/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class ProgressIndicatorViewBuilder {

    private unowned let controller: ProgressIndicatorViewController
    private var view: UIView! { return controller.view }

    init(controller: ProgressIndicatorViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension ProgressIndicatorViewBuilder {

    private func setupProperties() {
        view.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        view.alpha = 0
        controller.progressIndicator.color = .black
    }

    private func setupHierarchy() {
        view.addSubview(controller.progressIndicator)
    }

    private func setupAutoLayout() {
        controller.progressIndicator.apply {
            $0.centerXAnchor.equal(to: view.centerXAnchor)
            $0.centerYAnchor.equal(to: view.centerYAnchor)
        }
    }

}

// MARK: - Public build methods
extension ProgressIndicatorViewBuilder {
    
    func buildProgressIndicator() -> UIActivityIndicatorView {
        return UIActivityIndicatorView(style: .whiteLarge).manualLayoutable().apply {
            $0.startAnimating()
        }
    }
    
}
