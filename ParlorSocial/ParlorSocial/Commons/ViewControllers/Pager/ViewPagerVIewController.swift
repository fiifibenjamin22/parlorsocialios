//
//  ViewPagerVIewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 19/04/2019.
//

import Foundation
import UIKit
import RxSwift

class ViewPagerViewController<ViewModelType: PagerLogic>: AppViewController, ViewPagersViewsHolder {

    // MARK: - Properties
    let analyticEventScreen: AnalyticEventScreen? = nil
    private(set) var pageController: UIPageViewController!
    var viewModel: ViewModelType!
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return nil
    }

    // MARK: - Views
    private(set) var bottomButton: UIButton!
    private(set) var havingTroubleLabel: UILabel!
    private(set) var getHelpLabel: UILabel!
    private(set) var pageContainer: UIView!
    
    init() {
        super.init(nibName: nil, bundle: nil)
        self.pageController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    override func bindUI() {
        super.bindUI()
        bottomButton
            .rx.tap
            .bind(to: viewModel.continueClicks)
            .disposed(by: disposeBag)
    }
    
    override func observeViewModel() {
        super.observeViewModel()
        
        viewModel.currentPageObs
            .subscribe(onNext: { [weak self] page in
                self?.moveToPage(page)
            }).disposed(by: disposeBag)
        
        viewModel.alertObs
            .show(using: self)
            .disposed(by: disposeBag)
        
        viewModel
            .destinationsObs
            .subscribe(onNext: { [weak self] destination in
                self?.router.replace(with: destination)
            }).disposed(by: disposeBag)
        
        viewModel
            .progressBarObs
            .showProgressBar(using: self)
            .disposed(by: disposeBag)
    }
    
    private func moveToPage(_ page: PagerPage) {
        let currentControllers = self.pageController.viewControllers ?? []
        self.pageController.setViewControllers([page.connectedController], direction: .forward, animated: !currentControllers.isEmpty, completion: nil)
        self.bottomButton.setTitle(page.bottomButtonText.uppercased(), for: .normal)
    }
    
    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
        addChild(viewController: pageController, inside: pageContainer)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    // MARK: - Selectors
    @objc private func didTapGetHelp() {
        guard let emailAddress = Config.globalSettings?.helpEmailAddress,
            let emailSubject = Config.globalSettings?.helpEmailSubject else { return }
        let emailSettings = EmailSettings(address: emailAddress, subject: emailSubject)
        openMail(basedOn: emailSettings)
    }

}

// MARK: - Private methods
extension ViewPagerViewController {
    
    private func setupViews() {
        let builder = ViewPagerControllerViewBuilder(controller: self)
        self.bottomButton = builder.buildContinueButton()
        self.havingTroubleLabel = builder.buildHavingTroubleLabel()
        self.getHelpLabel = builder.buildGetHelpLabel()
        self.pageContainer = builder.buildPageContainerView()
        builder.setupViews()
    }
    
    private func setupPresentationLogic() {
        [havingTroubleLabel, getHelpLabel].forEach {
            $0.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapGetHelp)))
        }
    }
    
}
