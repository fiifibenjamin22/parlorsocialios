//
//  PagerLogic.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 19/04/2019.
//

import Foundation
import RxSwift

protocol PagerLogic {
    var continueClicks: PublishSubject<Void> { get }

    var alertObs: Observable<AlertData> { get }
    var currentPageObs: Observable<PagerPage> { get }
    var destinationsObs: Observable<ResetPasswordDestination> { get }
    var progressBarObs: Observable<Bool> { get }
}
