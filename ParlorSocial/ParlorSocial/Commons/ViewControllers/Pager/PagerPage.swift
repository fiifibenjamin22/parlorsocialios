//
//  PagerPager.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 19/04/2019.
//

import Foundation

protocol PagerPage {
    var connectedController: AppViewController { get }
    var bottomButtonText: String { get }
}
