//
//  BaseViewPagerViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 19/04/2019.
//

import Foundation
import RxSwift

class ViewPagerPageViewModel<PageType: PagerPage> {
    
    let disposeBag = DisposeBag()
    
    init(withInitialPage initialPage: PageType) {
        self.showingPageRelay = BehaviorSubject(value: initialPage)
    }
    
    // MARK: public relays
    let continueClicks: PublishSubject<Void> = PublishSubject()
    
    // MARK: private relays
    let showingPageRelay: BehaviorSubject<PageType>
    let alertRelay: PublishSubject<AlertData> = PublishSubject()
    let destinationRelay: PublishSubject<ResetPasswordDestination> = PublishSubject()
    let progressBarSubject: PublishSubject<Bool> = PublishSubject()
    
}

// MARK: Private logic
private extension ViewPagerPageViewModel { }

// MARK: Interface logic methods
extension ViewPagerPageViewModel: PagerLogic {
    
    var currentPageObs: Observable<PagerPage> {
        return showingPageRelay.asObservable().map { $0 }
    }

    var alertObs: Observable<AlertData> {
        return alertRelay.asObservable()
    }
    
    var destinationsObs: Observable<ResetPasswordDestination> {
        return destinationRelay.asObservable()
    }
    
    var progressBarObs: Observable<Bool> {
        return progressBarSubject.asObservable()
    }
    
}
