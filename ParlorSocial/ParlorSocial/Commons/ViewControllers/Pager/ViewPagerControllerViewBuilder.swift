//
//  ResetPasswordViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 04/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

protocol ViewPagersViewsHolder: NSObject {
    var bottomButton: UIButton! { get }
    var havingTroubleLabel: UILabel! { get }
    var getHelpLabel: UILabel! { get }
    var pageContainer: UIView! { get }
    var view: UIView! { get }
}

class ViewPagerControllerViewBuilder {

    private unowned let controller: ViewPagersViewsHolder
    private var view: UIView! { return controller.view }
    
    private weak var havingTroubleStack: UIStackView?

    init(controller: ViewPagersViewsHolder) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension ViewPagerControllerViewBuilder {

    private func setupProperties() {
        view.backgroundColor = .white
        controller.apply {
            $0.havingTroubleLabel.text = Strings.ResetPassword.havingTrouble.localized
            $0.getHelpLabel.text = Strings.ResetPassword.getHelp.localized
            $0.bottomButton.setTitle(Strings.ResetPassword.continueButton.localized.uppercased(), for: .normal)
        }
    }

    private func setupHierarchy() {
        addHavingTroubleStack()
        controller.apply {
            view.addSubviews([$0.pageContainer, $0.bottomButton])
        }
    }

    private func setupAutoLayout() {
        havingTroubleStack?.centerXAnchor.equal(to: view.centerXAnchor)
        havingTroubleStack?.bottomAnchor.equal(to: view.bottomAnchor, constant: -Constants.ResetPassword.bottomMargin)
        
        controller.apply {
            $0.bottomButton.leadingAnchor.equal(to: view.leadingAnchor, constant: Constants.horizontalMarginBig)
            $0.bottomButton.trailingAnchor.equal(to: view.trailingAnchor, constant: -Constants.horizontalMarginBig)
            $0.bottomButton.bottomAnchor.equal(to: havingTroubleStack!.topAnchor, constant: -Constants.ResetPassword.continueBottomMargin)
            $0.bottomButton.heightAnchor.equalTo(constant: Constants.bigButtonHeight)
            
            $0.pageContainer.edgesToParent(anchors: [.leading, .trailing])
            $0.pageContainer.topAnchor.equal(to: view.safeAreaLayoutGuide.topAnchor, constant: Constants.ResetPassword.topMargin)
            $0.pageContainer.bottomAnchor.equal(to: $0.bottomButton.topAnchor)
        }
    }
    
    private func addHavingTroubleStack() {
        let stack = UIStackView(arrangedSubviews: [controller.havingTroubleLabel, controller.getHelpLabel]).manualLayoutable()
        stack.axis = .horizontal
        stack.spacing = Constants.Margin.tiny
        view.addSubview(stack)
        self.havingTroubleStack = stack
    }

}

// MARK: - Public build methods
extension ViewPagerControllerViewBuilder {
    
    func buildContinueButton() -> RoundedButton {
        return RoundedButton().manualLayoutable().apply { it in
            it.backgroundColor = UIColor.black
            it.setTitleColor(UIColor.white, for: .normal)
            it.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium)
            it.applyTouchAnimation()
        }
    }
    
    func buildHavingTroubleLabel() -> UILabel {
        return UILabel().manualLayoutable().apply {
            $0.textColor = UIColor.appTextSemiLight
            $0.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.regular)
        }
    }
    
    func buildGetHelpLabel() -> UILabel {
        return UILabel().manualLayoutable().apply {
            $0.textColor = UIColor.textGreen
            $0.applyTouchAnimation()
            $0.isUserInteractionEnabled = true
            $0.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.bold)
        }
    }
    
    func buildPageContainerView() -> UIView {
        return UIView().manualLayoutable()
    }

}

private extension Constants {
    
    enum ResetPassword {
        static let topMargin: CGFloat = UIScreen.main.sizeType == .iPhones_5_5s_5c_SE ? 40 : 80
        static let bottomMargin: CGFloat = 39
        static let continueBottomMargin: CGFloat = 35
    }
}
