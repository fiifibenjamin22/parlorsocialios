//
//  FilterableViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 12/03/2019.
//

import UIKit
import RxSwift

@available(*, deprecated, message: "Should be removed in next task from Parlor 2.0. Use NewFilterableViewController.")
class FilterableViewController: AppViewController {
    var analyticEventScreen: AnalyticEventScreen? {
        return nil
    }
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return nil
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        createNavigationBar(
            searchAction: #selector(didTapSearchBarButtonItem),
            calendarAction: #selector(didTapCalendarBarButtonItem),
            categoriesAction: #selector(didTapCategoriesBarButtonItem)
        )
    }
    
    var controllerTitle: String? { return nil }
    
    var setTitleAsButton: Bool { return false }
    
    var setBottomShadow: Bool { return true }
}

extension FilterableViewController: HasFilterableNavigationBar {
    
    var navigationTitle: String? { return controllerTitle }
    
    var navigationTitleClickable: Bool { return setTitleAsButton }
    
    var isNavigationBottomShadowEnable: Bool { return setBottomShadow }
    
    @objc func didTapSearchBarButtonItem() {
        if Config.userProfile?.isPreview ?? false {
            self.router.push(destination: FilterableDestination.membershipPlans)
        } else {
            self.router.present(destination: FilterableDestination.search, animated: true, withStyle: .fullScreen)
        }
    }

    @objc func didTapCalendarBarButtonItem() {
        if Config.userProfile?.isPreview ?? false {
            self.router.push(destination: FilterableDestination.membershipPlans)
        } else {
            self.router.present(destination: FilterableDestination.calendar, animated: true, withStyle: .fullScreen)
        }
    }

    @objc func didTapCategoriesBarButtonItem(sender: UIControl) {}
}

