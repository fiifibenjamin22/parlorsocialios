//
//  FilterableDestination.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 16/07/2019.
//

import UIKit

enum FilterableDestination {
    case calendar
    case search
    case membershipPlans
}

extension FilterableDestination: Destination {
    
    var viewController: UIViewController {
        switch self {
        case .calendar:
            return CalendarViewController().embedInNavigationController()
        case .search:
            return MainSearchViewController().embedInNavigationController()
        case .membershipPlans:
            return MembershipPlansViewController()
        }
    }
    
}
