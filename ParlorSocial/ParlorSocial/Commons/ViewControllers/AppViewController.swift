//
//  AppViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 26/02/2019.
//

import UIKit
import RxSwift
import MessageUI
import SloppySwiper

protocol ObservableSources {
    var alertDataObs: Observable<AlertData> { get }
    var errorObs: Observable<AppError> { get }
    var progressBarObs: Observable<Bool> { get }
    var closeObs: Observable<Void> { get }
}

protocol AppViewProtocol {
    var analyticEventScreen: AnalyticEventScreen? { get }
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? { get }
}

typealias AppViewController = AppViewControllerClass & AppViewProtocol

class AppViewControllerClass: UIViewController {
    
    private lazy var swiper: SloppySwiper = SloppySwiper(navigationController: self.navigationController) /// Adds swipe back gesture on whole screen
    lazy var disposeBag = DisposeBag()
    lazy var router = Router(viewController: self)
    var onboardingViewController: OnboardingViewController?
    
    var includeNavBarSafeAreaInsets: Bool {
        return true
    }
    
    var connectedOnboardingType: OnboardingType? {
        return nil
    }
    
    var observableSources: ObservableSources? {
        return nil
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.bindUI()
        self.observeViewModel()
        self.navigationController?.delegate = swiper
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showOnboardingIfNeeded()
        registerNotificationCenter()
        logAnalyticScreenEvent(type: .screenOpened)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unregisterNotificationCenter()
        logAnalyticScreenEvent(type: .screenClosed)
    }
    
    func observeViewModel() {
        observableSources?.alertDataObs.show(using: self).disposed(by: disposeBag)
        observableSources?.errorObs.handleWithAlerts(using: self).disposed(by: disposeBag)
        observableSources?.progressBarObs.showProgressBar(using: self).disposed(by: disposeBag)
        observableSources?.closeObs.subscribe(onNext: { [unowned self] _ in self.dismiss() }).disposed(by: disposeBag)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if !includeNavBarSafeAreaInsets,
            let extraHeight = self.navigationController?.navigationBar.frame.height {
            self.additionalSafeAreaInsets = UIEdgeInsets(top: -extraHeight, left: 0, bottom: 0, right: 0)
        }
    }
    
    func bindUI() {}
    
    func showOnboardingIfNeeded() {
        guard let type = connectedOnboardingType, !Config.wasOnboardingShown(for: type) else { return }

        onboardingViewController = OnboardingViewController(witih: type)
        onboardingViewController?.delegate = self
        present(onboardingViewController!, animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    func openMail(basedOn data: EmailSettings) {
        guard MFMailComposeViewController.canSendMail() else {
            showOpenMailAlert(email: data.address)
            return
        }
        
        let mail = MFMailComposeViewController().apply { [weak self] in
            $0.view.tintColor = .black
            $0.mailComposeDelegate = self
            if #available(iOS 13, *) {} else {
                $0.statusBar?.backgroundColor = .white
            }
        }
        if let address = data.address {
            mail.setToRecipients([address])
        }
        if let subject = data.subject {
            mail.setSubject(subject)
        }
        present(mail, animated: true)
    }
    
}

// MARK: - Private methods
extension AppViewControllerClass {
    
    // MARK: - Present view controllers from push notifications or deep links
    func showVCAfterPushNotificationOrLinkClicked(for destination: AppViewControllerDestination) {
        router.present(destination: destination, animated: true, withStyle: .fullScreen)
    }
    
    func showAvailableGuestListPopup(activationId: Int, showGuestListWhenAvailable: Bool, animated: Bool = true) {
        let destinationVC = AvailableGuestListPopupViewController(
            activationId: activationId, delegate: self, showGuestListWhenAvailable: showGuestListWhenAvailable)
        destinationVC.modalTransitionStyle = .crossDissolve
        destinationVC.modalPresentationStyle = .overFullScreen
        destinationVC.modalPresentationCapturesStatusBarAppearance = true
        present(destinationVC, animated: animated)
    }
    
    // MARK: - Show alerts from push notifications if notification is received on active application
    func showAlertFromPushNotification(
        basedOn data: PushNotificationData,
        showMessageInTitle: Bool = false,
        buttonAction: @escaping () -> Void) {
        Observable.just(AlertData.fromPushNotificationDetails(
            data,
            buttonAction: buttonAction,
            showMessageInTitle: showMessageInTitle))
            .show(using: self)
            .disposed(by: disposeBag)
    }
    
    private func showOpenMailAlert(email: String?) {
        guard let email = email else { return }
        Observable.just(AlertData(
            icon: nil,
            title: String(format: Strings.Profile.pleaseContactUsAt.localized, email),
            message: nil,
            bottomButtonTitle: Strings.clipToBoard.localized.uppercased(),
            buttonAction: { UIPasteboard.general.string = email }))
            .show(using: self)
            .disposed(by: disposeBag)
    }
    
    func findAppTabBarInHierarchy(startingWith controller: UIViewController) -> AppTabBarController? {
        if let tabBar = controller as? AppTabBarController {
            return tabBar
        }
        if let parent = controller.parent {
            return findAppTabBarInHierarchy(startingWith: parent)
        } else {
            return nil
        }
    }
    
    private func logAnalyticScreenEvent(type: AnalyticEventType) {
        guard let appController = self as? AppViewController,
            let screen = appController.analyticEventScreen else { return }
        let data = AnalyticEventViewControllerData(eventType: type, screenName: screen, timestamp: Int64(Date().timeIntervalSince1970 * 1000))
        appController.analyticEventScreenSubject?.onNext(data)
    }
    
    private func registerNotificationCenter() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(onAppDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(onAppWillResignActive), name: UIApplication.willResignActiveNotification, object: nil)
    }
    
    private func unregisterNotificationCenter() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
        notificationCenter.removeObserver(self, name: UIApplication.willResignActiveNotification, object: nil)
    }
    
    @objc func onAppDidBecomeActive() {
        logAnalyticScreenEvent(type: .screenOpened)
    }
    
    @objc func onAppWillResignActive() {
        logAnalyticScreenEvent(type: .screenClosed)
    }
    
}

// MARK: - MFMailComposeViewControllerDelegate
extension AppViewControllerClass: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}

// MARK: - OnboardingViewControllerDelegate
extension AppViewControllerClass: OnboardingViewControllerDelegate {
    func onboardingWasClosed() {
        onboardingViewController = nil
    }
}
