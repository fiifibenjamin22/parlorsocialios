//
//  FullScreenAlertIconedMessageView.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 05/09/2019.
//

import UIKit

class FullScreenAlertIconedMessageView: UIView {
    
    let titleTopMargin: CGFloat = 25
    let messageTopMargin: CGFloat = 10
    
    let roundedIcon =  RoundedIconView(usesDefaultSize: false)
    
    private(set) lazy var titleLabel = {
        return ParlorLabel.styled(
            withTextColor: .appTextBright,
            withFont: UIFont.custom(ofSize: Constants.FontSize.hintSize, font: UIFont.Roboto.medium),
            alignment: .center,
            numberOfLines: 0
            ).apply {
                $0.lineHeight = 26
        }
    }()
    
    private(set) lazy var messageLabel = {
        return ParlorLabel.styled(
            withTextColor: .appTextSemiLight,
            withFont: UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.regular),
            alignment: .center,
            numberOfLines: 0
            ).apply {
                $0.lineHeight = 23
        }
    }()
    
    convenience init(icon: UIImage?, title: String?, message: String?) {
        self.init(frame: .zero)
        roundedIcon.iconImage = icon
        titleLabel.text = title
        messageLabel.text = message
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        let subviews = [roundedIcon, titleLabel, messageLabel]
        subviews.forEach { $0.manualLayoutable() }
        addSubviews(subviews)
        setupLayout()
    }
    
    private func setupLayout() {
        roundedIcon.topAnchor.equal(to: self.topAnchor)
        roundedIcon.heightAnchor.equalTo(constant: 70)
        roundedIcon.widthAnchor.equal(to: roundedIcon.heightAnchor)
        roundedIcon.centerXAnchor.equal(to: self.centerXAnchor)
        
        titleLabel.centerXAnchor.equal(to: self.centerXAnchor)
        titleLabel.topAnchor.equal(to: roundedIcon.bottomAnchor, constant: titleTopMargin)
        
        messageLabel.centerXAnchor.equal(to: self.centerXAnchor)
        messageLabel.topAnchor.equal(to: titleLabel.bottomAnchor, constant: messageTopMargin)
        messageLabel.edgesToParent(anchors: [.leading, .trailing, .bottom])
    }
    
}
