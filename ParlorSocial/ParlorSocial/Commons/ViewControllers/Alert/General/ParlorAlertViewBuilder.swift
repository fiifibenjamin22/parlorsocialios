//
//  ParlorAlertViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 08/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class ParlorAlertViewBuilder {

    private unowned let controller: ParlorAlertViewController
    private var view: UIView! { return controller.view }

    init(controller: ParlorAlertViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension ParlorAlertViewBuilder {

    private func setupProperties() {
        view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        
        controller.apply {
            $0.alertLogoImage.image = $0.alertData.icon
            $0.titleLabel.text = $0.alertData.title
            $0.messageLabel.text = $0.alertData.message
            $0.confirmButton.setTitle($0.alertData.bottomButtonTitle, for: .normal)
            $0.titleLabel.isHidden = $0.alertData.title == nil || ($0.alertData.title?.isEmpty ?? true)
            $0.messageLabel.isHidden = $0.alertData.message == nil || ($0.alertData.message?.isEmpty ?? true)
        }
    }

    private func setupHierarchy() {
        view.addSubview(controller.contentView)
        
        controller.apply {
            $0.contentView.addSubviews([
                $0.alertLogoImage,
                $0.scrollView,
                $0.closeButton,
                $0.confirmButton
                ])
            
            $0.scrollView.addSubview($0.contentScrollView)
            
            $0.contentScrollView.addSubview($0.labelsStack)
            
            $0.labelsStack.addArrangedSubviews([
                $0.titleLabel,
                $0.messageLabel
                ])
        }
        
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.contentView.centerYAnchor.equal(to: view.centerYAnchor)
            $0.contentView.edgesToParent(anchors: [.leading, .trailing], padding: Constants.horizontalMarginBig)
            
            $0.closeButton.edgesToParent(anchors: [.top, .trailing])
            
            $0.alertLogoImage.centerXAnchor.equal(to: view.centerXAnchor)
            $0.alertLogoImage.topAnchor.equal(to: $0.contentView.topAnchor, constant: Constants.Alert.imageTopMargin)
            
            $0.scrollView.topAnchor.equal(to: $0.alertLogoImage.bottomAnchor, constant: Constants.Alert.topStackMargin)
            $0.scrollView.edgesToParent(anchors: [.leading, .trailing], padding: Constants.Margin.large)
            
            $0.contentScrollView.edgesToParent()
            $0.contentScrollView.widthAnchor.equal(to: $0.scrollView.widthAnchor)
            
            $0.labelsStack.edgesToParent()
            
            $0.confirmButton.topAnchor.equal(to: $0.scrollView.bottomAnchor, constant: Constants.Alert.bottomButtonTopMargin)
            $0.confirmButton.bottomAnchor.equal(to: $0.contentView.bottomAnchor, constant: -Constants.Alert.bottomButtonBottomMargin)
            $0.confirmButton.centerXAnchor.equal(to: $0.contentView.centerXAnchor)
        }
    }

}

// MARK: - Public build methods
extension ParlorAlertViewBuilder {

    func buildView() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildContentView() -> UIView {
        return UIView().manualLayoutable().apply {
            $0.backgroundColor = .white
        }
    }
    
    func buildLabelsStack() -> UIStackView {
        return UIStackView(axis: .vertical, with: [], spacing: Constants.Alert.messageStackSpacing, alignment: .center, distribution: .equalSpacing).manualLayoutable()
    }
    
    func buildIconImageView() -> UIImageView {
        return UIImageView().manualLayoutable()
    }
    
    func buildScrollView() -> UIScrollView {
        return UIScrollView().manualLayoutable()
    }
    
    func buildTitleLabel() -> ParlorLabel {
        return ParlorLabel().apply {
             $0.textAlignment = controller.alertData.title?.contains("\n") ?? false ? .left : .center
            $0.font = UIFont.custom(ofSize: Constants.FontSize.hintSize, font: UIFont.Roboto.regular)
            $0.textColor = UIColor.appTextMediumBright
            $0.numberOfLines = 0
            $0.lineHeight = 24
        }
    }
    
    func buildMessageLabel() -> UILabel {
        return ParlorLabel().apply {
            $0.textAlignment = controller.alertData.message?.contains("\n") ?? false ? .left : .center
            $0.font = UIFont.custom(ofSize: Constants.FontSize.xTiny, font: UIFont.Roboto.regular)
            $0.textColor = UIColor.textFieldUnderline
            $0.numberOfLines = 0
            $0.lineHeight = 20
        }
    }
    
    func buildBottomButton() -> UIButton {
        return UIButton().manualLayoutable().apply {
            $0.setTitleColor(UIColor.appTextMediumBright, for: .normal)
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.medium)
            $0.applyTouchAnimation()
        }
    }
    
    func buildCloseButton() -> UIButton {
        return UIButton().manualLayoutable().apply {
            let inset = Constants.Margin.standard
            $0.setImage(Icons.NavigationBar.close.imageWith(newSize: Constants.Alert.closeIconSize), for: .normal)
            $0.contentEdgeInsets = UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
            $0.tintColor = UIColor.closeTintColor
            $0.applyTouchAnimation()
        }
    }
    
    func buildContentScrollViewHeightContraint() -> NSLayoutConstraint {
        return controller.scrollView.heightAnchor.equalTo(constant: 200)
    }
    
}

extension Constants {
    
    enum Alert {
        static let messageStackSpacing: CGFloat = 5
        static let imageTopMargin: CGFloat = 53
        static let topStackMargin: CGFloat = 30
        static let bottomButtonTopMargin: CGFloat = 45
        static let bottomButtonBottomMargin: CGFloat = 35
        static let closeIconSize = CGSize(width: 11, height: 11)
    }
    
}
