//
//  AlertData.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 08/04/2019.
//

import Foundation
import UIKit

struct AlertData {
    let icon: UIImage?
    let title: String?
    let message: String?
    let bottomButtonTitle: String?
    let statusCode: Int?
    let buttonAction: Function
    let closeButtonAction: Function
    let analyticAlertSourceName: AnalyticEventAlertSourceName?
    
    var shouldShowAlertPopup: Bool {
        return statusCode != Config.paymentRequiredCode
    }
    
    // MARK: - Initialization
    init(icon: UIImage?,
         title: String?,
         message: String?,
         bottomButtonTitle: String?,
         statusCode: Int? = nil,
         buttonAction: @escaping Function = {},
         closeButtonAction: @escaping Function = {},
         analyticAlertSourceName: AnalyticEventAlertSourceName? = nil) {
        self.icon = icon
        self.title = title
        self.message = message
        self.bottomButtonTitle = bottomButtonTitle
        self.statusCode = statusCode
        self.buttonAction = buttonAction
        self.closeButtonAction = closeButtonAction
        self.analyticAlertSourceName = analyticAlertSourceName
    }
    
    func withButtonAction(sourceName: AnalyticEventAlertSourceName? = nil, buttonAction: @escaping Function) -> AlertData {
        return AlertData(icon: self.icon, title: self.title, message: self.message, bottomButtonTitle: self.bottomButtonTitle,
                         buttonAction: buttonAction, closeButtonAction: self.closeButtonAction, analyticAlertSourceName: sourceName)
    }
    
    func withCloseButtonAction(closeButtonAction: @escaping Function = {}) -> AlertData {
        return AlertData(icon: self.icon, title: self.title, message: self.message, bottomButtonTitle: self.bottomButtonTitle, buttonAction: self.buttonAction, closeButtonAction: closeButtonAction)
    }
    
}

extension AlertData {
    
    static func removeCardAlert(withLastFour lastFour: String) -> AlertData {
        return AlertData(
            icon: Icons.Cards.remove,
            title: Strings.Cards.removeAlertTitle.localized,
            message: String(format: Strings.Cards.removeAlertContentPlaceholder.localized, arguments: [lastFour]),
            bottomButtonTitle: Strings.confirm.localized.uppercased()
        )
    }
    
    static func fromAppError(appError: AppError, bottomButtonAction: @escaping Function = {}, closeButtonAction: @escaping Function = {}) -> AlertData {
        return AlertData(
            icon: Icons.fromAppError(appError: appError),
            title: appError.fullErrorMessage,
            message: nil,
            bottomButtonTitle: Strings.ok.localized.uppercased(),
            statusCode: appError.code,
            buttonAction: bottomButtonAction,
            closeButtonAction: closeButtonAction,
            analyticAlertSourceName: appError.analyticErrorSourceName
        )
    }
    
    static func fromSimpleMessage(message: String, withIcon icon: UIImage = Icons.ResetPassword.createNew, action: @escaping Function = {},
                                  closeButtonAction: @escaping Function = {}, sourceName: AnalyticEventAlertSourceName? = nil) -> AlertData {
        return AlertData(
            icon: icon,
            title: message,
            message: nil,
            bottomButtonTitle: Strings.ok.localized.uppercased(),
            buttonAction: action,
            closeButtonAction: closeButtonAction,
            analyticAlertSourceName: sourceName
        )
    }
    
    static func confirmActivationCancel(isPayable: Bool, action: @escaping Function = {}, sourceName: AnalyticEventAlertSourceName? = nil) -> AlertData {
        return AlertData(
            icon: Icons.Common.warning,
            title: isPayable ? Strings.GuestForm.confirmCancelPayment.localized : Strings.GuestForm.confirmCancel.localized,
            message: nil,
            bottomButtonTitle: Strings.confirm.localized.uppercased(),
            buttonAction: action,
            analyticAlertSourceName: sourceName
        )
    }
    
    static func premiumActivationAlert(title: String? = nil, icon: UIImage? = nil, buttonAction: @escaping Function) -> AlertData {
        return AlertData(
            icon: icon ?? Icons.Error.premium,
            title: title ?? Strings.Activation.premiumAlert.localized,
            message: nil,
            bottomButtonTitle: Strings.Activation.premiumButton.localized.uppercased(),
            buttonAction: buttonAction
        )
    }
    
    static func noCalendarPermissions() -> AlertData {
        return AlertData(icon: Icons.Common.warning,
                         title: Strings.Calendar.permissionTitle.localized,
                         message: Strings.Calendar.permissionMessage.localized,
                         bottomButtonTitle: Strings.ok.localized.uppercased(),
                         buttonAction: {
                            CalendarSyncConfig.isSyncEnabled = true
                            guard let url = URL(string: UIApplication.openSettingsURLString) else { return }
                            UIApplication.shared.open(url)},
                         closeButtonAction: { CalendarSyncConfig.isSyncEnabled = false },
                         analyticAlertSourceName: .calendarPermissionsDenied
        )
    }
    
    static func noNotificationsPermissions() -> AlertData {
        return AlertData(icon: Icons.Common.warning,
                         title: Strings.EnableNotificationsReminder.notificationPermissionsAlertTitle.localized,
                         message: Strings.EnableNotificationsReminder.notificationPermissionsAlertMessage.localized,
                         bottomButtonTitle: Strings.ok.localized.uppercased(),
                         buttonAction: {
                            guard let url = URL(string: UIApplication.openSettingsURLString) else { return }
                            UIApplication.shared.open(url)}
        )
    }
    
    static func locationErrorAlert(error: AppError) -> AlertData {
        return AlertData(
            icon: Icons.Error.location,
            title: error.fullErrorMessage,
            message: nil,
            bottomButtonTitle: Strings.settings.localized.uppercased(),
            statusCode: error.code,
            buttonAction: {
                if let url = URL(string: UIApplication.openSettingsURLString) {
                    UIApplication.shared.open(url)
                }}
        )
    }
    
    static func locationErrorTryAgainAlertData(error: AppError, buttonAction: @escaping Function) -> AlertData {
        return AlertData(
            icon: Icons.Error.location,
            title: error.fullErrorMessage,
            message: nil,
            bottomButtonTitle: Strings.tryAgain.localized.uppercased(),
            statusCode: error.code,
            buttonAction: buttonAction
        )
    }
    
    static func fromPushNotificationDetails(_ pushNotificationData: PushNotificationData,
                                            buttonAction: @escaping Function,
                                            showMessageInTitle: Bool = false) -> AlertData {
        return AlertData(
            icon: pushNotificationData.destination?.alertIcon,
            title: showMessageInTitle ? pushNotificationData.body : pushNotificationData.title,
            message: showMessageInTitle ? nil : pushNotificationData.body,
            bottomButtonTitle: Strings.ok.localized.uppercased(),
            buttonAction: buttonAction
        )
    }
    
    static func addProfilePhoto() -> AlertData {
        return AlertData(icon: Icons.Error.general,
                         title: Strings.AddProfilePhoto.photoCouldNotBeUploaded.localized,
                         message: nil,
                         bottomButtonTitle: Strings.ok.localized.uppercased()
        )
    }
 
    static func webViewBaseAlert(buttonAction: @escaping Function) -> AlertData {
        return AlertData(icon: Icons.Error.general,
                         title: Strings.BaseWebView.pageCannotBeLoaded.localized,
                         message: nil,
                         bottomButtonTitle: Strings.ok.localized.uppercased(),
                         buttonAction: buttonAction
        )
    }
 
    static func fullGuestListAlert(title: String, buttonAction: @escaping Function) -> AlertData {
        return AlertData(
            icon: Icons.GuestsForm.guestSlotsNotAvailable,
            title: title,
            message: nil,
            bottomButtonTitle: Strings.continue.localized.uppercased(),
            buttonAction: buttonAction,
            analyticAlertSourceName: AnalyticEventAlertSourceName.activationFullGuestList
        )
    }
    
    static func applePayErrorAlert(withTitle title: String) -> AlertData {
        return AlertData(
            icon: Icons.Cards.remove,
            title: title,
            message: nil,
            bottomButtonTitle: Strings.ok.localized.uppercased(),
            analyticAlertSourceName: AnalyticEventAlertSourceName.applePayPaymentError
        )
    }
}
