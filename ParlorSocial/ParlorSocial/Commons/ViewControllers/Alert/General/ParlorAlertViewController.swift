//
//  ParlorAlertViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 08/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

class ParlorAlertViewController: AppViewController {

    // MARK: - Properties
    let analyticEventScreen: AnalyticEventScreen? = nil
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }
    
    private var viewModel: BaseViewModelLogic!
    private let animationDuration: TimeInterval = 0.5
    let alertData: AlertData
    
    var buttonAction: () -> Void = {}
    var closeButtonAction: () -> Void = {}
    
    var contentViewMask: UIView {
        return contentView.mask!
    }
    
    private var maxScrollViewHeight: CGFloat {
        return view.frame.height
        - alertLogoImage.frame.height
        - confirmButton.frame.height
        - sumViewVerticalConstants
    }
    
    private var sumViewVerticalConstants: CGFloat {
        return Constants.Alert.imageTopMargin
            + Constants.Alert.topStackMargin
            + 2 * Constants.Alert.bottomButtonTopMargin
            + 2 * Constants.horizontalMarginBig
    }
    
    override var prefersStatusBarHidden: Bool {
        if #available(iOS 13.0, *) {
            return false
        }
        return true
    }
    
    // MARK: - Views
    private(set) var contentView: UIView!
    private(set) var labelsStack: UIStackView!
    private(set) var alertLogoImage: UIImageView!
    private(set) var scrollView: UIScrollView!
    private(set) var contentScrollView: UIView!
    private(set) var titleLabel: ParlorLabel!
    private(set) var messageLabel: UILabel!
    private(set) var confirmButton: UIButton!
    private(set) var closeButton: UIButton!
    
    private(set) var contentScrollViewHeightContraint: NSLayoutConstraint!
    
    // MARK: - Initialization
    init(alertData: AlertData) {
        self.alertData = alertData
        self.buttonAction = alertData.buttonAction
        self.closeButtonAction = alertData.closeButtonAction
        super.init(nibName: nil, bundle: nil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }

    private func setup() {
        modalPresentationStyle = .overFullScreen
        modalTransitionStyle = .crossDissolve
        modalPresentationCapturesStatusBarAppearance = true
        viewModel = BaseViewModelClass()
    }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
        self.contentView.alpha = 0
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        contentScrollViewHeightContraint.constant = min(contentScrollView.frame.height, maxScrollViewHeight)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.contentView.animateEntrance()
        scrollView.flashScrollIndicators()
    }
    
    @objc private func didTapCloseButton() {
        if let buttonName = AnalyticEventClickableElementName.AlertDialog.close.withAlertSourceName(alertData.analyticAlertSourceName) {
            AnalyticService.shared.saveViewClickAnalyticEvent(withName: buttonName)
        }
        contentView.animateDismiss { [weak self] in
            self?.dismiss(animated: true, completion: self?.closeButtonAction)
        }
    }
    
    @objc private func didTapBottomButton() {
        if let buttonName = AnalyticEventClickableElementName.AlertDialog.positive.withAlertSourceName(alertData.analyticAlertSourceName) {
            AnalyticService.shared.saveViewClickAnalyticEvent(withName: buttonName)
        }
        contentView.animateDismiss { [weak self] in
            self?.dismiss(animated: true, completion: self?.buttonAction)
        }
    }
}

// MARK: - Private methods
extension ParlorAlertViewController {

    private func setupViews() {
        let builder = ParlorAlertViewBuilder(controller: self)
        self.contentView = builder.buildContentView()
        self.labelsStack = builder.buildLabelsStack()
        self.closeButton = builder.buildCloseButton()
        self.alertLogoImage = builder.buildIconImageView()
        self.scrollView = builder.buildScrollView()
        self.contentScrollView = builder.buildView()
        self.titleLabel = builder.buildTitleLabel()
        self.messageLabel = builder.buildMessageLabel()
        self.confirmButton = builder.buildBottomButton()
        self.contentScrollViewHeightContraint = builder.buildContentScrollViewHeightContraint()
        builder.setupViews()
    }

    private func setupPresentationLogic() {
        closeButton.addTarget(self, action: #selector(didTapCloseButton), for: .touchUpInside)
        confirmButton.addTarget(self, action: #selector(didTapBottomButton), for: .touchUpInside)
    }

}
