//
//  NewFilterableViewController.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 29/04/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

class NewFilterableViewController: AppViewController {
    var titleButton: ParlorButton?

    var analyticEventScreen: AnalyticEventScreen? { return nil }
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? { return nil }

    var analyticsSearchButtonEvent: String? { return nil }
    var analyticsCalendarButtonEvent: String? { return nil }

    var controllerTitle: String? { return nil }
    var setTitleAsButton: Bool { return true }
    var setBottomShadow: Bool { return true }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(
            searchAction: #selector(didTapSearchBarButtonItem),
            calendarAction: #selector(didTapCalendarBarButtonItem),
            categoriesAction: #selector(didTapCategoriesBarButtonItem)
        )
    }
}

extension NewFilterableViewController {
    @objc func didTapSearchBarButtonItem() {
        if let eventName = analyticsSearchButtonEvent {
            AnalyticService.shared.saveViewClickAnalyticEvent(withName: eventName)
        }

        if Config.userProfile?.isPreview ?? false {
            self.router.push(destination: FilterableDestination.membershipPlans)
        } else {
            self.router.present(destination: FilterableDestination.search, animated: true, withStyle: .fullScreen)
        }
    }

    @objc func didTapCalendarBarButtonItem() {
        if let eventName = analyticsCalendarButtonEvent {
            AnalyticService.shared.saveViewClickAnalyticEvent(withName: eventName)
        }

        if Config.userProfile?.isPreview ?? false {
            self.router.push(destination: FilterableDestination.membershipPlans)
        } else {
            self.router.present(destination: FilterableDestination.calendar, animated: true, withStyle: .fullScreen)
        }
    }

    @objc func didTapCategoriesBarButtonItem(sender: UIControl) {}
}

extension NewFilterableViewController {
    func change(title: String?) {
        titleButton?.title = title
    }

    private func setupNavigationBar(
        searchAction: Selector? = nil,
        calendarAction: Selector? = nil,
        categoriesAction: Selector? = nil
    ) {
        navigationItem.leftBarButtonItem = .createSearchButton(target: self, action: searchAction)
        navigationItem.rightBarButtonItem = .createCalendarButton(target: self, action: calendarAction)

        if let categoriesAction = categoriesAction {
            navigationItem.titleView = buildTitleView(with: categoriesAction)
        }

        if setBottomShadow {
            navigationController?.navigationBar.addShadow(withOpacity: 0.1, radius: 10)
            navigationController?.navigationBar.shadowImage = UIImage()
        } else {
            navigationController?.navigationBar.shadowImage = UIImage()
        }
    }

    private func buildTitleView(with clickAction: Selector) -> UIView {
        let titleView = UIView().manualLayoutable()
        titleView.applyTouchAnimation()

        let buttonStyle: ParlorButtonStyle = setTitleAsButton ? .selectable : .plain(titleColor: .black, font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.bold), letterSpacing: Constants.LetterSpacing.small)
        let button = ParlorButton(style: buttonStyle).manualLayoutable()
        button.addTarget(self, action: clickAction, for: .touchUpInside)
        button.title = controllerTitle
        titleButton = button

        if !setTitleAsButton { button.isEnabled = false }

        titleView.addSubview(button)
        button.edgesToParent(insets: .margins(top: 4))

        return titleView
    }
}
