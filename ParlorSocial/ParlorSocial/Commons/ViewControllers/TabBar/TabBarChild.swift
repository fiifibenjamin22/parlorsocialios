//
//  TabBarChild.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 12/03/2019.
//

import Foundation

import UIKit

protocol TabBarChild: class {
    func didTouchTabItem()
    func didSelectTab()
}

extension TabBarChild where Self: AppViewController {
    func didTouchTabItem() { }
    
    func didSelectTab() { }
    
    var topRouter: Router {
        guard let parent = self.parent?.parent as? AppTabBarController else { fatalError("Did you manipulate with controller hierarchy?") }
        return parent.router
    }
}
