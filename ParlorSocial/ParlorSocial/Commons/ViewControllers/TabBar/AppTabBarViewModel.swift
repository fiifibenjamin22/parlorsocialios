//
//  AppTabBarViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 12/07/2019.
//

import Foundation
import RxSwift
import RxCocoa

protocol TabSelectedAnalyticLogic {
    func saveTabSelectedAnalyticEvent(index: Int?)
}

protocol AppTabBarViewModelLogic: BaseViewModelLogic, TabSelectedAnalyticLogic {
    func fetchUnreadAnnouncementsInfo()
    func initFlow()
    func showEnableNotificationsSettingsIfNeeded(when viewType: EnableNotificationsReminderViewType)
    var unreadAnnoucementsInfoObs: Observable<Bool> { get }
    var newActivationNotificationObs: Observable<Bool> { get }
    var destinationObs: Observable<AppTabBarDestination> { get }
}

class AppTabBarViewModel: BaseViewModel {
    let analyticEventReferencedId: Int? = nil
    let annoucementsRepository = AnnouncementsRepository.shared
    let homeRepository = HomeRepository.shared
    let profileRepository: ProfileRepositoryProtocol = ProfileRepository.shared
    let unreadAnnoucementsInfoSubject = PublishSubject<Bool>()
    let unreadMyRsvpsSubject = PublishSubject<Bool>()
    
    private let newActivationsNotificationSubject: PublishSubject<Bool> = PublishSubject()
    private let destinationRelay = PublishRelay<AppTabBarDestination>()
    private let notificationsReminderProvider = EnableNotificationsReminderViewProvider()
    
    // MARK: - Initialization
    
    override init() {
        super.init()
    }
}

// MARK: - Private methods
private extension AppTabBarViewModel {
    
    private func handleUnreadAnnouncementsInfo(response: AnnouncementsInfoApiResponse) {
        switch response {
        case .success(let response):
            unreadAnnoucementsInfoSubject.onNext(response.data.hasUnreadAnnouncements)
        case .failure:
            break
        }
    }
    
}

// MARK: - AppTabBarViewModelLogic
extension AppTabBarViewModel: AppTabBarViewModelLogic {

    var destinationObs: Observable<AppTabBarDestination> {
        destinationRelay.asObservable()
    }

    var unreadAnnoucementsInfoObs: Observable<Bool> {
        return unreadAnnoucementsInfoSubject.asObservable()
    }
    
    var newActivationNotificationObs: Observable<Bool> {
        return newActivationsNotificationSubject.asObservable()
    }
    
    func fetchUnreadAnnouncementsInfo() {
        annoucementsRepository.getUnreadAnnouncementsInfo()
            .subscribe(onNext: handleUnreadAnnouncementsInfo)
            .disposed(by: disposeBag)
    }
    
    func initFlow() {
        annoucementsRepository.unreadAnnouncementsObs
            .subscribe(onNext: { [unowned self] isUnread in
                self.unreadAnnoucementsInfoSubject.onNext(isUnread)
            }).disposed(by: disposeBag)
        
        homeRepository.newActivationsNotificationSubject
            .bind(to: newActivationsNotificationSubject)
            .disposed(by: disposeBag)
    }
    
    func showEnableNotificationsSettingsIfNeeded(when viewType: EnableNotificationsReminderViewType) {
        notificationsReminderProvider.showNotificationsReminder(when: viewType) { [weak self] shouldShow, notificationsSettingsChangeSource in
            guard shouldShow else { return }
            DispatchQueue.main.async { [weak self] in
                self?.destinationRelay.accept(.notificationsReminder(notificationsSettingsChangeSource))
            }
        }
    }
    
}

// MARK: - TabSelectedAnalyticLogic
extension AppTabBarViewModel {
    
    func saveTabSelectedAnalyticEvent(index: Int?) {
        guard let strongIndex = index,
            let mainTabBarScreen = MainTabBarScreen(rawValue: strongIndex) else { return }
        AnalyticService.shared.saveTabSelectedAnalyticEvent(type: mainTabBarScreen.analyticSelectedTab)
    }

}
