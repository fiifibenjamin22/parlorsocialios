//
//  AppTabBarController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 12/03/2019.
//

import UIKit
import RxSwift

enum MainTabBarScreen: Int, CaseIterable {
    case home
    case community
    case announcements
    case profile
    
    var analyticSelectedTab: AnalyticEventTabSelected {
        switch self {
        case .home:
            return .home
        case .community:
            return .community
        case .announcements:
            return .announcements
        case .profile:
            return .profile
        }
    }
}

class AppTabBarController: UITabBarController {
    
    // MARK: - Properties
    private let disposeBag = DisposeBag()
    private lazy var viewModel: AppTabBarViewModelLogic = {
       return AppTabBarViewModel()
    }()
    private var previousControllerClass: AnyClass?

    lazy var router = Router(viewController: self)

    // MARK: - Initialization
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    init(selectedTab: MainTabBarScreen) {
        super.init(nibName: nil, bundle: nil)
        selectedViewController = viewControllers?[selectedTab.rawValue]
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewControllers()
        setupAppearance()
        setupObservables()
        setupNotificationObserver()
        viewModel.initFlow()
        delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        statusBar?.backgroundColor = .appTintColor
        viewModel.fetchUnreadAnnouncementsInfo()
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.showEnableNotificationsSettingsIfNeeded(when: .onDashboard)
    }
    
    func updateAnnouncementsTabBarItem(isUnread: Bool) {
        let tabData = MainTabBarScreen.TabData(
            title: nil,
            viewController: AnnouncementsViewController().embedInNavigationController(),
            icon: isUnread ? Icons.TabBar.unreadAnnouncements : Icons.TabBar.announcements,
            selectedIcon: isUnread ? Icons.TabBar.unreadAnnouncementsSelected : Icons.TabBar.announcementsSelected
        )
        viewControllers?[MainTabBarScreen.announcements.rawValue].tabBarItem =
            MainTabBarScreen.announcements.createTabBarItem(with: tabData)
    }

    func updateHomeTabBarItem(hasNewActivations: Bool) {
        let tabData = MainTabBarScreen.TabData(
            title: nil,
            viewController: HomeViewController().embedInNavigationController(),
            icon: hasNewActivations ? Icons.TabBar.unreadHome : Icons.TabBar.home,
            selectedIcon: hasNewActivations ? Icons.TabBar.unreadHomeSelected : Icons.TabBar.homeSelected
        )
        viewControllers?[MainTabBarScreen.home.rawValue].tabBarItem =
            MainTabBarScreen.home.createTabBarItem(with: tabData)
    }
    
    func selectScreen(_ screen: MainTabBarScreen) {
        selectedIndex = screen.rawValue
    }
    
    func selectHomeTab(withSelectedSubtab subtab: HomeActivationsFilterType) {
        selectScreen(.home)
        guard let homeController = (selectedViewController as? AppNavigationController)?.viewControllers.first(where: { $0 is HomeViewControllerProtocol }) as? HomeViewControllerProtocol else { return }
        homeController.changeSelectedPage(to: subtab)
    }
}

// MARK: - Private methods
private extension AppTabBarController {
    
    private func setupViewControllers() {
        viewControllers = [
            MainTabBarScreen.home,
            MainTabBarScreen.community,
            MainTabBarScreen.announcements,
            MainTabBarScreen.profile
            ].compactMap {
                let data = $0.tabData
                data.viewController.tabBarItem = $0.createTabBarItem(with: data)
                return data.viewController
        }
    }
    
    private func setupAppearance() {
        tabBar.isTranslucent = false
        tabBar.addShadow(withOpacity: 0.1, radius: 2)
        
        UITabBar.appearance().shadowImage = UIImage()
        UITabBar.appearance().backgroundImage = UIImage()
    }
    
    private func hideWelcomeScreenIfNeeded(for parentTabBarControoler: UITabBarController) {
        guard let currentController =
            ((parentTabBarControoler.selectedViewController as? UINavigationController)?.topViewController)
                as? HomeViewController else {
                    return
        }
        currentController.homeWelcomeView.hide()
    }
    
    private func setupObservables() {
        viewModel.unreadAnnoucementsInfoObs
            .subscribe(onNext: { [unowned self] isUnread in
                self.handleUnreadAnnouncementsInfo(isUnread: isUnread)
            }).disposed(by: disposeBag)
        
        viewModel.newActivationNotificationObs
            .subscribe(onNext: { [unowned self] hasNew in
                self.updateHomeTabBarItem(hasNewActivations: hasNew)
            }).disposed(by: disposeBag)
        
        viewModel.destinationObs
            .subscribe(onNext: { [unowned self] destination in
                self.handleDestination(destination)
            }).disposed(by: disposeBag)
    }
    
    private func handleUnreadAnnouncementsInfo(isUnread: Bool) {
        updateAnnouncementsTabBarItem(isUnread: isUnread)
    }

    private func setupNotificationObserver() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(applicationDidBecomeActive),
            name: UIApplication.didBecomeActiveNotification,
            object: nil)
    }
    
    private func handleDestination(_ destination: AppTabBarDestination) {
        switch destination {
        case .membershipPlans:
            router.push(destination: destination)
        case .notificationsReminder:
            router.present(destination: destination)
        }
    }
    
    private func selectedIndex(viewController: UIViewController) -> Int? {
        return viewControllers?.firstIndex(where: {$0 == viewController})
    }
        
    @objc private func applicationDidBecomeActive() {
        if Config.keychainWorker.isTokenPresent {
            self.viewModel.fetchUnreadAnnouncementsInfo()
        }
        viewModel.showEnableNotificationsSettingsIfNeeded(when: .onDashboard)
    }
    
}

extension MainTabBarScreen {

    typealias TabData = (
        title: String?,
        viewController: UIViewController,
        icon: UIImage?,
        selectedIcon: UIImage?
    )

    var tabData: TabData {
        switch self {
        case .home:
            return TabData(
                title: nil,
                viewController: HomeViewController().embedInNavigationController(),
                icon: Icons.TabBar.home,
                selectedIcon: Icons.TabBar.homeSelected
            )
        case .community:
            return TabData(
                title: nil,
                viewController: CommunityViewController().embedInNavigationController(),
                icon: Icons.TabBar.community,
                selectedIcon: Icons.TabBar.communitySelected
            )
        case .announcements:
            return TabData(
                title: nil,
                viewController: AnnouncementsViewController().embedInNavigationController(),
                icon: Icons.TabBar.announcements,
                selectedIcon: Icons.TabBar.announcementsSelected
            )
        case .profile:
            return TabData(
                title: nil,
                viewController: ProfileViewController().embedInNavigationController(),
                icon: Icons.TabBar.profile,
                selectedIcon: Icons.TabBar.profileSelected
            )
        }
    }

    func createTabBarItem(with data: TabData) -> UITabBarItem {
        let tabBarItem = UITabBarItem(
            title: data.title,
            image: data.icon,
            selectedImage: data.selectedIcon)

        if data.title == nil {
            tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        }

        return tabBarItem
    }
}

extension AppTabBarController: UITabBarControllerDelegate {

    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        viewModel.saveTabSelectedAnalyticEvent(index: selectedIndex(viewController: viewController))
        hideWelcomeScreenIfNeeded(for: tabBarController)
        previousControllerClass = (tabBarController.selectedViewController as? UINavigationController)?
            .topViewController?.classForCoder
        let toView: UIView = viewController.view
        if let fromView = tabBarController.selectedViewController!.view, fromView != toView {
            UIView.transition(
                from: fromView,
                to: toView,
                duration: 0.3,
                options: .transitionCrossDissolve)
        }

        return true
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if let selectedCotroller = (viewController as? UINavigationController)?.topViewController,
            let previousControllerClass = previousControllerClass,
            let tabBarController = selectedCotroller as? TabBarChild {
            tabBarController.didSelectTab()
            if previousControllerClass == selectedCotroller.classForCoder {
                tabBarController.didTouchTabItem()
            }
        }
    }

}
