//
//  AppTabBarDestination.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 19/11/2019.
//  Copyright © 2019 KISSdigital. All rights reserved.
//

import UIKit

enum AppTabBarDestination {
    case membershipPlans
    case notificationsReminder(NotificationsSettingsChangedEventSource)
}

extension AppTabBarDestination: Destination {
    
    var viewController: UIViewController {
        switch self {
        case .membershipPlans:
            return MembershipPlansViewController()
        case .notificationsReminder(let source):
            return EnableNotificationsReminderViewController(notificationsSettingsChangeSource: source)
        }
    }
    
}
