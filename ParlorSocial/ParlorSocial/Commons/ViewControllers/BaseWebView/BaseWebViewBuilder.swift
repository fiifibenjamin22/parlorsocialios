//
//  BaseWebViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 11/06/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import WebKit

class BaseWebViewBuilder {

    private unowned let controller: BaseWebViewController
    private var view: UIView! { return controller.view }

    init(controller: BaseWebViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension BaseWebViewBuilder {

    private func setupProperties() {
        view.apply {
            $0.clipsToBounds = true
            $0.backgroundColor = .white
        }
    }

    private func setupHierarchy() {
        view.addSubview(controller.webView)
    }

    private func setupAutoLayout() {
        controller.webView.apply {
            $0.edgesToParent(anchors: [.top, .leading, .trailing, .bottom])
        }
    }

}

// MARK: - Public build methods
extension BaseWebViewBuilder {

    func buildView() -> UIView {
        return UIView()
    }
    
    func buildWebView() -> WKWebView {
        return WKWebView().manualLayoutable().apply {
            $0.backgroundColor = .white
        }
    }
}
