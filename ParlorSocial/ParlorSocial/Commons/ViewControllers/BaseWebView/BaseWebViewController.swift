//
//  BaseWebViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 11/06/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import WebKit

class BaseWebViewController: AppViewController {

    // MARK: - Properties
    let analyticEventScreen: AnalyticEventScreen? = .webView
    private var viewModel: BaseWebViewLogic!
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }
    
    // MARK: - Views
    private(set) var webView: WKWebView!

    // MARK: - Initialization
    init(data: BaseWebViewData) {
        super.init(nibName: nil, bundle: nil)
        initialize(data: data)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError()
    }

    private func initialize(data: BaseWebViewData) {
        self.viewModel = BaseWebViewModel(data: data)
        viewModel.setupNavigationBarTitle()
    }
    
    override var observableSources: ObservableSources? {
        return viewModel
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func observeViewModel() {
        super.observeViewModel()
        
        viewModel.contentURLObs
            .subscribe(onNext: { [unowned self] url in
                self.webView.load(URLRequest(url: url))
            }).disposed(by: disposeBag)
        
        viewModel.navigationTitleObs
            .subscribe(onNext: { [unowned self] title in
                self.navigationItem.title = title
            }).disposed(by: disposeBag)
        
        viewModel.destinationObs
            .subscribe(onNext: { [unowned self] destination in
                self.handle(destination: destination)
            }).disposed(by: disposeBag)
        
    }

    // MARK: - Lifecycle
    override func loadView() {
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        adjustNavigationBar()
    }

}

// MARK: - Private methods
extension BaseWebViewController {

    private func setupViews() {
        let builder = BaseWebViewBuilder(controller: self)
        view = builder.buildView()
        webView = builder.buildWebView()

        builder.setupViews()
    }

    private func setupPresentationLogic() {
        webView.navigationDelegate = viewModel
        viewModel.getContent()
    }
    
    private func adjustNavigationBar() {
        self.statusBar?.backgroundColor = .clear
        self.navigationController?.setNavigationBarBorderColor()
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.bold),
             NSAttributedString.Key.kern: CGFloat(1.3)]
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: Icons.NavigationBar.close, style: .plain, target: self, action: #selector(tapBackButton)).apply {
            $0.tintColor = UIColor.closeTintColor
        }
    }
    
    private func handle(destination: BaseWebViewDestination) {
        switch destination {
        case .back:
            navigationController?.dismiss()
        }
    }
    
    @objc private func tapBackButton() {
        navigationController?.dismiss(animated: true)
    }

}
