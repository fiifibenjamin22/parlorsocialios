//
//  TermsAndPrivacyViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 11/06/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import WebKit

protocol BaseWebViewLogic: BaseViewModelLogic, WKNavigationDelegate {
    var navigationTitleObs: Observable<String> { get }
    var contentURLObs: Observable<URL> { get }
    var destinationObs: Observable<BaseWebViewDestination> { get }
    func getContent()
    func setupNavigationBarTitle()
}

class BaseWebViewModel: BaseViewModel {
    // MARK: - Properties
    let analyticEventReferencedId: Int? = nil
    let destinationSubject: PublishSubject<BaseWebViewDestination> = .init()
    private let webViewData: BaseWebViewData
    private let profileRepository = ProfileRepository.shared
    private let navigationTitleRelay = BehaviorRelay<String>(value: "")
    private let contentURLRelay = PublishRelay<URL>()
    
    // MARK: - Initialization
    init(data: BaseWebViewData) {
        webViewData = data
        super.init()
    }
    
}

// MARK: Interface logic methods
extension BaseWebViewModel: BaseWebViewLogic {
    
    var destinationObs: Observable<BaseWebViewDestination> {
        return destinationSubject.asObservable()
    }
    
    var navigationTitleObs: Observable<String> {
        return navigationTitleRelay.asObservable()
    }
    
    var contentURLObs: Observable<URL> {
        return contentURLRelay.asObservable()
    }
    
    func getContent() {
        progressBarSubject.onNext(true)
        contentURLRelay.accept(webViewData.url)
    }
    
    func setupNavigationBarTitle() {
        guard let title = webViewData.navBarTitle else { return }
        navigationTitleRelay.accept(title)
    }
}

// MARK: - WKNavigationDelegate
extension BaseWebViewModel {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        progressBarSubject.onNext(false)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        progressBarSubject.onNext(true)
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        progressBarSubject.onNext(false)
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        progressBarSubject.onNext(false)
        alertDataSubject.onNext(AlertData.webViewBaseAlert(buttonAction: { [unowned self] in
            self.destinationSubject.onNext(.back)
        }))
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        progressBarSubject.onNext(false)
        alertDataSubject.onNext(AlertData.webViewBaseAlert(buttonAction: { [unowned self] in
            self.destinationSubject.onNext(.back)
        }))
    }

}
