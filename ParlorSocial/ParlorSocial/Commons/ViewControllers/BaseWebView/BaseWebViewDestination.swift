//
//  BaseWebViewDestination.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 30/10/2019.
//  Copyright © 2019 KISSdigital. All rights reserved.
//

import UIKit

enum BaseWebViewDestination {
    case back
}

extension BaseWebViewDestination: Destination {
    
    var viewController: UIViewController {
        switch self {
        case .back:
            fatalError()
        }
    }
    
}
