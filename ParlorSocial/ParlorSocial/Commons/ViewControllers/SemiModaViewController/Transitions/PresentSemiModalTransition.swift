//
//  PresentSemiModalTransition.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 29/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class PresentSemiModalTransition: SemiModalTransition {
    private enum Constants {
        enum OpacityView {
            static let opacity: CGFloat = 0.4
        }
    }

    override func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let toView = transitionContext.view(forKey: .to) else {
            assertionFailure("toView is not available!")

            return transitionContext.completeTransition(false)
        }

        let containerView = transitionContext.containerView

        let opacityView = OpacityView()
        opacityView.backgroundColor = UIColor.black
        opacityView.alpha = 0.0

        containerView.addSubview(opacityView)
        opacityView.frame = containerView.bounds

        toView.transform = CGAffineTransform(translationX: 0.0, y: toView.frame.height)
        containerView.addSubview(toView)

        animate(
            positionOf: toView,
            transform: .identity,
            opacityOf: opacityView,
            to: Constants.OpacityView.opacity,
            transitionContext: transitionContext
        )
    }
}
