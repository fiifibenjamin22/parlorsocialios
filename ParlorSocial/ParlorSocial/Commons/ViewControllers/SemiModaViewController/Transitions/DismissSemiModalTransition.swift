//
//  DismissSemiModalTransition.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 29/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class DismissSemiModalTransition: SemiModalTransition {
    override func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromView = transitionContext.view(forKey: .from) else {
            assertionFailure("fromView is not available!")

            return transitionContext.completeTransition(false)
        }

        guard let opacityView = transitionContext.containerView.subviews.first(where: { $0 is OpacityView }) else {
            assertionFailure("opacity view is not available!")

            return transitionContext.completeTransition(false)
        }

        animate(
            positionOf: fromView,
            transform: CGAffineTransform(translationX: 0.0, y: fromView.frame.height),
            opacityOf: opacityView,
            to: 0.0,
            transitionContext: transitionContext
        )
    }
}

