//
//  SemiModalTransition.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 29/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

class SemiModalTransition: NSObject, UIViewControllerAnimatedTransitioning {
    private enum Constants {
        static let transitionDuration = 0.5
    }

    class OpacityView: UIView {}

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        assertionFailure("This method should be implemented in subclass")
    }

    func animationEnded(_ transitionCompleted: Bool) {}

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return Constants.transitionDuration
    }

    func animate(
        positionOf view: UIView,
        transform: CGAffineTransform,
        opacityOf opacityView: UIView,
        to opacity: CGFloat,
        transitionContext: UIViewControllerContextTransitioning
    ) {
        UIView.animate(
            withDuration: transitionDuration(using: transitionContext),
            delay: 0.0,
            options: [.curveEaseInOut],
            animations: {
                view.transform = transform
                opacityView.alpha = opacity
        },
            completion: { _ in
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
        )
    }
}
