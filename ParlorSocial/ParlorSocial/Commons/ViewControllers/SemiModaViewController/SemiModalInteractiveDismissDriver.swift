//
//  SemiModalInteractiveDismissDriver.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 29/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class SemiModalInteractiveDismissDriver: UIPercentDrivenInteractiveTransition {
    private enum Constants {
        static let finishThreshold: CGFloat = 0.33
        static let completionSpeed: CGFloat = 0.3
    }

    weak var viewController: SemiModalViewController?

    private(set) var started = false
    private var shouldFinish = false

    @objc func panGesture(_ recognizer: UIPanGestureRecognizer) {
        guard let viewController = viewController, let view = viewController.view else { return }

        let translation = recognizer.translation(in: view)
        let verticalMovement = translation.y / view.bounds.height
        let progress = verticalMovement.clamped(to: 0.0...1.0)

        switch recognizer.state {
        case .began:
            started = true
            viewController.dismiss(animated: true)
            viewController.interactiveDismissBegan()
        case .changed:
            shouldFinish = progress > Constants.finishThreshold
            update(progress)
        case .cancelled:
            started = false
            cancel()
            viewController.interactiveDismissCancelled()
        case .ended:
            started = false
            if shouldFinish {
                // Slow down on finish so dismiss does not look glitchy
                completionSpeed = Constants.completionSpeed
                finish()
            } else {
                cancel()
                viewController.interactiveDismissCancelled()
            }
        default:
            ()
        }
    }
}
