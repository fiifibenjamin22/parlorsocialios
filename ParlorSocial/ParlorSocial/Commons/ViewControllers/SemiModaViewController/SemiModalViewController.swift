//
//  SemiModalViewController.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 29/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

class SemiModalViewController: AppViewController {
    enum ContentHeight {
        case relative(CGFloat)
        case automatic
    }

    // Analytics shouldn't be saved from semi modal view, beacuse of the resulting inconsistency Android.
    final var analyticEventScreen: AnalyticEventScreen?
    final var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>?

    let contentView = UIView().manualLayoutable()

    override var title: String? {
        get { return titleLabel.text }
        set { titleLabel.text = newValue }
    }

    private let contentHeight: ContentHeight
    private let titleLabel = ParlorLabel().manualLayoutable()
    private let container = UIView().manualLayoutable()
    private let navigationBar = UINavigationBar().manualLayoutable()
    private let navigationBarUnderline = UIView().manualLayoutable()
    private let tapToDismissView = UIView().manualLayoutable()
    private let interactiveDismissDriver = SemiModalInteractiveDismissDriver()

    init(contentHeight: ContentHeight) {
        self.contentHeight = contentHeight

        super.init(nibName: nil, bundle: nil)

        transitioningDelegate = self
        interactiveDismissDriver.viewController = self
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    @objc func closeButtonDidTap() {}

    func interactiveDismissBegan() {}

    func interactiveDismissCancelled() {}

    private func setup() {
        view.backgroundColor = .clear

        setupContainer()
        setupNavigationBar()
        setupNavigationBarUnderline()
        setupContentView()
        setupTapToDismissView()
    }

    private func setupContainer() {
        container.backgroundColor = .white
        container.layer.cornerRadius = Constants.SemiModalViewController.Container.cornerRadius
        container.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        container.layer.masksToBounds = true

        view.addSubview(container)

        container.edgesToParent(anchors: [.leading, .trailing, .bottom])
        if case .relative(let multiplier) = contentHeight {
            container.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: multiplier).activate()
        }
    }

    private func setupNavigationBar() {
        navigationItem.rightBarButtonItem = .createCloseButton(target: self, action: #selector(closeButtonDidTap))
        navigationBar.addGestureRecognizer(UIPanGestureRecognizer(target: interactiveDismissDriver, action: #selector(interactiveDismissDriver.panGesture)))

        titleLabel.font = Constants.SemiModalViewController.NavigationBar.titleFont
        titleLabel.textColor = UIColor.appTextSemiLight

        navigationItem.titleView = titleLabel
        navigationBar.setItems([navigationItem], animated: false)

        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()

        container.addSubview(navigationBar)

        navigationBar.edgesToParent(anchors: [.top, .leading, .trailing])
        navigationBar.heightAnchor.equalTo(constant: Constants.SemiModalViewController.NavigationBar.height)
    }

    private func setupNavigationBarUnderline() {
        navigationBarUnderline.backgroundColor = .shareActivationNavigationUnderline

        container.addSubview(navigationBarUnderline)

        let inset = Constants.SemiModalViewController.NavigationBar.Underline.horizontalInset
        navigationBarUnderline.heightAnchor.equalTo(constant: 1)
        navigationBarUnderline.leadingAnchor.equal(
            to: navigationBar.leadingAnchor,
            constant: inset
        )
        navigationBarUnderline.trailingAnchor.equal(
            to: navigationBar.trailingAnchor,
            constant: -inset
        )
        navigationBarUnderline.bottomAnchor.equal(to: navigationBar.bottomAnchor)
    }

    private func setupContentView() {
        container.addSubview(contentView)

        contentView.edgesToParent(anchors: [.leading, .trailing, .bottom])
        contentView.topAnchor.equal(to: navigationBar.bottomAnchor)
    }

    private func setupTapToDismissView() {
        tapToDismissView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeButtonDidTap)))
        tapToDismissView.addGestureRecognizer(UIPanGestureRecognizer(target: interactiveDismissDriver, action: #selector(interactiveDismissDriver.panGesture)))

        view.addSubview(tapToDismissView)

        tapToDismissView.edgesToParent(anchors: [.top, .leading, .trailing])
        tapToDismissView.bottomAnchor.equal(to: container.topAnchor)
    }
}

extension SemiModalViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return PresentSemiModalTransition()
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DismissSemiModalTransition()
    }

    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactiveDismissDriver.started ? interactiveDismissDriver : nil
    }
}

private extension Constants {
    enum SemiModalViewController {
        enum Container {
            static let cornerRadius: CGFloat = 20.0
        }

        enum NavigationBar {
            static let height: CGFloat = 46.0
            static let titleFont = UIFont.custom(ofSize: Constants.FontSize.xTiny, font: UIFont.Roboto.regular)

            enum Underline {
                static let horizontalInset: CGFloat = 22
            }
        }
    }
}
