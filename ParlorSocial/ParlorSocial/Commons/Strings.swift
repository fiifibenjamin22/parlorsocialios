//
//  Strings.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 25/02/2019.
//

import Foundation

enum Strings: String {

    case ok
    case cancel
    case done = "done"
    case unknownError = "unknown.error"
    case noInternetConnectionError = "no.internet.connection.error"
    case encodingError = "encoding.error"
    case filter = "filter"
    case edit = "edit"
    case confirm = "confirm"
    case parlorSymbol = "P"
    case settings = "Settings"
    case tryAgain = "try.again"
    case system = "ios"
    case clipToBoard = "clip.to.board"
    case yes
    case cool
    case select
    case `continue`

    enum Activation: String {
        case all = "activation.all"
        case happenings = "activation.happenings"
        case rsvp = "activation.rsvp"
        case rsvpd = "activation.rsvpd"
        case premium = "activation.premium"
        case hostedBy = "activation.hosted.by"
        case premiumAlert = "activation.premium.alert"
        case startedAlert = "activation.starter.alert"
        case premiumButton = "activation.premium.button"
        case notifyMe = "activation.notify.me"
        case rsvpBeginsAt = "activation.rsvp.begins.at"
        case emptyForYou = "activation.empty.foryou"
        case dateTBD = "activation.date.tbd"
        case tbd = "activation.tbd"
        case upcomingEvents = "activation.upcoming.events"
        case selectMembershipPlanButton = "activation.membership.plan.button"
        case guestList = "activation.guestlist"
        case updates = "activation.updates"
        case live = "activation.live"
        case mixersButton = "activation.mixers.button"
        case happeningsButton = "activation.happenings.button"
        case rsvpsButton = "activation.rsvps.button"
        case today = "activation.upcoming.today"
        case tonight = "activation.upcoming.tonight"
        case tomorrow = "activation.upcoming.tomorrow"
    }

    enum Home {
        enum EmptyDataView {
            enum Mixers: String {
                case label = "home.emptyDataView.mixers.label"
                case button = "home.emptyDataView.mixers.button"
            }
            enum Happenings: String {
                case label = "home.emptyDataView.happenings.label"
                case button = "home.emptyDataView.happenings.button"
            }
            enum Rsvps: String {
                case label = "home.emptyDataView.rsvps.label"
                case button = "home.emptyDataView.rsvps.button"
            }
        }
    }
    
    enum Rsvps: String {
        case upcomingEvents = "rsvps.upcoming.events"
        case pastEvents = "rsvps.past.events"
    }

    enum MyRsvps: String {
        case title = "my.rsvps.title"
        case upcomingButton = "my.rsvps.upcoming.button"
        case historyButton = "my.rsvps.history.button"
        case upcomingParlorPassTitle = "my.rsvps.upcoming.parlor.pass.title"
        case upcomingToday = "my.rsvps.upcoming.today"
        case upcomingTonight = "my.rsvps.upcoming.tonight"
        case upcomingTomorrow = "my.rsvps.upcoming.tomorrow"
        case formattedGuestCountUserOnly = "my.rsvps.upcoming.guest.count.user.only"
        case formattedGuestCountOne = "my.rsvps.upcoming.guest.count.one"
        case formattedGuestCount = "my.rsvps.upcoming.guest.count"
        case comingSoon = "my.rsvps.coming.soon"
        case notAvailable = "my.rsvps.not.available"
        case guestList = "my.rsvps.guest.list"
        case externalTicketInfo = "my.rsvps.external.ticket.info"
        case buyExternalTickets = "my.rsvps.buy.external.tickets"
        case cancelReservation = "my.rsvps.cancel.reservation"
        case upcomingWaitlist = "my.rsvps.upcoming.waitlist"
        case formattedJustMe = "my.rsvps.just.me"
        case formattedJustMePlusOne = "my.rsvps.just.me.plus.one"
        case formattedJustMePlusMore = "my.rsvps.just.me.plus.more"
        case activationAndSegmentStartTime = "my.rsvps.activation.and.segment.start.time"
        case ticketAtDoorInfo = "my.rsvps.ticket.at.door.info"
    }
    
    enum Announcements: String {
        case title = "announcements.title"
        case readMore = "announcements.read.more"
        case parOf = "announcement.part.of"
        case interest = "announcement.interest"
        case circle = "announcement.circle"
        case justNow = "announcement.just.now"
        case daysAgo = "announcement.days.ago"
        case today = "announcement.today"
        case singleDayAgo = "anouncement.single.day.ago"
    }

    enum Login: String {
        case email = "login.email"
        case password = "login.password"
        case forgotPassword = "login.forgotpassword"
        case login = "login.login"
        case becomeMember = "login.become.member"
        case emailChanged = "login.email.changed"
        case unauthorizedErrorMessage = "login.unauthorized.error.message"
        case paymentRequiredErrorMessage = "login.payment.required.error.message"
    }

    enum ResetPassword: String {
        case continueButton = "reset.continue"
        case havingTrouble = "reset.trouble"
        case getHelp = "reset.help"
        case resetTitle = "login.forgotpassword"
        case resetEmailMessage = "reset.message"
        case email = "reset.email"
        case createNew = "reset.create"
        case createPlaceholder = "reset.placeholder.create"
        case newPassword = "reset.new.password"
        case confirmPassword = "reset.confirm"
        case checkInbox = "reset.check.inbox"
        case login = "reset.login"
        case submitPassword = "reset.submit"
        case noMatch = "reset.no.match"
    }

    enum GuestForm: String {
        case done = "guest.form.done"
        case name = "guest.name"
        case surname = "guest.surname"
        case email = "guest.email"
        case textFieldRequired = "guest.text.field.required"
        case textFieldOptional = "guest.text.field.optional"
        case nominate = "guest.nominate"
        case guest = "guest"
        case remove = "guest.remove"
        case add = "guest.add"
        case timeOfReservation = "guest.time.segment"
        case justMe = "guest.just.me"
        case moreGuestsPlaceholder = "guest.more.placeholder"
        case guestHowMany = "guest.howmany"
        case price = "guest.price"
        case ticket = "guest.ticket"
        case ticketInfo = "guest.ticket.info"
        case rsvpDetails = "guest.rsvp.details"
        case editTitle = "guest.edit"
        case update = "guest.update"
        case cancel = "guest.cancel"
        case noGuestInfoClick = "guest.no.guest.click.info"
        case noGuestInfoEditableIsClosed = "guest.no.guest.editable.closed"
        case onlyMembersEvent = "guest.only.members.event"
        case confirmCancel = "guest.cancel.confirm"
        case confirmCancelPayment = "guest.cancel.confirm.payment"
        case zeroPrice = "guest.zero.price"
        case paymentConfirmationAlert = "guest.payment.confirmation.alert"
        case addCard = "guest.add.card"
        case emptyCardAlert = "guest.empty.card.alert"
        case provideEmail = "guest.form.provide.email"
        case fullGuestListAlert = "guest.form.full.guest.list.alert"
        case fullGuestListOnlyYouAlert = "guest.form.full.guest.list.only.you.alert"
        case fullGuestListNumberOfAvailablePlacesAlert = "guest.form.full.guest.list.number.available.places.alert"
    }

    enum ActivationDetails: String {
        case guestHost = "details.guest.hosts"
        case parlorHost = "details.parlor.hosts"
        case notableHost = "details.notable.hosts"
        case memberHost = "details.member.hosts"
        case time = "details.time"
        case entry = "details.entry"
        case share = "details.share"
        case location = "details.location"
        case guestList = "details.guest.list"
        case live = "details.live"
        case updates = "details.updates"
        case viewOnMap = "details.view.map"
        case free = "details.free"
        case confirmed = "details.confirmed"
        case updated = "details.updated"
        case onWaitlist = "details.rsvp.waitlist"
        case stayTuned = "details.stay.tuned"
        case reservationConfirmed = "details.reservation.confirmed"
        case visitBottomNavigation = "details.visit.bottom.navigation"
        case rsvpClosed = "details.rsvp.closed"
    }
    
    enum ActivationDetailsInfo: String {
        case guestList = "details.info.guest.list"
        case updates = "details.info.updates"
        case comingSoon = "details.info.guest.list.coming.soon"
        case join = "details.info.join"
    }

    enum ShareActivation: String {
        case titleLink = "share.event.title.link"
        case titleEvent = "share.event.title.event"
        case messages = "share.event.messages"
        case mail = "share.event.mail"
        case messenger = "share.event.messenger"
        case whatsapp = "share.event.whatsapp"
        case copyLink = "share.event.copy.link"
        case more = "share.event.more"

        enum Scheme: String {
            case messenger = "fb-messenger://"
            case whatsapp = "whatsapp://"
            case messengerShare = "fb-messenger://share/"
            case whatsappShare = "whatsapp://send/"
        }
    }
    
    enum HostDetails: String {
        case interestGroups = "host.interestgroups"
        case about = "host.about"
        case position = "host.position"
        case company = "host.company"
        case education = "host.education"
    }
    
    enum Onboarding: String {
        
        case gotIt = "onboarding.gotit"
        case remindMe = "onboarding.remind.me"
        
        enum Announcements: String {
            case title = "onboarding.announcements.title"
            case message = "onboarding.announcements.message"
        }
        
        enum Home: String {
            case title = "onboarding.home.title"
            case message = "onboarding.home.message"
        }

        enum Profile: String {
            case title = "onboarding.profile.title"
            case message = "onboarding.profile.message"
        }
        
        enum GuestList: String {
            case title = "onboarding.guest.list.title"
            case message = "onboarding.guest.list.message"
        }

        enum Happenings: String {
            case title = "onboarding.happenings.title"
            case message = "onboarding.happenings.message"
        }

        enum Mixers: String {
            case title = "onboarding.mixers.title"
            case message = "onboarding.mixers.message"
        }

        enum Rsvps: String {
            case title = "onboarding.rsvps.title"
            case message = "onboarding.rsvps.message"
        }

        enum Community: String {
            case title = "onboarding.community.title"
            case message = "onboarding.community.message"
        }
    }
    
    enum Welcome: String {
        case title = "welcome.title"
        case message = "welcome.message"
        case getStarted = "welcome.getstarted"
        case youreInvited = "welcome.invited"
        case yourePremium = "welcome.premium"
        case namePlaceholder = "welcome.name.placeholder"
    }
    
    enum Register: String {
        case continueButton = "reset.continue"
        case addEmail = "register.addemail"
        case addEmailMessage = "register.addemail.message"
        case emailHint = "reset.email"
        case addBirthDate = "register.birth.date"
        case addBirhDateMessage = "register.birth.date.message"
        case addBirthDatePlaceholder = "register.birth.date.placeholder"
    }

    enum Waitlist: String {
        case title = "waitlist.title"
        case mainMessage = "waitlist.mainmessage"
        case mainTransferableMessage = "waitlist.mainmessage.transferable"
        case submessage = "waitlist.submessage"
        case removeMe = "waitlist.remove"
        case confirmTransfer = "waitlist.confirm.submessage"
        case notOnWaitlist = "waitlist.not.on.waitlist"
    }
    
    enum Interests: String {
        case suggest = "interest.suggest"
        case title = "interest.title"
        case save = "interest.save"
        case interests = "interests"
        case sendUs = "interests.send.us"
        case submitHere = "interests.submit.here"
    }
    
    enum EditProfile: String {
        case title = "edit.profile.title"
        case name = "edit.profile.name"
        case contact = "edit.profile.contact"
        case gender = "edit.profile.gender"
        case birthDate = "edit.profile.birthday"
        case education = "edit.profile.education"
        case position = "edit.profile.position"
        case firstName = "edit.profile.firstname"
        case lastName = "edit.profile.lastname"
        case street = "edit.profile.street"
        case homeZip = "edit.profile.home.zip"
        case workZip = "edit.profile.work.zip"
        case phone = "edit.profile.phone"
        case positionPlaceholder = "edit.profile.position.placeholder"
        case employer = "edit.profile.employer"
        case success = "edit.profile.success"
        case selectGender = "edit.profile.select.gender"
    }
    
    enum GuestListRsvp: String {
        case title = "guest.list.title"
        case premiumMember = "guest.list.premium.member"
        case profile = "guest.list.profile"
        case introRequest = "guest.list.intro.request"
        case sendRequestToParlor = "guest.list.send.request.to.parlor"
        case noThanks = "guest.list.no.thanks"
        case countPremiumMembers = "guest.list.count.premium.members"
        case onePremiumMember = "guest.list.one.premium.member"
        case availableForPremium = "guest.list.available.for.premium"
        case selectTheMembers = "guest.list.select.the.members"
        case guestListIsEmpty = "guest.list.is.empty"
        case introRequestSuccess = "guest.list.intro.request.success"
    }

    enum Profile: String {
        case title = "profile.title"
        case circles = "profile.circles"
        case interest = "profile.interest"
        case contact = "profile.contact"
        case settings = "profile.settings"
        case yourCircles = "profile.your.circles"
        case yourCircle = "profile.your.circle"
        case leaveCircle = "profile.leave.circle"
        case yourInterests = "profile.your.interests"
        case addMoreInterests = "profile.interests.addmore"
        case referFriend = "profile.refer.friend"
        case upgradeToPremium = "profile.upgrade"
        case emailUs = "profile.emailus"
        case suggestEventsOrVenues = "profile.suggest.events.or.venues"
        case referFriendSuccess = "profile.refer.friend.success"
        case noCirclesTitle = "profile.no.circles.title"
        case noCirclesMessage = "profile.no.circles.message"
        case pleaseContactUsAt = "profile.please.contact.us.at"
        case confirmLeavingCircle = "profile.confirm.leaving.circle"
    }

    enum MemberProfile: String {
        case about = "member.profile.about"
        case position = "member.profile.position"
        case company = "member.profile.company"
        case education = "member.profile.education"
        case interestGroups = "member.profile.interestGroups"
        case attending = "member.profile.attending"
        case viewEvent = "member.profile.view.event"
        case connected = "member.profile.connected"
        case notAccepted = "member.profile.accept"
        case requested = "member.profile.requested"
        case connect = "member.profile.connect"
        case linkedin = "member.profile.linkedin"
        case instagram = "member.profile.instagram"
    }
    
    enum ReferFriend: String {
        case referMessage = "refer.message"
        case inviteMessage = "refer.invite.message"
        case enterInfo = "refer.enter.info"
        case firstName = "refer.first.name"
        case lastName = "refer.last.name"
        case email = "refer.email"
        case invite = "refer.invite"
        case referSubMessage = "refer.sub.message"
        case remove = "refer.remove"
        case friend = "refer.friend"
    }
    
    enum AvailableGuestListPopup: String {
        case viewGuestList = "guest.list.popup.view.guest.list"
        case guestListAvailable = "guest.list.popup.guest.list.available"
    }
    
    enum LocationService: String {
        case locationAccessDenied = "location.service.access.denied"
        case locationIsDisabled = "location.service.is.disabled"
        case locationCannotBeDetermined = "location.service.cannot.determined"
    }

    enum Settings: String {
        case email = "settings.email"
        case password = "settings.password"
        case membershipRenewal = "settings.membership.renewal"
        case creditCard = "settings.credit.card"
        case mapProvider = "settings.map.provider"
        case seeManual = "settings.see.manual"
        case privacy = "settings.privacy"
        case terms = "settings.terms"
        case logout = "settings.logout"
        case calendarSync = "settings.sync"
    }
    
    enum ChangeEmail: String {
        case newEmail = "change.email.new"
        case title = "change.email.title"
        case update = "change.email.update"
    }
    
    enum ChangePassword: String {
        case currentPass = "change.pass.current"
        case newPass = "change.pass.new"
        case title = "change.pass.title"
        case update = "change.pass.update"
    }
    
    enum Upgrade: String {
        case title = "upgrade.title"
        case paymentTitle = "upgrade.payment.title"
        case standardTitle = "upgrade.standard"
        case message = "upgrade.message"
        case apply = "upgrade.apply"
        case upgradeInformationPoint = "upgrade.information.point"
    }
    
    enum MapProvider: String {
        case title = "map.provider.title"
        case appleMapAppName = "map.provider.apple.map.app.name"
        case googleMapAppName = "map.provider.google.map.app.name"
        case wazeMapAppName = "map.provider.waze.map.app.name"
        case mapQuestAppName = "map.provider.map.quest.app.name"
        case appleMapUrl = "map.provider.apple.map.url"
        case googleMapUrl = "map.provider.google.map.url"
        case wazeMapUrl = "map.provider.waze.map.url"
        case mapQuestUrl = "map.provider.map.quest.url"
    }

    enum TermsAndPrivacy: String {
        case privacyTitle = "terms.and.privacy.privacy.title"
        case termsTitle = "terms.and.privacy.terms.title"
    }
    
    enum Cards: String {
        case listTitle = "card.list.title"
        case paymentMethods = "card.list.payment.method"
        case addMethod = "card.list.add"
        case edit = "card.list.edit"
        case remove = "card.list.remove"
        case removeAlertTitle = "card.remove.alert.title"
        case removeAlertContentPlaceholder = "card.remove.alert.content"
        case fourNumberWithDots = "card.four.number.with.dots"
        case addAnother = "card.add.another"
        
        enum Add: String {
            case title = "card.add.title"
            case number = "card.add.number"
            case mm = "card.add.mm"
            case yy = "card.add.yy"
            case cvv = "card.add.cvv"
            case name = "card.add.name"
            case address1 = "card.add.address1"
            case address2 = "card.add.address2"
            case city = "card.add.city"
            case state = "card.add.state"
            case zip = "card.add.zip"
            case addButton = "card.add.button"
            case appliedPremium = "card.add.applied"
            case appliedFill = "card.applied.fill"
            case newCardAdded = "card.add.new.was.added"
        }
        
        enum Edit: String {
            case title = "card.edit.title"
            case continueButton = "card.edit.continue.button"
            case cardIsUpdated = "card.edit.updated"
        }
    }

    enum Premium: String {
        case proceed = "premium.proceed"
        case creditCard = "premium.credit.card"
        case payWith = "premium.pay.with"
        case membership = "premium.membership"
        case premium = "premium.premium"
        case standard = "premium.standard"
        case priceInterval = "upgrade.price.interval"
        case payUsingCard = "premium.pay.using.card"
        case proratedDiscount = "premium.prorated.discount"
    }
    
    enum Calendar: String {
        case title = "calendar.title"
        case syncTitle = "calendar.sync.title"
        case syncMessage = "calendar.sync.message"
        case permissionTitle = "calendar.permission.error.title"
        case permissionMessage = "calendar.permission.error.message"
        case titleForSystemCalendar = "calendar.title.for.system.calendar"
    }
    
    enum Search: String {
        case searchPlaceholder = "search.placeholder"
        case searchHistory = "search.history"
        case clearRecentSearches = "search.clear.recent.searches"
        case notFound = "search.not.found"
    }
    
    enum AddProfilePhoto: String {
        case title = "add.profile.photo.title"
        case pictureInfoMessage = "add.profile.photo.picture.info.message"
        case photoCouldNotBeUploaded = "add.profile.photo.could.not.be.updated"
    }
    
    enum BaseWebView: String {
        case pageCannotBeLoaded = "base.web.view.page.cannot.be.loaded"
    }
    
    enum MembershipPlans: String {
        case navTitle = "membership.plans.nav.title"
        case somethingAboutItsCool = "membership.plans.something.about.its.cool"
        case standard = "membership.plans.standard"
        case premium = "membership.plans.premium"
        case free = "membership.plans.free"
        case toActivateMembership = "membership.to.activate.membership"
        case toExtendMembership = "membership.to.extend.membership"
        case daysCount = "membership.days.count"
        case oneDayCount = "membership.one.day.count"
        case moreButtonTitle = "membership.more.button.title"
        case lastDay = "membership.last.day"
    }
    
    enum NotificationService: String {
        case open = "notification.service.open"
    }
    
    enum Validation: String {
        case notBeEmpty = "validation.must.not.be.empty"
        case maxLength = "validation.max.length"
        case minLength = "validation.min.length"
        case passwordPower = "validation.password.power"
        case dateMustBeLeastYears = "validation.date.must.be.least.years"
        case monthCardInvalid = "validation.month.card.is.invalid"
        case yearCardInvalid = "validation.year.card.is.invalid"
    }
    
    enum EnableNotificationsReminder: String {
        case `continue` = "enable.notification.reminder.continue"
        case notificationTitle = "enable.notification.reminder.title"
        case notificationPersmissionMessage = "enable.notification.reminder.permission.message"
        case notificationPermissionsAlertTitle = "enable.notification.reminder.permission.error.title"
        case notificationPermissionsAlertMessage = "enable.notification.reminder.permission.error.message"
        case notificationSwitchTitle = "enable.notification.reminder.switch.title"
    }

    enum UserNotApproved: String {
        case allowNotifications = "user.not.approved.allow.notifications"
        case notificationsAllowedTitle = "user.not.approved.notifications.allowed.title"
        case notificationsNotAllowedTitle = "user.not.approved.notifications.not.allowed.title"
        case applicationStatusNotificationsAllowedTitle = "user.not.approved.application.status.notifications.allowed.title"
        case applicationStatusNotificationsNotAllowedTitle = "user.not.approved.application.status.notifications.not.allowed.title"
        case logOut = "user.not.approved.log.out"
        case confirmLogoutTitle = "user.not.approved.log.out.confirm.title"
        case followUs = "user.not.approved.follow.us"
    }

    enum AppUpdateRequired: String {
        case updateRequired = "app.update.required.update.required"
        case updateInfo = "app.update.required.update.info"
        case update = "app.update.required.update"
    }
    
    enum ApplePay: String {
        case applePay = "apple.pay"
        case notAvailable = "apple.pay.not.available"
        case unexpectedError = "apple.pay.unexpected.error"
    }
    
    enum Community: String {
        case title = "community.title"
    }
    
    enum Connections: String {
        case connectWithFriends = "connections.connect.with.friends"
        case syncContacts = "connections.sync.contacts"
        case referFriend = "connections.refer.a.friend"
        case viewGuestLists = "connections.view.guest.lists"
        case searchPeople = "connections.search.people.on.parlor"
        case myConnections = "connections.my.connections"
        case accept = "connections.request.accept"
        case viewMore = "connections.view.more"
        case connectionRequests = "connections.connection.requests"
    }

    enum ConnectionRequests: String {
        case title = "connection.requests.title"
    }

    enum MyConnections: String {
        case title = "my.connections.title"
        case shareTitle = "my.connections.share.title"
        case shareSyncButton = "my.connections.share.sync.button"
        case shareReferButton = "my.connections.share.refer.button"
    }

//    enum ConnectionsAttending: String {
//        case title = "connections.attending.title"
//    }
}
