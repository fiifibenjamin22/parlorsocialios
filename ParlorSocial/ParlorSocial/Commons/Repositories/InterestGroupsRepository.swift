//
//  InterestGroupsRepository.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 09/05/2019.
//

import Foundation
import RxSwift

protocol InterestGroupsRepositoryProtocol {
    func proposeInterestGroup(withName name: String) -> MessageApiResponse
    func getInterestGroups(atPage page: Int) -> Observable<InterestGroupsResposne>
    func updateInterestGroups(_ data: [InterestGroup]) -> MessageApiResponse
}

final class InterestGroupsRepository: InterestGroupsRepositoryProtocol {
    static let shared: InterestGroupsRepositoryProtocol = InterestGroupsRepository()
    
    private init() {}
    
    private let interestService: InterestGroupsApiService = ApiService.shared
    private let profileRepository = ProfileRepository.shared
}

extension InterestGroupsRepository {
    
    func proposeInterestGroup(withName name: String) -> MessageApiResponse {
        return interestService.proposeInterestGroup(withName: name)
    }
    
    func getInterestGroups(atPage page: Int) -> Observable<InterestGroupsResposne> {
        return interestService.getInterestGroups(atPage: page)
    }
    
    func updateInterestGroups(_ data: [InterestGroup]) -> MessageApiResponse {
        return interestService.updateInterestGroups(InterestsGroupUpdateRequest(ids:
            Dictionary(uniqueKeysWithValues: data.map { interest in (interest.id, interest.isSelected ?? false) })
        )).doOnNext({ [unowned self] response in
            if case ApiResponse.success(_) = response {
                self.profileRepository.updateKnownInterestsGroups(basedOn: data)
            }
        })
    }
    
}
