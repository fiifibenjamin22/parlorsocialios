//
//  SearchRepository.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 31/07/2019.
//

import RxSwift

protocol SearchRepositoryProtocol {
    func search(withPhrase query: String) -> Observable<SearchApiResponse>
}

final class SearchRepository: SearchRepositoryProtocol {
    
    static let shared: SearchRepositoryProtocol = SearchRepository()
    
    private let apiService: SearchApiService = ApiService.shared
    
    private init() { }

}

extension SearchRepository {
    
    func search(withPhrase query: String) -> Observable<SearchApiResponse> {
        return apiService.search(withPhrase: query)
    }
    
}
