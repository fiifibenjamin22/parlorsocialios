//
//  AnalyticRepository.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 07/10/2019.
//  Copyright © 2019 KISSdigital. All rights reserved.
//

import RxSwift

protocol AnalyticRepositoryProtocol {
    func sendEvents(with data: AnalyticEventRequest) -> MessageApiResponse
}

final class AnalyticRepository: AnalyticRepositoryProtocol {
    
    static let shared: AnalyticRepositoryProtocol = AnalyticRepository()
    
    private let apiService: AnalyticApiService = ApiService.shared
    
    private init() { }

}

extension AnalyticRepository {
    
    func sendEvents(with data: AnalyticEventRequest) -> MessageApiResponse {
        return apiService.sendEvents(with: data)
    }
    
}
