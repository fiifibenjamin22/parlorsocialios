//
//  GuestListRsvpRepository.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 23/05/2019.
//

import Foundation
import RxSwift

protocol GuestListRsvpRepositoryProtocol {
    func getGuestList(forActivationWithId id: Int) -> Observable<GuestListRsvpMetaResponse>
    func sendIntroRequest(forActivationWithId id: Int, data: [GuestListGuest]) -> Observable<GuestListRsvpResponse>
    func checkInGuestList(forActivationId id: Int, data: CheckInRequest) -> Observable<RsvpResponse>
}

final class GuestListRsvpRepository: GuestListRsvpRepositoryProtocol {
    static let shared: GuestListRsvpRepositoryProtocol = GuestListRsvpRepository()

    private let guestListRsvpService: GuestListRsvpApiService = ApiService.shared
    private let activationsRepository = ActivationsRepository.shared
    private let disposeBag = DisposeBag()

    private init() {}

    private func updateActivation(with id: Int, rsvp: Rsvp) {
        self.activationsRepository.getActivation(withId: id)
            .doOnNext { [unowned self] response in
                guard case ApiResponse.success(let activationData) = response else { return }

                self.activationsRepository.updateNewActivations(
                    activation: activationData.data.with(
                        rsvp: rsvp,
                        checkInStrategy: .notRequired
                    )
                )
        }.subscribe().disposed(by: self.disposeBag)
    }
}

extension GuestListRsvpRepository {
    func getGuestList(forActivationWithId id: Int) -> Observable<GuestListRsvpMetaResponse> {
        return guestListRsvpService.getGuestList(forActivationWithId: id)
    }
    
    func sendIntroRequest(forActivationWithId id: Int, data: [GuestListGuest]) -> Observable<GuestListRsvpResponse> {
        let sendIntroRequest = SendIntroRequest(ids: data.compactMap { return $0.id })
        return guestListRsvpService.sendIntroRequest(forActivationWithId: id, data: sendIntroRequest)
    }
    
    func checkInGuestList(forActivationId id: Int, data: CheckInRequest) -> Observable<RsvpResponse> {
        return guestListRsvpService.checkInGuestList(
            forActivationWithId: id,
            data: data
        ).doOnNext { [unowned self] response in
            guard case ApiResponse.success(let rsvpData) = response else { return }

            self.updateActivation(with: id, rsvp: rsvpData.data)
        }.asObservable()
    }
}
