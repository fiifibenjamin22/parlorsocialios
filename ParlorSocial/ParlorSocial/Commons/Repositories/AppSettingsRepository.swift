//  AppSettingsRepository.swift
//  ParlorSocialClub
//
//  Created on 08/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import RxSwift

protocol AppSettingsRepositoryProtocol {
    func getAppVersionInfo() -> Observable<AppUpdateInfoResponse>
}

final class AppSettingsRepository: AppSettingsRepositoryProtocol {
    
    // MARK: - Properties
    
    private let apiService: AppSettingsService
    
    // MARK: - Init
    
    init(apiService: AppSettingsService = ApiService.shared) {
        self.apiService = apiService
    }
    
    // MARK: - AppSettingsRepositoryProtocol

    func getAppVersionInfo() -> Observable<AppUpdateInfoResponse> {
        return apiService.getAppVersionInfo()
    }
}
