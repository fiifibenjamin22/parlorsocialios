//
//  AttendingsRepository.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 28/08/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation
import RxSwift

protocol AttendingRepositoryProtocol {
    func getMyConnections(using model: MyConnections.Get.Request) -> MyConnectionsResponseObs
    func getConnectionRequests(using model: ConnectionRequests.Get.Request) -> ConnectionRequestsResponseObs
    func createConnection(using data: CreateConnectionRequest) -> ConnectionChangeApiResponseObs
    func updateConnection(with connectionId: Int, using data: UpdateConnectionRequest) -> ConnectionChangeApiResponseObs
    func deleteConnection(with connectionId: Int) -> MessageApiResponse
}

final class AttendingsRepository {

    static let shared: AttendingRepositoryProtocol = AttendingsRepository()

    private let apiService: ConnectionsApiService = ApiService.shared

    private init() { }

}

extension AttendingsRepository: AttendingRepositoryProtocol {
    
    func getMyConnections(using model: MyConnections.Get.Request) -> MyConnectionsResponseObs {
        return apiService.getMyConnections(using: model)
    }

    func getConnectionRequests(using model: ConnectionRequests.Get.Request) -> ConnectionRequestsResponseObs {
        return apiService.getConnectionRequests(using: model)
    }
    
    func createConnection(using data: CreateConnectionRequest) -> ConnectionChangeApiResponseObs {
        return apiService.createConnection(using: data)
    }

    func updateConnection(with connectionId: Int, using data: UpdateConnectionRequest) -> ConnectionChangeApiResponseObs {
        return apiService.updateConnection(with: connectionId, using: data)
    }
    
    func deleteConnection(with connectionId: Int) -> MessageApiResponse {
        return apiService.deleteConnection(with: connectionId)
    }
}
