//
//  GetHomeRsvps.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 09/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

enum HomeRsvps {
    enum Get {
        struct Request: Codable {
            let with: String = "hosts.interestGroups"

            private(set) var page: Int

            private enum CodingKeys: String, CodingKey {
                case page
            }

            static var initial: Request {
                return Request(page: 1)
            }

            mutating func set(page: Int) {
                self.page = page
            }

            mutating func increasePage() {
                self.page += 1
            }

        }

        struct Response: Codable & PaginatableResponse {
            typealias ListElement = HomeRsvp

            let data: [HomeRsvp]
            let meta: ListMetadata

            func withRsvps(_ activations: [HomeRsvp]) -> Response {
                return Response(data: activations, meta: self.meta)
            }
        }
    }
}
