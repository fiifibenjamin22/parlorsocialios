//
//  RsvpsRepository.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 09/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import RxSwift

protocol HomeRepositoryProtocol {
    var newActivationsNotificationSubject: PublishSubject<Bool> { get }

    func getHomeActivations(using model: HomeActivations.Get.Request) -> HomeActivationsResponseObs
    func getHomeActivation(withId id: Int) -> Observable<SingleHomeActivationResposne>
    func getRsvps(using model: HomeRsvps.Get.Request) -> HomeRsvpsResponseObs
    func getRsvp(withId id: Int) -> Observable<SingleHomeRsvpResposne>
}

final class HomeRepository {
    static let shared: HomeRepositoryProtocol = HomeRepository()

    let newHomeActivationsSubject: PublishSubject<HomeActivation> = PublishSubject()
    let newRsvpsSubject: PublishSubject<HomeRsvp> = PublishSubject()
    let newActivationsNotificationSubject: PublishSubject<Bool> = PublishSubject()

    private let apiService: HomeApiService = ApiService.shared

    private init() { }
}

extension HomeRepository: HomeRepositoryProtocol {
    @discardableResult
    func getHomeActivations(using model: HomeActivations.Get.Request) -> HomeActivationsResponseObs {
        return apiService.getHomeActivations(using: model).doOnNext { [unowned self] resposne in
            if case let ApiResponse.success(item) = resposne {
                self.newActivationsNotificationSubject.onNext(false)
                item.data.forEach { self.newHomeActivationsSubject.onNext($0) }
            }
        }
    }

    func getHomeActivation(withId id: Int) -> Observable<SingleHomeActivationResposne> {
        return apiService.getHomeActivation(withId: id).doOnNext { [unowned self] resposne in
            if case let ApiResponse.success(item) = resposne {
                self.newHomeActivationsSubject.onNext(item.data)
            }
        }
    }

    @discardableResult
    func getRsvps(using model: HomeRsvps.Get.Request) -> HomeRsvpsResponseObs {
        return apiService.getRsvps(using: model).doOnNext { [unowned self] resposne in
            if case let ApiResponse.success(item) = resposne {
                item.data.forEach { self.newRsvpsSubject.onNext($0) }
            }
        }
    }

    func getRsvp(withId id: Int) -> Observable<SingleHomeRsvpResposne> {
        return apiService.getRsvp(withId: id).doOnNext { [unowned self] resposne in
            if case let ApiResponse.success(item) = resposne {
                self.newRsvpsSubject.onNext(item.data)
            }
        }
    }
}
