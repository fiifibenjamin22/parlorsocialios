//
//  ActivationsRepository.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 27.02.2019.
//

import Foundation
import RxSwift

protocol ActivationsRepositoryProtocol {
    var newActivationDataObs: Observable<Activation> { get }
    var activationWithIdChangedObs: Observable<Int> { get }
    var rsvpWithIdCanceledObs: Observable<Int> { get }

    func getActivations(using model: Activations.Get.Request) -> ActivationsResponseObs
    func getActivation(withId id: Int) -> Observable<SingleActivationResposne>
    func getActivationInfo(forActivationWithId activationId: Int) -> Observable<ActivationInfoResposne>
    func getActivationUpdates(forActivationWithId activationId: Int, using model: ActivationUpdates.Get.Request) -> ActivationUpdatesResponseObs
    func getShareData(forActivationId id: Int) -> Observable<ShareDataResponse>
    func createRsvp(forActivation activation: Activation, data: RsvpRequest) -> Observable<RsvpResponse>
    func getRsvpsStartDates() -> Observable<DatesApiResponse>
    func updateRsvp(forActivation activation: Activation, data: RsvpRequest) -> Observable<RsvpResponse>
    func cancelRsvp(forActivation activation: Activation) -> MessageApiResponse
    func transferRsvpToGuestList(forActivation activation: Activation, data: RsvpTransferRequest) -> MessageApiResponse
    func notifyMeRsvpAvailability(forActivation activation: Activation) -> MessageApiResponse
    func updateNewActivations(activation: Activation)
    func getAmountOfSlots(activationId: Int) -> Observable<AvailableSlotsResponse>
}

final class ActivationsRepository {

    static let shared: ActivationsRepositoryProtocol = ActivationsRepository()
    
    private init() { }

    let newActivationsSubject: PublishSubject<Activation> = PublishSubject()
    let newActivationUpdatesSubject: PublishSubject<ActivationUpdate> = PublishSubject()

    private let createRsvpSubject: PublishSubject<Activation> = PublishSubject()

    private let activationWithIdChangedSubject: PublishSubject<Int> = PublishSubject()
    private let rsvpWithIdCanceledSubject: PublishSubject<Int> = PublishSubject()
    private let apiService: ActivationsApiService = ApiService.shared
    
}

extension ActivationsRepository: ActivationsRepositoryProtocol {
    var activationWithIdChangedObs: Observable<Int> {
        return activationWithIdChangedSubject.asObservable()
    }

    var rsvpWithIdCanceledObs: Observable<Int> {
        return rsvpWithIdCanceledSubject.asObservable()
    }

    var newActivationDataObs: Observable<Activation> {
        return newActivationsSubject.asObservable()
    }

    @discardableResult
    func getActivations(using model: Activations.Get.Request) -> ActivationsResponseObs {
        return apiService.getActivations(using: model).doOnNext { [unowned self] resposne in
            if case let ApiResponse.success(item) = resposne {
                item.data.forEach { self.newActivationsSubject.onNext($0) }
            }
        }
    }
    
    func getActivation(withId id: Int) -> Observable<SingleActivationResposne> {
        return apiService.getActivation(withId: id).doOnNext { [unowned self] response in
            if case let ApiResponse.success(item) = response {
                self.newActivationsSubject.onNext(item.data)
            }
        }
    }

    func getShareData(forActivationId id: Int) -> Observable<ShareDataResponse> {
        return apiService.getShareData(forActivationWithId: id)
    }
    
    func getActivationInfo(forActivationWithId activationId: Int) -> Observable<ActivationInfoResposne> {
        return apiService.getActivationInfo(forActivationWithId: activationId)
    }
    
    func getActivationUpdates(forActivationWithId activationId: Int, using model: ActivationUpdates.Get.Request) -> ActivationUpdatesResponseObs {
        return apiService.getActivationUpdates(forActivationWithId: activationId, using: model).doOnNext { [unowned self] response in
            if case let ApiResponse.success(item) = response {
                item.data.forEach { self.newActivationUpdatesSubject.onNext($0) }
            }
        }
    }
    
    func createRsvp(forActivation activation: Activation, data: RsvpRequest) -> Observable<RsvpResponse> {
        return apiService.createRsvp(forActivationWithId: activation.id, data: data).doOnNext { [unowned self] response in
            if case let ApiResponse.success(item) = response {
                let rsvp = item.data
                self.newActivationsSubject.onNext(activation.withRsvp(rsvp))
                self.activationWithIdChangedSubject.onNext(activation.id)
                if rsvp.status == .guestList {
                    self.handleNewCalendarEvent(forActivation: activation, basedOn: rsvp)
                }
            }
        }
    }

    func getRsvpsStartDates() -> Observable<DatesApiResponse> {
        return apiService.getRsvpsStartDates()
    }

    func updateRsvp(forActivation activation: Activation, data: RsvpRequest) -> Observable<RsvpResponse> {
        return apiService.updateRsvp(forActivationWithId: activation.id, data: data).doOnNext { [unowned self] response in
            if case let ApiResponse.success(item) = response {
                self.activationWithIdChangedSubject.onNext(activation.id)
                self.newActivationsSubject.onNext(activation.withRsvp(item.data))
            }
        }
    }
    
    func cancelRsvp(forActivation activation: Activation) -> MessageApiResponse {
        return apiService.cancelRsvp(forActivationWithId: activation.id).doOnNext { [unowned self] response in
            if case ApiResponse.success = response {
                self.activationWithIdChangedSubject.onNext(activation.id)
                self.rsvpWithIdCanceledSubject.onNext(activation.id)
                self.newActivationsSubject.onNext(activation.withRsvp(nil))
            }
        }
    }
    
    func transferRsvpToGuestList(forActivation activation: Activation, data: RsvpTransferRequest) -> MessageApiResponse {
        return apiService.transferRsvpToGuestList(forActivationWithId: activation.id, data: data).doOnNext { [unowned self] response in
            if case ApiResponse.success = response {
                let updatedActivation = activation.withRsvp(activation.rsvp?.withStatus(.guestList))
                self.newActivationsSubject.onNext(updatedActivation)
                self.handleNewCalendarEvent(forActivation: activation, basedOn: updatedActivation.rsvp)
            }
        }
    }
    
    func notifyMeRsvpAvailability(forActivation activation: Activation) -> MessageApiResponse {
        return apiService.notifyMeRsvpAvailability(forActivationWithId: activation.id).doOnNext { [unowned self] response in
            if case ApiResponse.success = response {
                self.newActivationsSubject.onNext(activation.withIsNotifyMe(true))
            }
        }
    }
    
    func updateNewActivations(activation: Activation) {
        newActivationsSubject.onNext(activation)
    }

    private func handleNewCalendarEvent(forActivation activation: Activation, basedOn rsvp: Rsvp?) {
        guard let rsvp = rsvp, CalendarUtilities.isSyncOn else { return }
        CalendarUtilities.shared.addRsvp(rsvp, forActivation: activation)
    }
    
    func getAmountOfSlots(activationId: Int) -> Observable<AvailableSlotsResponse> {
        return apiService.getAmountOfSlots(forActivationId: activationId)
    }
}
