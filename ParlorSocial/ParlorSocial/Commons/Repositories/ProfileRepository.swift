//
//  ProfileRepository.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 05/04/2019.
//

import Foundation
import RxSwift

protocol ProfileRepositoryProtocol {
    var knownCircles: Observable<[Circle]> { get }
    var knownInterests: Observable<[InterestGroup]> { get }
    var knownCardsObs: Observable<[CardData]> { get }
    var currentProfileObs: Observable<Profile> { get }
    var currentColorThemeObs: Observable<PremiumColorManager.Theme> { get }
    func getEnableNotificationsSettingsRemindModel(_ completionHandler: @escaping (EnableNotificationsSettingsRemindModel) -> Void)
    func subscribeToPremium(usingCardWithId id: Int, planId: Int) -> MessageApiResponse
    func addCard(withData data: CardUploadData) -> Observable<SingleCardResponse>
    func addCard(usingToken token: CardTokenRequest) -> Observable<SingleCardResponse>
    func editCard(withData data: EditCardRequest, cardId: Int) -> Observable<SingleCardResponse>
    func removeCard(withId id: Int) -> Observable<EmptyResponse>
    func getCardDetails(withId id: Int) -> Observable<CardDetailsResponse>
    func changeDefaultCard(to newCardId: Int) -> Observable<SingleCardResponse>
    func changePassword(using data: ChangePasswordRequest) -> Observable<ChangePasswordResponse>
    func changeEmail(using data: ChangeEmailRequest) -> MessageApiResponse
    func createNewPassword(forUserWithEmail email: String) -> Observable<ApiResponse<MessageResponse>>
    func resetPassword(using requestData: ResetPasswordRequest) -> Observable<ApiResponse<MessageResponse>>
    func getWelcomeScreens() -> Observable<WelcomeScreensResposne>
    func getUserCards() -> Observable<CardsResponse>
    func getMyProfileData() -> Observable<ProfileResponse>
    func updateProfile(withData data: ProfileUpdateModel) -> Observable<ProfileResponse>
    func uploadNewPhoto(_ image: UIImage) -> Observable<ProfileResponse>
    func getMyCircles() -> Observable<CirclesResposne>
    func removeMyCircle(withId id: Int) -> Observable<EmptyResponse>
    func getMyInterestGroups() -> Observable<MyInterestGroupsResponse>
    func updateKnownInterestsGroups(basedOn data: [InterestGroup])
    func getSettingsInfo() -> Observable<SettingsResponse>
    func getPlanInfo() -> Observable<PlanInfoResponse>
    func inviteFriend(with data: ReferFriendData) -> MessageApiResponse
    func getMembershipPlans(using model: MembershipPlans.Get.Request) -> Observable<MembershipPlansResponse>
    func getPremiumPlanInfo() -> Observable<PlanInfoResponse>
    func refreshProfileData()
}

class ProfileRepository: ProfileRepositoryProtocol {
    
    static let shared: ProfileRepositoryProtocol = ProfileRepository()
    
    private init() {
        bindInterestsUpdates()
    }
    
    private let profileService: ProfileApiService = ApiService.shared
    private let notificationCenter: UNUserNotificationCenter = UNUserNotificationCenter.current()
    private let disposeBag: DisposeBag = DisposeBag()
    
    private let currentProfileSubject: BehaviorSubject<CachedProfile> = BehaviorSubject(value: CachedProfile(value: Config.userProfile))
    private let knownUserInterestsSubject: BehaviorSubject<[InterestGroup]> = BehaviorSubject(value: [])
    private let knownUserCirclesSubject: BehaviorSubject<[Circle]> = BehaviorSubject(value: [])
    private let knownCardsSubject: BehaviorSubject<[CardData]> = BehaviorSubject(value: [])
    private let currentColorThemeSubject: BehaviorSubject<PremiumColorManager.Theme> = BehaviorSubject(value: PremiumColorManager.shared.currentTheme)

    private let successfullInterestsUpdates: PublishSubject<[InterestGroup]> = PublishSubject()
    
    private func bindInterestsUpdates() {
        _ = successfullInterestsUpdates.withLatestFrom(knownUserInterestsSubject) { ($0, $1) }
            .map { (updateData, knownInterests) in
                let removedIds = updateData.filter { $0.isSelected == false }.map { $0.id }
                let added = updateData.filter { $0.isSelected == true }
                return (knownInterests.filter { !removedIds.contains($0.id) } + added)
                    .uniqueElements
                    .sorted(by: { $0.name < $1.name })
            }.bind(to: knownUserInterestsSubject)
    }
}

extension ProfileRepository {
    
    func subscribeToPremium(usingCardWithId id: Int, planId: Int) -> MessageApiResponse {
        return profileService.subscribeToPremium(using: SubscriptionRequest(cardId: id, planId: planId))
            .flatMap { [unowned self] messageResponse in
                self.getMyProfileData().map { _ in return messageResponse }
            }
    }
    
    func addCard(withData data: CardUploadData) -> Observable<SingleCardResponse> {
        return profileService.addCard(withData: data)
            .withLatestFrom(knownCardsSubject) { ($0, $1) }
            .doOnNext({ [unowned self] response, knownCards in
                if case let ApiResponse.success(item) = response {
                    self.knownCardsSubject.onNext(knownCards.mapOnCardAdded(withData: item.data))
                }
            }).map { $0.0 }
    }
    
    func addCard(usingToken token: CardTokenRequest) -> Observable<SingleCardResponse> {
        return profileService.addCard(usingToken: token)
            .withLatestFrom(knownCardsSubject) { ($0, $1) }
            .doOnNext({ [unowned self] response, knownCards in
                if case let ApiResponse.success(item) = response {
                    self.knownCardsSubject.onNext(knownCards.mapOnCardAdded(withData: item.data))
                }
            }).map { $0.0 }
    }
    
    func editCard(withData data: EditCardRequest, cardId: Int) -> Observable<SingleCardResponse> {
        return profileService.editCard(withData: data, cardId: cardId)
    }
    
    func removeCard(withId id: Int) -> Observable<EmptyResponse> {
        return profileService.removeCard(withId: id)
    }
    
    func getCardDetails(withId id: Int) -> Observable<CardDetailsResponse> {
        return profileService.getCardDetails(withId: id)
    }
    
    func changeDefaultCard(to newCardId: Int) -> Observable<SingleCardResponse> {
        let currentlyKnown = (try? knownCardsSubject.value()) ?? []
        self.knownCardsSubject.onNext(currentlyKnown.mapOnDefualtChanged(newDefaultId: newCardId))
        return profileService.changeDefaultCard(to: newCardId)
            .doOnNext({ [unowned self] response in
                if case ApiResponse.failure = response {
                    self.knownCardsSubject.onNext(currentlyKnown)
                }
            })
    }
    
    func changePassword(using data: ChangePasswordRequest) -> Observable<ChangePasswordResponse> {
        return profileService.changePassword(using: data).doOnNext({ response in
            if case let ApiResponse.success(item) = response {
                Config.updateTokenData(withAccessToken: item.data.accessToken, refreshToken: item.data.refreshToken)
            }
        })
    }
    
    func changeEmail(using data: ChangeEmailRequest) -> MessageApiResponse {
        return profileService.changeEmail(using: data)
    }
    
    var knownCircles: Observable<[Circle]> {
        return knownUserCirclesSubject.asObservable()
    }
    
    var knownInterests: Observable<[InterestGroup]> {
        return knownUserInterestsSubject.asObservable()
    }
    
    var knownCardsObs: Observable<[CardData]> {
        return knownCardsSubject.asObservable()
    }
    
    var currentProfileObs: Observable<Profile> {
        return currentProfileSubject.map(\.value).asObservable().ignoreNil()
    }
    
    var currentColorThemeObs: Observable<PremiumColorManager.Theme> {
        return currentColorThemeSubject.asObservable()
    }
    
    func getEnableNotificationsSettingsRemindModel(_ completionHandler: @escaping (EnableNotificationsSettingsRemindModel) -> Void) {
        notificationCenter.getNotificationSettings { settings in
            completionHandler(EnableNotificationsSettingsRemindModel(
                enableNotificationsRemindLevel: NotificationsConfig.enableNotificationsRemindLevel ?? .zero,
                rsvpsCount: NotificationsConfig.rsvpsCount,
                lastRejectNotificationsPermissionsDate: NotificationsConfig.lastRejectNotificationsPermissionsDate,
                appEntryCount: NotificationsConfig.appEntryCount,
                notificationsPermissions: settings.authorizationStatus
            ))
        }
    }
    
    func createNewPassword(forUserWithEmail email: String) -> Observable<ApiResponse<MessageResponse>> {
        return profileService.createNewPassword(forUserWithEmail: email)
    }
    
    func resetPassword(using requestData: ResetPasswordRequest) -> Observable<ApiResponse<MessageResponse>> {
        return profileService.resetPassword(using: requestData)
    }
    
    func getWelcomeScreens() -> Observable<WelcomeScreensResposne> {
        return profileService.getWelcomeScreens()
    }
    
    func getUserCards() -> Observable<CardsResponse> {
        return profileService.getUserCards().doOnNext({ [weak self] response in
            if case let ApiResponse.success(item) = response {
                self?.knownCardsSubject.onNext(item.data)
            }
        })
    }
    
    func getMyProfileData() -> Observable<ProfileResponse> {
        return profileService.getMyProfileData().updatingOnSuccess(using: currentProfileSubject, colorThemeSubject: currentColorThemeSubject)
    }
    
    func updateProfile(withData data: ProfileUpdateModel) -> Observable<ProfileResponse> {
        return profileService.updateProfile(withData: data).updatingOnSuccess(using: currentProfileSubject, colorThemeSubject: currentColorThemeSubject)
    }
    
    func uploadNewPhoto(_ image: UIImage) -> Observable<ProfileResponse> {
        return profileService.uploadNewPhoto(withData: ImageUploadModel(image: image)).updatingOnSuccess(using: currentProfileSubject, colorThemeSubject: currentColorThemeSubject)
    }
    
    func getMyCircles() -> Observable<CirclesResposne> {
        return profileService.getMyCircles().doOnNext({ [weak self] response in
            if case let ApiResponse.success(item) = response {
                self?.knownUserCirclesSubject.onNext(item.data)
            }
        })
    }
    
    func removeMyCircle(withId id: Int) -> Observable<EmptyResponse> {
        return profileService.removeMyCircle(withId: id).doOnNext({ [unowned self] response in
            if case EmptyResponse.success = response {
                if let currentCircles = try? self.knownUserCirclesSubject.value() {
                    self.knownUserCirclesSubject.onNext(currentCircles.filter { $0.id != id })
                }
            }
        })
    }
    
    func getMyInterestGroups() -> Observable<MyInterestGroupsResponse> {
        return profileService.getMyInterestGroups().doOnNext({ [weak self] response in
            if case let ApiResponse.success(item) = response {
                self?.knownUserInterestsSubject.onNext(item.data)
            }
        })
    }
    
    func updateKnownInterestsGroups(basedOn data: [InterestGroup]) {
        successfullInterestsUpdates.onNext(data)
    }
    
    func getSettingsInfo() -> Observable<SettingsResponse> {
        return profileService.getSettingsInfo().doOnNext { resposne in
            if case let ApiResponse.success(data) = resposne {
                Config.globalSettings = data.data
            }
        }
    }
    
    func getPlanInfo() -> Observable<PlanInfoResponse> {
        return profileService.getPlanData()
    }
    
    func inviteFriend(with data: ReferFriendData) -> MessageApiResponse {
        return profileService.inviteFriend(with: data)
    }
    
    func getMembershipPlans(using model: MembershipPlans.Get.Request) -> Observable<MembershipPlansResponse> {
        return profileService.getMembershipPlans(using: model)
    }
    
    func getPremiumPlanInfo() -> Observable<PlanInfoResponse> {
        return profileService.getPremiumPlanInfo()
    }
    
    func refreshProfileData() {
        do {
            let profile = try currentProfileSubject.value()
            if profile.isUpdateDataRequired() {
                getMyProfileData().subscribe().disposed(by: disposeBag)
            } else {
                currentProfileSubject.onNext(profile)
            }
        } catch {
            getMyProfileData().subscribe().disposed(by: disposeBag)
        }
    }
}

fileprivate extension Observable where Element == ProfileResponse {
    
    func updatingOnSuccess(using subject: BehaviorSubject<CachedProfile>, colorThemeSubject: BehaviorSubject<PremiumColorManager.Theme>) -> Observable<ProfileResponse> {
        return doOnNext { [weak subject] resposne in
            if case let ApiResponse.success(item) = resposne {
                if Config.userProfile?.isPremium != item.data.isPremium {
                    colorThemeSubject.onNext(PremiumColorManager.shared.theme(from: item.data))
                }
                Config.userProfile = item.data
                let cachedProfile = CachedProfile(value: item.data, timestamp: Date())
                subject?.onNext(cachedProfile)
            }
        }
    }
    
}

fileprivate extension Observable where Element == ProfileMessageResponse {
    
    func updatingOnSuccess(using subject: BehaviorSubject<Profile?>, colorThemeSubject: BehaviorSubject<PremiumColorManager.Theme>) -> Observable<ProfileMessageResponse> {
        return doOnNext { [weak subject] resposne in
            if case let ApiResponse.success(item) = resposne {
                if Config.userProfile?.isPremium != item.data.isPremium {
                    colorThemeSubject.onNext(PremiumColorManager.shared.theme(from: item.data))
                }
                Config.userProfile = item.data
                subject?.onNext(item.data)
            }
        }
    }
    
}

fileprivate extension Array where Element == CardData {
    
    func mapOnCardAdded(withData newCardData: CardData) -> [CardData] {
        let currentCards = self.map { $0.withDefault(newCardData.isDefault ? false : $0.isDefault) }
        return currentCards + [newCardData]
    }
    
    func mapOnDefualtChanged(newDefaultId: Int) -> [CardData] {
        return self.map {
            $0.withDefault($0.id == newDefaultId)
        }
    }
    
}
