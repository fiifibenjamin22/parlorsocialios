//
//  ProcessableState.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 01/03/2019.
//

import Foundation

protocol ProcessableState {
    var isProcessing: Bool { get }
}
