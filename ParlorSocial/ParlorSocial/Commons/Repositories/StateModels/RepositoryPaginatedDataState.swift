//
//  RepositoryPaginatedDataState.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 01/03/2019.
//

import Foundation

enum RepositoryPaginatedDataState<ListModel> {
    case loading
    case paging(elements: [ListModel])
    case populated(elements: [ListModel])
    case error(_ error: AppError)
}

extension RepositoryPaginatedDataState {

    var elemets: [ListModel] {
        switch self {
        case .paging(let elements): return elements
        case .populated(let elements): return elements
        default: return []
        }
    }

}

extension RepositoryPaginatedDataState: ProcessableState {

    var isProcessing: Bool {
        switch self {
        case .loading: return true
        default: return false
        }
    }

}
