//
//  RepositoryDataState.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 01/03/2019.
//

import Foundation

enum RepositoryDataState<ResponseModel> {
    case processing
    case succeed(response: ResponseModel)
    case error(_ error: AppError)
}

extension RepositoryDataState: ProcessableState {

    var isProcessing: Bool {
        switch self {
        case .processing: return true
        default: return false
        }
    }

}
