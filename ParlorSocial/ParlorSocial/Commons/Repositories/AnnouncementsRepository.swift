//
//  AnnouncementsRepository.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 03/07/2019.
//

import RxSwift

protocol AnnouncementsRepositoryProtocol {
    var unreadAnnouncementsObs: Observable<Bool> { get }
    func getAnnouncement(withId id: Int) -> Observable<AnnouncementApiResponse>
    func getAnnouncements(atPage page: Int) -> Observable<AnnouncementsApiResponse>
    func getUnreadAnnouncementsInfo() -> Observable<AnnouncementsInfoApiResponse>
    func setUnreadAnnouncementsSubject(_ value: Bool)
}

final class AnnouncementsRepository: AnnouncementsRepositoryProtocol {
    
    static let shared: AnnouncementsRepositoryProtocol = AnnouncementsRepository()
    
    private let announcementsService: AnnouncementsApiService = ApiService.shared
        
    private let unreadAnnouncementsSubject: BehaviorSubject<Bool> = .init(value: false)
    
    private init() { }
    
    var unreadAnnouncementsObs: Observable<Bool> {
        return unreadAnnouncementsSubject.asObservable()
    }
    
    func getAnnouncement(withId id: Int) -> Observable<AnnouncementApiResponse> {
        return announcementsService.getAnnouncement(withId: id)
    }
    
    func getAnnouncements(atPage page: Int) -> Observable<AnnouncementsApiResponse> {
        return announcementsService.getAnnouncements(atPage: page).doOnNext { [unowned self] status in
            if case ApiResponse.success = status {
                self.unreadAnnouncementsSubject.onNext(false)
            }
        }
    }
    
    func getUnreadAnnouncementsInfo() -> Observable<AnnouncementsInfoApiResponse> {
        return announcementsService.getUnreadAnnouncementsInfo().doOnNext { [unowned self] status in
            if case ApiResponse.success(let response) = status {
                self.unreadAnnouncementsSubject.onNext(response.data.hasUnreadAnnouncements)
            }
        }
    }
    
    func setUnreadAnnouncementsSubject(_ value: Bool) {
        unreadAnnouncementsSubject.onNext(value)
    }
    
}
