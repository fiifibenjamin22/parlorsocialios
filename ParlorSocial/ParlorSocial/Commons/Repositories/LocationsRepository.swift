//
//  LocationsRepository.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 04/04/2019.
//

import Foundation
import RxSwift

protocol LocationsRepositoryProtocol {
    func getLocations(using model: Locations.Get.Request) -> LocationsResponseObs
}

final class LocationsRepository: LocationsRepositoryProtocol {
    static let shared: LocationsRepositoryProtocol = LocationsRepository()
    
    private init() {}
    
    private let apiService: LocationsApiService = ApiService.shared
}

extension LocationsRepository {
    
    @discardableResult
    func getLocations(using model: Locations.Get.Request) -> LocationsResponseObs {
        return apiService.getLocations(using: model)
    }
}
