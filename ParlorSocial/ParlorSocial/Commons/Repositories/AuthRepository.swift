//
//  AuthRepository.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 02/04/2019.
//

import Foundation
import RxSwift

protocol AuthRepositoryProtocol {
    func login(using request: LoginRequest) -> Observable<ApiResponse<TokenData>>
    func registerDevice(using request: RegisterDeviceRequest) -> MessageApiResponse
    func unregisterDevice(using request: UnregisterDeviceRequest) -> MessageApiResponse
    func changeEmail(using request: String) -> MessageApiResponse
}

final class AuthRepository {
    static let shared: AuthRepositoryProtocol = AuthRepository()
    
    private init() {}
    
    private let authService: AuthApiService = ApiService.shared
}

extension AuthRepository: AuthRepositoryProtocol {
    
    @discardableResult
    func login(using request: LoginRequest) -> Observable<ApiResponse<TokenData>> {
        return authService.login(using: request)
    }
    
    func registerDevice(using request: RegisterDeviceRequest) -> MessageApiResponse {
        return authService.registerDevice(using: request)
    }
    
    func unregisterDevice(using request: UnregisterDeviceRequest) -> MessageApiResponse {
        return authService.unregisterDevice(using: request)
    }
    
    func changeEmail(using request: String) -> MessageApiResponse {
        return authService.changeEmail(using: request)
    }
}
