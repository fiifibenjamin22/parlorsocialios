//
//  GlobalSettings.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 13/06/2019.
//

struct GlobalSettings: Codable {
    let contactEmailAddress: String?
    let contactEmailSubject: String?
    let helpEmailAddress: String?
    let helpEmailSubject: String?
    let showWelcomeVideo: Bool?
    let enablePaymentInApplication: Bool
    let suggestEventsOrVenuesEmailAddress: String?
    let suggestEventsOrVenuesEmailSubject: String?
    let referralLinkIos: String?
    let applePayBusinessMerchantName: String?
}

struct EmailSettings: Codable {
    let address: String?
    let subject: String?
}
