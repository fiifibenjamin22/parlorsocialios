//
//  PlanData.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 23/07/2019.
//

import Foundation

struct PlanData: Codable {
    let id: Int
    let name: String
    let currency: Currency?
    let price: Double?
    let proratedPrice: Double?
    let proratedPriceText: String?
    let proratedDiscount: Double?
    let interval: String?
    let priceInformation: String?
    let isPremium: Bool?
    let description: [PlanDataDescription]?
}

extension PlanData {
    var membershipPlanAnalyticContentType: String {
        return isPremium == true ? MembershipPlanAnalyticContentType.premiumMembership.rawValue : MembershipPlanAnalyticContentType.standardMembership.rawValue
    }
}
