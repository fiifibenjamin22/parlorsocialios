//
//  PlanDataDescription.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 18/11/2019.
//  Copyright © 2019 KISSdigital. All rights reserved.
//

import Foundation

struct PlanDataDescription: Codable {
    let element: String
}
