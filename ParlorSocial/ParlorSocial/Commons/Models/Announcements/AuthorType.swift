//
//  AuthorType.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 03/07/2019.
//

import Foundation

enum AuthorType: String, Codable, Equatable {
    case parlor = "parlor"
    case interestGroup = "interest_groups"
    case circle = "circles"
}
