//
//  AnnouncementsInfo.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 12/07/2019.
//

import Foundation

struct AnnouncementsInfo: Codable, Equatable {
    let hasUnreadAnnouncements: Bool
}
