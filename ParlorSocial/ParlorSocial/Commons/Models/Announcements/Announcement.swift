//
//  Announcement.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 03/07/2019.
//

import Foundation

struct Announcement: Codable, Equatable {
    static let parlorInitials = "P"
    
    let id: Int
    let author: String
    let authorId: Int?
    let title: String
    let message: String
    let authorType: AuthorType
    let colors: AnnouncementColors
    let imageUrls: AnnouncementImageUrls
    let publishedAt: Date
}

extension Announcement {
    var topInfoCardText: String {
        let base = Strings.Announcements.parOf.localized
        switch authorType {
        case .interestGroup:
            return String(format: base, self.author, Strings.Announcements.interest.localized).uppercased()
        case .circle:
            return String(format: base, self.author, Strings.Announcements.circle.localized).uppercased()
        default:
            return ""
        }
    }
    
    var initials: String {
        switch authorType {
        case .parlor:
            return Announcement.parlorInitials
        default:
            return author.split(separator: " ").reduce("", { acc, curr in
                "\(acc)\(curr.first ?? Character(""))"
            }).uppercased()
        }
    }
    
    var timeAgoText: String {
        publishedAt.toAnnouncementsFormat()
    }
    
}
