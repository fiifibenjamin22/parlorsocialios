//
//  AnnouncementColors.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 04/07/2019.
//

import UIKit

struct AnnouncementColors: Codable, Equatable {
    
    let title: String
    let message: String
    let background: String
    
    var titleColor: UIColor {
        return UIColor(hexString: title)
    }
    
    var messageColor: UIColor {
        return UIColor(hexString: message)
    }
    
    var backgroundColor: UIColor {
        return UIColor(hexString: background)
    }
}
