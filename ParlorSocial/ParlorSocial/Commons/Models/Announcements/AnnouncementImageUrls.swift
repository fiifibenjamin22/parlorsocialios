//
//  AnnouncementImageUrls.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 05/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

struct AnnouncementImageUrls: Codable, Equatable {
    enum CodingKeys: String, CodingKey {
        case imageUrl8x3 = "imageUrl8X3"
        case imageUrl11x10 = "imageUrl11X10"
        case imageUrls2x3 = "imageUrls2X3"
    }

    let imageUrl8x3: URL?
    let imageUrl11x10: URL?
    let imageUrls2x3: [URL]

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        imageUrl8x3 = try container.decodeIfPresent(String.self, forKey: .imageUrl8x3)?.url
        imageUrl11x10 = try container.decodeIfPresent(String.self, forKey: .imageUrl11x10)?.url
        guard let imageUrls2x3Strings = try? container.decode([String].self, forKey: .imageUrls2x3) else {
            self.imageUrls2x3 = []
            return
        }

        imageUrls2x3 = imageUrls2x3Strings.compactMap { $0.url }
    }
}
