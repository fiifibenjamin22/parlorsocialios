//
//  ReferFriendData.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 09/07/2019.
//

import Foundation

enum ReferFriendStringField {
    case firstName
    case lastName
    case email
}

struct ReferFriendData: Codable {
    let firstName: String
    let lastName: String
    let email: String
    
    static func empty() -> ReferFriendData {
        return ReferFriendData(
            firstName: "",
            lastName: "",
            email: ""
        )
    }
}

extension ReferFriendData {
    
    func withStringField(ofType type: ReferFriendStringField, value: String) -> ReferFriendData {
        return ReferFriendData(
            firstName: (type == .firstName) ? value : self.firstName,
            lastName: (type == .lastName) ? value : self.lastName,
            email: (type == .email) ? value : self.email
        )
    }
    
}
