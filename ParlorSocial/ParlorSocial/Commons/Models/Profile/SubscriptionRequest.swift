//
//  SubscriptionRequest.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 24/07/2019.
//

import Foundation

struct SubscriptionRequest: Codable {
    let cardId: Int
    let planId: Int
}
