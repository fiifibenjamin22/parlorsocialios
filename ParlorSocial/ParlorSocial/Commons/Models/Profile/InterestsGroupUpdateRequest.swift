//
//  InterestsGroupUpdateRequest.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 10/05/2019.
//

import Foundation

struct InterestsGroupUpdateRequest: Codable {
    let ids: [Int: Bool]
}
