//  CachedProfile.swift
//  ParlorSocialClub
//
//  Created on 21/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

struct CachedProfile {
    let value: Profile?
    private let timestamp: Date?
    private let maxCacheStoreSeconds: TimeInterval = 5
    
    init(value: Profile?, timestamp: Date? = nil) {
        self.value = value
        self.timestamp = timestamp
    }
    
    func isUpdateDataRequired() -> Bool {
        guard let date = timestamp else { return true }
        let currentTime = Date()
        return date.addingTimeInterval(maxCacheStoreSeconds) < currentTime
    }
}
