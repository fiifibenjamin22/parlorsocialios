//
//  MembershipExpiryReminderType.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 03/03/2020.
//

import Foundation

enum MembershipExpiryReminderType: String, Codable {
    case complimentary
    case preview
}
