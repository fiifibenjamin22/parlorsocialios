//
//  ApplicationStatus.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 15/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

enum ApplicationStatus: String, Codable {
    case forWaitlist = "for_waitlist"
    case rejected
    case withdrawn
    case confirmFuture = "confirm_future"

    var notificationAllowedTitle: String {
        switch self {
        case .forWaitlist, .rejected, .withdrawn, .confirmFuture:
            return Strings.UserNotApproved.applicationStatusNotificationsAllowedTitle.localized
        }
    }

    var notificationsNotAllowedTitle: String {
        switch self {
        case .forWaitlist, .rejected, .withdrawn, .confirmFuture:
            return Strings.UserNotApproved.applicationStatusNotificationsNotAllowedTitle.localized
        }
    }
}
