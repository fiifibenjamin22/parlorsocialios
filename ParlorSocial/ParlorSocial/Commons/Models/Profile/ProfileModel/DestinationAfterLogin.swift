//
//  DestinationAfterLogin.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 21/01/2020.
//

import Foundation

enum DestinationAfterLogin: String, Codable {
    case selectMembership = "select_membership"
    case payment
    case dashboard
    case userNotApproved = "user_not_approved"
}
