//
//  Profile.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 08/05/2019.
//

import Foundation

struct Profile: Codable, Equatable {
    let id: Int
    let email: String
    let firstName: String
    let lastName: String
    let type: ProfileType?
    let birthday: String?
    let education: String?
    let university: String?
    let company: String?
    let position: String?
    let homeAddressStreet: String?
    let homeAddressZip: String?
    let homeAddressCity: String?
    let workAddressZip: String?
    let gender: String?
    let maxIntroRequest: Int?
    let membershipRenewalDate: Date?
    let imageUrl: String?
    let destinationAfterLogin: DestinationAfterLogin?
    let membershipExpiryReminder: MembershipExpiryReminder?
    let applicationStatus: ApplicationStatus?

    var imageURL: URL? {
        return URL(string: imageUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))
    }
    
    var birthDate: Date? {
        guard let birthday = birthday else { return nil }
        return DateFormatter().apply { $0.dateFormat = Date.Format.dateOfBirth }.date(from: birthday)
    }
    
    var fullName: String {
        return "\(firstName) \(lastName)"
    }
    
    var positionAndCompany: String? {
        switch (position, company) {
        case (_, _) where position.isEmptyOrNil && !company.isEmptyOrNil:
            return company
        case (_, _) where company.isEmptyOrNil && !position.isEmptyOrNil:
            return position
        case (_, _) where !position.isEmptyOrNil && !company.isEmptyOrNil:
            return "\(position.emptyIfNil), \(company.emptyIfNil)"
        default:
            return nil
        }
    }
    
    var isPremium: Bool { return type == .premium }
    var isStandard: Bool { return type == .standard }
    var isPreview: Bool { return type == .preview }
    
}
