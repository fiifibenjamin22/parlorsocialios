//
//  MembershipExpiryReminder.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 03/03/2020.
//

import Foundation

struct MembershipExpiryReminder: Codable, Equatable {
    let type: MembershipExpiryReminderType?
    let expiryDate: Date?
}
