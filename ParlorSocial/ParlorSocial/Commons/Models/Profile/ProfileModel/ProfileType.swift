//
//  ProfileType.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 14/11/2019.
//

import Foundation

enum ProfileType: String, Codable {
    case premium
    case standard
    case preview
    case notApproved = "not_approved"
}
