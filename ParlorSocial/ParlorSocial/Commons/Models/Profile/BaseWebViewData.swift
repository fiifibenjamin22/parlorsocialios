//
//  BaseWebViewData.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 02/09/2019.
//

import Foundation

struct BaseWebViewData {
    let navBarTitle: String?
    let url: URL
}
