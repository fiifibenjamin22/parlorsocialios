//
//  CardDataDetails.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 10/07/2019.
//

struct CardDataDetails: Decodable {
    let id: Int
    let cardBrand: CardBrand
    let cardLastFour: String
    let isDefault: Bool?
    let name: String?
    let expMonth: Int?
    let expYear: Int?
    let addressLine1: String?
    let addressLine2: String?
    let country: String?
    let addressState: String?
    let addressZip: String?
    let addressCity: String?
    
    static func empty() -> CardDataDetails {
        return CardDataDetails(
            id: -1,
            cardBrand: .unknown,
            cardLastFour: "",
            isDefault: nil,
            name: "",
            expMonth: nil,
            expYear: nil,
            addressLine1: "",
            addressLine2: "",
            country: "",
            addressState: "",
            addressZip: "",
            addressCity: ""
        )
    }
}
