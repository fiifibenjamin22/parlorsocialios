//
//  ChangeEmailRequest.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 29/08/2019.
//

import Foundation

struct ChangeEmailRequest: Codable {
    let email: String
}
