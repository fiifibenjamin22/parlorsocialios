//
//  CardUploadModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 24/06/2019.
//

import Foundation

enum CardUploadStringField {
    case number
    case cvc
    case name
    case address1
    case address2
    case state
    case city
    case zip
}

enum CardUploadIntField {
    case expYear
    case expMonth
}

struct CardUploadData: Codable {
    let number: String
    let expMonth: Int?
    let expYear: Int?
    let cvc: String
    let name: String
    let addressLine1: String
    let addressLine2: String
    let addressState: String
    let addressZip: String
    let addressCity: String
    
    static func empty() -> CardUploadData {
        return CardUploadData(
            number: "",
            expMonth: nil,
            expYear: nil,
            cvc: "",
            name: "",
            addressLine1: "",
            addressLine2: "",
            addressState: "",
            addressZip: "",
            addressCity: ""
        )
    }
}

extension CardUploadData {
    
    func withStringField(ofType type: CardUploadStringField, value: String) -> CardUploadData {
        return CardUploadData(
            number: (type == .number) ? value : self.number,
            expMonth: self.expMonth,
            expYear: self.expYear,
            cvc: (type == .cvc) ? value : self.cvc,
            name: (type == .name) ? value : self.name,
            addressLine1: (type == .address1) ? value : self.addressLine1,
            addressLine2: (type == .address2) ? value : self.addressLine2,
            addressState: (type == .state) ? value : self.addressState,
            addressZip: (type == .zip) ? value : self.addressZip,
            addressCity: (type == .city) ? value : self.addressCity
        )
    }
    
    func withIntField(ofType type: CardUploadIntField, value: Int?) -> CardUploadData {
        return CardUploadData(
            number: self.number,
            expMonth: (type == .expMonth) ? value : self.expMonth,
            expYear: (type == .expYear) ? value : self.expYear,
            cvc: self.cvc,
            name: self.name,
            addressLine1: self.addressLine1,
            addressLine2: self.addressLine2,
            addressState: self.addressState,
            addressZip: self.addressZip,
            addressCity: self.addressCity
        )
    }
    
}
