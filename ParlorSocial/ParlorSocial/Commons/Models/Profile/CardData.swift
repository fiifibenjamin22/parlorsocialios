//
//  CardData.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 30/04/2019.
//

import Foundation
import UIKit

struct CardData: Decodable {
    let id: Int
    let cardBrand: CardBrand
    let cardLastFour: String
    let isDefault: Bool
    
    var userVisibleText: String {
        return cardLastFour
    }
    
}

extension CardData {
    
    func withDefault(_ isDefault: Bool) -> CardData {
        return CardData(id: self.id, cardBrand: self.cardBrand, cardLastFour: self.cardLastFour, isDefault: isDefault)
    }
    
}

extension CardData: PaymentMethodOption {
    
    var icon: UIImage? {
        return cardBrand.image
    }
    
    var title: String {
        return userVisibleText
    }
    
    var type: PaymentMethodType {
        return .card
    }
    
}

enum CardBrand: String, EnumDecodable {
    
    static let defaultDecoderValue: CardBrand = .unknown

    case masterCard = "MasterCard"
    case visa = "Visa"
    case americanExpress = "American Express"
    case dinersClub = "Diners Club"
    case discover = "Discover"
    case jcb = "JCB"
    case unionPay = "UnionPay"
    case unknown = "Unknown"
}

extension CardBrand {
    
    var image: UIImage? {
        switch self {
        case .americanExpress:
            return Icons.Cards.amex
        case .dinersClub:
            return Icons.Cards.diners
        case .discover:
            return Icons.Cards.discover
        case .jcb:
            return Icons.Cards.jcb
        case .masterCard:
            return Icons.Cards.masterCard
        case .unionPay:
            return Icons.Cards.union
        case .visa:
            return Icons.Cards.visa
        case .unknown:
            return nil
        }
    }
    
}
