//
//  CreateNewPasswordRequest.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 05/04/2019.
//

import Foundation

struct CreateNewPasswordRequest: Codable {
    let email: String
}
