//
//  GenderOption.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 01/08/2019.
//

import UIKit

enum Gender: String {
    case male
    case female
    case other
    
    var displayedText: String {
        return self.rawValue.capitalized
    }
}

extension Gender: DropDownOption {
    
    var id: Int {
        return self.hashValue
    }
    
    var icon: UIImage? {
        return nil
    }
    
    var title: String {
        return displayedText
    }
    
}
