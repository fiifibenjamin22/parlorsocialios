//
//  ProfileUpdateModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 21/05/2019.
//

import Foundation

struct ProfileUpdateModel: Codable {
    
    var email: String
    var firstName: String
    var lastName: String
    var birthday: String?
    var education: String?
    var university: String?
    var company: String?
    var position: String?
    var homeAddressStreet: String?
    var homeAddressZip: String?
    var homeAddressCity: String?
    var workAddressZip: String?
    var gender: String?
    
    init(profile: Profile) {
        self.email = profile.email
        self.firstName = profile.firstName
        self.lastName = profile.lastName
        self.birthday = profile.birthday
        self.education = profile.education
        self.university = profile.university
        self.company = profile.company
        self.position = profile.position
        self.homeAddressStreet = profile.homeAddressStreet
        self.homeAddressZip = profile.homeAddressZip
        self.homeAddressCity = profile.homeAddressCity
        self.workAddressZip = profile.workAddressZip
        self.gender = profile.gender
    }
}
