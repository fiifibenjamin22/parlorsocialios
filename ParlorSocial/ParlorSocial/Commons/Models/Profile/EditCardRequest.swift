//
//  EditCardRequest.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 10/07/2019.
//

import Foundation

struct EditCardRequest: Codable {
    let name: String
    let addressLine1: String
    let addressLine2: String
    let addressState: String
    let addressZip: String
    let addressCity: String
}

extension EditCardRequest {
    static func from(cardUploadData data: CardUploadData) -> EditCardRequest {
        return EditCardRequest(
            name: data.name,
            addressLine1: data.addressLine1,
            addressLine2: data.addressLine2,
            addressState: data.addressState,
            addressZip: data.addressZip,
            addressCity: data.addressCity
        )
    }
}
