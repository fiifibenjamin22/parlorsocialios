//
//  MambershipPlan.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 15/11/2019.
//  Copyright © 2019 KISSdigital. All rights reserved.
//

import Foundation

enum MembershipPlan {
    case standard
    case premium
}
