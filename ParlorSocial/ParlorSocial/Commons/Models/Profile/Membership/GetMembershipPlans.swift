//
//  GetMembershipPlans.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 18/11/2019.
//  Copyright © 2019 KISSdigital. All rights reserved.
//

import Foundation

enum MembershipPlans {
    enum Get {
        struct Request: Codable {
            let visible: Bool
        }
    }
}
