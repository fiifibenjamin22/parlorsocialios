//
//  ImageUploadModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 21/05/2019.
//

import Foundation
import UIKit

struct ImageUploadModel: Codable {
    
    let imageFile: String
    
    init(image: UIImage) {
        self.imageFile = image.base64Encoded
    }
    
}
