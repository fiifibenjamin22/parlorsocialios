//  CardTokenRequest.swift
//  ParlorSocialClub
//
//  Created on 17/04/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

struct CardTokenRequest: Codable {
    let tokenId: String
}
