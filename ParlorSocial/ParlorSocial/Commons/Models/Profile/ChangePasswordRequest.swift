//
//  ChangePasswordRequest.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 18/06/2019.
//

import Foundation

struct ChangePasswordRequest: Codable {
    let password: String
    let oldPassword: String
}
