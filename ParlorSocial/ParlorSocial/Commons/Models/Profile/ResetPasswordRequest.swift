//
//  ResetPasswordRequest.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 09/04/2019.
//

import Foundation

struct ResetPasswordRequest: Codable {
    let email: String
    let token: String
    let password: String
    let passwordConfirmation: String
}
