//
//  Member.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 23/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

struct Member: Codable, Equatable {
    let id: Int
    let imageUrl: URL
    let name: String
    let attendingActivation: HomeActivation
    let about: String
    let position: String
    let company: String
    let education: String
    let interestGroups: String
    let connectionStatus: ConnectionStatus
    let socialLinks: [SocialLink]
}

extension Member {
    static let mock = Member(
        id: 1,
        imageUrl: URL(string: "https://images.pexels.com/photos/733872/pexels-photo-733872.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")!,
        name: "Sarah Kim",
        attendingActivation: .mock,
        about: "I am a Producer and Director at The Solar Film in NYC. I'm a native New Yorker, and I love exploring the city and discovering new sights, flavors, sounds and people. Outside of film, some of my other interests are vegan cooking, vintage records and skateboarding.",
        position: "Producer and Director",
        company: "The Solar Film",
        education: "MBA of New York University",
        interestGroups: "Wine, Real Estate, Fashion, Tech",
        connectionStatus: .connected,
        socialLinks: [
            SocialLink(title: "LinkedIn", url: "https://www.linkedin.com/sarakim"),
            SocialLink(title: "Instagram", url: "https://www.instagram.com/sarakim")
        ]
    )
}
