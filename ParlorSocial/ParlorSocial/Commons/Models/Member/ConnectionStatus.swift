//
//  ConnectionStatus.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 23/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

enum ConnectionStatus: String, Codable {
    case connected
    case rejected
    case requested
    case disconnected

    var title: String {
        switch self {
        case .connected: return Strings.MemberProfile.connected.localized
        case .rejected: return Strings.MemberProfile.notAccepted.localized
        case .requested: return Strings.MemberProfile.requested.localized
        case .disconnected: return Strings.MemberProfile.connect.localized
        }
    }

    var defaultStyle: ParlorButtonStyle {
        switch self {
        case .connected:
            return .outlined(
                titleColor: .appTextBright,
                borderColor: .appGreyLightest,
                font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium),
                letterSpacing: Constants.LetterSpacing.none
            )
        case .rejected:
            return .filledWith(
                color: .appTextBright,
                titleColor: .white,
                font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium),
                letterSpacing: Constants.LetterSpacing.none
            )
        case .requested:
            return .filledWith(
                color: .appGreyLightest,
                titleColor: .appGreyLight,
                font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium),
                letterSpacing: Constants.LetterSpacing.none
            )
        case .disconnected:
            return .filledWith(
                color: .appBlack,
                titleColor: .white,
                font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium),
                letterSpacing: Constants.LetterSpacing.none
            )
        }
    }

    var isDeclineButtonHidden: Bool {
        return self != .rejected
    }

    var isUserInteractionEnabled: Bool {
        switch self {
        case .connected, .requested: return false
        case .rejected, .disconnected: return true
        }
    }
}
