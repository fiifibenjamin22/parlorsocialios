//
//  SocialLink.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 23/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

struct SocialLink: Codable, Equatable {
    let title: String
    let url: String
}
