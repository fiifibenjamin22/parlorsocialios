//
//  InterestGroup.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 23/04/2019.
//

import Foundation

struct InterestGroup: Codable, Equatable, Hashable {
    
    let id: Int
    let name: String
    let isSelected: Bool?

    private let imageUrl: String?
    
    var imageURL: URL? {
        return URL(string: imageUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))
    }
    
    func withSelectedState(isSelected: Bool?) -> InterestGroup {
        return InterestGroup(id: self.id, name: self.name, isSelected: isSelected, imageUrl: imageUrl)
    }
    
}

extension InterestGroup {
    
    static func addMoreInterestData() -> InterestGroup {
        return InterestGroup(id: -1, name: Strings.Profile.addMoreInterests.localized, isSelected: false, imageUrl: nil)
    }
    
    var isAddMoreData: Bool {
        return id == -1
    }
    
}
