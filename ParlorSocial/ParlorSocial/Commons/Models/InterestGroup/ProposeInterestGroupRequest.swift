//
//  ProposeInterestGroup.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 30/05/2019.
//

import Foundation

struct ProposeInterestGroupRequest: Codable {
    let name: String
}
