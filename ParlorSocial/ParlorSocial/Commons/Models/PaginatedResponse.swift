//
//  InterestGroupsResposne.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 09/05/2019.
//

import Foundation

struct PaginatedResponse<Model: Decodable>: Decodable & PaginatableResponse {
    
    typealias ListElement = Model
    
    let data: [Model]
    let meta: ListMetadata
    
}
