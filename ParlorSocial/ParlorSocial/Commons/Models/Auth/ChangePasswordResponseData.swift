//
//  ChangePasswordResponse.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 19/06/2019.
//

import Foundation

struct ChangePasswordResponseData: Decodable {
    let message: String
    let data: TokenData
}
