//
//  LoginRequest.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 02/04/2019.
//

import Foundation

struct LoginRequest: Codable {
    let username: String?
    let password: String?
    let refreshToken: String?
    let clientSecret: String = LoginRequest.clientSecret
    let clientId: Int = 1
    let grantType: String
}

extension LoginRequest {

    static func usingPassword(username: String, password: String) -> LoginRequest {
        return LoginRequest(username: username, password: password, refreshToken: nil, grantType: "password")
    }

    static func usingRefreshToken(refreshToken: String) -> LoginRequest {
        return LoginRequest(username: nil, password: nil, refreshToken: refreshToken, grantType: "refresh_token")
    }
    
    static var clientSecret: String {
        guard let clientSecret = Bundle.main.infoDictionary!["CLIENT_SECRET"] as? String else {
            fatalError("CLIENT_SECRET have to be placed in plist file")
        }
        
        return clientSecret
    }

}
