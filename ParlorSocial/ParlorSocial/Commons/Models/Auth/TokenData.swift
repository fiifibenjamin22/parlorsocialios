//
//  TokenData.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 02/04/2019.
//

import Foundation

struct TokenData: Codable {
    let tokenType: String
    let expiresIn: Int
    let accessToken: String
    let refreshToken: String
}
