//
//  Venue.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 01/03/2019.
//

import Foundation

struct Location: Codable, Equatable {
    let id: Int?
    let name: String
    let address1: String
    let address2: String?
    let neighborhood: String
    let city: String
    let state: String
    let zip: String
    let phone: String
    let www: String?
    let lat: Double
    let lng: Double
    let activations: [Activation]?
    let imageUrls: LocationImageUrls?
}

extension Location {
    
    var detailedTwoLinesLocation: String {
        if let address2 = address2 {
            return "\(address1) \(address2),\n\(city), \(state) \(zip)"
        }
        return "\(address1),\n\(city), \(state) \(zip)"
    }
    
    var detailedOneLineLocation: String {
        if let address2 = address2 {
            return "\(address1) \(address2), \(city), \(state) \(zip)"
        }
        return "\(address1), \(city), \(state) \(zip)"
    }
    
    var mapLocation: String {
        return [address1, city, zip].joined(separator: ", ")
    }
    
}
