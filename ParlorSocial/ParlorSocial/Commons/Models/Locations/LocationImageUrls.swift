//  LocationImageUrls.swift
//  ParlorSocialClub
//
//  Created on 31/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

struct LocationImageUrls: Codable, Equatable {
    enum CodingKeys: String, CodingKey {
        case imageUrl16x9 = "imageUrl16X9"
    }

    let imageUrl16x9: URL?

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        imageUrl16x9 = try container.decodeIfPresent(String.self, forKey: .imageUrl16x9)?.url
    }
}
