//
//  GetLocations.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 04/04/2019.
//

import Foundation

enum Locations {
    
    enum Get {
        
        struct Request: Codable {
            let with: String = "activations.hosts.interestGroups"
            let activationType: String = "venue"
            
            private(set) var page: Int
            
            static var initial: Request {
                return Request(page: 1)
            }
            
            mutating func set(page: Int) {
                self.page = page
            }
            
            mutating func increasePage() {
                self.page += 1
            }
        }
        
        struct Response: Codable & PaginatableResponse {
            typealias ListElement = Location
            
            let data: [Location]
            let meta: ListMetadata
        }
        
    }
}
