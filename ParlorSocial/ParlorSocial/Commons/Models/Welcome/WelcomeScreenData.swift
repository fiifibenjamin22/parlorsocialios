//
//  WelcomeScreenData.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 24/04/2019.
//

import Foundation

struct WelcomeScreenData: Codable {
    let category: WelcomeCategory
    let text: String
    let imageUrl: String
}
