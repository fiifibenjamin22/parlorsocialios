//
//  WelcomeCategory.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 24/04/2019.
//

import Foundation

enum WelcomeCategory: String, Codable {
    case morning
    case afternoon
    case evening
}

extension WelcomeCategory {
    
    static var currentTimeCategory: WelcomeCategory {
        let hours = Calendar.current.component(.hour, from: Date())
        let minutes = Calendar.current.component(.minute, from: Date())
        switch hours * 60 + minutes {
        case (4 * 60 + 31)...(12 * 60):
            return .morning
        case (12 * 60 + 1)...(17 * 60 + 30):
            return .afternoon
        default:
            return .evening
        }
    }
}
