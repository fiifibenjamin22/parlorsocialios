//
//  MembershipPlanAnalyticContentType.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 03/03/2020.
//

import Foundation

enum MembershipPlanAnalyticContentType: String {
    case premiumMembership = "premium_membership"
    case standardMembership = "standard_membership"
    case selectMembershipScreen = "select_membership"
}
