//
//  ListMetadata.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 01/03/2019.
//

import Foundation

struct ListMetadata: Codable {
    let currentPage: Int
    let lastPage: Int
    let perPage: Int
    let total: Int
}

extension ListMetadata {
    
    static var initial: ListMetadata {
        return ListMetadata(currentPage: 1, lastPage: 2, perPage: 1, total: 1)
    }
    
    var isLastPage: Bool { return currentPage >= lastPage }
    
    var isFirstPage: Bool { return currentPage <= 1 }
}
