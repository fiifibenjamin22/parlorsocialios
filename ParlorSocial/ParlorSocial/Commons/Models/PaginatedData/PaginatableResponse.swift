//
//  PaginatableResponse.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 01/03/2019.
//

import Foundation

protocol PaginatableResponse {
    associatedtype ListElement

    var data: [ListElement] { get }
    var meta: ListMetadata { get }

}
