//  AppUpdateInfoData.swift
//  ParlorSocialClub
//
//  Created on 08/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

struct AppUpdateInfoData: Codable {
    let latestAppVersion: String
    let appUpdateStrategy: AppUpdateStrategy?
}

enum AppUpdateStrategy: String, Codable {
    case optional
    case required
}
