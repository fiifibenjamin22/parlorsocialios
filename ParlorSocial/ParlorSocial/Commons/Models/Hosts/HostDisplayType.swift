//
//  HostDisplayType.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 16/04/2019.
//

import Foundation

enum HostDisplayType: String, Codable {
    case categories
    case about
}
