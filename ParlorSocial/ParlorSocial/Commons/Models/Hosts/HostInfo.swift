//
//  HostInfo.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 16/04/2019.
//

import Foundation

struct HostInfo: Codable, Equatable {
    let about: String?
    let position: String?
    let company: String?
    let education: String?
    let interestGroups: [InterestGroup]?
    
    var formattedGroups: String? {
        guard let interestGroups = interestGroups else { return nil }
        return interestGroups.map { $0.name }.joined(separator: ", ")
    }
}
