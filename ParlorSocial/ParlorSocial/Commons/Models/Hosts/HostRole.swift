//
//  HostRole.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 16/04/2019.
//

import Foundation

enum HostRole: String, Codable {
    case parlorHost = "parlor_host"
    case notableHost = "notable_host"
    case guestHost = "guest_host"
    case memberHost = "member_host"
}

extension HostRole {
    
    var displayName: String {
        switch self {
        case .guestHost:
            return Strings.ActivationDetails.guestHost.localized
        case .memberHost:
            return Strings.ActivationDetails.memberHost.localized
        case .notableHost:
            return Strings.ActivationDetails.notableHost.localized
        case .parlorHost:
            return Strings.ActivationDetails.parlorHost.localized
        }
    }
    
    func shouldBeCombinedWith(other role: HostRole) -> Bool {
        if self == .parlorHost {
            return self == role
        } else {
            return self != .parlorHost && role != .parlorHost
        }
    }
    
    var sortIndex: Int {
        switch self {
        case .parlorHost:
            return 0
        case .notableHost:
            return 1
        case .guestHost:
            return 2
        case .memberHost:
            return 3
        }
    }
}
