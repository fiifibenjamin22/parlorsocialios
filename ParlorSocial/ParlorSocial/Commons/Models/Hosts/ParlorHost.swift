//
//  ParlorHost.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 16/04/2019.
//

import Foundation

/// *Person that is host of activation and has special functions on event.*
struct ParlorHost: Codable, Equatable {
    let id: Int
    let name: String
    let role: HostRole
    let displayType: HostDisplayType
    let info: HostInfo
    let imageUrl: URL?
    let imageUrlGreyscale: URL?
    
    var formattedHostWithRole: String {
        return "\(role.displayName): \(name)"
    }
}

extension Array where Element == ParlorHost {
    
    func withRole(_ role: HostRole) -> [ParlorHost] {
        return self.filter { $0.role == role }
    }
    
}
