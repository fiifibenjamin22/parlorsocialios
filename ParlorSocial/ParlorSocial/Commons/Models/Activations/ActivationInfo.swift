//
//  ActivationInfo.swift
//  ParlorSocialClub
//
//  Created on 15/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

struct ActivationInfo: Codable, Equatable {
    let id: Int
    let type: Activation.ActivationType
    let name: String
    let imageUrl: String
    let timeSegment: TimeSegment
    let guestListAccess: GuestListAccess?
    let links: [ActivationInfoLink]
}
