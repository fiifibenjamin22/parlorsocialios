//
//  ActivationUpdate.swift
//  ParlorSocialClub
//
//  Created on 18/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

struct ActivationUpdate: Codable, Equatable {
    let id: Int
    let content: String
    let createdAt: Date
}
