//
//  ActivationInfoLink.swift
//  ParlorSocialClub
//
//  Created on 18/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

struct ActivationInfoLink: Codable, Equatable {
    let id: Int
    let name: String
    let url: String
}
