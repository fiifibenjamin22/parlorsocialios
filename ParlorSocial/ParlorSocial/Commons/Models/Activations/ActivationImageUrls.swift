//
//  ActivationImageUrls.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 12/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

struct ActivationImageUrls: Codable, Equatable {
    enum CodingKeys: String, CodingKey {
        case imageUrl2x1 = "imageUrl2X1"
        case imageUrl4x3 = "imageUrl4X3"
        case imageUrl1x1 = "imageUrl1X1"
        case imageUrl3x4 = "imageUrl3X4"
        case imageUrl4x5 = "imageUrl4X5"
    }

    let imageUrl2x1: URL?
    let imageUrl4x3: URL?
    let imageUrl1x1: URL?
    let imageUrl3x4: URL?
    let imageUrl4x5: URL?

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        imageUrl2x1 = try container.decodeIfPresent(String.self, forKey: .imageUrl2x1)?.url
        imageUrl4x3 = try container.decodeIfPresent(String.self, forKey: .imageUrl4x3)?.url
        imageUrl1x1 = try container.decodeIfPresent(String.self, forKey: .imageUrl1x1)?.url
        imageUrl3x4 = try container.decodeIfPresent(String.self, forKey: .imageUrl3x4)?.url
        imageUrl4x5 = try container.decodeIfPresent(String.self, forKey: .imageUrl4x5)?.url
    }
}
