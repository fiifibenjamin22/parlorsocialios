//
//  GetActivationUpdates.swift
//  ParlorSocialClub
//
//  Created on 18/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

enum ActivationUpdates {
    enum Get {
        struct Request: Codable {
            private(set) var page: Int

            private enum CodingKeys: String, CodingKey {
                case page
            }

            static var initialAll: Request {
                return Request(
                    page: 1
                )
            }

            mutating func set(page: Int) {
                self.page = page
            }

            mutating func increasePage() {
                self.page += 1
            }
        }

        struct Response: Codable & PaginatableResponse {
            typealias ListElement = ActivationUpdate
            let data: [ActivationUpdate]
            let meta: ListMetadata
        }
    }
}
