//
//  TimeSegment.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 18/04/2019.
//

import UIKit
/// *Each activation has at least time segment. User doesn't have to book place on whole activation but only one segment when activation is more than once.*
struct TimeSegment: Codable, Equatable {
    let id: Int
    let startAt: Date
    let endAt: Date
    let length: Int
}

extension TimeSegment: DropDownOption {
    
    var icon: UIImage? { return nil }

    var title: String { return startAt.toString(withFormat: Date.Format.activationDetails) }

}
