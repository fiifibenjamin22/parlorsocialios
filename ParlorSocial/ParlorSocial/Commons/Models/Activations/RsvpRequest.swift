//
//  RsvpRequest.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 12/08/2019.
//

import Foundation

struct RsvpRequest: Codable {
    let id: Int?
    let timeSegmentId: Int
    let cardId: Int?
    let guests: [RsvpGuest]
    
    init(rsvp: Rsvp, cardId: Int?) {
        self.id = rsvp.id
        self.timeSegmentId = rsvp.timeSegmentId
        self.cardId = cardId
        self.guests = rsvp.guests
    }
    
    init(updatingPreviousRequest request: RsvpRequest, cardId: Int) {
        id = request.id
        timeSegmentId = request.timeSegmentId
        guests = request.guests
        self.cardId = cardId
    }
}
