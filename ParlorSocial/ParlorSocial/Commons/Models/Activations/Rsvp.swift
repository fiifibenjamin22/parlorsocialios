//
//  Rsvp.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 30/04/2019.
//

import Foundation

enum RsvpStatus: Int, Codable {
    case creating = -1
    case waitList = 0
    case guestList = 1
    case transferableToGuestList = 2
}

/// *Struct that describes booking on activation.*
struct Rsvp: Codable, Equatable {
    
    let id: Int
    let timeSegmentId: Int
    let guests: [RsvpGuest]
    let status: RsvpStatus
    /// Date when user checked in activation.
    let checkedInAt: Date?
    
    init(timeSegmentId: Int, guests: [RsvpGuest], id: Int? = nil, checkedInAt: Date? = nil) {
        self.init(id: id ?? -1, timeSegmentId: timeSegmentId, guests: guests, status: .creating, checkedInAt: checkedInAt)
    }

    init(id: Int, timeSegmentId: Int, guests: [RsvpGuest], status: RsvpStatus, checkedInAt: Date? = nil) {
        self.id = id
        self.timeSegmentId = timeSegmentId
        self.guests = guests
        self.status = status
        self.checkedInAt = checkedInAt
    }
    
    func segment(basedOn activation: Activation) -> TimeSegment? {
        return activation.segments.first(where: { $0.id == self.timeSegmentId })
    }
    
    func withStatus(_ status: RsvpStatus) -> Rsvp {
        return Rsvp(id: self.id, timeSegmentId: self.timeSegmentId, guests: self.guests, status: status, checkedInAt: self.checkedInAt)
    }
    
    func withCheckedInAt(_ date: Date?) -> Rsvp {
        return Rsvp(id: self.id, timeSegmentId: self.timeSegmentId, guests: self.guests, status: self.status, checkedInAt: date)
    }
    
    var isWaiting: Bool {
        switch status {
        case .waitList, .transferableToGuestList:
            return true
        default:
            return false
        }
    }
    
}
