//
//  RsvpGuest.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 16/04/2019.
//

import Foundation
/// Member of activation - other user of application or another guest who was invited by parlor member.
struct RsvpGuest: Codable, Equatable {
    
    let id: Int?
    let firstName: String
    let lastName: String
    let email: String?
    let isNominated: Bool?
    
    init(name: String, surname: String, email: String? = nil, id: Int? = nil, isNominated: Bool? = nil) {
        self.id = id
        self.firstName = name
        self.lastName = surname
        self.email = email
        self.isNominated = isNominated
    }
    
    static func empty() -> RsvpGuest {
        let guest = RsvpGuest(name: "", surname: "")
        return guest
    }
    
    func withName(_ name: String) -> RsvpGuest {
        return RsvpGuest(name: name, surname: self.lastName, email: self.email, id: self.id, isNominated: self.isNominated)
    }
    
    func withSurname(_ surname: String) -> RsvpGuest {
        return RsvpGuest(name: self.firstName, surname: surname, email: self.email, id: self.id, isNominated: self.isNominated)
    }
    
    func withEmail(_ email: String?) -> RsvpGuest {
        return RsvpGuest(name: self.firstName, surname: self.lastName, email: email, id: self.id, isNominated: self.isNominated)
    }
    
    func withNominated(_ isNominated: Bool) -> RsvpGuest {
        return RsvpGuest(name: self.firstName, surname: self.lastName, email: self.email, id: self.id, isNominated: isNominated)
    }
    
}
