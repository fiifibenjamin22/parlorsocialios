//
//  Currency.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 09/08/2019.
//

import Foundation

struct Currency: Codable, Equatable {
    let name: String?
    let code: String?
    let symbol: String?
}
