//
//  Activation.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 27.02.2019.
//

import UIKit

/// *An event in which user can take participation.*
struct Activation: Codable, Equatable {

    let id: Int
    let locationId: Int
    let type: ActivationType
    let name: String
    let description: String
    let summary: String?
    let ticketPrice: Double
    let ticketExternal: Bool
    let forYou: Bool
    /// *Is* `true` *when activation is available for members with premium subscription.*
    let isPremium: Bool
    let hosts: [ParlorHost]
    let segments: [TimeSegment]
    let rsvp: Rsvp?
    let membersGuestLimit: Int
    /// It is `true` when user can be transformed from wait list to guest list.
    let isAllowedToRsvpOngoing: Bool
    let disableIntroRequests: Bool?
    let isPurchasedAtTheDoor: Bool
    let actionButtonType: ActionButtonType?

    var startAt: Date
    let endAt: Date?
    let allowToRsvpAt: Date?
    let allowToRsvpUntil: Date?

    /// It is `true` when user should be informed that booking activation is available.
    let isNotifyMe: Bool?
    let isPayable: Bool?
    let currency: Currency?
    let location: Location?
    /// It is `true` when activation data is not exact.
    let isApproximatedStartTime: Bool
    let guestlistState: GuestListAvailabilityState?
    /// Use it to check if location should be checked before open guest list
    let checkInStrategy: CheckInStrategy?
    var shareData: ShareData?

    let imageUrls: ActivationImageUrls?
    
    var hostedBy: ParlorHost? {
        return hosts.withRole(.notableHost).first
    }
    
    var isFree: Bool {
        return self.ticketPrice == 0
    }
    
    var formattedPrice: String {
        guard let currency = currency else { return "" }
        return isFree ? Strings.ActivationDetails.free.localized : ticketPrice.getCurrencyString(currency: currency) ?? ""
    }

    private let ticketUrl: String?
    var ticketURL: URL? {
        guard let ticketUrl = ticketUrl else { return nil }
        return URL(string: ticketUrl)
    }

    var isRsvpd: Bool {
        return rsvp?.status == .guestList || rsvp?.status == .waitList || rsvp?.status == .transferableToGuestList
    }

    var notRsvpd: Bool {
        return rsvp == nil
    }
    
    var formattedJustMeText: String? {
        switch (rsvp?.guests.count) ?? 0 {
        case 0:
            return Strings.MyRsvps.formattedJustMe.localized
        case 1:
            return Strings.MyRsvps.formattedJustMePlusOne.localized
        default:
            return String(format: Strings.MyRsvps.formattedJustMePlusMore.localized, rsvp?.guests.count ?? 0)
        }
    }
    
    var isGuestListAvailable: Bool {
        return guestlistState == .available
    }
    
    var isRsvpOpen: Bool {
        let currentDate = Date()
        if let allowToRsvpUntilStrong = allowToRsvpUntil {
            return currentDate >= (allowToRsvpAt ?? Date()) && allowToRsvpUntilStrong > currentDate
        } else {
            return currentDate >= (allowToRsvpAt ?? Date())
        }
    }
    
    var isCheckedIn: Bool {
        return rsvp?.checkedInAt != nil
    }

    var rsvpdTimeSegment: TimeSegment? {
        return segments.first(where: { $0.id == rsvp?.timeSegmentId })
    }

    /// *Types of activation. User can filter activation by them.*
    enum ActivationType: String, Codable {
        case all = ""
        case mixer
        case happening
    }
    
    func withRsvp(_ rsvp: Rsvp?) -> Activation {
        return Activation(
            id: id, locationId: locationId, type: type, name: name, description: description,
            summary: summary, ticketPrice: ticketPrice, ticketExternal: ticketExternal,
            forYou: forYou, isPremium: isPremium, hosts: hosts, segments: segments,
            rsvp: rsvp, membersGuestLimit: membersGuestLimit, isAllowedToRsvpOngoing: isAllowedToRsvpOngoing,
            disableIntroRequests: disableIntroRequests, isPurchasedAtTheDoor: isPurchasedAtTheDoor, actionButtonType: actionButtonType,
            startAt: startAt, endAt: endAt, allowToRsvpAt: allowToRsvpAt, allowToRsvpUntil: allowToRsvpUntil,
            isNotifyMe: isNotifyMe, isPayable: isPayable, currency: currency, location: location,
            isApproximatedStartTime: isApproximatedStartTime, guestlistState: guestlistState,
            checkInStrategy: checkInStrategy, shareData: shareData, imageUrls: imageUrls, ticketUrl: ticketUrl)
    }
    
    func with(rsvp: Rsvp?, checkInStrategy: CheckInStrategy) -> Activation {
        return Activation(
            id: id, locationId: locationId, type: type, name: name, description: description,
            summary: summary, ticketPrice: ticketPrice, ticketExternal: ticketExternal,
            forYou: forYou, isPremium: isPremium, hosts: hosts, segments: segments,
            rsvp: rsvp, membersGuestLimit: membersGuestLimit, isAllowedToRsvpOngoing: isAllowedToRsvpOngoing,
            disableIntroRequests: disableIntroRequests, isPurchasedAtTheDoor: isPurchasedAtTheDoor, actionButtonType: actionButtonType,
            startAt: startAt, endAt: endAt, allowToRsvpAt: allowToRsvpAt, allowToRsvpUntil: allowToRsvpUntil,
            isNotifyMe: isNotifyMe, isPayable: isPayable, currency: currency, location: location,
            isApproximatedStartTime: isApproximatedStartTime, guestlistState: guestlistState,
            checkInStrategy: checkInStrategy, shareData: shareData, imageUrls: imageUrls, ticketUrl: ticketUrl)
    }
    
    func withIsNotifyMe(_ isNotifyMe: Bool?) -> Activation {
        return Activation(
            id: id, locationId: locationId, type: type, name: name, description: description,
            summary: summary, ticketPrice: ticketPrice, ticketExternal: ticketExternal,
            forYou: forYou, isPremium: isPremium, hosts: hosts, segments: segments,
            rsvp: rsvp, membersGuestLimit: membersGuestLimit, isAllowedToRsvpOngoing: isAllowedToRsvpOngoing,
            disableIntroRequests: disableIntroRequests, isPurchasedAtTheDoor: isPurchasedAtTheDoor, actionButtonType: actionButtonType,
            startAt: startAt, endAt: endAt, allowToRsvpAt: allowToRsvpAt, allowToRsvpUntil: allowToRsvpUntil,
            isNotifyMe: isNotifyMe, isPayable: isPayable, currency: currency, location: location,
            isApproximatedStartTime: isApproximatedStartTime, guestlistState: guestlistState,
            checkInStrategy: checkInStrategy, shareData: shareData, imageUrls: imageUrls, ticketUrl: ticketUrl)
    }

    mutating func setShareData(_ data: ShareData) {
        self.shareData = data
    }
    
    static func empty(withId id: Int) -> Activation {
        return Activation(
            id: id,
            locationId: 0,
            type: .all,
            name: "",
            description: "",
            summary: nil,
            ticketPrice: 0,
            ticketExternal: false,
            forYou: false,
            isPremium: false,
            hosts: [],
            segments: [],
            rsvp: nil,
            membersGuestLimit: 0,
            isAllowedToRsvpOngoing: false,
            disableIntroRequests: nil,
            isPurchasedAtTheDoor: false,
            actionButtonType: nil,
            startAt: Date(timeIntervalSince1970: 0),
            endAt: nil,
            allowToRsvpAt: nil,
            allowToRsvpUntil: nil,
            isNotifyMe: nil,
            isPayable: nil,
            currency: nil,
            location: nil,
            isApproximatedStartTime: false,
            guestlistState: nil,
            checkInStrategy: nil,
            shareData: nil,
            imageUrls: nil,
            ticketUrl: ""
        )
    }
}

extension Activation.ActivationType {

    var icon: UIImage? {
        switch self {
        case .all: return nil
        case .happening: return Icons.ActivationType.event
        case .mixer: return Icons.ActivationType.private
        }
    }
    
    var iconDark: UIImage? {
        switch self {
        case .all: return nil
        case .happening: return Icons.ActivationType.eventDark
        case .mixer: return Icons.ActivationType.privateDark
        }
    }
    
    var iconBlack: UIImage? {
        switch self {
        case .all: return nil
        case .happening: return Icons.ActivationType.eventBlack
        case .mixer: return Icons.ActivationType.privateBlack
        }
    }
}

extension Activation {
    
    var canSeeDetails: Bool {
        guard !isPremium || Config.isPremiumUser else { return false }
        
        return isAllowedToSeeFutureActivation
    }
    
    var isAllowedToSeeFutureActivation: Bool {
        return startAt.isFuture || isAllowedToRsvpOngoing
    }
    
    var canBeEdited: Bool {
        return ![RsvpStatus.transferableToGuestList, RsvpStatus.waitList].contains(self.rsvp?.status)
    }
}
