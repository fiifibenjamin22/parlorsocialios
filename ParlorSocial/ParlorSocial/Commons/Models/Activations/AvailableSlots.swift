//
//  AvailableSlots.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 13/11/2019.
//  Copyright © 2019 KISSdigital. All rights reserved.
//

import Foundation

struct AvailableSlots: Decodable {
    let availableSlots: Int
}
