//
//  GetActivations.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 18/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

enum Activations {
    enum Get {
        struct Request: Codable {
            let with: String = "hosts.interestGroups"

            private(set) var page: Int
            private(set) var type: Activation.ActivationType
            private(set) var forYou: Bool?
            private(set) var rsvped: Bool?
            private(set) var status: Status?
            private(set) var day: Date?
            private(set) var order: OrderType?
            private(set) var withoutApproximated: Bool?

            private enum CodingKeys: String, CodingKey {
                case page, type, forYou, rsvped, status, day, order, with
                case withoutApproximated = "without-approximated"
            }

            static func calendar(forDate date: Date, page: Int) -> Request {
                return Request(
                    page: page,
                    type: .all,
                    forYou: nil,
                    rsvped: nil,
                    status: .all,
                    day: date,
                    order: nil,
                    withoutApproximated: true
                )
            }

            static var initialAll: Request {
                return Request(
                    page: 1,
                    type: .all,
                    forYou: false,
                    rsvped: nil,
                    status: nil,
                    day: nil,
                    order: nil,
                    withoutApproximated: nil
                )
            }

            static var initialUpcoming: Request {
                return Request(
                    page: 1,
                    type: .all,
                    forYou: nil,
                    rsvped: true,
                    status: .upcomingWithOffset,
                    day: nil,
                    order: nil,
                    withoutApproximated: nil
                )
            }

            static var initialHistory: Request {
                return Request(
                    page: 1,
                    type: .all,
                    forYou: nil,
                    rsvped: true,
                    status: .finishedWithOffset,
                    day: nil,
                    order: .desc,
                    withoutApproximated: nil
                )
            }

            mutating func set(page: Int) {
                self.page = page
            }

            mutating func increasePage() {
                self.page += 1
            }

            mutating func set(type: Activation.ActivationType) {
                self.type = type
            }

            enum Status: String, Codable {
                case forthcoming
                case ongoing
                case finishedWithOffset = "finished_with_offset"
                case upcomingWithOffset = "upcoming_with_offset"
                case all
            }

            enum OrderType: String, Codable {
                case desc
                case asc
            }
        }

        struct Response: Codable & PaginatableResponse {
            typealias ListElement = Activation

            let data: [Activation]
            let meta: ListMetadata

            func withActivations(_ activations: [Activation]) -> Response {
                return Response(data: activations, meta: self.meta)
            }
        }
    }
}
