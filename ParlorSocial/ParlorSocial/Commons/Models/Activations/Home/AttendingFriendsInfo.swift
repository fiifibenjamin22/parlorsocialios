//
//  AttendingFriendsInfo.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 04/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

struct FriendInfo: Codable, Equatable {
    let id: Int
    let imageUrl: String
}

struct AttendingFriendsInfo: Codable, Equatable {
    let text: String?
    let friends: [FriendInfo]

    var imageUrls: [URL] {
        return friends.map { URL(string: $0.imageUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)) }.compactMap { $0 }
    }
}
