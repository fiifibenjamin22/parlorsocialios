//
//  RsvpCompletionStatus.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 10/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

enum RsvpCompletionStatus: String, Codable {
    case upcoming
    case finished
}
