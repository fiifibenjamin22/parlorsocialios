//
//  HomeRsvp.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 09/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

struct HomeRsvp: Codable, Equatable {
    let id: Int
    let name: String
    let location: HomeListLocation?
    let imageUrl: String
    var actionButtonType: ActionButtonType
    let guestListAccess: GuestListAccess
    let guestCount: Int
    let rsvpStatus: RsvpStatus
    let timeSegment: TimeSegment
    let status: RsvpCompletionStatus

    var imageURL: URL? {
        return URL(string: imageUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))
    }

    var formattedGuestCountText: String? {
        guard let userFullName = Config.userProfile?.fullName else { return nil }
        switch guestCount {
        case 0:
            return String(format: Strings.MyRsvps.formattedGuestCountUserOnly.localized, userFullName)
        case 1:
            return String(format: Strings.MyRsvps.formattedGuestCountOne.localized, userFullName)
        default:
            return String(format: Strings.MyRsvps.formattedGuestCount.localized, userFullName, guestCount)
        }
    }

    var isGuestListAvailable: Bool {
        return guestListAccess.guestlistState == .available
    }

    var isWaiting: Bool {
        switch rsvpStatus {
        case .waitList, .transferableToGuestList:
            return true
        default:
            return false
        }
    }
}
