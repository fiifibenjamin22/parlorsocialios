//
//  HomeListHost.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 04/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

struct HomeListHost: Codable, Equatable {
    let id: Int
    let fullName: String
}
