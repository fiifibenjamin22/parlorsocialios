//
//  HomeListLocation.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 04/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

struct HomeListLocation: Codable, Equatable {
    let name: String
    let neighborhood: String
}
