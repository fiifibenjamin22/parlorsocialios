//
//  HomeActivation.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 15/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

struct HomeActivation: Codable, Equatable {
    let id: Int
    let summary: String?
    let isPremium: Bool
    let name: String
    let hostedBy: [HomeListHost]
    let actionButtonType: ActionButtonType
    let isAllowedToRsvpOngoing: Bool
    let startAt: Date
    let endAt: Date?
    let attendingFriendsInfo: AttendingFriendsInfo
    let location: HomeListLocation?
    let isApproximatedStartTime: Bool
    let guestListAccess: GuestListAccess

    private let imageUrl: String

    var imageURL: URL? {
        return URL(string: imageUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))
    }

    var mainHost: HomeListHost? {
        return hostedBy.first
    }

    func with(_ activation: Activation?) -> HomeActivation {
        var actionButtonType = self.actionButtonType
        switch actionButtonType {
        case .live, .updates:
            if activation?.isRsvpd == true {
                actionButtonType = activation?.isGuestListAvailable == true ? .guestList : .rsvpd
            }
        case .rsvp, .rsvpd, .guestList:
            if activation?.isRsvpd == true {
                actionButtonType = activation?.isGuestListAvailable == true ? .guestList : .rsvpd
            } else if activation?.notRsvpd == true {
                actionButtonType = .rsvp
            }
        }

        return HomeActivation(
            id: id,
            summary: summary,
            isPremium: isPremium,
            name: name,
            hostedBy: hostedBy,
            actionButtonType: actionButtonType,
            isAllowedToRsvpOngoing: isAllowedToRsvpOngoing,
            startAt: startAt,
            endAt: endAt,
            attendingFriendsInfo: attendingFriendsInfo,
            location: location,
            isApproximatedStartTime: isApproximatedStartTime,
            guestListAccess: guestListAccess,
            imageUrl: imageUrl
        )
    }
}

extension HomeActivation {
    var canSeeDetails: Bool {
        guard !isPremium || Config.isPremiumUser else { return false }

        return isAllowedToSeeFutureActivation
    }

    var isAllowedToSeeFutureActivation: Bool {
        return startAt.isFuture || isAllowedToRsvpOngoing
    }
}

extension HomeActivation {
    static let mock = HomeActivation(
        id: 1,
        summary: "Test",
        isPremium: false,
        name: "Test",
        hostedBy: [],
        actionButtonType: .guestList,
        isAllowedToRsvpOngoing: true,
        startAt: Date(),
        endAt: Date(),
        attendingFriendsInfo: AttendingFriendsInfo(text: nil, friends: []),
        location: nil,
        isApproximatedStartTime: false,
        guestListAccess: GuestListAccess(guestlistState: .available, checkInStrategy: .notRequired),
        imageUrl: "https://static-parlor-dev.s3.eu-central-1.amazonaws.com/uploads/screenshot-2020-07-20-at-155920.jpeg"
    )
}
