//
//  GuestListAccess.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 09/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

struct GuestListAccess: Codable, Equatable {
    let guestlistState: GuestListAvailabilityState
    let checkInStrategy: CheckInStrategy
}
