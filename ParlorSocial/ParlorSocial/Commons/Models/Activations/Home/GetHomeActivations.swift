//
//  GetHomeActivations.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 27.02.2019.
//

import Foundation

enum HomeActivations {
    enum Get {
        struct Request: Codable {
            let with: String = "hosts.interestGroups"

            private(set) var page: Int
            private(set) var type: Activation.ActivationType
            
            private enum CodingKeys: String, CodingKey {
                case page, type
            }

            static var initialAll: Request {
                return Request(
                    page: 1,
                    type: .all
                )
            }

            static var initialMixers: Request {
                return Request(
                    page: 1,
                    type: .mixer
                )
            }

            static var initialHappenings: Request {
                return Request(
                    page: 1,
                    type: .happening
                )
            }
            
            mutating func set(page: Int) {
                self.page = page
            }
            
            mutating func increasePage() {
                self.page += 1
            }
            
            mutating func set(type: Activation.ActivationType) {
                self.type = type
            }
        }

        struct Response: Codable & PaginatableResponse {
            typealias ListElement = HomeActivation

            let data: [HomeActivation]
            let meta: ListMetadata
            
            func withActivations(_ activations: [HomeActivation]) -> Response {
                return Response(data: activations, meta: self.meta)
            }
        }
    }
}
