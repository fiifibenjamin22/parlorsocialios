//
//  RsvpTransferRequest.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 20/08/2019.
//

import Foundation

struct RsvpTransferRequest: Codable {
    let cardId: Int?
}
