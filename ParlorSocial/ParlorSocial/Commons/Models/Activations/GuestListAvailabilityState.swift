//
//  GuestListAvailabilityState.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 21/01/2020.
//

enum GuestListAvailabilityState: String, Codable {
    case notAvailable = "not_available"
    case available
    case comingSoon = "coming_soon"
}
