//
//  ShareData.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 02/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

struct ShareData: Codable, Equatable {
    let url: String
    let message: String
    let emailTitle: String

    var content: String { return "\(message) \(url)" }
}
