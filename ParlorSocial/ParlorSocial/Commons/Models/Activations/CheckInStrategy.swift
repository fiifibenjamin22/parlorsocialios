//
//  CheckInState.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 22/01/2020.
//

enum CheckInStrategy: String, Codable {
    case required
    case optional
    case notRequired = "not_required"
}
