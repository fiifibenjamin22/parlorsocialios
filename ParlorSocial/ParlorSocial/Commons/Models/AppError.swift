//
//  AppError.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 26/02/2019.
//

import Foundation
import Alamofire

struct AppError: Error, Decodable {
    
    var title: String?
    var message: String
    var errors: [String: [String]]?
    var status: Int?
    var code: Int?
    var errorType: AppErrorType?
    var analyticErrorSourceName: AnalyticEventAlertSourceName?
    
    var localizedDescription: String {
        return message
    }
    
    var isOfflineError: Bool {
        return code == URLError.Code.notConnectedToInternet.rawValue
    }
    
    var isUnauthorizedError: Bool {
        return code == Config.unauthorizedCode
    }
    
    var isAlreadyCheckedInError: Bool {
        return code == Config.conflictCode
    }
    
    var isPaymentRequiredError: Bool {
        return code == Config.paymentRequiredCode
    }
    
    var shouldShowAlertPopup: Bool {
        return !isPaymentRequiredError
    }
    
    // MARK: - Initialization
    
    init(title: String?, message: String, errors: [String: [String]]?, status: Int?,
         code: Int?, errorType: AppErrorType?, analyticErrorSourceName: AnalyticEventAlertSourceName?) {
        self.title = title
        self.message = message
        self.errors = errors
        self.status = status
        self.code = code
        self.errorType = errorType
        self.analyticErrorSourceName = analyticErrorSourceName
    }
    
    init(with message: String?, and title: String? = nil) {
        self.message = message ?? Strings.unknownError.localized
        self.title = title
    }
    
    init() {
        self.message = Strings.unknownError.localized
    }
    
    init(with error: Error?, errorSource: AnalyticEventAlertSourceName? = nil) {
        if let alamofireError = error as? AFError {
            self.code = alamofireError.responseCode
        } else {
            self.code = (error as NSError?)?.code
        }
        
        if code == URLError.Code.notConnectedToInternet.rawValue {
            self.message = Strings.noInternetConnectionError.localized
        } else {
            self.message = error?.localizedDescription ?? Strings.unknownError.localized
        }
        self.errorType = getErrorType()
        analyticErrorSourceName = errorSource
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.message = try container.decode(String.self, forKey: .message)
        self.errors = try container.decodeIfPresent([String: [String]].self, forKey: .errors)
    }
    
    init(from data: Data, code: Int, errorSource: AnalyticEventAlertSourceName) {
        if let defaultError = try? AppError.make(from: data) {
            self = defaultError
            self.code = code
            self.analyticErrorSourceName = errorSource
            return
        }
        message = Strings.unknownError.localized
        analyticErrorSourceName = errorSource
    }
    
    init(from validatedOutputs: [ValidatedOutput]) {
        let errorStrings = validatedOutputs.flatMap { validatedOutput in
            validatedOutput.errorMessages.map { errorMessage -> String in
                if validatedOutput.name.isEmpty {
                    return errorMessage
                } else {
                    return "\(validatedOutput.name) \(errorMessage)"
                }
            }
        }
        self.errors = ["": errorStrings]
        self.message = ""
        self.analyticErrorSourceName = .validationError
    }
    
    func setErrorType(errorType: AppErrorType) -> AppError {
        return AppError(title: self.title, message: self.message, errors: self.errors, status: self.status,
                        code: self.code, errorType: self.errorType ?? errorType, analyticErrorSourceName: self.analyticErrorSourceName)
    }
    
    private func getErrorType() -> AppErrorType? {
        if isOfflineError {
            return .notConnectedToInternet
        } else {
            return nil
        }
    }
    
    enum CodingKeys: String, CodingKey {
        case error
        case errors
        case message
    }
    
    enum AppErrorType {
        case notConnectedToInternet
        case login
        case premium
        case guestData
        case payment
        case introRequest
        case location
        case changeEmail
    }
    
}

extension AppError {
    
    var printedErrors: String? {
        guard let errors = errors else { return nil }
        return errors
            .flatMap { $0.value }
            .joined(separator: "\n")
    }
    
    var fullErrorMessage: String {
        if let printedErrors = printedErrors, !printedErrors.isEmpty {
            return printedErrors
        } else {
            return message
        }
    }
    
    var simpleAlertData: AlertData {
        return AlertData.fromAppError(appError: self)
    }

}
