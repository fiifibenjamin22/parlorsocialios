//
//  SearchData.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 19/07/2019.
//

import Foundation

struct SearchData: Codable {
    let id: Int
    let name: String
    let type: SearchResponseItemType
    
}

