//
//  SearchResponseItemType.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 30/07/2019.
//

import Foundation

enum SearchResponseItemType: String, Codable {
    case activation = "activation"
    case rsvpdActivation = "activation_rsvped"
    case announcement = "announcement"
    case notFound = "not_found"
}
