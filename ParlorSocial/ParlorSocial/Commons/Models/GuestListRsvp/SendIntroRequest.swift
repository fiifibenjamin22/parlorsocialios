//
//  SendIntroRequest.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 30/05/2019.
//

import Foundation

struct SendIntroRequest: Codable {
    let ids: [Int]
}
