//
//  CheckInRequest.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 10/06/2019.
//

struct CheckInRequest: Codable {
    let coordinates: Coordinates
}

struct Coordinates: Codable {
    let lat: Double
    let lng: Double
}
