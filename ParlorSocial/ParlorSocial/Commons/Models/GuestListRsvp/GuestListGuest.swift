//
//  GuestListGuest.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 08/11/2019.
//  Copyright © 2019 KISSdigital. All rights reserved.
//

import Foundation
/// Member of activation - other user of application.
struct GuestListGuest: Codable, Equatable {
    
    let id: Int?
    let firstName: String
    let lastName: String
    let email: String?
    let isPremium: Bool?
    let isIntroRequested: Bool?
    let info: GuestInfo?
    let imageUrl: URL?
    let isCheckedToIntroRequest: Bool?
    let thumbnailImageUrl: URL?
    let thumbnailImageUrlGreyscale: URL?
    
    init(name: String, surname: String, email: String? = nil, id: Int? = nil,
         isPremium: Bool? = nil, isIntroRequested: Bool? = nil, info: GuestInfo? = nil,
         imageUrl: URL? = nil, isCheckedToIntroRequest: Bool? = false, thumbnailImageUrl: URL? = nil, thumbnailImageUrlGreyscale: URL? = nil) {
        self.id = id
        self.firstName = name
        self.lastName = surname
        self.email = email
        self.isPremium = isPremium
        self.isIntroRequested = isIntroRequested
        self.info = info
        self.imageUrl = imageUrl
        self.isCheckedToIntroRequest = isCheckedToIntroRequest
        self.thumbnailImageUrl = thumbnailImageUrl
        self.thumbnailImageUrlGreyscale = thumbnailImageUrlGreyscale
    }
    
    func withCheckedToIntroRequest(_ isChecked: Bool) -> GuestListGuest {
        return GuestListGuest(name: self.firstName, surname: self.lastName, email: self.email, id: self.id,
                     isPremium: self.isPremium, isIntroRequested: self.isIntroRequested,
                     info: self.info, imageUrl: self.imageUrl, isCheckedToIntroRequest: isChecked,
                     thumbnailImageUrl: self.thumbnailImageUrl, thumbnailImageUrlGreyscale: self.thumbnailImageUrlGreyscale)
    }
    
    var nameSurname: String {
        return "\(firstName) \(lastName)"
    }
    
}

struct GuestInfo: Codable, Equatable {
    let position: String?
    let company: String?
    let education: String?
    let university: String?
}
