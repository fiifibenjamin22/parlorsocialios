//
//  GetGuestListMetaResponse.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 30/05/2019.
//

struct GetGuestListMetaResponse: Codable {
    let data: [GuestListGuest]
    let meta: MetadataContent?
    
    struct MetadataContent: Codable {
        let countPremiumMembers: Int?
    }
}
