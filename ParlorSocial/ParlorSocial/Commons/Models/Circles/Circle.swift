//
//  Section.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 24/05/2019.
//

import Foundation

struct Circle: Codable {
    
    let id: Int
    let name: String
    let description: String
    
    private let images: [String]
    var imageURLs: [URL?] {
        return images.map { URL(string: $0.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)) }
    }
}
