//  AnalyticEventPaymentMethodType.swift
//  ParlorSocialClub
//
//  Created on 04/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

enum AnalyticEventPaymentMethodType: String {
    case applePay = "apple_pay"
    case card = "credit_card"
}
