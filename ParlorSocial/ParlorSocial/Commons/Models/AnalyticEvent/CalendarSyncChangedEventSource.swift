//  CalendarSyncChangedEventSource.swift
//  ParlorSocialClub
//
//  Created on 29/04/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

enum CalendarSyncChangedEventSource: String {
    case afterFirstRsvp = "after_first_rsvp"
    case reminderOther = "reminder_other"
    case manual = "manual"
}
