//
//  AnalyticEventScreen.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 03/10/2019.
//  Copyright © 2019 KISSdigital. All rights reserved.
//

import Foundation

enum AnalyticEventScreen: String, Codable {
    case activationDetails = "activation_details"
    case activationDetailsInfo = "activation_info"
    case activationsList = "activations_list"
    case allList = "home_all_list"
    case mixersList = "home_mixers_list"
    case happeningsList = "home_happenings_list"
    case rsvpsList = "home_rsvps_list"
    case addOrUpdateCreditCard = "add_or_update_credit_card"
    case addProfilePhoto = "add_profile_photo"
    case announcementDetails = "announcement_details"
    case announcementsList = "announcements_list"
    case calendar
    case calendarSync = "calendar_sync"
    case changeEmail = "change_email"
    case changePassword = "change_password"
    case circleDetails = "circle_details"
    case createOrUpdateRsvp = "create_or_update_rsvp"
    case creditCardsList = "credit_cards_list"
    case editProfile = "edit_profile"
    case enableNotificationsReminder = "enable_notifications_reminder"
    case forgotPassword = "forgot_password"
    case gallery
    case guestList = "guest_list"
    case guestListPopup = "guest_list_popup"
    case hostDetails = "host_details"
    case memberProfile = "member_profile"
    case interestGroups = "interest_groups"
    case locationsList = "locations_list"
    case mapProvider = "map_provider"
    case membershipPlans = "select_membership_plan"
    case parlorPassDetails = "parlor_pass_details"
    case payment
    case profile
    case referAFriend = "refer_a_friend"
    case rsvpConfirmed = "rsvp_confirmed"
    case search
    case suggestInterest = "suggest_interest"
    case upgradeToPremium = "upgrade_to_premium"
    case waitlist
    case webView = "web_view"
    case tutorial
    case userNotApproved = "user_not_approved"
    case connections
    case connectionRequests = "connection_requests"
    case appUpdateRequired = "app_update_required"
    case myConnections = "my_connections"
    case connectionsAttending = "connections_attending"
}
