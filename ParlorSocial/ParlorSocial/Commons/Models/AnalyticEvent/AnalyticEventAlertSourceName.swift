//  AnalyticEventAlertSourceName.swift
//  ParlorSocialClub
//
//  Created on 14/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

enum AnalyticEventAlertSourceName: String {
    case apiError = "api_error"
    case validationError = "validation_error"
    case activationHasAlreadyStarted = "activation_has_already_started"
    case activationCancelReservation = "activation_cancel_reservation"
    case calendarPermissionsDenied = "calendar_permissions_denied"
    case proceedWithPayment = "proceed_with_payment"
    case creditCardRequiredToRsvp = "credit_card_required_to_rsvp"
    case applePayPaymentError = "apple_pay_payment_error"
    case activationFullGuestList = "activation_full_guest_list"
}
