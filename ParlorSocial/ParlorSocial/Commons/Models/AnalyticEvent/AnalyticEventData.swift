//
//  AnalyticEventData.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 03/10/2019.
//  Copyright © 2019 KISSdigital. All rights reserved.
//

import Foundation

struct AnalyticEventData: Codable, Hashable {
    let name: String?
    let duration: Int64?
    let referencedId: Int?
    let isEnabled: Bool?
    let source: String?
    let sourceId: Int?
    
    init(usingData data: AnalyticEventDataEntity?) {
        name = data?.name
        duration = data?.duration?.int64Value
        referencedId = data?.referencedId?.intValue
        isEnabled = data?.isEnabled
        source = data?.source
        sourceId = data?.sourceId?.intValue
    }
    
    init(name: String?, duration: Int64?, referencedId: Int?, isEnabled: Bool?, source: String?, sourceId: Int?) {
        self.name = name
        self.duration = duration
        self.referencedId = referencedId
        self.isEnabled = isEnabled
        self.source = source
        self.sourceId = sourceId
    }
    
    static func createForScreenOpenType(name: AnalyticEventScreen, referencedId: Int?, source: String? = nil, sourceId: Int? = nil) -> AnalyticEventData {
        AnalyticEventData(name: name.rawValue, duration: nil, referencedId: referencedId, isEnabled: nil, source: source, sourceId: sourceId)
    }
    
    static func createForScreenCloseType(name: AnalyticEventScreen, duration: Int64, referencedId: Int?, source: String? = nil, sourceId: Int? = nil) -> AnalyticEventData {
        AnalyticEventData(name: name.rawValue, duration: duration, referencedId: referencedId, isEnabled: nil, source: source, sourceId: sourceId)
    }
    
    static func createForSpecialType(name: String? = nil, duration: Int64? = nil, referencedId: Int? = nil, isEnabled: Bool? = nil, source: String? = nil, sourceId: Int? = nil) -> AnalyticEventData {
        AnalyticEventData(name: name, duration: duration, referencedId: referencedId, isEnabled: isEnabled, source: source, sourceId: sourceId)
    }
 
    static func createForButtonTapAction(withName name: String, referenceId: Int? = nil) -> AnalyticEventData {
        AnalyticEventData(name: name, duration: nil, referencedId: referenceId, isEnabled: nil, source: nil, sourceId: nil)
    }
}
