//  UpgradeToPremiumNavigationSource.swift
//  ParlorSocialClub
//
//  Created on 22/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

enum UpgradeToPremiumNavigationSource {
    case activation(Int)
    case profile
    
    var name: String {
        switch self {
        case .activation: return "activation"
        case .profile: return "profile"
        }
    }
}
