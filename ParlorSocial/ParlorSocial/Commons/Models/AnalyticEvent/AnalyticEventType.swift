//
//  AnalyticEventType.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 03/10/2019.
//  Copyright © 2019 KISSdigital. All rights reserved.
//

import Foundation

enum AnalyticEventType: String, Codable {
    case screenOpened = "screen_opened"
    case screenClosed = "screen_closed"
    case rsvpCreated = "rsvp_created"
    case delayedRsvpNotificationSubscribed = "delayed_rsvp_notification_subscribed"
    case tabSelected = "tab_selected"
    case calendarSyncSettingsChanged = "calendar_sync_settings_changed"
    case notificationsSettingsChanged = "notifications_settings_changed"
    case pushNotificationOpened = "push_notification_opened"
    case membershipSubscriptionPurchased = "membership_subscription_purchased"
    case waitlistRsvpCreated = "waitlist_rsvp_created"
    case buttonClicked = "button_clicked"
    case nativeShare = "native_share"
}
