//  NotificationsSettingsChangedEventSource.swift
//  ParlorSocialClub
//
//  Created on 27/04/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

enum NotificationsSettingsChangedEventSource: String {
    case reminderLoginFlow = "reminder_login_flow"
    case reminderOther = "reminder_other"
    case manual = "manual"
    case userNotApprovedScreen = "user_not_approved_screen"
}
