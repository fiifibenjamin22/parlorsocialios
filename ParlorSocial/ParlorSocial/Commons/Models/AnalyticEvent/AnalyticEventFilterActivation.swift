//
//  AnalyticEventFilterActivation.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 06/11/2019.
//  Copyright © 2019 KISSdigital. All rights reserved.
//

import Foundation

enum AnalyticEventFilterActivation: String, Codable {
    case all
    case collective
    case scene
    case forYou = "for_you"
    case happenings
}
