//
//  AnalyticEventRequest.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 07/10/2019.
//  Copyright © 2019 KISSdigital. All rights reserved.
//

import Foundation

struct AnalyticEventRequest: Codable {
    let events: [AnalyticEvent]
    
    init(usingData data: [AnalyticEventEntity]) {
        events = data.map { event in
            AnalyticEvent(
                userId: event.userId.intValue,
                deviceToken: event.deviceToken,
                timestamp: event.timestamp.int64Value,
                type: AnalyticEventType(rawValue: event.type)!,
                data: AnalyticEventData(usingData: event.eventData),
                sessionId: event.sessionId.int64Value
            )
        }
    }
}
