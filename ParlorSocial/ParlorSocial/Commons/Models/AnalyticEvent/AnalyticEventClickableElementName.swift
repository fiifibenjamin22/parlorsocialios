//  AnalyticEventClickableElementName.swift
//  ParlorSocialClub
//
//  Created on 13/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

enum AnalyticEventClickableElementName {
    enum HomeScreen: String {
        case search = "home_search"
        case calendar = "home_calendar"

        enum All: String {
            case tab = "home_all_list_tab"
            case item = "home_all_list_item"
            case rsvpActionButton = "home_all_list_item_action_rsvp"
            case rsvpdActionButton = "home_all_list_item_action_rsvpd"
            case liveActionButton = "home_all_list_item_action_live"
            case guestListActionButton = "home_all_list_item_action_guest_list"
            case updatesActionButton = "home_all_list_item_action_updates"
        }

        enum Mixers: String {
            case tab = "home_mixers_list_tab"
            case item = "home_mixers_list_item"
            case rsvpActionButton = "home_mixers_list_item_action_rsvp"
            case rsvpdActionButton = "home_mixers_list_item_action_rsvpd"
            case liveActionButton = "home_mixers_list_item_action_live"
            case guestListActionButton = "home_mixers_list_item_action_guest_list"
            case updatesActionButton = "home_mixers_list_item_action_updates"
            case emptyViewButton = "home_mixers_list_empty_view_all_events"
        }

        enum Happenings: String {
            case tab = "home_happenings_list_tab"
            case item = "home_happenings_list_item"
            case rsvpActionButton = "home_happenings_item_action_rsvp"
            case rsvpdActionButton = "home_happenings_list_item_action_rsvpd"
            case liveActionButton = "home_happenings_list_item_action_live"
            case guestListActionButton = "home_happenings_list_item_action_rsvp"
            case updatesActionButton = "home_happenings_list_item_action_updates"
            case emptyViewButton = "home_happenings_list_empty_view_all_events"
        }

        enum Rsvps: String {
            case tab = "home_rsvps_list_tab"
            case upcomingItem = "home_rsvps_list_item_upcoming"
            case historyItem = "home_rsvps_list_item_history"
            case liveActionButton = "home_rsvps_list_item_action_live"
            case guestListActionButton = "home_rsvps_list_item_action_guest_list"
            case updatesActionButton = "home_rsvps_list_item_action_updates"
            case emptyViewButton = "home_rsvps_list_empty_view_all_events"
        }
    }

    enum CalendarScreen: String {
        case showPreviousMonthArrow = "calendar_show_previous_month_arrow"
        case showNextMonthArrow = "calendar_show_next_month_arrow"
        case close = "calendar_close"
        case dayItem = "calendar_day_item"
        case activationItem = "calendar_activation_item"
    }
    
    enum SearchScreen: String {
        case cancel = "search_cancel"
        case clearText = "search_clear_text"
        case clearHistory = "search_clear_history"
        case historyItem = "search_history_list_item"
        case listItem = "search_list_item"
    }
    
    enum ActivationDetails: String {
        case close = "activation_details_close"
        case share = "activation_details_share"
        case actionGuestList = "activation_details_action_guest_list"
        case actionLive = "activation_details_action_live"
        case actionUpdates = "activation_details_action_updates"
        case viewOnMap = "activation_details_view_on_map"
        case rsvp = "activation_details_rsvp"
        case guestsNumber = "activation_details_guests_number"
        case guestsPickerPlus = "activation_details_guests_picker_plus"
        case guestsPickerMinus = "activation_details_guests_picker_minus"
        case guestsPickerDone = "activation_details_guests_picker_done"
        case editRsvp = "activation_details_edit_rsvp"
        case notifyMe = "activation_details_notify_me"
        case hostItem = "activation_details_host_item"
        case link = "activation_details_link"
    }
    
    enum ActivationDetailsInfo: String {
        case activationName = "activation_info_activation_name"
        case linkJoin = "activation_info_link_join"
        case linkShare = "activation_info_link_share"
        case linkInUpdateContent = "activation_info_link_in_update_content"
        case guestList = "activation_info_guest_list"
    }
    
    enum ShareActivationScreen: String {
        case messages = "share_activation_messages"
        case mail = "share_activation_mail"
        case messenger = "share_activation_messenger"
        case whatsapp = "share_activation_whatsapp"
        case copy = "share_activation_copy"
        case more = "share_activation_more"
    }
    
    enum AlertDialog: String {
        case close = "dialog_close"
        case positive = "dialog_positive"
        
        func withAlertSourceName(_ sourceName: AnalyticEventAlertSourceName?) -> String? {
            guard let name = sourceName?.rawValue else { return nil }
            return name + "_" + self.rawValue
        }
    }
    
    enum ParlorPassDetails: String {
        case activationName = "parlor_pass_details_activation_name"
        case locationName = "parlor_pass_details_location_name"
        case timeInformation = "parlor_pass_details_time_information"
        case guests = "parlor_pass_details_guests"
        case guestList = "parlor_pass_details_guest_list"
        case buyExternalTickets = "parlor_pass_details_buy_external_tickets"
        case cancelReservation = "parlor_pass_details_cancel_reservation"
        case host = "parlor_pass_details_host"
    }
    
    enum Profile: String {
        case circlesTab = "profile_circles_tab"
        case interestsTab = "profile_interests_tab"
        case contactTab = "profile_contact_tab"
        case settingsTab = "profile_settings_tab"
        case editProfile = "profile_edit_profile"
        case changeEmail = "profile_change_email"
        case changePassword = "profile_change_password"
        case mapProvider = "profile_map_provider"
        case seeManualAgain = "profile_see_manual_again"
        case terms = "profile_terms_and_conditions"
        case privacy = "profile_privacy_policy"
        case creditCard = "profile_credit_card"
        case logout = "profile_logout"
    }
    
    enum RSVPDetails: String {
        case close = "create_or_update_rsvp_close"
        case cancelReservation = "create_or_update_rsvp_cancel_reservation"
        case addGuest = "create_or_update_rsvp_add_guest"
        case done = "create_or_update_rsvp_done"
        case update = "create_or_update_rsvp_update"
        case removeGuest = "create_or_update_rsvp_remove_guest"
        case nominateGuest = "create_or_update_rsvp_nominate_guest"
        case timeSegments = "create_or_update_rsvp_time_segments"
        case timeSegmentsItem = "create_or_update_rsvp_time_segments_picker_item"
        case buyExternalTickets = "create_or_update_rsvp_buy_external_tickets"
        case paymentMethods = "create_or_update_rsvp_payment_methods"
        case paymentMethodsItem = "create_or_update_rsvp_payment_methods_picker_item"
        case paymentMethodsAddAnother = "create_or_update_rsvp_payment_methods_picker_add_another"
    }

    enum Connections: String {
        case syncContacts = "connections_sync_contacts"
        case referFriend = "connections_refer_friend"
        case viewGuestLists = "connections_view_guest_lists"
        case search = "connections_search"
        case acceptRequest = "connections_accept_connection_request"
        case rejectRequest = "connections_reject_connection_request"
        case myConnection = "connections_my_connection_item"
        case myConnectionViewMore = "connections_my_connection_view_more"
        case viewMoreMyConnections = "connections_view_more_my_connections"
        case viewMoreConnectionRequests = "connections_view_more_connection_requests"
        case connectionRequest = "connections_connection_request_item"
    }
    
    enum ActivateMembership: String {
        case more = "activate_membership_more"
    }

    enum RsvpConfirmed: String {
        case done = "rsvp_confirmed_done"
        case close = "rsvp_confirmed_close"
    }
    
    enum ConnectionRequests: String {
        case acceptRequest = "connection_requests_accept_connection_request"
        case rejectRequest = "connection_requests_reject_connection_request"
    }
    
    enum MyConnections: String {
        case sync = "my_connections_sync"
        case refer = "my_connections_refer"
    }
}
