//
//  AnalyticEvent.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 03/10/2019.
//  Copyright © 2019 KISSdigital. All rights reserved.
//

import Foundation

struct AnalyticEvent: Codable, Hashable {
    let userId: Int
    let deviceToken: String?
    let timestamp: Int64
    let type: AnalyticEventType
    let data: AnalyticEventData?
    let sessionId: Int64
}
