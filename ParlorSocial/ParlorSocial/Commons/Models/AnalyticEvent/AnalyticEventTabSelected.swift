//
//  AnalyticEventTabSelected.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 17/01/2020.
//

import Foundation

enum AnalyticEventTabSelected: String, Codable {
    case home
    case parlorPasses = "parlor_passes"
    case community
    case announcements
    case profile
}
