//
//  AnalyticEventViewControllerData.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 03/10/2019.
//  Copyright © 2019 KISSdigital. All rights reserved.
//

import Foundation

struct AnalyticEventViewControllerData {
    let eventType: AnalyticEventType
    let screenName: AnalyticEventScreen
    let timestamp: Int64
}
