//
//  ApiResponse.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 03/04/2019.
//

import Foundation

enum ApiResponse<T: Decodable> {
    case success(T)
    case failure(AppError)
}

enum EmptyResponse {
    case success
    case failure(AppError)
}

