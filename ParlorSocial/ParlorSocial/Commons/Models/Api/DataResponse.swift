
//
//  DataResposne.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 17/04/2019.
//

import Foundation

struct DataResponse<Model: Decodable>: Decodable {
    let data: Model
}

struct DataMessageResponse<Model: Decodable>: Decodable {
    let message: String
    let data: Model
}
