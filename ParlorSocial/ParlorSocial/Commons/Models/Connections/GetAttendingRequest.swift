//
//  GetAttendingRequest.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 28/08/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

enum ConnectionsAttendings {
    enum Get {
        struct Request: Codable {
            private(set) var page: Int

            static var initial: Request {
                return Request(
                    page: 1
                )
            }

            mutating func set(page: Int) {
                self.page = page
            }

            mutating func increasePage() {
                self.page += 1
            }
        }

        struct Response: Codable & PaginatableResponse {
            typealias ListElement = ConnectionsAttending

            let data: [ConnectionsAttending]
            let meta: ListMetadata

            func withActivations(_ data: [ConnectionsAttending]) -> Response {
                return Response(data: data, meta: self.meta)
            }
        }
    }
}
