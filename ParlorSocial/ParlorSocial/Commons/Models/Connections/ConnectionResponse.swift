//  ConnectionResponse.swift
//  ParlorSocialClub
//
//  Created on 13/08/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

struct ConnectionResponse: Codable {
    let id: Int
    let connectionStatus: ConnectionStatus
}
