//
//  ConnectionRequest.swift
//  ParlorSocialClub
//
//  Created by Przemysław Cygan - KISS digital on 24/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

struct ConnectionRequest: Codable, Equatable {
    let requestId: Int
    let memberId: Int
    let name: String
    let imageUrl: String?

    var imageURL: URL? {
        return imageUrl?.url
    }
}
