//
//  GetMyConnections.swift
//  ParlorSocialClub
//
//  Created by Przemysław Cygan - KISS digital on 24/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

enum MyConnections {
    enum Get {
        struct Request: Codable {
            private(set) var page: Int

            static var initial: Request {
                return Request(
                    page: 1
                )
            }

            mutating func set(page: Int) {
                self.page = page
            }

            mutating func increasePage() {
                self.page += 1
            }
        }

        struct Response: Codable & PaginatableResponse {
            typealias ListElement = MyConnection

            let data: [MyConnection]
            let meta: ListMetadata

            func withActivations(_ data: [MyConnection]) -> Response {
                return Response(data: data, meta: self.meta)
            }
        }
    }
}
