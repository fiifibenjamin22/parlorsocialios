//
//  UpdateConnectionRequest.swift
//  ParlorSocialClub
//
//  Created by Przemysław Cygan - KISS digital on 24/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

struct UpdateConnectionRequest: Codable {
    let accept: Bool
}
