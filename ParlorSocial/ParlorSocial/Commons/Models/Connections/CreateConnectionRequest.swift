//  CreateConnectionRequest.swift
//  ParlorSocialClub
//
//  Created on 13/08/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

struct CreateConnectionRequest: Codable {
    let memberId: Int
}
