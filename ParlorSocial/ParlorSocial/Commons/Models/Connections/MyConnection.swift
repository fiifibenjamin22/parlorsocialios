//
//  MyConnection.swift
//  ParlorSocialClub
//
//  Created by Przemysław Cygan - KISS digital on 24/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

struct MyConnection: Codable, Equatable {
    let connectionId: Int
    let memberId: Int
    let name: String
    let company: String?
    let position: String?
    let imageUrl: String?

    var imageURL: URL? {
        return imageUrl?.url
    }
}
