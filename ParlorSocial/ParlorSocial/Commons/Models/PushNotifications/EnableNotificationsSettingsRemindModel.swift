//
//  EnableNotificationsSettingsRemindModel.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 10/03/2020.
//

import Foundation
import UserNotifications

struct EnableNotificationsSettingsRemindModel {
    let enableNotificationsRemindLevel: EnableNotificationsSettingsRemindLevel
    let rsvpsCount: Int
    let lastRejectNotificationsPermissionsDate: Date?
    let appEntryCount: Int
    let notificationsPermissions: UNAuthorizationStatus
}
