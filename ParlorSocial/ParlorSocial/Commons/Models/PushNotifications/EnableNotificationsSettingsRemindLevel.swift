//
//  EnableNotificationsSettingsRemindLevel.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 09/03/2020.
//

import Foundation

enum EnableNotificationsSettingsRemindLevel: Int, Codable {
    case zero
    case first
    case second
    case third
}
