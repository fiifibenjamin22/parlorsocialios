//  PushNotificationData.swift
//  ParlorSocialClub
//
//  Created on 01/04/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

struct PushNotificationData {
    let id: Int?
    let destination: NotificationDestination?
    let destinationId: Int?
    let title: String?
    let body: String?
        
    init(from data: PushNotificationDetail) {
        self.id = data.aps.id ?? data.id
        self.destination = data.aps.destination ?? data.destination
        self.destinationId = data.aps.destinationId ?? data.destinationId
        self.title = data.aps.alert?.title
        self.body = data.aps.alert?.body
    }
}
