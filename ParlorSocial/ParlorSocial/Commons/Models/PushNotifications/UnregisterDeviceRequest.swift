//
//  UnregisterDeviceRequest.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 25/06/2019.
//

import Foundation

struct UnregisterDeviceRequest: Codable {
    let deviceToken: String
}
