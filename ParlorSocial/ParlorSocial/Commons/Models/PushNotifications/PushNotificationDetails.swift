//
//  PushNotificationDetails.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 25/06/2019.
//

import UIKit

struct PushNotificationDetail: Codable {
    let aps: Aps
    let destination: NotificationDestination?
    let destinationId: Int?
    let id: Int?
}
struct Aps: Codable {
    let alert: Alert?
    let badge: Int?
    let sound: String?
    let destination: NotificationDestination?
    let destinationId: Int?
    let id: Int?
}
struct Alert: Codable {
    let title: String?
    let body: String?
}

enum NotificationDestination: String, Codable {
    case guestListAvailable
    case transferFromWaitlist
    case announcement
    case rsvpOpen
    case premiumMembershipAccepted
    case newActivationPublished
    case upcomingActivationReminder
    case previewActivateMembership
    case complimentaryMembershipExpirationWarning
    case memberApplicationApproved
    
    var alertIcon: UIImage? {
        switch self {
        case .premiumMembershipAccepted, .previewActivateMembership, .complimentaryMembershipExpirationWarning, .memberApplicationApproved:
            return Icons.PushNotifications.generalPremium
        case .transferFromWaitlist:
            return Icons.PushNotifications.waitListTransfer
        case .rsvpOpen, .newActivationPublished, .upcomingActivationReminder:
            return Icons.PushNotifications.rsvpOpen
        default:
            return nil
        }
    }
}
