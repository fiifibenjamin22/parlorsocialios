//
//  RegisterDeviceRequest.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 24/06/2019.
//

import Foundation

struct RegisterDeviceRequest: Codable {
    let system: String
    let token: String
    let language: String
    
    init(token: String, language: String) {
        self.token = token
        self.language = language
        self.system = Strings.system.localized
    }
}
