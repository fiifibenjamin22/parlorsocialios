//
//  Destination.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 03/04/2019.
//

import Foundation
import UIKit

protocol Destination {
    var viewController: UIViewController { get }
}

enum AppViewControllerDestination: Destination {
    case waitListInfo(activationId: Int)
    case appTabBar(selectedTab: MainTabBarScreen)
    case activationDetails(activationId: Int, openingType: OpeningActivationDetailsType, navigationSource: ActivationDetailsNavigationSource)
    case premiumSubscription
    case rsvpDetails(activationId: Int)
    case membershipPlans
    case announcementDetails(announcementId: Int)
    
    var viewController: UIViewController {
        switch self {
        case .waitListInfo(let activationId):
            return WaitlistInfoViewController(forActivationId: activationId).embedInNavigationController()
        case .appTabBar(let selectedTab):
            return AppTabBarController(selectedTab: selectedTab).embedInNavigationController()
        case .activationDetails(let activationId, let openingType, let navigationSource):
            return ActivationDetailsNavigationProvider.getVCByAvailabilityOfOpeningActivationDetails(
                forActivationId: activationId, openingType: openingType, navigationSource: navigationSource).embedInNavigationController()
        case .premiumSubscription:
            return PremiumPaymentViewController()
        case .rsvpDetails(let activationId):
            return RsvpDetailsViewController(activationId: activationId).embedInNavigationController()
        case .membershipPlans:
            return MembershipPlansViewController().embedInNavigationController()
        case .announcementDetails(let announcementId):
            return AnnouncementDetailsViewController(withAnnouncementId: announcementId).embedInNavigationController()
        }
    }
}

