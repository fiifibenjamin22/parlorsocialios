//
//  Router.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 03/04/2019.
//

import Foundation
import UIKit

class Router {

    private weak var viewController: UIViewController?

    init(viewController: UIViewController?) {
        self.viewController = viewController
    }

    func replace(with destination: Destination, animated: Bool = true) {
        viewController?.dismiss()
        let options = animated ? UIWindow.TransitionOptions(direction: .fade, style: .easeInOut) : UIWindow.TransitionOptions()
        UIApplication.shared.keyWindow?.setRootViewController(
            destination.viewController,
            options: options
        )
    }

    func present(destination: Destination, animated: Bool = true, withStyle style: UIModalPresentationStyle = .fullScreen) {
        let destinationViewController = destination.viewController
        destinationViewController.modalPresentationStyle = style
        guard let viewController = viewController, !viewController.isKind(of: destinationViewController.classForCoder) else { return }
        viewController.present(destinationViewController, animated: animated)
    }

    func push(destination: Destination, animated: Bool = true) {
        let destinationViewController = destination.viewController
        viewController?.navigationController?.pushViewController(destinationViewController, animated: animated)
    }
}
