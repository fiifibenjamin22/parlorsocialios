/* 
  Localizable.strings
  ParlorSocial

  Created by Benjamin Acquah on 25/02/2019.
  Copyright © 2019 KISSdigital. All rights reserved.
*/

"ok" = "OK";
"cancel" = "Cancel";
"unknown.error" = "Unknown error";
"no.internet.connection.error" = "There is no Internet connection";
"enconding.error" = "Encoding error";
"filter" = "Filter";
"done" = "Done";
"edit" = "Edit";
"confirm" = "Confirm";
"save" = "Save";
"try.again" = "Try Again";
"clip.to.board" = "Clip to board";

// MARK: - Activations
"activation.all" = "All";
"activation.events" = "Happenings";
"activation.rsvp" = "RSVP";
"activation.rsvpd" = "RSVP'D";
"activation.premium" = "Premium Only";
"activation.hosted.by" = "Hosted By:";
"activation.premium.alert" = "To take part in this activation you need to be a\U00A0Premium Member.";
"activation.starter.alert" = "This activation has already started.";
"activation.premium.button" = "Apply for Premium membership";
"activation.notify.me" = "Notify me";
"activation.rsvp.begins.at" = "RSVP begins %@";
"activation.empty.foryou" = "Please add more interests in your profile for personalized recommendations, and check this page for updates.";
"activation.date.tbd" = "Date TBD";
"activation.tbd" = "TBD";
"activation.upcoming.events" = "Upcoming at Parlor";
"activation.membership.plan.button" = "Select membership plan";
"activation.guestlist" = "GUEST LIST";
"activation.updates" = "UPDATES";
"activation.live" = "LIVE";
"activation.mixers.button" = "MIXERS";
"activation.happenings.button" = "HAPPENINGS";
"activation.rsvps.button" = "RSVP'S";
"activation.upcoming.today" = "Today";
"activation.upcoming.tonight" = "Tonight";
"activation.upcoming.tomorrow" = "Tomorrow";

// MARK: - Home empty data
"home.emptyDataView.mixers.label" = "No Mixers on the calendar right now.";
"home.emptyDataView.mixers.button" = "VIEW ALL EVENTS";
"home.emptyDataView.happenings.label" = "No Happenings on the calendar right now.";
"home.emptyDataView.happenings.button" = "VIEW ALL EVENTS";
"home.emptyDataView.rsvps.label" = "You don’t have any Guest Lists yet.\nRSVP to an event to get started!";
"home.emptyDataView.rsvps.button" = "VIEW UPCOMING EVENTS";

// MARK: - Rsvps
"rsvps.upcoming.events" = "Upcoming Events";
"rsvps.past.events" = "Past Events";

// MARK: - MyRsvps
"my.rsvps.title" = "MY RSVPS";
"my.rsvps.upcoming.button" = "UPCOMING";
"my.rsvps.history.button" = "HISTORY";
"my.rsvps.upcoming.parlor.pass.title" = "Parlor Pass";
"my.rsvps.upcoming.today" = "Today";
"my.rsvps.upcoming.tonight" = "Tonight";
"my.rsvps.upcoming.tomorrow" = "Tomorrow";
"my.rsvps.upcoming.guest.count.user.only" = "%@ Only";
"my.rsvps.upcoming.guest.count.one" = "%@ + 1 Guest";
"my.rsvps.upcoming.guest.count" = "%@ + %d Guests";
"my.rsvps.coming.soon" = "Coming soon";
"my.rsvps.not.available" = "Not Available";
"my.rsvps.guest.list" = "Guest list";
"my.rsvps.external.ticket.info" = "To take part in this activation you need to buy external tickets.";
"my.rsvps.buy.external.tickets" = "Buy external tickets";
"my.rsvps.cancel.reservation" = "Cancel this reservation";
"my.rsvps.upcoming.waitlist" = "Waitlist";
"my.rsvps.just.me" = "Just me";
"my.rsvps.just.me.plus.one" = "Me + 1 Guest";
"my.rsvps.just.me.plus.more" = "Me + %d Guests";
"my.rsvps.activation.and.segment.start.time" = "Your Reservation Time: %@\nGeneral Opening Time: %@";
"my.rsvps.ticket.at.door.info" = "To take part in this activation you need to buy tickets at the venue.";

// MARK: - Announcements
"announcements.title" = "ANNOUNCEMENTS";
"announcements.read.more" = "Read more";
"announcement.part.of" = "You’re part of this %@ %@";
"announcement.interest" = "Interest group";
"announcement.circle" = "Circle";
"announcement.just.now" = "Just now";
"announcement.days.ago" = "%d days ago";
"anouncement.single.day.ago" = "1 day ago";
"announcement.today" = "Today";

// MARK: - Login
"login.email" = "Email address";
"login.password" = "Password";
"login.forgotpassword" = "Forgot Password?";
"login.login" = "Login";
"login.become.member" = "Become a Parlor member";
"login.email.changed" = "Your email was changed.";
"login.unauthorized.error.message" = "Your session is expired";
"login.payment.required.error.message" = "You need to log in to activate your membership";
"login.validation.fields.login" = "Login field";
"login.validation.fields.password" = "Password field";


// MARK: - ResetPassword
"reset.trouble" = "Having Trouble?";
"reset.help" = "Get Help";
"reset.continue" = "continue";
"reset.message" = "We just need your registered email address to send you a reset link.";
"reset.email" = "E-mail";
"reset.create" = "Create a New Password";
"reset.placeholder.create" = "Hi %@. We have confirmed your identity and membership. Now you can create your membership password.";
"reset.new.password" = "New password";
"reset.confirm" = "Confirm password";
"reset.check.inbox" = "Check your inbox for a password reset email.\nClick on the URL provided in the email and\nenter a new password.";
"reset.login" = "login";
"reset.submit" = "submit password";
"reset.no.match" = "Passwords do not match";
"reset.password.validation.fields.email" = "Email field";
"reset.password.validation.fields.password" = "Password field";
"reset.password.validation.fields.confirm.password" = "Confirm password field";

// MARK: - Guest Form
"guest.form.done" = "Done";
"guest.name" = "First Name";
"guest.surname" = "Last Name";
"guest.email" = "Email Address";
"guest.text.field.required" = "required";
"guest.text.field.optional" = "optional";
"guest.nominate" = "Nominate this guest for membership";
"guest" = "Guest";
"guest.remove" = "Remove";
"guest.add" = "Add a guest";
"guest.time.segment" = "Time of Reservation";
"guest.just.me" = "Just Me";
"guest.more.placeholder" = "You + %d Guests";
"guest.howmany" = "How many guests?";
"guest.price" = "Price:";
"guest.ticket" = "Ticket Purchase";
"guest.ticket.info" = "You may edit or cancel this event 24 hours before however there are no refunds.";
"guest.rsvp.details" = "RSVP details";
"guest.edit" = "Edit your reservation";
"guest.update" = "Update";
"guest.cancel" = "Cancel this reservation";
"guest.no.guest.click.info" = "You currently have no guests joining. Click here to add a guest to your reservation.";
"guest.no.guest.editable.closed" = "You have no guests joining.";
"guest.only.members.event" = "This is a members-only event.";
"guest.cancel.confirm.payment" = "Do you need to cancel this reservation? Unfortunately there will be no refunds.";
"guest.cancel.confirm" = "Do you need to cancel this reservation?";
"guest.zero.price" = "0 $";
"guest.payment.confirmation.alert" = "Proceed with payment?";
"guest.add.card" = "Add card";
"guest.empty.card.alert" = "You need to add a credit card to rsvp to this activation.";
"guest.form.provide.email" = "Please provide your guests' email addresses if you would like us to send invitations to this event.";
"guest.form.full.guest.list.alert" = "Unfortunately this event is fully booked. You can sign up to the waitlist, and we'll let you know if any spots open up.";
"guest.form.full.guest.list.only.you.alert" = "Unfortunately the event is fully booked and we can only accommodate you but not any of your guests. If you prefer to take somebody with you, you may sign up to the waitlist.";
"guest.form.full.guest.list.number.available.places.alert" = "Unfortunately the event is fully booked and we can only accommodate %d of your guests. If you prefer to go with the entire group, you may sign up to the waitlist.";

// MARK: - Activation Details
"details.guest.hosts" = "Guest Host";
"details.parlor.hosts" = "Parlor Host";
"details.member.hosts" = "Member Host";
"details.notable.hosts" = "Notable Host";
"details.time" = "time:";
"details.entry" = "entry:";
"details.share" = "Share with friend";
"details.guest.list" = "Guest list";
"details.live" = "Live";
"details.updates" = "Updates";
"details.location" = "location:";
"details.view.map" = "View on map";
"details.free" = "Free";
"details.confirmed" = "Reservation confirmed";
"details.updated" = "Reservation updated";
"details.rsvp.waitlist" = "You're on the waitlist";
"details.reservation.confirmed" = "Your reservation is confirmed.";
"details.visit.bottom.navigation" = "Visit the bottom navigation P icon to view your Parlor Pass for this event.";

// MARK: - Activation Details Info
"details.info.guest.list" = "Guest list";
"details.info.updates" = "Updates";
"details.info.guest.list.coming.soon" = "Coming soon";
"details.info.join" = "Join";

// MARK: - Share Event
"share.event.title.link" = "Share Link";
"share.event.title.event" = "Share Event";
"share.event.messages" = "Messages";
"share.event.mail" = "Mail";
"share.event.messenger" = "Messenger";
"share.event.whatsapp" = "Whatsapp";
"share.event.copy.link" = "Copy Link";
"share.event.more" = "More";

// MARK: - Activation Details
"host.position" = "position";
"host.company" = "company";
"host.education" = "education";
"host.interestgroups" = "interest groups";
"host.about" = "about";
"details.stay.tuned" = "Stay tuned for more info about this event";
"details.rsvp.closed" = "The guest list for this event is now closed";

// MARK: - Onboarding
"onboarding.gotit" = "Got it";
"onboarding.remind.me" = "Remind me later";
"onboarding.announcements.title" = "Announcements";
"onboarding.announcements.message" = "This is where you'll see the latest on Parlor.";
"onboarding.rsvps.title" = "RSVP's";
"onboarding.rsvps.message" = "This is where you'll see all of the events you're attending, have access to guest lists and your RSVP's.";
"onboarding.home.title" = "Home";
"onboarding.home.message" = "This is where you'll find everything that's going on.";
"onboarding.profile.title" = "Profile";
"onboarding.profile.message" = "This is your profile. Check if your personal data is correct";
"onboarding.guest.list.title" = "Guest List";
"onboarding.guest.list.message" = "You have the ability to select people on the guest list that you would like to be connecting with. This information will only be used internally to enhance your experience and personalize future activations.";
"onboarding.happenings.title" = "Happenings";
"onboarding.happenings.message" = "Parlor reviews all the cultural and social events across the city and curates selections for the members. You will be able to see the profiles of other members at these events on your app and connect with them digitally.";
"onboarding.mixers.title" = "Mixers";
"onboarding.mixers.message" = "Mixers are events exclusively for members and their guests providing the opportunity to socialize with each other in a private setting.";
"onboarding.community.title" = "Community";
"onboarding.community.message" = "This is the community tab. You can manage your member connections, select members to meet and endorse applicants who want to join parlor.";

// MARK: - Welcome
"welcome.title" = "Welcome to Parlor";
"welcome.message" = "Let's show you around";
"welcome.getstarted" = "Get started";
"welcome.invited" = "You are invited";
"welcome.premium" = "You're a premium Member";
"welcome.name.placeholder" = "Hello, %@";

// MARK: - Register
"register.addemail" = "Add Your E-mail address";
"register.addemail.message" = "We will send you a verification code to confirm your identity.";
"register.birth.date" = "Add Your Date of Birth";
"register.birth.date.message" = "This helps us to confirm the information you submitted in your member application.";
"register.birth.date.placeholder" = "Month / Date / Year";

// MARK: - Waitlist
"waitlist.title" = "you're on the waitlist";
"waitlist.mainmessage" = "Unfortunately this event is fully booked but you have been added to the waitlist";
"waitlist.mainmessage.transferable" = "There are available places for this event";
"waitlist.submessage" = "Parlor will be in touch to let you know when it's open.\nIf this is a ticketed event, you won't get charged until your reservation is confirmed.";
"waitlist.remove" = "Remove me from waitlist";
"waitlist.confirm.submessage" = "Would you like to be transferred to guest list?";
"waitlist.not.on.waitlist" = "You are not on the waitlist";

// MARK: - Interest groups
"interest.suggest" = "Suggest your own interest";
"interest.title" = "Interest groups";
"interest.save" = "Save";
"interests" = "Interests";
"interests.send.us" = "Send us a private message";
"interests.submit.here" = "Submit here";

// MARK: - Edit profile
"edit.profile.title" = "Edit profile";
"edit.profile.name" = "Name";
"edit.profile.contact" = "Contact";
"edit.profile.gender" = "Gender";
"edit.profile.birthday" = "Date of Birth";
"edit.profile.education" = "Educational Background";
"edit.profile.position" = "Position & Employer";
"edit.profile.firstname" = "First name";
"edit.profile.lastname" = "Last name";
"edit.profile.street" = "Street";
"edit.profile.home.zip" = "Home zip";
"edit.profile.work.zip" = "Work zip";
"edit.profile.phone" = "Phone";
"edit.profile.position.placeholder" = "Position";
"edit.profile.employer" = "Employer";
"edit.profile.success" = "Profile updated successfully";
"edit.profile.select.gender" = "Select Gender";

// MARK: - Guest list rsvp
"guest.list.title" = "Guest list";
"guest.list.premium.member" = "Premium Member";
"guest.list.profile" = "Profile";
"guest.list.intro.request" = "Personalize Crowd";
"guest.list.send.request.to.parlor" = "Send request to parlor";
"guest.list.no.thanks" = "No thanks";
"guest.list.count.premium.members" = "+ %d Premium Members";
"guest.list.one.premium.member" = "+ 1 Premium Member";
"guest.list.available.for.premium" = "To personalize connections you need to be a\U00A0Premium Member.";
"guest.list.select.the.members" = "You may select up to %d people you would like to be connecting with. It is only used internally to personalize future activations.";
"guest.list.is.empty" = "No other members RSVP'ed to this activation so far";
"guest.list.intro.request.success" = "Your selections have been delivered to our team.";

// MARK: - Profile
"profile.title" = "Profile";
"profile.circles" = "Circles";
"profile.interest" = "Interests";
"profile.contact" = "Contact";
"profile.settings" = "Settings";
"profile.your.circles" = "Your Circles";
"profile.no.circles.title" = "You are currently not part of any Circles";
"profile.no.circles.message" = "We will be creating more Circles soon, so check back here for your personalized updates.";
"profile.your.circle" = "Your circle";
"profile.your.interests" = "Your Interests";
"profile.leave.circle" = "Leave this circle";
"profile.interests.addmore" = "Add More";
"profile.refer.friend" = "Refer a friend";
"profile.upgrade" = "Upgrade to Premium Membership";
"profile.emailus" = "Email us";
"profile.suggest.events.or.venues" = "Suggest events or venues";
"profile.refer" = "Refer a friend";
"profile.refer.friend.success" = "Please tell your friend to keep an eye out for an email from Parlor.";
"profile.please.contact.us.at" = "Please contact us at: %@";
"profile.confirm.leaving.circle" = "Are you sure you want to leave this Circle? You will need to be reinvited by a Membership Director to rejoin.";

// MARK: - Member profile
"member.profile.about" = "About";
"member.profile.attending" = "Attending";
"member.profile.view.event" = "View Event";
"member.profile.connected" = "Connected";
"member.profile.accept" = "Accept";
"member.profile.requested" = "Requested";
"member.profile.connect" = "Connect";
"member.profile.position" = "Position";
"member.profile.company" = "Company";
"member.profile.education" = "Education";
"member.profile.interestGroups" = "Interest Groups";
"member.profile.linkedin" = "Linkedin";
"member.profile.instagram" = "Instagram";

// MARK: - Refer a friend

"refer.message" = "Refer a Friend to Parlor Social Club";
"refer.sub.message" = "Invite your friends to join and connect with a diverse group of accomplished individuals";
"refer.invite.message" = "Invite your friends to join and connect with a diverse group of accomplished individuals";
"refer.enter.info" = "Enter Their Contact Info";
"refer.first.name" = "First name";
"refer.last.name" = "Last name";
"refer.email" = "Email address";
"refer.invite" = "Send Invitation";
"refer.fiend.validation.fields.first.name" = "First name field";
"refer.fiend.validation.fields.last.name" = "Last name field";
"refer.fiend.validation.fields.email" = "Email field";
"refer.remove" = "Remove";
"refer.friend" = "Friend";

// MARK: - Available guest list popup
"guest.list.popup.view.guest.list" = "View guest list";
"guest.list.popup.guest.list.available" = "Guestlist available";

// MARK: - Location service
"location.service.access.denied" = "You have not allowed “Parlor Social” to use your location. If you want to see the guest list please contact Parlor Host or change the settings of your device.";
"location.service.is.disabled" = "The devices GPS coordinates are not available. If you want to check-in for the activation automatically please change settings of your device.";
"location.service.cannot.determined" = "Your location cannot be determined correctly. Please try again or ask Parlor Host to check you in.";

// MARK: - Settings
"settings.email" = "Email Address";
"settings.password" = "Password";
"settings.membership.renewal" = "Membership Renewal Date";
"settings.credit.card" = "Credit Card";
"settings.map.provider" = "Map Provider";
"settings.see.manual" = "See the manual once again";
"settings.privacy" = "Privacy Policy";
"settings.terms" = "Terms and Conditions";
"settings.logout" = "Log out";
"settings.sync" = "Calendar Sync";

// MARK: - Settings - Change Email
"change.email.new" = "New Email";
"change.email.title" = "Change email";
"change.email.update" = "Update Email";
"change.email.validation.fields.email" = "Email field";

// MARK: - Settings - Change Password
"change.pass.current" = "Current Password";
"change.pass.new" = "New Password";
"change.pass.title" = "Change password";
"change.pass.update" = "Update Password";
"change.pass.validation.fields.current.password" = "Current password field";
"change.pass.validation.fields.new.password" = "New password field";

// MARK: - Contact - Upgrade
"upgrade.title" = "Upgrade to premium membership";
"upgrade.payment.title" = "Payment";
"upgrade.standard" = "You currently have a Standard Membership.";
"upgrade.message" = "Want more? Upgrade to a Premium Membership below and enjoy higher quality experiences.";
"upgrade.apply" = "Upgrade to premium membership";
"upgrade.information.point" = "When you upgrade, the amount you will be charged will be prorated till the end of your membership year.";

// MARK: - Map provider
"map.provider.title" = "Map provider";
"map.provider.apple.map.app.name" = "Apple Map";
"map.provider.google.map.app.name" = "Google Map";
"map.provider.waze.map.app.name" = "Waze";
"map.provider.map.quest.app.name" = "Map quest";
"map.provider.apple.map.url" = "http://maps.apple.com/";
"map.provider.google.map.url" = "comgooglemaps://";
"map.provider.waze.map.url" = "waze://";
"map.provider.map.quest.url" = "mapquest://";

// MARK: - Terms and Privacy
"terms.and.privacy.privacy.title" = "Privacy policy";
"terms.and.privacy.terms.title" = "Terms and conditions";

// MARK: - Cards
"card.list.title" = "Credit card";
"card.list.payment.method" = "Payment methods";
"card.list.add" = "Add a new payment method";
"card.list.edit" = "Edit";
"card.list.remove" = "Remove";
"card.remove.alert.title" = "Remove card?";
"card.remove.alert.content" = "Do you want to remove card %@";
"card.add.title" = "Add credit card";
"card.add.number" = "Number";
"card.add.mm" = "MM";
"card.add.yy" = "YY";
"card.add.cvv" = "CVV";
"card.add.name" = "Name";
"card.add.address1" = "Address 1";
"card.add.address2" = "Address 2";
"card.add.city" = "City";
"card.add.state" = "State";
"card.add.zip" = "Zip";
"card.add.button" = "Add";
"card.add.applied" = "You have applied for %@ membership";
"card.applied.fill" = "Please fill in the details of the card to proceed with payment";
"card.add.new.was.added" = "New card was added.";
"card.edit.title" = "Edit credit card";
"card.edit.continue.button" = "Continue";
"card.edit.updated" = "The card is updated.";
"card.four.number.with.dots" = "...%@";
"card.add.another" = "Add another";
"card.validation.fields.month.field" = "Month field";
"card.validation.fields.year.field" = "Year field";

// MARK: - Premium payment
"premium.proceed" = "Proceed with payment";
"premium.credit.card" = "Credit card";
"premium.pay.with" = "Pay with:";
"premium.membership" = "%@ membership";
"premium.premium" = "Premium";
"premium.standard" = "Standard";
"upgrade.price.interval" = "%@ %@";
"premium.pay.using.card" = "or pay using card";
"premium.prorated.discount" = "Prorated discount for current membership";

// MARK: - Calendar
"calendar.title" = "Calendar";
"calendar.sync.title" = "Calendar Sync";
"calendar.sync.message" = "Do you want to add your activations to your outside calendar?";
"calendar.permission.error.title" = "You have denied calendar permissions";
"calendar.permission.error.message" = "Click ok to go to settings and turn Parlor calendar permissions on";
"calendar.title.for.system.calendar" = "Parlor: %@";

// MARK: - Search
"search.placeholder" = "Search";
"search.history" = "Search history";
"search.clear.recent.searches" = "Clear recent searches";
"search.not.found" = "Not found";

// MARK: - Add profile photo
"add.profile.photo.title" = "Profile photo required to RSVP and see event details";
"add.profile.photo.picture.info.message" = "This is the picture that will be visible on the guest list when you sign up for activations";
"add.profile.photo.could.not.be.updated" = "This photo could not be updated. Choose another photo.";

// MARK: - Base web view
"base.web.view.page.cannot.be.loaded" = "This page cannot be loaded";

// MARK: - Membership plans
"membership.plans.nav.title" = "Membership";
"membership.plans.something.about.its.cool" = "To view details and take part in our events, please activate your membership.";
"membership.plans.standard" = "Standard";
"membership.plans.premium" = "Premium";
"membership.plans.free" = "Free";
"membership.to.activate.membership" = "to Activate Membership";
"membership.to.extend.membership" = "to Extend Membership";
"membership.days.count" = "%d days";
"membership.more.button.title" = "More";
"membership.last.day" = "It's last day";
"membership.one.day.count" = "1 day";

// MARK: - Notification service
"notification.service.open" = "Open";

// MARK: - Validation
"validation.must.not.be.empty" = "is required.";
"validation.max.length" = "max length must not be more than %d.";
"validation.min.length" = "min length must not be less than %d.";
"validation.password.power" = "must be eight characters including one uppercase letter, one lowercase letter and one number.";
"validation.date.must.be.least.years" = "You must be at least %d years old to be a member.";
"validation.month.card.is.invalid" = "Your credit card's month is invalid.";
"validation.year.card.is.invalid" = "Your credit card's year is invalid.";

// MARK: - Enable notification reminder
"enable.notification.reminder.continue" = "Continue";
"enable.notification.reminder.title" = "Notifications";
"enable.notification.reminder.permission.message" = "By giving us permission to send notifications you will be informed about new events, guest list availability and Parlor announcements";
"enable.notification.reminder.permission.error.title" = "You have denied notifications permissions";
"enable.notification.reminder.permission.error.message" = "Click ok to go to settings and turn Parlor notifications permissions on";
"enable.notification.reminder.switch.title" = "Notifications";

// MARK: - User application not verified
"user.not.approved.allow.notifications" = "ALLOW NOTIFICATIONS";
"user.not.approved.notifications.allowed.title" = "Your application is under review.\nPlease do not log out of the app to ensure we can notify you when you receive access.";
"user.not.approved.notifications.not.allowed.title" = "Your application is under review.\nPlease click the \"Allow Notifications\" button below to ensure you will be notified when you receive access.";
"user.not.approved.application.status.notifications.allowed.title" = "Your application is on our waitlist.\nWe will send you a notification if there are any changes to your application status.";
"user.not.approved.application.status.notifications.not.allowed.title" = "Your application is on our waitlist.\nPlease click the \"Allow Notifications\" button below to ensure you will be notified when you receive access.";
"user.not.approved.log.out" = "Log Out";
"user.not.approved.log.out.confirm.title" = "If you log out we will not be able to send you a notification when you receive access.";
"user.not.approved.follow.us" = "Follow Us";

// MARK: - App update required
"app.update.required.update.required" = "Update Required";
"app.update.required.update.info" = "There has been an important update to our app.\nPlease install the latest version";
"app.update.required.update" = "UPDATE";

// Mark: - Apple pay
"apple.pay" = "Apple Pay";
"apple.pay.not.available" = "Apple Pay is not available";
"apple.pay.unexpected.error" = "Unexpected error happened. Please try different payment method.";

// MARK: - Community
"community.title" = "COMMUNITY";

// MARK: - Connections
"connections.connect.with.friends" = "Connect with friends to chat and see\nwhat events they are attending!";
"connections.sync.contacts" = "Sync contacts";
"connections.refer.a.friend" = "Refer a friend";
"connections.view.guest.lists" = "View guest lists";
"connections.search.people.on.parlor" = "Search people on Parlor";
"connections.my.connections" = "My Connections";
"connections.request.accept" = "Accept";
"connections.view.more" = "View More";
"connections.connection.requests" = "Connection Requests";

// MARK: MyConnections
"my.connections.title" = "My Connections";
"my.connections.share.title" = "Share Parlor with your friends";
"my.connections.share.sync.button" = "Sync contacts";
"my.connections.share.refer.button" = "Refer a friend";

// MARK: - ConnectionRequests
"connection.requests.title" = "Connection Requests";

//MARK: - Connections Attending
"connections.attending.title" = "Connections Attending";
