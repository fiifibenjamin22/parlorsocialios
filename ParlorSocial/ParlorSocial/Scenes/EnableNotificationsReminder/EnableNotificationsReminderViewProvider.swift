//
//  EnableNotificationsReminderViewProvider.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 09/03/2020.
//

import Foundation

enum EnableNotificationsReminderViewType {
    case beforeDashboard
    case onDashboard
    case afterFirstRsvp
}

final class EnableNotificationsReminderViewProvider {
    private let profileRepository: ProfileRepositoryProtocol
    private let countOfDaysWhenShowReminderOnDashboardForFirstLevel = 14
    private let appEntryCountWhenShowReminderOnDashboard = 5
    private let countOfMonthsWhenShowReminderOnDashboardForSecondLevel = 1
    private let countOfMonthsWhenShowReminderOnDashboardForThirdLevel = 3
    
    init(profileRepository: ProfileRepositoryProtocol = ProfileRepository.shared) {
        self.profileRepository = profileRepository
    }
    
    func showNotificationsReminder(when viewType: EnableNotificationsReminderViewType, _ completionHandler: @escaping (Bool, NotificationsSettingsChangedEventSource) -> Void) {
        switch viewType {
        case .beforeDashboard:
            showNotificationsReminderBeforeDashboard(completionHandler)
        case .onDashboard:
            showNotificationsReminderOnDashboard(completionHandler)
        case .afterFirstRsvp:
            showNotificationsReminderAfterFirstRsvp(completionHandler)
        }
    }
    
}

// MARK: - Private methods
private extension EnableNotificationsReminderViewProvider {
    func showNotificationsReminderBeforeDashboard(_ completionHandler: @escaping (Bool, NotificationsSettingsChangedEventSource) -> Void) {
        profileRepository.getEnableNotificationsSettingsRemindModel { [unowned self] model in
            let isDayAfterLastRejection = self.isMinDaysAfterLastPermissionRejectionDate(model.lastRejectNotificationsPermissionsDate)
            let isLevelEqualsZero = model.enableNotificationsRemindLevel == .zero
            let isPermissionsNotAuthorized = model.notificationsPermissions != .authorized
            
            if isLevelEqualsZero && isPermissionsNotAuthorized && (isDayAfterLastRejection || model.lastRejectNotificationsPermissionsDate == nil) {
                completionHandler(true, .reminderLoginFlow)
            } else {
                completionHandler(false, .reminderLoginFlow)
            }
        }
    }
    
    func showNotificationsReminderAfterFirstRsvp(_ completionHandler: @escaping (Bool, NotificationsSettingsChangedEventSource) -> Void) {
        profileRepository.getEnableNotificationsSettingsRemindModel { [unowned self] model in
            let isDayAfterLastRejection = self.isMinDaysAfterLastPermissionRejectionDate(model.lastRejectNotificationsPermissionsDate)
            if model.enableNotificationsRemindLevel == .first && model.rsvpsCount >= 1 && isDayAfterLastRejection {
                completionHandler(true, .reminderOther)
            } else {
                completionHandler(false, .reminderOther)
            }
        }
    }
    
    func showNotificationsReminderOnDashboard(_ completionHandler: @escaping (Bool, NotificationsSettingsChangedEventSource) -> Void) {
        profileRepository.getEnableNotificationsSettingsRemindModel { [unowned self] model in
            let currentDate = Date()
            guard let countOfDays = currentDate.countOfDaysFrom(date: model.lastRejectNotificationsPermissionsDate),
                let countOfMonths = currentDate.countOfMonthsFrom(date: model.lastRejectNotificationsPermissionsDate) else {
                    completionHandler(false, .reminderOther)
                    return
            }
            if model.enableNotificationsRemindLevel == .first,
                (countOfDays >= self.countOfDaysWhenShowReminderOnDashboardForFirstLevel ||
                model.appEntryCount >= self.appEntryCountWhenShowReminderOnDashboard) {
                completionHandler(true, .reminderOther)
            } else if model.enableNotificationsRemindLevel == .second,
                countOfMonths >= self.countOfMonthsWhenShowReminderOnDashboardForSecondLevel {
                completionHandler(true, .reminderOther)
            } else if model.enableNotificationsRemindLevel == .third,
                countOfMonths >= self.countOfMonthsWhenShowReminderOnDashboardForThirdLevel {
                completionHandler(true, .reminderOther)
            } else {
                completionHandler(false, .reminderOther)
            }
        }
    }
    
    private func isMinDaysAfterLastPermissionRejectionDate(_ lastRejectionDate: Date?) -> Bool {
        guard let lastRejectionDate = lastRejectionDate else { return false }
        let currentDate = Date()
        let numberOfDays = currentDate.countOfDaysFrom(date: lastRejectionDate) ?? 0
        return numberOfDays >= NotificationsConfig.minDaysAfterLastReminderRejection
    }
}
