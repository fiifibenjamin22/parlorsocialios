//
//  EnableNotificationsReminderViewBuilder.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 05/03/2020.
//

import UIKit

final class EnableNotificationsReminderViewBuilder {

    private unowned let controller: EnableNotificationsReminderViewController
    private var view: UIView! { return controller.view }

    init(controller: EnableNotificationsReminderViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension EnableNotificationsReminderViewBuilder {

    private func setupProperties() {
        view.backgroundColor = .white
        
        controller.apply {
            $0.confirmButton.setTitle(Strings.EnableNotificationsReminder.continue.localized.uppercased(), for: .normal)
            $0.notificationSwitch.label.text = Strings.EnableNotificationsReminder.notificationSwitchTitle.localized
            $0.notificationSwitch.isOn = true
        }
    }

    private func setupHierarchy() {
        controller.apply {
            view.addSubviews([$0.iconedMessage, $0.notificationSwitch, $0.confirmButton])
        }
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.iconedMessage.edgesToParent(anchors: [.leading, .trailing],
                                           insets: .margins(left: Constants.horizontalMarginBig, right: Constants.horizontalMarginBig))
            $0.iconedMessage.topAnchor.equal(to: view.safeAreaLayoutGuide.topAnchor, constant: 70)
            
            $0.confirmButton.edgesToParent(anchors: [.leading, .trailing],
                                           insets: .margins(left: Constants.horizontalMarginBig, right: Constants.horizontalMarginBig))
            $0.confirmButton.bottomAnchor.equal(to: view.bottomAnchor, constant: -90)
            $0.confirmButton.heightAnchor.equalTo(constant: Constants.bigButtonHeight)

            $0.notificationSwitch.edgesToParent(anchors: [.leading, .trailing],
                                        insets: .margins(left: Constants.horizontalMarginBig, right: Constants.horizontalMarginBig))
            $0.notificationSwitch.centerYAnchor.equal(to: view.centerYAnchor, constant: 80)
        }
    }

}

// MARK: - Public build methods
extension EnableNotificationsReminderViewBuilder {
    
    func buildContentView() -> UIView {
        return UIView(frame: UIScreen.main.bounds)
    }

    func buildConfirmButton() -> RoundedButton {
        return RoundedButton().manualLayoutable().apply { it in
            it.backgroundColor = UIColor.black
            it.setTitleColor(UIColor.white, for: .normal)
            it.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium)
            it.applyTouchAnimation()
        }
    }
    
    func buildNotificationsMessageView() -> IconedMessageView {
        return IconedMessageView(icon: Icons.EnableNotificationsSettingsReminder.phoneNotificationIcon,
                                 title: Strings.EnableNotificationsReminder.notificationTitle.localized,
                                 message: Strings.EnableNotificationsReminder.notificationPersmissionMessage.localized).manualLayoutable()
    }
    
    func buildLabeledSwitch() -> ParlorLabeledSwitch {
        return ParlorLabeledSwitch().manualLayoutable().apply {
            $0.uiSwitch.onTintColor = .appGreen
        }
    }

}
