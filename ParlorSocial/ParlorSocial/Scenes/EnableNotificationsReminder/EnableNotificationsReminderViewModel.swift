//
//  EnableNotificationsReminderViewModel.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 05/03/2020.
//

import Foundation
import RxSwift
import RxCocoa

protocol EnableNotificationsReminderLogic: BaseViewModelLogic {
    var notificationSwitchStateObs: Observable<Bool> { get }
    func didTapContinue()
    func notificationSwitchStateChanged(state: Bool)
}

class EnableNotificationsReminderViewModel: BaseViewModel {
    // MARK: - Properties
    var analyticEventReferencedId: Int?
    private let notificationService = GlobalNotificationsService.shared
    private let updateNotificationsReminderDataManager: UpdateNotificationsReminderDataManager = .init()
    private let notificationSwitchStateRelay: BehaviorRelay<Bool> = .init(value: true)
    private let notificationsSettingsChangeSource: NotificationsSettingsChangedEventSource
    
    init(notificationsSettingsChangeSource: NotificationsSettingsChangedEventSource) {
        self.notificationsSettingsChangeSource = notificationsSettingsChangeSource

        super.init()
    }
}

// MARK: Interface logic methods
extension EnableNotificationsReminderViewModel: EnableNotificationsReminderLogic {
    var notificationSwitchStateObs: Observable<Bool> {
        return notificationSwitchStateRelay.asObservable()
    }
    
    func notificationSwitchStateChanged(state: Bool) {
        notificationSwitchStateRelay.accept(state)
    }
    
    func didTapContinue() {
        guard notificationSwitchStateRelay.value else {
            updateNotificationsReminderDataManager.updateDataOnEnableNotificationsReminderVC(isGranted: false, notificationsSettingsChangeSource: notificationsSettingsChangeSource)
            closeViewSubject.emitElement()
            return
        }
        
        notificationService.configurePushNotifications { [weak self] isGranted, alertData in
            guard let source = self?.notificationsSettingsChangeSource else { return }
            self?.updateNotificationsReminderDataManager.updateDataOnEnableNotificationsReminderVC(isGranted: isGranted, notificationsSettingsChangeSource: source)
            DispatchQueue.main.async { [weak self] in
                if let alertData = alertData, self?.notificationSwitchStateRelay.value == true {
                    self?.alertDataSubject.onNext(alertData)
                } else {
                    self?.closeViewSubject.emitElement()
                }
            }
        }
    }
}
