//
//  EnableNotificationsReminderViewController.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 05/03/2020.
//

import UIKit
import RxSwift

class EnableNotificationsReminderViewController: AppViewController {

    // MARK: - Views
    private(set) var iconedMessage: IconedMessageView!
    private(set) var confirmButton: RoundedButton!
    private(set) var notificationSwitch: ParlorLabeledSwitch!

    // MARK: - Properties
    let analyticEventScreen: AnalyticEventScreen? = .enableNotificationsReminder
    var viewModel: EnableNotificationsReminderViewModel!
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }

    override var observableSources: ObservableSources? {
        return viewModel
    }
    
    // MARK: - Initialization
    init(notificationsSettingsChangeSource: NotificationsSettingsChangedEventSource) {
        super.init(nibName: nil, bundle: nil)
        setup(notificationsSettingsChangeSource: notificationsSettingsChangeSource)
    }

    required init?(coder aDecoder: NSCoder) {
       fatalError()
    }

    private func setup(notificationsSettingsChangeSource: NotificationsSettingsChangedEventSource) {
        self.viewModel = EnableNotificationsReminderViewModel(notificationsSettingsChangeSource: notificationsSettingsChangeSource)
    }

    // MARK: - Lifecycle
    override func loadView() {
        setupViews()
    }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        adjustNavigationBar()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        statusBar?.backgroundColor = .white
    }
    
    override func observeViewModel() {
        super.observeViewModel()
        viewModel.notificationSwitchStateObs
            .distinctUntilChanged()
            .subscribe(onNext: { [unowned self] isOn in
                self.handleSwitchStateChanging(state: isOn)
            }).disposed(by: disposeBag)
    }
}

// MARK: - Private methods
extension EnableNotificationsReminderViewController {

    private func setupViews() {
        let builder = EnableNotificationsReminderViewBuilder(controller: self)
        view = builder.buildContentView()
        self.iconedMessage = builder.buildNotificationsMessageView()
        self.confirmButton = builder.buildConfirmButton()
        self.notificationSwitch = builder.buildLabeledSwitch()

        builder.setupViews()
        
    }

    private func setupPresentationLogic() {
        notificationSwitch.uiSwitch.addTarget(self, action: #selector(notificationSwitchValueDidChange), for: .valueChanged)
        confirmButton.addTarget(self, action: #selector(didTapContinue), for: .touchUpInside)
    }
    
    private func handleSwitchStateChanging(state: Bool) {
        guard notificationSwitch.isOn != state else { return }
        self.notificationSwitch.isOn = state
    }
    
    private func adjustNavigationBar() {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @objc private func notificationSwitchValueDidChange(syncSwitch: UISwitch) {
        viewModel.notificationSwitchStateChanged(state: syncSwitch.isOn)
    }

    @objc private func didTapContinue() {
        viewModel.didTapContinue()
    }

}
