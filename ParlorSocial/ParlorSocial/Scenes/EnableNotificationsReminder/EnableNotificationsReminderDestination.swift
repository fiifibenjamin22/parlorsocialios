//
//  EnableNotificationsReminderDestination.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 05/03/2020.
//

import UIKit

enum EnableNotificationsReminderDestination {
    case dashboard
}

extension EnableNotificationsReminderDestination: Destination {
    var viewController: UIViewController {
        switch self {
        case .dashboard:
            return AppTabBarController().embedInNavigationController()
        }
    }
}
