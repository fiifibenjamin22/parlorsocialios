//
//  RsvpDetailsViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 13/05/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class RsvpDetailsViewBuilder {

    private unowned let controller: RsvpDetailsViewController
    private var view: UIView! { return controller.view }

    init(controller: RsvpDetailsViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension RsvpDetailsViewBuilder {

    private func setupProperties() {
        view.backgroundColor = .appBackground
        controller.apply {
            $0.mainScrollView.showsVerticalScrollIndicator = false
            $0.mainScrollView.contentInsetAdjustmentBehavior = .never
            
            $0.rsvpImageView.clipsToBounds = true
            
            $0.imageShadowView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            
            $0.dateLabel.adjustsFontSizeToFitWidth = true
            
            $0.scrollContentView.clipsToBounds = true
            $0.scrollContentView.backgroundColor = .appBackground
            
            $0.lineViews.forEach { $0.backgroundColor = UIColor.lightBackgroundSeperator.withAlphaComponent(0.44) }
            
            $0.buyExternalTicketButton.setTitle(Strings.MyRsvps.buyExternalTickets.localized.uppercased(), for: .normal)
            $0.buyExternalTicketButton.setTitleColor(.appText, for: .normal)
            $0.buyExternalTicketButton.trackingId = AnalyticEventClickableElementName.ParlorPassDetails.buyExternalTickets.rawValue
            
            $0.cancelReservationButton.setTitle(Strings.MyRsvps.cancelReservation.localized.uppercased(), for: .normal)
            $0.cancelReservationButton.setTitleColor(.appText, for: .normal)
            $0.cancelReservationButton.trackingId = AnalyticEventClickableElementName.ParlorPassDetails.cancelReservation.rawValue
        }
    }

    private func setupHierarchy() {
        controller.apply {
            [$0.mainScrollView].addTo(parent: view)
            
            $0.mainScrollView.addSubview($0.scrollContentView)
            
            $0.dateTimeStackView.addArrangedSubviews([$0.dateLabel, $0.timeLabel])
            $0.infoVerticalStackView.addArrangedSubviews([
                $0.titleInfoElementView,
                $0.lineViews[0],
                $0.locationInfoElementView,
                $0.lineViews[1],
                $0.segmentTimeElementView,
                $0.lineViews[2],
                $0.guestsInfoElementView,
                $0.lineViews[3],
                $0.hostsListStackView,
                $0.lineViews[4],
                $0.guestsListInfoElementView,
                $0.lineViews[5]])
            
            $0.scrollContentView.addSubviews([$0.rsvpImageView, $0.imageShadowView, $0.dateTimeStackView, $0.infoVerticalStackView, $0.bottomButtonsStackView])
            
            $0.bottomButtonsStackView.addArrangedSubviews([
                $0.ticketAtDoorInformationView,
                $0.externalTicketView,
                $0.cancelReservationButton
            ])
            
            $0.externalTicketView.addSubviews([$0.externalTicketIconImageView, $0.externalTicketInfoLabel, $0.buyExternalTicketButton])
            
        }
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.mainScrollView.edgesToParent(anchors: [.leading, .trailing, .bottom])
            
            $0.scrollContentView.apply {
                $0.edgesToParent()
                $0.widthAnchor.equal(to: view.widthAnchor)
            }
            
            $0.rsvpImageView.apply {
                $0.edgesToParent(anchors: [.leading, .trailing, .top])
                $0.heightAnchor.constraint(
                    equalTo: $0.widthAnchor,
                    multiplier: Constants.RsvpDetails.imageRatio).activate()
            }
            
            $0.imageShadowView.edges(equalTo: $0.rsvpImageView)
            
            $0.dateTimeStackView.apply {
                $0.leadingAnchor.equal(
                    to: controller.rsvpImageView.leadingAnchor,
                    constant: 20)
                $0.trailingAnchor.equal(
                    to: controller.rsvpImageView.trailingAnchor,
                    constant: -20)
                $0.centerYAnchor.equal(to: controller.rsvpImageView.centerYAnchor)
            }
            
            $0.infoVerticalStackView.apply {
                $0.edgesToParent(anchors: [.leading, .trailing], insets: UIEdgeInsets.margins(
                    left: Constants.RsvpDetails.contentMarginLeft,
                    right: Constants.RsvpDetails.contentMarginRight
                ))
                $0.topAnchor.equal(to: controller.rsvpImageView.bottomAnchor, constant: 30)
            }
            
            $0.lineViews.forEach {
                $0.heightAnchor.equalTo(constant: 1)
            }
            
            $0.bottomButtonsStackView.apply {
                $0.topAnchor.equal(
                    to: controller.infoVerticalStackView.bottomAnchor,
                    constant: Constants.RsvpDetails.roundedButtonVerticalMargin)
                $0.leadingAnchor.equal(to: controller.infoVerticalStackView.leadingAnchor)
                $0.trailingAnchor.equal(to: controller.infoVerticalStackView.trailingAnchor)
                $0.bottomAnchor.equal(
                    to: controller.scrollContentView.bottomAnchor,
                    constant: -Constants.RsvpDetails.roundedButtonVerticalMargin
                )
            }
            
            $0.externalTicketIconImageView.apply {
                $0.edgesToParent(anchors: [.leading, .top])
                $0.heightAnchor.equalTo(constant: Constants.RsvpDetails.buyTicketIconSize)
                $0.widthAnchor.equalTo(constant: Constants.RsvpDetails.buyTicketIconSize)
            }
            
            $0.externalTicketInfoLabel.apply {
                $0.edgesToParent(anchors: [.trailing, .top])
                $0.leadingAnchor.equal(to: controller.externalTicketIconImageView.trailingAnchor, constant: 8)
            }
            
            $0.buyExternalTicketButton.apply {
                $0.topAnchor.equal(
                    to: controller.externalTicketInfoLabel.bottomAnchor,
                    constant: 19)
                $0.edgesToParent(anchors: [.leading, .bottom, .trailing])
                $0.heightAnchor.equalTo(constant: Constants.RsvpDetails.roundedButtonHeight)
            }
            
            $0.cancelReservationButton.apply {
                $0.heightAnchor.equalTo(constant: Constants.RsvpDetails.roundedButtonHeight)
            }
            
        }
    }

}

// MARK: - Public build methods
extension RsvpDetailsViewBuilder {
    
    func buildMainScrollView() -> UIScrollView {
        return UIScrollView().manualLayoutable()
    }
    
    func buildView() -> UIView {
        return UIView().manualLayoutable()
    }

    func buildRsvpImageView() -> UIImageView {
        return UIImageView().manualLayoutable().apply {
            $0.contentMode = .scaleAspectFill
        }
    }
    
    func buildDateTimeStackView() -> UIStackView {
        return UIStackView(axis: .vertical, spacing: 10, alignment: .center, distribution: .fill)
    }
    
    func buildDateLabel() -> UILabel {
        return UILabel.styled(
            textColor: .white,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.largeDate,
                font: UIFont.Editor.bold),
            alignment: .center,
            numberOfLines: 1)
    }
    
    func buildTimeLabel() -> UILabel {
        return UILabel.styled(
            textColor: .white,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.hintSize,
                font: UIFont.Roboto.bold),
            alignment: .center)
    }
    
    func buildInfoVerticalStackView() -> UIStackView {
        return UIStackView(axis: .vertical, spacing: 25, alignment: .fill, distribution: .fill)
    }
    
    func buildElementView() -> RsvpDetailsListElementView {
        return RsvpDetailsListElementView()
    }
    
    func buildLineViews() -> [UIView] {
        var views: [UIView] = []
        for _ in 0..<6 {
            views.append(UIView().manualLayoutable())
        }
        return views
    }
    
    func buildHostsListStackView() -> UIStackView {
        return UIStackView(axis: .vertical, spacing: 20, alignment: .fill, distribution: .fill)
    }
    
    func buildBottomButtonsStackView() -> UIStackView {
        return UIStackView(axis: .vertical, spacing: 19, alignment: .fill, distribution: .fill)
    }
    
    func buildExternalTicketView() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildExternalTicketIconImageView() -> UIImageView {
        return UIImageView().manualLayoutable().apply {
            $0.image = Icons.RsvpDetails.externalTicketIcon
        }
    }
    
    func buildExternalTicketInfoLabel() -> UILabel {
        return UILabel.styled(
            textColor: UIColor.appTextSemiLight,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.subbody,
                font: UIFont.Roboto.regular),
            alignment: .left).apply {
                $0.text = Strings.MyRsvps.externalTicketInfo.localized
        }
    }
    
    func buildBuyExternalTicketButton() -> RoundedButton {
        return RoundedButton().manualLayoutable().apply {
            $0.layer.borderColor = UIColor.lightBackgroundSeperator.cgColor
            $0.layer.borderWidth = 1
            $0.applyTouchAnimation()
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium)
        }
    }
    
    func buildCancelReservationButton() -> RoundedButton {
        return RoundedButton().manualLayoutable().apply {
            $0.layer.borderColor = UIColor.lightBackgroundSeperator.cgColor
            $0.layer.borderWidth = 1
            $0.applyTouchAnimation()
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium)
        }
    }
    
    func buildTicketAtDoorInformationView() -> InformationView {
        return InformationView().manualLayoutable().apply {
            $0.title = Strings.MyRsvps.ticketAtDoorInfo.localized
            $0.icon = Icons.GuestsForm.info
            $0.tintColor = .appTextSemiLight
            $0.clipIconToTop = true
            $0.font = UIFont.custom(
                ofSize: Constants.FontSize.subbody,
                font: UIFont.Roboto.regular)
            $0.iconSize = 11
        }
    }
    
}

// MARK: - Constants
extension Constants {
    
    enum RsvpDetails {
        static let imageRatio: CGFloat = 460.0 / 375.0
        static let contentMarginLeft: CGFloat = 28.0
        static let contentMarginRight: CGFloat = 22.0
        static let roundedButtonHeight: CGFloat = 55.0
        static let roundedButtonVerticalMargin: CGFloat = 45.0
        static let roundedButtonHorizontalMargin: CGFloat = 16.0
        static let buyTicketIconSize: CGFloat = 11.0
    }
    
}
