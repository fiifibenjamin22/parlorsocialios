//
//  RsvpDetailsViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 13/05/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class RsvpDetailsViewController: AppViewController {

    // MARK: - Properties
    let analyticEventScreen: AnalyticEventScreen? = .parlorPassDetails
    private var viewModel: RsvpDetailsLogic!
    private var currentStatusBarStyle = UIStatusBarStyle.lightContent
    private var hostsListLineViewIndex = 4
    private var guestListLineViewIndex = 5
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }
    
    private lazy var refreshControl: UIRefreshControl = {
        return UIRefreshControl().apply { $0.tintColor = .black }
    }()
    
    // MARK: - Views
    private(set) var mainScrollView: UIScrollView!
    private(set) var scrollContentView: UIView!
    private(set) var imageShadowView: UIView!
    private(set) var rsvpImageView: UIImageView!
    private(set) var dateLabel: UILabel!
    private(set) var timeLabel: UILabel!
    private(set) var dateTimeStackView: UIStackView!
    private(set) var infoVerticalStackView: UIStackView!
    private(set) var titleInfoElementView: RsvpDetailsListElementView!
    private(set) var lineViews: [UIView]!
    private(set) var locationInfoElementView: RsvpDetailsListElementView!
    private(set) var segmentTimeElementView: RsvpDetailsListElementView!
    private(set) var guestsInfoElementView: RsvpDetailsListElementView!
    private(set) var hostsListStackView: UIStackView!
    private(set) var guestsListInfoElementView: RsvpDetailsListElementView!
    private(set) var bottomButtonsStackView: UIStackView!
    private(set) var externalTicketView: UIView!
    private(set) var externalTicketIconImageView: UIImageView!
    private(set) var externalTicketInfoLabel: UILabel!
    private(set) var buyExternalTicketButton: RoundedButton!
    private(set) var cancelReservationButton: RoundedButton!
    private(set) var ticketAtDoorInformationView: InformationView!
    
    private(set) var topConstraint: NSLayoutConstraint!

    // MARK: - Initialization
    init(activation: Activation) {
        super.init(nibName: nil, bundle: nil)
        initialize(with: activation)
    }

    init(activationId id: Int) {
        super.init(nibName: nil, bundle: nil)
        initialize(with: id)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return currentStatusBarStyle
    }
    
    override var observableSources: ObservableSources? {
        return viewModel
    }
    
    override func observeViewModel() {
        super.observeViewModel()
        
        viewModel.activationObs
            .subscribe(onNext: { [unowned self] activation in
                self.setup(with: activation)
            }).disposed(by: disposeBag)
        
        viewModel.refreshingObs
            .filter { !$0 }
            .subscribe(onNext: { [unowned self] _ in
                self.refreshControl.endRefreshing()
            }).disposed(by: disposeBag)
        
        viewModel.locationClickObs
            .subscribe(onNext: { [unowned self] url in
                self.openMap(with: url)
            }).disposed(by: disposeBag)
    }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
        disableScrollInsets(for: mainScrollView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        mainScrollView.delegate = self
        adjustNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        changeNavigationBarAlphaWithAnimation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        mainScrollView.delegate = nil
        mainScrollView.stopScrolling()
    }

}

// MARK: - Private methods
extension RsvpDetailsViewController {
    
    private func initialize(with activation: Activation) {
        self.viewModel = RsvpDetailsViewModel(activation: activation)
    }
    
    private func initialize(with id: Int) {
        self.viewModel = RsvpDetailsViewModel(activationId: id)
    }
    
    private func setup(with activation: Activation) {
        rsvpImageView.getImage(from: activation.imageUrls?.imageUrl4x5)
        changeNavigationBarAlphaWithAnimation()
        dateLabel.text = activation.rsvp?.segment(basedOn: activation)?.startAt.toUpcomingRsvpsDate()
        timeLabel.text = activation.rsvp?.segment(basedOn: activation)?.startAt
            .toString(withFormat: Date.Format.activationDetails).uppercased()
        titleInfoElementView.setup(with: RsvpDetailsListElementModel(
            leftIcon: activation.type.iconDark,
            host: nil,
            text: activation.name,
            tapCallback: navigateToActivationDetails,
            isClickable: true,
            rightText: nil))
        locationInfoElementView.setup(with: RsvpDetailsListElementModel(
            leftIcon: Icons.RsvpDetails.locationDark,
            host: nil,
            text: activation.location?.name,
            tapCallback: navigateToMap,
            isClickable: true,
            rightText: nil))
        segmentTimeElementView.setup(with: RsvpDetailsListElementModel(
            leftIcon: Icons.RsvpDetails.time.scale(to: CGSize(width: 17, height: 17)),
            host: nil,
            text: String(
                format: Strings.MyRsvps.activationAndSegmentStartTime.localized,
                activation.rsvp?.segment(basedOn: activation)?.startAt.toString(withFormat: Date.Format.activationDetails).uppercased() ?? "",
                activation.startAt.toString(withFormat: Date.Format.activationDetails).uppercased()),
            tapCallback: timeSegmentsSelected,
            isClickable: true,
            rightText: nil))
        guestsInfoElementView.setup(with: RsvpDetailsListElementModel(
            leftIcon: Icons.RsvpDetails.hostDark,
            host: nil,
            text: activation.formattedJustMeText,
            tapCallback: guestInfoSelected,
            isClickable: true,
            rightText: nil))
        setupHostsList(hosts: activation.hosts, tapCallback: navigateToParlorHost)
        guestsListInfoElementView.setup(with: RsvpDetailsListElementModel(
            leftIcon: Icons.RsvpDetails.guestListDark,
            host: nil,
            text: Strings.MyRsvps.guestList.localized,
            tapCallback: navigateToGuestList,
            isClickable: activation.isGuestListAvailable,
            rightText: guestListRightLabelText(activation: activation)))
        guestsListInfoElementView.isHidden = activation.rsvp?.isWaiting ?? false
        lineViews[guestListLineViewIndex].isHidden = activation.rsvp?.isWaiting ?? false
        externalTicketView.isHidden = !activation.ticketExternal
        ticketAtDoorInformationView.isHidden = !activation.isPurchasedAtTheDoor
        view.layoutIfNeeded()
        
    }
    
    private func adjustNavigationBar() {
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.applyTransparentTheme(withColor: .white)
        statusBar?.backgroundColor = .clear
        setNeedsStatusBarAppearanceUpdate()
        guard self.presentingViewController != nil else { return }
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: Icons.NavigationBar.close, style: .plain, target: self, action: #selector(tapBackButton))
    }

    private func setupViews() {
        let builder = RsvpDetailsViewBuilder(controller: self)
        mainScrollView = builder.buildMainScrollView()
        scrollContentView = builder.buildView()
        imageShadowView = builder.buildView()
        rsvpImageView = builder.buildRsvpImageView()
        imageShadowView = builder.buildView()
        dateTimeStackView = builder.buildDateTimeStackView()
        dateLabel = builder.buildDateLabel()
        timeLabel = builder.buildTimeLabel()
        mainScrollView.delegate = self
        mainScrollView.refreshControl = refreshControl
        infoVerticalStackView = builder.buildInfoVerticalStackView()
        titleInfoElementView = builder.buildElementView()
        lineViews = builder.buildLineViews()
        locationInfoElementView = builder.buildElementView()
        segmentTimeElementView = builder.buildElementView()
        guestsInfoElementView = builder.buildElementView()
        hostsListStackView = builder.buildHostsListStackView()
        guestsListInfoElementView = builder.buildElementView()
        bottomButtonsStackView = builder.buildBottomButtonsStackView()
        externalTicketView = builder.buildExternalTicketView()
        externalTicketIconImageView = builder.buildExternalTicketIconImageView()
        externalTicketInfoLabel = builder.buildExternalTicketInfoLabel()
        buyExternalTicketButton = builder.buildBuyExternalTicketButton()
        cancelReservationButton = builder.buildCancelReservationButton()
        ticketAtDoorInformationView = builder.buildTicketAtDoorInformationView()

        builder.setupViews()
        topConstraint = mainScrollView.topAnchor.equal(to: view.topAnchor)
    }

    private func setupPresentationLogic() {
        buyExternalTicketButton.addTarget(self, action: #selector(didTapBuyExternalTickets), for: .touchUpInside)
        refreshControl.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        cancelReservationButton.addTarget(self, action: #selector(didTapCancelButton), for: .touchUpInside)
        bindDestinationObservable()
    }
    
    private func bindDestinationObservable() {
        viewModel.destinationObs
            .subscribe(onNext: { [unowned self] destination in
                self.handle(destination: destination)
            }).disposed(by: disposeBag)
    }
    
    private func handle(destination: RsvpDetailsDestination) {
        switch destination {
        case .activationDetails, .hostDetails, .mapProvider:
            router.push(destination: destination)
        case .guestList, .editRsvp, .waitlist, .externalTicketWebSite:
            router.present(destination: destination, withStyle: .fullScreen)
        case .back:
            navigationController?.popViewController(animated: true)
        }
    }
    
    private func setupHostsList(hosts: [ParlorHost]?, tapCallback: ((ParlorHost?) -> Void)?) {
        guard var hosts = hosts, !hosts.isEmpty else {
            hostsListStackView.isHidden = true
            lineViews[hostsListLineViewIndex].isHidden = true
            return
        }
        
        hostsListStackView.isHidden = false
        lineViews[hostsListLineViewIndex].isHidden = false
        hostsListStackView.removeArrangedSubviews()
        hosts.sort { $0.role.sortIndex < $1.role.sortIndex }
        for (index, host) in hosts.enumerated() {
            let item = RsvpDetailsListElementView()
            let icon = (index == 0) ? Icons.RsvpDetails.hostDark : nil
            item.setup(with: RsvpDetailsListElementModel(
                leftIcon: icon,
                host: host,
                text: host.formattedHostWithRole,
                tapCallback: tapCallback,
                isClickable: true,
                rightText: nil)
            )
            hostsListStackView.addArrangedSubview(item)
        }
    }
    
    private func guestListRightLabelText(activation: Activation) -> String? {
        switch activation.guestlistState {
        case .notAvailable, .none:
            return Strings.MyRsvps.notAvailable.localized
        case .comingSoon:
            return Strings.MyRsvps.comingSoon.localized
        case .available:
            return nil
        }
    }
    
    private func openBuyTicketWebsite(with url: URL) {
        UIApplication.shared.open(url)
    }
    
    private func openMap(with url: URL) {
        UIApplication.shared.open(url)
    }
    
    private func changeNavigationBarAlphaWithAnimation() {
           UIView.animate(withDuration: 0.2) { [unowned self] in
               if self.rsvpImageView.image != nil {
                   self.mainScrollView.changeNavigationBarAlpha(using: self.rsvpImageView, to: self.navigationController?.navigationBar)
               }
           }
       }
    
    @objc private func didPullToRefresh() {
        viewModel.didPullToRefresh()
    }
    
    @objc private func didTapBuyExternalTickets() {
        viewModel.navigateToBuyTicket()
    }
    
    @objc private func didTapCancelButton() {
        viewModel.cancelClicks.emitElement()
    }
    
    @objc func tapBackButton() {
        parent?.dismiss()
    }
}

extension RsvpDetailsViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < 0 {
            topConstraint.constant = -scrollView.contentOffset.y / 2
        } else {
            topConstraint.constant = 0
        }
        mainScrollView.changeNavigationBarAlpha(using: rsvpImageView, to: navigationController?.navigationBar)
    }
    
}

// MARK: - Tap callbacks for RsvpDetailsListElementView
extension RsvpDetailsViewController {
    
    private func navigateToActivationDetails(host: ParlorHost?) {
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.ParlorPassDetails.activationName.rawValue)
        viewModel.navigateToActivationDetails()
    }
    
    private func navigateToMap(host: ParlorHost?) {
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.ParlorPassDetails.locationName.rawValue)
        viewModel.navigateToMap()
    }
    
    private func navigateToParlorHost(host: ParlorHost?) {
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.ParlorPassDetails.host.rawValue)
        viewModel.navigateToParlorHost(host: host)
    }
    
    private func navigateToGuestList(host: ParlorHost?) {
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.ParlorPassDetails.guestList.rawValue)
        viewModel.navigateToGuestList()
    }
    
    private func timeSegmentsSelected(host: ParlorHost?) {
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.ParlorPassDetails.timeInformation.rawValue)
        viewModel.navigateToEditRsvp()
    }
    
    private func guestInfoSelected(hsot: ParlorHost?) {
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.ParlorPassDetails.guests.rawValue)
        viewModel.navigateToEditRsvp()
    }
}
