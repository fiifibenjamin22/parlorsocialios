//
//  RsvpDetailsViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 13/05/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CoreLocation

protocol RsvpDetailsLogic: BaseViewModelLogic {
    var activationObs: Observable<Activation> { get }
    var destinationObs: Observable<RsvpDetailsDestination> { get }
    var locationClickObs: Observable<URL> { get }
    var cancelClicks: PublishSubject<Void> { get }
    var refreshingObs: Observable<Bool> { get }
    
    func navigateToActivationDetails()
    func navigateToParlorHost(host: ParlorHost?)
    func navigateToBuyTicket()
    func navigateToMap()
    func navigateToGuestList()
    func navigateToEditRsvp()
    func didPullToRefresh()
}

class RsvpDetailsViewModel: BaseViewModel {
    
    // MARK: - Properties
    lazy var checkInService: CheckInService = {
        let checkInService = CheckInService(locationService: LocationService(), guestListRepository: guestListRepository, activationRepository: activationRepository)
        checkInService.delegate = self
        return checkInService
    }()

    let analyticEventReferencedId: Int?
    let cancelClicks = PublishSubject<Void>()

    private let activationRepository = ActivationsRepository.shared
    private let guestListRepository = GuestListRsvpRepository.shared
    private let activationRelay: BehaviorRelay<Activation>
    private let destinationRelay: PublishRelay<RsvpDetailsDestination> = PublishRelay()
    private let locationClickRelay: PublishRelay<URL> = PublishRelay()
    private let refreshingRelay = BehaviorRelay(value: false)
    
    init(activation: Activation) {
        self.analyticEventReferencedId = activation.id
        self.activationRelay = BehaviorRelay(value: activation)
        super.init()
        self.activationRelay.accept(activation)
        self.updateActivationDetails(withId: activation.id)
        self.initializeFlow()
    }
    
    init(activationId id: Int) {
        self.analyticEventReferencedId = id
        self.activationRelay = BehaviorRelay(value: Activation.empty(withId: id))
        super.init()
        self.updateActivationDetailsWithProgressBar(withId: id)
        self.initializeFlow()
    }
}

// MARK: Private logic
private extension RsvpDetailsViewModel {
    
    private func updateActivationDetails(withId id: Int) {
        activationRepository.getActivation(withId: id)
            .subscribe(onNext: { [weak self] response in
                self?.handleResponse(response)
            }).disposed(by: disposeBag)
    }
    
    private func updateActivationDetailsWithProgressBar(withId id: Int) {
        activationRepository.getActivation(withId: id).showingProgressBar(with: self)
            .subscribe(onNext: { [weak self] response in
                self?.handleResponse(response)
            }).disposed(by: disposeBag)
    }
    
    private func initializeFlow() {
        refreshingRelay
            .filter { $0 }
            .withLatestFrom(activationRelay)
            .flatMapFirst { [unowned self] activation in self.activationRepository.getActivation(withId: activation.id) }
            .subscribe(onNext: { [weak self] response in
                self?.handleResponse(response)
            }).disposed(by: disposeBag)
        
        activationRepository.newActivationDataObs
            .withLatestFrom(activationRelay) { ($0, $1) }
            .filter { $0.id == $1.id && $0 != $1 }
            .map { (new, _) in return new }
            .bind(to: activationRelay)
            .disposed(by: disposeBag)
        
        cancelClicks
            .withLatestFrom(activationRelay)
            .flatMapWithConfirmation(
                using: alertDataSubject,
                alertData: AlertData.confirmActivationCancel(isPayable: activationRelay.value.isPayable ?? false, sourceName: .activationCancelReservation)
            )
            .flatMap { [unowned self] activation in
                self.activationRepository
                    .cancelRsvp(forActivation: activation)
                    .showingProgressBar(with: self)
            }
            .doOnNext({ [unowned self] response in
                self.handleCancelResponse(response)
            })
            .subscribe()
            .disposed(by: disposeBag)
        
    }
    
    private func handleResponse(_ response: SingleActivationResposne) {
        refreshingRelay.accept(false)
        switch response {
        case .success(let resposne):
            activationRelay.accept(resposne.data)
        case .failure(let error):
            errorSubject.onNext(error)
        }
    }
    
    private func handleCancelResponse<Data>(_ response: ApiResponse<Data>) {
        switch response {
        case .success:
            destinationRelay.accept(.back)
        case .failure(let error):
            errorSubject.onNext(error)
        }
    }
    
}

// MARK: Interface logic methods
extension RsvpDetailsViewModel: RsvpDetailsLogic {
    var activationObs: Observable<Activation> {
        return activationRelay.asObservable()
    }
    
    var destinationObs: Observable<RsvpDetailsDestination> {
        return destinationRelay.asObservable()
    }
    
    var locationClickObs: Observable<URL> {
        return locationClickRelay.asObservable()
    }

    var refreshingObs: Observable<Bool> {
        return refreshingRelay.asObservable()
    }
    
    func navigateToActivationDetails() {
        let openingType = ActivationDetailsNavigationProvider.getOpeningActivationDetailsType(with: activationRelay.value)
        switch openingType {
        case .activationDetailsVC, .addProfilePhotoVC, .upgradeToPremiumForStandard:
            self.destinationRelay.accept(.activationDetails(activation: activationRelay.value, openingType: openingType, navigationSource: .parlorPassDetails))
        case .startedAlert:
            self.alertDataSubject.onNext(
                AlertData.fromSimpleMessage(message: Strings.Activation.startedAlert.localized, sourceName: AnalyticEventAlertSourceName.activationHasAlreadyStarted)
            )
        }
    }
    
    func navigateToParlorHost(host: ParlorHost?) {
        guard let host = host else {
            fatalError("You try open HostDetails but host is nil")
        }
        destinationRelay.accept(.hostDetails(host: host))
    }
    
    func navigateToBuyTicket() {
        guard let url = activationRelay.value.ticketURL else { return }
        let data = BaseWebViewData(navBarTitle: nil, url: url)
        destinationRelay.accept(.externalTicketWebSite(webViewData: data))
    }
    
    func navigateToMap() {
        guard let lat = activationRelay.value.location?.lat,
            let lng = activationRelay.value.location?.lng,
            let address = activationRelay.value.location?.mapLocation else { return }
        guard let mapUrl = RouteMapService.getMapUrl(lat: lat, lng: lng, address: address) else { destinationRelay.accept(.mapProvider); return }
        locationClickRelay.accept(mapUrl)
    }
    
    func navigateToGuestList() {
        let activation = activationRelay.value
        checkInService.navigateToGuestList(activationId: activation.id, checkInStrategy: activation.checkInStrategy)
    }
    
    func navigateToEditRsvp() {
        guard let isWaiting = activationRelay.value.rsvp?.isWaiting else { return }
        if isWaiting {
            destinationRelay.accept(.waitlist(activation: activationRelay.value))
        } else {
            destinationRelay.accept(.editRsvp(
                activation: activationRelay.value,
                guestCount: activationRelay.value.rsvp?.guests.count))
        }
    }

    func didPullToRefresh() {
        refreshingRelay.accept(true)
    }
}

// MARK: - Check-in service
extension RsvpDetailsViewModel: CheckInServiceDelegate {
    func didEndCheckInSuccess(_ checkInService: CheckInService, activationId: Int) {
        progressBarSubject.onNext(false)
        destinationRelay.accept(.guestList(activationId: activationId))
    }
}
