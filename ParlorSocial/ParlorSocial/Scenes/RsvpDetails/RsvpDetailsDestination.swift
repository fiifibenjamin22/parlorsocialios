//
//  RsvpDetailsDestination.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 16/05/2019.
//

import UIKit

enum RsvpDetailsDestination {
    case activationDetails(activation: Activation, openingType: OpeningActivationDetailsType, navigationSource: ActivationDetailsNavigationSource)
    case hostDetails(host: ParlorHost)
    case guestList(activationId: Int)
    case editRsvp(activation: Activation, guestCount: Int?)
    case mapProvider
    case waitlist(activation: Activation)
    case back
    case externalTicketWebSite(webViewData: BaseWebViewData)
}

extension RsvpDetailsDestination: Destination {
    
    var viewController: UIViewController {
        switch self {
        case .activationDetails(let activation, let openingType, let navigationSource):
            return ActivationDetailsNavigationProvider.getVCByAvailabilityOfOpeningActivationDetails(
                forActivation: activation, openingType: openingType, navigationSource: navigationSource)
        case .hostDetails(let host):
            return HostDetailsViewController(withHost: host)
        case .guestList(let activationId):
            return GuestListRsvpViewController(activationId: activationId).embedInNavigationController()
        case .editRsvp(activation: let activation, let guestCount):
            return GuestListFormViewController(forActivation: activation, andGuestsCount: guestCount)
                .embedInNavigationController()
        case .mapProvider:
            return MapProviderViewController()
        case .waitlist(let activation):
            return WaitlistInfoViewController(withActivation: activation).embedInNavigationController()
        case .back:
            return UIViewController()
        case .externalTicketWebSite(let data):
            return BaseWebViewController(data: data).embedInNavigationController()
        }
    }
    
}
