//
//  LoginRouter.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 03/04/2019.
//

import Foundation
import UIKit
import SafariServices

enum LoginDestination {
    case dashboard
    case payment
    case resetPassword
    case register(signInURL: URL)
    case membershipPlans
    case userNotApproved
}

extension LoginDestination: Destination {

    var viewController: UIViewController {
        switch self {
        case .dashboard:
            return AppTabBarController().embedInNavigationController()
        case .payment:
            return PremiumPaymentViewController()
        case .resetPassword:
            return ResetPasswordViewController()
        case .register(let signInURL):
            return SFSafariViewController(url: signInURL)
        case .membershipPlans:
            return MembershipPlansViewController()
        case .userNotApproved:
            return UserNotApprovedViewController()
        }
    }
    
    static func getType(baseOn profileDestinationAfterLogin: DestinationAfterLogin) -> Self {
        switch profileDestinationAfterLogin {
        case .dashboard: return dashboard
        case .payment: return payment
        case .selectMembership: return membershipPlans
        case .userNotApproved: return userNotApproved
        }
    }

}
