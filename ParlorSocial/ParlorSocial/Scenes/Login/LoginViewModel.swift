//
//  ActivationsViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 27.02.2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol LoginLogic: BaseViewModelLogic {
    var emailRelay: BehaviorRelay<String> { get }
    var passwordRelay: BehaviorRelay<String> { get }
    var loginClicks: PublishRelay<Void> { get }
    var destinationsObs: Observable<LoginDestination> { get }
    var errorObs: Observable<AppError> { get }
    var clickForgotPasswordSubject: PublishSubject<Void> { get }
    var clickBecomeMemberSubject: PublishSubject<Void> { get }
    func initFlow()
    func performLogoutClear()
}

final class LoginViewModel: BaseViewModel {
    let analyticEventReferencedId: Int? = nil
    private let validator = Validator()
    private let authRepository = AuthRepository.shared
    private let profileRespository = ProfileRepository.shared
    private let deepLinkUrlParametersRelay = BehaviorRelay<String?>(value: nil)
    private let errorCodeRelay = BehaviorRelay<Int?>(value: nil)
    private let loginValidationHelper = LoginValidationHelper()
    private let incrementAppEntryCountHelper = IncrementAppEntryCountHelper()
    private let notificationsAnalyticService = NotificationsAnalyticService()
    
    let emailRelay: BehaviorRelay<String> = BehaviorRelay(value: "")
    let passwordRelay: BehaviorRelay<String> = BehaviorRelay(value: "")
    let loginClicks: PublishRelay<Void> = PublishRelay()
    let destinationsRelay: PublishRelay<LoginDestination> = PublishRelay()
    let clickForgotPasswordSubject: PublishSubject<Void> = .init()
    let clickBecomeMemberSubject: PublishSubject<Void> = .init()
    
    override init() {
        super.init()
    }
    
    init(forUrlParameters parameters: String) {
        super.init()
        deepLinkUrlParametersRelay.accept(parameters)
    }
    
    init(with errorCode: Int) {
        super.init()
        errorCodeRelay.accept(errorCode)
    }
}

// MARK: Private logic
private extension LoginViewModel {
    
    func tryToFetchUserData() {
        profileRespository.getMyProfileData()
            .showingProgressBar(with: self)
            .doOnNext { [weak self] response in
                self?.handleProfileResponse(response)
            }.subscribe().disposed(by: disposeBag)
    }
    
}

// MARK: Interface logic methods
extension LoginViewModel: LoginLogic {
    
    var destinationsObs: Observable<LoginDestination> {
        return destinationsRelay.asObservable()
    }
    
    func initFlow() {
        loginClicks
            .withLatestFrom(emailRelay)
            .withLatestFrom(passwordRelay) { ($0, $1) }
            .subscribe(onNext: { [weak self] response in
                self?.loginValidation(login: response.0, password: response.1)
            }).disposed(by: disposeBag)
        
        deepLinkUrlParametersRelay
            .ignoreNil()
            .flatMapFirst { [unowned self] data in
                self.authRepository.changeEmail(using: data).showingProgressBar(with: self)
            }.subscribe(onNext: { [unowned self] response in
                self.progressBarSubject.onNext(false)
                self.handleChangeEmailResponse(response)
            }).disposed(by: disposeBag)
        
        clickForgotPasswordSubject
            .subscribe(onNext: { [unowned self] _ in
                self.destinationsRelay.accept(.resetPassword)
            }).disposed(by: disposeBag)
        
        clickBecomeMemberSubject
            .subscribe(onNext: { [unowned self] _ in
                let url = self.getSignInURL()
                self.destinationsRelay.accept(.register(signInURL: url))
            }).disposed(by: disposeBag)
        
        errorCodeRelay
            .ignoreNil()
            .subscribe(onNext: { [unowned self] code in
                self.handleErrorCode(code)
            }).disposed(by: disposeBag)
    }
    
    func performLogoutClear() {
        guard Config.userProfile != nil else { return }
        GlobalNotificationsService.shared.unregisterDeviceToken { _ in Config.performLogoutClear() }
    }
}

// MARK: - Private methods
extension LoginViewModel {
    
    private func handleLoginResponse(_ response: ApiResponse<TokenData>) {
        switch response {
        case .success(let token):
            Config.updateTokenData(withAccessToken: token.accessToken, refreshToken: token.refreshToken)
            tryToFetchUserData()
        case .failure(let error):
            errorSubject.onNext(error.setErrorType(errorType: .login))
        }
    }
    
    private func handleProfileResponse(_ response: ProfileResponse) {
        switch response {
        case .success(let response):
            notificationsAnalyticService.saveNotificationsSettingsChangeAnalyticEvent(withCheckingIfChanged: false, source: .manual)
            incrementAppEntryCountHelper.incrementAppEntryCounters()
            CalendarUtilities.shared.checkIfPermissionsAreChangedInDeviceSetting(settingsChangeSource: .manual)
            navigate(to: response.data.destinationAfterLogin)
        case .failure(let error):
            Config.performLogoutClear()
            errorSubject.onNext(error)
        }
    }
    
    private func handleChangeEmailResponse(_ response: ApiResponse<MessageResponse>) {
        switch response {
        case .success(let data):
            alertDataSubject.onNext(
                AlertData(
                    icon: Icons.ChangeEmail.changeEmailSuccess,
                    title: data.message,
                    message: nil,
                    bottomButtonTitle: Strings.ok.localized.uppercased()
                )
            )
        case .failure(let error):
            errorSubject.onNext(error.setErrorType(errorType: .changeEmail))
        }
    }
    
    private func sendLoginRequest(login: String, password: String) {
        authRepository.login(using: LoginRequest.usingPassword(username: login, password: password))
            .showingProgressBar(with: self)
            .subscribe(onNext: { [weak self] response in
                self?.handleLoginResponse(response)
            }).disposed(by: disposeBag)
    }
    
    private func navigate(to destination: DestinationAfterLogin?) {
        guard let strongDestination = destination else { return }
        destinationsRelay.accept(LoginDestination.getType(baseOn: strongDestination))
    }
    
    private func handleErrorCode(_ code: Int) {
        if code == Config.unauthorizedCode {
            alertDataSubject.onNext(AlertData.fromSimpleMessage(
                message: Strings.Login.unauthorizedErrorMessage.localized,
                withIcon: Icons.Error.general,
                action: {}))
        } else if code == Config.paymentRequiredCode {
            alertDataSubject.onNext(AlertData.fromSimpleMessage(
                message: Strings.Login.paymentRequiredErrorMessage.localized,
                withIcon: Icons.Error.general,
                action: {}))
        }
    }
    
    private func loginValidation(login: String, password: String) {
        let validationResult = loginValidationHelper.validate(login: login, password: password, validator: validator)
        switch validationResult {
        case .success:
            sendLoginRequest(login: login, password: password)
        case .failure(let error):
            errorSubject.onNext(error.setErrorType(errorType: .login))
        }
    }
    
    private func getSignInURL() -> URL {
        guard let stringUrl = Config.globalSettings?.referralLinkIos, let url = URL(string: stringUrl) else {
            return Config.signInURL
        }
        return url
    }
}
