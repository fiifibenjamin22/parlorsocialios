//
//  LoginViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 29/03/2019.
//

import Foundation
import UIKit

class LoginViewBuilder {
    
    private unowned let controller: LoginViewController
    private var view: UIView! { return controller.view }
    
    init(controller: LoginViewController) {
        self.controller = controller
    }
    
    func setupViews() {
        setupHierarchy()
        setupAutoLayout()
        setupProperties()
    }
    
    private func setupHierarchy() {
        view.addSubviews([
            controller.backgroundImageView,
            controller.scrollView
            ])
        
        controller.apply {
            $0.scrollView.addSubview($0.contentScrollView)
            
            $0.contentScrollView.addSubviews([
                controller.logoImageView,
                controller.emailTextField,
                controller.passwordTextField,
                controller.loginButton,
                controller.becomeMemberButton,
                controller.forgotPasswordButton
                ])
        }
    }
    
    private func setupProperties() {
        view.backgroundColor = .white
        controller.apply { it in
            it.becomeMemberButton.setAttributedTitle(NSAttributedString(string: Strings.Login.becomeMember.localized, attributes: [
                .underlineStyle: NSUnderlineStyle.single.rawValue,
                .foregroundColor: UIColor.Login.becomeParlorMember,
                .font: UIFont.custom(ofSize: Constants.Login.buttonFontSize, font: UIFont.Roboto.regular)
                ]), for: .normal)
            
            it.emailTextField.attributedPlaceholder = NSAttributedString(
                string: Strings.Login.email.localized,
                attributes: [
                    NSAttributedString.Key.foregroundColor: UIColor.appTextWhite,
                    NSAttributedString.Key.font: UIFont.custom(ofSize: Constants.FontSize.subheading, font: UIFont.Roboto.light)
                ])
            it.passwordTextField.attributedPlaceholder = NSAttributedString(
                string: Strings.Login.password.localized,
                attributes: [
                    NSAttributedString.Key.foregroundColor: UIColor.appTextWhite,
                    NSAttributedString.Key.font: UIFont.custom(ofSize: Constants.FontSize.subheading, font: UIFont.Roboto.light)
                ])
            it.loginButton.setTitle(Strings.Login.login.localized.uppercased(), for: .normal)
            it.forgotPasswordButton.setTitle(Strings.Login.forgotPassword.localized, for: .normal)
        }
    }
    
    private func setupAutoLayout() {
        controller.apply { it in
            it.scrollView.edges(equalTo: view)
            
            it.contentScrollView.edges(equalTo: it.scrollView)
            it.contentScrollView.widthAnchor.equal(to: it.scrollView.widthAnchor)
            it.contentScrollView.heightAnchor.equal(to: it.scrollView.heightAnchor, withPriority: .defaultLow)
            
            it.backgroundImageView.edges(equalTo: view)
            
            it.logoImageView.centerXAnchor.equal(to: it.contentScrollView.centerXAnchor)
            it.logoImageView.topAnchor.equal(to: it.contentScrollView.safeAreaLayoutGuide.topAnchor, constant: Constants.Login.topMargin)
            
            it.emailTextField.centerXAnchor.equal(to: it.contentScrollView.centerXAnchor)
            it.emailTextField.heightAnchor.equalTo(constant: Constants.Login.textFieldHeight)
            addViewHorizontalMarginsConstraints(it.emailTextField)
            
            it.passwordTextField.topAnchor.equal(to: it.emailTextField.bottomAnchor, constant: Constants.Login.textFieldSpace)
            it.passwordTextField.centerXAnchor.equal(to: it.contentScrollView.centerXAnchor)
            it.passwordTextField.heightAnchor.equalTo(constant: Constants.Login.textFieldHeight)
            addViewHorizontalMarginsConstraints(controller.passwordTextField)
            
            it.forgotPasswordButton.trailingAnchor.equal(to: it.passwordTextField.trailingAnchor)
            it.forgotPasswordButton.topAnchor.equal(to: it.passwordTextField.bottomAnchor, constant: Constants.Login.verticalMarginForgot)
            it.forgotPasswordButton.bottomAnchor.equal(to: it.loginButton.topAnchor, constant: -Constants.Login.loginButtonTopMargin)
            
            it.loginButton.heightAnchor.equalTo(constant: Constants.Login.loginButtonHeight)
            it.loginButton.bottomAnchor.equal(to: it.becomeMemberButton.topAnchor, constant: -Constants.Login.horizontalMargin)
            it.loginButton.centerXAnchor.equal(to: it.contentScrollView.centerXAnchor)
            addViewHorizontalMarginsConstraints(controller.loginButton)
            
            it.becomeMemberButton.centerXAnchor.equal(to: it.contentScrollView.centerXAnchor)
            it.becomeMemberButton.bottomAnchor.equal(to: it.contentScrollView.safeAreaLayoutGuide.bottomAnchor, constant: -Constants.Login.bottomMargin)
        }
    }
    
    private func addViewHorizontalMarginsConstraints(_ childView: UIView) {
        childView.leadingAnchor.equal(to: view.leadingAnchor, constant: Constants.Login.horizontalMargin).withPriority(.defaultHigh)
        childView.trailingAnchor.equal(to: self.view.trailingAnchor, constant: -Constants.Login.horizontalMargin).withPriority(.defaultHigh)
    }
}

extension LoginViewBuilder {
    
    func buildScrollView() -> UIScrollView {
        return UIScrollView().manualLayoutable()
    }
    
    func buildView() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildPasswordTextField() -> LoginTextField {
        return LoginTextField().manualLayoutable().apply {
            $0.isPasswordField = true
            $0.textColor = .appTextWhite
            $0.font = UIFont.custom(ofSize: Constants.FontSize.subtitle, font: UIFont.Roboto.regular)
            $0.addBottomBorder(withColor: .textFieldUnderline)
        }
    }
    
    func buildEmailTextField() -> LoginTextField {
        return LoginTextField().manualLayoutable().apply { it in
            it.keyboardType = .emailAddress
            it.autocapitalizationType = .none
            it.autocorrectionType = .no
            it.textColor = .appTextWhite
            it.font = UIFont.custom(ofSize: Constants.FontSize.subtitle, font: UIFont.Roboto.regular)
            it.addBottomBorder(withColor: .textFieldUnderline)
        }
    }
    
    func buildBackgroundImageView() -> UIImageView {
        return UIImageView(image: Icons.Login.background).manualLayoutable().apply {
            $0.contentMode = .scaleAspectFill
        }
    }
    
    func buildLogoImageView() -> UIImageView {
        return UIImageView(image: Icons.Login.loginLogo).manualLayoutable()
    }
    
    func buildBecomeMemberButton() -> UIButton {
        return UIButton().manualLayoutable().apply {
            $0.applyTouchAnimation()
        }
    }
    
    func buildForgotPasswordButton() -> UIButton {
        return UIButton().manualLayoutable().apply { it in
            it.applyTouchAnimation()
            it.setTitleColor(UIColor.Login.forgotPasswordColor, for: .normal)
            it.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.xTiny, font: UIFont.Roboto.regular)
        }
    }
    
    func buildLoginButton() -> RoundedButton {
        return RoundedButton().manualLayoutable().apply { it in
            it.backgroundColor = UIColor.white
            it.setTitleColor(UIColor.appText, for: .normal)
            it.titleLabel?.font = UIFont.custom(ofSize: Constants.Login.buttonFontSize, font: UIFont.Roboto.medium)
            it.applyTouchAnimation()
        }
    }
}

private extension Constants {
    
    enum Login {
        static let horizontalMargin: CGFloat = 35
        static let textFieldHeight: CGFloat = 55
        static let loginButtonHeight: CGFloat = 55
        static let topMargin: CGFloat = 50
        static let buttonFontSize: CGFloat = 12
        static let bottomMargin: CGFloat = 40
        static let verticalMarginForgot: CGFloat = 20
        static let textFieldSpace: CGFloat = 10
        static let loginButtonTopMargin: CGFloat = 48
    }
}

private extension UIColor {
    
    enum Login {
        static let becomeParlorMember = UIColor.init(red: 228/255, green: 228/255, blue: 228/255, alpha: 1)
        static let forgotPasswordColor = UIColor.init(red: 204/255, green: 204/255, blue: 204/255, alpha: 1)
    }
}
