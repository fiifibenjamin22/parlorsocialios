//
//  LoginValidationHelper.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 04/02/2020.
//

import Foundation

class LoginValidationHelper {
    
    // MARK: - Execution
    func validate(login: String, password: String, validator: Validator) -> Validator.Result {
        let loginValidatedInput = ValidatedInput(rules: [StringNotBlank()], value: login, name: Strings.Login.email.localized)
        let passwordValidatedInput = ValidatedInput(rules: [StringNotBlank()], value: password, name: Strings.Login.password.localized)
        
        return validator.validateWithResult(for: [loginValidatedInput, passwordValidatedInput])
    }
    
}
