//
//  LoginViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 29/03/2019.
//

import UIKit
import RxSwift
import RxCocoa

class LoginViewController: AppViewController {
    
    // MARK: - Properties
    private var viewModel: LoginLogic!
    let analyticEventScreen: AnalyticEventScreen? = nil
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>?
    
    override var observableSources: ObservableSources? {
        return viewModel
    }

    // MARK: - Views
    private(set) var scrollView: UIScrollView!
    private(set) var contentScrollView: UIView!
    private(set) var backgroundImageView: UIImageView!
    private(set) var logoImageView: UIImageView!
    private(set) var emailTextField: LoginTextField!
    private(set) var passwordTextField: LoginTextField!
    private(set) var loginButton: UIButton!
    private(set) var becomeMemberButton: UIButton!
    private(set) var forgotPasswordButton: UIButton!
    
    private lazy var textFields: [UITextField] = {
        return [emailTextField, passwordTextField]
    }()
    
    // MARK: - Initialization
    init() {
        super.init(nibName: nil, bundle: nil)
        setup()
    }
    
    init(forUrlParameters parameters: String) {
        super.init(nibName: nil, bundle: nil)
        setup(forUrlParameters: parameters)
    }
    
    init(with errorCode: Int) {
        super.init(nibName: nil, bundle: nil)
        setup(with: errorCode)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        viewModel = LoginViewModel()
    }
    
    private func setup(forUrlParameters parameters: String) {
        viewModel = LoginViewModel(forUrlParameters: parameters)
    }
    
    private func setup(with errorCode: Int) {
        viewModel = LoginViewModel(with: errorCode)
    }
    
    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        clearTextFields()
        adjustNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.performLogoutClear()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func adjustNavigationBar() {
        navigationController?.setNavigationBarHidden(true, animated: false)
        statusBar?.backgroundColor = .clear
    }
    
    override func observeViewModel() {
        observableSources?.alertDataObs.show(using: self).disposed(by: disposeBag)
        observableSources?.errorObs.handleWithAlerts(using: self).disposed(by: disposeBag)
        observableSources?.progressBarObs.showProgressBar(using: self, isDark: true).disposed(by: disposeBag)
        
        viewModel
            .destinationsObs
            .subscribe(onNext: { [weak self] destination in
                self?.handleDestination(with: destination)
            }).disposed(by: disposeBag)

    }
    
    override func bindUI() {
        super.bindUI()
        
        emailTextField.rx
            .text
            .emptyOnNil()
            .bind(to: viewModel.emailRelay)
            .disposed(by: disposeBag)
        
        passwordTextField.rx
            .text
            .emptyOnNil()
            .bind(to: viewModel.passwordRelay)
            .disposed(by: disposeBag)
        
        loginButton.rx
            .tap
            .bind(to: viewModel.loginClicks)
            .disposed(by: disposeBag)
        
        forgotPasswordButton.rx
            .tap
            .bind(to: viewModel.clickForgotPasswordSubject)
            .disposed(by: disposeBag)
        
        becomeMemberButton.rx
            .tap
            .bind(to: viewModel.clickBecomeMemberSubject)
            .disposed(by: disposeBag)
        
    }
}

// MARK: - Private methods
extension LoginViewController {
    
    private func setupViews() {
        let builder = LoginViewBuilder(controller: self)
        scrollView = builder.buildScrollView()
        contentScrollView = builder.buildView()
        backgroundImageView = builder.buildBackgroundImageView()
        emailTextField = builder.buildEmailTextField()
        passwordTextField = builder.buildPasswordTextField()
        logoImageView = builder.buildLogoImageView()
        becomeMemberButton = builder.buildBecomeMemberButton()
        loginButton = builder.buildLoginButton()
        forgotPasswordButton = builder.buildForgotPasswordButton()
        builder.setupViews()
    }
    
    private func setupPresentationLogic() {
        viewModel.initFlow()
    }
    
    private func handleDestination(with destination: LoginDestination) {
        switch destination {
        case .dashboard, .userNotApproved:
            router.replace(with: destination)
        case .payment, .resetPassword, .membershipPlans:
            router.push(destination: destination)
        case .register:
            router.present(destination: destination)
        }
    }
    
    private func clearTextFields() {
        textFields.forEach {
            $0.text = ""
            $0.sendActions(for: .valueChanged)
        }
    }
    
}
