//
//  RsvpConfirmedViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 05/09/2019.
//

import UIKit
import RxSwift

class RsvpConfirmedViewController: AppViewController {
    
    // MARK: - Views
    private(set) var iconedMessageView: FullScreenAlertIconedMessageView!
    private(set) var bottomButton: UIButton!
    
    // MARK: - Properties
    let analyticEventScreen: AnalyticEventScreen? = .rsvpConfirmed
    let viewModel: BaseViewModelLogic!
    private let notificationsReminderProvider = EnableNotificationsReminderViewProvider()
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }
        
    // MARK: - Initialization
    init() {
        self.viewModel = BaseViewModelClass()
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        adjustNavigationBar()
    }
    
}

// MARK: - Private methods
private extension RsvpConfirmedViewController {
    
    private func setupViews() {
        let builder = RsvpConfirmedViewBuilder(controller: self)
        iconedMessageView = builder.buildIconedMessageView()
        bottomButton = builder.buildBottomButton()
        builder.setupViews()
    }
    
    private func adjustNavigationBar() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: Icons.NavigationBar.close,
            style: .plain,
            target: self,
            action: #selector(didTapCloseButton)
            ).apply {
            $0.tintColor = UIColor.closeTintColor
        }
        statusBar?.backgroundColor = .white
        navigationController?.applyProfileItemAppearance(withTitle: "")
        navigationController?.setNavigationBarBorderColor()
        setNeedsStatusBarAppearanceUpdate()
    }
    
    private func setupPresentationLogic() {
        bottomButton.addTarget(self, action: #selector(didTapBottomButton), for: .touchUpInside)
    }
    
    @objc private func didTapCloseButton() {
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.RsvpConfirmed.close.rawValue)
        showEnableNotificationsSettingsIfNeeded()
    }
    
    @objc private func didTapBottomButton() {
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.RsvpConfirmed.done.rawValue)
        showEnableNotificationsSettingsIfNeeded()
    }
    
    private func showEnableNotificationsSettingsIfNeeded() {
        notificationsReminderProvider.showNotificationsReminder(when: .afterFirstRsvp) { [weak self] shouldShow, notificationsSettingsChangeSource in
            DispatchQueue.main.async { [weak self] in
                guard shouldShow else {
                    self?.dismiss(animated: true, completion: nil)
                    return
                }
                let controller = EnableNotificationsReminderViewController(notificationsSettingsChangeSource: notificationsSettingsChangeSource)
                self?.navigationController?.setViewControllers([controller], animated: true)
            }
        }
    }
    
}
