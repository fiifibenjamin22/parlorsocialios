//
//  RsvpConfirmedViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 05/09/2019.
//

import UIKit

final class RsvpConfirmedViewBuilder {
    private unowned let controller: RsvpConfirmedViewController
    private var view: UIView! { return controller.view }
    
    init(controller: RsvpConfirmedViewController) {
        self.controller = controller
    }
    
    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension RsvpConfirmedViewBuilder {
    
    private func setupProperties() {
        view.apply {
            $0.clipsToBounds = true
            $0.backgroundColor = .white
        }
        controller.apply {
            $0.bottomButton.setTitle(Strings.cool.localized.uppercased(), for: .normal)
        }
    }
    
    private func setupHierarchy() {
        controller.apply {
            view.addSubviews([$0.iconedMessageView, $0.bottomButton])
        }
    }
    
    private func setupAutoLayout() {
        controller.apply {
            
            $0.iconedMessageView.apply {
                $0.centerXAnchor.equal(to: view.centerXAnchor)
                $0.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 281/375).activate()
                $0.centerYAnchor.equal(to: view.centerYAnchor, constant: -77)
            }
            
            $0.bottomButton.apply {
                $0.edgesToParent(anchors: [.leading, .bottom, .trailing])
                $0.heightAnchor.equalTo(constant: 72)
            }
            
        }
    }
    
}

// MARK: - Public build methods
extension RsvpConfirmedViewBuilder {
    
    func buildIconedMessageView() -> FullScreenAlertIconedMessageView {
        return FullScreenAlertIconedMessageView(
            icon: Icons.Common.checkedBig,
            title: Strings.ActivationDetails.reservationConfirmed.localized,
            message: Strings.ActivationDetails.visitBottomNavigation.localized
        ).manualLayoutable()
    }
    
    func buildBottomButton() -> UIButton {
        let button = UIButton().manualLayoutable()
        button.backgroundColor = .white
        button.setTitleColor(.appText, for: .normal)
        button.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.medium)
        button.addShadow(withOpacity: 0.1, radius: 10)
        button.applyTouchAnimation()
        
        return button
    }
}

