//
//  AvailableGuestListPopupDestination.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 04/06/2019.
//

import UIKit

enum AvailableGuestListPopupDestination {
    case guestList(activationId: Int, presentWithAnimation: Bool)
}

extension AvailableGuestListPopupDestination: Destination {
    
    var viewController: UIViewController {
        switch self {
        case .guestList(let activationId, _):
            return GuestListRsvpViewController(activationId: activationId).embedInNavigationController()
        }
    }
}

