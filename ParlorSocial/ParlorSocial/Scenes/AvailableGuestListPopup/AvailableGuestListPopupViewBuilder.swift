//
//  AvailableGuestListPopupViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 04/06/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class AvailableGuestListPopupViewBuilder {

    private unowned let controller: AvailableGuestListPopupViewController
    private var view: UIView! { return controller.view }

    init(controller: AvailableGuestListPopupViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension AvailableGuestListPopupViewBuilder {

    private func setupProperties() {
        view.clipsToBounds = true
        view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        controller.apply {
            $0.contentView.roundCorners(with: 20)
            $0.activationImageView.apply {
                $0.contentMode = .scaleAspectFill
                $0.clipsToBounds = true
                $0.alpha = 0
            }
            $0.darkAlphaView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            $0.darkAlphaView.clipsToBounds = true
            $0.headerTextLabel.text = Strings.parlorSymbol.rawValue
            $0.headerLeftLineView.backgroundColor = .white
            $0.headerRightLineView.backgroundColor = .white
            $0.activationStartAtLabel.text = Strings.AvailableGuestListPopup.guestListAvailable.localized.uppercased()
            $0.viewGuestListButton.setTitle(Strings.AvailableGuestListPopup.viewGuestList.localized.uppercased(), for: .normal)
            $0.activationNameLabel.alpha = 0
        }
    }

    private func setupHierarchy() {
        controller.apply {
            view.addSubview($0.contentView)
            
            $0.contentView.addSubviews([
                $0.activationImageView,
                $0.darkAlphaView,
                $0.headerStackView,
                $0.closeButton,
                $0.activationNameLabel,
                $0.activationStartAtLabel,
                $0.viewGuestListButton])
            
            $0.headerStackView.addArrangedSubviews([
                $0.headerLeftLineView,
                $0.headerTextLabel,
                $0.headerRightLineView])
        }
    }

    private func setupAutoLayout() {
        controller.contentView.apply {
            $0.edgesToParent(anchors: [.leading, .trailing], insets: UIEdgeInsets.margins(left: 30, right: 30))
            $0.heightAnchor.equalTo(constant: 420)
            $0.centerYAnchor.equal(to: view.centerYAnchor, constant: -10)
        }
        
        controller.activationImageView.apply {
            $0.edgesToParent()
        }
        
        controller.darkAlphaView.edgesToParent()
        
        controller.headerStackView.apply {
            $0.edgesToParent(anchors: [.top], insets: UIEdgeInsets.margins(top: 18))
            $0.centerXAnchor.equal(to: controller.contentView.centerXAnchor)
        }
        
        controller.headerLeftLineView.apply {
            $0.heightAnchor.equalTo(constant: 1)
            $0.widthAnchor.equalTo(constant: 16)
        }
        
        controller.headerRightLineView.apply {
            $0.heightAnchor.equalTo(constant: 1)
            $0.widthAnchor.equalTo(constant: 16)
        }
        
        controller.closeButton.apply {
            $0.edgesToParent(anchors: [.top, .trailing])
        }
        
        controller.activationStartAtLabel.apply {
            $0.edgesToParent(anchors: [.leading, .trailing], insets: UIEdgeInsets.margins(
                left: Constants.AvailableGuestListPopup.cellContentMargin,
                right: Constants.AvailableGuestListPopup.cellContentMargin))
            $0.topAnchor.equal(to: controller.headerStackView.bottomAnchor, constant: 56)
        }
        
        controller.activationNameLabel.apply {
            $0.edgesToParent(anchors: [.leading, .trailing], insets: UIEdgeInsets.margins(
                left: Constants.AvailableGuestListPopup.cellContentMargin,
                right: Constants.AvailableGuestListPopup.cellContentMargin))
            $0.topAnchor.equal(to: controller.activationStartAtLabel.bottomAnchor, constant: 33)
        }
        
        controller.viewGuestListButton.apply {
            $0.edgesToParent(anchors: [.bottom], insets: UIEdgeInsets.margins(bottom: 27))
            $0.heightAnchor.equalTo(constant: 50)
            $0.widthAnchor.equalTo(constant: 196)
            $0.centerXAnchor.equal(to: controller.contentView.centerXAnchor)
            $0.topAnchor.greaterThanOrEqual(to: controller.activationNameLabel.bottomAnchor, constant: -30)
        }
    }

}

// MARK: - Public build methods
extension AvailableGuestListPopupViewBuilder {

    func buildView() -> UIView {
        return UIView()
    }
    
    func buildContentView() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildActivationImageView() -> UIImageView {
        return UIImageView().manualLayoutable()
    }
    
    func buildDarkAlphaView() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildHeaderStackView() -> UIStackView {
        return UIStackView(axis: .horizontal, spacing: 2, alignment: .center, distribution: .fillEqually)
    }
    
    func buildHeaderLeftLineView() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildHeaderTextLabel() -> UILabel {
        return UILabel.styled(textColor: .white,
                              withFont: UIFont.custom(
                                ofSize: Constants.FontSize.heading,
                                font: UIFont.Editor.medium),
                              alignment: .center)
    }
    
    func buildHeaderRightLineView() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildCloseButton() -> UIButton {
        return UIButton().manualLayoutable().apply {
            let inset = Constants.Margin.big
            $0.contentEdgeInsets = UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
            $0.tintColor = .closeTintColor
            $0.applyTouchAnimation()
            $0.setImage(Icons.AvailableGuestList.closeButton, for: .normal)
        }
    }
    
    func buildActivationNameLabel() -> ParlorLabel {
        return ParlorLabel.styled(
            withTextColor: .white,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.smallTitle,
                font: UIFont.Editor.bold),
            alignment: .center).apply {
                $0.lineHeight = 37
        }
    }
    
    func buildActivationStartAtLabel() -> UILabel {
        return UILabel.styled(
            textColor: .white,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.hintSize,
                font: UIFont.Roboto.bold),
            alignment: .center)
    }
    
    func buildViewGuestListButton() -> RoundedButton {
        return RoundedButton().manualLayoutable().apply {
            $0.backgroundColor = .backgroundGreen
            $0.setTitleColor(.white, for: .normal)
            $0.applyTouchAnimation()
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium)
        }
    }

}

// MARK: - Constants
extension Constants {
    enum AvailableGuestListPopup {
        static let cellContentMargin: CGFloat = 20.0
    }
}
