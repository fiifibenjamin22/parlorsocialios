//
//  AvailableGuestListPopupViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 04/06/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol AvailableGuestListPopupViewControllerDelegate: class {
    func navigateToGuestList(destinationController: UIViewController, animated: Bool)
}

class AvailableGuestListPopupViewController: AppViewController {
    
    // MARK: - Properties
    private var viewModel: AvailableGuestListPopupLogic!
    let analyticEventScreen: AnalyticEventScreen? = .guestListPopup
    private weak var delegate: AvailableGuestListPopupViewControllerDelegate?
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }

    // MARK: - Views
    private(set) var contentView: UIView!
    private(set) var activationImageView: UIImageView!
    private(set) var darkAlphaView: UIView!
    private(set) var headerStackView: UIStackView!
    private(set) var headerTextLabel: UILabel!
    private(set) var headerLeftLineView: UIView!
    private(set) var headerRightLineView: UIView!
    private(set) var closeButton: UIButton!
    private(set) var activationNameLabel: ParlorLabel!
    private(set) var activationStartAtLabel: UILabel!
    private(set) var viewGuestListButton: RoundedButton!
    
    // MARK: - Initialization
    init(activationId: Int,
         delegate: AvailableGuestListPopupViewControllerDelegate,
         showGuestListWhenAvailable: Bool) {
        super.init(nibName: nil, bundle: nil)
        initialize(with: activationId, delegate: delegate, showGuestListWhenAvailable: showGuestListWhenAvailable)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError()
    }
    
    private func initialize(
        with activationId: Int,
        delegate: AvailableGuestListPopupViewControllerDelegate,
        showGuestListWhenAvailable: Bool) {
        self.delegate = delegate
        self.viewModel = AvailableGuestListPopupViewModel(
            with: activationId, showGuestListWhenAvailable: showGuestListWhenAvailable)
    }
    
    // MARK: - Lifecycle
    override func loadView() {
        setupViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        adjustNavigationBar()
    }
    
    override func observeViewModel() {
        super.observeViewModel()
        viewModel.activationObs
            .subscribe(onNext: { [unowned self] activation in
                self.setup(with: activation)
            }).disposed(by: disposeBag)
        
        viewModel.destinationObs
            .subscribe(onNext: { [unowned self] destination in
                self.handle(destination: destination)
            }).disposed(by: disposeBag)
        
        viewModel.alertDataObs
            .show(using: self)
            .disposed(by: disposeBag)
        
        viewModel.errorObs
            .handleWithAlerts(using: self)
            .disposed(by: disposeBag)
        
        viewModel.progressBarFullScreen
            .showProgressBar(using: self, isDark: true)
            .disposed(by: disposeBag)
        
        viewModel.progressBarButtonObs
            .showProgressBar(using: self, inside: viewGuestListButton)
            .disposed(by: disposeBag)
        
        viewModel.contentViewVisibilityObs
            .subscribe(onNext: { [unowned self] isVisible in
                self.contentView.isHidden = !isVisible
            }).disposed(by: disposeBag)
    }
    
    override func bindUI() {
        super.bindUI()
        viewGuestListButton
            .rx.tap
            .bind(to: viewModel.viewGuestListButtonClick)
            .disposed(by: disposeBag)
    }
    
    override var prefersStatusBarHidden: Bool {
        if #available(iOS 13.0, *) {
            return false
        }
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.getActivation()
    }

}

// MARK: - Private methods
extension AvailableGuestListPopupViewController {
    private func setup(with activation: Activation?) {
        guard let activation = activation else { return }
        activationNameLabel.text = activation.name
        activationImageView.getImage(from: activation.imageUrls?.imageUrl1x1)
        UIView.animate(withDuration: 0.3) { [unowned self] in
            self.activationNameLabel.alpha = 1
            self.activationImageView.alpha = 1
        }
    }
    
    private func adjustNavigationBar() {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    private func setupViews() {
        let builder = AvailableGuestListPopupViewBuilder(controller: self)
        view = builder.buildView()
        contentView = builder.buildContentView()
        activationImageView = builder.buildActivationImageView()
        darkAlphaView = builder.buildDarkAlphaView()
        headerStackView = builder.buildHeaderStackView()
        headerTextLabel = builder.buildHeaderTextLabel()
        headerLeftLineView = builder.buildHeaderLeftLineView()
        headerRightLineView = builder.buildHeaderRightLineView()
        closeButton = builder.buildCloseButton()
        activationNameLabel = builder.buildActivationNameLabel()
        activationStartAtLabel = builder.buildActivationStartAtLabel()
        viewGuestListButton = builder.buildViewGuestListButton()
        
        builder.setupViews()
        closeButton.setupTapGestureRecognizer(target: self, action: #selector(tapCloseButton))
    }
    
    private func handle(destination: AvailableGuestListPopupDestination) {
        switch destination {
        case .guestList(_, let presentWithAnimation):
            dismiss(animated: true) { [unowned self] in
                self.delegate?.navigateToGuestList(destinationController: destination.viewController, animated: presentWithAnimation) }
        }
    }
    
    @objc private func tapCloseButton() {
        dismiss(animated: true)
    }

}
