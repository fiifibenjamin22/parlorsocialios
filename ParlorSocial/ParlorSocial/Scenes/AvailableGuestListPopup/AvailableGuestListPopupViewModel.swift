//
//  AvailableGuestListPopupViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 04/06/2019.
//

import Foundation
import RxSwift
import RxCocoa
import CoreLocation

protocol AvailableGuestListPopupLogic: BaseViewModelLogic {
    var activationObs: Observable<Activation?> { get }
    var viewGuestListButtonClick: PublishRelay<Void> { get }
    var destinationObs: Observable<AvailableGuestListPopupDestination> { get }
    var progressBarButtonObs: Observable<Bool> { get }
    var progressBarFullScreen: Observable<Bool> { get }
    var contentViewVisibilityObs: Observable<Bool> { get }
    func getActivation()
}

class AvailableGuestListPopupViewModel: BaseViewModel {
    // MARK: - Properties
    let analyticEventReferencedId: Int?
    private let activationId: Int
    private let guestListRepository = GuestListRsvpRepository.shared
    private let activationRepository = ActivationsRepository.shared
    private let destinationRelay: PublishRelay<AvailableGuestListPopupDestination> = PublishRelay()
    private let activationRelay: BehaviorRelay<Activation?>
    private let showGuestListWhenAvailableRelay: BehaviorRelay<Bool>
    private let contentViewVisibilityRelay: PublishRelay<Bool> = .init()
    lazy var checkInService: CheckInService = {
        let checkInService = CheckInService(locationService: LocationService(), guestListRepository: guestListRepository, activationRepository: activationRepository)
        checkInService.delegate = self
        return checkInService
    }()
    let viewGuestListButtonClick = PublishRelay<Void>()
    var pushNotificationId: Int?
    
    // MARK: - Initialization
    init(with activationId: Int,
         showGuestListWhenAvailable: Bool) {
        self.activationId = activationId
        self.activationRelay = BehaviorRelay(value: nil)
        self.analyticEventReferencedId = activationId
        self.showGuestListWhenAvailableRelay = .init(value: showGuestListWhenAvailable)
        super.init()
        initFlow()
    }
    
}

// MARK: Private logic
private extension AvailableGuestListPopupViewModel {
    private func initFlow() {
    
        viewGuestListButtonClick
            .withLatestFrom(activationRelay)
            .ignoreNil()
            .subscribe(onNext: { [unowned self] activation in
                self.checkInService.navigateToGuestList(activationId: activation.id, checkInStrategy: activation.checkInStrategy)
            }).disposed(by: disposeBag)
        
        activationRepository.newActivationDataObs
            .withLatestFrom(activationRelay) { newActivation, oldActivation -> (Activation, Activation)? in
                guard let activation = oldActivation else { return nil }
                return (newActivation, activation)
            }
            .ignoreNil()
            .filter { $0.id == $1.id && $0 != $1 }
            .map { (new, _) in return new }
            .bind(to: activationRelay)
            .disposed(by: disposeBag)        
    }
    
    private func handleResponse(_ response: SingleActivationResposne) {
        switch response {
        case .success(let successResponse):
            handleResponseSuccess(with: successResponse.data)
        case .failure(let error):
            errorSubject.onNext(error)
        }
    }
    
    private func handleResponseSuccess(with activation: Activation) {
        activationRelay.accept(activation)
        guard showGuestListWhenAvailableRelay.value,
            activation.hasGuestListAvailableWithoutPopup else {contentViewVisibilityRelay.accept(true); return }
        viewGuestListButtonClick.accept(())
    }
}

// MARK: Interface logic methods
extension AvailableGuestListPopupViewModel: AvailableGuestListPopupLogic {
    var contentViewVisibilityObs: Observable<Bool> {
        return contentViewVisibilityRelay.asObservable()
    }
    
    var destinationObs: Observable<AvailableGuestListPopupDestination> {
        return destinationRelay.asObservable()
    }
    
    var activationObs: Observable<Activation?> {
        return activationRelay.asObservable()
    }
    
    var progressBarButtonObs: Observable<Bool> {
        return progressBarSubject
            .withLatestFrom(showGuestListWhenAvailableRelay) { ($0, $1) }
            .filter { !$1 }
            .map { showProgressBar, _ in
                return showProgressBar }
            .asObservable()
    }
    
    var progressBarFullScreen: Observable<Bool> {
        return progressBarSubject
            .withLatestFrom(showGuestListWhenAvailableRelay) { ($0, $1) }
            .filter { $1 }
            .map { showProgressBar, _ in
                return showProgressBar }
            .asObservable()
    }
    
    func getActivation() {
        if showGuestListWhenAvailableRelay.value { contentViewVisibilityRelay.accept(false) }
        activationRepository.getActivation(withId: activationId).showingProgressBar(with: self)
            .subscribe(onNext: { [weak self] response in
                self?.handleResponse(response)
            }).disposed(by: disposeBag)
    }
}

// MARK: - Check-in service
extension AvailableGuestListPopupViewModel: CheckInServiceDelegate {
    func didEndCheckInSuccess(_ checkInService: CheckInService, activationId: Int) {
        progressBarSubject.onNext(false)
        destinationRelay.accept(.guestList(activationId: activationId, presentWithAnimation: !showGuestListWhenAvailableRelay.value))
    }
}

fileprivate extension Activation {
    var hasGuestListAvailableWithoutPopup: Bool {
        return isGuestListAvailable && checkInStrategy != .required
    }
}
