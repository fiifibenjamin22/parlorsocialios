//
//  GuestDataView.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 16/04/2019.
//

import UIKit

protocol GuestDataViewDelegate: class {
    func guestDataView(_ guestDataView: GuestDataView, didTapRemoveOnGuest guest: RsvpGuest, withNumber number: Int)
}

final class GuestDataView: UIView {

    private(set) var guestIcon: UIImageView!
    private(set) var guestIconContainer: UIView!
    private(set) var guestTitle: UILabel!
    private(set) var guestRemoveButton: TrackableButton!
    private(set) var headerStackView: UIStackView!

    private(set) var nameTextFieldView: GuestTextField!
    private(set) var surnameTextFieldView: GuestTextField!
    private(set) var emailTextFieldView: GuestTextField!

    private(set) var nominateCheckBox: AppCheckBox!
    private(set) var contentStackView: UIStackView!

    private(set) var stackViewContainer: UIView!
    
    weak var delegate: GuestDataViewDelegate?
    
    var showingGuest: RsvpGuest?
    
    var guestNumber: Int? {
        didSet {
            guard let guestNumber = guestNumber else { return }
            updateGuestNumber(guestNumber)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - Public methods
extension GuestDataView { }

// MARK: - Private methods
extension GuestDataView {
    
    private func updateGuestNumber(_ number: Int) {
        guestTitle.text = "\(Strings.GuestForm.guest.localized) \(number)"
    }
    
    func configure(with model: RsvpGuest, activation: Activation?) {
        showingGuest = model
        nameTextFieldView.text = model.firstName
        surnameTextFieldView.text = model.lastName
        nominateCheckBox.isChecked = model.isNominated ?? false
        emailTextFieldView.text = model.email
        emailTextFieldView.isRequired = model.isNominated ?? false
        guard let isRsvpOpen = activation?.isRsvpOpen else { return }
        nominateCheckBox.isHidden = !isRsvpOpen && !(model.isNominated ?? true)
        nominateCheckBox.isUserInteractionEnabled = isRsvpOpen
        emailTextFieldView.isHidden = !isRsvpOpen && emailTextFieldView.text.isEmptyOrNil
        [nameTextFieldView, surnameTextFieldView, emailTextFieldView].forEach { $0?.isUserInteractionEnabled = isRsvpOpen }
    }

    private func initialize() {
        clipsToBounds = true
        manualLayoutable()
        setupViews()
        nominateCheckBox.delegate = self
        guestRemoveButton.addTarget(self, action: #selector(didTapRemove), for: .touchUpInside)
        nameTextFieldView.addTarget(self, action: #selector(guestNameDidChanged), for: .allEditingEvents)
        surnameTextFieldView.addTarget(self, action: #selector(guestSurnameDidChanged), for: .allEditingEvents)
        emailTextFieldView.addTarget(self, action: #selector(guestEmailDidChanged), for: .allEditingEvents)
    }

    private func setupViews() {
        let builder = GuestDataViewBuilder(view: self)

        guestIcon = builder.buildGuestIcon()
        guestIconContainer = builder.buildView()
        guestTitle = builder.buildGuestTitleLabel()
        guestRemoveButton = builder.buildGuestRemoveButton()
        headerStackView = builder.buildHeaderStackView()

        nameTextFieldView = builder.buildGuestTextFieldView()
        surnameTextFieldView = builder.buildGuestTextFieldView()
        emailTextFieldView = builder.buildGuestTextFieldView()

        nominateCheckBox = builder.buildNominationCheckBox()
        contentStackView = builder.buildContentStackView()

        stackViewContainer = builder.buildView()

        builder.setupViews()
    }
    
    @objc private func didTapRemove() {
        guard let guest = showingGuest, let guestNumber = guestNumber else { return }
        delegate?.guestDataView(self, didTapRemoveOnGuest: guest, withNumber: guestNumber)
    }
    
    @objc private func guestNameDidChanged() {
        showingGuest = showingGuest?.withName(nameTextFieldView.text.emptyIfNil)
    }
    
    @objc private func guestSurnameDidChanged() {
        showingGuest = showingGuest?.withSurname(surnameTextFieldView.text.emptyIfNil)
    }
    
    @objc private func guestEmailDidChanged() {
        showingGuest = showingGuest?.withEmail(emailTextFieldView.text.emptyIfNil)
    }
}

extension GuestDataView: AppCheckBoxDelegate {

    func appCheckboxDidChangeStatus(_ appCheckbox: AppCheckBox) {
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.RSVPDetails.nominateGuest.rawValue)
        showingGuest = showingGuest?.withNominated(appCheckbox.isChecked)
        emailTextFieldView.isRequired = appCheckbox.isChecked
    }

}
