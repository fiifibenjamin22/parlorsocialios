//
//  JustMeInformationView.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 13/05/2019.
//

import UIKit

class JustMeInformationView: UIView {

    lazy var justMeInfo: InformationView = {
        return InformationView().manualLayoutable().apply {
            $0.icon = Icons.GuestsForm.guest
            $0.title = Strings.GuestForm.justMe.localized
            $0.tintColor = .rsvpBorder
            $0.font = UIFont.custom(
                ofSize: Constants.FontSize.tiny,
                font: UIFont.Roboto.regular)
            $0.iconSize = 18
        }
    }()
    
    lazy var bottomInfo: ParlorLabel = {
        return ParlorLabel.styled(withTextColor: .appTextBright, withFont: UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.regular), alignment: .left, numberOfLines: 0).apply {
            $0.lineHeight = 19
        }
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        addSubviews([justMeInfo, bottomInfo])
        justMeInfo.topAnchor.equal(to: self.topAnchor, constant: 18)
        justMeInfo.edgesToParent(anchors: [.leading])
        bottomInfo.edgesToParent(anchors: [.leading, .trailing, .bottom])
        bottomInfo.topAnchor.equal(to: justMeInfo.bottomAnchor, constant: 13)
        bottomInfo.bottomAnchor.equal(to: self.bottomAnchor, constant: -20)
    }

}
