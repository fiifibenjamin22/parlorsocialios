//
//  GuestTextField.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 07/11/2019.
//  Copyright © 2019 KISSdigital. All rights reserved.
//

import Foundation
import UIKit

final class GuestTextField: UITextField {
    
    // MARK: - Properties
    override var text: String? {
        didSet {
            showOptionalLabel = text.isEmptyOrNil
        }
    }
    
    var isRequired: Bool = true {
        didSet {
            optionalLabel.text = isRequired ? Strings.GuestForm.textFieldRequired.localized : Strings.GuestForm.textFieldOptional.localized
        }
    }
    
    var showOptionalLabel: Bool = true {
        didSet {
            optionalLabel.isHidden = !showOptionalLabel
        }
    }
    
    // MARK: - Views
    private(set) lazy var optionalLabel: ParlorLabel = {
        return ParlorLabel.styled(
            withTextColor: .appGreyLight,
            withFont: UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.regular),
            alignment: .right,
            numberOfLines: 1).manualLayoutable().apply {
                $0.text = Strings.GuestForm.textFieldRequired.localized
        }
    }()
    
    // MARK: - Initialization
    init() {
        super.init(frame: .zero)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        addSubview(optionalLabel)
        setupAutoLayout()
        
        showOptionalLabel = self.text.isEmptyOrNil
        self.addTarget(self, action: #selector(textFieldDidChanged), for: .allEditingEvents)
    }
    
    private func setupAutoLayout() {
        setupTextField()
        optionalLabel.edgesToParent(anchors: [.top, .bottom, .trailing], insets: UIEdgeInsets.margins(right: 20))
    }
    
    private func setupTextField() {
        backgroundColor = .white
        font = UIFont.custom(ofSize: Constants.FontSize.hintSize, font: UIFont.Roboto.regular)
        textColor = .appTextMediumBright
        layer.borderWidth = 0.5
        layer.borderColor = UIColor.textVeryLight.cgColor
        setLeftPadding(21)
        setRightPadding(21)
        autocorrectionType = .no
        tintColor = .black
    }
    
    @objc private func textFieldDidChanged() {
        showOptionalLabel = self.text.isEmptyOrNil
    }
}
