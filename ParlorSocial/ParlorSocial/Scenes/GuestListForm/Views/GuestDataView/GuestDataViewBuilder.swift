//
//  GuestDataViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 16/04/2019.
//

import UIKit

final class GuestDataViewBuilder {

    private unowned let view: GuestDataView

    init(view: GuestDataView) {
        self.view = view
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }

}

extension GuestDataViewBuilder {

    private func setupProperties() {
        view.apply {
            $0.backgroundColor = .appBackground

            $0.guestRemoveButton.setTitle(Strings.GuestForm.remove.localized, for: .normal)
            $0.guestRemoveButton.trackingId = AnalyticEventClickableElementName.RSVPDetails.removeGuest.rawValue
            $0.nominateCheckBox.title = Strings.GuestForm.nominate.localized

            $0.nameTextFieldView.attributedPlaceholder = getAttributedPlaceholder(with: Strings.GuestForm.name.localized)
            $0.surnameTextFieldView.attributedPlaceholder = getAttributedPlaceholder(with: Strings.GuestForm.surname.localized)
            $0.emailTextFieldView.attributedPlaceholder = getAttributedPlaceholder(with: Strings.GuestForm.email.localized)
            
            $0.nameTextFieldView.isRequired = true
            $0.surnameTextFieldView.isRequired = true
            $0.emailTextFieldView.isRequired = false
            
            $0.emailTextFieldView.keyboardType = .emailAddress
        }
    }

    private func setupHierarchy() {
        view.apply {
            $0.addSubview($0.stackViewContainer)
            $0.stackViewContainer.addSubview($0.contentStackView)
            $0.guestIconContainer.addSubview($0.guestIcon)

            $0.headerStackView.addArrangedSubviews([$0.guestIconContainer, $0.guestTitle, $0.guestRemoveButton])
            $0.contentStackView.addArrangedSubviews([$0.headerStackView, $0.nameTextFieldView, $0.surnameTextFieldView, $0.emailTextFieldView, $0.nominateCheckBox])

            $0.guestIcon.heightAnchor.equalTo(constant: 18)
            $0.guestIcon.widthAnchor.constraint(equalTo: $0.guestIcon.heightAnchor, multiplier: 1).activate()

            [$0.nameTextFieldView, $0.surnameTextFieldView, $0.emailTextFieldView].forEach {
                $0?.heightAnchor.equalTo(constant: 60)
            }

            $0.guestIcon.edgesToParent(anchors: [.leading, .trailing])
            $0.guestIcon.centerYAnchor.equal(to: $0.guestTitle.centerYAnchor)
        }
    }

    private func setupAutoLayout() {
        view.contentStackView.edgesToParent()
        view.stackViewContainer.edgesToParent(insets: UIEdgeInsets(
            top: 0,
            left: Constants.Margin.standard,
            bottom: Constants.Margin.standard,
            right: Constants.Margin.standard
        ))
    }

    private func getAttributedPlaceholder(with text: String) -> NSAttributedString {
        let attributes: [NSAttributedString.Key: Any] = [
                .foregroundColor: UIColor.appGreyLight,
                .font: UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.regular)
        ]

        return NSAttributedString(string: text, attributes: attributes)
    }
}

extension GuestDataViewBuilder {

    func buildGuestIcon() -> UIImageView {
        return UIImageView().manualLayoutable().apply {
            $0.contentMode = .scaleAspectFit
            $0.clipsToBounds = true
            $0.image = Icons.GuestsForm.guest
            $0.tintColor = .appTextMediumBright
        }
    }

    func buildGuestTitleLabel() -> UILabel {
        return ParlorLabel.styled(
            withTextColor: .appTextMediumBright,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.tiny,
                font: UIFont.Roboto.regular),
            alignment: .left
        ).apply {
            $0.insets.top = 20
            $0.insets.bottom = 19
            $0.setContentCompressionResistancePriority(.required, for: .vertical)
        }
    }

    func buildGuestRemoveButton() -> TrackableButton {
        return TrackableButton().manualLayoutable().apply {
            $0.applyTouchAnimation()
            $0.setTitleColor(.appTextMediumBright, for: .normal)
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.regular)
            $0.setContentHuggingPriority(.required, for: .horizontal)
            $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        }
    }

    func buildHeaderStackView() -> UIStackView {
        return UIStackView(axis: .horizontal, spacing: 5, distribution: .fill)
    }

    func buildContentStackView() -> UIStackView {
        return UIStackView(axis: .vertical, spacing: 0)
    }

    func buildGuestTextFieldView() -> GuestTextField {
        return GuestTextField().manualLayoutable()
    }

    func buildNominationCheckBox() -> AppCheckBox {
        let checkBox = AppCheckBox()
        checkBox.backgroundColor = .appBackground

        return checkBox
    }

    func buildView() -> UIView {
        return UIView().manualLayoutable().apply {
            $0.backgroundColor = .appBackground
            $0.clipsToBounds = true
        }
    }

}
