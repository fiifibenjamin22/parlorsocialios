//
//  GuestFormActivationInfoCellBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 12/04/2019.
//

import UIKit

final class GuestFormActivationInfoViewBuilder {

    private unowned let view: GuestFormActivationInfoView

    init(view: GuestFormActivationInfoView) {
        self.view = view
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }

}

extension GuestFormActivationInfoViewBuilder {

    private func setupProperties() {
        view.backgroundColor = .white
        view.topSeparator.backgroundColor = .appBackground
    }

    private func setupHierarchy() {
        view.apply {
            view.addSubviews([$0.contentStackView, $0.topSeparator])
            $0.contentStackView.addArrangedSubviews([$0.imageContainer, $0.informationContainer])
            $0.imageContainer.addSubview($0.logoImageView)
            $0.informationContainer.addSubview($0.textStackView)
            $0.textStackView.addArrangedSubviews([$0.dateLabel, $0.titleLabel, $0.locationLabel])
        }
    }

    private func setupAutoLayout() {
        view.topSeparator.edgesToParent(anchors: [.top, .leading, .trailing])
        view.topSeparator.heightAnchor.equalTo(constant: 1)
        
        view.contentStackView.edgesToParent(insets: .margins(top: 25, left: 16, bottom: 31, right: 16))
        view.logoImageView.apply {
            $0.heightAnchor.equalTo(constant: 100)
            $0.widthAnchor.constraint(equalTo: $0.heightAnchor, multiplier: 1).activate()
        }
        
        view.textStackView.edgesToParent(anchors: [.leading, .top, .trailing])
        view.textStackView.bottomAnchor.lessThanOrEqual(to: view.informationContainer.bottomAnchor)
        
        view.logoImageView.edgesToParent(anchors: [.leading, .top, .trailing])
        view.logoImageView.bottomAnchor.lessThanOrEqual(to: view.imageContainer.bottomAnchor)
    }

}

extension GuestFormActivationInfoViewBuilder {

    func buildImageView() -> UIImageView {
        let imageView = UIImageView().manualLayoutable()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true

        return imageView
    }

    func buildTitleLabel() -> UILabel {
        let label = UILabel.styled(
            textColor: .appTextBright,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.hintSize,
                font: UIFont.Editor.medium),
            alignment: .left)

        label.setContentCompressionResistancePriority(.required, for: .horizontal)
        return label
    }

    func buildSubtitleLabel() -> UILabel {
        let label = UILabel.styled(
            textColor: .appTextSemiLight,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.tiny,
                font: UIFont.Roboto.regular),
            alignment: .left)

        label.setContentCompressionResistancePriority(.required, for: .horizontal)
        return label
    }

    func buildView() -> UIView {
        return UIView().manualLayoutable()
    }

    func buildTextStackView() -> UIStackView {
        return UIStackView(axis: .vertical, spacing: Constants.Margin.small, alignment: .leading)
    }

    func buildContentStackView() -> UIStackView {
        return UIStackView(axis: .horizontal, spacing: 18)
    }

}
