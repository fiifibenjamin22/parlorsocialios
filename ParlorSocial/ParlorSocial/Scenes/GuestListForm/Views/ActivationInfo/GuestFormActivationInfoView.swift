//
//  GuestFormActivationInfoCell.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 12/04/2019.
//

import UIKit

final class GuestFormActivationInfoView: UIView {
    
    // MARK: Private properties
    private(set) var topSeparator: UIView!
    private(set) var dateLabel: UILabel!
    private(set) var titleLabel: UILabel!
    private(set) var locationLabel: UILabel!
    private(set) var logoImageView: UIImageView!
    
    private(set) var textStackView: UIStackView!
    private(set) var contentStackView: UIStackView!
    private(set) var imageContainer: UIView!
    private(set) var informationContainer: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - Public methods
extension GuestFormActivationInfoView {

    func configure(with model: Activation) {
        dateLabel.text = model.startAt.toFullDescriptiveDateFormat()
        titleLabel.text = model.name
        if let location = model.location {
            locationLabel.text = "\(location.address1), \(location.city)"
        }
        logoImageView.getImage(from: model.imageUrls?.imageUrl1x1)
    }
    
}

// MARK: - Private methods
extension GuestFormActivationInfoView {
    
    private func initialize() {
        clipsToBounds = true
        manualLayoutable()
        setupViews()
    }
    
    private func setupViews() {
        let builder = GuestFormActivationInfoViewBuilder(view: self)
        
        topSeparator = builder.buildView()
        dateLabel = builder.buildSubtitleLabel()
        locationLabel = builder.buildSubtitleLabel()
        titleLabel = builder.buildTitleLabel()
        logoImageView = builder.buildImageView()
        
        textStackView = builder.buildTextStackView()
        contentStackView = builder.buildContentStackView()
        
        imageContainer = builder.buildView()
        informationContainer = builder.buildView()
        
        builder.setupViews()
    }
}
