//
//  TicketAtDoorView.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 23/10/2019.
//  Copyright © 2019 KISSdigital. All rights reserved.
//

import UIKit

class TicketAtDoorView: UIView {

    lazy var ticketAtDoorInformation: InformationView = {
        return InformationView().manualLayoutable().apply {
            $0.title = Strings.MyRsvps.ticketAtDoorInfo.localized
            $0.icon = Icons.GuestsForm.info
            $0.tintColor = .appTextSemiLight
            $0.clipIconToTop = true
            $0.font = UIFont.custom(
                ofSize: Constants.FontSize.xTiny,
                font: UIFont.Roboto.regular)
            $0.iconSize = 11
        }
    }()
    
    // MARK: - Initalization
    init() {
        super.init(frame: .zero)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        setupAutoLayout()
    }
    
    private func setupAutoLayout() {
        addSubview(ticketAtDoorInformation)
        
        ticketAtDoorInformation.edgesToParent(insets: UIEdgeInsets.margins(top: 22))
    }
    
}
