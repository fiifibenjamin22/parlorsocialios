//
//  DropDownViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 18/04/2019.
//

import UIKit

final class DropDownViewBuilder {
    
    private unowned let view: DropDownView
    
    init(view: DropDownView) {
        self.view = view
    }
    
    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
    
}

extension DropDownViewBuilder {
    
    private func setupProperties() {
        view.apply {
            $0.backgroundColor = .appBackground
            $0.optionBackgroundView.backgroundColor = .clear
            $0.optionBackgroundView.layer.borderWidth = 1
            $0.optionBackgroundView.layer.borderColor = UIColor.textVeryLight.cgColor
            $0.optionArrowDown.image = Icons.GuestsForm.arrow
            $0.optionBackgroundView.applyTouchAnimation()
            $0.addOptionButton.setImage(Icons.GuestsForm.add, for: .normal)
            $0.optionStackView.isHidden = true
            $0.optionButton.isHidden = true
        }
    }
    
    private func setupHierarchy() {
        view.apply {
            $0.addSubview($0.contentStackView)
            $0.contentStackView.addArrangedSubviews([$0.upperStackView, $0.additionalInformationView])
            $0.headerStackView.addArrangedSubviews([$0.headerInformationView, $0.headerValueLabel])
            $0.upperStackView.addArrangedSubviews([$0.headerStackView, $0.optionBackgroundView])
            
            [$0.optionStackView, $0.optionButton, $0.addOptionButton].addTo(parent: $0.optionBackgroundView)
            $0.optionStackView.addArrangedSubviews([$0.optionIcon, $0.optionLabel, $0.optionArrowDown])
        }
    }
    
    private func setupAutoLayout() {
        view.apply {
            $0.contentStackView.edgesToParent(insets: UIEdgeInsets.margins(top: 30, left: 0, bottom: 30, right: 0))
            $0.optionBackgroundView.heightAnchor.equalTo(constant: 60)
            
            $0.optionStackView.edgesToParent(anchors: [.trailing], insets: UIEdgeInsets.margins(right: 20))
            $0.optionStackView.centerYAnchor.equal(to: $0.optionBackgroundView.centerYAnchor)
            
            $0.optionIcon.heightAnchor.equalTo(constant: 30)
            $0.optionIcon.setContentHuggingPriority(.required, for: .horizontal)
            $0.optionIcon.setContentCompressionResistancePriority(.required, for: .horizontal)
            
            $0.optionArrowDown.setContentHuggingPriority(.required, for: .horizontal)
            $0.optionArrowDown.setContentCompressionResistancePriority(.required, for: .horizontal)
            
            $0.optionButton.edges(equalTo: $0.optionBackgroundView)
            $0.addOptionButton.edgesToParent(anchors: [.top, .leading, .trailing])
            $0.addOptionButton.heightAnchor.equalTo(constant: Constants.bigButtonHeight)
        }
    }
    
    func setupOptionStackViewLeading() -> NSLayoutConstraint {
        return view.optionStackView.leadingAnchor.equal(to: view.optionBackgroundView.leadingAnchor, constant: 21)
    }
    
}

extension DropDownViewBuilder {
    
    func builTitleInformationView() -> InformationView {
        return InformationView().apply {
            $0.tintColor = .rsvpBorder
            $0.font = UIFont.custom(
                ofSize: Constants.FontSize.tiny,
                font: UIFont.Roboto.regular)
            $0.iconSize = 18
        }
    }
    
    func buildHeaderValueLabel() -> ParlorLabel {
        return ParlorLabel.styled(
            withTextColor: .appTextMediumBright,
            withFont: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.bold),
            alignment: .left,
            numberOfLines: 1)
            .manualLayoutable()
    }
    
    func builAdditionalInformationView() -> InformationView {
        return InformationView().apply {
            $0.icon = Icons.GuestsForm.info
            $0.tintColor = .appTextSemiLight
            $0.font = UIFont.custom(
                ofSize: Constants.FontSize.xTiny,
                font: UIFont.Roboto.regular)
            $0.iconSize = 11
        }
    }
    
    func buildDropDownButton() -> TrackableButton {
        return TrackableButton().manualLayoutable().apply {
            $0.setTitleColor(.appTextMediumBright, for: .normal)
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.regular)
            $0.setContentHuggingPriority(.required, for: .horizontal)
            $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        }
    }
    
    func buildOptionIcon() -> UIImageView {
        return UIImageView().manualLayoutable().apply {
            $0.contentMode = .scaleAspectFit
            $0.tintColor = .appTextBright
        }
    }
    
    func buildOptionTitle() -> UILabel {
        return UILabel.styled(
            textColor: .rsvpBorder,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.hintSize,
                font: UIFont.Roboto.regular),
            alignment: .left,
            numberOfLines: 1)
    }
    
    func buildUpperStackView() -> UIStackView {
        return UIStackView(axis: .vertical, spacing: 20)
    }
    
    func buildContentStackView() -> UIStackView {
        return UIStackView(axis: .vertical, spacing: 15)
    }
    
    func buildOptionStackView() -> UIStackView {
        return UIStackView(axis: .horizontal, spacing: 10, alignment: .fill, distribution: .fill).manualLayoutable()
    }
    
    func buildAddOptionButton() -> TrackableButton {
        return TrackableButton().manualLayoutable().apply {
            $0.backgroundColor = .white
            $0.tintColor = .appText
            $0.layer.cornerRadius = Constants.bigButtonHeight / 2
            $0.layer.borderColor = UIColor.textVeryLight.cgColor
            $0.layer.borderWidth = 1
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium)
            $0.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 5)
            $0.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
            $0.setTitleColor(UIColor.appText, for: .normal)
            $0.applyTouchAnimation()
        }
    }
    
    func buildHeaderStackView() -> UIStackView {
        return UIStackView(axis: .horizontal, spacing: 3)
    }
    
    func buildView() -> UIView {
        return UIView().manualLayoutable().apply {
            $0.backgroundColor = .appBackground
            $0.clipsToBounds = true
        }
    }
    
}
