//
//  DropDownPopupDestination.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 19/04/2019.
//

import UIKit

enum DropDownPopupDestination {
    case popup(data: DropDownData, selectedOption: DropDownOption, callback: (Int) -> Void, addOptionInPopupCallback: () -> Void = {})
}

extension DropDownPopupDestination: Destination {

    var viewController: UIViewController {
        switch self {
        case .popup(let data, let selectedOption, let callback, let addOptionInPopupCallback):
            let vc = DropDownPopupViewController(with: data, selectedOption: selectedOption)
            vc.selectionCallback = callback
            vc.addBottomButtonCallback = addOptionInPopupCallback
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve

            return vc
        }
    }
    
}
