//
//  ExternalTicketGroup.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 28/05/2019.
//

import UIKit

protocol ExternalTicketGroupDelegate: class {
    func didTapExternalTicketButton()
}

class ExternalTicketGroup: UIView {
    // MARK: - Properties
    weak var delegate: ExternalTicketGroupDelegate?
    
    // MARK: - Views
    lazy var buyExternalTicketButton: RoundedButton = {
        return RoundedButton().manualLayoutable().apply {
            $0.backgroundColor = .appBackground
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium)
            $0.setTitleColor(.appText, for: .normal)
            $0.layer.borderWidth = 1
            $0.layer.borderColor = UIColor.appSeparator.cgColor
            $0.applyTouchAnimation()
            $0.setTitle(Strings.MyRsvps.buyExternalTickets.localized.uppercased(), for: .normal)
            $0.trackingId = AnalyticEventClickableElementName.RSVPDetails.buyExternalTickets.rawValue
        }
    }()
    
    lazy var externalTicketInformation: InformationView = {
        return InformationView().manualLayoutable().apply {
            $0.title = Strings.MyRsvps.externalTicketInfo.localized
            $0.icon = Icons.GuestsForm.info
            $0.tintColor = .appTextSemiLight
            $0.clipIconToTop = true
            $0.font = UIFont.custom(
                ofSize: Constants.FontSize.xTiny,
                font: UIFont.Roboto.regular)
            $0.iconSize = 11
        }
    }()
    
    // MARK: - Initalization
    init() {
        super.init(frame: .zero)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        addSubviews([externalTicketInformation, buyExternalTicketButton])
        setupAutoLayout()
        buyExternalTicketButton.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
    }
    
    private func setupAutoLayout() {
        externalTicketInformation.edgesToParent(anchors: [.leading, .trailing, .top], insets: UIEdgeInsets.margins(top: 22))
        externalTicketInformation.bottomAnchor.equal(to: buyExternalTicketButton.topAnchor, constant: -Constants.Margin.standard)
        
        buyExternalTicketButton.heightAnchor.equalTo(constant: Constants.bigButtonHeight)
        buyExternalTicketButton.edgesToParent(anchors: [.leading, .trailing, .bottom])
    }
    
    // MARK: - Private Logics
    @objc private func didTapButton() {
        delegate?.didTapExternalTicketButton()
    }
    
}
