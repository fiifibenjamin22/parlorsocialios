//
//  GuestListFormViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 12/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import WebKit

final class GuestListFormViewBuilder {
    
    private unowned let controller: GuestListFormViewController
    private var view: UIView! { return controller.view }
    
    init(controller: GuestListFormViewController) {
        self.controller = controller
    }
    
    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
    
}

extension GuestListFormViewBuilder {
    
    private func setupProperties() {
        view.backgroundColor = .appBackground
        controller.apply {
            $0.contentView.backgroundColor = .appBackground
            $0.cancelReservationButton.setTitle(Strings.GuestForm.cancel.localized.uppercased(), for: .normal)
            $0.cancelReservationButton.trackingId = AnalyticEventClickableElementName.RSVPDetails.cancelReservation.rawValue
            $0.doneButton.setTitle(Strings.GuestForm.done.localized.uppercased(), for: .normal)
            $0.addGuestButton.setImage(Icons.GuestsForm.add, for: .normal)
            $0.addGuestButton.setTitle(Strings.GuestForm.add.localized.uppercased(), for: .normal)
            $0.addGuestButton.trackingId = AnalyticEventClickableElementName.RSVPDetails.addGuest.rawValue
            $0.timeSegmentSelector.optionButton.trackingId = AnalyticEventClickableElementName.RSVPDetails.timeSegments.rawValue
            $0.ticketPurchaseSelector.optionButton.trackingId = AnalyticEventClickableElementName.RSVPDetails.paymentMethods.rawValue
            $0.ticketPurchaseSelector.addOptionButton.trackingId = AnalyticEventClickableElementName.RSVPDetails.paymentMethodsAddAnother.rawValue
        }
    }
    
    private func setupHierarchy() {
        controller.apply {
            view.addSubview($0.scrollView)
            view.addSubview($0.doneButton)
            
            $0.scrollView.addSubview($0.contentView)
            $0.contentView.addSubview($0.contentStackView)
            $0.contentStackView.addArrangedSubview($0.informationView)
            $0.contentStackView.addDefaultSeperator(withColor: .appBackground, size: 20)
            $0.contentStackView.addArrangedSubviews([$0.guestsStackView, $0.marginedStackView])
            
            let seperatorColor = UIColor.appSeparator.withAlphaComponent(0.5)
            $0.marginedStackView.addArrangedSubviews([$0.justMeEditInfo, $0.provideEmailInfoView])
            $0.marginedStackView.addArrangedSubviews([$0.addGuestButton, $0.addGuestBottomSpace])
            $0.marginedStackView.addDefaultSeperator(withColor: seperatorColor)
            $0.marginedStackView.addArrangedSubview($0.timeSegmentSelector)
            $0.marginedStackView.addDefaultSeperator(withColor: seperatorColor)
            $0.marginedStackView.addArrangedSubviews([
                $0.ticketPurchaseGroup,
                $0.externalTicketGroup,
                $0.ticketAtDoorView
            ])
            $0.marginedStackView.addDefaultSeperator(withColor: .appBackground, size: 40)
            $0.marginedStackView.addArrangedSubview($0.cancelReservationButton)
            
            $0.ticketPurchaseGroup.addSubviews([$0.ticketPurchaseSelector, $0.stripeButton])
        }
    }
    
    private func setupAutoLayout() {
        controller.doneButton.apply {
            $0.edgesToParent(
                anchors: [.leading, .bottom, .trailing],
                insets: UIEdgeInsets.margins(left: -10, right: -10))
            $0.heightAnchor.equalTo(constant: 72)
        }
        
        controller.scrollView.apply {
            $0.edgesToParent(anchors: [.leading, .trailing, .top])
            $0.bottomAnchor.equal(to: controller.doneButton.topAnchor)
        }
        
        controller.apply {
            $0.contentView.edgesToParent()
            $0.contentView.widthAnchor.equal(to: $0.scrollView.widthAnchor)
        }
        
        controller.apply {
            $0.ticketPurchaseSelector.topAnchor.equal(to: $0.ticketPurchaseGroup.topAnchor)
            $0.ticketPurchaseSelector.edgesToParent(anchors: [.leading, .trailing])
            
            $0.stripeButton.topAnchor.equal(to: $0.ticketPurchaseSelector.bottomAnchor, constant: -Constants.Margin.small)
            $0.stripeButton.centerXAnchor.equal(to: $0.ticketPurchaseGroup.centerXAnchor)
            $0.stripeButton.bottomAnchor.equal(to: $0.ticketPurchaseGroup.bottomAnchor)
        }
        
        controller.addGuestButton.heightAnchor.equalTo(constant: Constants.bigButtonHeight)
        controller.cancelReservationButton.heightAnchor.equalTo(constant: Constants.bigButtonHeight)
        controller.contentStackView.edgesToParent()
    }
    
}

extension GuestListFormViewBuilder {
    
    func buildView() -> UIView {
        let view = UIView().manualLayoutable()
        return view
    }
    
    func buildMainScrollView() -> UIScrollView {
        return UIScrollView().manualLayoutable()
    }
    
    func buildScrollContentView() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildDoneButton() -> UIButton {
        let button = UIButton().manualLayoutable()
        button.backgroundColor = .white
        button.setTitleColor(.appText, for: .normal)
        button.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.medium)
        button.addShadow(withOpacity: 0.1, radius: 10)
        button.applyTouchAnimation()
        
        return button
    }
    
    func buildDropDownView() -> DropDownView {
        return DropDownView(controller: controller)
    }
    
    func buildStripeButton() -> StripeButton {
        return StripeButton().manualLayoutable()
    }
    
    func buildRoundedTransparentButton() -> TrackableButton {
        return RoundedButton().apply {
            $0.backgroundColor = .appBackground
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium)
            $0.setTitleColor(.appText, for: .normal)
            $0.layer.borderWidth = 1
            $0.layer.borderColor = UIColor.appSeparator.cgColor
            $0.applyTouchAnimation()
        }
    }
    
    func buildJustMeInfo() -> JustMeInformationView {
        return JustMeInformationView()
    }
    
    func buildProvideEmailInfoView() -> UIView {
        let informationView = InformationView().apply {
            $0.icon = Icons.GuestsForm.info
            $0.tintColor = .appTextSemiLight
            $0.font = UIFont.custom(
                ofSize: Constants.FontSize.xTiny,
                font: UIFont.Roboto.regular)
            $0.iconSize = 11
            $0.title = Strings.GuestForm.provideEmail.localized
        }
        
        let view = UIView().manualLayoutable()
        
        view.addSubview(informationView)
        informationView.edgesToParent(insets: UIEdgeInsets.margins(bottom: 20))
        return view
    }
    
    func buildAddGuestButtonBottomSpace() -> UIView {
        return UIView().apply {
            $0.heightAnchor.equalTo(constant: 40)
        }
    }
    
    func buildAddGuestButton() -> TrackableButton {
        return TrackableButton().manualLayoutable().apply {
            $0.backgroundColor = .white
            $0.tintColor = .appText
            $0.layer.cornerRadius = Constants.bigButtonHeight / 2
            $0.layer.borderColor = UIColor.textVeryLight.cgColor
            $0.layer.borderWidth = 1
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium)
            $0.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 5)
            $0.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
            $0.setTitleColor(UIColor.appText, for: .normal)
            $0.applyTouchAnimation()
        }
    }
    
    func buildExternalTicketGroup() -> ExternalTicketGroup {
        return ExternalTicketGroup().manualLayoutable()
    }
    
    func buildTicketAtDoorView() -> TicketAtDoorView {
        return TicketAtDoorView().manualLayoutable()
    }
    
    func buildCloseItem() -> UIBarButtonItem {
        return UIBarButtonItem(image: Icons.NavigationBar.close, style: .plain, target: controller, action: #selector(controller.didTapClose)).apply {
            $0.tintColor = UIColor.closeTintColor
        }
    }
    
    func buildContentStackView() -> UIStackView {
        return UIStackView(axis: .vertical, spacing: 0)
    }
    
    func buildContentMarginedStackView() -> UIStackView {
        return UIStackView(axis: .vertical, spacing: 0).apply {
            $0.layoutMargins = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
            $0.isLayoutMarginsRelativeArrangement = true
        }
    }
    
    func buildGuestsStackView() -> UIStackView {
        return UIStackView(axis: .vertical, spacing: 0)
    }
    
    func buildInformationView() -> GuestFormActivationInfoView {
        return GuestFormActivationInfoView()
    }
    
    func buildWebView() -> WKWebView {
        return WKWebView().manualLayoutable().apply {
            $0.backgroundColor = .white
        }
    }
}
