//
//  GuestListFormViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 12/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxSwiftExt
import PassKit

protocol GuestListFormLogic: BaseViewModelLogic {
    var activationInfoRelay: BehaviorRelay<Activation?> { get }
    var guestsRelay: BehaviorRelay<[RsvpGuest]> { get }
    var addGuestButtonVisibleObs: Observable<Bool> { get }
    
    var selectedSegmentIndexSubject: BehaviorSubject<Int> { get }
    var selectedPaymentMethodIndexSubject: BehaviorSubject<Int?> { get }
    var cancelClicks: PublishSubject<Void> { get }
    var doneClicks: PublishSubject<Void> { get }
    var addCardClicks: PublishSubject<Void> { get }
    var guestRemovedSubject: PublishSubject<Int> { get }
    var addGuestClicksSubject: PublishSubject<Void> { get }
    var guestAdditionObs: Observable<RsvpGuest> { get }
    var guestRemovedObs: Observable<Int> { get }
    var paymentMethodsObs: Observable<([PaymentMethodOption], PaymentMethodOption?)> { get }
    var isJustMeInfoVisible: Observable<Bool> { get }
    var provideEmailInfoViewVisible: Observable<Bool> { get }
    var ticketPriceObs: Observable<Double> { get }
    
    var destinationObs: Observable<GuestListFormDestination> { get }
    
    var isInEditMode: Bool { get }
    func openExternalTicketWebView()
}

class GuestListFormViewModel: BaseViewModel {
    let analyticEventReferencedId: Int?
    private let minimumPrice = 0.001
    private let activationsRepository = ActivationsRepository.shared
    private let profileRepository = ProfileRepository.shared
    private var applePayManager: ApplePayManager!
    private let cardManagementService = CardManagementService()
    private var addToGuestListHelper: AddToGuestListHelperProtocol?
    private let rsvpRequestValidationHelper = RsvpRequestValidationHelper()
    private let validator = Validator()
    private let calendarSyncViewProvider = CalendarSyncViewProvider()
    private let incrementRsvpCountHelper = IncrementRsvpCountHelper()
    private var rsvpResponseAfterApplePayPayment: RsvpResponse?
    
    private let destinationSubject: PublishSubject<GuestListFormDestination> = PublishSubject()
    private let paymentMethodsSubject: BehaviorRelay<[PaymentMethodOption]> = BehaviorRelay(value: [])
    
    let activationInfoRelay: BehaviorRelay<Activation?>
    let selectedSegmentIndexSubject: BehaviorSubject<Int> = BehaviorSubject(value: 0)
    let selectedPaymentMethodIndexSubject: BehaviorSubject<Int?> = BehaviorSubject(value: nil)
    let ticketPriceRelay: BehaviorRelay<Double> = BehaviorRelay(value: 0)

    let guestsRelay = BehaviorRelay<[RsvpGuest]>(value: [])

    let doneClicks = PublishSubject<Void>()
    let doneClicksFlow = PublishSubject<Void>()
    let addGuestClicksSubject = PublishSubject<Void>()
    let cancelClicks = PublishSubject<Void>()
    let addCardClicks = PublishSubject<Void>()
    
    let guestRemovedSubject = PublishSubject<Int>()
    private var selectedSegment: Observable<TimeSegment?> {
        return Observable.combineLatest(activationInfoRelay, selectedSegmentIndexSubject) { activation, index in
            return activation?.segments[index]
        }
    }
    
    private var selectedPaymentMethodObs: Observable<PaymentMethodOption?> {
        return Observable.combineLatest(paymentMethodsSubject, selectedPaymentMethodIndexSubject) { paymentMethods, selectedIndex in
            guard let index = selectedIndex,
                index < paymentMethods.count else { return nil }
            return paymentMethods[index]
        }
    }
    
    private var rsvpRequest: Observable<RsvpRequest?> {
        return Observable.combineLatest(activationInfoRelay, selectedSegment, guestsRelay, selectedPaymentMethodObs) { activation, segment, guests, paymentMethod in
            guard let segmentId = segment?.id else { return nil }
            let rsvp = Rsvp(timeSegmentId: segmentId, guests: guests, id: activation?.rsvp?.id, checkedInAt: activation?.rsvp?.checkedInAt)
            let cardId = paymentMethod?.type == .card ? paymentMethod?.id : nil
            return RsvpRequest(rsvp: rsvp, cardId: cardId)
        }
    }
    
    init(forActivation activation: Activation, andGuestsCount guestCount: Int?, guestSlotsChecked: Bool, applePayPresentationController: UIViewController) {
        activationInfoRelay = BehaviorRelay(value: activation)
        analyticEventReferencedId = activation.id
        super.init()
        if let rsvp = activation.rsvp {
            initializeForEditableMode(rsvp)
        } else {
            initializeForNewRsvp(withGuestsCount: guestCount ?? 0, and: guestSlotsChecked)
        }
        
        applePayManager = ApplePayManager(presentationController: applePayPresentationController, delegate: self)
        initializeFlow()
    }
    
}

// MARK: Private logic
private extension GuestListFormViewModel {
    
    private func initializeForEditableMode(_ rsvp: Rsvp) {
        addToGuestListHelper = AddToGuestListHelper(viewModel: self, checkingGuestCountIsNeeded: false)
        guestsRelay.accept(rsvp.guests)
        if let timeSegmentIndex = activationInfoRelay.value?.segments.firstIndex(where: { $0.id == rsvp.timeSegmentId }) {
            selectedSegmentIndexSubject.onNext(timeSegmentIndex)
        } else {
            fatalError("This activation has no time segment with that id")
        }
    }
    
    private func initializeForNewRsvp(withGuestsCount guestsCount: Int, and guestSlotsChecked: Bool) {
        addToGuestListHelper = AddToGuestListHelper(viewModel: self, checkingGuestCountIsNeeded: !guestSlotsChecked)
        if guestsCount > 0 {
            guestsRelay.accept(Array(1...guestsCount).map { _ in RsvpGuest.empty() })
        }
        selectedSegmentIndexSubject.onNext(0)
        let newPrice = (activationInfoRelay.value?.ticketPrice ?? 0) * (Double(guestsCount) + 1)
        ticketPriceRelay.accept(roundPrice(newPrice))
    }
    
    private func initializeFlow() {
        addGuestClicksSubject
            .withLatestFrom(activationInfoRelay) { $1?.ticketPrice }
            .withLatestFrom(ticketPriceRelay) { ($0 ?? 0, $1) }
            .doOnNext { [unowned self] tuple in
                let (ticketPrice, summaryPrice) = tuple
                self.ticketPriceRelay.accept(self.roundPrice(summaryPrice + ticketPrice))
            }
            .withLatestFrom(guestsRelay)
            .map { currentGuests in
                return currentGuests + [RsvpGuest.empty()]
            }
            .bind(to: guestsRelay)
            .disposed(by: disposeBag)
        
        guestRemovedObs
            .withLatestFrom(guestsRelay) { ($0, $1) }
            .map { [unowned self] tuple in
                let (indexToRemove, currentGuests) = tuple
                let newPrice = self.ticketPriceRelay.value - (self.activationInfoRelay.value?.ticketPrice ?? 0)
                self.ticketPriceRelay.accept(self.roundPrice(newPrice))
                return currentGuests.enumerated().filter { $0.offset != indexToRemove }.map { $0.element }
            }
            .bind(to: guestsRelay)
            .disposed(by: disposeBag)
        
        doneClicks
            .withLatestFrom(ticketPriceRelay)
            .withLatestFrom(selectedPaymentMethodObs) { ($0, $1) }
            .subscribe(onNext: { [unowned self] tuple in
                let (ticketPrice, selectedPaymentMethod) = tuple
                self.handleDoneButtonClick(ticketPrice: ticketPrice, selectedPaymentMethod: selectedPaymentMethod)
            }).disposed(by: disposeBag)

        doneClicksFlow
            .withLatestFrom(rsvpRequest)
            .ignoreNil()
            .withLatestFrom(activationInfoRelay) { (rsvpRequest, activation) -> (RsvpRequest, Activation)? in
                guard let activation = activation else { return nil }
                return (rsvpRequest, activation)}
            .ignoreNil()
            .subscribe(onNext: { [unowned self] data in
                self.validate(rsvpRequest: data.0, activation: data.1)
            }).disposed(by: disposeBag)
            
        paymentMethodsSubject
            .subscribe(onNext: { [unowned self] paymentMethods in
                self.setDefaultPaymentMethod(usingPaymentMethods: paymentMethods)
            }).disposed(by: disposeBag)
        
        loadUserCards()
        
        profileRepository.knownCardsObs
            .map { $0 as [PaymentMethodOption] }
            .bind(onNext: { [unowned self] cards in
                var paymentMethods: [PaymentMethodOption] = cards
                paymentMethods.insert(ApplePayPaymentMethodOption(), at: 0)
                self.paymentMethodsSubject.accept(paymentMethods)
            })
            .disposed(by: disposeBag)
        
        cancelClicks
            .withLatestFrom(activationInfoRelay)
            .flatMapWithConfirmation(
                using: alertDataSubject,
                alertData: AlertData.confirmActivationCancel(isPayable: activationInfoRelay.value?.isPayable ?? false, sourceName: .activationCancelReservation)
            )
            .ignoreNil()
            .flatMap { [unowned self] activation in self.activationsRepository.cancelRsvp(forActivation: activation).showingProgressBar(with: self) }
            .doOnNext({ [unowned self] response in
                self.handleCancelResponse(response)
            })
            .subscribe()
            .disposed(by: disposeBag)
        
        addCardClicks
            .subscribe(onNext: { [unowned self] _ in
                self.destinationSubject.onNext(.addCard)
            }).disposed(by: disposeBag)
    }
    
    private func loadUserCards() {
        profileRepository.getUserCards()
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    private func setDefaultPaymentMethod(usingPaymentMethods paymentMethods: [PaymentMethodOption]) {
        let defaultMethodIndex = paymentMethods.firstIndex(where: { $0.isDefault }) ?? paymentMethods.firstIndex(where: { $0.type == .applePay})
        selectedPaymentMethodIndexSubject.onNext(defaultMethodIndex)
    }
    
    private func handleBasicResponse<Data>(_ response: ApiResponse<Data>) {
        switch response {
        case .success:
            guard !isInEditMode else { destinationSubject.onNext(.back); return }
            saveRsvpAnalyticEvent(withType: .rsvpCreated)
            if !isInEditMode, !calendarSyncViewProvider.showCalendarSyncViewController() {
                destinationSubject.onNext(.reservationConfirmed)
            } else {
                destinationSubject.onNext(.back)
            }
            incrementRsvpCountHelper.incrementRsvpCountersAfterRsvp()
        case .failure(let error):
            errorSubject.onNext(error.setErrorType(errorType: .guestData))
        }
    }
    
    private func handleCancelResponse<Data>(_ response: ApiResponse<Data>) {
        switch response {
        case .success:
            destinationSubject.onNext(.back)
        case .failure(let error):
            errorSubject.onNext(error.setErrorType(errorType: .guestData))
        }
    }
    
    private func handleRsvpResponse(_ response: RsvpResponse) {
        switch response {
        case .success(let data) where data.data.status == .waitList || data.data.status == .transferableToGuestList:
            saveRsvpAnalyticEvent(withType: .waitlistRsvpCreated)
            guard let activation = activationInfoRelay.value else { return }
            destinationSubject.onNext(.waitlisted(activation: activation))
        default:
            handleBasicResponse(response)
        }
    }
    
    private func handleDoneButtonClick(ticketPrice: Double, selectedPaymentMethod: PaymentMethodOption?) {
        addToGuestListHelper?.successCallback = { [unowned self] in
            self.showCardOrPayableAlertIfNeeded(ticketPrice: ticketPrice, selectedPaymentMethod: selectedPaymentMethod)
        }
        guard let activationId = activationInfoRelay.value?.id else { return }
        self.progressBarSubject.onNext(true)
        addToGuestListHelper?.checkIsAvailablePlacesInActivation(activationId: activationId, guestsWithUserCount: guestsRelay.value.count + 1)
    }
    
    private func showCardOrPayableAlertIfNeeded(ticketPrice: Double, selectedPaymentMethod: PaymentMethodOption?) {
        if emptyCardAlertIsNeeded(isSelectedPaymentMethodEmpty: selectedPaymentMethod == nil) {
            self.progressBarSubject.onNext(false)
            showEmptyCardAlert()
        } else if payableAlertIsNeeded(price: ticketPrice, selectedPaymentMethod: selectedPaymentMethod) {
            self.progressBarSubject.onNext(false)
            showPayableAlert()
        } else {
            doneClicksFlow.emitElement()
        }
    }
    
    private func roundPrice(_ value: Double) -> Double {
        return max((value * 100).rounded() / 100, 0)
    }
    
    private func saveRsvpAnalyticEvent(withType type: AnalyticEventType) {
        if !isInEditMode {
            guard let sessionId = AnalyticService.shared.sessionId,
                let userId = Config.userProfile?.id else { return }
            let event = AnalyticEvent(
                userId: userId,
                deviceToken: Config.deviceToken,
                timestamp: Int64(Date().timeIntervalSince1970 * 1000),
                type: type,
                data: .createForSpecialType(referencedId: analyticEventReferencedId),
                sessionId: sessionId
            )
            AnalyticService.shared.saveEventInDatabase(event)
        }
    }
    
    private func payableAlertIsNeeded(price: Double, selectedPaymentMethod: PaymentMethodOption?) -> Bool {
        guard selectedPaymentMethod?.type == .card else { return false }
        return (activationInfoRelay.value?.isPayable ?? false) && price >= minimumPrice
    }
    
    private func emptyCardAlertIsNeeded(isSelectedPaymentMethodEmpty: Bool) -> Bool {
        return (activationInfoRelay.value?.isPayable ?? false) && isSelectedPaymentMethodEmpty
    }
    
    private func showEmptyCardAlert() {
        alertDataSubject.onNext(AlertData(
            icon: Icons.Cards.remove,
            title: Strings.GuestForm.emptyCardAlert.localized,
            message: nil,
            bottomButtonTitle: Strings.ok.localized.uppercased(),
            analyticAlertSourceName: AnalyticEventAlertSourceName.creditCardRequiredToRsvp
        ))
    }
    
    private func showPayableAlert() {
        alertDataSubject.onNext(AlertData(
            icon: Icons.Cards.add,
            title: Strings.GuestForm.paymentConfirmationAlert.localized,
            message: nil,
            bottomButtonTitle: Strings.yes.localized.uppercased(),
            buttonAction: { [unowned self] in self.doneClicksFlow.emitElement()},
            analyticAlertSourceName: AnalyticEventAlertSourceName.proceedWithPayment
        ))
    }
    
    private func validate(rsvpRequest: RsvpRequest, activation: Activation) {
        let result = rsvpRequestValidationHelper.validate(rsvpRequest: rsvpRequest, validator: validator)
        switch result {
        case .success:
            sendRsvpRequest(rsvpRequest, activation: activation)
        case .failure(let error):
            progressBarSubject.onNext(false)
            errorSubject.onNext(error.setErrorType(errorType: .guestData))
        }
    }
    
    private func sendRsvpRequest(_ rsvpRequest: RsvpRequest, activation: Activation) {
        Observable.just((rsvpRequest, activation))
            .withLatestFrom(selectedPaymentMethodObs) { ($0, $1) }
            .filterMap { [unowned self] (tuple, paymentMethod) -> FilterMap<(RsvpRequest, Activation)> in
                let (_, activation) = tuple
                if activation.isPayable == true && self.ticketPriceRelay.value > self.minimumPrice && paymentMethod?.type == .applePay {
                    self.payUsingApplePay()
                    return .ignore
                } else {
                    return .map(tuple)
                }
            }
            .flatMapFirst { [unowned self] data -> Observable<RsvpResponse> in
                self.finalizeRsvpRequest(data.0, activation: data.1)
            }
            .subscribe(onNext: { [weak self] response in
                self?.handleRsvpResponse(response)
            }).disposed(by: disposeBag)
    }
    
    private func finalizeRsvpRequest(_ rsvpRequest: RsvpRequest, activation: Activation) -> Observable<RsvpResponse> {
        if isInEditMode {
            return activationsRepository.updateRsvp(forActivation: activation, data: rsvpRequest)
                .showingProgressBar(with: self)
        } else {
            return activationsRepository.createRsvp(forActivation: activation, data: rsvpRequest)
                .showingProgressBar(with: self)
        }
    }
    
    private func payUsingApplePay() {
        guard let activation = activationInfoRelay.value else { return }
        guard let currencyCode = activation.currency?.code else {
            alertDataSubject.onNext(AlertData.applePayErrorAlert(withTitle: Strings.ApplePay.unexpectedError.localized))
            return
        }
        
        let paymentItems = ApplePayPaymentSummaryItemsProvider.createPaymentItemsForActivation(usingName: activation.name, price: activation.ticketPrice)
        let paymentData = ApplePayPaymentData(currencyCode: currencyCode, paymentItems: paymentItems)
        do {
            try applePayManager.makePayment(usingData: paymentData)
        } catch ApplePayError.notAvailable {
            handleApplePayError(withTitle: ApplePayError.notAvailable.localizedDescription)
        } catch {
            handleApplePayError(withTitle: Strings.ApplePay.notAvailable.localized)
        }
    }
    
    private func handleApplePayError(withTitle title: String) {
        progressBarSubject.onNext(false)
        alertDataSubject.onNext(AlertData.applePayErrorAlert(withTitle: title))
    }
}

// MARK: Interface logic methods
extension GuestListFormViewModel: GuestListFormLogic {
    
    var addGuestButtonVisibleObs: Observable<Bool> {
        return guestsRelay
            .map { $0.count }
            .withLatestFrom(activationInfoRelay.ignoreNil()) { ($0, $1) }
            .map { (guestCount, activation) in
                return guestCount < activation.membersGuestLimit && activation.isRsvpOpen
        }
    }
    
    var isJustMeInfoVisible: Observable<Bool> {
        return guestsRelay
            .map { guests in
                return guests.isEmpty
        }
    }
    
    var provideEmailInfoViewVisible: Observable<Bool> {
        return guestsRelay
            .withLatestFrom(activationInfoRelay.ignoreNil()) { ($0, $1) }
            .map { (guests, activation) in
                return !guests.isEmpty && activation.isRsvpOpen
        }
    }
    
    var destinationObs: Observable<GuestListFormDestination> {
        return destinationSubject.asObservable()
    }
    
    var isInEditMode: Bool {
        return activationInfoRelay.value?.rsvp != nil
    }
    
    var paymentMethodsObs: Observable<([PaymentMethodOption], PaymentMethodOption?)> {
        return paymentMethodsSubject
            .withLatestFrom(selectedPaymentMethodObs) { ($0, $1) }
            .asObservable()
    }
    
    var guestRemovedObs: Observable<Int> {
        return guestRemovedSubject.asObservable()
    }
    
    var guestAdditionObs: Observable<RsvpGuest> {
        return guestsRelay.pairwise()
            .filter { tuple in tuple.0.count < tuple.1.count }
            .map { $0.1.last }
            .ignoreNil()
    }
    
    var ticketPriceObs: Observable<Double> {
        return ticketPriceRelay
            .asObservable()
    }
    
    func openExternalTicketWebView() {
        guard let ticketURL = activationInfoRelay.value?.ticketURL else { return }
        let data = BaseWebViewData(navBarTitle: nil, url: ticketURL)
        destinationSubject.onNext(.externalTicketWebSite(webViewData: data))
    }
}

// MARK: - ApplePayManagerDelegate

extension GuestListFormViewModel: ApplePayManagerDelegate {
    func applePayManager(_ manager: ApplePayManager, didFinishWith response: StripeTokenResponse, paymentCompletionHandler completion: @escaping (PKPaymentAuthorizationStatus) -> Void) {
        if let token = response.token {
            let tokenRequest = CardTokenRequest(tokenId: token.tokenId)
            cardManagementService.addCard(usingToken: tokenRequest)
                .filterMap { [unowned self] result -> FilterMap<CardData> in
                    switch result {
                    case .success(let item):
                        return .map(item.data)
                    case .failure(let error):
                        completion(.failure)
                        self.applePayManager.paymentErrorToPresent = error
                        return .ignore
                    }
                }
                .withLatestFrom(rsvpRequest) { ($0, $1) }
                .withLatestFrom(activationInfoRelay) { ($0, $1) }
                .flatMapFirst { [unowned self] (tuple, activation) -> Observable<RsvpResponse>  in
                    let (card, rsvpRequest) = tuple
                    let updatedRsvpRequest = RsvpRequest(updatingPreviousRequest: rsvpRequest!, cardId: card.id)
                    return self.finalizeRsvpRequest(updatedRsvpRequest, activation: activation!)
                }
                .subscribe(onNext: { [unowned self] response in
                    switch response {
                    case .success:
                        completion(.success)
                        self.rsvpResponseAfterApplePayPayment = response
                    case .failure(let error):
                        completion(.failure)
                        self.applePayManager.paymentErrorToPresent = error
                        self.loadUserCards()
                    }
                })
                .disposed(by: disposeBag)
            
        } else {
            completion(.failure)
            applePayManager.paymentErrorToPresent = AppError(with: response.error)
        }
    }
    
    func applePayManagerDidDismissPaymentController(_ manager: ApplePayManager) {
        if let error = manager.paymentErrorToPresent {
            errorSubject.onNext(error)
        } else if let response = rsvpResponseAfterApplePayPayment {
            handleRsvpResponse(response)
            rsvpResponseAfterApplePayPayment = nil
        } else {
            progressBarSubject.onNext(false)
        }
    }
}
