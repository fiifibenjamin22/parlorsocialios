//
//  GuestListFormViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 12/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

class GuestListFormViewController: AppViewController {
    
    // MARK: - Properties
    let analyticEventScreen: AnalyticEventScreen? = .createOrUpdateRsvp
    private var viewModel: GuestListFormLogic!
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }
    
    private var editedGuests: [RsvpGuest] {
        return guestsStackView.arrangedSubviews.map { guestView in
            guard let guestView = guestView as? GuestDataView, let showingGuest = guestView.showingGuest else { fatalError("Guests stack should contain only guest views with filled guest") }
            return showingGuest
        }
    }
    
    override var observableSources: ObservableSources? {
        return viewModel
    }

    // MARK: - Views
    private(set) var scrollView: UIScrollView!
    private(set) var contentView: UIView!
    
    private(set) var contentStackView: UIStackView!
    private(set) var guestsStackView: UIStackView!
    private(set) var marginedStackView: UIStackView!
    private(set) var justMeEditInfo: JustMeInformationView!
    private(set) var provideEmailInfoView: UIView!
    private(set) var addGuestBottomSpace: UIView!
    private(set) var addGuestButton: TrackableButton!
    private(set) var cancelReservationButton: TrackableButton!
    
    private(set) var doneButton: UIButton!
    private(set) var informationView: GuestFormActivationInfoView!
    
    private(set) var timeSegmentSelector: DropDownView!
    private(set) var ticketPurchaseGroup: UIView!
    private(set) var stripeButton: StripeButton!
    private(set) var ticketPurchaseSelector: DropDownView!
    private(set) var externalTicketGroup: ExternalTicketGroup!
    private(set) var ticketAtDoorView: TicketAtDoorView!

    private(set) var closeButtonItem: UIBarButtonItem!

    // MARK: - Initialization
    init(forActivation activation: Activation, andGuestsCount guestCount: Int?, and guestSlotsChecked: Bool = false) {
        super.init(nibName: nil, bundle: nil)
        setup(forActivation: activation, andGuestsCount: guestCount, guestSlotsChecked: guestSlotsChecked)
    }
 
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setup(forActivation activation: Activation, andGuestsCount guestCount: Int?, guestSlotsChecked: Bool) {
        self.viewModel = GuestListFormViewModel(
            forActivation: activation,
            andGuestsCount: guestCount,
            guestSlotsChecked: guestSlotsChecked,
            applePayPresentationController: self
        )
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = closeButtonItem
        setupPresentationLogic()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        statusBar?.backgroundColor = .white
        navigationController?.setNavigationBarBorderColor()
    }
    
    @objc private func didTapAddGuest() {
        viewModel.addGuestClicksSubject.emitElement()
    }
    
    @objc func didTapClose() {
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.RSVPDetails.close.rawValue)
        navigationController?.dismiss(animated: true)
    }
    
    @objc private func didTapDoneButton() {
        saveDoneButtonClickAnalyticEvent()
        viewModel.guestsRelay.accept(editedGuests)
        viewModel.doneClicks.emitElement()
    }
    
    @objc private func didTapCancelButton() {
        viewModel.cancelClicks.emitElement()
    }
    
    private func saveDoneButtonClickAnalyticEvent() {
        let event = viewModel.isInEditMode ? AnalyticEventClickableElementName.RSVPDetails.update : AnalyticEventClickableElementName.RSVPDetails.done
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: event.rawValue)
    }
}

// MARK: - Private methods
extension GuestListFormViewController {

    private func setupViews() {
        let builder = GuestListFormViewBuilder(controller: self)
        
        scrollView = builder.buildMainScrollView()
        contentView = builder.buildScrollContentView()
        
        marginedStackView = builder.buildContentMarginedStackView()
        contentStackView = builder.buildContentStackView()
        guestsStackView = builder.buildGuestsStackView()
        justMeEditInfo = builder.buildJustMeInfo()
        provideEmailInfoView = builder.buildProvideEmailInfoView()
        addGuestBottomSpace = builder.buildAddGuestButtonBottomSpace()
        addGuestButton = builder.buildAddGuestButton()
        
        doneButton = builder.buildDoneButton()
        cancelReservationButton = builder.buildRoundedTransparentButton()
        informationView = builder.buildInformationView()
        timeSegmentSelector = builder.buildDropDownView()
        stripeButton = builder.buildStripeButton()
        ticketPurchaseGroup = builder.buildView()
        ticketPurchaseSelector = builder.buildDropDownView()
        externalTicketGroup = builder.buildExternalTicketGroup()
        externalTicketGroup.delegate = self
        ticketAtDoorView = builder.buildTicketAtDoorView()
        
        closeButtonItem = builder.buildCloseItem()
        
        builder.setupViews()
    }

    private func setupPresentationLogic() {
        addGuestButton.addTarget(self, action: #selector(didTapAddGuest), for: .touchUpInside)
        doneButton.addTarget(self, action: #selector(didTapDoneButton), for: .touchUpInside)
        cancelReservationButton.addTarget(self, action: #selector(didTapCancelButton), for: .touchUpInside)
        bindActivationInfo()
        bindGuests()
        bindSegments()
        bindCardSelector()
        bindDestinations()
    }
    
    private func bindDestinations() {
        viewModel.destinationObs
            .subscribe(onNext: { [unowned self] destination in
                self.handle(destination: destination)
            }).disposed(by: disposeBag)
    }
    
    private func handle(destination: GuestListFormDestination) {
        switch destination {
        case .back:
            navigationController?.dismiss(animated: true)
        case .waitlisted, .reservationConfirmed:
            navigationController?.setViewControllers([destination.viewController], animated: true)
        case .addCard:
            router.push(destination: destination)
        case .externalTicketWebSite:
            router.present(destination: destination)
        }
    }
    
    private func bindSegments() {
        timeSegmentSelector.selectionChangedCallback = { [weak self] index in
            AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.RSVPDetails.timeSegmentsItem.rawValue)
            self?.viewModel.selectedSegmentIndexSubject.onNext(index)
        }
        
        ticketPurchaseSelector.selectionChangedCallback = { [weak self] index in
            AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.RSVPDetails.paymentMethodsItem.rawValue)
            self?.viewModel.selectedPaymentMethodIndexSubject.onNext(index)
        }
    }
    
    private func adjustUiToCurrentMode() {
        navigationItem.title = viewModel.isInEditMode ? Strings.GuestForm.editTitle.localized.uppercased() : Strings.GuestForm.rsvpDetails.localized.uppercased()
        doneButton.setTitle(viewModel.isInEditMode ? Strings.GuestForm.update.localized.uppercased() : Strings.GuestForm.done.localized.uppercased(), for: .normal)
        cancelReservationButton.isHidden = !viewModel.isInEditMode
        !viewModel.isInEditMode ? marginedStackView.removeLastSubview() : marginedStackView.addDefaultSeperator(withColor: .appBackground, size: 40)
    }

    private func bindActivationInfo() {
        viewModel.activationInfoRelay
            .subscribe(
                onNext: { [unowned self] activation in
                    guard let activation = activation else { return }
                    self.handle(activation: activation)
                },
                onError: { error in print(error) }
            ).disposed(by: disposeBag)
        
    }
    
    private func bindCardSelector() {
        Observable.combineLatest(viewModel.paymentMethodsObs, viewModel.activationInfoRelay) { ($0, $1) }
            .subscribe(
                onNext: { [unowned self] (tuple, activation) in
                    let (paymentMethods, selectedMethod) = tuple
                    self.ticketPurchaseSelector.configure(
                        with: DropDownData(
                            icon: Icons.GuestsForm.card,
                            title: Strings.GuestForm.price.localized,
                            popupTitle: Strings.GuestForm.ticket.localized,
                            additionalInformation: Strings.GuestForm.ticketInfo.localized,
                            emptyOptionsButtonText: Strings.GuestForm.addCard.localized.uppercased(),
                            options: paymentMethods,
                            popupAddBottomButtonTitle: Strings.Cards.addAnother.localized.uppercased(),
                            canOpenOptionsPopup: activation?.isRsvpOpen))
                    self.ticketPurchaseSelector.selectedOption = selectedMethod
                    self.ticketPurchaseSelector.addOptionCallback = { [unowned self] in self.viewModel.addCardClicks.emitElement() }
                    self.ticketPurchaseSelector.addOptionInPopupCallback = { [unowned self] in
                        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.RSVPDetails.paymentMethodsAddAnother.rawValue)
                        self.viewModel.addCardClicks.emitElement() }
                }
            ).disposed(by: disposeBag)
        
        viewModel.ticketPriceObs
            .subscribe(onNext: { [unowned self] price in
                self.ticketPurchaseSelector.titleValue = price
            }).disposed(by: disposeBag)
    }
    
    private func bindGuests() {
        viewModel.guestsRelay
            .take(1) // other changes made with remove / add animations
            .withLatestFrom(viewModel.activationInfoRelay.ignoreNil()) { ($0, $1) }
            .subscribe(
                onNext: { [unowned self] (guests, activation) in
                    let guestViews = guests.enumerated().map { index, guest in self.createView(for: guest, activation: activation, withIndex: index + 1) }
                    self.add(guestViews: guestViews, to: self.guestsStackView)
                },
                onError: { error in print(error) }
            ).disposed(by: disposeBag)
        
        viewModel.isJustMeInfoVisible
            .doOnNext { [unowned self] isVisible in
                self.justMeEditInfo.isHidden = !isVisible
            }.subscribe().disposed(by: disposeBag)
        
        viewModel.provideEmailInfoViewVisible
            .doOnNext { [unowned self] isVisible in
                self.provideEmailInfoView.isHidden = !isVisible
            }.subscribe().disposed(by: disposeBag)
        
        viewModel
            .guestAdditionObs
            .subscribe(onNext: { [unowned self] newGuest in
                self.addGuestViewWithAnimation(forGuest: newGuest)
            }).disposed(by: disposeBag)
        
        viewModel.addGuestButtonVisibleObs
            .doOnNext { [unowned self] isVisible in
                [self.addGuestBottomSpace, self.addGuestButton].forEach { $0.isHidden = !isVisible }
            }.subscribe().disposed(by: disposeBag)
    }

    private func createView(for guest: RsvpGuest, activation: Activation?, withIndex index: Int) -> GuestDataView {
        let guestView = GuestDataView().apply {
            $0.configure(with: guest, activation: activation)
            $0.guestNumber = index
            $0.delegate = self
        }
        return guestView
    }
    
    private func add(guestViews: [GuestDataView], to stackView: UIStackView) {
        stackView.removeArrangedSubviews()
        stackView.addArrangedSubviews(guestViews)
    }
    
    private func addGuestViewWithAnimation(forGuest guest: RsvpGuest) {
        let newViewIndex = guestsStackView.arrangedSubviews.count + 1
        let newView = createView(for: guest, activation: nil, withIndex: newViewIndex)
        let fakeView = UIView() //Adding this view to stack seeems to solve the issue of strange animation of first element
        newView.isHidden = true
        newView.alpha = 0
        guestsStackView.addArrangedSubview(fakeView)
        guestsStackView.addArrangedSubview(newView)
    
        UIView.animate(withDuration: 0.25, animations: {
            newView.isHidden = false
            newView.alpha = 1
        }) { _ in
            self.guestsStackView.removeArrangedSubview(fakeView)
        }
    }
    
    private func removeGuestViewWithAnimation(_ guestView: GuestDataView) {
        guestView.alpha = 0
        UIView.animate(withDuration: 0.25, animations: { [unowned guestView] in
            guestView.isHidden = true
        }) { _ in
            self.guestsStackView.removeArrangedSubview(guestView)
            self.updateGuestIndexes()
        }
    }
    
    private func updateGuestIndexes() {
        for (index, guestView) in guestsStackView.arrangedSubviews.enumerated() {
            guard let guestView = guestView as? GuestDataView else { fatalError("Guest stack should contain only guest views") }
            guestView.guestNumber = index + 1
        }
    }
    
    private func handle(activation: Activation) {
        informationView.configure(with: activation)
        ticketPurchaseGroup.isHidden = !(activation.isPayable ?? false)
        externalTicketGroup.isHidden = !activation.ticketExternal
        ticketAtDoorView.isHidden = !activation.isPurchasedAtTheDoor
        adjustUiToCurrentMode()
        timeSegmentSelector.configure(
            with: DropDownData(
                icon: Icons.GuestsForm.timePeriod,
                title: Strings.GuestForm.timeOfReservation.localized,
                popupTitle: Strings.GuestForm.timeOfReservation.localized,
                options: activation.segments,
                canOpenOptionsPopup: activation.isRsvpOpen))
        timeSegmentSelector.selectedOption = activation.rsvp?.segment(basedOn: activation) ?? activation.segments.first
        ticketPurchaseSelector.currency = activation.currency
        
        // TODO: Refactor - add this to viewModel and connect with isJustMeInfoVisible observable
        if activation.membersGuestLimit == 0 {
            justMeEditInfo.bottomInfo.text = Strings.GuestForm.onlyMembersEvent.localized
        } else {
            if activation.isRsvpOpen {
                justMeEditInfo.bottomInfo.text = Strings.GuestForm.noGuestInfoClick.localized
            } else {
                justMeEditInfo.bottomInfo.text = Strings.GuestForm.noGuestInfoEditableIsClosed.localized
            }
        }
    }
}

extension GuestListFormViewController: GuestDataViewDelegate {
    
    func guestDataView(_ guestDataView: GuestDataView, didTapRemoveOnGuest guest: RsvpGuest, withNumber number: Int) {
        viewModel.guestRemovedSubject.onNext(number - 1)
        removeGuestViewWithAnimation(guestDataView)
    }
    
}

// MARK: - ExternalTicketGroupDelegate
extension GuestListFormViewController: ExternalTicketGroupDelegate {
    func didTapExternalTicketButton() {
        viewModel.openExternalTicketWebView()
    }
}
