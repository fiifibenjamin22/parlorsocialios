//
//  RsvpRequestValidationHelper.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 11/02/2020.
//

import Foundation

class RsvpRequestValidationHelper {
    
    func validate(rsvpRequest: RsvpRequest?, validator: Validator) -> Validator.Result {
        var validatedInputs: [ValidatedInput<String?>] = []
        rsvpRequest?.guests.forEach { guest in
            let firstNameValidatedInput = ValidatedInput(rules: [StringNotBlank(), StringMaxLength(191)],
                                                         value: guest.firstName, name: Strings.GuestForm.name.localized)
            let lastNameValidatedInput = ValidatedInput(rules: [StringNotBlank(), StringMaxLength(191)],
                                                        value: guest.lastName, name: Strings.GuestForm.surname.localized)
            validatedInputs.append(contentsOf: [firstNameValidatedInput, lastNameValidatedInput])
            if guest.isNominated == true {
                 validatedInputs.append(ValidatedInput(rules: [StringNotBlank(), StringMaxLength(191)],
                                                             value: guest.email, name: Strings.GuestForm.email.localized))
            }
        }
        return validator.validateWithResult(for: validatedInputs)
    }
    
}
