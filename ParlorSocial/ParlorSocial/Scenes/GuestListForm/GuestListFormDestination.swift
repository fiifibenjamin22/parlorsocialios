//
//  GuestListFormDestination.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 13/05/2019.
//

import Foundation
import UIKit

enum GuestListFormDestination {
    case back
    case waitlisted(activation: Activation)
    case addCard
    case reservationConfirmed
    case externalTicketWebSite(webViewData: BaseWebViewData)
}

extension GuestListFormDestination: Destination {
    
    var viewController: UIViewController {
        switch self {
        case .waitlisted(let activation):
            return WaitlistInfoViewController(withActivation: activation)
        case .addCard:
            return AddCardViewController(withApplyingType: .add)
        case .reservationConfirmed:
            return RsvpConfirmedViewController()
        case .externalTicketWebSite(let data):
            return BaseWebViewController(data: data).embedInNavigationController()
        case .back:
            fatalError()
        }
    }
}
