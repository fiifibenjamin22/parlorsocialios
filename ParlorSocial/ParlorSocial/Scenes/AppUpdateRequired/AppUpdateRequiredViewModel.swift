//  AppUpdateRequiredViewModel.swift
//  ParlorSocialClub
//
//  Created on 07/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import RxSwift

protocol AppUpdateRequiredLogic: BaseViewModelLogic {
    func updateApp()
}

class AppUpdateRequiredViewModel: BaseViewModel {
    
    // MARK: - Properties
    
    var analyticEventReferencedId: Int?
}

// MARK: - AppUpdateRequiredLogic

extension AppUpdateRequiredViewModel: AppUpdateRequiredLogic {
    func updateApp() {
        let appUrl = Config.appStoreUrl
        if UIApplication.shared.canOpenURL(appUrl) {
            UIApplication.shared.open(appUrl)
        }
    }
}
