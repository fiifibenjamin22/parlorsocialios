//  AppUpdateRequiredViewBuilder.swift
//  ParlorSocialClub
//
//  Created on 07/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

class AppUpdateRequiredViewBuilder {
    
    // MARK: - Properties
    
    private unowned let controller: AppUpdateRequiredViewController
    private var view: UIView! { return controller.view }
    
    // MARK: - Init
    
    init(controller: AppUpdateRequiredViewController) {
        self.controller = controller
    }
    
    // MARK: - Setup
    
    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
    
    private func setupProperties() {
        view.backgroundColor = .white
        
        controller.apply {
            $0.titleLabel.lineHeight = 26
            $0.titleLabel.text = Strings.AppUpdateRequired.updateRequired.localized
            $0.subtitleLabel.lineHeight = 18
            $0.subtitleLabel.text = Strings.AppUpdateRequired.updateInfo.localized
            $0.updateButton.setTitle(Strings.AppUpdateRequired.update.localized, for: .normal)
        }
    }

    private func setupHierarchy() {
        controller.apply {
            view.addSubviews([$0.iconImageView, $0.titleLabel, $0.subtitleLabel, $0.updateButton])
        }
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.iconImageView.centerXAnchor.equal(to: view.centerXAnchor)
            $0.iconImageView.safeAreaLayoutGuide.topAnchor.equal(to: view.safeAreaLayoutGuide.topAnchor, constant: 81)
            $0.iconImageView.heightAnchor.equalTo(constant: 100)
            $0.iconImageView.widthAnchor.equalTo(constant: 100)

            $0.titleLabel.topAnchor.equal(to: $0.iconImageView.bottomAnchor, constant: 35)
            $0.titleLabel.edgesToParent(anchors: [.leading, .trailing], insets: .init(padding: Constants.horizontalMarginBig))
            
            $0.subtitleLabel.topAnchor.equal(to: $0.titleLabel.bottomAnchor, constant: 15)
            $0.subtitleLabel.edgesToParent(anchors: [.leading, .trailing], insets: .init(padding: Constants.horizontalMarginBig))
            
            $0.updateButton.centerXAnchor.equal(to: view.centerXAnchor)
            $0.updateButton.edgesToParent(anchors: [.leading, .trailing, .bottom], insets: .margins(left: Constants.horizontalMarginBig, bottom: 92, right: Constants.horizontalMarginBig))
            $0.updateButton.heightAnchor.equalTo(constant: Constants.bigButtonHeight)
        }
    }
    
    // MARK: - Build views methods
    
    func buildIconImageView() -> UIImageView {
        return UIImageView().manualLayoutable().apply {
            $0.image = Icons.AppUpdateRequired.appUpdateLogo
        }
    }
    
    func buildTitleLabel() -> ParlorLabel {
        return ParlorLabel.styled(
            withTextColor: .appTextBright,
            withFont: UIFont.custom(ofSize: Constants.FontSize.subtitle, font: UIFont.Roboto.medium),
            alignment: .center,
            numberOfLines: 1
        )
    }
    
    func buildSubtitleLabel() -> ParlorLabel {
        return ParlorLabel.styled(
            withTextColor: .appTextSemiLight,
            withFont: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.regular),
            alignment: .center,
            numberOfLines: 2
        ).apply {
            $0.adjustsFontSizeToFitWidth = true
            $0.minimumScaleFactor = 0.7
        }
    }
    
    func buildUpdateButton() -> RoundedButton {
        return RoundedButton().manualLayoutable().apply {
            $0.backgroundColor = .black
            $0.setTitleColor(.white, for: .normal)
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium)
            $0.applyTouchAnimation()
        }
    }
}
