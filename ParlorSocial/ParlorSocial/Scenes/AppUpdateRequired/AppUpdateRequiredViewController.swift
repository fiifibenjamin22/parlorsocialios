//  AppUpdateRequiredViewController.swift
//  ParlorSocialClub
//
//  Created on 07/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

class AppUpdateRequiredViewController: AppViewController {
    
    // MARK: - Views
    
    private(set) var iconImageView: UIImageView!
    private(set) var titleLabel: ParlorLabel!
    private(set) var subtitleLabel: ParlorLabel!
    private(set) var updateButton: RoundedButton!

    // MARK: - Properties
    
    private var viewModel: AppUpdateRequiredLogic!
    var analyticEventScreen: AnalyticEventScreen? = .appUpdateRequired
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }
    override var observableSources: ObservableSources? {
        return viewModel
    }
        
    // MARK: - Initialization
    
    init() {
        super.init(nibName: nil, bundle: nil)
        
        viewModel = AppUpdateRequiredViewModel()
    }

    required init?(coder aDecoder: NSCoder) {
       fatalError()
    }
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        setupActions()
    }
    
    // MARK: - Setup
    
    private func setupViews() {
        let builder = AppUpdateRequiredViewBuilder(controller: self)
        iconImageView = builder.buildIconImageView()
        titleLabel = builder.buildTitleLabel()
        subtitleLabel = builder.buildSubtitleLabel()
        updateButton = builder.buildUpdateButton()
        
        builder.setupViews()
    }
    
    private func setupActions() {
        updateButton.addTarget(self, action: #selector(didTapUpdateButton), for: .touchUpInside)
    }
    
    // MARK: - Actions
    
    @objc private func didTapUpdateButton() {
        viewModel.updateApp()
    }
}
