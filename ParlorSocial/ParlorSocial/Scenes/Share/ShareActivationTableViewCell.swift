//
//  ShareActivationTableViewCell.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 30/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

class ShareActivationTableViewCell: UITableViewCell {
    private let iconImageView = UIImageView().manualLayoutable()
    private let nameLabel = UILabel().manualLayoutable()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setup() {
        setupImageView()
        setupNameLabel()
    }

    private func setupImageView() {
        contentView.addSubview(iconImageView)

        iconImageView.leadingAnchor.equal(to: contentView.leadingAnchor, constant: Constants.ShareActivationTableViewCell.NameLabel.leadingInset)
        iconImageView.centerYAnchor.equal(to: contentView.centerYAnchor)
        iconImageView.heightAnchor.equalTo(constant: Constants.ShareActivationTableViewCell.ImageView.size)
        iconImageView.widthAnchor.equalTo(constant: Constants.ShareActivationTableViewCell.ImageView.size)
    }

    private func setupNameLabel() {
        nameLabel.font = Constants.ShareActivationTableViewCell.NameLabel.font

        contentView.addSubview(nameLabel)

        nameLabel.trailingAnchor.equal(to: contentView.trailingAnchor, constant: Constants.ShareActivationTableViewCell.NameLabel.trailingInset)
        nameLabel.centerYAnchor.equal(to: contentView.centerYAnchor)
        nameLabel.leadingAnchor.equal(to: iconImageView.trailingAnchor, constant: Constants.ShareActivationTableViewCell.NameLabel.trailingInset)
    }
}

extension ShareActivationTableViewCell: ConfigurableCell {
    typealias Model = ShareActivationOption

    func configure(with model: ShareActivationOption) {
        iconImageView.image = model.appearance.image
        nameLabel.text = model.appearance.name
    }
}

private extension Constants {
    enum ShareActivationTableViewCell {
        enum ImageView {
            static let size: CGFloat = 29.0
            static let leadingInset: CGFloat = 23.0
        }
        enum NameLabel {
            static let font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.regular)
            static let leadingInset: CGFloat = 15.0
            static let trailingInset: CGFloat = 23.0
        }
    }
}
