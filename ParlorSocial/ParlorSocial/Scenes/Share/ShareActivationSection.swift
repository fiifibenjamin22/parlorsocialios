//
//  ShareActivationSection.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 30/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class ShareActivationSection: BasicTableSection<ShareActivationOption, ShareActivationTableViewCell> {
    override func didSelectItem(_ tableView: UITableView, at indexPath: IndexPath) {
        super.didSelectItem(tableView, at: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension ShareActivationSection {
    static func make(
        for options: [ShareActivationOption],
        itemSelector: @escaping (ShareActivationOption) -> Void
    ) -> [TableSection] {
        return [ShareActivationSection(items: options, itemSelector: itemSelector)]
    }
}
