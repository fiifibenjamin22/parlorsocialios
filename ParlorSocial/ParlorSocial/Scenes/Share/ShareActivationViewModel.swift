//
//  ShareActivationViewModel.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 30/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import RxSwift
import RxCocoa
import MessageUI

protocol ShareActivationLogic: BaseViewModelLogic {
    var shareSectionsObs: Observable<[TableSection]> { get }
    var destinationObs: Observable<ShareActivationDestination> { get }
    var moreOptionsSelectedObs: Observable<ShareData> { get }
}

final class ShareActivationViewModel: BaseViewModel {
    var analyticEventReferencedId: Int?

    private var availableShareOptions: [ShareActivationOption] { return ShareActivationOption.allCases.filter { $0.isAvailable } }

    private lazy var optionSelector: (ShareActivationOption) -> Void = { [unowned self] option in
        self.shareOptionDidSelect(option: option)
    }

    let shareSectionsRelay = BehaviorRelay<[TableSection]>(value: [])

    private let destinationSubject: PublishSubject<ShareActivationDestination> = .init()
    private let moreOptionsSelectedSubject: PublishSubject<ShareData> = .init()
    private let shareData: ShareData
    private let pasteboard: UIPasteboard

    init(shareData: ShareData, pasteboard: UIPasteboard) {
        self.shareData = shareData
        self.pasteboard = pasteboard

        super.init()

        setup()
    }

    private func setup() {
        let tableSections = createTableSections(from: availableShareOptions)
        shareSectionsRelay.accept(tableSections)
    }

    private func createTableSections(from options: [ShareActivationOption]) -> [TableSection] {
        return ShareActivationSection.make(
            for: options,
            itemSelector: self.optionSelector
        )
    }

    private func shareOptionDidSelect(option: ShareActivationOption) {
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: option.analyticSelectedOption.rawValue)
        switch option {
        case .more:
            moreOptionsSelectedSubject.onNext(shareData)
        case .copy:
            pasteboard.string = shareData.url
            closeViewSubject.emitElement()
        case .messages:
            destinationSubject.onNext(.smsComposer(data: shareData, delegate: self))
        case .mail:
            destinationSubject.onNext(.mailComposer(data: shareData, delegate: self))
        case .messenger:
            guard let url = createMessengerShareContentSchemeUrl(with: shareData) else { return closeViewSubject.emitElement() }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        case .whatsapp:
            guard let url = createWhatsappShareContentSchemeUrl(with: shareData) else { return closeViewSubject.emitElement() }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }

    private func createMessengerShareContentSchemeUrl(with data: ShareData) -> URL? {
        let string = Strings.ShareActivation.Scheme.messengerShare.rawValue + "?link=\(data.url)"
        return URL(string: string)
    }

    private func createWhatsappShareContentSchemeUrl(with data: ShareData) -> URL? {
        let string = Strings.ShareActivation.Scheme.whatsappShare.rawValue + "?text=\(data.content)"
        return URL(string: string.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))
    }
}

// MARK: Interface logic methods
extension ShareActivationViewModel: ShareActivationLogic {
    var shareSectionsObs: Observable<[TableSection]> {
        return shareSectionsRelay.asObservable()
    }

    var destinationObs: Observable<ShareActivationDestination> {
        return destinationSubject.asObservable()
    }

    var moreOptionsSelectedObs: Observable<ShareData> {
        return moreOptionsSelectedSubject.asObservable()
    }
}

extension ShareActivationViewModel: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        closeViewSubject.emitElement()
    }
}

extension ShareActivationViewModel: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        closeViewSubject.emitElement()
    }
}
