//
//  ShareActivationDestination.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 02/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit
import MessageUI

enum ShareActivationDestination {
    case smsComposer(data: ShareData, delegate: MFMessageComposeViewControllerDelegate)
    case mailComposer(data: ShareData, delegate: MFMailComposeViewControllerDelegate)
}

extension ShareActivationDestination: Destination {
    var viewController: UIViewController {
        switch self {
        case .smsComposer(let data, let delegate):
            let controller = MFMessageComposeViewController()
            controller.body = data.content
            controller.messageComposeDelegate = delegate
            controller.view.tintColor = .black

            return controller
        case .mailComposer(let data, let delegate):
            let controller = MFMailComposeViewController()
            controller.setSubject(data.emailTitle)
            controller.setMessageBody(data.content, isHTML: false)
            controller.mailComposeDelegate = delegate
            controller.view.tintColor = .black

            return controller
        }
    }
}
