//
//  ShareActivationViewController.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 30/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

protocol ShareActivationDelegate: class {
    func presentMoreShareOptions(with content: ShareData)
}

final class ShareActivationViewController: SemiModalViewController {
    private weak var delegate: ShareActivationDelegate!

    private var tableManager: TableViewManager!
    private var tableViewHeightConstraint: NSLayoutConstraint!

    private let viewModel: ShareActivationLogic!
    private let tableView = UITableView().manualLayoutable()

    init(viewModel: ShareActivationLogic, delegate: ShareActivationDelegate) {
        self.viewModel = viewModel
        self.delegate = delegate

         super.init(contentHeight: .automatic)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        let maxHeight = view.bounds.height * Constants.ShareActivationViewController.Content.heightMultiplier
        tableViewHeightConstraint.constant = min(tableView.contentSize.height, maxHeight)
    }

    override func closeButtonDidTap() {
        dismiss(animated: true)
    }

    private func setup() {
        setupTableView()
        bindSections()
    }

    private func setupTableView() {
        tableView.registerCell(ShareActivationTableViewCell.self)
        tableView.rowHeight = Constants.ShareActivationViewController.TableView.rowHeight
        tableView.separatorInset = Constants.ShareActivationViewController.TableView.separatorInset
        tableView.separatorColor = .lightSeparator
        tableView.tableFooterView = UIView()

        tableManager = TableViewManager(tableView: tableView, delegate: self)

        contentView.addSubview(tableView)

        tableView.edgesToParent()
        tableViewHeightConstraint = tableView.heightAnchor.equalTo(constant: 0).activate()
    }

    private func bindSections() {
        viewModel.shareSectionsObs
            .bind { [unowned self] elements in
                self.tableManager.setupWith(sections: elements)
            }.disposed(by: disposeBag)

        viewModel.closeObs
            .bind { [unowned self] _ in
                self.dismiss()
            }.disposed(by: disposeBag)

        viewModel.destinationObs
            .bind { [unowned self] destination in
                self.handle(destination: destination)
            }.disposed(by: disposeBag)

        viewModel.moreOptionsSelectedObs
            .bind { [unowned self] content in
                self.dismiss(animated: true) {
                    self.delegate?.presentMoreShareOptions(with: content)
                }
            }.disposed(by: disposeBag)
    }

    private func handle(destination: ShareActivationDestination) {
        switch destination {
        case .smsComposer, .mailComposer: router.present(destination: destination)
        }
    }
}

// MARK: - Table Manager Delegate
extension ShareActivationViewController: TableManagerDelegate {
    func didSwipeForRefresh() {}

    func loadMoreData() {}

    func tableViewDidScroll(_ scrollView: UIScrollView) { }
}

private extension Constants {
    enum ShareActivationViewController {
        enum TableView {
            static let rowHeight: CGFloat = 50.0
            static let separatorInset = UIEdgeInsets(horizontal: 22)
        }

        enum Content {
            static let heightMultiplier: CGFloat = 0.77
        }
    }
}
