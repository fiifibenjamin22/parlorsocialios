//
//  ShareActivationOption.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 02/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit
import MessageUI

enum ShareActivationOption: CaseIterable {
    case messages
    case mail
    case messenger
    case whatsapp
    case copy
    case more

    struct Appearance {
        let image: UIImage
        let name: String
    }

    var appearance: Appearance {
        switch self {
        case .whatsapp:
            return Appearance(
                image: Icons.Share.whatsapp,
                name: Strings.ShareActivation.whatsapp.localized
            )
        case .messenger:
            return Appearance(
                image: Icons.Share.messenger,
                name: Strings.ShareActivation.messenger.localized
            )
        case .messages:
            return Appearance(
                image: Icons.Share.messages,
                name: Strings.ShareActivation.messages.localized
            )
        case .mail:
            return Appearance(
                image: Icons.Share.mail,
                name: Strings.ShareActivation.mail.localized
            )
        case .more:
            return Appearance(
                image: Icons.Share.more,
                name: Strings.ShareActivation.more.localized
            )
        case .copy:
            return Appearance(
                image: Icons.Share.copy,
                name: Strings.ShareActivation.copyLink.localized
            )
        }
    }

    var analyticSelectedOption: AnalyticEventClickableElementName.ShareActivationScreen {
        switch self {
        case .messages: return .messages
        case .mail: return .mail
        case .messenger: return .messenger
        case .whatsapp: return .whatsapp
        case .copy: return .copy
        case .more: return .more
        }
    }

    var isAvailable: Bool {
        switch self {
        case .messages:
            return MFMessageComposeViewController.canSendText()
        case .mail:
            return MFMailComposeViewController.canSendMail()
        case .messenger, .whatsapp:
            guard let scheme = dedicatedAppScheme,
                let url = URL(string: scheme) else { return false }
            return UIApplication.shared.canOpenURL(url)
        case .copy, .more:
            return true
        }
    }

    private var dedicatedAppScheme: String? {
        let scheme = Strings.ShareActivation.Scheme.self
        switch self {
        case .messenger: return scheme.messenger.rawValue
        case .whatsapp: return scheme.whatsapp.rawValue
        default: return nil
        }
    }
}
