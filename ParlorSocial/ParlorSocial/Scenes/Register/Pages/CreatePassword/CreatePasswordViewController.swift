//
//  CreatePasswordViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 23/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class CreatePasswordViewController: RegisterPageViewController {

    // MARK: - Views
    private(set) var iconedMessage: IconedMessageView!
    private(set) var newPasswordField: LoginTextField!
    
    // MARK: - Properties

    // MARK: - Initialization
    init() {
        super.init(nibName: nil, bundle: nil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() { }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
    }

}

// MARK: - Private methods
extension CreatePasswordViewController {

    private func setupViews() {
        let builder = CreatePasswordViewBuilder(controller: self)
        self.iconedMessage = builder.buildIconedMessage()
        self.newPasswordField = builder.buildNewPasswordTextField()
        builder.setupViews()
    }

    private func setupPresentationLogic() {

    }

}
