//
//  CreatePasswordViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 23/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class CreatePasswordViewBuilder {

    private unowned let controller: CreatePasswordViewController
    private var view: UIView! { return controller.view }

    init(controller: CreatePasswordViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension CreatePasswordViewBuilder {

    private func setupProperties() {
        controller.apply {
            $0.newPasswordField.placeholder = Strings.ResetPassword.newPassword.localized
            $0.iconedMessage.messageLabel.isHidden = true
        }
    }
    
    private func setupHierarchy() {
        controller.apply {
            view.addSubviews([$0.iconedMessage, $0.newPasswordField])
        }
    }
    
    private func setupAutoLayout() {
        controller.apply {
            $0.iconedMessage.topAnchor.equal(to: view.topAnchor)
            $0.iconedMessage.edgesToParent(anchors: [.leading, .trailing], padding: Constants.horizontalMarginBig)
            
            $0.newPasswordField.edgesToParent(anchors: [.leading, .trailing], padding: Constants.horizontalMarginBig)
            $0.newPasswordField.constraintByCenteringVerticallyBeetween($0.iconedMessage.bottomAnchor, and: view.bottomAnchor)
            $0.newPasswordField.heightAnchor.equalTo(constant: Constants.bigButtonHeight)
        }
    }

}

// MARK: - Public build methods
extension CreatePasswordViewBuilder {
    func buildIconedMessage() -> IconedMessageView {
        return IconedMessageView(
            icon: Icons.ResetPassword.createNew,
            title: Strings.ResetPassword.createNew.localized,
            message: "")
            .manualLayoutable()
    }
    
    func buildNewPasswordTextField() -> LoginTextField {
        return LoginTextField().manualLayoutable().withLightBackgroundStyle().apply {
            $0.isPasswordField = true
        }
    }
}
