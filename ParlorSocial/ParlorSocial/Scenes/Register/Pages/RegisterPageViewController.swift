//
//  RegisterPageViewController.swift
//  
//
//  Created by Benjamin Acquah on 23/04/2019.
//

import UIKit
import RxSwift

class RegisterPageViewController: AppViewController {
    let analyticEventScreen: AnalyticEventScreen? = nil
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return nil
    }
    
    var viewModel: RegisterLogic {
        guard let controller = parent?.parent as? RegisterViewController else { fatalError() }
        return controller.viewModel
    }
}
