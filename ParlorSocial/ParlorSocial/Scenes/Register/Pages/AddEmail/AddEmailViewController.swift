//
//  AddEmailViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 23/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class AddEmailViewController: RegisterPageViewController {
    
    private(set) var iconedMessage: IconedMessageView!
    private(set) var emailTextField: UITextField!

    // MARK: - Properties

    // MARK: - Initialization
    init() {
        super.init(nibName: nil, bundle: nil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() { }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
    }

}

// MARK: - Private methods
extension AddEmailViewController {

    private func setupViews() {
        let builder = AddEmailViewBuilder(controller: self)
        self.emailTextField = builder.buildEmailTextField()
        self.iconedMessage = builder.buildIconedMessageView()
        builder.setupViews()
    }

    private func setupPresentationLogic() {

    }

}
