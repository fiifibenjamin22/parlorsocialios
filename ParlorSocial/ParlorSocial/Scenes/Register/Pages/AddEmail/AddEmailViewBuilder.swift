//
//  AddEmailViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 23/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class AddEmailViewBuilder {

    private unowned let controller: AddEmailViewController
    private var view: UIView! { return controller.view }

    init(controller: AddEmailViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension AddEmailViewBuilder {

    private func setupProperties() {
        controller.apply {
            $0.emailTextField.placeholder = Strings.Register.emailHint.localized
        }
    }
    
    private func setupHierarchy() {
        controller.apply {
            view.addSubviews([$0.iconedMessage, $0.emailTextField])
        }
    }
    
    private func setupAutoLayout() {
        controller.apply {
            $0.iconedMessage.topAnchor.equal(to: view.topAnchor)
            $0.iconedMessage.edgesToParent(anchors: [.leading, .trailing], padding: Constants.horizontalMarginBig)
            
            $0.emailTextField.edgesToParent(anchors: [.leading, .trailing], padding: Constants.horizontalMarginBig)
            $0.emailTextField.heightAnchor.equalTo(constant: Constants.bigButtonHeight)
            $0.emailTextField.topAnchor.equal(to: $0.iconedMessage.bottomAnchor, constant: Constants.AddEmail.emailTopMargin)
        }
    }

}

// MARK: - Public build methods
extension AddEmailViewBuilder {

    func buildEmailTextField() -> UITextField {
        return ParlorTextField().manualLayoutable().apply {
            $0.keyboardType = .emailAddress
            $0.autocapitalizationType = .none
            $0.autocorrectionType = .no
            }.withLightBackgroundStyle()
    }
    
    func buildIconedMessageView() -> IconedMessageView {
        return IconedMessageView(
            icon: Icons.Register.addEmail,
            title: Strings.Register.addEmail.localized,
            message: Strings.Register.addEmailMessage.localized)
            .manualLayoutable()
            .apply { $0.roundedIcon.backgroundColor = UIColor.appBackground }
    }

}

private extension Constants {
    
    enum AddEmail {
        static let emailTopMargin: CGFloat = 42
    }
}
