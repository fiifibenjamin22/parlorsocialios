//
//  BirthDateViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 23/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class BirthDateViewBuilder {

    private unowned let controller: BirthDateViewController
    private var view: UIView! { return controller.view }

    init(controller: BirthDateViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension BirthDateViewBuilder {

    private func setupProperties() {
        controller.birthDateField.placeholder = Strings.Register.addBirthDatePlaceholder.localized
    }

    private func setupHierarchy() {
        view.addSubviews([controller.iconedMessage, controller.birthDateField])
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.iconedMessage.topAnchor.equal(to: view.topAnchor)
            $0.iconedMessage.edgesToParent(anchors: [.leading, .trailing], padding: Constants.horizontalMarginBig)
            
            $0.birthDateField.edgesToParent(anchors: [.leading, .trailing], padding: Constants.horizontalMarginBig)
            $0.birthDateField.heightAnchor.equalTo(constant: Constants.bigButtonHeight)
            $0.birthDateField.topAnchor.equal(to: $0.iconedMessage.bottomAnchor, constant: 60)
        }
    }

}

// MARK: - Public build methods
extension BirthDateViewBuilder {

    func buildIconedMessage() -> IconedMessageView {
        return IconedMessageView(icon: Icons.Register.birthDate,
                                 title: Strings.Register.addBirthDate.localized,
                                 message: Strings.Register.addEmailMessage.localized).manualLayoutable()
    }
    
    func buildBirthdayTextField() -> ParlorTextField {
        return ParlorTextField().manualLayoutable().apply {
            $0.textAlignment = .center
            }.withLightBackgroundStyle()
    }
}
