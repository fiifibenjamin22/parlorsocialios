//
//  BirthDateViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 23/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class BirthDateViewController: RegisterPageViewController {
    
    // MARK: - Views
    private(set) var iconedMessage: IconedMessageView!
    private(set) var birthDateField: ParlorTextField!

    // MARK: - Properties
    private(set) lazy var datePicker: UIDatePicker = {
        return UIDatePicker().apply {
            $0.datePickerMode = .date
        }
    }()

    // MARK: - Initialization
    init() {
        super.init(nibName: nil, bundle: nil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() { }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
    }
    
    @objc private func didSelectDate() {
        let components = Calendar.current.dateComponents([.year, .month, .day], from: datePicker.date)
        if let day = components.day, let month = components.month, let year = components.year {
            birthDateField.text = "\(month) / \(day) / \(year)"
        }
        birthDateField.resignFirstResponder()
    }

}

// MARK: - Private methods
extension BirthDateViewController {

    private func setupViews() {
        let builder = BirthDateViewBuilder(controller: self)
        self.iconedMessage = builder.buildIconedMessage()
        self.birthDateField = builder.buildBirthdayTextField()
        builder.setupViews()
    }

    private func setupPresentationLogic() {
        let doneButton = UIBarButtonItem(title: Strings.done.localized, style: UIBarButtonItem.Style.plain, target: self, action: #selector(didSelectDate))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let toolbar = UIToolbar().apply {
            $0.barStyle = UIBarStyle.default
            $0.isTranslucent = true
            $0.tintColor = UIColor.black
            $0.sizeToFit()
            $0.setItems([ spaceButton, doneButton], animated: false)
            $0.isUserInteractionEnabled = true
        }
        birthDateField.inputView = datePicker
        birthDateField.inputAccessoryView = toolbar
    }

}
