//
//  RegisterViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 19/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class RegisterViewController: ViewPagerViewController<RegisterViewModel> {
    
    init(withInitialPage initialPage: RegisterPage = .addEmail) {
        super.init()
        viewModel = RegisterViewModel(withInitialPage: initialPage)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
