//
//  RegisterPage.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 19/04/2019.
//

import Foundation

enum RegisterPage: PagerPage {
    case addEmail
    case checkInbox
    case birthDate
    case typePassword
}

extension RegisterPage {
    
    var connectedController: AppViewController {
        switch self {
        case .addEmail:
            return AddEmailViewController()
        case .checkInbox:
            return CheckInboxViewController()
        case .birthDate:
            return BirthDateViewController()
        case .typePassword:
            return CreatePasswordViewController()
        }
    }
    
    var bottomButtonText: String {
        switch self {
        case .addEmail, .checkInbox, .birthDate, .typePassword:
            return Strings.Register.continueButton.localized
        }
    }
    
}
