//
//  RegisterViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 19/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxSwift

protocol RegisterLogic {
    var userMailSubject: BehaviorSubject<String> { get }
    var userBirthDateSubject: BehaviorSubject<Date> { get }
}

class RegisterViewModel: ViewPagerPageViewModel<RegisterPage> {
    let userMailSubject: BehaviorSubject<String> = BehaviorSubject(value: "")
    let userBirthDateSubject: BehaviorSubject<Date> = BehaviorSubject(value: Date(timeIntervalSince1970: 0))
    
    override init(withInitialPage initialPage: RegisterPage) {
        super.init(withInitialPage: initialPage)
        initializeFlow()
    }
}

// MARK: Private logic
private extension RegisterViewModel {
    
    private func initializeFlow() {
        continueClicks.withLatestFrom(userMailSubject)
            .onlyIfCurrentPage(is: .addEmail, pageStream: self.showingPageRelay)
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] _ in
                self?.showingPageRelay.onNext(.checkInbox)
            }).disposed(by: disposeBag)
        
        continueClicks
            .onlyIfCurrentPage(is: .checkInbox, pageStream: self.showingPageRelay)
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] _ in
                self?.showingPageRelay.onNext(.birthDate)
            }).disposed(by: disposeBag)
        
        continueClicks
            .onlyIfCurrentPage(is: .birthDate, pageStream: self.showingPageRelay)
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] _ in
                self?.showingPageRelay.onNext(.typePassword)
            }).disposed(by: disposeBag)
    }

}

// MARK: Interface logic methods
extension RegisterViewModel: RegisterLogic {

}
extension ObservableType {
    
    func onlyIfCurrentPage(is page: RegisterPage, pageStream: Observable<RegisterPage>) -> Observable<Element> {
        return self.withLatestFrom(pageStream) { ($0, $1) }
            .filter { $1 == page }
            .map { $0.0 }
    }
    
}
