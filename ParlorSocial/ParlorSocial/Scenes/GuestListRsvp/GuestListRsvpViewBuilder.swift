//
//  GuestListRsvpViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 22/05/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class GuestListRsvpViewBuilder {

    private unowned let controller: GuestListRsvpViewController
    private var view: UIView! { return controller.view }

    init(controller: GuestListRsvpViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension GuestListRsvpViewBuilder {
    
    private func setupProperties() {
        view.backgroundColor = .white
        controller.tableView.apply {
            $0.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 15, right: 0)
        }
    }

    private func setupHierarchy() {
        controller.apply {
            view.addSubviews([$0.tableView])
        }
    }
    
    private func setupAutoLayout() {
        controller.apply {
            $0.tableView.edges(equalTo: view)
        }
    }

}

// MARK: - Public build methods
extension GuestListRsvpViewBuilder {

    func buildView() -> UIView {
        return UIView()
    }
    
    func buildTableView() -> UITableView {
        return UITableView(frame: .zero, style: .plain).manualLayoutable().apply {
            $0.separatorStyle = .none
            $0.showsVerticalScrollIndicator = false
            $0.backgroundColor = .appBackground
        }
    }

}
