//
//  GuestListRsvpViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 22/05/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

typealias FirstNilCellConfigurator = TableCellConfigurator<GuestListRsvpFirstNilCell, GuestListRsvpFirstNilCellModel>
typealias HostCellConfigurator = TableCellConfigurator<GuestListRsvpHostCell, GuestListRsvpHostCellModel>
typealias IntroTextCellConfigurator = TableCellConfigurator<GuestListRsvpIntroTextCell, GuestListRsvpIntroTextCellModel>
typealias GuestCellConfigurator = TableCellConfigurator<GuestListRsvpGuestCell, GuestListRsvpGuestCellModel>
typealias CountPremiumMembersCellConfigurator = TableCellConfigurator<GuestListRsvpCountPremiumMembersCell,
    GuestListRsvpCountPremiumMembersCellModel>

protocol GuestListRsvpLogic: BaseViewModelLogic {
    var destinationObs: Observable<GuestListRsvpViewDestination> { get }
    var loadTableData: [CellConfigurator] { get }
    var refreshingObs: Observable<Bool> { get }
    var reloadTableViewObs: Observable<Void> { get }
    var reloadIntroViewObs: Observable<Void> { get }
    var appCheckBoxDidChangeStatusObs: Observable<(Bool, UITableViewCell)> { get }
    var isIntroView: Bool { get }

    func loadGuestList()
    func checkInIfNeeded()
    func handleCheckBoxDidChangeStatus(index: Int, isChecked: Bool)
}

class GuestListRsvpViewModel: BaseViewModel {
    
    // MARK: - Properties

    var loadTableData: [CellConfigurator] = []
    var isIntroView: Bool = false

    lazy var checkInService: CheckInService = {
        let checkInService = CheckInService(locationService: locationService, guestListRepository: guestListRepository, activationRepository: activationRepository)
        checkInService.delegate = self
        return checkInService
    }()

    private var activation: Activation?
    private var guestsCount = 0

    let analyticEventReferencedId: Int?

    private let refreshingRelay = BehaviorRelay(value: false)
    private let reloadTableViewRelay = BehaviorRelay<Void>(value: ())
    private let reloadIntroViewRelay = PublishRelay<Void>()
    private let destinationRelay = PublishRelay<GuestListRsvpViewDestination>()
    private let guestListRepository = GuestListRsvpRepository.shared
    private let activationRepository = ActivationsRepository.shared
    private let sendIntroRequestClick = PublishRelay<[GuestListGuest]>()
    private let appCheckBoxDidChangeStatusRelay = PublishRelay<(Bool, UITableViewCell)>()
    private let locationService = LocationService()
    private let activationId: Int

    // MARK: - Initialization

    init(activationId: Int) {
        self.activationId = activationId
        self.analyticEventReferencedId = activationId

        super.init()
        self.fetchActivationWithProgressBar()
        bindRelays()
    }
}

// MARK: Private logic
private extension GuestListRsvpViewModel {
    private func fetchActivationWithProgressBar() {
        activationRepository.getActivation(withId: activationId).showingProgressBar(with: self)
            .subscribe(onNext: { [weak self] response in
                self?.handleResponse(response)
            }).disposed(by: disposeBag)
    }

    private func handleResponse(_ response: SingleActivationResposne) {
        refreshingRelay.accept(false)
        switch response {
        case .success(let dataResponse):
            activation = dataResponse.data
            isIntroView = !(dataResponse.data.disableIntroRequests == true)
            setupTopCellsConfigurators(activation: dataResponse.data)
        case .failure(let error):
            alertDataSubject.onNext(AlertData.fromAppError(
                appError: error,
                bottomButtonAction: activation == nil ? { [unowned self] in self.closeViewSubject.emitElement() } : {},
                closeButtonAction: activation == nil ? { [unowned self] in self.closeViewSubject.emitElement() } : {}
            ))
        }
    }

    private func setupTopCellsConfigurators(activation: Activation) {
        setupHostCellConfigurators(activation: activation)
        if !(activation.disableIntroRequests ?? false) {
            setupIntroTextCellConfigurator(activation: activation)
        }
        reloadTableViewRelay.accept(())
    }
    
    private func setupHostCellConfigurators(activation: Activation) {
        loadTableData.append(FirstNilCellConfigurator(item: GuestListRsvpFirstNilCellModel()))
        for (index, host) in activation.hosts.enumerated() {
            let item = GuestListRsvpHostCellModel(
                host: host,
                disableIntroRequest: (activation.disableIntroRequests ?? false),
                isLast: index == activation.hosts.count - 1,
                delegate: self
            )
            loadTableData.append(HostCellConfigurator(item: item))
        }
    }
    
    private func setupIntroTextCellConfigurator(activation: Activation) {
        let introTextCellItem = GuestListRsvpIntroTextCellModel(
            activationName: activation.name,
            isIntroView: isIntroView,
            maxMembersCount: Config.userProfile?.maxIntroRequest ?? 0,
            activationEndAt: activation.endAt,
            delegate: self)
        loadTableData.append(IntroTextCellConfigurator(item: introTextCellItem))
    }
    
    private func handleGuestsResponse(_ response: GuestListRsvpMetaResponse) {
        switch response {
        case .success(let response):
            setupGuestCellConfigurator(data: response)
        case .failure(let error):
            alertDataSubject.onNext(AlertData.fromAppError(
                appError: error,
                bottomButtonAction: { [weak self] in self?.closeViewSubject.emitElement() },
                closeButtonAction: { [weak self] in self?.closeViewSubject.emitElement() }
            ))
        }
    }
    
    private func setupGuestCellConfigurator(data: GetGuestListMetaResponse) {
        guestsCount = data.data.count
        let isHiddenCountPremiumMembersCell = (Config.userProfile?.isPremium ?? false) || data.meta?.countPremiumMembers == 0
        for (index, guest) in data.data.enumerated() {
            let item = GuestListRsvpGuestCellModel(
                guest: guest,
                isIntroView: isIntroView,
                isLast: index == data.data.count - 1 && isHiddenCountPremiumMembersCell,
                isFirst: index == 0,
                delegate: self)
            loadTableData.append(GuestCellConfigurator(item: item))
        }
        if !isHiddenCountPremiumMembersCell {
            loadTableData.append(
                CountPremiumMembersCellConfigurator(
                    item: GuestListRsvpCountPremiumMembersCellModel(premiumMembersCount: data.meta?.countPremiumMembers)
                )
            )
        }
        reloadTableViewRelay.accept(())
    }
    
    private func updateCellConfigurators(with guests: [GuestListGuest]) {
        var index = 0
        loadTableData.forEach {
            if let guestCellConfigurator = $0 as? GuestCellConfigurator {
                guestCellConfigurator.item.isIntroView = isIntroView
                guestCellConfigurator.item.guest = guests[index]
                index += 1
            }
        }
    }
    
    private func bindRelays() {
        sendIntroRequestClick
            .map { guests in
                return guests.filter { ($0.isCheckedToIntroRequest ?? false) || ($0.isIntroRequested ?? false) }}
            .flatMap { [unowned self] in
                return self.guestListRepository.sendIntroRequest(
                    forActivationWithId: self.activationId,
                    data: $0).showingProgressBar(with: self)}
            .subscribe(onNext: { [weak self] response in
                self?.handleIntroRequestResponse(response)})
            .disposed(by: disposeBag)
    }

    private func handleIntroRequestResponse(_ response: GuestListRsvpResponse) {
        switch response {
        case .success(let response):
            alertDataSubject.onNext(AlertData(
                icon: Icons.GuestListRsvp.introRequestSuccess,
                title: Strings.GuestListRsvp.introRequestSuccess.localized, message: nil,
                bottomButtonTitle: Strings.ok.rawValue.uppercased()))
            updateCellConfigurators(with: response.data)
        case .failure(let error):
            errorSubject.onNext(error.setErrorType(errorType: .introRequest))
        }
    }
    
    private func guestCanBeChecked() -> Bool {
        let data = loadTableData.filter { $0 is GuestCellConfigurator }
        guard let guestCellConfigurator = data as? [GuestCellConfigurator] else { return false }
        let introRequestedGuestsCount = guestCellConfigurator
            .map { $0.item.guest }
            .filter { ($0.isCheckedToIntroRequest ?? false) || ($0.isIntroRequested ?? false) }
            .count
        return introRequestedGuestsCount < Config.userProfile?.maxIntroRequest ?? 0
    }
}

// MARK: Interface logic methods
extension GuestListRsvpViewModel: GuestListRsvpLogic {
    var reloadIntroViewObs: Observable<Void> {
        return reloadIntroViewRelay.asObservable()
    }

    var reloadTableViewObs: Observable<Void> {
        return reloadTableViewRelay.asObservable()
    }

    var appCheckBoxDidChangeStatusObs: Observable<(Bool, UITableViewCell)> {
        return appCheckBoxDidChangeStatusRelay.asObservable()
    }
    
    var destinationObs: Observable<GuestListRsvpViewDestination> {
        return destinationRelay.asObservable()
    }

    var refreshingObs: Observable<Bool> {
        return refreshingRelay.asObservable()
    }
    
    func loadGuestList() {
        guestListRepository.getGuestList(forActivationWithId: activationId)
            .showingProgressBar(with: self)
            .subscribe(onNext: { [weak self] response in
                self?.handleGuestsResponse(response)
            }).disposed(by: disposeBag)
    }
    
    func checkInIfNeeded() {
        guard let activation = activation else { return }

        checkInService.checkInOnGuestListView(
            activationId: activation.id,
            checkInStrategy: activation.checkInStrategy
        )
    }

    func handleCheckBoxDidChangeStatus(index: Int, isChecked: Bool) {
        if let guestCellConfigurator = loadTableData[index] as? GuestCellConfigurator {
            guestCellConfigurator.item.guest = guestCellConfigurator.item.guest.withCheckedToIntroRequest(isChecked)
        }
    }
}

// MARK: - HostCellDelegate
extension GuestListRsvpViewModel: GuestListRsvpHostCellDelegate {
    func didTapOnProfile(host: ParlorHost) {
        destinationRelay.accept(.hostDetails(host: host))
    }
}

// MARK: - IntroButtonCellDelegate
extension GuestListRsvpViewModel: GuestListRsvpIntroTextCellDelegate {
    func didTapIntroRequest() {
        let data = loadTableData.filter { $0 is GuestCellConfigurator }
        guard let guestCellConfigurator = data as? [GuestCellConfigurator] else { return }
        sendIntroRequestClick.accept(guestCellConfigurator.map {$0.item.guest})
    }
}

// MARK: - GuestCellDelegate
extension GuestListRsvpViewModel: GuestListRsvpGuestCellDelegate {
    func appCheckboxDidChangeStatus(isChecked: Bool, cell: UITableViewCell) {
        if guestCanBeChecked() || !isChecked {
            appCheckBoxDidChangeStatusRelay.accept((isChecked, cell))
        } else {
            guard let guestCell = cell as? GuestListRsvpGuestCell else { return }
            guestCell.selectToIntroCheckBox.isChecked = false
        }
    }
}

// MARK: - Check-in service
extension GuestListRsvpViewModel: CheckInServiceDelegate {
    func didEndCheckInSuccess(_ checkInService: CheckInService, activationId: Int) { }
    
    func didStartedCheckIn(_ checkInService: CheckInService) { }
    
    func didGetLocationError(_ checkInService: CheckInService, errorType: GetLocationErrorType) { }
    
    func didCheckInError(_ checkInService: CheckInService, error: AppError) { }
}
