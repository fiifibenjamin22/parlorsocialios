//
//  GuestListRsvpViewDestination.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 28/05/2019.
//

import Foundation
import UIKit

enum GuestListRsvpViewDestination {
    case hostDetails(host: ParlorHost)
}

extension GuestListRsvpViewDestination: Destination {
    
    var viewController: UIViewController {
        switch self {
        case .hostDetails(let host):
            return HostDetailsViewController(withHost: host)
        }
    }
}

