//
//  GuestListRsvpTableViewCell.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 22/05/2019.
//  Copyright © 2019 KISSdigital. All rights reserved.
//

import UIKit

protocol GuestListRsvpTableViewCellDelegate: class {
    func appCheckboxDidChangeStatus(isChecked: Bool, cell: UITableViewCell)
}

final class GuestListRsvpTableViewCell: UITableViewCell {
    
    // MARK: - Properties
    private weak var delegate: GuestListRsvpTableViewCellDelegate?

    // MARK: - Views
    private(set) var guestAvatarRoundedImageView: RoundedImageView!
    private(set) var mainVerticalStackView: UIStackView!
    private(set) var guestNameLabel: UILabel!
    private(set) var positionCompanyUniversityStackView: UIStackView!
    private(set) var guestPositionLabel: UILabel!
    private(set) var guestCompanyUniversityLabel: UILabel!
    private(set) var premiumStackView: UIStackView!
    private(set) var premiumIconImageView: UIImageView!
    private(set) var premiumLabel: UILabel!
    private(set) var selectToIntroCheckBox: AppCheckBox!
    private(set) var bottomLine: UIView!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initializeCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - Public methods
extension GuestListRsvpTableViewCell {
    func setup(with model: Guest, isIntroView: Bool, isLast: Bool, delegate: GuestListRsvpTableViewCellDelegate?) {
        self.delegate = delegate
        guestAvatarRoundedImageView.getImage(from: model.imageUrl)
        guestNameLabel.text = model.nameSurname
        guestPositionLabel.text = model.info?.position
        guestCompanyUniversityLabel.text = model.info?.company ?? model.info?.university
        premiumStackView.isHidden = !(model.isPremium ?? false)
        bottomLine.isHidden = isLast
        contentView.backgroundColor = isIntroView ? .appBackground : .white
        setupCheckBox(model: model, isIntroView: isIntroView)
    }
}

// MARK: - Private methods
extension GuestListRsvpTableViewCell {
    
    private func initializeCell() {
        clipsToBounds = true
        selectionStyle = .none
        backgroundColor = UIColor.appBackground
        setupViews()
    }
    
    private func setupViews() {
        let builder = GuestListRsvpTableViewCellBuilder(cell: self)
        
        guestAvatarRoundedImageView = builder.buildGuestAvatarRoundedImageView()
        mainVerticalStackView = builder.buildMainVerticalStackView()
        guestNameLabel = builder.buildGuestNameLabel()
        positionCompanyUniversityStackView = builder.buildPositionCompanyUniversityStackView()
        guestPositionLabel = builder.buildGuestPositionLabel()
        guestCompanyUniversityLabel = builder.buildGuestCompanyUniversityLabel()
        premiumStackView = builder.buildPremiumStackView()
        premiumIconImageView = builder.buildPremiumIconImageView()
        premiumLabel = builder.buildPremiumLabel()
        selectToIntroCheckBox = builder.buildSelectToIntroCheckBox(delegate: self)
        bottomLine = builder.buildBottomLine()
        
        builder.setupViews()
    }
    
    private func setupCheckBox(model: Guest, isIntroView: Bool) {
        selectToIntroCheckBox.isHidden = !isIntroView
        selectToIntroCheckBox.isChecked = (model.isCheckedToIntroRequest ?? false) || (model.isIntroRequested ?? false)
        if let isIntroRequested = model.isIntroRequested {
            selectToIntroCheckBox.isUserInteractionEnabled = !isIntroRequested
        } else {
            selectToIntroCheckBox.isUserInteractionEnabled = true
        }
    }
    
}

// MARK: - AppCheckBoxDelegate
extension GuestListRsvpTableViewCell: AppCheckBoxDelegate {
    func appCheckboxDidChangeStatus(_ appCheckbox: AppCheckBox) {
        delegate?.appCheckboxDidChangeStatus(isChecked: appCheckbox.isChecked, cell: self)
    }
}

