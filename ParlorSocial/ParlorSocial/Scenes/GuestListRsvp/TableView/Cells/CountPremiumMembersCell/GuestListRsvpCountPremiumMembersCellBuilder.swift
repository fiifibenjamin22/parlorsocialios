//
//  GuestListRsvpCountPremiumMembersCellBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 30/05/2019.
//

import UIKit

final class GuestListRsvpCountPremiumMembersCellBuilder {
    private unowned let cell: GuestListRsvpCountPremiumMembersCell
    private var view: UIView! { return cell.contentView }
    
    init(cell: GuestListRsvpCountPremiumMembersCell) {
        self.cell = cell
    }
    
    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension GuestListRsvpCountPremiumMembersCellBuilder {
    
    private func setupProperties() {
        view.backgroundColor = .white
        cell.apply {
            $0.parlorSymbolLabel.text = Strings.parlorSymbol.rawValue
        }
    }
    
    private func setupHierarchy() {
        view.addSubview(cell.horizontalStackView)
        cell.apply {
            $0.horizontalStackView.addArrangedSubviews([$0.roundedBackgroundView, $0.countPremiumMembersLabel])
            $0.roundedBackgroundView.addSubview($0.parlorSymbolLabel)
        }
    }
    
    private func setupAutoLayout() {
        cell.horizontalStackView.apply {
            $0.edgesToParent(insets: UIEdgeInsets.margins(top: 26, left: 16, bottom: 26, right: 16))
        }
        
        cell.roundedBackgroundView.apply {
            $0.heightAnchor.equalTo(constant: Constants.CountPremiumMembersCell.roundedViewSize)
            $0.widthAnchor.equalTo(constant: Constants.CountPremiumMembersCell.roundedViewSize)
        }
        
        cell.parlorSymbolLabel.apply {
            $0.edgesToParent()
        }
    }
}

// MARK: - Public methods
extension GuestListRsvpCountPremiumMembersCellBuilder {

    func buildHorizontalStackView() -> UIStackView {
        return UIStackView(axis: .horizontal, spacing: 15, alignment: .center, distribution: .fill)
    }
    
    func buildRoundedBackgroundView() -> UIView {
        return UIView().manualLayoutable().apply {
            $0.backgroundColor = .appBackground
            $0.roundCorners(with: Constants.CountPremiumMembersCell.roundedViewSize / 2)
        }
    }
    
    func buildParlorSymbolLabel() -> UILabel {
        return UILabel.styled(
            textColor: .appGreyLight,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.subtitle,
                font: UIFont.Editor.bold
            ),
            alignment: .center
        )
    }
    
    func buildCountPremiumMembersLabel() -> UILabel {
        return UILabel.styled(
            textColor: .appTextMediumBright,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.hintSize,
                font: UIFont.Roboto.medium
            ),
            alignment: .left
        )
    }
}

// MARK: - Constants
extension Constants {
    enum CountPremiumMembersCell {
        static let roundedViewSize: CGFloat = 68.0
    }
}
