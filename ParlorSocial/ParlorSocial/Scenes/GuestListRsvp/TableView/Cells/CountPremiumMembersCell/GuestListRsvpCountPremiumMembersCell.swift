//
//  GuestListRsvpCountPremiumMembersCell.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 30/05/2019.
//

import UIKit

final class GuestListRsvpCountPremiumMembersCell: UITableViewCell, ConfigurableCell {
    typealias Model = GuestListRsvpCountPremiumMembersCellModel
    
    // MARK: - Views
    private(set) var horizontalStackView: UIStackView!
    private(set) var roundedBackgroundView: UIView!
    private(set) var parlorSymbolLabel: UILabel!
    private(set) var countPremiumMembersLabel: UILabel!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initializeCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - Public methods
extension GuestListRsvpCountPremiumMembersCell {
    func configure(with model: Model) {
        switch model.premiumMembersCount {
        case 1:
            countPremiumMembersLabel.text = String(format: Strings.GuestListRsvp.onePremiumMember.localized)
        default:
            countPremiumMembersLabel.text = String(format: Strings.GuestListRsvp.countPremiumMembers.localized, model.premiumMembersCount ?? 0)
        }
    }
}

// MARK: - Private methods
extension GuestListRsvpCountPremiumMembersCell {
    
    private func initializeCell() {
        clipsToBounds = true
        selectionStyle = .none
        backgroundColor = .white
        setupViews()
    }
    
    private func setupViews() {
        let builder = GuestListRsvpCountPremiumMembersCellBuilder(cell: self)
        horizontalStackView = builder.buildHorizontalStackView()
        roundedBackgroundView = builder.buildRoundedBackgroundView()
        parlorSymbolLabel = builder.buildParlorSymbolLabel()
        countPremiumMembersLabel = builder.buildCountPremiumMembersLabel()
        
        builder.setupViews()
    }
    
}

// MARK: - Cell model
struct GuestListRsvpCountPremiumMembersCellModel {
    let premiumMembersCount: Int?
}
