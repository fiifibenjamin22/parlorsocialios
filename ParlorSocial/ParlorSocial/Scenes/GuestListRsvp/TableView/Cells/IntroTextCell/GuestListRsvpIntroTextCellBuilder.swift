//
//  GuestListRsvpIntroTextCellBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 30/05/2019.
//

import UIKit

final class GuestListRsvpIntroTextCellBuilder {
    private unowned let cell: GuestListRsvpIntroTextCell
    private var view: UIView! { return cell.contentView }
    
    init(cell: GuestListRsvpIntroTextCell) {
        self.cell = cell
    }
    
    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
    
}

// MARK: - Private methods
extension GuestListRsvpIntroTextCellBuilder {
    
    private func setupProperties() {
        view.backgroundColor = .appBackground
        cell.apply {
            $0.introRequestRoundedButton.setTitle(Strings.GuestListRsvp.sendRequestToParlor.localized.uppercased(), for: .normal)
            $0.introRequestViewTopLine.backgroundColor = .textVeryLight
            $0.introRequestViewBottomLine.backgroundColor = .textVeryLight
        }
    }
    
    private func setupHierarchy() {
        cell.apply {
            view.addSubview($0.introRequestView)
            $0.introRequestView.addSubviews([
                $0.introRequestViewTopLine,
                $0.introRequestStackView,
                $0.introRequestViewBottomLine
                ])
            $0.introRequestStackView.addArrangedSubviews([
                $0.introRequestLabel, $0.introRequestRoundedButton
                ])
        }
    }
    
    private func setupAutoLayout() {
        cell.introRequestView.apply {
            $0.edgesToParent(anchors: [.top, .leading, .trailing])
            $0.bottomAnchor.equal(to: view.bottomAnchor, withPriority: .defaultHigh)
        }
        
        cell.introRequestViewTopLine.apply {
            $0.edgesToParent(anchors: [.leading, .top, .trailing])
            $0.heightAnchor.equalTo(constant: 1)
        }
        
        cell.introRequestStackView.apply {
            $0.edgesToParent(
                anchors: [.top, .leading, .bottom, .trailing],
                insets: UIEdgeInsets.margins(top: 27, left: 16, bottom: 30, right: 16))
            $0.centerXAnchor.equal(to: view.centerXAnchor)
        }
        
        cell.introRequestLabel.apply {
            $0.widthAnchor.constraint(
                equalTo: cell.introRequestView.widthAnchor,
                multiplier: Constants.GuestListRsvpIntroTextCell.introRequestLabelWidthMultiplier
            ).activate()
        }
        
        cell.introRequestRoundedButton.apply {
            $0.heightAnchor.equalTo(constant: 55, withPriority: .defaultHigh)
            $0.edgesToParent(anchors: [.leading, .trailing])
        }
        
        cell.introRequestViewBottomLine.apply {
            $0.edgesToParent(anchors: [.leading, .bottom, .trailing])
            $0.heightAnchor.equalTo(constant: 1)
        }
    }
    
}

// MARK: - Public build methods
extension GuestListRsvpIntroTextCellBuilder {
    
    func buildIntroRequestViewTopLine() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildIntroRequestView() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildIntroRequestStackView() -> UIStackView {
        return UIStackView(axis: .vertical, spacing: 25, alignment: .center, distribution: .fill)
    }
    
    func buildIntroRequestLabel() -> UILabel {
        return ParlorLabel.styled(
            withTextColor: .appTextMediumBright,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.subbody,
                font: UIFont.Roboto.regular
            ),
            alignment: .center
            ).apply {
                $0.lineHeight = 21
        }
    }
    
    func buildIntroRequestRoundedButton() -> RoundedButton {
        return RoundedButton().manualLayoutable().apply {
            $0.backgroundColor = .white
            $0.layer.borderColor = UIColor.appSeparator.cgColor
            $0.layer.borderWidth = 1
            $0.applyTouchAnimation()
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium)
            $0.setTitleColor(.appText, for: .normal)
        }
    }
    
    func buildIntroRequestViewBottomLine() -> UIView {
        return UIView().manualLayoutable()
    }
    
}

// MARK: - Constants
extension Constants {
    enum GuestListRsvpIntroTextCell {
        static let introRequestLabelWidthMultiplier: CGFloat = 198/375
    }
}
