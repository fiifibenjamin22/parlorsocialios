//
//  GuestListRsvpIntroTextCell.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 30/05/2019.
//

import UIKit

protocol GuestListRsvpIntroTextCellDelegate: class {
    func didTapIntroRequest()
}

class GuestListRsvpIntroTextCell: UITableViewCell, ConfigurableCell {
    typealias Model = GuestListRsvpIntroTextCellModel
    
    private weak var delegate: GuestListRsvpIntroTextCellDelegate?
    
    // MARK: - Views
    private(set) var introRequestView: UIView!
    private(set) var introRequestViewTopLine: UIView!
    private(set) var introRequestStackView: UIStackView!
    private(set) var introRequestLabel: UILabel!
    private(set) var introRequestRoundedButton: RoundedButton!
    private(set) var introRequestViewBottomLine: UIView!
    
    // MARK: - Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initializeCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - Public methods

extension GuestListRsvpIntroTextCell {
    
    func configure(with model: Model) {
        self.delegate = model.delegate
        introRequestLabel.text = String(format: Strings.GuestListRsvp.selectTheMembers.localized, model.maxMembersCount)
        introRequestRoundedButton.isHidden = !model.isIntroView
    }
    
}

// MARK: - Private methods
extension GuestListRsvpIntroTextCell {
    
    private func initializeCell() {
        clipsToBounds = true
        selectionStyle = .none
        backgroundColor = .appBackground
        setupViews()
    }
    
    private func setupViews() {
        let builder = GuestListRsvpIntroTextCellBuilder(cell: self)
        introRequestViewTopLine = builder.buildIntroRequestViewTopLine()
        introRequestView = builder.buildIntroRequestView()
        introRequestStackView = builder.buildIntroRequestStackView()
        introRequestLabel = builder.buildIntroRequestLabel()
        introRequestRoundedButton = builder.buildIntroRequestRoundedButton()
        introRequestViewBottomLine = builder.buildIntroRequestViewBottomLine()
        
        builder.setupViews()
        introRequestRoundedButton.setupTapGestureRecognizer(target: self, action: #selector(tapOnIntroRequest))
    }
    
    @objc func tapOnIntroRequest() {
        self.delegate?.didTapIntroRequest()
    }
    
}

// MARK: Cell model

struct GuestListRsvpIntroTextCellModel {
    let activationName: String
    var isIntroView: Bool
    let maxMembersCount: Int
    let activationEndAt: Date?
    weak var delegate: GuestListRsvpIntroTextCellDelegate?
}

