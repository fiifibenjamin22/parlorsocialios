//
//  GuestListRsvpGuestCell.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 22/05/2019.
//

import UIKit

protocol GuestListRsvpGuestCellDelegate: class {
    func appCheckboxDidChangeStatus(isChecked: Bool, cell: UITableViewCell)
}

final class GuestListRsvpGuestCell: UITableViewCell, ConfigurableCell {
    typealias Model = GuestListRsvpGuestCellModel
    
    // MARK: - Properties
    private weak var delegate: GuestListRsvpGuestCellDelegate?

    // MARK: - Views
    private(set) var guestAvatarRoundedImageView: RoundedImageView!
    private(set) var mainVerticalStackView: UIStackView!
    private(set) var guestNameLabel: UILabel!
    private(set) var positionCompanyUniversityStackView: UIStackView!
    private(set) var guestPositionLabel: UILabel!
    private(set) var guestCompanyUniversityLabel: UILabel!
    private(set) var premiumStackView: UIStackView!
    private(set) var premiumIconImageView: UIImageView!
    private(set) var premiumLabel: UILabel!
    private(set) var selectToIntroCheckBox: AppCheckBox!
    private(set) var bottomLine: UIView!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initializeCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - Public methods
extension GuestListRsvpGuestCell {
    func configure(with model: Model) {
        self.delegate = model.delegate
        guestAvatarRoundedImageView.getImage(from: model.guest.thumbnailImageUrl, placeholder: Icons.Common.placeholderPhoto)
        guestNameLabel.text = model.guest.nameSurname
        guestPositionLabel.text = model.guest.info?.position
        let info = model.guest.info
        guestPositionLabel.isHidden = info?.position == nil
        guestCompanyUniversityLabel.text = info?.company ?? info?.education ?? info?.university
        guestCompanyUniversityLabel.isHidden = info?.company == nil
            && info?.university == nil && info?.education == nil
        positionCompanyUniversityStackView.isHidden = isHiddenPositionCompanyUniversityStackView(model: model)
        premiumStackView.isHidden = !(model.guest.isPremium ?? false)
        bottomLine.isHidden = model.isLast
        setupCheckBoxAndBackground(model: model.guest, isIntroView: model.isIntroView)
    }
}

// MARK: - Private methods
extension GuestListRsvpGuestCell {
    
    private func initializeCell() {
        clipsToBounds = true
        selectionStyle = .none
        backgroundColor = UIColor.appBackground
        setupViews()
    }
    
    private func setupViews() {
        let builder = GuestListRsvpGuestCellBuilder(cell: self)
        
        guestAvatarRoundedImageView = builder.buildGuestAvatarRoundedImageView()
        mainVerticalStackView = builder.buildMainVerticalStackView()
        guestNameLabel = builder.buildGuestNameLabel()
        positionCompanyUniversityStackView = builder.buildPositionCompanyUniversityStackView()
        guestPositionLabel = builder.buildGuestPositionLabel()
        guestCompanyUniversityLabel = builder.buildGuestCompanyUniversityLabel()
        premiumStackView = builder.buildPremiumStackView()
        premiumIconImageView = builder.buildPremiumIconImageView()
        premiumLabel = builder.buildPremiumLabel()
        selectToIntroCheckBox = builder.buildSelectToIntroCheckBox(delegate: self)
        bottomLine = builder.buildBottomLine()
        
        builder.setupViews()
    }
    
    private func setupCheckBoxAndBackground(model: GuestListGuest, isIntroView: Bool) {
        UIView.animate(withDuration: 0.5) { [unowned self] in
            self.selectToIntroCheckBox.alpha = isIntroView ? 1.0 : 0.0
            self.contentView.backgroundColor = isIntroView ? .appBackground : .white
        }
        selectToIntroCheckBox.isChecked = (model.isCheckedToIntroRequest ?? false) || (model.isIntroRequested ?? false)
        if let isIntroRequested = model.isIntroRequested {
            selectToIntroCheckBox.isUserInteractionEnabled = !isIntroRequested
        } else {
            selectToIntroCheckBox.isUserInteractionEnabled = true
        }
    }
    
    private func isHiddenPositionCompanyUniversityStackView(model: Model) -> Bool {
        guard let info = model.guest.info else { return true }
        return info.position == nil
            && info.company == nil
            && info.university == nil
            && info.education == nil
    }
}

// MARK: - AppCheckBoxDelegate
extension GuestListRsvpGuestCell: AppCheckBoxDelegate {
    func appCheckboxDidChangeStatus(_ appCheckbox: AppCheckBox) {
        delegate?.appCheckboxDidChangeStatus(isChecked: appCheckbox.isChecked, cell: self)
    }
}

// MARK: Cell model

struct GuestListRsvpGuestCellModel {
    var guest: GuestListGuest
    var isIntroView: Bool
    let isLast: Bool
    let isFirst: Bool
    weak var delegate: GuestListRsvpGuestCellDelegate?
}
