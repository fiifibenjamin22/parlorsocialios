//
//  GuestListRsvpGuestCellBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 22/05/2019.
//

import UIKit

final class GuestListRsvpGuestCellBuilder {
    private unowned let cell: GuestListRsvpGuestCell
    private var view: UIView! { return cell.contentView }
    
    init(cell: GuestListRsvpGuestCell) {
        self.cell = cell
    }
    
    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension GuestListRsvpGuestCellBuilder {
    
    private func setupProperties() {
        view.backgroundColor = .white
        cell.apply {
            $0.premiumLabel.text = Strings.GuestListRsvp.premiumMember.localized
            $0.bottomLine.backgroundColor = .textVeryLight
        }
        cell.selectToIntroCheckBox.apply {
            $0.alpha = 0.0
            $0.backgroundColor = UIColor.white.withAlphaComponent(0)
            $0.checkedImage = Icons.GuestListRsvp.checkBoxSelectedIcon
            $0.uncheckedImage = Icons.GuestListRsvp.checkBoxIcon
        }
        cell.guestAvatarRoundedImageView.contentMode = .scaleAspectFill
    }
    
    private func setupHierarchy() {
        cell.apply {
            [$0.guestAvatarRoundedImageView, $0.mainVerticalStackView, $0.selectToIntroCheckBox, $0.bottomLine]
                .addTo(parent: view)
            
            $0.mainVerticalStackView.addArrangedSubviews([
                $0.guestNameLabel,
                $0.positionCompanyUniversityStackView,
                $0.premiumStackView
                ])
            
            $0.positionCompanyUniversityStackView.addArrangedSubviews([
                $0.guestPositionLabel,
                $0.guestCompanyUniversityLabel
                ])
            
            $0.premiumStackView.addArrangedSubviews([$0.premiumIconImageView, $0.premiumLabel])
        }
    }
    
    private func setupAutoLayout() {
        cell.guestAvatarRoundedImageView.apply {
            $0.edgesToParent(anchors: [.leading, .top], insets: UIEdgeInsets.margins(
                    top: Constants.GuestListRsvpCell.contentVerticalMargin,
                    left: 16
                )
            )
            $0.heightAnchor.equalTo(constant: Constants.GuestListRsvpCell.guestImageSize)
            $0.widthAnchor.equalTo(constant: Constants.GuestListRsvpCell.guestImageSize)
        }
        
        cell.mainVerticalStackView.apply {
            $0.leadingAnchor.equal(to: cell.guestAvatarRoundedImageView.trailingAnchor, constant: 15)
            $0.edgesToParent(anchors: [.top, .bottom], insets: UIEdgeInsets.margins(
                top: Constants.GuestListRsvpCell.contentVerticalMargin,
                bottom: Constants.GuestListRsvpCell.contentVerticalMargin
            ))
            $0.trailingAnchor.equal(to: cell.selectToIntroCheckBox.leadingAnchor, constant: -10)
        }
        
        cell.premiumIconImageView.apply {
            $0.setContentHuggingPriority(.required, for: .horizontal)
        }
        
        cell.selectToIntroCheckBox.apply {
            $0.edgesToParent(anchors: [.trailing], insets: UIEdgeInsets.margins(right: 25))
            $0.centerYAnchor.equal(to: view.centerYAnchor)
            $0.widthAnchor.equalTo(constant: 30)
            $0.heightAnchor.equalTo(constant: 37)
        }
        
        cell.bottomLine.apply {
            $0.edgesToParent(
                anchors: [.leading, .bottom, .trailing],
                insets: UIEdgeInsets.margins(
                    left: Constants.GuestListRsvpCell.lineHorizontalMargin,
                    right: Constants.GuestListRsvpCell.lineHorizontalMargin
                )
            )
            $0.heightAnchor.equalTo(constant: 1)
            $0.bottomAnchor.greaterThanOrEqual(to: cell.guestAvatarRoundedImageView.bottomAnchor, constant: Constants.GuestListRsvpCell.contentVerticalMargin)
        }
    }
    
}

// MARK: - Public methods
extension GuestListRsvpGuestCellBuilder {
    func buildGuestAvatarRoundedImageView() -> RoundedImageView {
        return RoundedImageView().manualLayoutable()
    }
    
    func buildMainVerticalStackView() -> UIStackView {
        return UIStackView(axis: .vertical, spacing: 10, alignment: .fill, distribution: .fill)
    }
    
    func buildGuestNameLabel() -> UILabel {
        return UILabel.styled(
            textColor: .appTextMediumBright,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.hintSize,
                font: UIFont.Roboto.medium
            ),
            alignment: .left
        )
    }
    
    func buildPositionCompanyUniversityStackView() -> UIStackView {
        return UIStackView(axis: .vertical, spacing: 5, alignment: .fill, distribution: .fill)
    }
    
    func buildGuestPositionLabel() -> UILabel {
        return UILabel.styled(
            textColor: .appTextMediumBright,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.tiny,
                font: UIFont.Roboto.regular
            ),
            alignment: .left
        )
    }
    
    func buildGuestCompanyUniversityLabel() -> UILabel {
        return UILabel.styled(
            textColor: .appTextMediumBright,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.tiny,
                font: UIFont.Roboto.regular
            ),
            alignment: .left
        )
    }
    
    func buildPremiumStackView() -> UIStackView {
        return UIStackView(axis: .horizontal, spacing: 5, alignment: .center, distribution: .fill)
    }
    
    func buildPremiumIconImageView() -> UIImageView {
        return UIImageView(image: Icons.GuestListRsvp.premiumIcon).manualLayoutable()
    }
    
    func buildPremiumLabel() -> UILabel {
        return UILabel.styled(
            textColor: .appTextGold,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.small,
                font: UIFont.Roboto.regular
            ),
            alignment: .left,
            numberOfLines: 1
        )
    }
    
    func buildSelectToIntroCheckBox(delegate: AppCheckBoxDelegate) -> AppCheckBox {
        return AppCheckBox().manualLayoutable().apply { $0.delegate = delegate }
    }
    
    func buildBottomLine() -> UIView {
        return UIView().manualLayoutable()
    }
}

// MARK: - Constatns Extension
extension Constants {
    enum GuestListRsvpCell {
        static let guestImageSize: CGFloat = 68.0
        static let contentVerticalMargin: CGFloat = 25.0
        static let lineHorizontalMargin: CGFloat = 16.0
    }
}
