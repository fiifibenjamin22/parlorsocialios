//
//  GuestListRsvpHostCell.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 30/05/2019.
//

import UIKit

protocol GuestListRsvpHostCellDelegate: class {
    func didTapOnProfile(host: ParlorHost)
}

class GuestListRsvpHostCell: UITableViewCell, ConfigurableCell {
    typealias Model = GuestListRsvpHostCellModel
    
    // MARK: - Properties
    private var model: Model?
    
    private(set) var hostAvatarRoundedImageView: RoundedImageView!
    private(set) var hostNameTypeStackView: UIStackView!
    private(set) var hostNameLabel: UILabel!
    private(set) var hostType: UILabel!
    private(set) var profileRoundedButton: RoundedButton!
    private(set) var bottomLine: UIView!
    
    private weak var delegate: GuestListRsvpHostCellDelegate?
    
    // MARK: - Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initializeCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - Public methods

extension GuestListRsvpHostCell {
    
    func configure(with model: Model) {
        self.model = model
        delegate = model.delegate
        hostAvatarRoundedImageView.getImage(from: model.host.imageUrl, placeholder: Icons.Common.placeholderPhoto)
        hostNameLabel.text = model.host.name
        hostType.text = model.host.role.displayName
        bottomLine.isHidden = model.isLast && !model.disableIntroRequest
    }
    
}

// MARK: - Private methods
extension GuestListRsvpHostCell {
    
    private func initializeCell() {
        clipsToBounds = true
        selectionStyle = .none
        backgroundColor = .white
        setupViews()
    }
    
    private func setupViews() {
        let builder = GuestListRsvpHostCellBuilder(cell: self)
        
        hostAvatarRoundedImageView = builder.buildHostAvatarRoundedImageView()
        hostNameTypeStackView = builder.buildHostNameTypeStackView()
        hostNameLabel = builder.buildHostNameLabel()
        hostType = builder.buildHostTypeLabel()
        profileRoundedButton = builder.buildProfileRoundedButton()
        bottomLine = builder.buildBottomLine()
        
        builder.setupViews()
        profileRoundedButton.setupTapGestureRecognizer(target: self, action: #selector(tapOnProfile))
    }
    
    @objc private func tapOnProfile() {
        guard let host = model?.host else { return }
        delegate?.didTapOnProfile(host: host)
    }
    
}

// MARK: Cell model

struct GuestListRsvpHostCellModel {
    let host: ParlorHost
    let disableIntroRequest: Bool
    let isLast: Bool
    weak var delegate: GuestListRsvpHostCellDelegate?
}
