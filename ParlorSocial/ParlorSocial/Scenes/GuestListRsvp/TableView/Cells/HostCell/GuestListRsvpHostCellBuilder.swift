//
//  GuestListRsvpHostCellBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 30/05/2019.
//

import UIKit

final class GuestListRsvpHostCellBuilder {
    private unowned let cell: GuestListRsvpHostCell
    private var view: UIView! { return cell.contentView }
    
    init(cell: GuestListRsvpHostCell) {
        self.cell = cell
    }
    
    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
    
}

// MARK: - Private methods
extension GuestListRsvpHostCellBuilder {
    
    private func setupProperties() {
        view.backgroundColor = .white
        cell.profileRoundedButton.apply {
            $0.setTitle(
                Strings.GuestListRsvp.profile.localized.uppercased(),
                for: .normal)
            $0.setTitleColor(.appText, for: .normal)
        }
        cell.bottomLine.backgroundColor = .textVeryLight
        cell.hostAvatarRoundedImageView.contentMode = .scaleAspectFill
    }
    
    private func setupHierarchy() {
        cell.apply {[
            $0.hostAvatarRoundedImageView,
            $0.hostNameTypeStackView,
            $0.profileRoundedButton,
            $0.bottomLine
            ].addTo(parent: view)
            
            $0.hostNameTypeStackView.addArrangedSubviews([$0.hostNameLabel, $0.hostType])
        }
    }
    
    private func setupAutoLayout() {
        cell.hostAvatarRoundedImageView.apply {
            $0.edgesToParent(anchors: [.top], insets: UIEdgeInsets.margins(top: 55, bottom: 0))
            $0.centerXAnchor.equal(to: view.centerXAnchor)
            $0.heightAnchor.equalTo(constant: Constants.GuestListRsvpHostCell.avatarSize)
            $0.widthAnchor.equalTo(constant: Constants.GuestListRsvpHostCell.avatarSize)
        }
        
        cell.hostNameTypeStackView.apply {
            $0.topAnchor.equal(to: cell.hostAvatarRoundedImageView.bottomAnchor, constant: 25)
            $0.edgesToParent(
                anchors: [.leading, .trailing],
                insets: UIEdgeInsets.margins(left: 20, right: 20)
            )
        }
        
        cell.profileRoundedButton.apply {
            $0.bottomAnchor.equal(to: view.bottomAnchor, constant: -42, withPriority: .defaultHigh)
            $0.heightAnchor.equalTo(constant: 40)
            $0.widthAnchor.equalTo(constant: 157)
            $0.topAnchor.equal(to: cell.hostNameTypeStackView.bottomAnchor, constant: 30)
            $0.centerXAnchor.equal(to: view.centerXAnchor)
        }
        
        cell.bottomLine.apply {
            $0.edgesToParent(
                anchors: [.leading, .bottom, .trailing],
                insets: UIEdgeInsets.margins(left: 16, right: 16)
            )
            $0.heightAnchor.equalTo(constant: 1)
        }
    }
    
}

// MARK: - Public build methods
extension GuestListRsvpHostCellBuilder {
    
    func buildHostAvatarRoundedImageView() -> RoundedImageView {
        return RoundedImageView().manualLayoutable()
    }
    
    func buildHostNameTypeStackView() -> UIStackView {
        return UIStackView(axis: .vertical, spacing: 10, alignment: .fill, distribution: .fill).manualLayoutable()
    }
    
    func buildHostNameLabel() -> UILabel {
        return UILabel.styled(
            textColor: .appTextMediumBright,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.smallTitle,
                font: UIFont.Editor.bold),
            alignment: .center
        )
    }
    
    func buildHostTypeLabel() -> UILabel {
        return UILabel.styled(
            textColor: .appTextMediumBright,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.subbody,
                font: UIFont.Roboto.regular),
            alignment: .center
        )
    }
    
    func buildProfileRoundedButton() -> RoundedButton {
        return RoundedButton().manualLayoutable().apply {
            $0.layer.borderColor = UIColor.appSeparator.cgColor
            $0.layer.borderWidth = 1
            $0.applyTouchAnimation()
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium)
        }
    }
    
    func buildBottomLine() -> UIView {
        return UIView().manualLayoutable()
    }
    
}

// MARK: - Constants extension
extension Constants {
    enum GuestListRsvpHostCell {
        static let avatarSize: CGFloat = 100.0
    }
}

