//
//  GuestListRsvpFirstNilCell.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 03/06/2019.
//

import UIKit

class GuestListRsvpFirstNilCell: UITableViewCell, ConfigurableCell {
    typealias Model = GuestListRsvpFirstNilCellModel

    // MARK: - Views
    private(set) lazy var view: UIView = {
        return UIView().manualLayoutable().apply {
            $0.backgroundColor = .appBackground
        }
    }()

    // MARK: - Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initializeCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - Public methods

extension GuestListRsvpFirstNilCell {
    
    func configure(with model: Model) {}
    
}

// MARK: - Private methods
extension GuestListRsvpFirstNilCell {
    
    private func initializeCell() {
        clipsToBounds = true
        selectionStyle = .none
        backgroundColor = .white
        setupViews()
    }
    
    private func setupViews() {
        self.contentView.addSubview(view)
        view.apply {
            $0.heightAnchor.equalTo(constant: 1)
            $0.edgesToParent()
        }
    }
    
}

// MARK: - Cell model
struct GuestListRsvpFirstNilCellModel {}
