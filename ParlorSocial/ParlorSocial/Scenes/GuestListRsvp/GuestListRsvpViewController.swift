//
//  GuestListViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 22/05/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class GuestListRsvpViewController: AppViewController {

    // MARK: - Properties
    let analyticEventScreen: AnalyticEventScreen? = .guestList
    private var viewModel: GuestListRsvpLogic!

    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var connectedOnboardingType: OnboardingType? {
        return .guestList
    }

    // MARK: - Views
    private(set) var tableView: UITableView!

    // MARK: - Initialization
    init(activationId id: Int) {
        super.init(nibName: nil, bundle: nil)
        initialize(activationId: id)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError()
    }

    private func initialize(activationId id: Int) {
        self.viewModel = GuestListRsvpViewModel(activationId: id)
    }
  
    override var observableSources: ObservableSources? {
        return viewModel
    }
    
    override func observeViewModel() {
        super.observeViewModel()

        viewModel.reloadTableViewObs
            .subscribe(onNext: { [unowned self] in
                self.tableView.reloadData()
            }).disposed(by: disposeBag)
        
        viewModel.reloadTableViewObs
            .subscribe(onNext: { [unowned self] in
                self.reloadIntroViewTableView()
            }).disposed(by: disposeBag)
        
        viewModel.appCheckBoxDidChangeStatusObs
            .subscribe(onNext: { [unowned self] (isChecked, cell) in
                self.appCheckBoxDidChangeStatus(isChecked: isChecked, cell: cell)
            }).disposed(by: disposeBag)
    }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
        setupTableView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        adjustNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        statusBar?.backgroundColor = .white
    }

}

// MARK: - Private methods
extension GuestListRsvpViewController {

    private func setupViews() {
        let builder = GuestListRsvpViewBuilder(controller: self)
        
        self.view = builder.buildView()
        self.tableView = builder.buildTableView()

        builder.setupViews()
    }

    private func setupPresentationLogic() {
        bindDestinationObservables()
        viewModel.loadGuestList()
        viewModel.checkInIfNeeded()
    }
    
    private func adjustNavigationBar() {
        self.title = Strings.GuestListRsvp.title.localized.uppercased()
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.apply {
            $0.setBackgroundImage(UIImage(), for: .default)
            $0.addShadow(withOpacity: 0.1, radius: 10)
            $0.shadowImage = UIImage()
            $0.tintColor = UIColor.lightBackArrowColor
            $0.barStyle = .default
            $0.isTranslucent = false
            $0.backgroundColor = .white
            $0.titleTextAttributes =
                [NSAttributedString.Key.foregroundColor: UIColor.black,
                 NSAttributedString.Key.font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.bold)]
        }
        guard self.presentingViewController != nil else { return }
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: Icons.NavigationBar.close, style: .plain, target: self, action: #selector(tapBackButton))
    }
    
    private func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.registerCell(GuestListRsvpFirstNilCell.self)
        tableView.registerCell(GuestListRsvpHostCell.self)
        tableView.registerCell(GuestListRsvpIntroTextCell.self)
        tableView.registerCell(GuestListRsvpGuestCell.self)
        tableView.registerCell(GuestListRsvpCountPremiumMembersCell.self)
    }
    
    private func bindDestinationObservables() {
        viewModel.destinationObs
            .subscribe(onNext: { [unowned self] destination in
                self.handle(destination: destination)
            }).disposed(by: disposeBag)
    }
    
    private func handle(destination: GuestListRsvpViewDestination) {
        switch destination {
        case .hostDetails:
            router.push(destination: destination)
        }
    }
    
    private func reloadIntroViewTableView() {
        tableView.beginUpdates()
        tableView.backgroundColor = viewModel.isIntroView ? .appBackground : .white
        guard let visibleIndexPaths = tableView.indexPathsForVisibleRows else { return }
        visibleIndexPaths.forEach {
            let item = viewModel.loadTableData[$0.row]
            if let introTextCell = tableView.cellForRow(at: $0) as? GuestListRsvpIntroTextCell {
                item.configure(cell: introTextCell)
            } else if let guestCell = tableView.cellForRow(at: $0) as? GuestListRsvpGuestCell {
                item.configure(cell: guestCell)
            }
        }
        tableView.endUpdates()
    }

    private func appCheckBoxDidChangeStatus(isChecked: Bool, cell: UITableViewCell) {
        guard let cellIndex = tableView.indexPath(for: cell) else { return }
        
        viewModel.handleCheckBoxDidChangeStatus(index: cellIndex.row, isChecked: isChecked)
    }

    @objc func tapBackButton() {
        parent?.dismiss()
    }
}

// MARK: - Table View Data Source
extension GuestListRsvpViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.loadTableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = viewModel.loadTableData[indexPath.row]
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: type(of: item).reuseId) else { fatalError() }
        item.configure(cell: cell)
        
        return cell
    }
    
}

// MARK: - Table View Delegate
extension GuestListRsvpViewController: UITableViewDelegate {

}
