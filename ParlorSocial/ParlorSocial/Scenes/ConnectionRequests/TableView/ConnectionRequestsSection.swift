//
//  ConnectionRequestsSection.swift
//  ParlorSocialClub
//
//  Created by Przemysław Cygan - KISS digital on 23/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class ConnectionRequestsSection: BasicTableSection<ConnectionRequest, ConnectionRequestTableViewCell> {

    private let acceptSelector: ((_ cell: UITableViewCell, _ memberId: Int) -> Void)
    private let rejectSelector: ((_ cell: UITableViewCell, _ memberId: Int) -> Void)

    init(
        items: [ConnectionRequest],
        itemSelector: @escaping ((ConnectionRequest) -> Void),
        acceptSelector: @escaping (_ cell: UITableViewCell, _ memberId: Int) -> Void,
        rejectSelector: @escaping (_ cell: UITableViewCell, _ memberId: Int) -> Void
    ) {
        self.acceptSelector = acceptSelector
        self.rejectSelector = rejectSelector
        super.init(items: items, itemSelector: itemSelector)
    }

    override func configure(cell: UITableViewCell, at indexPath: IndexPath) {
        if let cell = cell as? ConnectionRequestTableViewCell {
            cell.acceptSelector = acceptSelector
            cell.rejectSelector = rejectSelector
        }

        super.configure(cell: cell, at: indexPath)
    }

    static func make(
        for connections: [ConnectionRequest],
        itemSelector: @escaping (ConnectionRequest) -> Void,
        acceptSelector: @escaping (_ cell: UITableViewCell, _ memberId: Int) -> Void,
        rejectSelector: @escaping (_ cell: UITableViewCell, _ memberId: Int) -> Void
    ) -> ConnectionRequestsSection {
        return ConnectionRequestsSection(
            items: connections,
            itemSelector: itemSelector,
            acceptSelector: acceptSelector,
            rejectSelector: rejectSelector
        )
    }

}
