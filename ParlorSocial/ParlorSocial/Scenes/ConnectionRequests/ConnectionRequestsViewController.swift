//
//  ConnectionRequestsViewController.swift
//  ParlorSocialClub
//
//  Created by Przemysław Cygan - KISS digital on 23/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

final class ConnectionRequestsViewController: AppViewController {

    // MARK: Properties

    var analyticEventScreen: AnalyticEventScreen? = .connectionRequests
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }

    override var observableSources: ObservableSources? {
        return viewModel
    }

    private var tableManager: TableViewManager!

    private let viewModel: ConnectionRequestsLogic = ConnectionRequestsViewModel()
    private let tableView: UITableView = UITableView().manualLayoutable()

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.loadView()
        setup()
        bindViewModel()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        adjustNavigationBar()
    }

    // MARK: Setup

    private func setup() {
        extendedLayoutIncludesOpaqueBars = true
        setupTableView()
    }

    private func setupTableView() {
        tableView.backgroundColor = .white
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorInset = Constants.ConnectionRequestsViewController.tableSeparatorInsets

        tableManager = TableViewManager(tableView: tableView, delegate: self)
        tableManager.setupRefreshControl()

        view.addSubview(tableView)
        tableView.edgesToParent()
    }

    private func adjustNavigationBar() {
        title = Strings.ConnectionRequests.title.localized.uppercased()
        statusBar?.backgroundColor = .white
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.setShadowBarEnabled(true)
        navigationController?.applyWhiteTheme()
    }

    // MARK: Data binding

    private func bindViewModel() {
        observableSources?.alertDataObs.show(using: self).disposed(by: disposeBag)
        observableSources?.errorObs.handleWithAlerts(using: self).disposed(by: disposeBag)
        observableSources?.progressBarObs.showProgressBar(using: self, isDark: true).disposed(by: disposeBag)

        viewModel.destinationsObs
            .bind { [unowned self] in self.navigate(using: $0) }
            .disposed(by: disposeBag)

        viewModel.paginatedSectionsObs
            .bind { [unowned self] in self.handle(paginatedSectionsState: $0) }
            .disposed(by: disposeBag)

        viewModel.connectionRequestTableUpdateObs
            .bind { [unowned self] in self.updateTableViewAfterConnectionRequestChange(data: $0) }
            .disposed(by: disposeBag)
    }

    private func handle(paginatedSectionsState state: SectionPaginatedRepositoryState) {
        switch state {
        case .loading: tableManager.showLoadingFooter()
        case .paging(let elements):
            tableManager.setupWith(sections: elements)
            tableManager.showLoadingFooter()
        case .populated(let elements):
            tableManager.invalidateCache()
            tableManager.setupWith(sections: elements)
            tableManager.hideLoadingFooter()
        case .error: tableManager.hideLoadingFooter()
        }
    }

    private func updateTableViewAfterConnectionRequestChange(data: ConnectionRequestUpdateTableModel) {
        let indexPath = IndexPath(item: data.itemIndex, section: data.sectionIndex)
        let newIndexPath = IndexPath(item: tableView.numberOfRows(inSection: data.sectionIndex) - 1, section: data.sectionIndex)
        var newFrame: CGRect
        var backgroundColor: UIColor
        var deleteRowAnimation: UITableView.RowAnimation

        switch data.actionType {
        case .accept:
            newFrame = CGRect(x: data.cellToRemove.bounds.width, y: data.cellToRemove.frame.origin.y, width: data.cellToRemove.bounds.width, height: data.cellToRemove.bounds.height)
            backgroundColor = .appYellow
            deleteRowAnimation = .right
        case .reject:
            newFrame = CGRect(x: -data.cellToRemove.bounds.width, y: data.cellToRemove.frame.origin.y, width: data.cellToRemove.bounds.width, height: data.cellToRemove.bounds.height)
            backgroundColor = .appGrey
            deleteRowAnimation = .left
        }

        animateBackgroundChange(forCell: data.cellToRemove, backgroundColor: backgroundColor) {
            UIView.animate(
                withDuration: Constants.ConnectionRequestsViewController.tableAnimationDuration,
                animations: { data.cellToRemove.frame = newFrame },
                completion: { _ in
                    self.tableView.performBatchUpdates({
                        self.tableView.deleteRows(at: [indexPath], with: deleteRowAnimation)
                        if data.addedNewItem {
                            self.tableView.insertRows(at: [newIndexPath], with: .bottom)
                        }
                        self.tableManager.setSections(data.updatedSections)
                    }) { _ in data.completionHandler() }
                }
            )
        }
    }

    private func animateBackgroundChange(forCell cell: UITableViewCell, backgroundColor: UIColor, completion: @escaping () -> Void) {
        UIView.animate(
            withDuration: 0.1,
            animations: { cell.contentView.backgroundColor = backgroundColor },
            completion: { _ in completion() }
        )
    }

    // MARK: - Actions

    private func navigate(using destination: ConnectionRequestsDestination) {
        switch destination {
        case .memberProfile:
            router.push(destination: destination)
        }
    }

}

// MARK: TableManagerDelegate

extension ConnectionRequestsViewController: TableManagerDelegate {
    func didSwipeForRefresh() {
        viewModel.refresh()
    }

    func loadMoreData() {
        viewModel.loadMore()
    }

    func tableViewDidScroll(_ scrollView: UIScrollView) {
    }
}

// MARK: Constants

private extension Constants {
    enum ConnectionRequestsViewController {
        static let tableAnimationDuration = 0.35
        static let tableSeparatorInsets = UIEdgeInsets(side: 16)
    }
}
