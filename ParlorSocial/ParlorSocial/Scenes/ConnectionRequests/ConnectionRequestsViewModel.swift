//
//  ConnectionRequestsViewModel.swift
//  ParlorSocialClub
//
//  Created by Przemysław Cygan - KISS digital on 23/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import RxSwift
import RxCocoa

protocol ConnectionRequestsLogic: BaseViewModelLogic {
    var destinationsObs: Observable<ConnectionRequestsDestination> { get }
    var paginatedSectionsObs: Observable<SectionPaginatedRepositoryState> { get }
    var connectionRequestTableUpdateObs: Observable<ConnectionRequestUpdateTableModel> { get }
    func refresh()
    func loadMore()
}

final class ConnectionRequestsViewModel: BaseViewModel {

    typealias ObservableApiResponse = Observable<ApiResponse<ConnectionRequests.Get.Response>>

    var analyticEventReferencedId: Int?

    private var connectionRequestsRequestModel: ConnectionRequests.Get.Request = ConnectionRequests.Get.Request.initial

    private let connectionsRepository: ConnectionsRepositoryProtocol = ConnectionsRepository.shared
    private let destinationRelay: PublishRelay<ConnectionRequestsDestination> = PublishRelay()
    private let loadDataRelay = BehaviorRelay(value: 0)
    private let paginatedDataRelay = BehaviorRelay<PaginatedData>(value: PaginatedData(sections: [], metadata: ListMetadata.initial))
    private let paginatedSectionsRelay = BehaviorRelay<SectionPaginatedRepositoryState>(value: .loading)
    private let connectionRequestTableUpdateRelay = PublishRelay<ConnectionRequestUpdateTableModel>()

    override init() {
        super.init()
        self.initFlow()
    }

    private func initFlow() {
        bindLoadData()
    }

    private func bindLoadData() {
        loadDataRelay
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .userInteractive))
            .setLoadingStateOnSubscribe(with: paginatedSectionsRelay)
            .skipOnListPopulated(with: paginatedDataRelay)
            .flatMapLatest { [unowned self] _ -> ObservableApiResponse in
                return self.connectionsRepository.getConnectionRequests(
                    using: self.connectionRequestsRequestModel
                )
            }
            .mapResponseToTableSection(
                with: paginatedDataRelay,
                sectionStateRelay: paginatedSectionsRelay,
                sectionMaker: { [unowned self] in self.makeSections(for: $0) }
            )
            .changePaginatedState(with: paginatedSectionsRelay)
            .doOnNext { [unowned self] _ in self.connectionRequestsRequestModel.increasePage() }
            .bind(to: paginatedDataRelay)
            .disposed(by: disposeBag)
    }

    private func makeSections(for connectionRequests: [ConnectionRequest]) -> [TableSection] {
        return [
            ConnectionRequestsSection.make(
                for: connectionRequests,
                itemSelector: { [unowned self] in self.didSelectConnectionRequest($0) },
                acceptSelector: { [unowned self] in self.didAcceptConnectionRequest($0, $1) },
                rejectSelector: { [unowned self] in self.didRejectConnectionRequest($0, $1) }
            )
        ]
    }

    private func didSelectConnectionRequest(_ model: ConnectionRequest) {
        self.destinationRelay.accept(.memberProfile(id: model.memberId ?? 1))
    }

    private func didAcceptConnectionRequest(_ cell: UITableViewCell, _ requestId: Int) {
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.ConnectionRequests.rejectRequest.rawValue)
        completeConnectionRequestStateChange(actionType: .accept, cell: cell, requestId: requestId)
        let data = UpdateConnectionRequest(accept: true)
        updateConnection(with: requestId, using: data)
    }

    private func didRejectConnectionRequest(_ cell: UITableViewCell, _ requestId: Int) {
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.ConnectionRequests.acceptRequest.rawValue)
        completeConnectionRequestStateChange(actionType: .reject, cell: cell, requestId: requestId)
        let data = UpdateConnectionRequest(accept: false)
        updateConnection(with: requestId, using: data)
    }

    private func updateConnection(with connectionId: Int, using data: UpdateConnectionRequest) {
        connectionsRepository.updateConnection(with: connectionId, using: data).subscribe().disposed(by: disposeBag)
    }

}

extension ConnectionRequestsViewModel: ConnectionRequestsLogic {
    var destinationsObs: Observable<ConnectionRequestsDestination> {
        return destinationRelay.asObservable()
    }

    var paginatedSectionsObs: Observable<SectionPaginatedRepositoryState> {
        return paginatedSectionsRelay.asObservable()
    }

    var connectionRequestTableUpdateObs: Observable<ConnectionRequestUpdateTableModel> {
        return connectionRequestTableUpdateRelay.asObservable()
    }

    func refresh() {
        paginatedDataRelay.accept(PaginatedData(sections: [], metadata: ListMetadata.initial))
        connectionRequestsRequestModel.set(page: 1)
        loadMore()
    }

    func loadMore() {
        loadDataRelay.accept(0)
    }
}

extension ConnectionRequestsViewModel {
    private func completeConnectionRequestStateChange(actionType: ConnectionRequestUpdateTableModel.ActionType, cell: UITableViewCell, requestId: Int) {
        var paginatedData = paginatedDataRelay.value
        let sections = paginatedData.sections

        guard
            let sectionIndex = sections.firstIndex(where: { $0.cellType == ConnectionRequestTableViewCell.self }),
            var items: [ConnectionRequest] = sections[sectionIndex].getItems(),
            let dataIndex = items.firstIndex(where: { $0.requestId == requestId })
        else { return }

        items.remove(at: dataIndex)
        sections[sectionIndex].setItems(items)

        let updateModel = ConnectionRequestUpdateTableModel(
            cellToRemove: cell,
            actionType: actionType,
            sectionIndex: sectionIndex,
            itemIndex: dataIndex,
            updatedSections: sections,
            addedNewItem: false,
            completionHandler: { [unowned self] in
                paginatedData.sections = sections
                self.paginatedDataRelay.accept(paginatedData)
            }
        )
        connectionRequestTableUpdateRelay.accept(updateModel)
    }
}
