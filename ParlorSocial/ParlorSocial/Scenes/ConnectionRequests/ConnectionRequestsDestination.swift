//
//  ConnectionRequestsDestination.swift
//  ParlorSocialClub
//
//  Created by Przemysław Cygan - KISS digital on 23/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

enum ConnectionRequestsDestination {
    case memberProfile(id: Int)
}

extension ConnectionRequestsDestination: Destination {
    var viewController: UIViewController {
        switch self {
        case .memberProfile:
            let viewController = MemberProfileViewController()
            viewController.viewModel = MemberProfileViewModel(member: .mock)

            return viewController
        }
    }
}
