//
//  ActivationDetailsViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 11/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol ActivationDetailsLogic: BaseViewModelLogic {
    var activationObs: Observable<Activation?> { get }
    var footerStateObs: Observable<EditableFooterState> { get }
    var destinationsObs: Observable<ActivationDetailsDestination> { get }
    var locationClickObs: Observable<URL> { get }
    var bottomBarTypeObs: Observable<ActivationDetailsBottomBarType> { get }
    var notifyMeDateObs: Observable<String> { get }
    var refreshingSubject: BehaviorSubject<Bool> { get }
    var clickNotifyMe: PublishSubject<Void> { get }
    var guestCountRelay: BehaviorRelay<Int> { get }
    func navigateToGuestForm()
    func navigateToGuestList()
    func editCurrentRsvp()
    func navigateToShareActivation()
    func navigateToMap()
    func navigateToHostDetails(host: ParlorHost)
    func didClickActionButton()
    func didClickOnConnections()
}

class ActivationDetailsViewModel: BaseViewModel {
    let analyticEventReferencedId: Int?
    private let activationRepository = ActivationsRepository.shared
    private let guestListRepository = GuestListRsvpRepository.shared
    private let activationRelay: BehaviorRelay<Activation?>
    private let destinationRelay: PublishRelay<ActivationDetailsDestination> = PublishRelay()
    private let locationClickRelay: PublishRelay<URL> = PublishRelay()
    private let activationId: Int
    private var addToGuestListHelper: AddToGuestListHelperProtocol?
    private let bottomBarTypeRelay: BehaviorRelay<ActivationDetailsBottomBarType> =
        .init(value: .canRsvpWithoutGuests)
    private let notifyMeDateRelay: BehaviorRelay<String> = .init(value: "")
    private var activationIsEmpty: Bool { return activationRelay.value == nil }
    private var calendarSyncViewProvider = CalendarSyncViewProvider()
    
    let refreshingSubject: BehaviorSubject<Bool> = BehaviorSubject(value: false)
    let clickNotifyMe: PublishSubject<Void> = PublishSubject()
    let guestCountRelay: BehaviorRelay<Int> = .init(value: 0)
    
    let hostSelectionSubject: PublishSubject<ParlorHost> = PublishSubject()
    
    lazy var checkInService: CheckInService = {
        let checkInService = CheckInService(locationService: LocationService(), guestListRepository: guestListRepository, activationRepository: activationRepository)
        checkInService.delegate = self
        return checkInService
    }()
    
    private var activationChangedObs: Observable<(Activation, Activation)> {
        return activationRepository.newActivationDataObs
            .withLatestFrom(activationRelay.ignoreNil()) { ($0, $1) }
            .filter { $0.id == $1.id && $0 != $1 }
    }

    private lazy var activityViewControllerCompletion: UIActivityViewController.CompletionWithItemsHandler = {
        activityType, completed, returnedItems, error in
        guard completed, let type = activityType else { return }

        AnalyticService.shared.saveNativeAnalyticShareActivation(with: type)
    }
    
    init(activation: Activation, navigationSource: ActivationDetailsNavigationSource) {
        self.activationRelay = BehaviorRelay(value: activation)
        self.activationId = activation.id
        self.analyticEventReferencedId = activation.id
        
        super.init()
        self.analyticEventNavigationSource = navigationSource.rawValue
        self.initFlow()
        self.refreshingSubject.onNext(true)
    }
    
    init(forActivationId activationId: Int, navigationSource: ActivationDetailsNavigationSource) {
        self.activationRelay = BehaviorRelay(value: nil)
        self.activationId = activationId
        self.analyticEventReferencedId = activationId
        
        super.init()
        self.analyticEventNavigationSource = navigationSource.rawValue
        self.initFlow()
        self.fetchActivationWithProgressBar()
    }

}

// MARK: Private logic
private extension ActivationDetailsViewModel {
    
    private func initFlow() {
        addToGuestListHelper = AddToGuestListHelper(viewModel: self)
        
        refreshingSubject
            .filter { $0 }
            .flatMapFirst { [unowned self] _ in self.activationRepository.getActivation(withId: self.activationId) }
            .subscribe(onNext: { [weak self] response in
                self?.handleActivationResponse(response)
            }).disposed(by: disposeBag)
        
        let changesObs = activationChangedObs.share()
        
        changesObs
            .map { (new, _) in return new }
            .bind(to: activationRelay)
            .disposed(by: disposeBag)
        
        changesObs
            .filter { [unowned self] _ in self.calendarSyncViewProvider.showCalendarSyncViewController() }
            .filter { (new, old) in
                return new.rsvp?.status == .guestList && old.rsvp?.status != new.rsvp?.status }
            .map { [unowned self] (new, _) in
                let changeSource: CalendarSyncChangedEventSource = self.calendarSyncViewProvider.isFirstRsvp() ? .afterFirstRsvp : .reminderOther
                return ActivationDetailsDestination.calendarSync(activation: new, settingsChangeSource: changeSource)
            }
            .observeOn(MainScheduler.asyncInstance)
            .bind(to: destinationRelay)
            .disposed(by: disposeBag)
        
        clickNotifyMe
            .withLatestFrom(activationRelay.ignoreNil())
            .flatMap { [unowned self] activation in
                return self.activationRepository.notifyMeRsvpAvailability(forActivation: activation)
            }.subscribe(onNext: { [unowned self] response in
                self.handleNotifyMeResponse(response)
            }).disposed(by: disposeBag)
        
        activationRelay.ignoreNil()
            .doOnNext { [unowned self] activation in
                self.navigateToUpgradeToPremiumIfNeeded(usingActivation: activation)
            }
            .subscribe(onNext: { [unowned self] activation in
                self.bottomBarTypeRelay.accept(self.getBottomBarType(by: activation))
                self.setupNotifyMeDate(activation: activation)
            }).disposed(by: disposeBag)

        activationRepository.activationWithIdChangedObs
            .subscribe(onNext: { [unowned self] _ in
                self.fetchActivationWithProgressBar()
            }).disposed(by: disposeBag)
    }
    
    private func handleActivationResponse(_ response: SingleActivationResposne) {
        refreshingSubject.onNext(false)
        switch response {
        case .success(let dataResponse):
            activationRelay.accept(dataResponse.data)
        case .failure(let error):
            alertDataSubject.onNext(AlertData.fromAppError(
                appError: error,
                bottomButtonAction: activationIsEmpty ? { [unowned self] in self.closeViewSubject.emitElement() } : {},
                closeButtonAction: activationIsEmpty ? { [unowned self] in self.closeViewSubject.emitElement() } : {}
            ))
        }
    }

    private func handleShareDataResponse(_ response: ShareDataResponse) {
        refreshingSubject.onNext(false)
        switch response {
        case .success(let dataResponse):
            var activation = activationRelay.value
            activation?.setShareData(dataResponse.data)
            activationRelay.accept(activation)
            destinationRelay.accept(.share(shareData: dataResponse.data, delegate: self))
        case .failure(let error):
            alertDataSubject.onNext(AlertData.fromAppError(
                appError: error,
                bottomButtonAction: activationIsEmpty ? { [unowned self] in self.closeViewSubject.emitElement() } : {},
                closeButtonAction: activationIsEmpty ? { [unowned self] in self.closeViewSubject.emitElement() } : {}
            ))
        }
    }
    
    private func handleNotifyMeResponse(_ response: ApiResponse<MessageResponse>) {
        switch response {
        case .success:
            saveNotifyDelayedRsvpAnalyticEvent()
        case .failure(let error):
            errorSubject.onNext(error)
        }
    }
    
    private func saveNotifyDelayedRsvpAnalyticEvent() {
        guard let sessionId = AnalyticService.shared.sessionId,
            let userId = Config.userProfile?.id else { return }
        let event = AnalyticEvent(
            userId: userId,
            deviceToken: Config.deviceToken,
            timestamp: Int64(Date().timeIntervalSince1970 * 1000),
            type: .delayedRsvpNotificationSubscribed,
            data: .createForSpecialType(referencedId: analyticEventReferencedId),
            sessionId: sessionId
        )
        AnalyticService.shared.saveEventInDatabase(event)
    }
    
    private func openRsvpForm(with guestSlotsAlertShown: Bool) {
        guard let activation = activationRelay.value else { return }
        destinationRelay.accept(
            .guestListForm(activation: activation, guestsCount: guestCountRelay.value, guestSlotsChecked: guestSlotsAlertShown)
        )
    }
    
    private func setupNotifyMeDate(activation: Activation) {
        if activation.isApproximatedStartTime {
            self.notifyMeDateRelay.accept(Strings.Activation.dateTBD.localized)
        } else {
            self.notifyMeDateRelay.accept(
                String(
                    format: Strings.Activation.rsvpBeginsAt.localized,
                    activation.allowToRsvpAt?.toString(withFormat: Date.Format.membershipRenewal) ?? "")
            )
        }
    }

    //    1. If user is preview and member guest limit is higher than 0 -> show view with green rsvp button and count of guests
    //    2. If user is preview and member guest limit is less or equal to 0 -> show view with green rsvp button without count of guests
    //    3. If I did rsvp (guestlist or waitlist) -> show edit bottom bar
    //    4. If I didn't do rsvp and allowRsvpUntil is lower than current date -> show “event is now closed”
    //    5. If I can't do rsvp yet and I did “notify me” -> show “stay tuned" view
    //    6. If I can't do rsvp yet and I didn't do "notify me" -> show view with button “notify me”
    //    7. If I can do rsvp and member guest limit is higher than 0 -> show view with green rsvp button and count of guests
    //    8. If nothing from above points -> show view with green rsvp button without count of guests
    private func getBottomBarType(by activation: Activation) -> ActivationDetailsBottomBarType {
        if Config.userProfile?.isPreview ?? false && activation.membersGuestLimit > 0 {
            return .canRsvpWithGuests
        } else if Config.userProfile?.isPreview ?? false && activation.membersGuestLimit <= 0 {
            return .canRsvpWithoutGuests
        } else if activation.rsvp != nil {
            return .editRsvp
        } else if Date() > activation.allowToRsvpUntil ?? Date() {
            return .rsvpIsClosed
        } else if activation.allowToRsvpAt ?? Date() > Date(), activation.isNotifyMe ?? false {
            return .stayTuned
        } else if activation.allowToRsvpAt ?? Date() > Date(), !(activation.isNotifyMe ?? false) {
            return .notifyMe
        } else if activation.membersGuestLimit > 0 {
            return .canRsvpWithGuests
        } else {
            return .canRsvpWithoutGuests
        }
    }
    
    private func fetchActivationWithProgressBar() {
        activationRepository.getActivation(withId: activationId).showingProgressBar(with: self)
            .subscribe(onNext: { [weak self] response in
                self?.handleActivationResponse(response)
            }).disposed(by: disposeBag)
    }

    private func fetchShareDataWithProgressBar() {
        activationRepository.getShareData(forActivationId: activationId).showingProgressBar(with: self)
            .subscribe(onNext: { [weak self] response in
                self?.handleShareDataResponse(response)
            }).disposed(by: disposeBag)
    }

    private func navigateToUpgradeToPremiumIfNeeded(usingActivation activation: Activation) {
        guard activation.isPremium && (Config.userProfile?.isStandard ?? false) else { return }
        destinationRelay.accept(.upgradeToPremium(navigationSource: .activation(activation.id)))
    }
}

// MARK: Interface logic methods
extension ActivationDetailsViewModel: ActivationDetailsLogic {
    
    
    var notifyMeDateObs: Observable<String> {
        return notifyMeDateRelay.asObservable()
    }
    
    var locationClickObs: Observable<URL> {
        return locationClickRelay.asObservable()
    }
    
    var footerStateObs: Observable<EditableFooterState> {
        return activationObs
            .ignoreNil()
            .take(1).concat(activationObs.ignoreNil())
            .pairwise().map { old, new -> EditableFooterState in
                if old.rsvp != new.rsvp && old.rsvp?.status == new.rsvp?.status {
                    return .edited
                } else {
                    return .rsvp(withStatus: new.rsvp?.status ?? .creating)
                }
        }
    }
    
    var activationObs: Observable<Activation?> {
        return activationRelay.asObservable()
    }
    
    var destinationsObs: Observable<ActivationDetailsDestination> {
        return destinationRelay.asObservable()
    }

    var bottomBarTypeObs: Observable<ActivationDetailsBottomBarType> {
        return bottomBarTypeRelay.asObservable()
    }
    
    func navigateToGuestForm() {
        if Config.userProfile?.isPreview ?? false {
            destinationRelay.accept(.membershipPlans)
        } else {
            addToGuestListHelper?.successCallback = { [weak self] in self?.openRsvpForm(with: false); self?.progressBarSubject.onNext(false) }
            addToGuestListHelper?.alertButtonCallback = { [weak self] in self?.openRsvpForm(with: true) }
            self.progressBarSubject.onNext(true)
            addToGuestListHelper?.checkIsAvailablePlacesInActivation(
                activationId: activationId,
                guestsWithUserCount: guestCountRelay.value + 1
            )
        }
    }
    
    func didClickOnConnections() {
        print("navigate in the model")
    }

    func navigateToShareActivation() {
        guard let shareData = activationRelay.value?.shareData else { return fetchShareDataWithProgressBar() }

        destinationRelay.accept(.share(shareData: shareData, delegate: self))
    }
    
    func navigateToGuestList() {
        guard let activation = activationRelay.value else { return }
        checkInService.navigateToGuestList(activationId: activationId, checkInStrategy: activation.checkInStrategy)
    }
    
    func navigateToMap() {
        guard let lat = activationRelay.value?.location?.lat,
            let lng = activationRelay.value?.location?.lng,
            let address = activationRelay.value?.location?.mapLocation else { return }
        guard let mapUrl = RouteMapService.getMapUrl(lat: lat, lng: lng, address: address) else { destinationRelay.accept(.mapProvider); return }
        locationClickRelay.accept(mapUrl)
    }
    
    func editCurrentRsvp() {
        guard let activation = activationRelay.value else { return }
        destinationRelay.accept(
            activation.canBeEdited ?
                .guestListForm(activation: activation, guestsCount: nil, guestSlotsChecked: true) :
                .waitList(activation: activation)
            )
    }
    
    func navigateToHostDetails(host: ParlorHost) {
        if Config.userProfile?.isPreview ?? false {
            destinationRelay.accept(.membershipPlans)
        } else {
            destinationRelay.accept(.hostDetails(host: host))
        }
    }

    func didClickActionButton() {
        guard let activation = activationRelay.value else { return }
        switch activation.actionButtonType {
        case .guestList:
            checkInService.navigateToGuestList(activationId: activationId, checkInStrategy: activation.checkInStrategy)
        case .updates, .live:
            destinationRelay.accept(.activationDetailsInfo(activationId: activationId))
        case .rsvp, .rsvpd, .none:
            break
        }
    }

}

extension ActivationDetailsViewModel: ShareActivationDelegate {
    func presentMoreShareOptions(with data: ShareData) {
        destinationRelay.accept(.moreShareOptions(shareData: data, completion: activityViewControllerCompletion))
    }
}

// MARK: - Check-in service
extension ActivationDetailsViewModel: CheckInServiceDelegate {
    func didEndCheckInSuccess(_ checkInService: CheckInService, activationId: Int) {
        progressBarSubject.onNext(false)
        destinationRelay.accept(.guestList(activationId: activationId))
    }
}
