//
//  ActivationDetailsViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 11/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import GoogleMaps
import RxSwift

class ActivationDetailsViewController: AppViewController {

    // MARK: - Properties
    let analyticEventScreen: AnalyticEventScreen? = .activationDetails
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }
    
    override var observableSources: ObservableSources? {
        return viewModel
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private let mapZoomLevel: Float = 12.0
    private var viewModel: ActivationDetailsLogic!
    private lazy var refreshControl: UIRefreshControl = {
        return UIRefreshControl().apply { $0.tintColor = .white }
    }()
    
    // MARK: - Views
    private(set) var mainScrollView: UIScrollView!
    private(set) var scrollContentView: UIView!
    private(set) var activationImageView: UIImageView!
    private(set) var topGradient: GradientView!
    private(set) var dateInfoLabel: UILabel!
    private(set) var titleLabel: UILabel!
    private(set) var friendsList: FriendsListView!
    private(set) var descriptionTextView: ParlorTextView!
    private(set) var hostsStack: HostsStack!
    private(set) var timeInfo: IconedLabeledInfo!
    private(set) var infoSeperator: UIView!
    private(set) var entryInfo: IconedLabeledInfo!
    private(set) var buttonsStack: UIStackView!
    private(set) var shareButton: ParlorButton!
    private(set) var actionButton: ParlorButton!
    private(set) var locationSeperator: UIView!
    private(set) var locationInfo: IconedLabeledInfo!
    private(set) var locationLabel: ParlorLabel!
    private(set) var addressScroll: UIScrollView!
    private(set) var addressLabel: ParlorLabel!
    private(set) var mapView: GMSMapView!
    private(set) var bottomEdgeMapView: UIView!
    private(set) var viewOnMapButton: RoundedButton!
    private(set) var guestsNumberView: GuestNumberView!
    private(set) var notifyMeBottomBarView: NotifyMeBottomBarView!
    private(set) var editableRsvpFooter: EditableActivationFooter!
    private(set) var guestPickerView: GuestPickerView!
    private(set) var stayTunedView: WaitingForRsvpAvailabilityView!
    private(set) var topConstraint: NSLayoutConstraint!
    private lazy var bottomBars: [UIView] = {
        return [guestsNumberView, notifyMeBottomBarView, editableRsvpFooter, stayTunedView]
    }()

    // MARK: - Initialization
    init(activation: Activation, navigationSource: ActivationDetailsNavigationSource) {
        super.init(nibName: nil, bundle: nil)
        initialize(with: activation, navigationSource: navigationSource)
    }
    
    init(forActivationId activationId: Int, navigationSource: ActivationDetailsNavigationSource) {
        super.init(nibName: nil, bundle: nil)
        initialize(forActivationId: activationId, navigationSource: navigationSource)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    override func observeViewModel() {
        observableSources?.alertDataObs.show(using: self).disposed(by: disposeBag)
        observableSources?.errorObs.handleWithAlerts(using: self).disposed(by: disposeBag)
        observableSources?.progressBarObs.showProgressBar(using: self, isDark: true).disposed(by: disposeBag)
        
        viewModel.activationObs
            .ignoreNil()
            .subscribe(onNext: { [weak self] activation in
                self?.setup(with: activation)
            }).disposed(by: disposeBag)
        
        viewModel.refreshingSubject
            .filter { !$0 }
            .subscribe(onNext: { [weak self] _ in
                self?.refreshControl.endRefreshing()
            }).disposed(by: disposeBag)

        viewModel.footerStateObs
            .doOnNext { [unowned self] state in
                self.editableRsvpFooter.state = state
            }.subscribe().disposed(by: disposeBag)
        
        viewModel.locationClickObs
            .subscribe(onNext: { [unowned self] url in
                self.openMap(with: url)
            }).disposed(by: disposeBag)
        
        viewModel.bottomBarTypeObs
            .subscribe(onNext: { [unowned self] bottomBarType in
                self.setupBottomBar(by: bottomBarType)
            }).disposed(by: disposeBag)
        
        viewModel.notifyMeDateObs
            .subscribe(onNext: { [unowned self] date in
                self.setupNotifyMeBottomBar(with: date)
            }).disposed(by: disposeBag)
        
        viewModel.closeObs
            .subscribe(onNext: { [unowned self] _ in
                self.dismiss()
            }).disposed(by: disposeBag)
        
    }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
        disableScrollInsets(for: mainScrollView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        mainScrollView.delegate = self
        adjustNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        changeNavigationBarAlphaWithAnimation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        mainScrollView.delegate = nil
        mainScrollView.stopScrolling()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    
}

// MARK: - Private methods
extension ActivationDetailsViewController {
    
    private func initialize(with activation: Activation, navigationSource: ActivationDetailsNavigationSource) {
        self.viewModel = ActivationDetailsViewModel(activation: activation, navigationSource: navigationSource)
    }
    
    private func initialize(forActivationId activationId: Int, navigationSource: ActivationDetailsNavigationSource) {
        self.viewModel = ActivationDetailsViewModel(forActivationId: activationId, navigationSource: navigationSource)
    }
    
    private func adjustNavigationBar() {
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.applyTransparentTheme(withColor: .white)
        statusBar?.backgroundColor = .clear
        setNeedsStatusBarAppearanceUpdate()
        guard self.presentingViewController != nil else { return }
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: Icons.NavigationBar.close, style: .plain, target: self, action: #selector(tapCloseButton))
    }

    private func setupViews() {
        let builder = ActivationDetailsViewBuilder(controller: self)
        self.mainScrollView = builder.buildMainScrollView()
        self.scrollContentView = builder.buildScrollContentView()
        self.activationImageView = builder.buildActivationImageView()
        self.topGradient = builder.buildGradientView()
        self.dateInfoLabel = builder.buildDateInfoLabel()
        self.titleLabel = builder.buildNameLabel()
        self.friendsList = builder.buildFriendList()
        self.descriptionTextView = builder.buildDescriptionTextView()
        self.hostsStack = builder.buildHostsStack()
        self.timeInfo = builder.buildTimeInfo()
        self.infoSeperator = builder.buildInfoSeperator()
        self.entryInfo = builder.buildEntryInfo()
        self.buttonsStack = builder.buildButtonsStack()
        self.shareButton = builder.buildShareButton()
        self.actionButton = builder.buildActionButton()
        self.locationSeperator = builder.buildLocationSeperator()
        self.locationInfo = builder.buildLocationInfo()
        self.locationLabel = builder.buildLocationLabel()
        self.addressScroll = builder.buildAddressScroll()
        self.addressLabel = builder.buildAddressLabel()
        self.mapView = builder.buildMapView()
        self.bottomEdgeMapView = builder.buildBottomEdgeMapView()
        self.viewOnMapButton = builder.buildViewOnMapButton()
        self.mainScrollView.delegate = self
        self.mainScrollView.refreshControl = refreshControl
        self.guestsNumberView = builder.buildGuestNumberView()
        self.notifyMeBottomBarView = builder.buildNotifyMeBottomBarView()
        self.guestPickerView = builder.buildGuestPickerView()
        self.guestPickerView.delegate = self
        self.guestsNumberView.guestsPickerView = guestPickerView
        self.editableRsvpFooter = builder.buildEditableFooter()
        self.stayTunedView = builder.buildStayTunedView()
        builder.setupViews()
        topConstraint = mainScrollView.topAnchor.equal(to: view.topAnchor)
    }

    private func setupPresentationLogic() {
        guestsNumberView.rsvpButton.addTarget(self, action: #selector(didTapRsvpButton), for: .touchUpInside)
        notifyMeBottomBarView.notifyMeButton.addTarget(self, action: #selector(didTapNotifyMeButton), for: .touchUpInside)
        editableRsvpFooter.editButton.addTarget(self, action: #selector(didTapEditRsvpButton), for: .touchUpInside)
        refreshControl.addTarget(self, action: #selector(didSwipForRefresh), for: .valueChanged)
        shareButton.addTarget(self, action: #selector(didTapShareButton), for: .touchUpInside)
        actionButton.addTarget(self, action: #selector(didTapActionButton), for: .touchUpInside)
        viewOnMapButton.addTarget(self, action: #selector(didTapViewOnMapButton), for: .touchUpInside)
        friendsList.isUserInteractionEnabled = true
        friendsList.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapConnectionsAttending)))
        bindDestinationObservable()
    }
    
    private func setup(with activation: Activation) {
        activationImageView.getImage(from: activation.imageUrls?.imageUrl4x3)
        changeNavigationBarAlphaWithAnimation()
        dateInfoLabel.text = activation.startAt.toDetailsFormat(
            withEndDate: activation.endAt,
            isApproximatedStartTime: activation.isApproximatedStartTime
        )
            .checkIfVersionAppIsPreview(with: "")
        titleLabel.text = activation.name
        descriptionTextView.attributedText = activation.description.attributedFromHtml(
            withColor: UIColor.appSeparator,
            font: UIFont.custom(
                ofSize: Constants.FontSize.hintSize,
                font: UIFont.Roboto.light
            )
        )
        
        let friendsListImageUrls = [
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT-drrCEz9GSA877KXOBNcfP7OSl2Y-TEnoGAIVfnaMUD0UzL0&s",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTZzL16nYyZzBvEqywxy590Iw3yRAHcW9uTypagLeHPc_angRMWgIYcORWqwdREXca4sdei4v4&usqp=CAc"
        ].compactMap { URL(string: $0) }
        friendsList.set(imageUrls: friendsListImageUrls, title: "\(friendsListImageUrls.count)+ friends")
        
        timeInfo.infoContentLabel.text = activation.startAt.toString(withFormat: Date.Format.activationDetails)
        entryInfo.infoContentLabel.text = activation.formattedPrice
        setupActionButton(with: activation.actionButtonType)
        locationLabel.text = activation.location?.name
        setupAddressLabel(activation: activation)
        hostsStack.populate(with: activation.hosts)
        hostsStack.delegate = self
        guestPickerView.maxPossibleGuests = activation.membersGuestLimit
        if let location = activation.location {
            setupMapFor(lat: location.lat, lng: location.lng)
        }
        if let actionButtonType = activation.actionButtonType {
            setupActionButton(with: actionButtonType)
        }
    }
    
    private func bindDestinationObservable() {
        viewModel.destinationsObs
            .subscribe(onNext: { [unowned self] destination in
                self.handle(destination: destination)
            }).disposed(by: disposeBag)
    }
    
    private func handle(destination: ActivationDetailsDestination) {
        switch destination {
        case .guestListForm, .waitList, .calendarSync:
            router.present(destination: destination, animated: true, withStyle: .fullScreen)
        case .share, .moreShareOptions:
            router.present(destination: destination, animated: true, withStyle: .overCurrentContext)
        case .guestList, .hostDetails, .mapProvider, .membershipPlans, .activationDetailsInfo, .contactsAttending:
            router.push(destination: destination)
        case .upgradeToPremium:
            guard let currentControllersStack = navigationController?.viewControllers else { return }
            navigationController?.setViewControllers(currentControllersStack.dropLast() + [destination.viewController], animated: false)
        }
    }
    
    private func setupActionButton(with actionButtonType: ActionButtonType?) {
        guard let actionButtonType = actionButtonType else {
            actionButton.isHidden = true
            return
        }
        
        actionButton.style = getActionButtonStyle(for: actionButtonType)
        actionButton.isHidden = false
        actionButton.setImage(nil, for: .normal)
        
        switch actionButtonType {
        case .live:
            actionButton.title = actionButtonType.title
            actionButton.trackingId = AnalyticEventClickableElementName.ActivationDetails.actionLive.rawValue
        case .updates:
            actionButton.title = actionButtonType.title
            actionButton.trackingId = AnalyticEventClickableElementName.ActivationDetails.actionUpdates.rawValue
        case .guestList:
            actionButton.title = actionButtonType.title
            actionButton.setImage(Icons.ActivationDetails.guestList, for: .normal)
            actionButton.trackingId = AnalyticEventClickableElementName.ActivationDetails.actionGuestList.rawValue
        case .rsvp, .rsvpd:
            actionButton.isHidden = true
        }
    }
    
    private func getActionButtonStyle(for actionButtonType: ActionButtonType) -> ParlorButtonStyle {
        switch actionButtonType {
        case .live:
            return .filledWith(
                color: UIColor.backgroundGreen,
                titleColor: UIColor.appText,
                font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium),
                letterSpacing: Constants.LetterSpacing.none
            )
        case .updates, .guestList, .rsvp, .rsvpd:
            return .filledWith(
                color: UIColor.white,
                titleColor: UIColor.appText,
                font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium),
                letterSpacing: Constants.LetterSpacing.none
            )
        }
    }
    
    private func setupMapFor(lat: Double, lng: Double) {
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: mapZoomLevel)
        let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: lat, longitude: lng)).apply {
            $0.icon = Icons.ActivationDetails.mapMarker
        }
        mapView.camera = camera
        marker.map = mapView
    }
    
    private func setupAddressLabel(activation: Activation) {
        addressLabel.text = activation.location?.detailedOneLineLocation
        addressLabel.layoutIfNeeded()
        addressScroll.layoutIfNeeded()
        if addressLabel.bounds.width > addressScroll.bounds.width {
            addressLabel.text = activation.location?.detailedTwoLinesLocation
        }
    }
    
    private func setupBottomBar(by type: ActivationDetailsBottomBarType) {
        bottomBars.forEach { $0.isHidden = true }
        switch type {
        case .editRsvp: editableRsvpFooter.isHidden = false
        case .rsvpIsClosed, .stayTuned: setupStayTunedView(by: type)
        case .canRsvpWithoutGuests, .canRsvpWithGuests: setupGuestNumberView(by: type)
        case .notifyMe: notifyMeBottomBarView.isHidden = false
        }
    }
    
    private func setupStayTunedView(by type: ActivationDetailsBottomBarType) {
        stayTunedView.isHidden = false
        stayTunedView.type = .getType(basedOn: type)
    }
    
    private func setupGuestNumberView(by type: ActivationDetailsBottomBarType) {
        guestsNumberView.isHidden = false
        guestsNumberView.type = .getType(baseOn: type)
    }
    
    private func setupNotifyMeBottomBar(with date: String) {
        notifyMeBottomBarView.dateText = date
    }
    
    private func changeNavigationBarAlphaWithAnimation() {
        UIView.animate(withDuration: 0.2) { [unowned self] in
            if self.activationImageView.image != nil {
                self.mainScrollView.changeNavigationBarAlpha(using: self.activationImageView, to: self.navigationController?.navigationBar)
            }
        }
    }
    
    private func openMap(with url: URL) {
        UIApplication.shared.open(url)
    }
    
    @objc func didTapConnectionsAttending() {
        print("goto connections attending")
        let vc = ConnectionsAttendingVC()
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func didTapEditRsvpButton() {
        viewModel.editCurrentRsvp()
    }
    
    @objc private func didTapRsvpButton() {
        viewModel.navigateToGuestForm()
    }
    
    @objc private func didSwipForRefresh() {
        viewModel.refreshingSubject.onNext(true)
    }
    
    @objc private func didTapShareButton() {
        viewModel.navigateToShareActivation()
    }
    
    @objc private func didTapActionButton() {
        viewModel.didClickActionButton()
    }
    
    @objc private func didTapViewOnMapButton() {
        viewModel.navigateToMap()
    }
    
    @objc private func didTapNotifyMeButton() {
        viewModel.clickNotifyMe.emitElement()
    }
    
    @objc func tapCloseButton() {
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.ActivationDetails.close.rawValue)
        parent?.dismiss()
    }

}

extension ActivationDetailsViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < 0 {
            topConstraint.constant = -scrollView.contentOffset.y / 2
        } else {
            topConstraint.constant = 0
        }
        scrollView.changeNavigationBarAlpha(using: activationImageView, to: navigationController?.navigationBar)
    }
    
}

extension ActivationDetailsViewController: HostStackDelegate {
    
    func hostsStack(_ hostStack: HostsStack, didSelectHost host: ParlorHost) {
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.ActivationDetails.hostItem.rawValue)
        viewModel.navigateToHostDetails(host: host)
    }
    
}

extension ActivationDetailsViewController: GuestPickerViewDelegate {
    
    func guestsNumberValueChanged(value: Int) {
        viewModel.guestCountRelay.accept(value)
    }
    
}
