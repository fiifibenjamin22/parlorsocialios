//
//  ActivationDetailsViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 11/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import GoogleMaps

class ActivationDetailsViewBuilder {

    private unowned let controller: ActivationDetailsViewController
    private var view: UIView! { return controller.view }

    init(controller: ActivationDetailsViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension ActivationDetailsViewBuilder {

    private func setupProperties() {
        view.backgroundColor = .darkBackground
        controller.shareButton.apply {
            $0.title = Strings.ActivationDetails.share.localized.uppercased()
            $0.trackingId = AnalyticEventClickableElementName.ActivationDetails.share.rawValue
            $0.setImage(Icons.ActivationDetails.share, for: .normal)
        }
        controller.viewOnMapButton.apply {
            $0.setTitle(Strings.ActivationDetails.viewOnMap.localized.uppercased(), for: .normal)
            $0.trackingId = AnalyticEventClickableElementName.ActivationDetails.viewOnMap.rawValue
        }
        controller.descriptionTextView.linkClickTrackingId = AnalyticEventClickableElementName.ActivationDetails.link.rawValue
        controller.notifyMeBottomBarView.notifyMeButton.trackingId = AnalyticEventClickableElementName.ActivationDetails.notifyMe.rawValue
    }

    private func setupHierarchy() {
        controller.apply {
            [$0.mainScrollView,
             $0.guestsNumberView,
             $0.notifyMeBottomBarView,
             $0.editableRsvpFooter,
             $0.guestPickerView,
             $0.stayTunedView].addTo(parent: view)
            
            $0.mainScrollView.addSubview($0.scrollContentView)
            $0.scrollContentView.addSubviews([
                $0.activationImageView,
                $0.dateInfoLabel,
                $0.titleLabel,
                $0.descriptionTextView,
                $0.friendsList,
                $0.hostsStack,
                $0.timeInfo,
                $0.infoSeperator,
                $0.entryInfo
            ])
            $0.scrollContentView.addSubviews([
                $0.buttonsStack,
                $0.locationSeperator,
                $0.locationInfo,
                $0.locationLabel,
                $0.addressScroll,
                $0.mapView,
                $0.bottomEdgeMapView,
                $0.viewOnMapButton
            ])
            
            $0.buttonsStack.addArrangedSubviews([$0.shareButton, $0.actionButton])
            $0.activationImageView.addSubview($0.topGradient)
            $0.addressScroll.addSubview($0.addressLabel)
        }
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.mainScrollView.edgesToParent(anchors: [.leading, .trailing])
            $0.mainScrollView.bottomAnchor.equal(to: $0.guestsNumberView.rsvpButton.topAnchor)
            
            $0.guestsNumberView.edgesToParent(anchors: [.trailing, .leading, .bottom])
            $0.guestsNumberView.heightAnchor.equalTo(constant: UIDevice.current.hasNotch ? 82 : 72).withPriority(.defaultHigh)
            $0.guestsNumberView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 1.0 / 10.0).activate()
            
            $0.notifyMeBottomBarView.edges(equalTo: $0.guestsNumberView)
            $0.editableRsvpFooter.edges(equalTo: $0.guestsNumberView)
            
            $0.guestPickerView.edgesToParent(anchors: [.leading, .trailing, .bottom])
            $0.guestPickerView.heightAnchor.equalTo(constant: 387)
            
            $0.scrollContentView.edgesToParent()
            $0.scrollContentView.widthAnchor.equal(to: view.widthAnchor)
            
            $0.activationImageView.edgesToParent(anchors: [.leading, .trailing, .top])
            $0.activationImageView.heightAnchor.constraint(equalTo: $0.activationImageView.widthAnchor, multiplier: Constants.ActivationDetails.imageRatio).activate()
            
            $0.topGradient.edgesToParent(anchors: [.leading, .trailing, .bottom], insets: .margins(bottom: -1))
            $0.topGradient.heightAnchor.constraint(equalTo: $0.topGradient.widthAnchor, multiplier: 0.5).activate()
            
            $0.dateInfoLabel.edgesToParent(anchors: [.leading, .trailing], padding: Constants.horizontalMargin)
            $0.dateInfoLabel.topAnchor.equal(to: $0.activationImageView.bottomAnchor, constant: Constants.ActivationDetails.topDateMargin)
            
            $0.titleLabel.withDefaultHorizontalMargin()
            $0.titleLabel.topAnchor.equal(to: $0.dateInfoLabel.bottomAnchor, constant: Constants.ActivationDetails.topTitleMargin)
            
            $0.descriptionTextView.withDefaultHorizontalMargin()
            $0.descriptionTextView.topAnchor.equal(to: $0.titleLabel.bottomAnchor, constant: Constants.ActivationDetails.topDescriptionMargin)
            $0.descriptionTextView.heightAnchor.constraint(greaterThanOrEqualToConstant: 10).activate()

            $0.friendsList.edgesToParent(anchors: [.leading, .trailing], padding: Constants.horizontalMargin)
            $0.friendsList.topAnchor.equal(to: $0.descriptionTextView.bottomAnchor, constant: Constants.ActivationDetails.topDescriptionMargin)
            $0.friendsList.heightAnchor.equalTo(constant: Constants.ActivationDetails.friendsListHeight)
            
            $0.hostsStack.withDefaultHorizontalMargin()
            $0.hostsStack.topAnchor.equal(to: $0.friendsList.bottomAnchor, constant: Constants.ActivationDetails.hostsStackTop)
            
            $0.timeInfo.leadingAnchor.equal(to: view.leadingAnchor, constant: Constants.horizontalMargin)
            $0.timeInfo.topAnchor.equal(to: $0.hostsStack.bottomAnchor, constant: Constants.ActivationDetails.infoTopMargin)
            
            $0.infoSeperator.centerXAnchor.equal(to: view.centerXAnchor)
            $0.infoSeperator.topAnchor.equal(to: $0.timeInfo.topAnchor)
            $0.infoSeperator.bottomAnchor.equal(to: $0.timeInfo.bottomAnchor)
            $0.infoSeperator.widthAnchor.equalTo(constant: 1)
            
            $0.entryInfo.leadingAnchor.equal(to: $0.infoSeperator.trailingAnchor, constant: Constants.horizontalMargin)
            $0.entryInfo.centerYAnchor.equal(to: $0.timeInfo.centerYAnchor)
            
            $0.buttonsStack.withDefaultHorizontalMargin()
            $0.buttonsStack.topAnchor.equal(to: $0.timeInfo.bottomAnchor, constant: Constants.ActivationDetails.buttonsStackTopMargin)
            
            $0.shareButton.heightAnchor.equalTo(constant: Constants.ActivationDetails.shareButtonHeight)
            $0.actionButton.heightAnchor.equalTo(constant: Constants.ActivationDetails.actionButtonHeight)
            
            $0.locationSeperator.withDefaultHorizontalMargin()
            $0.locationSeperator.heightAnchor.equalTo(constant: 1)
            $0.locationSeperator.topAnchor.equal(to: $0.buttonsStack.bottomAnchor, constant: Constants.ActivationDetails.locationSeparatorTopMargin)
            
            $0.locationInfo.leadingAnchor.equal(to: view.leadingAnchor, constant: Constants.horizontalMargin)
            $0.locationInfo.topAnchor.equal(to: $0.locationSeperator.bottomAnchor, constant: Constants.ActivationDetails.locationInfoTopMargin)
            
            $0.locationLabel.leadingAnchor.equal(to: view.leadingAnchor, constant: 49)
            $0.locationLabel.trailingAnchor.equal(to: view.trailingAnchor, constant: -Constants.horizontalMargin)
            $0.locationLabel.topAnchor.equal(to: $0.locationInfo.bottomAnchor, constant: Constants.ActivationDetails.locationLabelTop)
            
            $0.addressScroll.topAnchor.equal(to: $0.locationLabel.bottomAnchor)
            $0.addressScroll.leadingAnchor.equal(to: $0.locationLabel.leadingAnchor)
            $0.addressScroll.trailingAnchor.equal(to: view.trailingAnchor, constant: -Constants.horizontalMargin)
            
            $0.addressLabel.edgesToParent()
            $0.addressLabel.heightAnchor.equal(to: $0.addressScroll.heightAnchor)
            
            $0.mapView.edgesToParent(anchors: [.leading, .trailing])
            $0.mapView.topAnchor.equal(to: $0.addressScroll.bottomAnchor, constant: Constants.ActivationDetails.mapMargin)
            $0.mapView.widthAnchor.constraint(equalTo: $0.mapView.heightAnchor, multiplier: Constants.ActivationDetails.mapRatio).activate()
            
            $0.bottomEdgeMapView.heightAnchor.equalTo(constant: 2)
            $0.bottomEdgeMapView.leadingAnchor.equal(to: $0.mapView.leadingAnchor)
            $0.bottomEdgeMapView.trailingAnchor.equal(to: $0.mapView.trailingAnchor)
            $0.bottomEdgeMapView.bottomAnchor.equal(to: $0.mapView.bottomAnchor)
            
            $0.viewOnMapButton.withDefaultHorizontalMargin()
            $0.viewOnMapButton.heightAnchor.equalTo(constant: Constants.ActivationDetails.viewOnMapHeight)
            $0.viewOnMapButton.topAnchor.equal(to: $0.mapView.bottomAnchor, constant: Constants.ActivationDetails.mapMargin)
            
            $0.viewOnMapButton.edgesToParent(anchors: [.bottom], padding: Constants.ActivationDetails.mapMargin)
            
            $0.stayTunedView.edges(equalTo: $0.guestsNumberView)
        }
    }

}

// MARK: - Public build methods
extension ActivationDetailsViewBuilder {
    
    func buildMainScrollView() -> UIScrollView {
        return UIScrollView().manualLayoutable()
    }
    
    func buildScrollContentView() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildGradientView() -> GradientView {
        return GradientView().manualLayoutable()
    }
    
    func buildActivationImageView() -> UIImageView {
        return UIImageView().manualLayoutable().apply {
            $0.contentMode = .scaleAspectFill
            $0.clipsToBounds = true
        }
    }
    
    func buildDateInfoLabel() -> UILabel {
        return UILabel.styled(textColor: UIColor.textVeryLight, withFont: UIFont.custom(ofSize: Constants.FontSize.small, font: UIFont.Roboto.regular), alignment: .left, numberOfLines: 1).manualLayoutable()
    }
    
    func buildNameLabel() -> UILabel {
        return UILabel.styled(textColor: .white, withFont: UIFont.custom(ofSize: 34, font: UIFont.Editor.medium), alignment: .left, numberOfLines: 0).manualLayoutable()
    }
    
    func buildFriendList() -> FriendsListView {
        return FriendsListView().manualLayoutable()
    }
    
    func buildDescriptionTextView() -> ParlorTextView {
        return ParlorTextView().manualLayoutable().apply {
            $0.font = UIFont.custom(ofSize: Constants.FontSize.body, font: UIFont.Roboto.light)
            $0.textColor = .textVeryLight
            $0.tintColor = .textVeryLight
            $0.lineHeight = Constants.ActivationDetails.descriptionLineHeight
        }
    }
    
    func buildHostsStack() -> HostsStack {
        return HostsStack().manualLayoutable()
    }
    
    func buildMapView() -> GMSMapView {
        return GMSMapView().manualLayoutable().apply { mapView in
            mapView.isUserInteractionEnabled = false
            mapView.mapStyle = try? GMSMapStyle(contentsOfFileURL: Config.mapStyle)
        }
    }
    
    func buildBottomEdgeMapView() -> UIView {
        return UIView().manualLayoutable().apply {
            $0.backgroundColor = .darkBackground
        }
    }
    
    func buildTimeInfo() -> IconedLabeledInfo {
        return IconedLabeledInfo(withIcon: Icons.ActivationDetails.time, title: Strings.ActivationDetails.time.localized.uppercased()).manualLayoutable()
    }
    
    func buildInfoSeperator() -> UIView {
        return UIView().manualLayoutable().apply { $0.backgroundColor = UIColor.rsvpBorder }
    }
    
    func buildEntryInfo() -> IconedLabeledInfo {
        return IconedLabeledInfo(withIcon: Icons.ActivationDetails.entry, title: Strings.ActivationDetails.entry.localized.uppercased()).manualLayoutable()
    }
    
    func buildButtonsStack() -> UIStackView {
        return UIStackView(
            axis: .vertical,
            with: [],
            spacing: Constants.ActivationDetails.buttonsStackSpacing,
            alignment: .fill,
            distribution: .fill
        )
    }
    
    func buildShareButton() -> ParlorButton {
        let shareButtonStyle = ParlorButtonStyle.filledWith(
            color: .appYellow,
            titleColor: .appText,
            font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium),
            letterSpacing: Constants.LetterSpacing.none
        )
        
        return ParlorButton(style: shareButtonStyle).manualLayoutable().apply {
            $0.layer.cornerRadius = Constants.ActivationDetails.shareButtonHeight / 2
            $0.imageEdgeInsets = Constants.ActivationDetails.shareButtonImageInsets
            $0.semanticContentAttribute = .forceRightToLeft
            $0.applyTouchAnimation()
        }
    }
    
    func buildActionButton() -> ParlorButton {
        return ParlorButton().manualLayoutable().apply {
            $0.layer.cornerRadius = Constants.ActivationDetails.actionButtonHeight / 2
            $0.imageEdgeInsets = Constants.ActivationDetails.actionButtonImageInsets
            $0.semanticContentAttribute = .forceRightToLeft
            $0.applyTouchAnimation()
        }
    }
    
    func buildLocationSeperator() -> UIView {
        return UIView().manualLayoutable().apply { $0.backgroundColor = UIColor.rsvpBorder }
    }
    
    func buildLocationInfo() -> IconedLabeledInfo {
        return IconedLabeledInfo(withIcon: Icons.ActivationDetails.location, title: Strings.ActivationDetails.location.localized.uppercased()).manualLayoutable()
    }
    
    func buildLocationLabel() -> ParlorLabel {
        return ParlorLabel.styled(
            withTextColor: UIColor.white,
            withFont: UIFont.custom(ofSize: Constants.FontSize.hintSize, font: UIFont.Roboto.regular),
            alignment: .left,
            numberOfLines: 0)
            .apply {
                $0.lineHeight = 25
        }
    }
    
    func buildAddressLabel() -> ParlorLabel {
        return ParlorLabel.styled(
            withTextColor: UIColor.white,
            withFont: UIFont.custom(ofSize: Constants.FontSize.hintSize, font: UIFont.Roboto.regular),
            alignment: .left,
            numberOfLines: 0)
            .apply {
                $0.lineHeight = 25
        }
    }
    
    func buildAddressScroll() -> UIScrollView {
        return UIScrollView().manualLayoutable().apply { $0.showsHorizontalScrollIndicator = false }
    }
    
    func buildViewOnMapButton() -> RoundedButton {
        return RoundedButton().manualLayoutable().apply {
            $0.layer.borderColor = UIColor.rsvpBorder.cgColor
            $0.layer.borderWidth = 1
            $0.applyTouchAnimation()
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium)
        }
    }
    
    func buildGuestNumberView() -> GuestNumberView {
        return GuestNumberView().manualLayoutable()
    }
    
    func buildNotifyMeBottomBarView() -> NotifyMeBottomBarView {
        return NotifyMeBottomBarView().manualLayoutable()
    }
    
    func buildGuestPickerView() -> GuestPickerView {
        return GuestPickerView().manualLayoutable()
    }
    
    func buildEditableFooter() -> EditableActivationFooter {
        return EditableActivationFooter().manualLayoutable()
    }
    
    func buildStayTunedView() -> WaitingForRsvpAvailabilityView {
        return WaitingForRsvpAvailabilityView().manualLayoutable()
    }
}

extension Constants {
    
    enum ActivationDetails {
        static let imageRatio: CGFloat = 280.0 / 375.0
        static let topDateMargin: CGFloat = 30
        static let topTitleMargin: CGFloat = 15
        static let friendsListHeight: CGFloat = 30
        static let topDescriptionMargin: CGFloat = 26
        static let hostsStackTop: CGFloat = 18
        static let infoTopMargin: CGFloat = 45
        static let locationSeperatorTopMargin: CGFloat = 45
        static let buttonsStackTopMargin: CGFloat = 45
        static let buttonsStackSpacing: CGFloat = 30
        static let shareButtonHeight: CGFloat = 55
        static let actionButtonHeight: CGFloat = 55
        static let locationSeparatorTopMargin: CGFloat = 45
        static let locationInfoTopMargin: CGFloat = 31
        static let locationLabelTop: CGFloat = 10
        static let mapMargin: CGFloat = 60
        static let mapRatio: CGFloat = 375.0 / 210.0
        static let viewOnMapHeight: CGFloat = 55
        static let descriptionLineHeight: CGFloat = 24
        static let shareButtonImageInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: -15)
        static let actionButtonImageInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: -20)
    }
    
}

fileprivate extension UIView {
    
    func withDefaultHorizontalMargin() {
        self.edgesToParent(anchors: [.leading, .trailing], padding: Constants.horizontalMargin)
    }
    
}
