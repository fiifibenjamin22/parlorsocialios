//
//  OpeningActivationDetailsType.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 27/11/2019.
//  Copyright © 2019 KISSdigital. All rights reserved.
//

import Foundation

enum OpeningActivationDetailsType {
    case upgradeToPremiumForStandard
    case startedAlert
    case addProfilePhotoVC
    case activationDetailsVC
}
