//  ActivationDetailsNavigationSource.swift
//  ParlorSocialClub
//
//  Created on 27/04/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

enum ActivationDetailsNavigationSource: String {
    case pushNotification = "push_notification"
    case deeplink = "deep_link"
    case activationsAll = "activations_all"
    case activationsMixers = "activations_mixers"
    case activationsHappenings = "activations_happenings"
    case others = "others"
    case parlorPassDetails = "parlor_pass_details"
    case rsvps = "rsvps"
}
