//
//  ActivationDetailsNavigationProvider.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 22/10/2019.
//  Copyright © 2019 KISSdigital. All rights reserved.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift

typealias ActivationRoutingData = ActivationDetailsNavigationProvider.RoutingData

class ActivationDetailsNavigationProvider {

    // MARK: - Models
    struct RoutingData {
        let activationId: Int?
        let activation: Activation?
        let navigationSource: ActivationDetailsNavigationSource
    }
    
    // MARK: - Properties
    private static var shouldShowAddProfilePhoto: Bool {
        return Config.userProfile?.imageURL == nil && Config.userProfile?.type != .preview
    }

    // MARK: - Private methods
    private static func shouldShowUpgradeToPremiumForStandard(activation: HomeActivation) -> Bool {
        return activation.isPremium && (Config.userProfile?.isStandard ?? false)
    }
    
    private static func shouldShowStartedAlert(activation: HomeActivation) -> Bool {
        return !activation.startAt.isFuture && !activation.isAllowedToRsvpOngoing
    }

    private static func shouldShowUpgradeToPremiumForStandard(activation: Activation) -> Bool {
        return activation.isPremium && (Config.userProfile?.isStandard ?? false)
    }

    private static func shouldShowStartedAlert(activation: Activation) -> Bool {
        return !activation.startAt.isFuture && !activation.isAllowedToRsvpOngoing
    }

    // 1. When user is standard and activation is premium open premium alert
    // 2. When activation isn't future and isn't allowed to rsvp ongoing show started alert
    // 3. When user doesn't have added avatar and isn't preview open AddProfilePhoto view
    // 4. If nothing from above points -> show activation details
    
    /// Use this function in view model when you want to show alerts or any view controller if activation details aren't available.
    static func getOpeningActivationDetailsType(with activation: Activation) -> OpeningActivationDetailsType {
        if shouldShowUpgradeToPremiumForStandard(activation: activation) {
            return .upgradeToPremiumForStandard
        } else if shouldShowStartedAlert(activation: activation) {
            return .startedAlert
        } else if shouldShowAddProfilePhoto {
            return .addProfilePhotoVC
        } else {
            return .activationDetailsVC
        }
    }

    /// Use this function in view model when you want to show alerts or any view controller if activation details aren't available.
    static func getOpeningActivationDetailsType(with activation: HomeActivation) -> OpeningActivationDetailsType {
        if shouldShowUpgradeToPremiumForStandard(activation: activation) {
            return .upgradeToPremiumForStandard
        } else if shouldShowStartedAlert(activation: activation) {
            return .startedAlert
        } else if shouldShowAddProfilePhoto {
            return .addProfilePhotoVC
        } else {
            return .activationDetailsVC
        }
    }

    static func getOpeningActivationDetailsType() -> OpeningActivationDetailsType {
        if shouldShowAddProfilePhoto {
            return .addProfilePhotoVC
        } else {
            return .activationDetailsVC
        }
    }
    
    /// Use this function in destiation class when you want to open activation details.
    /// This method checks if activation details are available, if not it will open another view controller.
    /// The `openingType` should be received from `getOpeningActivationDetailsType` method.
    static func getVCByAvailabilityOfOpeningActivationDetails(
        forActivationId id: Int,
        openingType: OpeningActivationDetailsType,
        navigationSource: ActivationDetailsNavigationSource
    ) -> UIViewController {
        switch openingType {
        case .upgradeToPremiumForStandard:
            return UpgradeMembershipViewController(navigationSource: .activation(id))
        case .addProfilePhotoVC:
            let routingData = RoutingData(activationId: id, activation: nil, navigationSource: navigationSource)
            return AddProfilePhotoViewController(forActivationData: routingData)
        case .activationDetailsVC:
            return ActivationDetailsViewController(forActivationId: id, navigationSource: navigationSource)
        default:
            fatalError("This type of opening activation details should not be used for opening view contoller")
        }
    }
    
    /// Use this function in destiation class when you want to open activation details.
    /// This method checks if activation details are available, if not it will open another view controller.
    /// The `openingType` should be received from `getOpeningActivationDetailsType` method.
    static func getVCByAvailabilityOfOpeningActivationDetails(
        forActivation activation: Activation, openingType: OpeningActivationDetailsType, navigationSource: ActivationDetailsNavigationSource) -> UIViewController {
        switch openingType {
        case .upgradeToPremiumForStandard:
            return UpgradeMembershipViewController(navigationSource: .activation(activation.id))
        case .addProfilePhotoVC:
            let routingData = RoutingData(activationId: nil, activation: activation, navigationSource: navigationSource)
            return AddProfilePhotoViewController(forActivationData: routingData)
        case .activationDetailsVC:
            return ActivationDetailsViewController(activation: activation, navigationSource: navigationSource)
        default:
            fatalError("This type of opening activation details should not be used for opening view contoller")
        }
    }
    
    static func getActivationDetailsVC(for routingData: ActivationRoutingData) -> ActivationDetailsViewController {
        if let activation = routingData.activation {
            return ActivationDetailsViewController(activation: activation, navigationSource: routingData.navigationSource)
        } else {
            guard let activationId = routingData.activationId else {
                    fatalError("Navigation data to ActivationDetails is not proper")
            }
            return ActivationDetailsViewController(forActivationId: activationId, navigationSource: routingData.navigationSource)
        }
    }
}
