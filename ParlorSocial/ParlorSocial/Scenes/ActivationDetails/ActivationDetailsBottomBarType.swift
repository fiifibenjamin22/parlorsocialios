//
//  ActivationDetailsBottomBarType.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 29/11/2019.
//  Copyright © 2019 KISSdigital. All rights reserved.
//

enum ActivationDetailsBottomBarType {
    case editRsvp
    case rsvpIsClosed
    case notifyMe
    case stayTuned
    case canRsvpWithGuests
    case canRsvpWithoutGuests
}
