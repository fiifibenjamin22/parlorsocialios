//
//  ActivationDetailsDestination.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 15/04/2019.
//

import UIKit

enum ActivationDetailsDestination {
    case guestListForm(activation: Activation, guestsCount: Int?, guestSlotsChecked: Bool)
    case guestList(activationId: Int)
    case hostDetails(host: ParlorHost)
    case waitList(activation: Activation)
    case calendarSync(activation: Activation, settingsChangeSource: CalendarSyncChangedEventSource)
    case mapProvider
    case membershipPlans
    case activationDetailsInfo(activationId: Int)
    case share(shareData: ShareData, delegate: ShareActivationDelegate)
    case moreShareOptions(shareData: ShareData, completion: UIActivityViewController.CompletionWithItemsHandler)
    case upgradeToPremium(navigationSource: UpgradeToPremiumNavigationSource)
    case contactsAttending
}

extension ActivationDetailsDestination: Destination {

    var viewController: UIViewController {
        switch self {
        case .contactsAttending:
            return ConnectionsAttendingVC().embedInNavigationController()
        case .guestListForm(let activation, let guestsCount, let guestSlotsChecked):
            return GuestListFormViewController(forActivation: activation, andGuestsCount: guestsCount, and: guestSlotsChecked)
                .embedInNavigationController()
        case .guestList(let activationId):
            return GuestListRsvpViewController(activationId: activationId)
        case .hostDetails(let host):
            return HostDetailsViewController(withHost: host)
        case .waitList(let activation):
            return WaitlistInfoViewController(withActivation: activation).embedInNavigationController()
        case .mapProvider:
            return MapProviderViewController()
        case .calendarSync(let activation, let settingsChangeSource):
            return CalendarSyncViewController(activationToSync: activation, settingsChangeSource: settingsChangeSource).embedInNavigationController()
        case .membershipPlans:
            return MembershipPlansViewController()
        case .activationDetailsInfo(let activationId):
            return ActivationDetailsInfoViewController(activationId: activationId)
        case .share(let shareData, let delegate):
            let viewModel = ShareActivationViewModel(shareData: shareData, pasteboard: UIPasteboard.general)
            let viewController = ShareActivationViewController(viewModel: viewModel, delegate: delegate)
            viewController.title = Strings.ShareActivation.titleEvent.localized
            return viewController
        case .moreShareOptions(let shareData, let completion):
            let controller = UIActivityViewController(
                activityItems: [shareData.url, shareData.message],
                applicationActivities: nil
            )

            UIButton.appearance().tintColor = .black
            controller.completionWithItemsHandler = completion

            return controller
        case .upgradeToPremium(let navigationSource):
            return UpgradeMembershipViewController(navigationSource: navigationSource)
        }
    }

}
