//
//  MembershipPlanPageViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 15/11/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol MembershipPlanPageLogic: BaseViewModelLogic {
    var membershipPlanDataSubject: BehaviorSubject<[PlanDataDescription]> { get }
    func fillAllPointsOfDescription(planData: PlanData)
}

class MembershipPlanPageViewModel: BaseViewModel {
    var analyticEventReferencedId: Int?
    let membershipPlanDataSubject: BehaviorSubject<[PlanDataDescription]> = .init(value: [])
}

// MARK: Private logic
private extension MembershipPlanPageViewModel {
    
}

// MARK: Interface logic methods
extension MembershipPlanPageViewModel: MembershipPlanPageLogic {
    func fillAllPointsOfDescription(planData: PlanData) {
        let priceString = String(
            format: Strings.Premium.priceInterval.localized,
            planData.price?.getCurrencyStringWithFreeOption(currency: planData.currency) ?? Strings.MembershipPlans.free.localized,
            planData.interval ?? ""
        )
        let firstPointOfDescription = PlanDataDescription(element: priceString)
        membershipPlanDataSubject.onNext([firstPointOfDescription] + (planData.description ?? []))
    }
}
