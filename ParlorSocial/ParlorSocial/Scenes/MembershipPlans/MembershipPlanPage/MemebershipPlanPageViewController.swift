//
//  MembershipPlanPageViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 15/11/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

class MembershipPlanPageViewController: AppViewController {
    
    // MARK: - Views
    private(set) var scrollView: UIScrollView!
    private(set) var contentScrollView: UIView!
    private(set) var descriptionPlanStackView: UIStackView!
    
    // MARK: - Properties
    private var viewModel: MembershipPlanPageLogic!
    var analyticEventScreen: AnalyticEventScreen?
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }
    
    // MARK: - Initialization
    init() {
        super.init(nibName: nil, bundle: nil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let className = String(describing: self)
        fatalError("Error with initialize \(className)")
    }

    private func setup() {
        self.viewModel = MembershipPlanPageViewModel()
    }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: (.now() + 0.1)) { [unowned self] in
            self.scrollView.flashScrollIndicators()
        }
    }
    
    override func observeViewModel() {
        super.observeViewModel()
        
        viewModel.membershipPlanDataSubject
            .subscribe(onNext: { [unowned self] descriptionPoints in
                self.setup(with: descriptionPoints)
            }).disposed(by: disposeBag)
    }
    
    func showPlanData(planData: PlanData) {
        viewModel.fillAllPointsOfDescription(planData: planData)
    }

}

// MARK: - Private methods
extension MembershipPlanPageViewController {

    private func setupViews() {
        let builder = MembershipPlanPageViewBuilder(controller: self)
        scrollView = builder.buildScrollView()
        contentScrollView = builder.buildView()
        descriptionPlanStackView = builder.buildDescriptionPlanStackView()

        builder.setupViews()
    }

    private func setupPresentationLogic() {

    }
    
    private func setup(with descriptionPoints: [PlanDataDescription]) {
        descriptionPoints.forEach {
            descriptionPlanStackView.addArrangedSubview(DescriptionStackViewItem(description: $0.element))
        }
        DispatchQueue.main.asyncAfter(deadline: (.now() + 0.1)) { [unowned self] in
            self.scrollView.flashScrollIndicators()
        }
    }

}
