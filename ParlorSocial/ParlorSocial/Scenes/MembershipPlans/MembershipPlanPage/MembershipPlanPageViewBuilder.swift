//
//  MembershipPlanPageViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 15/11/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class MembershipPlanPageViewBuilder {

    private unowned let controller: MembershipPlanPageViewController
    private var view: UIView! { return controller.view }

    init(controller: MembershipPlanPageViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension MembershipPlanPageViewBuilder {

    private func setupProperties() {
        controller.apply {
            $0.scrollView.backgroundColor = .appTextWhite
            $0.contentScrollView.backgroundColor = .clear
        }
    }

    private func setupHierarchy() {
        view.addSubview(controller.scrollView)
        controller.apply {
            $0.scrollView.addSubview($0.contentScrollView)
            $0.contentScrollView.addSubview($0.descriptionPlanStackView)
        }
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.scrollView.edges(equalTo: view)
            
            $0.contentScrollView.edges(equalTo: $0.scrollView)
            $0.contentScrollView.widthAnchor.equal(to: $0.scrollView.widthAnchor)
            
            $0.descriptionPlanStackView.edgesToParent(
                anchors: [.top, .leading, .trailing],
                insets: .margins(
                    top: 20,
                    left: Constants.MembershipPlanPageView.descriptionHorizontalMargin,
                    right: Constants.MembershipPlanPageView.descriptionHorizontalMargin
                )
            )
            $0.descriptionPlanStackView.bottomAnchor.equal(to: $0.contentScrollView.bottomAnchor, constant: -30)
        }
    }

}

// MARK: - Public build methods
extension MembershipPlanPageViewBuilder {

    func buildView() -> UIView {
        return UIView()
    }
    
    func buildScrollView() -> UIScrollView {
        return UIScrollView().manualLayoutable()
    }
    
    func buildDescriptionPlanStackView() -> UIStackView {
        return UIStackView(
            axis: .vertical,
            spacing: 0,
            alignment: .fill,
            distribution: .fill
            ).manualLayoutable()
    }

}

// MARK: - Constants
fileprivate extension Constants {
    enum MembershipPlanPageView {
        static let descriptionHorizontalMargin: CGFloat = 45
    }
}
