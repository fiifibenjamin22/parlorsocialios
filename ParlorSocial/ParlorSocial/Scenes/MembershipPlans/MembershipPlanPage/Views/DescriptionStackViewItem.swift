//
//  DescriptionStackViewItem.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 18/11/2019.
//  Copyright © 2019 KISSdigital. All rights reserved.
//

import UIKit

final class DescriptionStackViewItem: UIStackView {
    private(set) lazy var dotView: UIView = {
        return UIView().manualLayoutable().apply {
            $0.backgroundColor = .clear
            
            let dot = UIView().manualLayoutable().apply {
                $0.backgroundColor = .appTextMediumBright
            }
            $0.addSubview(dot)
            dot.apply {
                $0.edgesToParent(
                    anchors: [.top, .leading, .trailing],
                    insets: .margins(top: Constants.DescriptionStackViewItem.descriptionLineHeight - 12)
                )
                $0.heightAnchor.equalTo(constant: Constants.DescriptionStackViewItem.dotHeightWidth)
                $0.widthAnchor.equal(to: $0.heightAnchor)
                $0.roundCorners(with: Constants.DescriptionStackViewItem.dotHeightWidth / 2)
            }
        }
    }()
    
    private(set) lazy var descriptionLabel: ParlorLabel = {
        return ParlorLabel.styled(
            withTextColor: .appTextMediumBright,
            withFont: .custom(ofSize: Constants.FontSize.body, font: UIFont.Roboto.light),
            alignment: .left,
            numberOfLines: 0).apply {
                $0.lineHeight = Constants.DescriptionStackViewItem.descriptionLineHeight
        }
    }()
    
    init(description: String) {
        super.init(frame: .zero)
        initialize(description: description)
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize(description: String) {
        layoutViews()
        descriptionLabel.text = description
    }
    
    private func layoutViews() {
        setupStackViewProperties()
        addArrangedSubviews([dotView, descriptionLabel])
    }
    
    private func setupStackViewProperties() {
        axis = .horizontal
        spacing = 20
        alignment = .top
        distribution = .fill
    }
}

// MARK: - Constants
fileprivate extension Constants {
    enum DescriptionStackViewItem {
        static let descriptionLineHeight: CGFloat = 30
        static let dotHeightWidth: CGFloat = 5
    }
}
