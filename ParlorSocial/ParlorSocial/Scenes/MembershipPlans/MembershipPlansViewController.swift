//
//  MembershipPlansViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 14/11/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

class MembershipPlansViewController: AppViewController {
    
    // MARK: - Views
    private(set) var descriptionTopLabel: ParlorLabel!
    private(set) var choosePlanStackView: UIStackView!
    private(set) var standardItem: TopTabBarItemView!
    private(set) var premiumItem: TopTabBarItemView!
    private(set) var separatorLine: UIView!
    private(set) var pageContainer: UIView!
    private(set) var selectButton: UIButton!
    private var planItems: [TopTabBarItemView] = []
    
    private(set) var pageController: UIPageViewController!
    private(set) lazy var pages: [UIViewController] = {
        return [MembershipPlanPageViewController(),
                MembershipPlanPageViewController()]
    }()
    
    // MARK: - Properties
    let analyticEventScreen: AnalyticEventScreen? = .membershipPlans
    private var viewModel: MembershipPlansLogic!

    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }
    
    override var observableSources: ObservableSources? {
        return viewModel
    }
    
    // MARK: - Initialization
    init() {
        super.init(nibName: nil, bundle: nil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() {
        self.viewModel = MembershipPlansViewModel()
        self.pageController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
        addChild(viewController: pageController, inside: pageContainer)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        adjustNavigationBar()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
    }
    
    override func observeViewModel() {
        super.observeViewModel()
        
        viewModel.updateViewAfterSelectedPlanObs
            .subscribe(onNext: { [unowned self] (index, pageNavDirection) in
                self.handleUpdateViewAfterSelectedPlan(index: index, pageNavDirection: pageNavDirection)
            }).disposed(by: disposeBag)
        
        viewModel.membershipPlansDataObs
            .filter { !$0.isEmpty }
            .subscribe(onNext: { [unowned self] plansData in
                self.handleMembershipPlansData(data: plansData)
            }).disposed(by: disposeBag)
        
        selectButton
            .rx
            .controlEvent(.touchUpInside)
            .bind(to: viewModel.selectClicked)
            .disposed(by: disposeBag)
        
        viewModel.destinationObs
            .subscribe(onNext: { [unowned self] destination in
                self.handle(destination: destination)
            }).disposed(by: disposeBag)
    }
}

// MARK: - Private methods
extension MembershipPlansViewController {

    private func setupViews() {
        let builder = MembershipPlansViewBuilder(controller: self)
        descriptionTopLabel = builder.buildDescriptionTopLabel()
        choosePlanStackView = builder.buildChoosePlanStackView()
        standardItem = builder.buildChoosePlanItem()
        premiumItem = builder.buildChoosePlanItem()
        separatorLine = builder.buildView()
        pageContainer = builder.buildView()
        selectButton = builder.buildSelectButton()
        
        builder.setupViews()
        planItems = [standardItem, premiumItem]
    }

    private func setupPresentationLogic() {
        viewModel.fetchMembershipPlans()
        setupPageViewController()
        setupChoosePlanStackView()
    }

    private func adjustNavigationBar() {
        navigationController?.applyProfileItemAppearance(withTitle: Strings.MembershipPlans.navTitle.localized.uppercased())
        navigationController?.setNavigationBarBorderColor()
        statusBar?.backgroundColor = .white
        setNeedsStatusBarAppearanceUpdate()
        guard self.presentingViewController != nil else { return }
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: Icons.NavigationBar.close, style: .plain, target: self, action: #selector(tapBackButton))
    }
    
    private func setupPageViewController() {
        pageController.dataSource = self
        pageController.delegate = self
        viewModel.currentPageIndexRelay.accept(1)
    }
    
    private func setupChoosePlanStackView() {
        planItems.forEach { $0.delegate = self }
    }
    
    private func handleUpdateViewAfterSelectedPlan(
        index: Int, pageNavDirection: UIPageViewController.NavigationDirection) {
        planItems.forEach { $0.isSelected = false }
        planItems[index].isSelected = true
        pageController.setViewControllers(
            [pages[index]],
            direction: pageNavDirection,
            animated: true,
            completion: nil
        )
    }
    
    private func handleMembershipPlansData(data: [PlanData]) {
        guard data.count >= pages.count else { return }
        pages.enumerated().forEach { index, page in
            guard let pageVC = page as? MembershipPlanPageViewController else { return }
            pageVC.showPlanData(planData: data[index])
        }
    }
    
    private func handle(destination: MembershipPlansDestination) {
        switch destination {
        case .payment:
            router.push(destination: destination)
        }
    }
    
    @objc func tapBackButton() {
        parent?.dismiss()
    }

}

// MARK: - UIPageViewController data source
extension MembershipPlansViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pages.firstIndex(of: viewController) else { return nil }
        let previousIndex = viewControllerIndex - 1
        guard previousIndex >= 0, pages.count > previousIndex else { return nil }
        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pages.firstIndex(of: viewController) else { return nil }
        let nextIndex = viewControllerIndex + 1
        guard nextIndex < pages.count, pages.count > nextIndex else { return nil }
        return pages[nextIndex]
    }
}

// MARK: - UIPageViewController delegate
extension MembershipPlansViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard let currenViewController = pageViewController.viewControllers?.first,
            let currentPageIndex = pages.firstIndex(of: currenViewController) else { return }
        viewModel.currentPageIndexRelay.accept(currentPageIndex)
    }
}

// MARK: - TopTabBarItemDelegate
extension MembershipPlansViewController: TopTabBarItemViewDelegate {
    func topTabBarItemView(_ topTabBarItemView: TopTabBarItemView, selected: Bool) {
        planItems.filter { $0 != topTabBarItemView }.forEach { $0.isSelected = false }
        guard let currentPageIndex = planItems.firstIndex(of: topTabBarItemView) else { return }
        viewModel.currentPageIndexRelay.accept(currentPageIndex)
    }
}

