//
//  MembershipPlansViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 14/11/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import UIKit.UIPageViewController

protocol MembershipPlansLogic: BaseViewModelLogic {
    var currentPageIndexRelay: BehaviorRelay<Int> { get }
    var updateViewAfterSelectedPlanObs: Observable<(Int, UIPageViewController.NavigationDirection)> { get }
    var membershipPlansDataObs: Observable<[PlanData]> { get }
    var selectClicked: PublishSubject<Void> { get }
    var destinationObs: Observable<MembershipPlansDestination> { get }
    func fetchMembershipPlans()
}

class MembershipPlansViewModel: BaseViewModel {
    
    // MARK: - Properties
    var analyticEventReferencedId: Int?
    
    let currentPageIndexRelay: BehaviorRelay<Int> = .init(value: 0)
    let membershipPlansDataRelay: BehaviorRelay<[PlanData]> = .init(value: [])
    let selectClicked: PublishSubject<Void> = .init()
    let destinationSubject: PublishSubject<MembershipPlansDestination> = .init()
    
    private let profileRepository = ProfileRepository.shared
    private let facebookAnalyticsService = FacebookAnalyticService()
    private let firebaseAnalyticsService = FirebaseAnalyticService()
    private let updateViewAfterSelectedPlanRelay: PublishRelay<(Int, UIPageViewController.NavigationDirection)> = .init()
    
    // MARK: - Initialization
    override init() {
        super.init()
        initFlow()
        facebookAnalyticsService.logSelectMembershipScreenOpened()
    }
}

// MARK: Private logic
private extension MembershipPlansViewModel {
    private func initFlow() {
        currentPageIndexRelay
            .distinctUntilChanged()
            .pairwise()
            .subscribe(onNext: { [unowned self] (previousIndex, currentIndex) in
                self.handleCurrentPage(previousIndex: previousIndex, currentIndex: currentIndex)
            }).disposed(by: disposeBag)
        
        selectClicked
            .withLatestFrom(membershipPlansDataRelay)
            .filter { !$0.isEmpty }
            .withLatestFrom(currentPageIndexRelay) { ($0, $1) }
            .subscribe(onNext: { [unowned self] (planData, pageIndex) in
                self.logMembershipSelectionAnalyticEvents(usingPlan: planData[pageIndex])
                self.destinationSubject.onNext(.payment(planData: planData[pageIndex]))
            }).disposed(by: disposeBag)
    }
    
    private func handleCurrentPage(previousIndex: Int, currentIndex: Int) {
        if previousIndex <= currentIndex {
            updateViewAfterSelectedPlanRelay.accept((currentIndex, .forward))
        } else {
            updateViewAfterSelectedPlanRelay.accept((currentIndex, .reverse))
        }
    }
    
    private func handleMembershipPlans(_ response: MembershipPlansResponse) {
        switch response {
        case .success(let responseData):
            let membershipPlansData = [
                responseData.data.first { $0.isPremium == false },
                responseData.data.first { $0.isPremium == true }
                ].compactMap { $0 }

            membershipPlansDataRelay.accept(membershipPlansData)
        case .failure(let error):
            errorSubject.onNext(error)
        }
    }
    
    private func logMembershipSelectionAnalyticEvents(usingPlan plan: PlanData) {
        facebookAnalyticsService.logMembershipSelectedEvent(membershipPlan: plan)
        firebaseAnalyticsService.logMembershipSelectedEvent(membershipPlan: plan)
    }
}

// MARK: Interface logic methods
extension MembershipPlansViewModel: MembershipPlansLogic {
    var destinationObs: Observable<MembershipPlansDestination> {
        return destinationSubject.asObservable()
    }
    
    var membershipPlansDataObs: Observable<[PlanData]> {
        return membershipPlansDataRelay.asObservable()
    }
    
    var updateViewAfterSelectedPlanObs: Observable<(Int, UIPageViewController.NavigationDirection)> {
        return updateViewAfterSelectedPlanRelay.asObservable()
    }
    
    func fetchMembershipPlans() {
        let request = MembershipPlans.Get.Request(visible: true)
        profileRepository.getMembershipPlans(using: request).showingProgressBar(with: self)
            .subscribe(onNext: { [unowned self] response in
                self.handleMembershipPlans(response)
            }).disposed(by: disposeBag)
    }
}
