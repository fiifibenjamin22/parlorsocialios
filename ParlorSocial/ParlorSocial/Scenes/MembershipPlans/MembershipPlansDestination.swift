//
//  MembershipPlansDestination.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 18/11/2019.
//  Copyright © 2019 KISSdigital. All rights reserved.
//

import Foundation
import UIKit

enum MembershipPlansDestination {
    case payment(planData: PlanData)
}

extension MembershipPlansDestination: Destination {
    
    var viewController: UIViewController {
        switch self {
        case .payment(let planData):
            return PremiumPaymentViewController(membershipPlan: planData)
        }
    }
    
}
