//
//  MembershipPlansViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 14/11/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class MembershipPlansViewBuilder {

    private unowned let controller: MembershipPlansViewController
    private var view: UIView! { return controller.view }

    init(controller: MembershipPlansViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension MembershipPlansViewBuilder {

    private func setupProperties() {
        view.backgroundColor = .white
        controller.apply {
            $0.descriptionTopLabel.text = Strings.MembershipPlans.somethingAboutItsCool.localized
            $0.selectButton.setTitle(Strings.select.localized.uppercased(), for: .normal)
            $0.standardItem.title = Strings.MembershipPlans.standard.localized
            $0.premiumItem.title = Strings.MembershipPlans.premium.localized
            $0.separatorLine.backgroundColor = .textVeryLight
            $0.extendedLayoutIncludesOpaqueBars = true
        }
    }

    private func setupHierarchy() {
        controller.apply {
            view.addSubviews([$0.descriptionTopLabel, $0.choosePlanStackView, $0.separatorLine, $0.pageContainer, $0.selectButton])
            $0.choosePlanStackView.addArrangedSubviews([$0.standardItem, $0.premiumItem])
        }
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.descriptionTopLabel.edgesToParent(
                anchors: [.leading, .trailing],
                insets: UIEdgeInsets.margins(left: 29, right: 29)
            )
            $0.descriptionTopLabel.safeAreaLayoutGuide.topAnchor.equal(to: view.safeAreaLayoutGuide.topAnchor, constant: 20)
            
            $0.choosePlanStackView.apply {
                $0.widthAnchor.constraint(
                    equalTo: view.widthAnchor,
                    multiplier: Constants.MembershipPlans.topTabBarStackViewMultiplier).activate()
                $0.topAnchor.equal(to: controller.descriptionTopLabel.bottomAnchor, constant: 25)
                $0.centerXAnchor.equal(to: view.centerXAnchor)
                $0.heightAnchor.equalTo(constant: 46)
            }
            
            $0.separatorLine.edgesToParent(anchors: [.leading, .trailing])
            $0.separatorLine.topAnchor.equal(to: $0.choosePlanStackView.bottomAnchor)
            $0.separatorLine.heightAnchor.equalTo(constant: 1)
            
            $0.pageContainer.edgesToParent(anchors: [.leading, .trailing])
            $0.pageContainer.topAnchor.equal(to: $0.separatorLine.bottomAnchor)
            $0.pageContainer.bottomAnchor.equal(to: $0.selectButton.topAnchor, constant: -28)
            
            $0.selectButton.edgesToParent(
                anchors: [.leading, .bottom, .trailing],
                insets: UIEdgeInsets.margins(left: 16, bottom: 28, right: 16)
            )
        }
    }

}

// MARK: - Public build methods
extension MembershipPlansViewBuilder {
    
    func buildDescriptionTopLabel() -> ParlorLabel {
        return ParlorLabel.styled(
            withTextColor: .appText,
            withFont: UIFont.custom(ofSize: Constants.FontSize.body, font: UIFont.Roboto.light),
            alignment: .left,
            numberOfLines: 0).apply {
                $0.lineHeight = 24
        }.manualLayoutable()
    }
    
    func buildChoosePlanStackView() -> UIStackView {
        return UIStackView(
            axis: .horizontal
            ).apply {
                $0.backgroundColor = UIColor.white
                $0.distribution = .fillEqually
        }.manualLayoutable()
    }
    
    func buildChoosePlanItem() -> TopTabBarItemView {
        return TopTabBarItemView(bottomLineStyle: .automatic).manualLayoutable().apply {
            $0.titleLabel.font = UIFont.custom(ofSize: Constants.FontSize.body, font: UIFont.Roboto.regular)
        }
    }
    
    func buildSelectButton() -> RoundedButton {
        return RoundedButton.withBackgroundStyle().manualLayoutable()
    }
    
    func buildView() -> UIView {
        return UIView().manualLayoutable()
    }
    
}

fileprivate extension Constants {
    enum MembershipPlans {
        static let topTabBarStackViewMultiplier: CGFloat = 2/3
    }
}
