//
//  ResetPasswordViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 04/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxSwift

protocol ResetPasswordLogic: PagerLogic {
    var forgottenMailSubject: BehaviorSubject<String> { get }
    var newPasswordRelay: BehaviorSubject<String> { get }
    var resetTokenRelay: BehaviorSubject<String> { get }
}

class ResetPasswordViewModel: ViewPagerPageViewModel<ResetPasswordPage> {
    // MARK: - Private properties
    private let validator = Validator()
    private let emailValidationHelper = ResetPasswordEmailValidationHelper()
    private let passwordRequestValidationHelper = ResetPasswordRequestValidationHelper()
    
    init(withInitialPage initialPage: ResetPasswordPage, email: String, token: String) {
        self.forgottenMailSubject = BehaviorSubject(value: email)
        self.resetTokenRelay = BehaviorSubject(value: token)
        super.init(withInitialPage: initialPage)
        self.initializeFlow()
    }
    
    let profileRepository = ProfileRepository.shared
    
    // MARK: public relays
    let forgottenMailSubject: BehaviorSubject<String>
    let newPasswordRelay: BehaviorSubject<String> = BehaviorSubject(value: "")
    let resetTokenRelay: BehaviorSubject<String>
    
    var resetDataObs: Observable<ResetPasswordRequest> {
        return Observable.combineLatest(forgottenMailSubject, resetTokenRelay, newPasswordRelay) { mail, token, password in
            return ResetPasswordRequest(email: mail, token: token, password: password, passwordConfirmation: password)
        }
    }
    
}

// MARK: Private logic
private extension ResetPasswordViewModel {
    
    private func initializeFlow() {
        continueClicks.withLatestFrom(forgottenMailSubject)
            .onlyIfCurrentPage(is: .typeEmail, pageStream: self.showingPageRelay)
            .subscribe(onNext: { [unowned self] email in
                self.validate(email: email)
            }).disposed(by: disposeBag)
        
        continueClicks.withLatestFrom(forgottenMailSubject)
            .onlyIfCurrentPage(is: .checkInbox, pageStream: self.showingPageRelay)
            .subscribe(onNext: { [weak self] _ in
                self?.destinationRelay.onNext(.login)
            }).disposed(by: disposeBag)
        
        continueClicks.withLatestFrom(resetDataObs)
            .onlyIfCurrentPage(is: .createNewPassword, pageStream: self.showingPageRelay)
            .subscribe(onNext: { [unowned self] requestData in
                self.validateResetPassword(requestData: requestData)
            }).disposed(by: disposeBag)
    }
    
    private func sendEmailRequest(email: String) {
        progressBarSubject.onNext(true)
        profileRepository.createNewPassword(forUserWithEmail: email)
            .subscribe(onNext: { [weak self] result in
                self?.progressBarSubject.onNext(false)
                switch result {
                case .success:
                    self?.showingPageRelay.onNext(.checkInbox)
                case .failure(let error):
                    self?.alertRelay.onNext(error.simpleAlertData)
                }
            }).disposed(by: disposeBag)
    }
    
    private func validate(email: String) {
        let result = emailValidationHelper.validate(email: email, validator: validator)
        switch result {
        case .success:
            sendEmailRequest(email: email)
        case .failure(let error):
            alertRelay.onNext(error.simpleAlertData)
        }
    }
    
    private func validateResetPassword(requestData: ResetPasswordRequest) {
        let result = passwordRequestValidationHelper.validate(requestData: requestData, validator: validator)
        switch result {
        case .success:
            sendResetPasswordRequest(requestData: requestData)
        case .failure(let error):
            alertRelay.onNext(error.simpleAlertData)
        }
    }
    
    private func sendResetPasswordRequest(requestData: ResetPasswordRequest) {
        progressBarSubject.onNext(true)
        profileRepository.resetPassword(using: requestData)
            .subscribe(onNext: { [weak self] result in
                self?.progressBarSubject.onNext(false)
                switch result {
                case .success(let response):
                    let success: Function = { [weak self] in self?.destinationRelay.onNext(.login) }
                    self?.alertRelay.onNext(AlertData.fromSimpleMessage(message: response.message, action: success))
                case .failure(let error):
                    self?.alertRelay.onNext(error.simpleAlertData)
                }
            }).disposed(by: disposeBag)
    }
}

// MARK: Interface logic methods
extension ResetPasswordViewModel: ResetPasswordLogic { }

extension ObservableType {
    
    func onlyIfCurrentPage(is page: ResetPasswordPage, pageStream: Observable<ResetPasswordPage>) -> Observable<Element> {
        return self.withLatestFrom(pageStream) { ($0, $1) }
            .filter { $1 == page }
            .map { $0.0 }
    }
    
}
