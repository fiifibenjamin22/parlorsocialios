//
//  PasswordRequestValidationUseCase.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 05/02/2020.
//

import Foundation

class ResetPasswordRequestValidationHelper {
    
    // MARK: - Execution
    func validate(requestData: ResetPasswordRequest, validator: Validator) -> Validator.Result {
        let passwordValidatedInput = ValidatedInput(
            rules: [StringNotBlank(), StringPasswordPower()],
            value: requestData.password,
            name: Strings.ResetPassword.newPassword.localized
        )
        return validator.validateWithResult(for: [passwordValidatedInput])
    }
    
}
