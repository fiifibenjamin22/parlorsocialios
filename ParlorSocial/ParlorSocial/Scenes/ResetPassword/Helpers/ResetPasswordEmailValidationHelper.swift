//
//  ResetPasswordEmailValidationHelper.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 05/02/2020.
//

import Foundation

class ResetPasswordEmailValidationHelper {
    
    // MARK: - Execution
    func validate(email: String, validator: Validator) -> Validator.Result {
        let emailValidatedInput = ValidatedInput(rules: [StringNotBlank()], value: email, name: Strings.ResetPassword.email.localized)
        
        return validator.validateWithResult(for: [emailValidatedInput])
    }
    
}
