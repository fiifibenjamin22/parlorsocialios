//
//  ResetPasswordViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 04/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class ResetPasswordViewController: ViewPagerViewController<ResetPasswordViewModel> {
    
    // MARK: - Initialization
    static func fromResetPasswordDeepLink(forUserWithEmail email: String, token: String) -> ResetPasswordViewController {
        let controller = ResetPasswordViewController(withInitialPage: .createNewPassword, email: email, token: token)
        return controller
    }
    
    init(withInitialPage initialPage: ResetPasswordPage = ResetPasswordPage.typeEmail, email: String = "", token: String = "") {
        super.init()
        self.viewModel = ResetPasswordViewModel(withInitialPage: initialPage, email: email, token: token)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }

}
