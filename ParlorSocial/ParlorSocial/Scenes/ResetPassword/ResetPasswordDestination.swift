//
//  ResetPasswordRouter.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 09/04/2019.
//

import Foundation
import UIKit

enum ResetPasswordDestination {
    case login
}

extension ResetPasswordDestination: Destination {
    
    var viewController: UIViewController {
        switch self {
        case .login:
            return LoginViewController().embedInNavigationController()
        }
    }
    
}
