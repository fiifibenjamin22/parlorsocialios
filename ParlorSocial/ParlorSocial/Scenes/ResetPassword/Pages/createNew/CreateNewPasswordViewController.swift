//
//  CreateNewPasswordViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 04/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class CreateNewPasswordViewController: ResetPasswordPageViewController {

    // MARK: - Properties
    
    // MARK: - Views
    private(set) var iconedMessage: IconedMessageView!
    private(set) var passwordsStack: UIStackView!
    private(set) var newPasswordField: LoginTextField!
    
    // MARK: - Initialization
    init() {
        super.init(nibName: nil, bundle: nil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() {
    }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
    }
    
    override func bindUI() {
        super.bindUI()
        
        newPasswordField.rx.text
            .emptyOnNil()
            .bind(to: viewModel.newPasswordRelay)
            .disposed(by: disposeBag)
    }

}

// MARK: - Private methods
extension CreateNewPasswordViewController {

    private func setupViews() {
        let builder = CreateNewPasswordViewBuilder(controller: self)
        self.iconedMessage = builder.buildIconedMessage()
        self.newPasswordField = builder.buildNewPasswordTextField()
        self.passwordsStack = builder.buildNewPasswordsStack()
        builder.setupViews()
    }

    private func setupPresentationLogic() {
    }

}
