//
//  CreateNewPasswordViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 04/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class CreateNewPasswordViewBuilder {

    private unowned let controller: CreateNewPasswordViewController
    private var view: UIView! { return controller.view }

    init(controller: CreateNewPasswordViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension CreateNewPasswordViewBuilder {

    private func setupProperties() {
        controller.apply {
            $0.newPasswordField.placeholder = Strings.ResetPassword.newPassword.localized
            $0.iconedMessage.messageLabel.isHidden = true
        }
    }

    private func setupHierarchy() {
        controller.apply {
            view.addSubviews([$0.iconedMessage, $0.passwordsStack])
        }
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.iconedMessage.topAnchor.equal(to: view.topAnchor)
            $0.iconedMessage.edgesToParent(anchors: [.leading, .trailing], padding: Constants.horizontalMarginBig)
            
            $0.passwordsStack.edgesToParent(anchors: [.leading, .trailing], padding: Constants.horizontalMarginBig)
            $0.passwordsStack.constraintByCenteringVerticallyBeetween($0.iconedMessage.bottomAnchor, and: view.bottomAnchor)
            
            $0.newPasswordField.heightAnchor.equalTo(constant: Constants.bigButtonHeight)
        }
    }

}

// MARK: - Public build methods
extension CreateNewPasswordViewBuilder {

    func buildIconedMessage() -> IconedMessageView {
        return IconedMessageView(
            icon: Icons.ResetPassword.createNew,
            title: Strings.ResetPassword.createNew.localized,
            message: "")
            .manualLayoutable()
    }
    
    func buildNewPasswordsStack() -> UIStackView {
        return UIStackView(axis: .vertical, with: [controller.newPasswordField], spacing: Constants.CreateNew.confirmPasswordTopMargin, alignment: .fill, distribution: .equalSpacing).manualLayoutable()
    }
    
    func buildNewPasswordTextField() -> LoginTextField {
        return LoginTextField().manualLayoutable().withLightBackgroundStyle().apply {
            $0.isPasswordField = true
        }
    }

}

private extension Constants {
    
    enum CreateNew {
        static let newPasswordTopMargin: CGFloat = 24
        static let confirmPasswordTopMargin: CGFloat = 10
        static let confirmPasswordBottomMargin: CGFloat = 66
    }
}
