//
//  ResetPasswordPage.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 05/04/2019.
//

import Foundation
import UIKit

enum ResetPasswordPage: PagerPage {
    case typeEmail
    case checkInbox
    case createNewPassword
}

extension ResetPasswordPage {
    
    var connectedController: AppViewController {
        switch self {
        case .createNewPassword:
            return CreateNewPasswordViewController()
        case .typeEmail:
            return TypeEmailViewController()
        case .checkInbox:
            return CheckInboxViewController()
        }
    }
    
    var bottomButtonText: String {
        switch self {
        case .typeEmail:
            return Strings.ResetPassword.continueButton.localized
        case .checkInbox:
            return Strings.ResetPassword.login.localized
        case .createNewPassword:
            return Strings.ResetPassword.submitPassword.localized
        }
    }
}
