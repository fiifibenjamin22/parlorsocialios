//
//  ResetPasswordPageViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 04/04/2019.
//

import UIKit
import RxSwift

class ResetPasswordPageViewController: AppViewController {
    let analyticEventScreen: AnalyticEventScreen? = .forgotPassword
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return nil
    }
    
    var viewModel: ResetPasswordLogic {
        guard let controller = parent?.parent as? ResetPasswordViewController else { fatalError() }
        return controller.viewModel
    }
    
}
