//
//  TypeEmailViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 04/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class TypeEmailViewController: ResetPasswordPageViewController {
    
    // MARK: - Views
    private(set) var iconedMessage: IconedMessageView!
    private(set) var emailTextField: UITextField!

    // MARK: - Initialization
    init() {
        super.init(nibName: nil, bundle: nil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() {}

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
    }
    
    override func bindUI() {
        super.bindUI()
        
        emailTextField.rx.text
            .emptyOnNil()
            .bind(to: viewModel.forgottenMailSubject)
            .disposed(by: disposeBag)
    }

}

// MARK: - Private methods
extension TypeEmailViewController {

    private func setupViews() {
        let builder = TypeEmailViewBuilder(controller: self)
        self.iconedMessage = builder.buildIconedMessageView()
        self.emailTextField = builder.buildEmailTextField()
        builder.setupViews()
    }

    private func setupPresentationLogic() {
    }

}
