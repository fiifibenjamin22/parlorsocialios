//
//  CheckInboxViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 05/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class CheckInboxViewController: ResetPasswordPageViewController {

    // MARK: - Properties
    
    // MARK: - Views
    private(set) var iconedMessage: IconedMessageView!

    // MARK: - Initialization
    init() {
        super.init(nibName: nil, bundle: nil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() { }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
    }

}

// MARK: - Private methods
extension CheckInboxViewController {

    private func setupViews() {
        let builder = CheckInboxViewBuilder(controller: self)
        self.iconedMessage = builder.buildIconedMessage()
        builder.setupViews()
    }

    private func setupPresentationLogic() {
    }

}
