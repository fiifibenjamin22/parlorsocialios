//
//  CheckInboxViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 05/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class CheckInboxViewBuilder {

    private unowned let controller: CheckInboxViewController
    private var view: UIView! { return controller.view }

    init(controller: CheckInboxViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension CheckInboxViewBuilder {

    private func setupProperties() {
    }

    private func setupHierarchy() {
        controller.apply {
            view.addSubviews([$0.iconedMessage])
        }
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.iconedMessage.topAnchor.equal(to: view.topAnchor)
            $0.iconedMessage.edgesToParent(anchors: [.leading, .trailing], padding: Constants.horizontalMarginBig)
        }
    }

}

// MARK: - Public build methods
extension CheckInboxViewBuilder {
    
    func buildIconedMessage() -> IconedMessageView {
        return IconedMessageView(
            icon: Icons.ResetPassword.tick,
            title: nil,
            message: Strings.ResetPassword.checkInbox.localized)
            .manualLayoutable().apply { $0.roundedIcon.backgroundColor = UIColor.backgroundGreen }
    }
    
}
