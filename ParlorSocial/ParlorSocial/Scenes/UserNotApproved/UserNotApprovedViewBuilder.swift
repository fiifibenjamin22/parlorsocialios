//  UserNotApprovedViewBuilder.swift
//  ParlorSocialClub
//
//  Created on 06/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

class UserNotApprovedViewBuilder {
    
    // MARK: - Properties
    
    private unowned let controller: UserNotApprovedViewController
    private var view: UIView! { return controller.view }
    
    // MARK: - Init
    
    init(controller: UserNotApprovedViewController) {
        self.controller = controller
    }
    
    // MARK: - Setup
    
    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
    
    private func setupProperties() {
        view.backgroundColor = .black
        
        controller.apply {
            $0.allowNotificationsButton.setTitle(Strings.UserNotApproved.allowNotifications.localized, for: .normal)
            $0.titleLabel.text = Strings.UserNotApproved.notificationsNotAllowedTitle.localized
            $0.titleLabel.lineHeight = 24
            $0.logoutButton.setTitle(Strings.UserNotApproved.logOut.localized, for: .normal)
        }
    }

    private func setupHierarchy() {
        controller.apply {
            view.addSubviews([$0.backgroundImageView, $0.backgroundMaskView, $0.containerView, $0.logoutButton])
            $0.containerView.addSubviews([$0.iconImageView, $0.titleLabel, $0.allowNotificationsButton, $0.followUsOnInstagramButton])
        }
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.backgroundImageView.edgesToParent()
            
            $0.backgroundMaskView.edgesToParent()
            
            $0.containerView.topAnchor.lessThanOrEqual(to: view.topAnchor, constant: 227)
            $0.containerView.edgesToParent(anchors: [.leading, .trailing], insets: .init(padding: 52))
            
            $0.iconImageView.centerXAnchor.equal(to: $0.containerView.centerXAnchor)
            $0.iconImageView.edgesToParent(anchors: [.top])
            $0.iconImageView.heightAnchor.equalTo(constant: 25)
            $0.iconImageView.widthAnchor.equalTo(constant: 25)

            $0.titleLabel.topAnchor.equal(to: $0.iconImageView.bottomAnchor, constant: 15)
            $0.titleLabel.edgesToParent(anchors: [.leading, .trailing])
            
            $0.allowNotificationsButton.topAnchor.equal(to: $0.titleLabel.bottomAnchor, constant: 20)
            $0.allowNotificationsButton.centerXAnchor.equal(to: $0.titleLabel.centerXAnchor)
            $0.allowNotificationsButton.heightAnchor.equalTo(constant: 40)
            $0.allowNotificationsButton.widthAnchor.constraint(equalTo: $0.view.widthAnchor, multiplier: 0.49).activate()
            $0.allowNotificationsButton.edgesToParent(anchors: [.bottom])
            
            $0.followUsOnInstagramButton.topAnchor.equal(to: $0.titleLabel.bottomAnchor, constant: 46)
            $0.followUsOnInstagramButton.centerXAnchor.equal(to: $0.titleLabel.centerXAnchor)
            $0.followUsOnInstagramButton.heightAnchor.equalTo(constant: 24)
            $0.followUsOnInstagramButton.widthAnchor.constraint(equalTo: $0.view.widthAnchor, multiplier: 0.4).activate()
            $0.followUsOnInstagramButton.edgesToParent(anchors: [.bottom])

            $0.logoutButton.centerXAnchor.equal(to: $0.view.centerXAnchor)
            $0.logoutButton.edgesToParent(anchors: [.bottom], insets: .init(padding: 46))
            $0.logoutButton.heightAnchor.equalTo(constant: Constants.bigButtonHeight)
            $0.logoutButton.widthAnchor.constraint(equalTo: $0.view.widthAnchor, multiplier: 0.4).activate()
            $0.logoutButton.topAnchor.greaterThanOrEqual(to: $0.containerView.bottomAnchor, constant: 30)
        }
    }
    
    // MARK: - Build views methods
    
    func buildBackgroundImageView() -> UIImageView {
        return UIImageView().manualLayoutable().apply {
            $0.image = Icons.UserNotApproved.backgroundImage
        }
    }
    
    func buildBackgroundMaskView() -> UIView {
        return UIView(frame: .zero).manualLayoutable().apply {
            $0.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        }
    }
    
    func buildContainerView() -> UIView {
        return UIView(frame: .zero).manualLayoutable()
    }
    
    func buildIconImageView() -> UIImageView {
        return UIImageView().manualLayoutable().apply {
            $0.image = Icons.Common.checkedWhite
        }
    }
    
    func buildTitleLabel() -> ParlorLabel {
        return ParlorLabel.styled(
            withTextColor: .appTextWhite,
            withFont: UIFont.custom(ofSize: Constants.FontSize.body, font: UIFont.Roboto.regular),
            alignment: .center,
            numberOfLines: 0
        )
    }

    func buildAllowNotificationsButton() -> RoundedButton {
        return RoundedButton().manualLayoutable().apply {
            $0.backgroundColor = .white
            $0.setTitleColor(.appTextMediumBright, for: .normal)
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.small, font: UIFont.Roboto.medium)
            $0.titleLabel?.adjustsFontSizeToFitWidth = true
            $0.titleLabel?.minimumScaleFactor = 0.7
            $0.applyTouchAnimation()
        }
    }
    
    func buildFollowUsOnInstagramButton() -> RoundedButton {
        return RoundedButton().manualLayoutable().apply {
            $0.backgroundColor = .clear
            $0.setTitleColor(.brownGrey, for: .normal)
            $0.setImage(Icons.UserNotApproved.instagramIcon, for: .normal)
            $0.adjustsImageWhenHighlighted = false
            $0.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 12)
            $0.setTitle(Strings.UserNotApproved.followUs.localized, for: .normal)
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.small, font: UIFont.Roboto.regular)
            $0.applyTouchAnimation()
        }
    }
    
    func buildLogoutButton() -> RoundedButton {
        return RoundedButton.withBackgroundStyle().manualLayoutable().apply {
            $0.setTitleColor(.appGreyLight, for: .normal)
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.small, font: UIFont.Roboto.regular)
            $0.backgroundColor = .clear
            $0.layer.borderColor = UIColor.clear.cgColor
        }
    }
}
