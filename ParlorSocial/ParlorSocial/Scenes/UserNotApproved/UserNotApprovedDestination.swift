//  UserNotApprovedDestination.swift
//  ParlorSocialClub
//
//  Created on 06/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

enum UserNotApprovedDestination {
    case login
    case dashboard
    case payment
    case membershipPlans
    case webView(withData: BaseWebViewData)
}

extension UserNotApprovedDestination: Destination {
    var viewController: UIViewController {
        switch self {
        case .login:
            return LoginViewController().embedInNavigationController()
        case .dashboard:
            return AppTabBarController().embedInNavigationController()
        case .payment:
            return PremiumPaymentViewController().embedInNavigationController()
        case .membershipPlans:
            return MembershipPlansViewController().embedInNavigationController()
        case .webView(let data):
            return BaseWebViewController(data: data).embedInNavigationController()
        }
    }
}
