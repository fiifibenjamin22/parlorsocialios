//  UserNotApprovedViewModel.swift
//  ParlorSocialClub
//
//  Created on 06/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import RxSwift

protocol UserNotApprovedLogic: BaseViewModelLogic {
    var profileObs: Observable<Profile> { get }
    var destinationObs: Observable<UserNotApprovedDestination> { get }
    var pushNotificationsEnableStateObs: Observable<Bool> { get }

    func logout()
    func enablePushNotifications()
    func checkPushNotificationsEnableState()
    func openInstagramProfileInWebView()
}

class UserNotApprovedViewModel: BaseViewModel {
    
    // MARK: - Properties
    
    var analyticEventReferencedId: Int?
    private let notificationService: GlobalNotificationsService
    private let profileRepository: ProfileRepositoryProtocol
    private let updateNotificationsReminderDataManager: UpdateNotificationsReminderDataManager
    private let destinationsSubject: PublishSubject<UserNotApprovedDestination> = PublishSubject()
    private let pushNotificationsEnableStateSubject: PublishSubject<Bool> = PublishSubject()
    
    // MARK: - Init
    
    init(
        profileRepository: ProfileRepositoryProtocol = ProfileRepository.shared,
        notificationService: GlobalNotificationsService = .shared,
        updateNotificationsReminderDataManager: UpdateNotificationsReminderDataManager = UpdateNotificationsReminderDataManager()
    ) {
        self.notificationService = notificationService
        self.profileRepository = profileRepository
        self.updateNotificationsReminderDataManager = updateNotificationsReminderDataManager
        
        super.init()
        
        bindUserProfile()
        registerForRemoteNotificationsIfAllowed()
        updateUserProfile()
    }
    
    private func bindUserProfile() {
        profileRepository.currentProfileObs
            .compactMap { $0.destinationAfterLogin }
            .compactMap { [unowned self] destination in self.mapDestinationAfterLoginToDestination(destinationAfterLogin: destination) }
            .bind(to: destinationsSubject)
            .disposed(by: disposeBag)
    }
    
    private func registerForRemoteNotificationsIfAllowed() {
        notificationService.arePermissionsAuthorized { isGranted in
            guard isGranted else { return }
            self.notificationService.setEnabledPushNotifications()
        }
    }
    
    // MARK: - Private methods
    
    private func updateUserProfile() {
        profileRepository.refreshProfileData()
    }
    
    private func mapDestinationAfterLoginToDestination(destinationAfterLogin: DestinationAfterLogin) -> UserNotApprovedDestination? {
        switch destinationAfterLogin {
        case .dashboard: return .dashboard
        case .payment: return .payment
        case .selectMembership: return .membershipPlans
        case .userNotApproved: return nil
        }
    }
    
    private func showLogoutConfirmationAlert(completion: @escaping () -> Void) {
        alertDataSubject.onNext(
            AlertData(
                icon: Icons.Error.login,
                title: Strings.UserNotApproved.confirmLogoutTitle.localized,
                message: nil,
                bottomButtonTitle: Strings.UserNotApproved.logOut.localized.uppercased(),
                buttonAction: completion
            )
        )
    }
}

// MARK: - UserNotApprovedLogic

extension UserNotApprovedViewModel: UserNotApprovedLogic {
    var destinationObs: Observable<UserNotApprovedDestination> {
        return destinationsSubject.asObservable()
    }
    
    var pushNotificationsEnableStateObs: Observable<Bool> {
        return pushNotificationsEnableStateSubject.asObservable()
    }

    var profileObs: Observable<Profile> {
        return profileRepository.currentProfileObs
    }
    
    func logout() {
        showLogoutConfirmationAlert { [unowned self] in
            self.progressBarSubject.onNext(true)
            self.notificationService.unregisterDeviceToken { [unowned self] _ in
                self.progressBarSubject.onNext(false)
                self.destinationsSubject.onNext(.login)
            }
        }
    }
    
    func enablePushNotifications() {
        notificationService.configurePushNotifications { [weak self] isGranted, alertData in
            if isGranted {
                self?.updateNotificationsReminderDataManager.updateDataOnEnableNotificationsReminderVC(isGranted: isGranted, notificationsSettingsChangeSource: .userNotApprovedScreen)
            }
            DispatchQueue.main.async { [weak self] in
                if let alertData = alertData {
                    self?.alertDataSubject.onNext(alertData)
                } else {
                    self?.pushNotificationsEnableStateSubject.onNext(isGranted)
                }
            }
        }
    }
    
    func checkPushNotificationsEnableState() {
        notificationService.arePermissionsAuthorized(completionHandler: { [unowned self] enabled in
            self.pushNotificationsEnableStateSubject.onNext(enabled)
        })
    }
    
    func openInstagramProfileInWebView() {
        let data = BaseWebViewData(navBarTitle: nil, url: Config.instagramProfileUrl)
        destinationsSubject.onNext(.webView(withData: data))
    }
}
