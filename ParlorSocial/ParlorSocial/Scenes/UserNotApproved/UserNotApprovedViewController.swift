//  UserNotApprovedViewController.swift
//  ParlorSocialClub
//
//  Created on 06/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

class UserNotApprovedViewController: AppViewController {
    
    // MARK: - Views
    
    private(set) var backgroundImageView: UIImageView!
    private(set) var backgroundMaskView: UIView!
    private(set) var containerView: UIView!
    private(set) var iconImageView: UIImageView!
    private(set) var titleLabel: ParlorLabel!
    private(set) var allowNotificationsButton: RoundedButton!
    private(set) var followUsOnInstagramButton: RoundedButton!
    private(set) var logoutButton: RoundedButton!

    // MARK: - Properties
    
    private var viewModel: UserNotApprovedLogic!
    var analyticEventScreen: AnalyticEventScreen? = .userNotApproved
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }
    override var observableSources: ObservableSources? {
        return viewModel
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
        
    // MARK: - Initialization
    
    init() {
        super.init(nibName: nil, bundle: nil)
        
        viewModel = UserNotApprovedViewModel()
    }

    required init?(coder aDecoder: NSCoder) {
       fatalError()
    }
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        bindView()
        setupActions()
        viewModel.checkPushNotificationsEnableState()
    }
    
    // MARK: - Setup
    
    private func setupViews() {
        let builder = UserNotApprovedViewBuilder(controller: self)
        backgroundImageView = builder.buildBackgroundImageView()
        backgroundMaskView = builder.buildBackgroundMaskView()
        containerView = builder.buildContainerView()
        iconImageView = builder.buildIconImageView()
        titleLabel = builder.buildTitleLabel()
        allowNotificationsButton = builder.buildAllowNotificationsButton()
        followUsOnInstagramButton = builder.buildFollowUsOnInstagramButton()
        logoutButton = builder.buildLogoutButton()
        
        builder.setupViews()
    }
    
    private func bindView() {
        viewModel.destinationObs
            .subscribe(onNext: { [unowned self] destination in
                self.handleDestination(destination)
            })
            .disposed(by: disposeBag)

        Observable.combineLatest(viewModel.pushNotificationsEnableStateObs, viewModel.profileObs) { ($0, $1) }
            .observeOn(MainScheduler.instance)
            .subscribe(
                onNext: { [unowned self] enabled, profile in
                    self.updateView(forPushNotificationsEnableState: enabled, profile: profile)
                }
        ).disposed(by: disposeBag)
    }
    
    private func setupActions() {
        allowNotificationsButton.addTarget(self, action: #selector(didTapAllowNotifications), for: .touchUpInside)
        logoutButton.addTarget(self, action: #selector(didTapLogout), for: .touchUpInside)
        followUsOnInstagramButton.addTarget(self, action: #selector(didTapFollowUsOnInstagram), for: .touchUpInside)
    }
    
    // MARK: - Actions
    
    @objc private func didTapLogout() {
        viewModel.logout()
    }
    
    @objc private func didTapAllowNotifications() {
        viewModel.enablePushNotifications()
    }
    
    @objc private func didTapFollowUsOnInstagram() {
        viewModel.openInstagramProfileInWebView()
    }
    
    private func handleDestination(_ destination: UserNotApprovedDestination) {
        switch destination {
        case .login, .dashboard, .membershipPlans, .payment:
            router.replace(with: destination)
        case .webView:
            router.present(destination: destination)
        }
    }
    
    private func updateView(forPushNotificationsEnableState enabled: Bool, profile: Profile) {
        if let applicationStatus = profile.applicationStatus {
            titleLabel.text = enabled ? applicationStatus.notificationAllowedTitle : applicationStatus.notificationsNotAllowedTitle
        } else {
            titleLabel.text = enabled ? Strings.UserNotApproved.notificationsAllowedTitle.localized : Strings.UserNotApproved.notificationsNotAllowedTitle.localized
        }

        allowNotificationsButton.isHidden = enabled
        followUsOnInstagramButton.isHidden = !enabled
    }
    
    override func onAppDidBecomeActive() {
        super.onAppDidBecomeActive()
        
        viewModel.checkPushNotificationsEnableState()
    }
}
