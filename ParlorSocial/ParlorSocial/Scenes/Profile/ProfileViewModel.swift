//
//  ProfileViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 12/03/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxSwift

protocol ProfileLogic: BaseViewModelLogic, HasActivateMembershipViewLogic {
    var profileObs: Observable<Profile> { get }
    var currentSections: Observable<[TableSection]> { get }
    var destinationObs: Observable<ProfileDestination> { get }
    var openMailComposerObs: Observable<EmailSettings> { get }
    var currentColorThemeObs: Observable<PremiumColorManager.Theme> { get }
    var navigationAndStatusBarVisibilityObs: Observable<Bool> { get }
    
    var currentTabSubject: BehaviorSubject<ProfileTabType> { get }
    var tabSelectedSubject: PublishSubject<Void> { get }

    func prepareDataToContactEmail()
    func prepareDataToSuggestEventsOrVenues()
    func resetManualFlags()
    func didTapEditProfile()
    func setupNavigationAndStatusBar()
}

class ProfileViewModel: BaseViewModel {
    let analyticEventReferencedId: Int? = nil
    private let profileRepository = ProfileRepository.shared
    var activateMembershipViewLogic: ActivateMembershipViewLogic?
    
    let currentTabSubject: BehaviorSubject<ProfileTabType> = BehaviorSubject(value: .circles)
    let openMailComposerSubject = PublishSubject<EmailSettings>()
    let tabSelectedSubject: PublishSubject<Void> = PublishSubject()
    let currentColorThemeSubject: BehaviorSubject<PremiumColorManager.Theme> = BehaviorSubject(value: PremiumColorManager.shared.currentTheme)
    var setupActivateMembershipViewSubject: PublishSubject<ActivateMembershipViewModel> = PublishSubject<ActivateMembershipViewModel>()
    let destinationSubject: PublishSubject<ProfileDestination> = PublishSubject<ProfileDestination>()
    private let navigationAndStatusBarVisibilitySubject = PublishSubject<Bool>()

    lazy var currentTabSource: Observable<ProfileTabContentSource> = {
        return currentTabSubject.map { $0.source }.share(replay: 1)
    }()
    
    override init() {
        super.init()
        initFlow()
    }
    
    override var closeObs: Observable<Void> {
        return currentTabSource.flatMapLatest { $0.closeObs }
    }
    
    override var alertDataObs: Observable<AlertData> {
        return currentTabSource.flatMapLatest { $0.alertDataObs }
    }
    
    override var errorObs: Observable<AppError> {
        return currentTabSource.flatMapLatest { $0.errorObs }
    }
    
    override var progressBarObs: Observable<Bool> {
        return currentTabSource.flatMapLatest { $0.progressBarObs }
    }
}

// MARK: Private logic
private extension ProfileViewModel {
    
    private func initFlow() {
        tabSelectedSubject
            .flatMapFirst { [unowned self] in
                self.profileRepository.getMyProfileData()
            }.subscribe().disposed(by: disposeBag)
        
        profileRepository.currentColorThemeObs
            .bind(to: currentColorThemeSubject)
            .disposed(by: disposeBag)
    }

}

// MARK: Interface logic methods
extension ProfileViewModel: ProfileLogic {
    var navigationAndStatusBarVisibilityObs: Observable<Bool> {
        return navigationAndStatusBarVisibilitySubject.asObservable()
    }
    
    var currentColorThemeObs: Observable<PremiumColorManager.Theme> {
        return currentColorThemeSubject.asObservable()
    }
    
    var openMailComposerObs: Observable<EmailSettings> {
        return openMailComposerSubject.asObservable()
    }
    
    var currentSections: Observable<[TableSection]> {
        return currentTabSource.flatMapLatest { $0.currentTableContent }
    }
    
    var profileObs: Observable<Profile> {
        return profileRepository.currentProfileObs
    }
    
    var destinationObs: Observable<ProfileDestination> {
        return Observable.of(destinationSubject, currentTabSource.flatMapLatest { $0.destinations }).merge()
    }
    
    func prepareDataToContactEmail() {
        guard let emailAddress = Config.globalSettings?.contactEmailAddress,
            let emailSubject = Config.globalSettings?.contactEmailSubject else { return }
        let emailSettings = EmailSettings(address: emailAddress, subject: emailSubject)
        openMailComposerSubject.onNext(emailSettings)
    }
    
    func prepareDataToSuggestEventsOrVenues() {
        guard let emailAddress = Config.globalSettings?.suggestEventsOrVenuesEmailAddress,
            let emailSubject = Config.globalSettings?.suggestEventsOrVenuesEmailSubject else { return }
        let emailSettings = EmailSettings(address: emailAddress, subject: emailSubject)
        openMailComposerSubject.onNext(emailSettings)
    }
    
    func resetManualFlags() {
        Config.setOnboardingAllTypesUnshown()
    }
    
    func didTapEditProfile() {
        destinationSubject.onNext(.editProfile)
    }
    
    func didTapActivateMembershipMoreButton() {
        activateMembershipViewLogic?.buttonClicked()
    }
    
    func initActivateMembershipViewLogic() {
        activateMembershipViewLogic = ActivateMembershipViewLogic(profileRepository: ProfileRepository.shared)
        
        activateMembershipViewLogic?.setupViewRelay
            .ignoreNil()
            .bind(to: setupActivateMembershipViewSubject)
            .disposed(by: disposeBag)
        
        activateMembershipViewLogic?.buttonClickedSubject
            .subscribe(onNext: { [unowned self] _ in
                self.destinationSubject.onNext(.membershipPlans)
            }).disposed(by: disposeBag)
    }
    
    func setupNavigationAndStatusBar() {
        activateMembershipViewLogic?.setupViewRelay
            .ignoreNil()
            .subscribe(onNext: { [unowned self] model in
                self.navigationAndStatusBarVisibilitySubject.onNext(!model.isHidden)
            }).disposed(by: disposeBag)
    }
    
}

fileprivate extension ProfileTabType {
    
    var source: ProfileTabContentSource {
        switch self {
        case .circles:
            return CirclesTabContent()
        case .interests:
            return InterestsTabContent()
        case .contact:
            return ContactsTabContent()
        case .settings:
            return SettingsTabContent()
        }
    }
    
}
