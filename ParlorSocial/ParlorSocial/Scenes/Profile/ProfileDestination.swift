//
//  ProfileDestination.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 11.04.2019.
//

import UIKit

enum ProfileDestination {
    case login
    case interests
    case editProfile
    case circleDetails(Circle)
    case referFriend
    case upgrade
    case changeEmail
    case changePassword
    case mapProvider
    case seeManual
    case terms
    case privacy
    case emailUs
    case suggestEventsOrVenues
    case cardList
    case payment
    case membershipPlans
}

extension ProfileDestination: Destination {
    
    var viewController: UIViewController {
        switch self {
        case .login:
            return LoginViewController().embedInNavigationController()
        case .interests:
            return InterestGroupsViewController()
        case .editProfile:
            return EditProfileViewController()
        case .circleDetails(let circle):
            return CircleDetailsViewController(withCircle: circle)
        case .referFriend:
            return ReferFriendViewController()
        case .changeEmail:
            return ChangeEmailViewController()
        case .changePassword:
            return ChangePasswordViewController()
        case .upgrade:
            return UpgradeMembershipViewController(navigationSource: .profile)
        case .mapProvider:
            return MapProviderViewController()
        case .seeManual:
            return HomeViewController()
        case .terms:
            let data = BaseWebViewData(
                navBarTitle: Strings.TermsAndPrivacy.termsTitle.localized.uppercased(),
                url: Config.termsAndConditionsURL)
            return BaseWebViewController(data: data).embedInNavigationController()
        case .privacy:
            let data = BaseWebViewData(
                navBarTitle: Strings.TermsAndPrivacy.privacyTitle.localized.uppercased(),
                url: Config.privacyPolicyURL)
            return BaseWebViewController(data: data).embedInNavigationController()
        case .cardList:
            return CreditCardListViewController()
        case .emailUs:
            fatalError("There should be used email composer")
        case .suggestEventsOrVenues:
            fatalError("There should be used email composer")
        case .payment:
            return PremiumPaymentViewController()
        case .membershipPlans:
            return MembershipPlansViewController()
        }
    }
    
}
