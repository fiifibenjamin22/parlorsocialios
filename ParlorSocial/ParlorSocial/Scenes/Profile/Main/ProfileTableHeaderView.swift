//
//  ProfileTableHeaderView.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 23/05/2019.
//

import UIKit

class ProfileTableHeaderView: UIView {

    // MARK: - Views

    private(set) lazy var profileImageView: UIImageView = {
        return UIImageView().manualLayoutable().apply {
            $0.contentMode = .scaleAspectFill
            $0.clipsToBounds = true
            $0.layer.cornerRadius = 50
        }
    }()
    
    private(set) lazy var nameAndPositionStack: UIStackView = {
        return UIStackView(axis: .vertical, with: [nameLabel, positionLabel, premiumMembershipStackView], spacing: 10,
                           alignment: .center, distribution: .equalSpacing).manualLayoutable()
    }()
    
    private(set) lazy var nameLabel: ParlorLabel = {
        return ParlorLabel.styled(
            withFont: UIFont.custom(ofSize: Constants.FontSize.title, font: UIFont.Editor.bold),
            alignment: .center, numberOfLines: 1)
    }()
    
    private(set) lazy var positionLabel: ParlorLabel = {
        return ParlorLabel.styled(
            withFont: UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.regular),
            alignment: .center, numberOfLines: 1)
    }()
    
    private(set) lazy var premiumMembershipIcon: UIImageView = {
        return UIImageView(image: Icons.GuestListRsvp.premiumIcon).manualLayoutable()
    }()
    
    private(set) lazy var premiumMembershipLabel: UILabel = {
        return UILabel.styled(
            textColor: .appTextGold,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.small,
                font: UIFont.Roboto.regular
            ),
            alignment: .left,
            numberOfLines: 1
            ).apply {
                $0.text = Strings.GuestListRsvp.premiumMember.localized
        }
    }()
    
    private(set) lazy var premiumMembershipStackView: UIStackView = {
        return UIStackView(axis: .horizontal, with: [premiumMembershipIcon, premiumMembershipLabel],
                           spacing: 5, alignment: .center, distribution: .fill)
    }()
    
    private(set) lazy var editProfileButton: RoundedButton = {
        return RoundedButton().manualLayoutable().apply {
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium)
            $0.layer.borderWidth = 1
            $0.applyTouchAnimation()
            $0.setTitle(Strings.EditProfile.title.localized.uppercased(), for: .normal)
            $0.trackingId = AnalyticEventClickableElementName.Profile.editProfile.rawValue
        }
    }()
    
    private(set) lazy var seperator: UIView = {
        return UIView().manualLayoutable()
    }()
    
    private(set) lazy var bottomSeperator: UIView = {
        return UIView().manualLayoutable()
    }()
    
    lazy var tabsView: ProfileTabsView = {
        return ProfileTabsView().manualLayoutable()
    }()
    
    // MARK: - Properties
    
    var profileData: Profile? = Config.userProfile {
        didSet {
            updateData()
        }
    }
    
    // MARK: - Initialization
    
    init() {
        super.init(frame: .zero)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        addSubviews([profileImageView, nameAndPositionStack, editProfileButton, seperator, tabsView, bottomSeperator])
        setupAutoLayout()
        updateData()
    }
    
    private func setupAutoLayout() {
        profileImageView.apply {
            $0.topAnchor.equal(to: self.topAnchor, constant: 57)
            $0.centerXAnchor.equal(to: self.centerXAnchor)
            $0.widthAnchor.equalTo(constant: 100)
            $0.heightAnchor.equalTo(constant: 100)
        }
        
        nameAndPositionStack.apply {
            $0.centerXAnchor.equal(to: self.centerXAnchor)
            $0.topAnchor.equal(to: profileImageView.bottomAnchor, constant: 35)
            $0.edgesToParent(anchors: [.leading, .trailing], padding: Constants.Margin.standard)
        }
        
        editProfileButton.apply {
            $0.topAnchor.equal(to: nameAndPositionStack.bottomAnchor, constant: 27)
            $0.centerXAnchor.equal(to: self.centerXAnchor)
            $0.widthAnchor.equalTo(constant: 157)
            $0.heightAnchor.equalTo(constant: 40)
        }
        
        seperator.apply {
            $0.edgesToParent(anchors: [.leading, .trailing])
            $0.heightAnchor.equalTo(constant: 1)
            $0.topAnchor.equal(to: editProfileButton.bottomAnchor, constant: 42)
        }

        tabsView.apply {
            $0.topAnchor.equal(to: seperator.bottomAnchor)
            $0.heightAnchor.equalTo(constant: 75)
            $0.edgesToParent(anchors: [.leading, .trailing])
            $0.bottomAnchor.equal(to: bottomSeperator.topAnchor)
        }
        
        bottomSeperator.apply {
            $0.edgesToParent(anchors: [.leading, .trailing])
            $0.heightAnchor.equalTo(constant: 1)
            $0.bottomAnchor.equal(to: self.bottomAnchor)
        }
    }
    
    // MARK: - Private Logic
    
    private func updateData() {
        guard let profile = profileData else { return }
        profileImageView.getImage(from: profile.imageURL,
                                  placeholder: PremiumColorManager.shared.currentTheme.placeholderPhoto,
                                  ifFailedOrNilSet: PremiumColorManager.shared.currentTheme.placeholderPhoto)
        nameLabel.text = profile.fullName
        positionLabel.text = profile.positionAndCompany ?? " "
        premiumMembershipStackView.isHidden = !profile.isPremium
        tabsView.selectableButtons.forEach { $0.progressBar.color = PremiumColorManager.shared.currentTheme.progressBarColor }
    }

}
