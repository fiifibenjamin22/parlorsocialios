//
//  ProfileTabsView.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 23/05/2019.
//

import UIKit

enum ProfileTabType {
    case circles
    case interests
    case contact
    case settings
}

protocol ProfileTabsViewDelegate: class {
    func profileTabsView(_ view: ProfileTabsView, didSelectTabWithType type: ProfileTabType)
}

class ProfileTabsView: UIStackView {
    
    // MARK: - Views

    lazy var circlesButton: SelectableButton = {
        return SelectableButton().applyingProfileTabStyle(withTitle: Strings.Profile.circles.localized).apply {
            $0.trackingId = AnalyticEventClickableElementName.Profile.circlesTab.rawValue
        }
    }()
    
    lazy var interestsButton: SelectableButton = {
        return SelectableButton().applyingProfileTabStyle(withTitle: Strings.Profile.interest.localized).apply {
            $0.trackingId = AnalyticEventClickableElementName.Profile.interestsTab.rawValue
        }
    }()
    
    lazy var contactButton: SelectableButton = {
        return SelectableButton().applyingProfileTabStyle(withTitle: Strings.Profile.contact.localized).apply {
            $0.trackingId = AnalyticEventClickableElementName.Profile.contactTab.rawValue
        }
    }()
    
    lazy var settingsButton: SelectableButton = {
        return SelectableButton().applyingProfileTabStyle(withTitle: Strings.Profile.settings.localized).apply {
            $0.trackingId = AnalyticEventClickableElementName.Profile.settingsTab.rawValue
        }
    }()
    
    lazy var buttonToTypeMap: [SelectableButton: ProfileTabType] = {
        return [circlesButton: .circles, interestsButton: .interests, contactButton: .contact, settingsButton: .settings]
    }()
    
    lazy var innerSpacerViews: [UIView] = {
        return Array(0..<3).map { _ in
            return UIView().manualLayoutable().apply {
                $0.heightAnchor.equalTo(constant: 30)
            }
        }
    }()
    
    var selectableButtons: [SelectableButton] {
        return [circlesButton, interestsButton, contactButton, settingsButton]
    }
    
    // MARK: - Properties
    
    weak var delegate: ProfileTabsViewDelegate?
    
    // MARK: - Initialization
    
    init() {
        super.init(frame: .zero)
        initialize()
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        distribution = .fill
        spacing = 0
        alignment = .fill
        addArrangedSubviews([
            circlesButton,
            createSpacerView(innerView: innerSpacerViews[0]),
            interestsButton,
            createSpacerView(innerView: innerSpacerViews[1]),
            contactButton,
            createSpacerView(innerView: innerSpacerViews[2]),
            settingsButton
            ])
        
        circlesButton.widthAnchor.equal(to: interestsButton.widthAnchor)
        interestsButton.widthAnchor.equal(to: contactButton.widthAnchor)
        contactButton.widthAnchor.equal(to: settingsButton.widthAnchor)
        selectableButtons.forEach {
            $0.addTarget(self, action: #selector(didTapTabButton(_:)), for: .touchUpInside)
        }
        circlesButton.isSelected = true
    }
    
    private func createSpacerView(innerView: UIView) -> UIView {
        return UIView().apply {
            $0.widthAnchor.equalTo(constant: 1)
            $0.addSubview(innerView)
            innerView.edgesToParent(anchors: [.leading, .trailing])
            innerView.centerYAnchor.equal(to: $0.centerYAnchor)
        }
    }
    
    func hideProgressBarInAllButtons() {
        selectableButtons.forEach { $0.progressBar(isHidden: true) }
    }
    
    // MARK: - Private Logic
    
    @objc private func didTapTabButton(_ button: SelectableButton) {
        guard let selectedType = buttonToTypeMap[button] else { fatalError("Make sure each button has type connected")}
        if !button.isSelected {
            selectableButtons.forEach {
                if $0.isSelected { $0.isSelected = false }
            }
            hideProgressBarInAllButtons()
            button.progressBar(isHidden: false)
            delegate?.profileTabsView(self, didSelectTabWithType: selectedType)
            button.isSelected = true
        }
    }
}

fileprivate extension SelectableButton {
    
    func applyingProfileTabStyle(withTitle title: String) -> SelectableButton {
        setTitle(title, for: .normal)
        unselectedFont = UIFont.custom(ofSize: Constants.FontSize.small, font: UIFont.Roboto.regular)
        selectedFont = UIFont.custom(ofSize: Constants.FontSize.small, font: UIFont.Roboto.bold)
        return self
    }
}
