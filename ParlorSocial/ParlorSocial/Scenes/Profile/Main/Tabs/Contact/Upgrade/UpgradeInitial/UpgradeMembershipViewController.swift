//
//  UpgradeMembershipViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 07/06/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

class UpgradeMembershipViewController: AppViewController {
    
    // MARK: - Properties
    private(set) var scrollView: UIScrollView!
    private(set) var contentScrollView: UIView!
    private(set) var applyButton: RoundedButton!
    private(set) var labelsStack: UIStackView!
    private(set) var titleLabel: ParlorLabel!
    private(set) var messageLabel: ParlorLabel!
    private(set) var descriptionPlanStackView: UIStackView!
    private var viewModel: UpgradeMembershipLogic!
    let analyticEventScreen: AnalyticEventScreen? = .upgradeToPremium
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }
    
    override var observableSources: ObservableSources? {
        return viewModel
    }

    // MARK: - Initialization
    init(navigationSource: UpgradeToPremiumNavigationSource) {
        super.init(nibName: nil, bundle: nil)
        setup(withNavigationSource: navigationSource)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    private func setup(withNavigationSource navigationSource: UpgradeToPremiumNavigationSource) {
        self.viewModel = UpgradeMembershipViewModel(navigationSource: navigationSource)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
        adjustNavigationBar()
    }
    
    override func bindUI() {
        super.bindUI()
        applyButton.rx.controlEvent(.touchUpInside)
            .bind(to: viewModel.applyClicks).disposed(by: disposeBag)
    }
    
    override func observeViewModel() {
        super.observeViewModel()
        viewModel.closeObs
            .doOnNext { [unowned self] _ in self.navigationController?.popViewController(animated: true) }
            .subscribe()
            .disposed(by: disposeBag)
        
        viewModel.destinationObs
            .subscribe(onNext: { [unowned self] destination in
                self.handle(destination: destination)
            }).disposed(by: disposeBag)
        
        viewModel.planInfoObs
            .subscribe(onNext: { [unowned self] descriptionPoints in
                self.setup(with: descriptionPoints)
            }).disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.applyProfileItemAppearance(withTitle: Strings.Upgrade.title.localized.uppercased())
        navigationController?.setNavigationBarBorderColor()
        statusBar?.backgroundColor = .white
        setNeedsStatusBarAppearanceUpdate()
    }

}

// MARK: - Private methods
extension UpgradeMembershipViewController {

    private func setupViews() {
        let builder = UpgradeMembershipViewBuilder(controller: self)
        self.scrollView = builder.buildScrollView()
        self.contentScrollView = builder.buildView()
        self.titleLabel = builder.buildTitleLabel()
        self.messageLabel = builder.buildMessageLabel()
        self.descriptionPlanStackView = builder.buildDescriptionPlanStackView()
        self.applyButton = builder.buildApplyButton()
        self.labelsStack = builder.buildLabelsStack()
        builder.setupViews()
    }

    private func setupPresentationLogic() {
        viewModel.getPremiumPlanInfo()
    }

    private func handle(destination: UpgradeMembershipDestination) {
        switch destination {
        case .premiumPayment:
            navigationController?.pushViewController(destination.viewController, animated: true)
        }
    }
    
    private func setup(with descriptionPoints: [PlanDataDescription]) {
        descriptionPoints.forEach {
            descriptionPlanStackView.addArrangedSubview(DescriptionStackViewItem(description: $0.element).apply { $0.spacing = 7 })
        }
        DispatchQueue.main.asyncAfter(deadline: (.now() + 0.1)) { [unowned self] in
            self.scrollView.flashScrollIndicators()
        }
    }
    
    private func adjustNavigationBar() {
        guard self.presentingViewController != nil else { return }
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: Icons.NavigationBar.close, style: .plain, target: self, action: #selector(closeButtonTapped))
    }

    @objc private func closeButtonTapped() {
        parent?.dismiss()
    }
}
