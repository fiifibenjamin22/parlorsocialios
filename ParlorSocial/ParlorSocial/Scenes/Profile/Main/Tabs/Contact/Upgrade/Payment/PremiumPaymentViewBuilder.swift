//
//  PremiumPaymentViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 26/06/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import PassKit

class PremiumPaymentViewBuilder {

    private unowned let controller: PremiumPaymentViewController
    private var view: UIView! { return controller.view }

    init(controller: PremiumPaymentViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension PremiumPaymentViewBuilder {

    private func setupProperties() {
        view.backgroundColor = .white
        
        controller.apply {
            $0.paymentMethodSelector.backgroundColor = .white
            $0.paymentMethodSelector.isHidden = true
            $0.paymentMethodSelector.headerInformationView.font = UIFont.custom(
                ofSize: Constants.FontSize.body,
                font: UIFont.Roboto.light
            )
            $0.proceedButton.setTitle(Strings.Premium.proceed.localized.uppercased(), for: .normal)
            $0.proceedButton.isHidden = true

            $0.applePayButton.isHidden = true
            $0.revealCardFormButton.isHidden = true
            $0.stripeButton.isHidden = true
            $0.paymentMethodSelector.isHidden = true

            $0.fillCardDataLabel.text = Strings.Cards.Add.appliedFill.localized
            $0.fillCardDataLabel.isHidden = true

            $0.paymentCardFormView.isHidden = true
        }
    }

    private func setupHierarchy() {
        controller.apply {
            view.addSubview($0.scrollView)
            $0.scrollView.addSubview($0.scrollContentView)
            $0.scrollContentView.addSubviews([$0.premiumMembershipLabel, $0.paymentMethodSelector, $0.priceAndInformationStack, $0.bottomButtonsStackView,
                                              $0.stripeButton, $0.middleStackView, $0.cardFormStackView])
            $0.middleStackView.addArrangedSubviews([$0.applePayButton, $0.revealCardFormButton])
            $0.cardFormStackView.addArrangedSubviews([$0.fillCardDataLabel, $0.paymentCardFormView])
            $0.bottomButtonsStackView.addArrangedSubviews([$0.proceedButton])
            $0.priceAndInformationStack.addArrangedSubviews([$0.premiumPriceLabel, $0.priceInformationLabel])
        }
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.scrollView.edgesToParent()
            $0.scrollContentView.edgesToParent()
            $0.scrollContentView.widthAnchor.equal(to: $0.scrollView.widthAnchor)
            $0.scrollContentView.heightAnchor.greaterThanOrEqual(to: view.heightAnchor)

            $0.premiumMembershipLabel.edgesToParent(
                anchors: [.leading, .trailing, .top],
                insets: .margins(top: 80, left: Constants.Margin.standard, right: Constants.Margin.standard)
            )
            
            $0.priceAndInformationStack.edgesToParent(
                anchors: [.leading, .trailing],
                insets: UIEdgeInsets.margins(left: Constants.Margin.standard, right: Constants.Margin.standard)
            )
            $0.priceAndInformationStack.topAnchor.equal(to: $0.premiumMembershipLabel.bottomAnchor, constant: 30)
            $0.priceAndInformationStack.heightAnchor.equalTo(constant: 56)
            
            $0.paymentMethodSelector.edgesToParent(anchors: [.leading, .trailing], padding: Constants.Margin.standard)
            $0.paymentMethodSelector.topAnchor.constraint(equalTo: $0.priceAndInformationStack.bottomAnchor).activate()

            $0.middleStackView.edgesToParent(anchors: [.leading, .trailing], padding: Constants.Margin.standard)
            $0.middleStackView.bottomAnchor.equal(to: $0.cardFormStackView.topAnchor, constant: -31)
            $0.middleStackView.topAnchor.constraint(equalTo: $0.priceAndInformationStack.bottomAnchor, constant: 75).activate()
            
            $0.cardFormStackView.edgesToParent(anchors: [.leading, .trailing], padding: Constants.Margin.standard)
            $0.cardFormStackView.bottomAnchor.lessThanOrEqual(to: $0.bottomButtonsStackView.topAnchor, constant: -42)

            $0.applePayButton.heightAnchor.equalTo(constant: Constants.bigButtonHeight)
            $0.applePayButton.widthAnchor.equal(to: $0.middleStackView.widthAnchor)

            $0.stripeButton.centerXAnchor.equal(to: view.centerXAnchor)
            $0.stripeButton.bottomAnchor.equal(to: $0.scrollContentView.bottomAnchor, constant: -30)
            
            $0.bottomButtonsStackView.edgesToParent(anchors: [.leading, .trailing], padding: Constants.Margin.standard)
            $0.bottomButtonsStackView.bottomAnchor.equal(to: $0.stripeButton.topAnchor, constant: -10)
        }
    }

}

// MARK: - Public build methods
extension PremiumPaymentViewBuilder {
    
    func buildScrollView() -> UIScrollView {
        return UIScrollView().manualLayoutable()
    }

    func buildContentView() -> UIView {
        return UIView().manualLayoutable()
    }

    func buildStackView() -> UIStackView {
        return UIStackView(
            axis: .vertical,
            spacing: 0,
            alignment: .fill,
            distribution: .fill
            ).manualLayoutable()
    }
    
    func buildBottomButtonsStackView() -> UIStackView {
        return UIStackView(
            axis: .vertical,
            spacing: 10,
            alignment: .fill,
            distribution: .fill
        ).manualLayoutable()
    }
    
    func buildTitleLabel() -> ParlorLabel {
        return ParlorLabel.styled(
            withTextColor: .appTextMediumBright,
            withFont: UIFont.custom(ofSize: 31, font: UIFont.Roboto.medium),
            alignment: .center,
            numberOfLines: 1).apply {
                $0.adjustsFontSizeToFitWidth = true
                $0.minimumScaleFactor = CGFloat.leastNonzeroMagnitude
                $0.lineHeight = 30
        }
    }
    
    func buildPriceInformationLabel() -> ParlorLabel {
        return ParlorLabel.styled(
            withTextColor: .appTextLight,
            withFont: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.regular),
            alignment: .left,
            numberOfLines: 0
            ).manualLayoutable()
    }
    
    func buildPriceLabel() -> ParlorLabel {
        return ParlorLabel.styled(
            withTextColor: .appTextMediumBright,
            withFont: UIFont.custom(ofSize: Constants.FontSize.heading, font: UIFont.Roboto.medium),
            alignment: .left,
            numberOfLines: 1
        )
    }
    
    func buildCardSelector() -> DropDownView {
        return DropDownView(controller: controller).manualLayoutable()
    }
    
    func buildProceedButton() -> RoundedButton {
        return RoundedButton().manualLayoutable().apply {
            $0.backgroundColor = .appText
            $0.applyTouchAnimation()
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium)
            $0.setTitleColor(.white, for: .normal)
            $0.heightAnchor.equalTo(constant: Constants.bigButtonHeight)
        }
    }

    func buildStripeButton() -> StripeButton {
        return StripeButton().manualLayoutable()
    }

    func buildMiddleButtonsStackView() -> UIStackView {
        return UIStackView(
            axis: .vertical,
            spacing: 41,
            alignment: .center,
            distribution: .fill
        ).manualLayoutable()
    }

    func buildApplePayButton() -> UIButton {
        let button = PKPaymentButton()
        if #available(iOS 12.0, *) {
            button.cornerRadius = 27
        }
        return button
    }

    func buildPayUsingCardButton() -> UIButton {
        return UIButton().apply {
            $0.applyTouchAnimation()
            $0.setTitleColor(.appText, for: .normal)
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.body, font: UIFont.Roboto.light)
            $0.setAttributedTitle(
                NSAttributedString(string: Strings.Premium.payUsingCard.localized, attributes: [.underlineStyle: NSUnderlineStyle.single.rawValue]),
                for: .normal
            )
        }
    }

    func buildCardFormStackView() -> UIStackView {
        return UIStackView(
            axis: .vertical,
            spacing: 47,
            alignment: .fill,
            distribution: .fill
        ).manualLayoutable()
    }

    func buildFillCardDataLabel() -> ParlorLabel {
        return ParlorLabel.styled(
            withTextColor: .appTextMediumBright,
            withFont: UIFont.custom(ofSize: Constants.FontSize.body, font: UIFont.Roboto.light),
            alignment: .left,
            numberOfLines: 2
        )
    }

    func buildPaymentCardFormView() -> CardFormView {
        return CardFormView()
    }
}
