//
//  UpgradeMembershipViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 07/06/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxSwift

protocol UpgradeMembershipLogic: BaseViewModelLogic {
    var applyClicks: PublishSubject<Void> { get }
    var destinationObs: Observable<UpgradeMembershipDestination> { get }
    var planInfoObs: Observable<[PlanDataDescription]> { get }
    func getPremiumPlanInfo()
}

class UpgradeMembershipViewModel: BaseViewModel {
    let analyticEventReferencedId: Int? = nil
    private let profileRepository = ProfileRepository.shared
    private let destinationSubject: PublishSubject<UpgradeMembershipDestination> = .init()
    private let planInfoSubject: BehaviorSubject<[PlanDataDescription]> = .init(value: [])
    private let premiumPlanDataSubject: PublishSubject<PlanData?> = PublishSubject()
    
    let applyClicks: PublishSubject<Void> = PublishSubject()
    
    init(navigationSource: UpgradeToPremiumNavigationSource) {
        super.init()
        initFlow()
        setAnalyticEventProperties(usingNavigationSource: navigationSource)
    }
    
    private func setAnalyticEventProperties(usingNavigationSource navigationSource: UpgradeToPremiumNavigationSource) {
        switch navigationSource {
        case .activation(let id):
            analyticEventNavigationSourceId = id
            analyticEventNavigationSource = navigationSource.name
        case .profile:
            analyticEventNavigationSource = navigationSource.name
        }
    }
}

// MARK: Private logic
private extension UpgradeMembershipViewModel {
    
    private func initFlow() {
        applyClicks
            .withLatestFrom(premiumPlanDataSubject)
            .subscribe(onNext: { [unowned self] planData in
                self.destinationSubject.onNext(.premiumPayment(plan: planData))
            }).disposed(by: disposeBag)
    }
    
    private func handlePremiumPlanInfo(_ response: PlanInfoResponse) {
        switch response {
        case .success(let dataResponse):
            let data = dataResponse.data
            premiumPlanDataSubject.onNext(data)
            fillAllPointsOfDescription(planData: data)
        case .failure(let error):
            errorSubject.onNext(error)
        }
    }
    
    func fillAllPointsOfDescription(planData: PlanData) {
        let priceString = String(
            format: Strings.Premium.priceInterval.localized,
            planData.price?.getCurrencyStringWithFreeOption(currency: planData.currency) ?? Strings.MembershipPlans.free.localized,
            planData.interval ?? ""
        )
        let proratedPricePointOfDescription = PlanDataDescription(element: Strings.Upgrade.upgradeInformationPoint.localized)
        let pricePointOfDescription = PlanDataDescription(element: priceString)
        planInfoSubject.onNext([pricePointOfDescription, proratedPricePointOfDescription] + (planData.description ?? []))
    }
}

// MARK: Interface logic methods
extension UpgradeMembershipViewModel: UpgradeMembershipLogic {
    var planInfoObs: Observable<[PlanDataDescription]> {
        return planInfoSubject.asObservable()
    }
    
    var destinationObs: Observable<UpgradeMembershipDestination> {
        return destinationSubject.asObservable()
    }
    
    func getPremiumPlanInfo() {
        profileRepository.getPremiumPlanInfo().showingProgressBar(with: self)
            .subscribe(onNext: { [unowned self] response in
                self.handlePremiumPlanInfo(response)
            }).disposed(by: disposeBag)
    }
}
