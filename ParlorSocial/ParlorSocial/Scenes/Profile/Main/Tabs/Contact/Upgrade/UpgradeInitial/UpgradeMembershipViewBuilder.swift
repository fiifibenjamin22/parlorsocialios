//
//  UpgradeMembershipViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 07/06/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class UpgradeMembershipViewBuilder {

    private unowned let controller: UpgradeMembershipViewController
    private var view: UIView! { return controller.view }

    init(controller: UpgradeMembershipViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension UpgradeMembershipViewBuilder {

    private func setupProperties() {
        view.backgroundColor = .white
        controller.apply {
            $0.titleLabel.text = Strings.Upgrade.standardTitle.localized
            $0.messageLabel.text = Strings.Upgrade.message.localized
            $0.applyButton.setTitle(Strings.Upgrade.apply.localized.uppercased(), for: .normal)
            $0.extendedLayoutIncludesOpaqueBars = true
        }
    }

    private func setupHierarchy() {
        controller.apply {
            view.addSubviews([$0.scrollView, $0.applyButton])
            $0.scrollView.addSubviews([$0.contentScrollView])
            $0.contentScrollView.addSubviews([$0.labelsStack, $0.descriptionPlanStackView])
            $0.labelsStack.addArrangedSubviews([$0.titleLabel, $0.messageLabel])
        }
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.scrollView.edgesToParent(anchors: [.top, .leading, .trailing])
            $0.scrollView.bottomAnchor.equal(to: $0.applyButton.topAnchor, constant: -Constants.Margin.standard)
            
            $0.contentScrollView.edgesToParent()
            $0.contentScrollView.widthAnchor.equal(to: $0.scrollView.widthAnchor)
            
            $0.descriptionPlanStackView.edgesToParent(
                anchors: [.leading, .trailing],
                insets: .margins(
                    left: Constants.Margin.large,
                    right: Constants.Margin.large
                )
            )
            $0.descriptionPlanStackView.topAnchor.equal(to: $0.labelsStack.bottomAnchor, constant: 40)
            $0.descriptionPlanStackView.bottomAnchor.equal(to: $0.contentScrollView.bottomAnchor)
            
            $0.labelsStack.edgesToParent(
                anchors: [.top, .trailing, .leading],
                insets: .margins(top: 33, left: Constants.Margin.large, right: Constants.Margin.large)
            )
            
            $0.applyButton.edgesToParent(
                anchors: [.bottom, .trailing, .leading],
                insets: .margins(left: Constants.Margin.standard, bottom: 35, right: Constants.Margin.standard)
            )
        }
    }

}

// MARK: - Public build methods
extension UpgradeMembershipViewBuilder {
    
    func buildScrollView() -> UIScrollView {
        return UIScrollView().manualLayoutable()
    }
    
    func buildView() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildTitleLabel() -> ParlorLabel {
        return ParlorLabel.styled(
            withTextColor: .appTextMediumBright,
            withFont: UIFont.custom(ofSize: Constants.FontSize.mainMessage, font: UIFont.Roboto.medium),
            alignment: .left,
            numberOfLines: 0).apply {
                $0.lineHeight = 30
            }
    }
    
    func buildMessageLabel() -> ParlorLabel {
        return ParlorLabel.styled(
            withTextColor: .appTextMediumBright,
            withFont: UIFont.custom(ofSize: Constants.FontSize.body, font: UIFont.Roboto.light),
            alignment: .left,
            numberOfLines: 0).apply {
                $0.lineHeight = 24
            }
    }
    
    func buildDescriptionPlanStackView() -> UIStackView {
        return UIStackView(
            axis: .vertical,
            spacing: 0,
            alignment: .fill,
            distribution: .fill
            ).manualLayoutable()
    }
    
    func buildLabelsStack() -> UIStackView {
        return UIStackView(
            axis: .vertical,
            with: [],
            spacing: Constants.Margin.standard,
            alignment: .fill,
            distribution: .equalSpacing
            ).manualLayoutable()
    }
    
    func buildApplyButton() -> RoundedButton {
        return RoundedButton.withBackgroundStyle().manualLayoutable()
    }
    
}
