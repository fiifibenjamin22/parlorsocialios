//
//  UpgradeMembershipDestination.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 22/11/2019.
//  Copyright © 2019 KISSdigital. All rights reserved.
//

import UIKit

enum UpgradeMembershipDestination {
    case premiumPayment(plan: PlanData?)
}

extension UpgradeMembershipDestination: Destination {

    var viewController: UIViewController {
        switch self {
        case .premiumPayment(let plan):
            return PremiumPaymentViewController(membershipPlan: plan)
        }
    }

}

