//
//  PremiumPaymentViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 26/06/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift
import PassKit

class PremiumPaymentViewController: AppViewController {
    // MARK: - Properties
    let analyticEventScreen: AnalyticEventScreen? = .payment
    private(set) var scrollView: UIScrollView!
    private(set) var scrollContentView: UIView!
    private(set) var premiumMembershipLabel: ParlorLabel!
    private(set) var priceAndInformationStack: UIStackView!
    private(set) var premiumPriceLabel: ParlorLabel!
    private(set) var priceInformationLabel: ParlorLabel!
    private(set) var paymentMethodSelector: DropDownView!
    private(set) var middleStackView: UIStackView!
    private(set) var applePayButton: UIButton!
    private(set) var revealCardFormButton: UIButton!
    private(set) var cardFormStackView: UIStackView!
    private(set) var fillCardDataLabel: ParlorLabel!
    private(set) var paymentCardFormView: CardFormView!
    private(set) var bottomButtonsStackView: UIStackView!
    private(set) var proceedButton: RoundedButton!
    private(set) var stripeButton: StripeButton!
    
    private var viewModel: PremiumPaymentLogic!
    
    override var observableSources: ObservableSources? {
        return viewModel
    }

    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }
    
    // MARK: - Initialization
    
    init(membershipPlan: PlanData? = nil) {
        super.init(nibName: nil, bundle: nil)
        setup(membershipPlan: membershipPlan)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError()
    }

    private func setup(membershipPlan: PlanData? = nil) {
        self.viewModel = PremiumPaymentViewModel(membershipPlan: membershipPlan, applePayPresentationController: self)
    }
    
    override func bindUI() {
        super.bindUI()
        proceedButton.rx.controlEvent(.touchUpInside)
            .bind(to: viewModel.payClicks)
            .disposed(by: disposeBag)

        revealCardFormButton.rx.controlEvent(.touchUpInside)
            .bind(to: viewModel.revealCardFormClicks)
            .disposed(by: disposeBag)
        
        applePayButton.rx.controlEvent(.touchUpInside)
            .subscribe(onNext: { [unowned self] in self.viewModel.payUsingApplePay() })
            .disposed(by: disposeBag)
        
        paymentCardFormView.stringFieldsToTypeMap.forEach { (key, value) in
            key.bindToStringCardField(ofType: value, to: viewModel.stringFieldChangeRelay).disposed(by: disposeBag)
        }

        paymentCardFormView.intFieldsToTypeMap.forEach { (key, value) in
            key.bindToIntCardField(ofType: value, to: viewModel.intFieldChangeRelay).disposed(by: disposeBag)
        }
        
        paymentMethodSelector.selectionChangedCallback = { [weak self] index in
            self?.viewModel.paymentMethodIndexChangedSubject.onNext(index)
        }
    }
    
    override func observeViewModel() {
        super.observeViewModel()

        viewModel
            .destinationObservable
            .doOnNext { [unowned self] destination in
                self.handleDestination(destination)
            }.subscribe().disposed(by: disposeBag)
        
        viewModel.paymentMethodOptionsObs
            .subscribe(
                onNext: { [unowned self] (paymentMethods, selectedMethod) in
                    self.paymentMethodSelector.configure(
                        with: DropDownData(
                            icon: Icons.GuestsForm.card,
                            title: Strings.Premium.payWith.localized,
                            popupTitle: Strings.Premium.creditCard.localized.uppercased(),
                            options: paymentMethods,
                            popupAddBottomButtonTitle: Strings.Cards.addAnother.localized.uppercased()))
                    self.setContentVisible(hasCards: !paymentMethods.filter { $0.type == .card }.isEmpty)
                    self.paymentMethodSelector.selectedOption = selectedMethod
                    self.paymentMethodSelector.addOptionInPopupCallback = { [unowned self] in self.viewModel.navigateToAddCardFromPopup() }
                },
                onError: { error in print(error) }
            ).disposed(by: disposeBag)
        
        viewModel.planData
            .ignoreNil()
            .doOnNext { [unowned self] planData in self.update(withData: planData) }
            .subscribe()
            .disposed(by: disposeBag)
        
        viewModel.closeObs
            .doOnNext { [unowned self] _ in self.navigationController?.popViewController(animated: true) }
            .subscribe()
            .disposed(by: disposeBag)
        
        viewModel.cardFormVisibilityObs
            .subscribe(onNext: { [unowned self] visible, hasCards in
                self.revealCardFormButton.isHidden = visible
                UIView.animate(withDuration: 0.25, animations: {
                    self.fillCardDataLabel.isHidden = !visible
                    self.paymentCardFormView.isHidden = !visible
                }, completion: { _ in
                    self.proceedButton.isHidden = hasCards ? false : !visible
                })
            })
            .disposed(by: disposeBag)
    }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.applyProfileItemAppearance(withTitle: Strings.Upgrade.paymentTitle.localized.uppercased())
        navigationController?.setNavigationBarBorderColor()
        statusBar?.backgroundColor = .white
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.initFlow()
    }
    
    private func update(withData planData: PlanData) {
        premiumMembershipLabel.text = String(
            format: Strings.Premium.membership.localized,
            (planData.isPremium ?? false) ? Strings.Premium.premium.localized : Strings.Premium.standard.localized
        )
        premiumPriceLabel.text = planData.proratedPriceText
        priceInformationLabel.isHidden = planData.priceInformation?.isEmpty ?? true
        priceInformationLabel.text = planData.priceInformation
    }
    
}

// MARK: - Private methods
extension PremiumPaymentViewController {
    
    private func setContentVisible(hasCards: Bool) {
        [paymentMethodSelector, proceedButton].forEach { $0?.isHidden = !hasCards }
        revealCardFormButton.isHidden = hasCards
        applePayButton.isHidden = hasCards
        stripeButton.isHidden = false
    }
    
    private func handleDestination(_ destination: CardDestination) {
        switch destination {
        case .add:
            guard let currentController = navigationController?.viewControllers else { return }
            navigationController?.setViewControllers(currentController.dropLast() + [destination.viewController], animated: false)
        case .resetToHome:
            router.replace(with: destination)
        case .close:
            navigationController?.popViewController(animated: true)
        case .addCardFromPopup:
            router.push(destination: destination)
        default:
            break
        }
    }

    private func setupViews() {
        let builder = PremiumPaymentViewBuilder(controller: self)
        self.scrollView = builder.buildScrollView()
        self.scrollContentView = builder.buildContentView()
        self.premiumMembershipLabel = builder.buildTitleLabel()
        self.priceAndInformationStack = builder.buildStackView()
        self.premiumPriceLabel = builder.buildPriceLabel()
        self.priceInformationLabel = builder.buildPriceInformationLabel()
        self.paymentMethodSelector = builder.buildCardSelector()
        self.middleStackView = builder.buildMiddleButtonsStackView()
        self.applePayButton = builder.buildApplePayButton()
        self.revealCardFormButton = builder.buildPayUsingCardButton()
        self.cardFormStackView = builder.buildCardFormStackView()
        self.fillCardDataLabel = builder.buildFillCardDataLabel()
        self.paymentCardFormView = builder.buildPaymentCardFormView()
        self.bottomButtonsStackView = builder.buildBottomButtonsStackView()
        self.proceedButton = builder.buildProceedButton()
        self.stripeButton = builder.buildStripeButton()
        builder.setupViews()
    }

}
