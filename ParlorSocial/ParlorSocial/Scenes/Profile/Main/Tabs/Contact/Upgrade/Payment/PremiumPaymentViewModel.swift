//
//  PremiumPaymentViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 26/06/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxSwiftExt
import PassKit

protocol PremiumPaymentLogic: BaseViewModelLogic {
    var paymentMethodOptionsObs: Observable<([PaymentMethodOption], PaymentMethodOption?)> { get }
    var planData: Observable<PlanData?> { get }
    var paymentMethodIndexChangedSubject: PublishSubject<Int> { get }
    var payClicks: PublishSubject<Void> { get }
    var revealCardFormClicks: PublishSubject<Void> { get }
    var cardFormVisibilityObs: Observable<(visible: Bool, hasCards: Bool)> { get }
    var destinationObservable: Observable<CardDestination> { get }
    var stringFieldChangeRelay: PublishSubject<(CardUploadStringField, String)> { get }
    var intFieldChangeRelay: PublishSubject<(CardUploadIntField, Int?)> { get }
    func navigateToAddCardFromPopup()
    func initFlow()
    func payUsingApplePay()
}

class PremiumPaymentViewModel: BaseViewModel {
    let analyticEventReferencedId: Int? = nil
    private let profileRepository = ProfileRepository.shared
    private let cardManagementService = CardManagementService()
    private var applePayManager: ApplePayManager!
    private let facebookAnalyticsService: FacebookAnalyticService = .init()
    private let firebaseAnalyticService: FirebaseAnalyticService = FirebaseAnalyticService()
    
    private let cardsDestination: PublishSubject<CardDestination> = PublishSubject()
    private let selectedPaymentMethodSubject: ReplaySubject<PaymentMethodOption?> = ReplaySubject.create(bufferSize: 1)
    private let planDataRelay: BehaviorRelay<PlanData?> = .init(value: nil)
    private let cardsSubject: ReplaySubject<[CardData]> = ReplaySubject.create(bufferSize: 1)
    private let cardUploadDataSubject: BehaviorRelay<CardUploadData> = BehaviorRelay(value: .empty())
    private let cardFormVisibleSubject: PublishSubject<(visible: Bool, hasCards: Bool)> = PublishSubject()
    private let paymentMethodOptionsSubject: PublishSubject<[PaymentMethodOption]> = PublishSubject()

    let payClicks: PublishSubject<Void> = PublishSubject()
    let revealCardFormClicks: PublishSubject<Void> = PublishSubject()
    let paymentMethodIndexChangedSubject: PublishSubject<Int> = PublishSubject()
    let stringFieldChangeRelay: PublishSubject<(CardUploadStringField, String)> = PublishSubject()
    let intFieldChangeRelay: PublishSubject<(CardUploadIntField, Int?)> = PublishSubject()
    
    init(membershipPlan: PlanData? = nil, applePayPresentationController: UIViewController) {
        if let plan = membershipPlan {
            planDataRelay.accept(plan)
        }
        super.init()
        
        applePayManager = ApplePayManager(presentationController: applePayPresentationController, delegate: self)
    }
}

// MARK: Private logic
private extension PremiumPaymentViewModel {
    private func payWithPossiblePaymentMethod(usingCardId cardId: Int?, andPlanId planId: Int) -> Observable<MessageResponse> {
        if let cardId = cardId {
            return pay(usingCardId: cardId, andPlanId: planId)
                .filterMapWithErrorHandling(errorSubject: errorSubject)
        } else {
            return addCardAndPay(data: cardUploadDataSubject.value, usingPlanId: planId)
                .filterMap { [unowned self] response in
                    switch response {
                    case .success(let item):
                        return .map(item)
                    case .failure(let error):
                        self.errorSubject.onNext(error)
                        self.refreshCards()
                        return .ignore
                    }
            }
        }
    }

    private func pay(usingCardId cardId: Int, andPlanId planId: Int) -> MessageApiResponse {
        return self.profileRepository.subscribeToPremium(usingCardWithId: cardId, planId: planId).showingProgressBar(with: self)
    }

    private func addCardAndPay(data: CardUploadData, usingPlanId planId: Int) -> MessageApiResponse {
        do {
            return try cardManagementService.addCard(data: data)
                .showingProgressBar(with: self)
                .filterMapWithErrorHandling(errorSubject: errorSubject)
                .flatMapFirst { [unowned self] card in
                    self.pay(usingCardId: card.id, andPlanId: planId)
            }
        } catch let error as AppError {
            errorSubject.onNext(error)
            return Observable.just(ApiResponse.failure(error))
        } catch {
            let appError = AppError(with: error)
            errorSubject.onNext(appError)
            return Observable.just(ApiResponse.failure(appError))
        }
    }

    private func refreshCards() {
        profileRepository.getUserCards()
            .showingProgressBar(with: self)
            .filterMap { response in
                switch response {
                case .success(let cards): return .map(cards.data)
                case .failure: return .ignore
                }
            }
            .doOnNext { [unowned self] cards in
                self.cardFormVisibleSubject.onNext((visible: cards.isEmpty, hasCards: !cards.isEmpty))
            }
            .bind(to: cardsSubject)
            .disposed(by: self.disposeBag)
    }

    private func setDefaultPaymentMethod(usingPaymentMethods paymentMethods: [PaymentMethodOption]) {
        let defaultMethod = paymentMethods.first(where: { $0.isDefault })
        selectedPaymentMethodSubject.onNext(defaultMethod)
    }
    
    private func finalizePayment(paymentMethodType: AnalyticEventPaymentMethodType) {
        guard let planData = planDataRelay.value else { return }
        logSuccessPaymentAnalyticEvents(usingPlanData: planData, paymentMethodType: paymentMethodType)
        cardsDestination.onNext(.resetToHome)
    }
    
    private func logSuccessPaymentAnalyticEvents(usingPlanData data: PlanData, paymentMethodType: AnalyticEventPaymentMethodType) {
        facebookAnalyticsService.logMembershipPurchasedEvent(membershipPlan: data)
        firebaseAnalyticService.logMembershipPurchasedEvent(membershipPlan: data)
        logMembershipSucbscriptionPurchased(usingPaymentMethod: paymentMethodType, andPlanId: data.id)
    }
    
    private func logMembershipSucbscriptionPurchased(usingPaymentMethod payemntMethod: AnalyticEventPaymentMethodType, andPlanId planId: Int) {
        guard let sessionId = AnalyticService.shared.sessionId, let userId = Config.userProfile?.id else { return }
        let event = AnalyticEvent(
            userId: userId,
            deviceToken: Config.deviceToken,
            timestamp: Int64(Date().timeIntervalSince1970 * 1000),
            type: .membershipSubscriptionPurchased,
            data: .createForSpecialType(referencedId: planId, source: payemntMethod.rawValue),
            sessionId: sessionId
        )
        AnalyticService.shared.saveEventInDatabase(event)
    }
    
    private func loadCards() {
        profileRepository.getUserCards()
            .showingProgressBar(with: self)
            .filterMapWithErrorHandling(errorSubject: errorSubject)
            .bind(to: cardsSubject)
            .disposed(by: disposeBag)
    }
    
    private func loadPlanWithCards() {
        Observable.combineLatest(profileRepository.getPlanInfo(), profileRepository.getUserCards())
            .showingProgressBar(with: self)
            .filterMap { [unowned self] (planResponse, cardsResponse) -> FilterMap<(PlanData, [CardData])> in
                switch (planResponse, cardsResponse) {
                case (.success(let plan), .success(let cards)):
                    return .map((plan.data, cards.data))
                case (.failure(let error), _), (_, .failure(let error)):
                    self.errorSubject.onNext(error)
                    return .ignore
                }
            }
            .subscribe(onNext: { [unowned self] (planData, cardsData) in
                self.cardsSubject.onNext(cardsData)
                self.planDataRelay.accept(planData)
            })
            .disposed(by: disposeBag)
    }
}

// MARK: Interface logic methods
extension PremiumPaymentViewModel: PremiumPaymentLogic {
    var cardFormVisibilityObs: Observable<(visible: Bool, hasCards: Bool)> {
        return cardFormVisibleSubject.asObservable()
    }

    var destinationObservable: Observable<CardDestination> {
        return cardsDestination.asObservable()
    }
    
    var planData: Observable<PlanData?> {
        return planDataRelay.asObservable()
    }
    
    var paymentMethodOptionsObs: Observable<([PaymentMethodOption], PaymentMethodOption?)> {
        return paymentMethodOptionsSubject
            .withLatestFrom(selectedPaymentMethodSubject) { ($0, $1) }
            .asObservable()
    }
    
    func navigateToAddCardFromPopup() {
        cardsDestination.onNext(.addCardFromPopup)
    }
    
    func initFlow() {
        if planDataRelay.value == nil {
            loadPlanWithCards()
        } else {
            loadCards()
        }
        
        paymentMethodIndexChangedSubject.withLatestFrom(paymentMethodOptionsSubject) { ($0, $1) }
            .map { (selectedIndex, paymentMethods) in return paymentMethods[selectedIndex] }
            .bind(to: selectedPaymentMethodSubject)
            .disposed(by: disposeBag)
        
        payClicks
            .withLatestFrom(planDataRelay)
            .ignoreNil()
            .withLatestFrom(selectedPaymentMethodSubject) { ($0, $1) }
            .filterMap { [unowned self] (selectedPlan: PlanData, selectedPaymentMethod: PaymentMethodOption?) -> FilterMap<(PlanData, PaymentMethodOption?)> in
                if selectedPaymentMethod?.type == .applePay {
                    self.payUsingApplePay()
                    return .ignore
                } else {
                    return .map((selectedPlan, selectedPaymentMethod))
                }
            }
            .flatMapFirst { [unowned self] tuple -> Observable<MessageResponse> in
                let (selectedPlan, selectedPaymentMethod) = tuple
                return self.payWithPossiblePaymentMethod(usingCardId: selectedPaymentMethod?.id, andPlanId: selectedPlan.id)
            }
            .doOnNext { [unowned self] _ in
                self.finalizePayment(paymentMethodType: .card)
            }
            .subscribe()
            .disposed(by: disposeBag)
        
        revealCardFormClicks
            .subscribe(onNext: { [unowned self] in
                self.cardFormVisibleSubject.onNext((visible: true, hasCards: false))
            })
            .disposed(by: disposeBag)

        stringFieldChangeRelay
            .withLatestFrom(cardUploadDataSubject) { ($0, $1) }
            .map { stringFieldChangeData, currentData in
                currentData.withStringField(ofType: stringFieldChangeData.0, value: stringFieldChangeData.1)
            }.bind(to: cardUploadDataSubject).disposed(by: disposeBag)

        intFieldChangeRelay
            .withLatestFrom(cardUploadDataSubject) { ($0, $1) }
            .map { intFieldData, currentData in
                return currentData.withIntField(ofType: intFieldData.0, value: intFieldData.1)
            }.bind(to: cardUploadDataSubject).disposed(by: disposeBag)
        
        cardsSubject
            .bind(onNext: { [unowned self] cards in
                var paymentMethods: [PaymentMethodOption] = cards
                paymentMethods.insert(ApplePayPaymentMethodOption(), at: 0)
                self.setDefaultPaymentMethod(usingPaymentMethods: paymentMethods)
                self.paymentMethodOptionsSubject.onNext(paymentMethods)
            })
            .disposed(by: disposeBag)
    }
    
    func payUsingApplePay() {
        guard let planData = planDataRelay.value, let price = planData.price, let currencyCode = planData.currency?.code else {
            alertDataSubject.onNext(AlertData.applePayErrorAlert(withTitle: Strings.ApplePay.unexpectedError.localized))
            return
        }
        let paymentItems = ApplePayPaymentSummaryItemsProvider.createPaymentItemsForPayment(usingPlanData: planData, price: price)
        let paymentData = ApplePayPaymentData(currencyCode: currencyCode, paymentItems: paymentItems)
        do {
            try applePayManager.makePayment(usingData: paymentData)
        } catch ApplePayError.notAvailable {
            alertDataSubject.onNext(AlertData.applePayErrorAlert(withTitle: ApplePayError.notAvailable.localizedDescription))
        } catch {
            alertDataSubject.onNext(AlertData.applePayErrorAlert(withTitle: Strings.ApplePay.notAvailable.localized))
        }
    }
}

// MARK: - ApplePayManagerDelegate

extension PremiumPaymentViewModel: ApplePayManagerDelegate {
    func applePayManager(_ manager: ApplePayManager, didFinishWith response: StripeTokenResponse, paymentCompletionHandler completion: @escaping (PKPaymentAuthorizationStatus) -> Void) {
        if let token = response.token {
            let tokenRequest = CardTokenRequest(tokenId: token.tokenId)
            cardManagementService.addCard(usingToken: tokenRequest)
                .filterMap { [unowned self] result -> FilterMap<CardData> in
                    switch result {
                    case .success(let item):
                        return .map(item.data)
                    case .failure(let error):
                        completion(.failure)
                        self.applePayManager.paymentErrorToPresent = error
                        return .ignore
                    }
                }
                .flatMapFirst { [unowned self] card in
                    return self.pay(usingCardId: card.id, andPlanId: self.planDataRelay.value!.id)
                }
                .filterMap { [unowned self] result -> FilterMap<MessageResponse> in
                    switch result {
                    case .success(let item):
                        return .map(item)
                    case .failure(let error):
                        completion(.failure)
                        self.applePayManager.paymentErrorToPresent = error
                        return .ignore
                    }
                }
                .subscribe(onNext: { [unowned self] _ in
                    completion(.success)
                    self.finalizePayment(paymentMethodType: .applePay)
                })
                .disposed(by: disposeBag)
            
        } else {
            completion(.failure)
            applePayManager.paymentErrorToPresent = AppError(with: response.error)
        }
    }
    
    func applePayManagerDidDismissPaymentController(_ manager: ApplePayManager) {
        if let error = manager.paymentErrorToPresent {
            errorSubject.onNext(error)
        }
    }
}
