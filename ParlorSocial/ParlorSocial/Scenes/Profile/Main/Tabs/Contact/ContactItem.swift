//
//  ContactItem.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 31/05/2019.
//

import Foundation
import UIKit

enum ContactItem {
    case referFriend
    case upgradeToPremium
    case emailUs
    case suggestEventsOrVenues
}

extension ContactItem {
    
    var title: String {
        switch self {
        case .referFriend:
            return Strings.Profile.referFriend.localized
        case .upgradeToPremium:
            return Strings.Profile.upgradeToPremium.localized
        case .emailUs:
            return Strings.Profile.emailUs.localized
        case .suggestEventsOrVenues:
            return Strings.Profile.suggestEventsOrVenues.localized
        }
    }
    
    var icon: UIImage {
        switch self {
        case .referFriend:
            return Icons.Profile.referFriend
        case .upgradeToPremium:
            return Icons.Profile.upgradeToPremium
        case .emailUs:
            return Icons.Profile.emailUs
        case .suggestEventsOrVenues:
            return Icons.Profile.emailUs
        }
    }
    
    var profileDestination: ProfileDestination {
        switch self {
        case .referFriend:
            return .referFriend
        case .upgradeToPremium:
            return .upgrade
        case .emailUs:
            return .emailUs
        case .suggestEventsOrVenues:
            return .suggestEventsOrVenues
        }
    }
    
}
