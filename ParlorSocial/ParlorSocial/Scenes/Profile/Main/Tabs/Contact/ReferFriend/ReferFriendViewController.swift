//
//  ReferFriendViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 31/05/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

class ReferFriendViewController: AppViewController {
    
    // MARK: - Views
    private(set) var mainScroll: UIScrollView!
    private(set) var scrollContent: UIView!
    private(set) var itemsStack: UIStackView!
    private(set) var titleItemsStack: UIStackView!
    
    private(set) var itemsTitleStack: UIStackView!
    private(set) var friendIcon: UIImageView!
    private(set) var friendLabel: UILabel!
    private(set) var removeLabel: UILabel!
    private(set) var firstNameField: ParlorTextField!
    private(set) var lastNameField: ParlorTextField!
    private(set) var emailField: ParlorTextField!
    
    private(set) var flexibleSpaceView: UIView!

    private(set) var referButtonArea: UIView!
    private(set) var referButton: RoundedButton!

    // MARK: - Properties
    private var viewModel: ReferFriendLogic!
    let analyticEventScreen: AnalyticEventScreen? = .referAFriend
       
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }
    
    private lazy var stringFieldsToTypeMap: [ParlorTextField: ReferFriendStringField] = {
        return [
            firstNameField: .firstName,
            lastNameField: .lastName,
            emailField: .email
        ]
    }()
    
    override var observableSources: ObservableSources? {
        return viewModel
    }

    // MARK: - Initialization
    init() {
        super.init(nibName: nil, bundle: nil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() {
        self.viewModel = ReferFriendViewModel()
    }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        mainScroll.contentInsetAdjustmentBehavior = .never
        setupPresentationLogic()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        adjustNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func bindUI() {
        super.bindUI()
        
        stringFieldsToTypeMap.forEach { (key, value) in
            key.bindToReferFriendField(ofType: value, to: viewModel.stringFieldChangeRelay).disposed(by: disposeBag)
        }
        
        referButton.rx.controlEvent(.touchUpInside)
            .bind(to: viewModel.inviteClicks)
            .disposed(by: disposeBag)
        
    }
    
    override func observeViewModel() {
        super.observeViewModel()
        
        viewModel.destinationObs
            .subscribe(onNext: { [unowned self] destination in
                self.handle(destination: destination)
            }).disposed(by: disposeBag)
    }
    
    private func handle(destination: ReferFriendDestination) {
        switch destination {
        case .back:
            dismiss()
        }
    }
    
    private func adjustNavigationBar() {
        statusBar?.backgroundColor = .white
        navigationController?.applyProfileItemAppearance(withTitle: Strings.Profile.referFriend.localized.uppercased())
        navigationController?.setNavigationBarBorderColor()
        setNeedsStatusBarAppearanceUpdate()
    }

}

// MARK: - Private methods
extension ReferFriendViewController {

    private func setupViews() {
        let builder = ReferFriendViewBuilder(controller: self)
        self.mainScroll = builder.buildScrollView()
        self.scrollContent = builder.buildContentView()
        self.titleItemsStack = builder.buildTitleItemsStack()
        self.removeLabel = builder.buildRemoveLabel()
        self.itemsStack = builder.buildItemsStack()
        self.itemsTitleStack = builder.builditemTitleStack()
        self.emailField = builder.buildTextField()
        self.firstNameField = builder.buildTextField()
        self.lastNameField = builder.buildTextField()
        self.referButtonArea = builder.buildReferButtonArea()
        self.referButton = builder.buildReferButton()
        self.flexibleSpaceView = builder.buildFlexibleSpaceView()
        builder.setupViews()
    }

    private func setupPresentationLogic() { }
    
}

fileprivate extension UITextField {
    func bindToReferFriendField(
        ofType type: ReferFriendStringField,
        to destination: PublishSubject<(ReferFriendStringField, String)>
        ) -> Disposable {
        return self.rx.text
            .emptyOnNil()
            .map { (type, $0) }
            .bind(to: destination)
    }
}
