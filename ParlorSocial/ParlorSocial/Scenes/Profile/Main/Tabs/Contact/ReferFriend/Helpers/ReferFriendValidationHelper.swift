//
//  ReferFriendValidationHelper.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 04/02/2020.
//

import Foundation

class ReferFriendValidationHelper {
    
    // MARK: - Execution
    func validate(referFriendData: ReferFriendData, validator: Validator) -> Validator.Result {
        let firstNameValidatedInput = ValidatedInput(rules: [StringNotBlank(), StringMaxLength(100)], value: referFriendData.firstName, name: Strings.ReferFriend.firstName.localized)
        let lastNameValidatedInput = ValidatedInput(rules: [StringNotBlank(), StringMaxLength(100)], value: referFriendData.lastName, name: Strings.ReferFriend.lastName.localized)
        let emailValidatedInput = ValidatedInput(rules: [StringNotBlank()], value: referFriendData.email, name: Strings.ReferFriend.email.localized)
        
        return validator.validateWithResult(for: [firstNameValidatedInput, lastNameValidatedInput, emailValidatedInput])
    }
    
}
