//
//  ReferFriendDestination.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 09/07/2019.
//

import Foundation
import UIKit

enum ReferFriendDestination {
    case back
}

extension ReferFriendDestination: Destination {
    
    var viewController: UIViewController {
        switch self {
        case .back:
            return UIViewController()
        }
    }
}
