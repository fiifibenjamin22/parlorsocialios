//
//  ReferFriendViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 31/05/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxSwift

protocol ReferFriendLogic: BaseViewModelLogic {
    var stringFieldChangeRelay: PublishSubject<(ReferFriendStringField, String)> { get }
    var inviteClicks: PublishSubject<Void> { get }
    var destinationObs: Observable<ReferFriendDestination> { get }
}

class ReferFriendViewModel: BaseViewModel {
    
    // MARK: - Properties
    let analyticEventReferencedId: Int? = nil
    private let profileRepository = ProfileRepository.shared
    private let referFriendDataSubject: BehaviorSubject<ReferFriendData> = BehaviorSubject(value: ReferFriendData.empty())
    private let destinationSubject: PublishSubject<ReferFriendDestination> = PublishSubject()
    private let validator = Validator()
    private let referFriendValidationHelper = ReferFriendValidationHelper()
    
    var stringFieldChangeRelay: PublishSubject<(ReferFriendStringField, String)> = PublishSubject()
    let inviteClicks: PublishSubject<Void> = PublishSubject()
    
    // MARK: - Initialization
    override init() {
        super.init()
        initFlow()
    }
}

// MARK: Private logic
private extension ReferFriendViewModel {
    private func initFlow() {
        stringFieldChangeRelay
            .withLatestFrom(referFriendDataSubject) { ($0, $1) }
            .map { stringFieldChangeData, currentData in
                currentData.withStringField(ofType: stringFieldChangeData.0, value: stringFieldChangeData.1)
            }.bind(to: referFriendDataSubject).disposed(by: disposeBag)
        
        inviteClicks
            .withLatestFrom(referFriendDataSubject)
            .subscribe(onNext: { [unowned self] data in
                self.validateReferFriendData(data)
            }).disposed(by: disposeBag)
    }
    
    private func sendReferFriendRequest(referFriendData: ReferFriendData) {
        profileRepository.inviteFriend(with: referFriendData).showingProgressBar(with: self)
            .subscribe(onNext: { [unowned self] response in
                self.handleResponse(response)
            }).disposed(by: disposeBag)
    }
    
    private func handleResponse(_ response: ApiResponse<MessageResponse>) {
        switch response {
        case .success:
            alertDataSubject.onNext(AlertData(
                icon: Icons.Profile.referFriendSuccess,
                title: Strings.Profile.referFriendSuccess.localized, message: nil,
                bottomButtonTitle: Strings.done.localized.uppercased(),
                buttonAction: { [unowned self] in self.destinationSubject.onNext(.back) }))
        case .failure(let error):
            errorSubject.onNext(error)
        }
    }
    
    private func validateReferFriendData(_ referFriendData: ReferFriendData) {
        let result = referFriendValidationHelper.validate(referFriendData: referFriendData, validator: validator)
        switch result {
        case .success:
            sendReferFriendRequest(referFriendData: referFriendData)
        case .failure(let error):
            errorSubject.onNext(error)
        }
    }
}

// MARK: Interface logic methods
extension ReferFriendViewModel: ReferFriendLogic {
    
    var destinationObs: Observable<ReferFriendDestination> {
        return destinationSubject.asObservable()
    }
    
}
