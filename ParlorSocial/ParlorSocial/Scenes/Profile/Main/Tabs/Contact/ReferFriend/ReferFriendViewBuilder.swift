//
//  ReferFriendViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 31/05/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class ReferFriendViewBuilder {

    private unowned let controller: ReferFriendViewController
    private var view: UIView! { return controller.view }

    init(controller: ReferFriendViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension ReferFriendViewBuilder {

    private func setupProperties() {
        view.backgroundColor = .white
        controller.apply {
            $0.firstNameField.placeholder = Strings.ReferFriend.firstName.localized
            $0.lastNameField.placeholder = Strings.ReferFriend.lastName.localized
            $0.referButton.setTitle(Strings.ReferFriend.invite.localized.uppercased(), for: .normal)
            [$0.firstNameField, $0.lastNameField].forEach {
                $0?.sideBorderColor = .textVeryLight
                $0?.autocorrectionType = .no
            }
        }
    }

    private func setupHierarchy() {
        controller.apply {
            
            view.addSubview($0.mainScroll)
            $0.mainScroll.addSubview($0.scrollContent)
            $0.scrollContent.addSubviews([$0.titleItemsStack,$0.referButtonArea])
            $0.referButtonArea.addSubviews([$0.flexibleSpaceView,$0.itemsStack,$0.referButton])
            //$0.referButtonArea.addSubviews([$0.flexibleSpaceView, $0.referButton])
            
            $0.titleItemsStack.addArrangedSubviews([
                buildTitleLabel(),
                transparentSeperator(withHeight: 20),
                buildSubTitle()
            ])
            
            $0.itemsTitleStack.addArrangedSubviews([
                buildFriendLabelIcon(),
                buildRemoveLabel()
            ])
            
            $0.itemsStack.addArrangedSubviews([
                $0.itemsTitleStack,
                transparentSeperator(withHeight: Constants.Margin.standard),
                buildTextFieldsSeperator(),
                $0.firstNameField,
                buildTextFieldsSeperator(),
                $0.lastNameField,
                buildTextFieldsSeperator(),
                transparentSeperator(withHeight: 15)
            ])
        }
    }

    private func setupAutoLayout() {
        let referVerticalMargin: CGFloat = 24

        controller.apply {
            $0.mainScroll.edgesToParent()
                        
            $0.scrollContent.widthAnchor.equal(to: $0.mainScroll.widthAnchor)
            $0.scrollContent.heightAnchor.greaterThanOrEqual(to: $0.mainScroll.heightAnchor)
            
            $0.titleItemsStack.edgesToParent(
                anchors: [.leading, .trailing, .top],
                insets: UIEdgeInsets.margins(top: 34, left: Constants.Margin.large, right: Constants.Margin.large)
            )
            
            $0.referButtonArea.edgesToParent(anchors: [.leading, .trailing, .bottom])
            $0.referButtonArea.topAnchor.equal(to: $0.titleItemsStack.bottomAnchor, constant: referVerticalMargin)
            
            $0.flexibleSpaceView.edgesToParent(anchors: [.top, .leading, .trailing])
            $0.flexibleSpaceView.heightAnchor.constraint(greaterThanOrEqualToConstant: 10).isActive = true
            
            $0.itemsTitleStack.widthAnchor.constraint(equalToConstant: $0.itemsStack.frame.size.width).isActive = true
            
            $0.itemsStack.edgesToParent(
                anchors: [.leading, .trailing, .top],
                insets: UIEdgeInsets.margins(top: 35, left: Constants.Margin.large, right: Constants.Margin.large)
            )
            
            [$0.firstNameField, $0.lastNameField].forEach {
                $0?.heightAnchor.equalTo(constant: 60)
            }

            $0.referButton.edgesToParent(anchors: [.leading, .trailing], insets: UIEdgeInsets.margins(
                left: Constants.Margin.standard,
                right: Constants.Margin.standard)
            )

            $0.referButton.topAnchor.equal(to: $0.lastNameField.bottomAnchor, constant: 34)
            $0.referButton.heightAnchor.equalTo(constant: Constants.bigButtonHeight)
        }
    }
    
    private func transparentSeperator(withHeight height: CGFloat) -> UIView {
        return UIView().apply {
            $0.backgroundColor = .clear
            $0.heightAnchor.equalTo(constant: height)
        }
    }
    
    private func buildTextFieldsSeperator() -> UIView {
        return UIView().apply {
            $0.backgroundColor = .textVeryLight
            $0.heightAnchor.equalTo(constant: 1)
        }
    }

}

// MARK: - Public build methods
extension ReferFriendViewBuilder {

    func buildTextField() -> ParlorTextField {
        return ParlorTextField().apply {
            let horizontalMargin: CGFloat = 22
            $0.backgroundColor = .white
            $0.padding = UIEdgeInsets(
                top: Constants.Margin.standard,
                left: horizontalMargin,
                bottom: Constants.Margin.standard,
                right: horizontalMargin
            )
        }
    }
    
    func buildScrollView() -> UIScrollView {
        return UIScrollView().manualLayoutable()
    }
    
    func buildContentView() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildFlexibleSpaceView() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildReferButtonArea() -> UIView {
        return UIView().manualLayoutable().apply {
            $0.backgroundColor = .appBackground
        }
    }
    
    func buildReferButton() -> RoundedButton {
        return RoundedButton.withBackgroundStyle().manualLayoutable()
    }
    
    func buildTitleItemsStack() -> UIStackView {
        return UIStackView(axis: .vertical, with: [], spacing: 0, alignment: .leading, distribution: .equalSpacing).manualLayoutable()
    }
    
    func builditemTitleStack() -> UIStackView {
        return UIStackView(axis: .horizontal, with: [], spacing: 0, alignment: .fill, distribution: .equalSpacing).manualLayoutable()
    }
    
    func buildItemsStack() -> UIStackView {
        return UIStackView(axis: .vertical, with: [], spacing: 0, alignment: .fill, distribution: .equalSpacing).manualLayoutable()
    }
    
    func buildTitleLabel() -> ParlorLabel {
        return ParlorLabel.styled(
            withTextColor: .appTextMediumBright,
            withFont: UIFont.custom(ofSize: Constants.FontSize.title, font: UIFont.Editor.medium),
            alignment: .left,
            numberOfLines: 0)
            .withText(Strings.ReferFriend.referMessage.localized)
            .isManualLayoutEnabled(false)
            .apply {
                $0.lineHeight = 30
        }
    }
    
    func buildSubTitle() -> ParlorLabel {
        return ParlorLabel.styled(
            withTextColor: .appTextMediumBright,
            withFont: UIFont.custom(ofSize: Constants.FontSize.subtitle, font: UIFont.Calibre.light),
            alignment: .left,
            numberOfLines: 0
        ).withText(Strings.ReferFriend.referSubMessage.localized)
        .isManualLayoutEnabled(false)
            .apply {
                $0.lineHeight = 20
        }
    }
    
    func buildFriendLabelIcon() -> CustomColouredIconedLabeledInfo {
        return CustomColouredIconedLabeledInfo(withIcon: Icons.ReferFriend.friend, title: Strings.ReferFriend.friend.localized, color: .appTextSemiLight).manualLayoutable()
    }
    
    func buildFriendLabel() -> ParlorLabel {
        return ParlorLabel.styled(
            withTextColor: .appTextSemiLight,
            withFont: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.regular),
            alignment: .right,
            numberOfLines: 0)
            .withText(Strings.ReferFriend.friend.localized)
            .isManualLayoutEnabled(false)
    }
    
    func buildRemoveLabel() -> ParlorLabel {
        return ParlorLabel.styled(
            withTextColor: .appTextSemiLight,
            withFont: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.regular),
            alignment: .right,
            numberOfLines: 0)
            .withText("")
            .isManualLayoutEnabled(false) //Strings.ReferFriend.remove.localized
    }
    
    func buildEnterInfoLabel() -> ParlorLabel {
        return ParlorLabel.styled(
            withTextColor: .appTextSemiLight,
            withFont: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.regular),
            alignment: .left,
            numberOfLines: 0)
            .withText(Strings.ReferFriend.enterInfo.localized)
            .isManualLayoutEnabled(false)
    }
}
