//
//  ContactsTabContent.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 31/05/2019.
//

import Foundation
import RxSwift

class ContactsTabContent: BaseTabSource {
    private let destinationsSubject: PublishSubject<ProfileDestination> = PublishSubject()
    private let profileRepository = ProfileRepository.shared
    private let disposeBag = DisposeBag()
    private let upgradeToPremiumIndex = 2
}

extension ContactsTabContent: ProfileTabContentSource {
    
    var currentItems: [ContactItem] {
        var items: [ContactItem] = [.emailUs, .suggestEventsOrVenues, .referFriend]
        if Config.userProfile?.isStandard ?? false {
            items.insert(.upgradeToPremium, at: upgradeToPremiumIndex)
        }
        return items
    }
    
    var currentTableContent: Observable<[TableSection]> {
        return Observable
            .just(Void())
            .concat(profileRepository.currentProfileObs.map { _ in return Void() })
            .map { [unowned self] _ in
                [ContactSection(items: self.currentItems, itemSelector: { [weak self] contactItem in
                    self?.didTapItem(item: contactItem)
                })]
        }
    }
    
    var destinations: Observable<ProfileDestination> {
        return destinationsSubject.asObservable()
    }
    
    private func handleResponse(response: SettingsResponse, item: ContactItem) {
        switch response {
        case .success:
            destinationsSubject.onNext(item.profileDestination)
        case .failure(let error):
            errorSubject.onNext(error)
        }
    }
    
    private func didTapItem(item: ContactItem) {
        switch item {
        case .emailUs, .suggestEventsOrVenues:
            self.progressBarSubject.onNext(true)
            self.profileRepository.getSettingsInfo()
                .subscribe(onNext: { [weak self] response in
                    self?.progressBarSubject.onNext(false)
                    self?.handleResponse(response: response, item: item)
                })
                .disposed(by: self.disposeBag)
        default:
            self.destinationsSubject.onNext(item.profileDestination)
        }
    }
}
