//
//  ContactItemCell.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 31/05/2019.
//

import UIKit

class ContactItemCell: UITableViewCell {
    
    // MARK: - Views
    lazy var cardView: UIView = {
        return UIView().manualLayoutable().apply {
            $0.applyTouchAnimation()
        }
    }()
    
    lazy var iconImage: UIImageView = {
        return UIImageView().apply {
            $0.contentMode = .scaleAspectFit
        }
    }()
    
    lazy var itemName: ParlorLabel = {
        return ParlorLabel.styled(
            withFont: UIFont.custom(ofSize: Constants.FontSize.body, font: UIFont.Roboto.regular),
            alignment: .left,
            numberOfLines: 1).apply {
                $0.adjustsFontSizeToFitWidth = true
                $0.minimumScaleFactor = 0.1
        }
    }()
    
    lazy var arrowImage: UIImageView = {
        return UIImageView(image: Icons.RsvpDetails.rightArrow).apply {
            $0.contentMode = .scaleAspectFit
            $0.setContentHuggingPriority(.required, for: .horizontal)
        }
    }()
    
    lazy var itemsStack: UIStackView = {
        return UIStackView(
            axis: .horizontal,
            with: [iconImage, itemName, arrowImage],
            spacing: 15,
            alignment: .fill,
            distribution: .fill
            ).manualLayoutable()
    }()

    // MARK: - Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        itemName.translatesAutoresizingMaskIntoConstraints = true
        backgroundColor = .clear
        selectionStyle = .none
        setupAutoLayout()
    }
    
    private func setupAutoLayout() {
        contentView.addSubview(cardView)
        cardView.addSubview(itemsStack)
        
        cardView.edgesToParent(
            insets: UIEdgeInsets.margins(top: 0, left: Constants.Margin.standard, bottom: 8, right: Constants.Margin.standard)
        )
        itemsStack.edgesToParent(
            insets: UIEdgeInsets.margins(top: 23, left: 22, bottom: 23, right: 22)
        )
        
        iconImage.widthAnchor.equalTo(constant: 16)
        arrowImage.widthAnchor.equalTo(constant: 14)
    }
}

extension ContactItemCell: ConfigurableCell {
    
    typealias Model = ContactItem

    func configure(with model: ContactItem) {
        iconImage.image = model.icon
        itemName.text = model.title
        itemName.textColor = PremiumColorManager.shared.currentTheme.profileItemContentTextColor
        configureCardViewColorTheme()
    }
}

// MARK: - Configure views with color theme methods
extension ContactItemCell {
    
    private func configureCardViewColorTheme() {
        switch PremiumColorManager.shared.currentTheme {
        case .nonPremium:
            cardView.addLightBorder()
        case .premium:
            cardView.addDarkBorder()
        }
        cardView.backgroundColor = PremiumColorManager.shared.currentTheme.profileItemBackgroundColor
    }
    
}
