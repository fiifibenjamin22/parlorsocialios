//
//  ContactSection.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 31/05/2019.
//

import Foundation
import UIKit

final class ContactSection: BasicTableSection<ContactItem, ContactItemCell> {
    
    override var headerType: UITableViewHeaderFooterView.Type? { return ProfileTabHeaderView.self }
    
    override func configure(header: UITableViewHeaderFooterView, in section: Int) {
        guard let sectionHeader = header as? ProfileTabHeaderView else {
            fatalError("Header is wrong type! Needed: \(ProfileTabHeaderView.name) but get instead \(String(describing: header))")
        }
        sectionHeader.apply {
            $0.label.text = Strings.Profile.contact.localized
            $0.label.textColor = PremiumColorManager.shared.currentTheme.profileTabHeaderLabelColor
            $0.contentView.backgroundColor = PremiumColorManager.shared.currentTheme.profileMainTableViewBackgroundColor
        }
    }
}
