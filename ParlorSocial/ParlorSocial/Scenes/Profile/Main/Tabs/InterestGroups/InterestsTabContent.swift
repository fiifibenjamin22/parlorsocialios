//
//  InterestsTabContent.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 29/05/2019.
//

import Foundation
import RxSwift

class InterestsTabContent: BaseTabSource {
    
    private let profileRepository = ProfileRepository.shared
    private let destinationsSubject: PublishSubject<ProfileDestination> = PublishSubject()
    
}

extension InterestsTabContent: ProfileTabContentSource {
    
    var currentTableContent: Observable<[TableSection]> {
        return profileRepository.getMyInterestGroups()
            .ignoreElements()
            .andThen(profileRepository.knownInterests)
            .map { ($0 + [InterestGroup.addMoreInterestData()]).chunked(into: 2) }
            .map { [InterestsSection(items: $0, addMoreClickCallback: { [weak self] in
                self?.destinationsSubject.onNext(.interests)
            })] }
    }
    
    var destinations: Observable<ProfileDestination> {
        return destinationsSubject.asObservable()
    }
    
}
