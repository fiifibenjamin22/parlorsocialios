//
//  InterestGroupsViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 09/05/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxSwift
import RxSwiftExt

protocol InterestGroupsLogic: BaseViewModelLogic {
    var reloadObs: Observable<Void> { get }
    var isDataLoaded: Observable<Bool> { get }
    var errorObs: Observable<AppError> { get }
    var destinationObs: Observable<InterestGroupsDestination> { get }

    var loadMoreNeeded: PublishSubject<Void> { get }
    var saveClicks: PublishSubject<Void> { get }
    
    var interestGroups: [InterestGroup] { get }

    func setItemSelected(atIndex index: Int, isSelected: Bool)
    func didTapSuggestInterest()
}

class InterestGroupsViewModel: BaseViewModel {
    let analyticEventReferencedId: Int? = nil
    private let interestRepository = InterestGroupsRepository.shared
    
    private let showingData: BehaviorSubject<[InterestGroup]> = BehaviorSubject(value: [])
    private let reloadSubject: PublishSubject<Void> = .init()
    private let interestGroupsSubject: BehaviorSubject<[PaginatedResponse<InterestGroup>]> = BehaviorSubject(value: [])
    private let manualSelectionDictSubject: BehaviorSubject<[Int: Bool]> = BehaviorSubject(value: [:])
    private let destinationsSubject: PublishSubject<InterestGroupsDestination> = PublishSubject()

    let loadMoreNeeded: PublishSubject<Void> = PublishSubject()
    let saveClicks: PublishSubject<Void> = PublishSubject()

    override init() {
        super.init()
        initFlow()
        loadMoreNeeded.emitElement()
    }
    
}

// MARK: Private logic
private extension InterestGroupsViewModel {
    
    private var interestGroupsObs: Observable<[InterestGroup]> {
        let apiResposneElements = interestGroupsSubject
            .map { paginatedResponses in return paginatedResponses.flatMap { $0.data }.uniqueElements }
        return Observable.combineLatest(apiResposneElements, manualSelectionDictSubject) { apiElements, manualSelection in
                return apiElements.map { interest in
                    let selectionState = manualSelection[interest.id] ?? (interest.isSelected ?? false)
                    return interest.withSelectedState(isSelected: selectionState)
                }
            }
            .asObservable()
    }
    
    private func initFlow() {
        interestGroupsObs
            .bind(to: showingData)
            .disposed(by: disposeBag)
        
        loadMoreNeeded.withLatestFrom(interestGroupsSubject)
            .filter { !($0.last?.meta.isLastPage ?? false ) }
            .flatMapFirst { [unowned self] (currentItems: [PaginatedResponse<InterestGroup>]) -> Observable<InterestGroupsResposne> in
                let nextPageToLoad = ((currentItems.last?.meta.currentPage) ?? 0) + 1
                return self.interestRepository.getInterestGroups(atPage: nextPageToLoad) }
            .mapSuccessfullResponses()
            .withLatestFrom(interestGroupsSubject) { $1 + [$0] }
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [unowned self] response in
                self.interestGroupsSubject.onNext(response)
                self.reloadSubject.emitElement()
            }).disposed(by: disposeBag)
        
        saveClicks
            .withLatestFrom(manualSelectionDictSubject)
            .doOnNext { [unowned self] userSelections in
                if userSelections.isEmpty {
                    self.destinationsSubject.onNext(.back)
                }
            }
            .filter { !$0.isEmpty }
            .flatMapFirst { [unowned self] userSelections -> MessageApiResponse in
                let updatedInterests = self.interestGroups.filter { userSelections.keys.contains($0.id) }
                return self.interestRepository.updateInterestGroups(updatedInterests)
            }
            .doOnNext { [unowned self] resposne in
                switch resposne {
                case .success:
                    self.destinationsSubject.onNext(.back)
                case .failure(let error):
                    self.errorSubject.onNext(error)
                }
            }.subscribe().disposed(by: disposeBag)
    }
    
}

// MARK: Interface logic methods
extension InterestGroupsViewModel: InterestGroupsLogic {
    
    var destinationObs: Observable<InterestGroupsDestination> {
        return destinationsSubject.asObservable()
    }
    
    var isDataLoaded: Observable<Bool> {
        return interestGroupsSubject.map { $0.last?.meta.isLastPage ?? false }
    }
    
    var reloadObs: Observable<Void> {
        return reloadSubject.asObservable()
    }
    
    func setItemSelected(atIndex index: Int, isSelected: Bool) {
        do {
            var currentValue = try manualSelectionDictSubject.value()
            currentValue[interestGroups[index].id] = isSelected
            manualSelectionDictSubject.onNext(currentValue)
        } catch { }
    }
    
    func didTapSuggestInterest() {
        destinationsSubject.onNext(.suggest)
    }
    
    var interestGroups: [InterestGroup] {
        return (try? showingData.value()) ?? []
    }

}

fileprivate extension ObservableType where Element == InterestGroupsResposne {
    
    func mapSuccessfullResponses() -> Observable<PaginatedResponse<InterestGroup>> {
        return filterMap { response -> FilterMap<PaginatedResponse<InterestGroup>> in
            switch response {
            case .success(let successResponse):
                return .map(successResponse)
            case .failure:
                return .ignore
            } }
    }
    
}
