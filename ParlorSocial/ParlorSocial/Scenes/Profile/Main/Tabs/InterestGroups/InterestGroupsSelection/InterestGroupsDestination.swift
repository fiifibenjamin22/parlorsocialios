//
//  InterestGroupsDestination.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 17/05/2019.
//

import Foundation
import UIKit

enum InterestGroupsDestination {
    case back
    case suggest
}

extension InterestGroupsDestination: Destination {
    
    var viewController: UIViewController {
        switch self {
        case .suggest:
            return SuggestInterestViewController()
        case .back:
            fatalError()
        }
    }
}
