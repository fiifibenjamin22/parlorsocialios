//
//  InterestGroupsViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 09/05/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

class InterestGroupsViewController: AppViewController {

    // MARK: - Views
    private(set) var interestGroupsCollection: UICollectionView!
    
    // MARK: - Properties
    let analyticEventScreen: AnalyticEventScreen? = .interestGroups
    private var viewModel: InterestGroupsLogic!
    private(set) var flowLayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout().apply {
        $0.minimumLineSpacing = Constants.Margin.standard
    }
    private var isDataLoaded: Bool = false {
        didSet {
            updateFooterState()
        }
    }
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }
    
    override var observableSources: ObservableSources? {
        return viewModel
    }
    
    // MARK: - Initialization
    init() {
        super.init(nibName: nil, bundle: nil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() {
        self.viewModel = InterestGroupsViewModel()
    }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
        setupCollectionView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        adjustNavigationBar()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let collectionItemSize = Int((interestGroupsCollection.bounds.width - Constants.Margin.standard) / 2.0)
        flowLayout.itemSize = CGSize(width: collectionItemSize, height: collectionItemSize)
        flowLayout.headerReferenceSize = CGSize(width: interestGroupsCollection.bounds.width, height: 75)
        updateFooterState()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

}

// MARK: - Private methods
extension InterestGroupsViewController {
    
    private func updateFooterState() {
        let height: CGFloat = isDataLoaded ? (41 * 2) + 55 : 75
        flowLayout.footerReferenceSize = CGSize(width: interestGroupsCollection.bounds.width, height: height)
    }

    private func setupViews() {
        let builder = InterestGroupsViewBuilder(controller: self)
        self.interestGroupsCollection = builder.buildCollectionView()
        builder.setupViews()
    }
    
    private func setupCollectionView() {
        interestGroupsCollection.apply {
            $0.register(InterestItemCell.self, forCellWithReuseIdentifier: InterestItemCell.self.name)
            $0.register(InterestsHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: InterestsHeader.self.name)
            $0.register(InterestsFooter.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: InterestsFooter.self.name)
            $0.allowsMultipleSelection = true
            $0.delegate = self
            $0.dataSource = self
        }
    }
    
    private func adjustNavigationBar() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.createSaveItem(target: self, action: #selector(didTapSave))
        navigationController?.applyProfileItemAppearance(withTitle: Strings.Interests.title.localized.uppercased())
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        statusBar?.backgroundColor = .white
        setNeedsStatusBarAppearanceUpdate()
    }

    private func setupPresentationLogic() {
        viewModel.reloadObs.doOnNext { [weak self] _ in
            self?.interestGroupsCollection.reloadData()
        }.subscribe().disposed(by: disposeBag)
        
        viewModel.isDataLoaded.distinctUntilChanged()
            .doOnNext { [weak self] isLoaded in
            self?.isDataLoaded = isLoaded
            }.subscribe().disposed(by: disposeBag)
        
        viewModel
            .errorObs
            .handleWithAlerts(using: self)
            .disposed(by: disposeBag)
        
        viewModel.destinationObs.doOnNext { [weak self] destination in
            self?.handle(destination: destination)
            }.subscribe().disposed(by: disposeBag)
    }
    
    @objc private func didTapSave() {
        viewModel.saveClicks.emitElement()
    }
    
    @objc private func didTapSuggest() {
        viewModel.didTapSuggestInterest()
    }
    
    private func handle(destination: InterestGroupsDestination) {
        switch destination {
        case .suggest:
            router.push(destination: destination)
        case .back:
            navigationController?.popViewController(animated: true)
        }
    }

}

extension InterestGroupsViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.interestGroups.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: InterestItemCell.self.name, for: indexPath) as? InterestItemCell else { fatalError() }
        cell.showingInterestGroup = viewModel.interestGroups[indexPath.row]
        if viewModel.interestGroups[indexPath.row].isSelected == true {
            collectionView.selectItem(at: indexPath, animated: false, scrollPosition: [])
        }
        return cell
    }
    
}

extension InterestGroupsViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: InterestsHeader.self.name, for: indexPath)
        case UICollectionView.elementKindSectionFooter:
            guard let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier:
                InterestsFooter.self.name, for: indexPath) as?  InterestsFooter else { fatalError() }
            return view.apply {
                $0.isLoadingFooterVisible = !isDataLoaded
                $0.suggestInterestButton.addTarget(self, action: #selector(didTapSuggest), for: .touchUpInside)
            }
        default:
            fatalError()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == viewModel.interestGroups.count - 1 {
            viewModel.loadMoreNeeded.emitElement()
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y + scrollView.frame.height > scrollView.contentSize.height - flowLayout.itemSize.height {
            viewModel.loadMoreNeeded.emitElement()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.setItemSelected(atIndex: indexPath.row, isSelected: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        viewModel.setItemSelected(atIndex: indexPath.row, isSelected: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let interestItemCell = cell as? InterestItemCell else { return }
        interestItemCell.interestView.interestImageView.kf.cancelDownloadTask()
    }
    
}
