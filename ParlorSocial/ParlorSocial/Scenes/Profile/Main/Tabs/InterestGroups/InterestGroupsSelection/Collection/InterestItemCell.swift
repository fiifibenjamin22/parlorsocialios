//
//  InterestItemCell.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 09/05/2019.
//

import UIKit

class InterestItemCell: UICollectionViewCell {
    
    override var isSelected: Bool {
        didSet {
            interestView.isChecked = self.isSelected
        }
    }
    
    private(set) lazy var interestView: InterestGroupView = {
        return InterestGroupView().manualLayoutable()
    }()
    
    var showingInterestGroup: InterestGroup? {
        get {
            return interestView.showingInterest
        }
        set {
            interestView.showingInterest = newValue
            self.isSelected = newValue?.isSelected ?? false
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(interestView)
        interestView.edgesToParent()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
