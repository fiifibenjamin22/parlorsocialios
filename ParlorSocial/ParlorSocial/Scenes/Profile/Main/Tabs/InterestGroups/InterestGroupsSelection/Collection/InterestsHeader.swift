//
//  InterestsHeader.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 09/05/2019.
//

import UIKit

class InterestsHeader: UICollectionReusableView {
    
    lazy var interestsLabel: ParlorLabel = {
        return ParlorLabel.styled(withTextColor: .closeTintColor, withFont: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.regular), alignment: .left, numberOfLines: 0)
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(interestsLabel)
        interestsLabel.text = Strings.Interests.interests.localized
        interestsLabel.leadingAnchor.equal(to: self.leadingAnchor)
        interestsLabel.bottomAnchor.equal(to: self.bottomAnchor, constant: -20)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
