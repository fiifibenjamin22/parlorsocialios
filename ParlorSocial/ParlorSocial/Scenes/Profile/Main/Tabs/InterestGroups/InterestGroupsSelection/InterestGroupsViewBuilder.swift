//
//  InterestGroupsViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 09/05/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class InterestGroupsViewBuilder {

    private unowned let controller: InterestGroupsViewController
    private var view: UIView! { return controller.view }

    init(controller: InterestGroupsViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension InterestGroupsViewBuilder {

    private func setupProperties() {
        view.backgroundColor = .appBackground
    }

    private func setupHierarchy() {
        controller.apply {
            view.addSubview($0.interestGroupsCollection)
        }
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.interestGroupsCollection.edgesToParent(anchors: [.leading, .trailing], padding: Constants.Margin.standard)
            $0.interestGroupsCollection.edgesToParent(anchors: [.top, .bottom])
        }
    }

}

// MARK: - Public build methods
extension InterestGroupsViewBuilder {
    
    func buildCollectionView() -> UICollectionView {
        return UICollectionView(frame: .zero, collectionViewLayout: controller.flowLayout).manualLayoutable().apply {
            $0.backgroundColor = .clear
            $0.showsVerticalScrollIndicator = false
        }
    }

}
