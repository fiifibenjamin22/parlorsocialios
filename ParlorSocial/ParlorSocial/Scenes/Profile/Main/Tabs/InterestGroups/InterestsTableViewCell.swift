//
//  InterestsTableViewCell.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 29/05/2019.
//

import Foundation
import UIKit

class InterestsTableViewCell: UITableViewCell {
    
    // MARK: - Views
    lazy var interestViewsStack: UIStackView = {
        return UIStackView(
            axis: .horizontal,
            with: (0...1).map { _ in InterestGroupView() },
            spacing: Constants.Margin.standard,
            alignment: .center,
            distribution: .fillEqually).manualLayoutable()
    }()
    
    var interestViews: [InterestGroupView] {
        return (0...1).map { return interestView(atIndex: $0) }
    }
    
    // MARK: - Properties
    var cellClickCallback: () -> Void = { }
    
    // MARK: - Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        backgroundColor = .clear
        selectionStyle = .none
        interestViews.forEach {
            $0.applyTouchAnimation()
            $0.checkedIconImage.alpha = 0
            $0.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapTouchArea)))
        }
        setupAutoLayout()
    }
    
    private func setupAutoLayout() {
        addSubview(interestViewsStack)
        interestViewsStack.edgesToParent(insets: UIEdgeInsets.margins(top: 0, left: Constants.Margin.standard, bottom: Constants.Margin.standard, right: Constants.Margin.standard))
        interestViews.forEach {
            $0.heightAnchor.equal(to: $0.widthAnchor)
        }
    }
    
    // MARK: - Private Logic
    func interestView(atIndex index: Int) -> InterestGroupView {
        guard let view = interestViewsStack.arrangedSubviews[index] as? InterestGroupView else { fatalError() }
        return view
    }
    
    @objc private func didTapTouchArea() {
        cellClickCallback()
    }
}

extension InterestsTableViewCell: ConfigurableCell {
    
    typealias Model = [InterestGroup]
    
    func configure(with model: [InterestGroup]) {
        interestViews.enumerated().forEach { index, view in
            let isInterestPresent = index < model.count
            if isInterestPresent {
                view.showingInterest = model[index]
                view.isChecked = model[index].isAddMoreData
                view.isUserInteractionEnabled = model[index].isAddMoreData
                view.backgroundColor = PremiumColorManager.shared.currentTheme.profileItemBackgroundColor
                view.interestLabel.textColor = view.interestImageView.image == nil ? PremiumColorManager.shared.currentTheme.profileItemContentTextColor : .white
            } else {
                view.isUserInteractionEnabled = false
                view.isChecked = false
            }
            view.alpha = isInterestPresent ? 1 : 0
        }
    }
}
