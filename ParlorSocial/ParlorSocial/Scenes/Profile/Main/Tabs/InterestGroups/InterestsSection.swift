//
//  InterestsSection.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 29/05/2019.
//

import Foundation
import UIKit

class InterestsSection: BasicTableSection<[InterestGroup], InterestsTableViewCell> {
    
    let addMoreClickCallback: (() -> Void)
    
    init(items: [[InterestGroup]], addMoreClickCallback: (@escaping () -> Void)) {
        self.addMoreClickCallback = addMoreClickCallback
        super.init(items: items, itemSelector: { _ in })
    }
    
    override var headerType: UITableViewHeaderFooterView.Type? { return ProfileTabHeaderView.self }
    
    override func configure(header: UITableViewHeaderFooterView, in section: Int) {
        guard let sectionHeader = header as? ProfileTabHeaderView else {
            fatalError("Header is wrong type! Needed: \(ProfileTabHeaderView.name) but get instead \(String(describing: header))")
        }
        sectionHeader.apply {
            $0.label.text = Strings.Profile.yourInterests.localized
            $0.label.textColor = PremiumColorManager.shared.currentTheme.profileTabHeaderLabelColor
            $0.contentView.backgroundColor = PremiumColorManager.shared.currentTheme.profileMainTableViewBackgroundColor
        }
    }
    
    override func configure(cell: UITableViewCell, at indexPath: IndexPath) {
        super.configure(cell: cell, at: indexPath)
        guard let interestCell = cell as? InterestsTableViewCell else {
            fatalError("Cell is wrong type! Needed: \(InterestsTableViewCell.name) but get instead \(String(describing: cell))")
        }
        interestCell.cellClickCallback = addMoreClickCallback
    }
    
}
