//
//  SuggestInterestViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 30/05/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxSwift

protocol SuggestInterestLogic: BaseViewModelLogic {
    var textChangesSubject: BehaviorSubject<String> { get }
    var sendClicks: PublishSubject<Void> { get }
}

class SuggestInterestViewModel: BaseViewModel {
    let analyticEventReferencedId: Int? = nil
    let interestsRepository = InterestGroupsRepository.shared
    
    let textChangesSubject: BehaviorSubject<String> = BehaviorSubject(value: "")
    let sendClicks: PublishSubject<Void> = PublishSubject()
    
    override init() {
        super.init()
        bindSaves()
    }
}

// MARK: Private logic
private extension SuggestInterestViewModel {
    
    private func bindSaves() {
        sendClicks.withLatestFrom(textChangesSubject)
            .flatMapFirst { [unowned self] in self.interestsRepository.proposeInterestGroup(withName: $0) }
            .doOnNext { [unowned self] response in
                switch response {
                case .success:
                    self.closeViewSubject.emitElement()
                case .failure(let error):
                    self.errorSubject.onNext(error)
                }
            }.subscribe().disposed(by: disposeBag)
    }

}

// MARK: Interface logic methods
extension SuggestInterestViewModel: SuggestInterestLogic { }
