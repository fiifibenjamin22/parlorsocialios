//
//  SuggestInterestViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 30/05/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class SuggestInterestViewBuilder {

    private unowned let controller: SuggestInterestViewController
    private var view: UIView! { return controller.view }

    init(controller: SuggestInterestViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension SuggestInterestViewBuilder {

    private func setupProperties() {
        view.backgroundColor = .appBackground
        controller.apply {
            $0.sendButton.setTitle(Strings.Interests.submitHere.localized.uppercased(), for: .normal)
            $0.sendUsLabel.text = Strings.Interests.sendUs.localized
            $0.typeMessageBackgroundView.apply {
                $0.layer.borderWidth = 1
                $0.layer.borderColor = UIColor.textVeryLight.cgColor
                $0.backgroundColor = .white
            }
        }
    }

    private func setupHierarchy() {
        controller.apply {
            view.addSubviews([$0.sendUsLabel, $0.typeMessageBackgroundView, $0.sendButton])
            $0.typeMessageBackgroundView.addSubview($0.typeMessageField)
        }
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.sendUsLabel.edgesToParent(anchors: [.top, .leading, .trailing], insets: UIEdgeInsets.margins(top: 25, left: Constants.Margin.standard, right: Constants.Margin.standard))
            
            $0.typeMessageBackgroundView.apply {
                $0.edgesToParent(anchors: [.leading, .trailing], padding: Constants.Margin.standard)
                $0.topAnchor.equal(to: controller.sendUsLabel.bottomAnchor, constant: 10)
                $0.heightAnchor.equalTo(constant: 140)
            }
            
            $0.typeMessageField.edgesToParent(padding: 21)
            
            $0.sendButton.topAnchor.equal(to: $0.typeMessageField.bottomAnchor, constant: 39)
            $0.sendButton.edgesToParent(anchors: [.leading, .trailing], insets: UIEdgeInsets.margins(left: Constants.Margin.standard, bottom: 23, right: Constants.Margin.standard))
        }
    }

}

// MARK: - Public build methods
extension SuggestInterestViewBuilder {
    
    func buildLabel() -> ParlorLabel {
        return ParlorLabel.styled(
            withTextColor: .appGreyLight,
            withFont: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.regular),
            alignment: .left,
            numberOfLines: 1
            ).apply { $0.setContentHuggingPriority(.required, for: .vertical) }
    }
    
    func buildView() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildSendButton() -> RoundedButton {
        return RoundedButton.withBackgroundStyle().manualLayoutable()
    }
    
    func buildTextField() -> UITextView {
        return UITextView().manualLayoutable().apply {
            $0.setContentCompressionResistancePriority(.required, for: .vertical)
            $0.backgroundColor = .white
            $0.font = UIFont.custom(
                ofSize: Constants.FontSize.body,
                font: UIFont.Roboto.regular
            )
            $0.textColor = .appTextMediumBright
            $0.tintColor = .textVeryLight
        }
    }
}
