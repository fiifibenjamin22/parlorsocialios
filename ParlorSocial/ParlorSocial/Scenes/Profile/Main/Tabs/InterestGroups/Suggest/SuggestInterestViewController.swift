//
//  SuggestInterestViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 30/05/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

class SuggestInterestViewController: AppViewController {
    
    // MARK: - Views
    private(set) var sendUsLabel: ParlorLabel!
    private(set) var typeMessageBackgroundView: UIView!
    private(set) var typeMessageField: UITextView!
    private(set) var sendButton: RoundedButton!

    // MARK: - Properties
    let analyticEventScreen: AnalyticEventScreen? = .suggestInterest
    private var viewModel: SuggestInterestLogic!
    
    override var observableSources: ObservableSources? {
        return viewModel
    }
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }

    // MARK: - Initialization
    init() {
        super.init(nibName: nil, bundle: nil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() {
        self.viewModel = SuggestInterestViewModel()
    }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        adjustNavigationBar()
    }

}

// MARK: - Private methods
extension SuggestInterestViewController {
    
    private func adjustNavigationBar() {
        self.title = Strings.Interests.suggest.localized.uppercased()
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        statusBar?.backgroundColor = .white
        setNeedsStatusBarAppearanceUpdate()
    }
    
    private func setupViews() {
        let builder = SuggestInterestViewBuilder(controller: self)
        sendUsLabel = builder.buildLabel()
        typeMessageBackgroundView = builder.buildView()
        typeMessageField = builder.buildTextField()
        sendButton = builder.buildSendButton()
        builder.setupViews()
    }

    private func setupPresentationLogic() {
        typeMessageField.rx.text
            .map { $0.emptyIfNil }
            .bind(to: viewModel.textChangesSubject)
            .disposed(by: disposeBag)
        
        sendButton.rx
            .controlEvent(.touchUpInside)
            .bind(to: viewModel.sendClicks)
            .disposed(by: disposeBag)
        
        viewModel.closeObs
            .doOnNext { [unowned self] _ in
                self.navigationController?.popViewController(animated: true)
            }.subscribe().disposed(by: disposeBag)
    }
    
}
