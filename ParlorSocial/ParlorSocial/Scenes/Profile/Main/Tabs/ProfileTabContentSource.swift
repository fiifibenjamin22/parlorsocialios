//
//  ProfileTab.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 24/05/2019.
//

import Foundation
import UIKit
import RxSwift

protocol ProfileTabContentSource: ObservableSources, AnyObject {
    
    var currentTableContent: Observable<[TableSection]> { get }
    
    var destinations: Observable<ProfileDestination> { get }
    
}
