//
//  CirclesTabContent.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 24/05/2019.
//

import Foundation
import RxSwift

class CirclesTabContent: BaseTabSource {
    private let profileRepository = ProfileRepository.shared
    private let destinationsSubject: PublishSubject<ProfileDestination> = PublishSubject()
}

extension CirclesTabContent: ProfileTabContentSource {
    
    var destinations: Observable<ProfileDestination> {
        return destinationsSubject.asObservable()
    }
    
    var currentTableContent: Observable<[TableSection]> {
        return profileRepository.getMyCircles()
            .ignoreElements()
            .andThen(profileRepository.knownCircles)
            .concat(profileRepository.knownCircles)
            .map { [CirclesSection(items: $0, itemSelector: { [weak self] circle in
                self?.destinationsSubject.onNext(.circleDetails(circle))
            })] }
    }
    
}
