//
//  CircleDetailsViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 27/05/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class CircleDetailsViewBuilder {

    private unowned let controller: CircleDetailsViewController
    private var view: UIView! { return controller.view }

    init(controller: CircleDetailsViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension CircleDetailsViewBuilder {

    private func setupProperties() {
        controller.apply {
            $0.yourCircleLabel.text = Strings.Profile.yourCircle.localized.uppercased()
            $0.leaveCircleButton.setTitle(Strings.Profile.leaveCircle.localized.uppercased(), for: .normal)
            $0.circleDescription.lineHeight = 24
        }
    }

    private func setupHierarchy() {
        controller.apply {
            view.addSubview($0.scrollView)
            $0.scrollView.addSubview($0.scrollContentView)
            $0.scrollContentView.addSubviews([$0.circleImageView, $0.labelsStack, $0.flexibleSpaceView, $0.leaveCircleButton])
        }
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.scrollView.edgesToParent()
            
            $0.scrollContentView.edgesToParent()
            $0.scrollContentView.widthAnchor.equal(to: $0.scrollView.widthAnchor)
            
            $0.circleImageView.edgesToParent(anchors: [.leading, .trailing, .top])
            $0.circleImageView.widthAnchor.constraint(equalTo: $0.circleImageView.heightAnchor, multiplier: 1.1).activate()
            
            $0.labelsStack.edgesToParent(anchors: [.leading, .trailing], padding: 25)
            $0.labelsStack.topAnchor.equal(to: $0.circleImageView.bottomAnchor, constant: 42)
            
            $0.flexibleSpaceView.apply {
                $0.topAnchor.equal(to: controller.labelsStack.bottomAnchor)
                $0.edgesToParent(anchors: [.leading, .trailing])
            }
            
            $0.leaveCircleButton.apply {
                $0.topAnchor.equal(to: controller.flexibleSpaceView.bottomAnchor)
                $0.edgesToParent(anchors: [.leading, .trailing], padding: 25)
                $0.edgesToParent(anchors: [.bottom], padding: 70)
                $0.heightAnchor.equalTo(constant: Constants.bigButtonHeight)
            }
        }
    }

}

// MARK: - Public build methods
extension CircleDetailsViewBuilder {
    
    func buildScrollView() -> UIScrollView {
        return UIScrollView().manualLayoutable().apply {
            $0.backgroundColor = .white
        }
    }
    
    func buildView() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildCircleImageView() -> UIImageView {
        return UIImageView().manualLayoutable().apply {
            $0.contentMode = .scaleAspectFill
            $0.clipsToBounds = true
        }
    }
    
    func buildLablesStack() -> UIStackView {
        return UIStackView(axis: .vertical,
                           with: [controller.yourCircleLabel, controller.circleTitleLabel, controller.circleDescription],
                           spacing: 15, alignment: .fill, distribution: .equalSpacing).manualLayoutable()
    }
    
    func buildYourCircleLabel() -> ParlorLabel {
        return ParlorLabel.styled(withTextColor: .rsvpBorder,
                                  withFont: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.regular),
                                  alignment: .left, numberOfLines: 1)
    }
    
    func buildCircleTitleLabel() -> ParlorLabel {
        return ParlorLabel.styled(withTextColor: .appTextMediumBright,
                                  withFont: UIFont.custom(ofSize: Constants.FontSize.title, font: UIFont.Editor.bold),
                                  alignment: .left, numberOfLines: 0)
    }
    
    func buildCircleDescriptionLabel() -> ParlorLabel {
        return ParlorLabel.styled(withTextColor: .appTextMediumBright,
                                  withFont: UIFont.custom(ofSize: Constants.FontSize.body, font: UIFont.Roboto.light),
                                  alignment: .left, numberOfLines: 0).apply {
                                    $0.lineHeight = Constants.lineHeightBig
        }
    }
    
    func buildLeaveCircleButton() -> RoundedButton {
        return RoundedButton().manualLayoutable().apply {
            $0.layer.borderWidth = 1
            $0.layer.borderColor = UIColor.appSeparator.cgColor
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium)
            $0.setTitleColor(.appText, for: .normal)
            $0.applyTouchAnimation()
        }
    }
    
    func buildImageGalleryNavigationItem() -> UIBarButtonItem {
        return UIBarButtonItem(image: Icons.Common.moreImages)
    }
    
    func buildFlexibleSpaceHeightConstraint() -> NSLayoutConstraint {
        return controller.flexibleSpaceView.heightAnchor.constraint(equalToConstant: 0).activate()
    }
}
