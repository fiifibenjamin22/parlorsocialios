//
//  CircleDetailsViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 27/05/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

class CircleDetailsViewController: AppViewController {
    // MARK: - Views
    private(set) var scrollView: UIScrollView!
    private(set) var scrollContentView: UIView!
    private(set) var circleImageView: UIImageView!
    private(set) var labelsStack: UIStackView!
    private(set) var yourCircleLabel: ParlorLabel!
    private(set) var circleTitleLabel: ParlorLabel!
    private(set) var circleDescription: ParlorLabel!
    private(set) var leaveCircleButton: RoundedButton!
    private(set) var moreImagesBarItem: UIBarButtonItem!
    
    private(set) var flexibleSpaceView: UIView!
    private(set) var flexibleSpaceHeightConstraint: NSLayoutConstraint!

    // MARK: - Properties    
    private var viewModel: CircleDetailsLogic!
    let analyticEventScreen: AnalyticEventScreen? = .circleDetails
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }
    
    override var observableSources: ObservableSources? {
        return viewModel
    }
    
    override var includeNavBarSafeAreaInsets: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Initialization
    init(withCircle circle: Circle) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = CircleDetailsViewModel(withCircle: circle)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() {
    }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        disableScrollInsets(for: scrollView)
        setupPresentationLogic()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        scrollView.delegate = self
        adjustNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.scrollView.changeNavigationBarAlpha(using: self.circleImageView, to: self.navigationController?.navigationBar)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        scrollView.delegate = nil
        scrollView.stopScrolling()
    }
    
    override func observeViewModel() {
        super.observeViewModel()
        viewModel.circleObs.doOnNext { [unowned self] circle in
            self.configure(with: circle)
            }.subscribe().disposed(by: disposeBag)
        
        viewModel.closeObs.doOnNext { [unowned self] _ in
            self.navigationController?.popViewController(animated: true)
            }.subscribe().disposed(by: disposeBag)
        
        viewModel.destinationsObservable.doOnNext { [unowned self] destination in
            self.handleDestination(destination)
            }.subscribe().disposed(by: disposeBag)
    }
    
    override func bindUI() {
        super.bindUI()
        leaveCircleButton.rx.controlEvent(.touchUpInside)
            .bind(to: viewModel.removeCircleClicks)
            .disposed(by: disposeBag)
        
        moreImagesBarItem.rx
            .tap.bind(to: viewModel.imageGalleryClicks)
            .disposed(by: disposeBag)
    }
    
}

// MARK: - Private methods
extension CircleDetailsViewController {
    
    private func handleDestination(_ destination: CircleDetailsDestination) {
        switch destination {
        case .imageGallery:
            self.router.present(destination: destination, animated: true, withStyle: .fullScreen)
        }
    }
    
    private func adjustNavigationBar() {
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.applyTransparentTheme(withColor: .white)
        statusBar?.backgroundColor = .clear
        setNeedsStatusBarAppearanceUpdate()
    }
    
    private func configure(with circle: Circle) {
        circleImageView.getImage(from: circle.imageURLs.first ?? nil)
        circleDescription.setHtmlText(circle.description)
        circleTitleLabel.text = circle.name
        navigationItem.rightBarButtonItem = circle.imageURLs.count > 1 ? moreImagesBarItem : nil
        title = circle.name.uppercased()
    }

    private func setupViews() {
        let builder = CircleDetailsViewBuilder(controller: self)
        self.scrollView = builder.buildScrollView()
        self.scrollView.delegate = self
        self.scrollContentView = builder.buildView()
        self.circleImageView = builder.buildCircleImageView()
        self.yourCircleLabel = builder.buildYourCircleLabel()
        self.circleTitleLabel = builder.buildCircleTitleLabel()
        self.circleDescription = builder.buildCircleDescriptionLabel()
        self.leaveCircleButton = builder.buildLeaveCircleButton()
        self.labelsStack = builder.buildLablesStack()
        self.moreImagesBarItem = builder.buildImageGalleryNavigationItem()
        self.flexibleSpaceView = builder.buildView()
        self.flexibleSpaceHeightConstraint = builder.buildFlexibleSpaceHeightConstraint()
        builder.setupViews()
    }

    private func setupPresentationLogic() {
        setupFlexibleSpaceConstraint()
    }
    
    private func setupFlexibleSpaceConstraint() {
        view.layoutIfNeeded()
        let heightDiff = scrollView.frame.height
            - scrollContentView.frame.height
        flexibleSpaceHeightConstraint.constant = max(heightDiff, 60)
    }

}

// MARK: - UIScrollViewDelegate
extension CircleDetailsViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollView.changeNavigationBarAlpha(using: circleImageView, to: navigationController?.navigationBar)
    }
    
}
