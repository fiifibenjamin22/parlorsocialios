//
//  CircleDetailsDestination.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 21/01/2020.
//

import UIKit

enum CircleDetailsDestination {
    case imageGallery([URL])
}

extension CircleDetailsDestination: Destination {
    
    var viewController: UIViewController {
        switch self {
        case .imageGallery(let urls):
            return ImagesGalleryViewController(withUrls: urls)
        }
    }
    
}
