//
//  CircleDetailsViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 31/05/2019.
//

import Foundation
import RxSwift

protocol CircleDetailsLogic: BaseViewModelLogic {
    var removeCircleClicks: PublishSubject<Void> { get }
    var circleObs: Observable<Circle> { get }
    var imageGalleryClicks: PublishSubject<Void> { get }
    var destinationsObservable: Observable<CircleDetailsDestination> { get }
}

class CircleDetailsViewModel: BaseViewModel {
    let analyticEventReferencedId: Int?
    private let profileRepository = ProfileRepository.shared
    private let circleSubject: BehaviorSubject<Circle>
    private let destinationsSubject: PublishSubject<CircleDetailsDestination> = PublishSubject()
    private let removeCircleFlowSubject: PublishSubject<Void> = .init()
    
    let textChangesSubject: BehaviorSubject<String> = BehaviorSubject(value: "")
    let removeCircleClicks: PublishSubject<Void> = PublishSubject()
    let imageGalleryClicks: PublishSubject<Void> = PublishSubject()

    init(withCircle circle: Circle) {
        self.analyticEventReferencedId = circle.id
        circleSubject = BehaviorSubject(value: circle)
        super.init()
        bindFlow()
    }
}

// MARK: Private logic
private extension CircleDetailsViewModel {
    
    private func bindFlow() {
        removeCircleClicks.withLatestFrom(circleSubject)
            .subscribe(onNext: { [unowned self] circle in
                self.alertDataSubject.onNext(AlertData(
                    icon: Icons.Profile.removeFromCircle,
                    title: String(format: Strings.Profile.confirmLeavingCircle.localized, circle.name),
                    message: nil,
                    bottomButtonTitle: Strings.confirm.localized.uppercased(),
                    buttonAction: { [unowned self] in self.removeCircleFlowSubject.emitElement() }
                ))
            }).disposed(by: disposeBag)
            
        removeCircleFlowSubject.withLatestFrom(circleSubject)
            .flatMapFirst { [unowned self] circle in
                self.profileRepository.removeMyCircle(withId: circle.id).showingProgressBar(with: self)
            }
            .doOnNext { [unowned self] response in
                switch response {
                case .success:
                    self.closeViewSubject.emitElement()
                case .failure(let error):
                    self.errorSubject.onNext(error)
                }
            }.subscribe().disposed(by: disposeBag)
        
        imageGalleryClicks
            .withLatestFrom(circleSubject)
            .doOnNext { [unowned self] circle in
                self.destinationsSubject.onNext(.imageGallery(circle.imageURLs.compactMap { $0 }))
            }.subscribe().disposed(by: disposeBag)
    }
    
}

// MARK: Interface logic

extension CircleDetailsViewModel: CircleDetailsLogic {
    
    var destinationsObservable: Observable<CircleDetailsDestination> {
        return destinationsSubject.asObservable()
    }
    
    var circleObs: Observable<Circle> {
        return circleSubject.asObservable()
    }
    
}
