//
//  CircleTableViewCell.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 24/05/2019.
//

import UIKit

class CircleTableViewCell: UITableViewCell {
    
    // MARK: - Views
    
    private lazy var cardView: UIView = {
        return UIView().manualLayoutable().apply {
            $0.layer.borderWidth = 0.5
            $0.applyTouchAnimation()
        }
    }()
    
    private lazy var circleImageView: UIImageView = {
        return UIImageView().manualLayoutable().apply {
            $0.contentMode = .scaleAspectFill
            $0.clipsToBounds = true
        }
    }()
    
    private lazy var circleTitleLabel: ParlorLabel = {
        return ParlorLabel.styled(
            withFont: UIFont.custom(ofSize: Constants.FontSize.hintSize, font: UIFont.Roboto.medium),
            alignment: .left, numberOfLines: 2)
    }()
    
    private lazy var selectIconImage: UIImageView = {
        return UIImageView(image: Icons.RsvpDetails.rightArrow).manualLayoutable().apply {
            $0.contentMode = .scaleAspectFit
            $0.setContentHuggingPriority(.required, for: .horizontal)
        }
    }()
    
    // MARK: - Initialization
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        backgroundColor = .clear
        selectionStyle = .none
        setupAutoLayout()
    }
    
    private func setupAutoLayout() {
        addSubview(cardView)
        
        cardView.apply {
            $0.addSubviews([circleImageView, circleTitleLabel, selectIconImage])
            $0.edgesToParent(insets: UIEdgeInsets(top: 0, left: Constants.Margin.standard, bottom: 0, right: Constants.Margin.standard))
        }
        
        circleImageView.apply {
            $0.edgesToParent(anchors: [.leading, .top, .bottom], insets: UIEdgeInsets(top: 10, left: 9, bottom: 10, right: 0))
            $0.widthAnchor.equalTo(constant: 120)
            $0.heightAnchor.equalTo(constant: 80)
        }
        
        circleTitleLabel.apply {
            $0.centerYAnchor.equal(to: cardView.centerYAnchor)
            $0.leadingAnchor.equal(to: circleImageView.trailingAnchor, constant: 23)
            $0.trailingAnchor.equal(to: selectIconImage.leadingAnchor, constant: -Constants.Margin.tiny)
        }
        
        selectIconImage.apply {
            let sideSize: CGFloat = 14
            $0.centerYAnchor.equal(to: cardView.centerYAnchor)
            $0.trailingAnchor.equal(to: cardView.trailingAnchor, constant: -20)
            $0.widthAnchor.equalTo(constant: sideSize)
            $0.heightAnchor.equalTo(constant: sideSize)

        }
    }
    
}

extension CircleTableViewCell: ConfigurableCell {
    typealias Model = Circle
    
    func configure(with model: Circle) {
        circleTitleLabel.text = model.name
        circleTitleLabel.textColor = PremiumColorManager.shared.currentTheme.profileItemContentTextColor
        configureCardViewColorTheme()
        if let image = model.imageURLs.first {
            circleImageView.getImage(from: image)
        }
    }

}

// MARK: - Configure views with color theme methods
extension CircleTableViewCell {
    
    private func configureCardViewColorTheme() {
        switch PremiumColorManager.shared.currentTheme {
        case .nonPremium:
            cardView.addLightBorder()
        case .premium:
            cardView.addDarkBorder()
        }
        cardView.backgroundColor = PremiumColorManager.shared.currentTheme.profileItemBackgroundColor
    }
    
}
