//
//  CriclesSection.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 24/05/2019.
//

import Foundation
import UIKit

final class CirclesSection: BasicTableSection<Circle, CircleTableViewCell> {
    
    override var headerType: UITableViewHeaderFooterView.Type? { return ProfileTabHeaderView.self }
    
    override var footerType: UITableViewHeaderFooterView.Type? { return NoDataFooter.self }
    
    override func configure(header: UITableViewHeaderFooterView, in section: Int) {
        guard let sectionHeader = header as? ProfileTabHeaderView else {
            fatalError("Header is wrong type! Needed: \(ProfileTabHeaderView.name) but get instead \(String(describing: header))")
        }
        sectionHeader.apply {
            $0.label.text = Strings.Profile.yourCircles.localized
            $0.label.textColor = PremiumColorManager.shared.currentTheme.profileTabHeaderLabelColor
            $0.contentView.backgroundColor = PremiumColorManager.shared.currentTheme.profileMainTableViewBackgroundColor
        }
    }
    
    override func configure(footer: UITableViewHeaderFooterView, in section: Int) {
        guard let sectionFooter = footer as? NoDataFooter else {
            fatalError("Footer is wrong type! Needed: \(NoDataFooter.name) but get instead \(String(describing: footer))")
        }
        sectionFooter.noDataTitle.text = Strings.Profile.noCirclesTitle.localized
        sectionFooter.noDataTitle.textColor = PremiumColorManager.shared.currentTheme.profileItemContentTextColor
        sectionFooter.noDataMessage.text = Strings.Profile.noCirclesMessage.localized
        sectionFooter.noDataMessage.textColor = PremiumColorManager.shared.currentTheme.profileEmptyCircleTextColor
        sectionFooter.isVisible = self.items.isEmpty
        configureCardViewColorTheme(footer: sectionFooter)
    }
    
    private func configureCardViewColorTheme(footer: NoDataFooter) {
        switch PremiumColorManager.shared.currentTheme {
        case .nonPremium:
            footer.cardView.addLightBorder()
        case .premium:
            footer.cardView.addDarkBorder()
        }
        footer.cardView.backgroundColor = PremiumColorManager.shared.currentTheme.profileItemBackgroundColor
    }
}

extension CirclesSection { }
