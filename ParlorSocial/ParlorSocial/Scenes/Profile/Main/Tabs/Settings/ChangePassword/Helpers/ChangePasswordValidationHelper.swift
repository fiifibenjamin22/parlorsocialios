//
//  ChangePasswordValidationHelper.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 04/02/2020.
//

import Foundation

class ChangePasswordValidationHelper {
    
    // MARK: - Execution
    func validate(oldPassword: String, newPassword: String, validator: Validator) -> Validator.Result {
        let oldPasswordValidatedInput = ValidatedInput(rules: [StringNotBlank()], value: oldPassword, name: Strings.ChangePassword.currentPass.localized)
        let newPasswordValidatedInput = ValidatedInput(rules: [StringNotBlank(), StringPasswordPower()], value: newPassword, name: Strings.ChangePassword.newPass.localized)
        
        return validator.validateWithResult(for: [oldPasswordValidatedInput, newPasswordValidatedInput])
    }
    
}
