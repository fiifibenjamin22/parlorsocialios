//
//  ChangePasswordViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 06/06/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class ChangePasswordViewBuilder {

    private unowned let controller: ChangePasswordViewController
    private var view: UIView! { return controller.view }

    init(controller: ChangePasswordViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension ChangePasswordViewBuilder {

    private func setupProperties() {
        view.backgroundColor = .appBackground
        
        controller.apply {
            $0.updateButton.setTitle(Strings.ChangePassword.update.localized.uppercased(), for: .normal)
            $0.newPasswordField.placeholder = Strings.ChangePassword.newPass.localized
            $0.currentPasswordField.placeholder = Strings.ChangePassword.currentPass.localized
        }
    }

    private func setupHierarchy() {
        controller.apply {
            view.addSubviews([$0.stackBackground, $0.updateButton])
            $0.stackBackground.addSubviews([$0.currentPasswordField, $0.newPasswordField])
        }
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.stackBackground.edgesToParent(
                anchors: [.leading, .top, .trailing],
                insets: UIEdgeInsets.margins(top: 1)
            )
            
            $0.currentPasswordField.edgesToParent(
                anchors: [.leading, .trailing, .top],
                insets: .margins(
                    top: 30,
                    left: Constants.Margin.standard,
                    right: Constants.Margin.standard
                )
            )
            
            $0.newPasswordField.topAnchor.equal(to: $0.currentPasswordField.bottomAnchor, constant: -1)
            $0.newPasswordField.edgesToParent(
                anchors: [.leading, .trailing, .bottom],
                insets: .margins(
                    left: Constants.Margin.standard,
                    bottom: 30,
                    right: Constants.Margin.standard
                )
            )
            
            $0.updateButton.edgesToParent(
                anchors: [.leading, .trailing, .bottom],
                insets: UIEdgeInsets.margins(
                    left: Constants.Margin.standard,
                    bottom: 38,
                    right: Constants.Margin.standard
                )
            )
        }
    }

}

// MARK: - Public build methods
extension ChangePasswordViewBuilder {
    
    func buildTextField() -> LoginTextField {
        return LoginTextField().apply {
            $0.isPasswordField = true
            $0.textColor = .appText
            $0.font = UIFont.custom(ofSize: Constants.FontSize.hintSize, font: UIFont.Roboto.regular)
            $0.heightAnchor.equalTo(constant: 65)
            $0.layer.borderWidth = 1
            $0.layer.borderColor = UIColor.textVeryLight.cgColor
            $0.padding = UIEdgeInsets(top: 0, left: Constants.Margin.standard, bottom: 0, right: Constants.Margin.standard)
        }.manualLayoutable()
    }
    
    func buildStackBackground() -> UIView {
        return UIView().manualLayoutable().apply {
            $0.backgroundColor = .white
        }
    }
    
    func buildMainStack() -> UIStackView {
        return UIStackView(
            axis: .vertical,
            with: [],
            spacing: 0,
            alignment: .fill,
            distribution: .equalSpacing
            ).manualLayoutable()
    }
    
    func buildUpdatePasswordButton() -> RoundedButton {
        return RoundedButton.withBackgroundStyle().manualLayoutable()
    }
    
}
