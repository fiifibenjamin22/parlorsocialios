//
//  ChangePasswordViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 06/06/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxSwift

protocol ChangePasswordLogic: BaseViewModelLogic {
    var currentPasswordRelay: BehaviorSubject<String> { get }
    var newPasswordRelay: BehaviorSubject<String> { get }
    var changeClicks: PublishSubject<Void> { get }
}

class ChangePasswordViewModel: BaseViewModel {
    let analyticEventReferencedId: Int? = nil
    private let profileRepository = ProfileRepository.shared
    private let changePasswordValidationHelper = ChangePasswordValidationHelper()
    private let validator = Validator()
    
    let currentPasswordRelay: BehaviorSubject<String> = BehaviorSubject(value: "")
    let newPasswordRelay: BehaviorSubject<String> = BehaviorSubject(value: "")
    let changeClicks: PublishSubject<Void> = PublishSubject()

    override init() {
        super.init()
        initFlow()
    }
}

// MARK: Private logic
private extension ChangePasswordViewModel {
    
    private func initFlow() {
        changeClicks
            .withLatestFrom(currentPasswordRelay)
            .withLatestFrom(newPasswordRelay) { ($0, $1) }
            .subscribe(onNext: { [unowned self] response in
                self.changePasswordValidation(oldPassword: response.0, newPassword: response.1)
            }).disposed(by: disposeBag)
    }
    
    private func sendChangePasswordRequest(request: ChangePasswordRequest) {
        profileRepository.changePassword(using: request)
            .showingProgressBar(with: self)
            .doOnNext { [unowned self] response in
                self.handleResponse(response)}
            .subscribe().disposed(by: disposeBag)
    }
    
    private func handleResponse(_ response: ChangePasswordResponse) {
        switch response {
        case .success(let data):
            alertDataSubject.onNext(AlertData(
                icon: Icons.ChangePassword.changePasswordSuccess,
                title: data.message,
                message: nil,
                bottomButtonTitle: Strings.ok.rawValue.uppercased(),
                buttonAction: { [ unowned self] in self.closeViewSubject.emitElement() }))
        case .failure(let error):
            errorSubject.onNext(error)
        }
    }
    
    private func changePasswordValidation(oldPassword: String, newPassword: String) {
        let result = changePasswordValidationHelper.validate(oldPassword: oldPassword, newPassword: newPassword, validator: validator)
        switch result {
        case .success:
            sendChangePasswordRequest(request: ChangePasswordRequest(password: newPassword, oldPassword: oldPassword))
        case .failure(let error):
            errorSubject.onNext(error)
        }
    }
}

// MARK: Interface logic methods
extension ChangePasswordViewModel: ChangePasswordLogic {
    
}
