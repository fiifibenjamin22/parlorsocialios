//
//  ChangePasswordViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 06/06/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

class ChangePasswordViewController: AppViewController {
      
    // MARK: - Views
    private(set) var stackBackground: UIView!
    private(set) var currentPasswordField: ParlorTextField!
    private(set) var newPasswordField: ParlorTextField!
    private(set) var updateButton: RoundedButton!

    // MARK: - Properties
    let analyticEventScreen: AnalyticEventScreen? = .changePassword
    private var viewModel: ChangePasswordLogic!
    
    override var observableSources: ObservableSources? {
        return viewModel
    }
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }

    // MARK: - Initialization
    init() {
        super.init(nibName: nil, bundle: nil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() {
        self.viewModel = ChangePasswordViewModel()
    }
    
    override func bindUI() {
        super.bindUI()
        newPasswordField.rx.text.ignoreNil().bind(to: viewModel.newPasswordRelay).disposed(by: disposeBag)
        currentPasswordField.rx.text.ignoreNil().bind(to: viewModel.currentPasswordRelay).disposed(by: disposeBag)
        updateButton.rx.controlEvent(.touchUpInside).bind(to: viewModel.changeClicks).disposed(by: disposeBag)
    }
    
    override func observeViewModel() {
        super.observeViewModel()
        
        viewModel.closeObs
            .doOnNext { [unowned self] in
                self.navigationController?.popViewController(animated: true)
            }.subscribe().disposed(by: disposeBag)
    }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.applyProfileItemAppearance(withTitle: Strings.ChangePassword.title.localized.uppercased())
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        statusBar?.backgroundColor = .white
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
}

// MARK: - Private methods
extension ChangePasswordViewController {

    private func setupViews() {
        let builder = ChangePasswordViewBuilder(controller: self)
        self.currentPasswordField = builder.buildTextField()
        self.newPasswordField = builder.buildTextField()
        self.stackBackground = builder.buildStackBackground()
        self.updateButton = builder.buildUpdatePasswordButton()
        builder.setupViews()
    }

    private func setupPresentationLogic() {

    }

}
