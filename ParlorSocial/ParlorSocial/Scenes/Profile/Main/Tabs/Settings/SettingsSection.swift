//
//  SettingsSection.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 05/06/2019.
//

import Foundation
import UIKit

final class SettingsSection: BasicTableSection<SettingItem, SettingItemCell> {
    
    var logoutCallback: () -> Void = { }
    
    init(items: [SettingItem],
         logoutCallback: @escaping () -> Void,
         itemSelector: @escaping ((SettingItem) -> Void) = { _ in }) {
        self.logoutCallback = logoutCallback
        super.init(items: items, itemSelector: itemSelector)
    }
    
    override var footerType: UITableViewHeaderFooterView.Type? { return SettingsSectionFooter.self }
    
    override var headerType: UITableViewHeaderFooterView.Type? { return ProfileTabHeaderView.self }
    
    override func configure(header: UITableViewHeaderFooterView, in section: Int) {
        guard let sectionHeader = header as? ProfileTabHeaderView else {
            fatalError("Header is wrong type! Needed: \(ProfileTabHeaderView.name) but get instead \(String(describing: header))")
        }
        sectionHeader.apply {
            $0.label.text = Strings.Profile.settings.localized
            $0.label.textColor = PremiumColorManager.shared.currentTheme.profileTabHeaderLabelColor
            $0.contentView.backgroundColor = PremiumColorManager.shared.currentTheme.profileMainTableViewBackgroundColor
        }
    }
    
    override func configure(footer: UITableViewHeaderFooterView, in section: Int) {
        guard let sectionFooter = footer as? SettingsSectionFooter else {
            fatalError("Header is wrong type! Needed: \(SettingsSectionFooter.name) but get instead \(String(describing: footer))")
        }
        sectionFooter.logoutButton.apply {
            $0.addTarget(self, action: #selector(didTapLogout), for: .touchUpInside)
            $0.setTitleColor(PremiumColorManager.shared.currentTheme.roundedButtonTextColor, for: .normal)
            $0.layer.borderColor = PremiumColorManager.shared.currentTheme.roundedButtonBorderColor.cgColor
        }
        sectionFooter.versionLabel.textColor = PremiumColorManager.shared.currentTheme.profilePositionTextColor
        sectionFooter.contentView.backgroundColor = PremiumColorManager.shared.currentTheme.profileMainTableViewBackgroundColor
    }
    
    @objc private func didTapLogout() {
        logoutCallback()
    }
    
}
