//
//  SettingsSectionFooter.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 05/06/2019.
//

import UIKit

class SettingsSectionFooter: UITableViewHeaderFooterView {
    
    // MARK: - Views
    lazy var logoutButton: RoundedButton = {
        return RoundedButton.withBackgroundStyle().manualLayoutable().apply {
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium)
            $0.backgroundColor = .clear
            $0.trackingId = AnalyticEventClickableElementName.Profile.logout.rawValue
        }
    }()
    
    lazy var versionLabel: ParlorLabel = {
        return ParlorLabel.styled(
            withFont: UIFont.custom(ofSize: Constants.FontSize.xTiny, font: UIFont.Roboto.regular),
            alignment: .center,
            numberOfLines: 1)
    }()
    
    // MARK: - Properties
    var versionText: String {
        var version = "v\(Config.appVersion)"
        if let buildNumber = Config.buildNumber {
            version.append("(#\(buildNumber))")
        }
        return version
    }
    
    // MARK: - Initialization
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        contentView.addSubviews([logoutButton, versionLabel])
        logoutButton.setTitle(Strings.Settings.logout.localized.uppercased(), for: .normal)
        versionLabel.text = versionText
        setupAutoLayout()
    }
    
    private func setupAutoLayout() {
        logoutButton.edgesToParent(
            anchors: [.leading, .trailing, .top],
            insets: UIEdgeInsets.margins(top: 43, left: Constants.Margin.standard, right: Constants.Margin.standard)
        )
        versionLabel.topAnchor.equal(to: logoutButton.bottomAnchor, constant: 20)
        versionLabel.edgesToParent(
            anchors: [.leading, .trailing, .bottom],
            insets: UIEdgeInsets.margins(bottom: 45)
        )
    }
    
}
