//
//  CardDataValidationHelper.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 10/02/2020.
//

import Foundation

class CardDataValidationHelper {
    
    // MARK: - Validation
    func validate(cardUploadData: CardUploadData, validator: Validator) -> Validator.Result {
        let cardNumberValidatedInput = ValidatedInput(rules: [StringNotBlank()], value: cardUploadData.number, name: Strings.Cards.Add.number.localized)
        let cardMonthValidatedInput = ValidatedInput(rules: [CardMonthIsAfterCurrent(year: cardUploadData.expYear)],
                                                     value: cardUploadData.expMonth, name: "")
        let cardYearValidatedInput = ValidatedInput(rules: [CardYearIsAfterCurrent()], value: cardUploadData.expYear, name: "")
        let cvvValidatedInput = ValidatedInput(rules: [StringNotBlank(), StringMinLength(3), StringMaxLength(4)], value: cardUploadData.cvc, name: Strings.Cards.Add.cvv.localized)
        let nameValidatedInput = ValidatedInput(rules: [StringNotBlank(), StringMaxLength(128)], value: cardUploadData.name, name: Strings.Cards.Add.name.localized)
        let firstAddressValidatedInput = ValidatedInput(rules: [StringNotBlank(), StringMaxLength(200)], value: cardUploadData.addressLine1, name: Strings.Cards.Add.address1.localized)
        let secondAddressValidatedInput = ValidatedInput(rules: [StringMaxLength(200)], value: cardUploadData.addressLine2, name: Strings.Cards.Add.address2.localized)
        let cityValidatedInput = ValidatedInput(rules: [StringNotBlank(), StringMaxLength(200)], value: cardUploadData.addressCity, name: Strings.Cards.Add.city.localized)
        let stateValidatedInput = ValidatedInput(rules: [StringNotBlank(), StringMaxLength(200)], value: cardUploadData.addressState, name: Strings.Cards.Add.state.localized)
        let zipValidatedInput = ValidatedInput(rules: [StringNotBlank(), StringMinLength(3), StringMaxLength(10)], value: cardUploadData.addressZip, name: Strings.Cards.Add.zip.localized)
        
        let stringValidatedOutputs = validator.validate(validatedInput: [cardNumberValidatedInput])
        let intValidatedOutputs = validator.validate(validatedInput: [cardMonthValidatedInput, cardYearValidatedInput])
        let secondStringValidatedOutputs = validator.validate(validatedInput: [
            cvvValidatedInput,
            nameValidatedInput,
            firstAddressValidatedInput,
            secondAddressValidatedInput,
            cityValidatedInput,
            stateValidatedInput,
            zipValidatedInput
        ])
        return validator.getResultFrom(validatedOutputs: stringValidatedOutputs + intValidatedOutputs + secondStringValidatedOutputs)
    }
    
    func validate(editCardRequest: EditCardRequest, validator: Validator) -> Validator.Result {
        let nameValidatedInput = ValidatedInput(rules: [StringNotBlank(), StringMaxLength(128)], value: editCardRequest.name, name: Strings.Cards.Add.name.localized)
        let firstAddressValidatedInput = ValidatedInput(rules: [StringNotBlank(), StringMaxLength(200)], value: editCardRequest.addressLine1, name: Strings.Cards.Add.address1.localized)
        let secondAddressValidatedInput = ValidatedInput(rules: [StringMaxLength(200)], value: editCardRequest.addressLine2, name: Strings.Cards.Add.address2.localized)
        let cityValidatedInput = ValidatedInput(rules: [StringNotBlank(), StringMaxLength(200)], value: editCardRequest.addressCity, name: Strings.Cards.Add.city.localized)
        let stateValidatedInput = ValidatedInput(rules: [StringNotBlank(), StringMaxLength(200)], value: editCardRequest.addressState, name: Strings.Cards.Add.state.localized)
        let zipValidatedInput = ValidatedInput(rules: [StringNotBlank(), StringMinLength(3), StringMaxLength(10)], value: editCardRequest.addressZip, name: Strings.Cards.Add.zip.localized)
        
        return validator.validateWithResult(for: [
            nameValidatedInput,
            secondAddressValidatedInput,
            firstAddressValidatedInput,
            secondAddressValidatedInput,
            cityValidatedInput,
            stateValidatedInput,
            zipValidatedInput
        ])
    }
    
}
