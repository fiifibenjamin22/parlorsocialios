//
//  CreditCardListHeaderView.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 11/07/2019.
//

import UIKit

final class CreditCardListHeaderView: UITableViewHeaderFooterView {
    
    private(set) lazy var methodsLabel: ParlorLabel = {
        return ParlorLabel.styled(
            withTextColor: .appText,
            withFont: UIFont.custom(ofSize: Constants.FontSize.hintSize, font: UIFont.Roboto.medium),
            alignment: .left,
            numberOfLines: 1
            )
    }()
    
    private(set) lazy var topSeperator: UIView = {
        return UIView().manualLayoutable().apply {
            $0.heightAnchor.equalTo(constant: 1)
            $0.backgroundColor = .appBackground
        }
    }()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        backgroundColor = .white
        methodsLabel.text = Strings.Cards.paymentMethods.localized
        layoutViews()
    }
    
    private func layoutViews() {
        addSubviews([methodsLabel, topSeperator])
        
        methodsLabel.edgesToParent(
            anchors: [.leading, .trailing, .top],
            insets: .margins(top: 28, left: Constants.Margin.standard, right: Constants.Margin.standard)
        )
        
        topSeperator.apply {
            $0.edgesToParent(anchors: [.leading, .trailing, .bottom])
            $0.topAnchor.equal(to: methodsLabel.bottomAnchor, constant: 21)
        }
    }
    
}
