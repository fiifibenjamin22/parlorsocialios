//  CardManagementService.swift
//  ParlorSocialClub
//
//  Created on 16/04/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import RxSwift

class CardManagementService {
    
    // MARK: - Properties
    
    private let repository: ProfileRepositoryProtocol
    private let cardDataValidationHelper = CardDataValidationHelper()
    private let validator = Validator()
    private let facebookAnalyticsService = FacebookAnalyticService()
    private let firebaseAnalyticsService = FirebaseAnalyticService()

    // MARK: - Init
    
    init(repository: ProfileRepositoryProtocol = ProfileRepository.shared) {
        self.repository = repository
    }
    
    // MARK: - Public methods
    
    func addCard(data: CardUploadData) throws -> Observable<SingleCardResponse> {
        let result = validateAddCardRequestData(data)
        switch result {
        case .success:
            return repository.addCard(withData: data)
                .doOnNext { [weak self] response in self?.logPaymentMethodEvent(response: response) }
        case .failure(let error):
            throw error.setErrorType(errorType: .payment)
        }
    }
    
    func addCard(usingToken token: CardTokenRequest) -> Observable<SingleCardResponse> {
        return repository.addCard(usingToken: token)
            .doOnNext { [weak self] response in self?.logPaymentMethodEvent(response: response) }
    }
    
    func editCard(withId id: Int, data: CardUploadData) throws -> Observable<SingleCardResponse> {
        let request = EditCardRequest.from(cardUploadData: data)
        let result = validateEditCardRequestData(request)
        switch result {
        case .success:
            return repository.editCard(withData: request, cardId: id)
        case .failure(let error):
            throw error.setErrorType(errorType: .payment)
        }
    }
    
    // MARK: - Private methods
    
    private func validateAddCardRequestData(_ requestData: CardUploadData) -> Validator.Result {
        let result = cardDataValidationHelper.validate(cardUploadData: requestData, validator: validator)
        return result
    }
    
    private func validateEditCardRequestData(_ requestData: EditCardRequest) -> Validator.Result {
        let result = cardDataValidationHelper.validate(editCardRequest: requestData, validator: validator)
        return result
    }
    
    // MARK: - Events logging
    
    private func logPaymentMethodEvent(response: SingleCardResponse) {
        switch response {
        case .success:
            facebookAnalyticsService.logPaymentMethodEvent(isSuccess: true)
            firebaseAnalyticsService.logPaymentMethodEvent(isSuccess: true)
        case .failure:
            facebookAnalyticsService.logPaymentMethodEvent(isSuccess: false)
            firebaseAnalyticsService.logPaymentMethodEvent(isSuccess: false)
        }
    }
}
