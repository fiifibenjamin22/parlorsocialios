//
//  CreditCardsSection.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 19/06/2019.
//

import UIKit
import RxSwift

class CreditCardsSection: BasicTableSection<CardData, CreditCardTableCell> {
    
    private let defaultCardChangeSubject: PublishSubject<CardData>
    private let removeCardSubject: PublishSubject<CardData>
    private let editCardSubject: PublishSubject<CardData>
    
    override var headerType: UITableViewHeaderFooterView.Type? { return CreditCardListHeaderView.self }

    init(items: [CardData],
         defaultCardChangeSubject: PublishSubject<CardData>,
         removeCardSubject: PublishSubject<CardData>,
         editCardSubject: PublishSubject<CardData>,
         itemSelector: @escaping ((CardData) -> Void) = { _ in }) {
        self.defaultCardChangeSubject = defaultCardChangeSubject
        self.removeCardSubject = removeCardSubject
        self.editCardSubject = editCardSubject
        super.init(items: items, itemSelector: itemSelector)
    }

    override func configure(cell: UITableViewCell, at indexPath: IndexPath) {
        super.configure(cell: cell, at: indexPath)
        guard let tableCell = cell as? CreditCardTableCell else { return }
        let cardData = self.items[indexPath.row]
        
        tableCell.apply {
            $0.didTryToChangeDefaultCardCallback = createCallback(withData: cardData, for: defaultCardChangeSubject)
            $0.removeClickCallback = createCallback(withData: cardData, for: removeCardSubject)
            $0.editClickCallback = createCallback(withData: cardData, for: editCardSubject)
        }
    }
    
    private func createCallback(withData cardData: CardData, for subject: PublishSubject<CardData>) -> (() -> Void) {
        return { [unowned subject] in
            subject.onNext(cardData)
        }
    }
}
