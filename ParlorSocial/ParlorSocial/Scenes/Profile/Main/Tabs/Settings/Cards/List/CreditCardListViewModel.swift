//
//  CreditCardListViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 19/06/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import RxSwift

protocol CreditCardListLogic: BaseViewModelLogic {
    var sectionsObs: Observable<[TableSection]> { get }
    var addClicks: PublishSubject<Void> { get }
    var destinationObs: Observable<CardDestination> { get }
}

class CreditCardListViewModel: BaseViewModel {
    let analyticEventReferencedId: Int? = nil
    private let profileRepository = ProfileRepository.shared
    private let cardDataSubject: ReplaySubject<[CardData]> = ReplaySubject.create(bufferSize: 1)
    private let defaultCardChangeSubject: PublishSubject<CardData> = PublishSubject()
    private let removeCardSubject: PublishSubject<CardData> = PublishSubject()
    private let editCardSubject: PublishSubject<CardData> = PublishSubject()
    private let destinationsSubject: PublishSubject<CardDestination> = PublishSubject()

    let addClicks: PublishSubject<Void> = PublishSubject()
    
    override init() {
        super.init()
        initFlow()
    }
}

// MARK: Private logic
private extension CreditCardListViewModel {
    
    private var cardsDownloadObs: Observable<CardsResponse> {
        return profileRepository
            .getUserCards()
            .doOnNext { [unowned self] response in
                switch response {
                case .success(let data):
                    self.cardDataSubject.onNext(data.data)
                case .failure(let error):
                    self.errorSubject.onNext(error.setErrorType(errorType: .payment))
                }
        }
    }
    
    private func initFlow() {
        cardsDownloadObs.subscribe().disposed(by: disposeBag)
        profileRepository.knownCardsObs.bind(to: cardDataSubject).disposed(by: disposeBag)
        
        defaultCardChangeSubject
            .distinctUntilChanged { $0.id == $1.id }
            .flatMapLatest { [unowned self] card -> Observable<SingleCardResponse> in
                return self.profileRepository.changeDefaultCard(to: card.id)
                    .doOnNext { [unowned self] response in
                        self.handleSingleCardResponse(response: response)
                }
            }
            .subscribe()
            .disposed(by: disposeBag)
        
        removeCardSubject
            .flatMap { [unowned self] data in
                Observable.just(data)
                    .flatMapWithConfirmation(
                        using: self.alertDataSubject,
                        alertData: AlertData.removeCardAlert(withLastFour: data.cardLastFour)
                )
            }
            .withLatestFrom(cardDataSubject) { ($0, $1) }
            .flatMap { [unowned self] (arg) -> Observable<EmptyResponse> in
                let (card, currentCards) = arg
                self.cardDataSubject.onNext(currentCards.withoutCard(withId: card.id))
                return self.profileRepository.removeCard(withId: card.id).showingProgressBar(with: self)
                    .doOnNext { [unowned self] response in
                        self.handleEmptyResponse(response: response)
                }
            }
            .flatMap { [unowned self] _ in self.cardsDownloadObs }
            .subscribe(onNext: handleCardResponse)
            .disposed(by: disposeBag)
        
        editCardSubject
            .flatMap { [unowned self] card -> Observable<CardDetailsResponse> in
                return self.profileRepository.getCardDetails(withId: card.id).showingProgressBar(with: self)
                    .doOnNext { [unowned self] response in
                        self.handleCardDetailsResponse(response: response)
                }
            }
            .subscribe()
            .disposed(by: disposeBag)
        
        addClicks
            .doOnNext { [unowned self] in self.destinationsSubject.onNext(.add(withApplyingType: .add)) }
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    private func handleCardResponse(response: CardsResponse) {
        switch response {
        case .success:
            break
        case .failure(let error):
            self.errorSubject.onNext(error.setErrorType(errorType: .payment))
        }
    }
    
    private func handleEmptyResponse(response: EmptyResponse) {
        switch response {
        case .success:
            break
        case .failure(let error):
            self.errorSubject.onNext(error.setErrorType(errorType: .payment))
        }
    }
    
    private func handleSingleCardResponse(response: SingleCardResponse) {
        switch response {
        case .success:
            break
        case .failure(let error):
            self.errorSubject.onNext(error.setErrorType(errorType: .payment))
        }
    }
    
    private func handleCardDetailsResponse(response: CardDetailsResponse) {
        switch response {
        case .success(let response):
            destinationsSubject.onNext(.edit(cardDetails: response.data))
        case .failure(let error):
            self.errorSubject.onNext(error.setErrorType(errorType: .payment))
        }
    }
    
}

// MARK: Interface logic methods
extension CreditCardListViewModel: CreditCardListLogic {
    
    var destinationObs: Observable<CardDestination> {
        return destinationsSubject.asObservable()
    }
    
    var sectionsObs: Observable<[TableSection]> {
        return cardDataSubject.map { [unowned self] in [CreditCardsSection(
            items: $0,
            defaultCardChangeSubject: self.defaultCardChangeSubject,
            removeCardSubject: self.removeCardSubject,
            editCardSubject: self.editCardSubject
            )] }
    }
    
}

fileprivate extension Array where Element == CardData {
    
    func withDefaultCard(defaultId: Int) -> [CardData] {
        return self.map { card in
            return CardData(
                id: card.id,
                cardBrand: card.cardBrand,
                cardLastFour: card.cardLastFour,
                isDefault: (defaultId == card.id)
            )
        }
    }
    
    func withoutCard(withId cardId: Int) -> [CardData] {
        return self.filter { $0.id != cardId }
    }
    
}
