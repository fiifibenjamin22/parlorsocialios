//
//  AddCardViewType.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 11/07/2019.
//

enum AddCardViewType {
    case add
    case edit
}
