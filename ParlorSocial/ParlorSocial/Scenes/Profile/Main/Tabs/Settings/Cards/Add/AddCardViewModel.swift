//
//  AddCardViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 24/06/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import RxSwift
import RxCocoa

protocol AddCardLogic: BaseViewModelLogic {
    var stringFieldChangeRelay: PublishSubject<(CardUploadStringField, String)> { get }
    var intFieldChangeRelay: PublishSubject<(CardUploadIntField, Int?)> { get }
    var addCardClicks: PublishSubject<Void> { get }
    var destiantionObs: Observable<CardDestination> { get }
    var editCardDataDetailsObs: Observable<CardDataDetails> { get }
    var viewTypeObs: Observable<AddCardViewType> { get }
}

class AddCardViewModel: BaseViewModel {
    let analyticEventReferencedId: Int? = nil
    private let cardManagementService: CardManagementService = CardManagementService()
    private let cardUploadDataSubject: BehaviorSubject<CardUploadData> = BehaviorSubject(value: CardUploadData.empty())
    private let destiantionSubject: PublishSubject<CardDestination> = PublishSubject()
    private let editCardDataDetailsRelay: BehaviorRelay<CardDataDetails> = BehaviorRelay(value: CardDataDetails.empty())
    private let viewTypeRelay: BehaviorRelay<AddCardViewType> = .init(value: .add)
    
    let stringFieldChangeRelay: PublishSubject<(CardUploadStringField, String)> = PublishSubject()
    let intFieldChangeRelay: PublishSubject<(CardUploadIntField, Int?)> = PublishSubject()
    let addCardClicks: PublishSubject<Void> = PublishSubject()
    let cancelClicks: PublishSubject<Void> = .init()
    
    init(withApplyingType type: AddCardViewType) {
        viewTypeRelay.accept(type)
        super.init()
        initFlow()
    }
    
    init(with cardDetails: CardDataDetails) {
        viewTypeRelay.accept(.edit)
        editCardDataDetailsRelay.accept(cardDetails)
        super.init()
        initFlow()
    }
}

// MARK: Private logic
private extension AddCardViewModel {
    
    private func initFlow() {
        stringFieldChangeRelay
            .withLatestFrom(cardUploadDataSubject) { ($0, $1) }
            .map { stringFieldChangeData, currentData in
                currentData.withStringField(ofType: stringFieldChangeData.0, value: stringFieldChangeData.1)
            }.bind(to: cardUploadDataSubject).disposed(by: disposeBag)
        
        intFieldChangeRelay
            .withLatestFrom(cardUploadDataSubject) { ($0, $1) }
            .map { intFieldData, currentData in
                return currentData.withIntField(ofType: intFieldData.0, value: intFieldData.1)
            }.bind(to: cardUploadDataSubject).disposed(by: disposeBag)
        
        addCardClicks
            .withLatestFrom(cardUploadDataSubject)
            .withLatestFrom(viewTypeRelay) { ($0, $1) }
            .subscribe(onNext: { [unowned self] response in
                self.performCardAction(usingData: response.0, forViewType: response.1)
            }).disposed(by: disposeBag)
    }
    
    private func performCardAction(usingData data: CardUploadData, forViewType type: AddCardViewType) {
        switch type {
        case .add:
            addCard(usingData: data)
        case .edit:
            editCard(usingData: data)
        }
    }

    private func addCard(usingData data: CardUploadData) {
        do {
            try cardManagementService.addCard(data: data)
                .showingProgressBar(with: self)
                .subscribe(onNext: { [unowned self] response in
                    self.handleCardResposne(response)
                })
                .disposed(by: disposeBag)
        } catch let error as AppError {
            errorSubject.onNext(error)
        } catch {
            errorSubject.onNext(AppError(with: error))
        }
    }

    private func editCard(usingData data: CardUploadData) {
        let cardId = editCardDataDetailsRelay.value.id
        do {
            try cardManagementService.editCard(withId: cardId, data: data)
                .showingProgressBar(with: self)
                .subscribe(onNext: { [unowned self] response in
                    self.handleCardResposne(response)
                })
                .disposed(by: disposeBag)
        } catch let error as AppError {
            errorSubject.onNext(error)
        } catch {
            errorSubject.onNext(AppError(with: error))
        }
    }

    private func handleCardResposne(_ response: SingleCardResponse) {
        switch response {
        case .success:
            destiantionSubject.onNext(CardDestination.close)
        case .failure(let error):
            errorSubject.onNext(error.setErrorType(errorType: .payment))
        }
    }
}

// MARK: Interface logic methods
extension AddCardViewModel: AddCardLogic {

    var viewTypeObs: Observable<AddCardViewType> {
        return viewTypeRelay.asObservable()
    }
    
    var editCardDataDetailsObs: Observable<CardDataDetails> {
        return editCardDataDetailsRelay.asObservable()
    }
    
    var destiantionObs: Observable<CardDestination> {
        return destiantionSubject.asObservable()
    }
}
