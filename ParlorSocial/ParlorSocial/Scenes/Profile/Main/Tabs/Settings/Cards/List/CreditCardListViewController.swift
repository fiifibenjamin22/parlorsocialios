//
//  CreditCardListViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 19/06/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

class CreditCardListViewController: AppViewController {
    
    // MARK: - Views
    private(set) var tableView: UITableView!
    private(set) var bottomSeperator: UIView!
    private(set) var stripeButton: StripeButton!
    private(set) var addButton: RoundedButton!
    private(set) var fixedTableHeightConstraint: NSLayoutConstraint!

    // MARK: - Properties
    let analyticEventScreen: AnalyticEventScreen? = .creditCardsList
    private var viewModel: CreditCardListLogic!
    private var tableManager: TableViewManager!
    
    override var observableSources: ObservableSources? {
        return viewModel
    }
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }

    // MARK: - Initialization
    init() {
        super.init(nibName: nil, bundle: nil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() {
        self.viewModel = CreditCardListViewModel()
    }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }
    
    override func observeViewModel() {
        super.observeViewModel()
        viewModel
            .sectionsObs
            .doOnNext { [unowned self] sections in
                self.tableManager.setupWith(sections: sections)
                self.tableManager.hideLoadingFooter()
            }.subscribe().disposed(by: disposeBag)
        
        viewModel
            .destinationObs
            .doOnNext { [unowned self] destination in
                self.handleDestination(destination)
            }.subscribe().disposed(by: disposeBag)
    }
    
    override func bindUI() {
        super.bindUI()
        addButton.rx.controlEvent(.touchUpInside)
            .bind(to: viewModel.addClicks)
            .disposed(by: disposeBag)
    }

    override func viewDidLoad() {
        self.tableManager = TableViewManager(tableView: tableView, delegate: self)
        super.viewDidLoad()
        setupPresentationLogic()
        observeTableContent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        adjustNavigationBar()
    }

}

// MARK: - Private methods
extension CreditCardListViewController {

    private func setupViews() {
        let builder = CreditCardListViewBuilder(controller: self)
        self.tableView = builder.buildTableView()
        self.addButton = builder.buildAddPaymentButton()
        self.bottomSeperator = builder.buildSeperator()
        self.stripeButton = builder.buildStripeButton()
        self.fixedTableHeightConstraint = builder.buidFixedTableHeightConstraint()
        builder.setupViews()
    }
    
    private func handleDestination(_ destination: CardDestination) {
        switch destination {
        case .add:
            router.push(destination: destination)
        case .edit:
            router.push(destination: destination)
        case .close:
            navigationController?.popViewController(animated: true)
        default:
            break
        }
    }

    private func setupPresentationLogic() {

    }
    
    private func observeTableContent() {
        tableView
            .rx.observeWeakly(CGSize.self, "contentSize")
            .ignoreNil()
            .map { $0.height }
            .doOnNext { [unowned self] newHeight in
                let spaceAvailable = self.stripeButton.frame.minY - self.tableView.frame.minY
                self.fixedTableHeightConstraint.constant = newHeight
                self.fixedTableHeightConstraint.isActive = spaceAvailable > newHeight
                self.tableView.bounces = !self.fixedTableHeightConstraint.isActive
            }.subscribe().disposed(by: disposeBag)
    }
    
    private func adjustNavigationBar() {
        navigationController?.applyProfileItemAppearance(withTitle: Strings.Cards.listTitle.localized.uppercased())
        navigationController?.setNavigationBarBorderColor()
        statusBar?.backgroundColor = .white
        setNeedsStatusBarAppearanceUpdate()
    }

}

// MARK: - Table Manager Delegate
extension CreditCardListViewController: TableManagerDelegate {
    
    func didSwipeForRefresh() { }
    
    func loadMoreData() { }
    
    func tableViewDidScroll(_ scrollView: UIScrollView) { }

}
