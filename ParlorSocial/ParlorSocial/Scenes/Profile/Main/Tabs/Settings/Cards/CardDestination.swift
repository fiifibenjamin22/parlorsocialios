//
//  CardDestination.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 24/06/2019.
//

import UIKit

enum CardDestination {
    case premiumPayment(plan: PlanData?)
    case add(withApplyingType: AddCardViewType)
    case edit(cardDetails: CardDataDetails)
    case close
    case resetToHome
    case addCardFromPopup
}

extension CardDestination: Destination {
    
    var viewController: UIViewController {
        switch self {
        case .add(let type):
            return AddCardViewController(withApplyingType: type)
        case .edit(let cardDetails):
            return AddCardViewController(with: cardDetails)
        case .resetToHome:
            return AppTabBarController().embedInNavigationController()
        case .close:
            fatalError("Close destination should not use any view controller")
        case .premiumPayment(let plan):
            return PremiumPaymentViewController(membershipPlan: plan)
        case .addCardFromPopup:
            return AddCardViewController(withApplyingType: .add)
        }
    }
    
}
