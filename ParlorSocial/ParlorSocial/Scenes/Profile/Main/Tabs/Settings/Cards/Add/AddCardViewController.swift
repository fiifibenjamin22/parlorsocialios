//
//  AddCardViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 24/06/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

class AddCardViewController: AppViewController {
    // MARK: - Views
    private(set) var scrollView: UIScrollView!
    private(set) var scrollContent: UIView!
    private(set) var addCardButton: RoundedButton!
    private(set) var mainStack: UIStackView!
    private(set) var cardIconNumberHorizontalStack: UIStackView!

    private(set) var cardIconImageView: UIImageView!
    private(set) var fourNumberLabel: ParlorLabel!
    private(set) var cardFormView: CardFormView!
    
    private(set) var flexibleSpaceView: UIView!
    private(set) var flexibleSpaceHeightConstraint: NSLayoutConstraint!
    
    private lazy var hideForEditViews: [UIView] = {
        return [cardFormView.numberTextField, cardFormView.monthTextField,
                cardFormView.yearTextField, cardFormView.cvvTextField]
    }()
    
    private lazy var appliedForEditView: UIView = {
        return cardIconNumberHorizontalStack
    }()

    // MARK: - Properties
    let analyticEventScreen: AnalyticEventScreen? = .addOrUpdateCreditCard
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }
    
    private var viewModel: AddCardLogic!
    
    override var observableSources: ObservableSources? {
        return viewModel
    }

    // MARK: - Initialization
    init(withApplyingType type: AddCardViewType) {
        self.viewModel = AddCardViewModel(withApplyingType: type)
        super.init(nibName: nil, bundle: nil)
    }
    
    init(with cardDetails: CardDataDetails) {
        self.viewModel = AddCardViewModel(with: cardDetails)
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
    }

    override func observeViewModel() {
        super.observeViewModel()
        
        viewModel.destiantionObs
            .doOnNext { [unowned self] destination in
                self.handleDestination(destination)
            }.subscribe().disposed(by: disposeBag)
        
        viewModel.editCardDataDetailsObs
            .subscribe(onNext: { [unowned self] cardDetails in
                self.setup(with: cardDetails)
            }).disposed(by: disposeBag)
        
        viewModel.viewTypeObs
            .subscribe(onNext: { [unowned self] viewType in
                self.setupByViewType(type: viewType)
            }).disposed(by: disposeBag)
    }
    
    override func bindUI() {
        super.bindUI()
        cardFormView.stringFieldsToTypeMap.forEach { (key, value) in
            key.bindToStringCardField(ofType: value, to: viewModel.stringFieldChangeRelay).disposed(by: disposeBag)
        }
        
        cardFormView.intFieldsToTypeMap.forEach { (key, value) in
            key.bindToIntCardField(ofType: value, to: viewModel.intFieldChangeRelay).disposed(by: disposeBag)
        }
        
        addCardButton.rx.controlEvent(.touchUpInside)
            .bind(to: viewModel.addCardClicks)
            .disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.setNavigationBarBorderColor()
    }

}

// MARK: - Private methods
extension AddCardViewController {

    private func setupViews() {
        let builder = AddCardViewBuilder(controller: self)
        self.scrollView = builder.buildMainScroll()
        self.scrollContent = builder.buildContent()
        self.mainStack = builder.buildMainStack()
        self.cardFormView = builder.buildCardFormView()
        self.cardIconNumberHorizontalStack = builder.buildHorizontalStack()
        self.cardIconImageView = builder.buildCardIconImageView()
        self.fourNumberLabel = builder.buildFourNumberLabel()
        self.addCardButton = builder.buildAddButton()
        self.flexibleSpaceView = builder.buildFlexibleSpaceView()
        self.flexibleSpaceHeightConstraint = builder.buildFlexibleSpaceHeightConstraint()
        builder.setupViews()
    }
    
    private func setup(with cardDetails: CardDataDetails) {
        cardIconImageView.image = cardDetails.cardBrand.image
        fourNumberLabel.text = cardDetails.cardLastFour
        cardFormView.nameTextField.text = cardDetails.name
        cardFormView.address1TextField.text = cardDetails.addressLine1
        cardFormView.address2TextField.text = cardDetails.addressLine2
        cardFormView.cityTextField.text = cardDetails.addressCity
        cardFormView.stateTextField.text = cardDetails.addressState
        cardFormView.zipTextField.text = cardDetails.addressZip
    }
    
    private func handleDestination(_ cardDestination: CardDestination) {
        switch cardDestination {
        case .close:
            navigationController?.popViewController(animated: true)
        case .premiumPayment:
            guard let currentController = navigationController?.viewControllers else { return }
            navigationController?.setViewControllers(currentController.dropLast() + [cardDestination.viewController], animated: true)
        default:
            break
        }
    }

    private func setupByViewType(type viewType: AddCardViewType) {
        hideForEditViews.forEach {
            $0.isHidden = viewType == .edit
        }
        appliedForEditView.isHidden = viewType != .edit
        setupNavTitle(viewType: viewType)
        setupAddCardButton(viewType: viewType)
    }
    
    private func setupPresentationLogic() {
        setupFlexibleSpaceConstraint()
    }
    
    private func setupFlexibleSpaceConstraint() {
        view.layoutIfNeeded()
        let heightDiff = scrollView.frame.height
            - scrollContent.frame.height
            - (navigationController?.navigationBar.frame.height ?? 0)
            - (statusBar?.frame.height ?? 0)
        flexibleSpaceHeightConstraint.constant = max(heightDiff, 0)
    }
    
    private func setupNavTitle(viewType: AddCardViewType) {
        var title: String
        switch viewType {
        case .add:
            title = Strings.Cards.Add.title.localized.uppercased()
        case .edit:
            title = Strings.Cards.Edit.title.localized.uppercased()
        }
        navigationController?.applyProfileItemAppearance(withTitle: title)
    }
    
    private func setupAddCardButton(viewType: AddCardViewType) {
        switch viewType {
        case .add:
            addCardButton.setTitle(Strings.Cards.Add.addButton.localized.uppercased(), for: .normal)
            addCardButton.whiteStyle()
        case .edit:
            addCardButton.setTitle(Strings.Cards.Edit.continueButton.localized.uppercased(), for: .normal)
            addCardButton.whiteStyle()
        }
    }
}

// MARK: - Rounded button extension
fileprivate extension RoundedButton {
    func whiteStyle() {
        backgroundColor = .white
        setTitleColor(.appText, for: .normal)
    }
    
    func blackStyle() {
        backgroundColor = .appText
        setTitleColor(.white, for: .normal)
    }
}
