//
//  CreditCardTableCell.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 19/06/2019.
//

import UIKit

class CreditCardTableCell: UITableViewCell {
    
    // MARK: - Views
    lazy var checkbox: AppCheckBox = {
         return AppCheckBox().manualLayoutable()
    }()
    
    lazy var cardImage: UIImageView = {
        return UIImageView().manualLayoutable().apply {
            $0.clipsToBounds = true
            $0.contentMode = .scaleAspectFit
        }
    }()
    
    lazy var lastFourLabel: ParlorLabel = {
        return ParlorLabel.styled(
            withTextColor: .appText,
            withFont: UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.regular),
            alignment: .left,
            numberOfLines: 1
        )
    }()
    
    lazy var editButton: UIButton = {
        return UIButton()
            .manualLayoutable()
            .withPaymentMethodsStyle()
    }()
    
    lazy var removeButton: UIButton = {
        return UIButton()
            .manualLayoutable()
            .withPaymentMethodsStyle()
    }()
    
    // MARK: - Properties
    var didTryToChangeDefaultCardCallback: () -> Void = { }
    var removeClickCallback: () -> Void = { }
    var editClickCallback: () -> Void = { }

    // MARK: - Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        selectionStyle = .none
        checkbox.checkedImage = Icons.GuestListRsvp.checkBoxSelectedIcon
        checkbox.uncheckedImage = Icons.GuestListRsvp.checkBoxIcon
        checkbox.delegate = self
        contentView.addSubviews([checkbox, cardImage, lastFourLabel, removeButton, editButton])
        setupAutoLayout()
        editButton.apply {
            $0.setTitle(Strings.Cards.edit.localized, for: .normal)
            $0.addTarget(self, action: #selector(didTapEdit), for: .touchUpInside)
        }
        removeButton.apply {
            $0.setTitle(Strings.Cards.remove.localized, for: .normal)
            $0.addTarget(self, action: #selector(didTapRemove), for: .touchUpInside)
        }
    }
    
    private func setupAutoLayout() {
        [checkbox, cardImage, lastFourLabel, removeButton, editButton].forEach {
            $0.centerYAnchor.equal(to: contentView.centerYAnchor)
        }
        checkbox.topAnchor.equal(to: contentView.topAnchor, constant: Constants.Margin.standard)
        checkbox.bottomAnchor.equal(to: contentView.bottomAnchor, constant: -Constants.Margin.standard, withPriority: .defaultHigh)
        checkbox.leadingAnchor.equal(to: contentView.leadingAnchor, constant: Constants.horizontalMargin)
        cardImage.leadingAnchor.equal(to: checkbox.trailingAnchor, constant: 17)
        lastFourLabel.leadingAnchor.equal(to: cardImage.trailingAnchor, constant: 10)
        removeButton.trailingAnchor.equal(to: contentView.trailingAnchor, constant: -Constants.horizontalMargin)
        editButton.trailingAnchor.equal(to: removeButton.leadingAnchor, constant: -10)
    }
    
    @objc private func didTapEdit() {
        editClickCallback()
    }
    
    @objc private func didTapRemove() {
        removeClickCallback()
    }
}

extension CreditCardTableCell: ConfigurableCell {
    
    typealias Model = CardData
    
    func configure(with model: CardData) {
        lastFourLabel.text = model.cardLastFour
        cardImage.image = model.cardBrand.image
        checkbox.isChecked = model.isDefault
        checkbox.isUserInteractionEnabled = !model.isDefault
    }
}

extension CreditCardTableCell: AppCheckBoxDelegate {
    
    func appCheckboxDidChangeStatus(_ appCheckbox: AppCheckBox) {
        if appCheckbox.isChecked {
            didTryToChangeDefaultCardCallback()
            appCheckbox.isUserInteractionEnabled = false
        }
    }
    
}

fileprivate extension UIButton {
    
    func withPaymentMethodsStyle() -> UIButton {
        return self.apply {
            $0.applyTouchAnimation()
            $0.backgroundColor = .clear
            $0.setTitleColor(.appText, for: .normal)
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.regular)
        }
    }
    
}
