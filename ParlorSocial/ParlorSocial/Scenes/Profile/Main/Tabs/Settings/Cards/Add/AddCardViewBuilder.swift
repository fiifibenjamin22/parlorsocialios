//
//  AddCardViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 24/06/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class AddCardViewBuilder {

    private unowned let controller: AddCardViewController
    private var view: UIView! { return controller.view }

    init(controller: AddCardViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension AddCardViewBuilder {

    private func setupProperties() {
        view.backgroundColor = .white
        controller.apply {
            $0.cardIconNumberHorizontalStack.isHidden = true
            $0.addCardButton.setTitle(Strings.Cards.Add.addButton.localized.uppercased(), for: .normal)
        }
    }

    private func setupHierarchy() {
        controller.apply {
            view.addSubview($0.scrollView)
            $0.scrollView.addSubview($0.scrollContent)
            $0.scrollContent.addSubview($0.mainStack)
            $0.mainStack.addArrangedSubviews([
                $0.cardIconNumberHorizontalStack,
                $0.cardFormView,
                $0.flexibleSpaceView,
                $0.addCardButton
                ])
            
            $0.cardIconNumberHorizontalStack.addArrangedSubviews([
                $0.cardIconImageView,
                $0.fourNumberLabel
                ])
        }
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.scrollView.edgesToParent()
            $0.scrollContent.edgesToParent()
            $0.scrollContent.widthAnchor.equal(to: $0.scrollView.widthAnchor)
            $0.mainStack.edgesToParent(insets: .margins(
                top: 25,
                left: Constants.Margin.standard,
                bottom: 32, right: Constants.Margin.standard
                )
            )
            
            $0.cardIconImageView.setContentHuggingPriority(.required, for: .horizontal)
        }
    }

}

// MARK: - Public build methods
extension AddCardViewBuilder {
    
    func buildMainScroll() -> UIScrollView {
        return UIScrollView().manualLayoutable()
    }
    
    func buildContent() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildAddButton() -> RoundedButton {
        return RoundedButton.withBackgroundStyle().manualLayoutable()
    }
    
    func buildMainStack() -> UIStackView {
        return UIStackView(
            axis: .vertical,
            with: [],
            spacing: 10,
            alignment: .fill, distribution: .equalSpacing
        ).manualLayoutable()
    }
    
    func buildHorizontalStack() -> UIStackView {
        return UIStackView(
            axis: .horizontal,
            with: [],
            spacing: Constants.Margin.tiny,
            alignment: .fill, distribution: .fill
            )
    }
    
    func buildCardIconImageView() -> UIImageView {
        return UIImageView().manualLayoutable().apply {
            $0.clipsToBounds = true
            $0.contentMode = .scaleAspectFit
        }
    }
    
    func buildFourNumberLabel() -> ParlorLabel {
        return ParlorLabel.styled(
            withTextColor: .appText,
            withFont: UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.regular),
            alignment: .left,
            numberOfLines: 1
        )
    }

    func buildStripeInfoLabel() -> ParlorLabel {
        return ParlorLabel.styled(
            withTextColor: .appTextSemiLight,
            withFont: UIFont.custom(ofSize: Constants.FontSize.xTiny, font: UIFont.Roboto.regular),
            alignment: .center,
            numberOfLines: 0
            ).apply {
                $0.insets.top = 7
        }
    }
    
    func buildFlexibleSpaceView() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildFlexibleSpaceHeightConstraint() -> NSLayoutConstraint {
        return controller.flexibleSpaceView.heightAnchor.constraint(equalToConstant: 0).activate()
    }
    
    func buildCardFormView() -> CardFormView {
        return CardFormView()
    }

}
