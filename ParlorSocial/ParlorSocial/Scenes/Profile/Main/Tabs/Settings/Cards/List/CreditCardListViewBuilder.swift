//
//  CreditCardListViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 19/06/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class CreditCardListViewBuilder {

    private unowned let controller: CreditCardListViewController
    private var view: UIView! { return controller.view }

    init(controller: CreditCardListViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension CreditCardListViewBuilder {

    private func setupProperties() {
        view.backgroundColor = .white
        controller.apply {
            $0.tableView.apply {
                $0.contentInset = UIEdgeInsets.margins(bottom: -3)
                $0.separatorInset = UIEdgeInsets(top: 0, left: Constants.Margin.standard, bottom: 0, right: Constants.Margin.standard)
                $0.separatorColor = .textVeryLight
                $0.showsVerticalScrollIndicator = false
                $0.backgroundColor = .white
            }
            $0.addButton.setTitle(Strings.Cards.addMethod.localized.uppercased(), for: .normal)
            $0.extendedLayoutIncludesOpaqueBars = true
        }        
    }

    private func setupHierarchy() {
        controller.apply {
            view.addSubviews(
                [$0.tableView, $0.bottomSeperator, $0.stripeButton, $0.addButton]
            )
        }
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.tableView.edgesToParent(anchors: [.leading, .trailing])
            $0.tableView.topAnchor.equal(to: $0.view.safeAreaLayoutGuide.topAnchor)
            $0.tableView.bottomAnchor.equal(to: $0.bottomSeperator.topAnchor)
            
            $0.bottomSeperator.bottomAnchor.equal(to: $0.stripeButton.topAnchor, constant: -Constants.Margin.small, withPriority: .defaultLow)
            $0.bottomSeperator.edgesToParent(anchors: [.leading, .trailing])

            $0.stripeButton.centerXAnchor.equal(to: view.centerXAnchor)
            $0.stripeButton.bottomAnchor.equal(to: $0.addButton.topAnchor, constant: -30)
            
            $0.addButton.edgesToParent(
                anchors: [.leading, .trailing, .bottom],
                insets: .margins(left: Constants.Margin.standard, bottom: 25, right: Constants.Margin.standard)
            )
        }
    }
}

// MARK: - Public build methods
extension CreditCardListViewBuilder {
    
    func buildTableView() -> UITableView {
        return UITableView(frame: .zero, style: .grouped).manualLayoutable()
    }
    
    func buildMethodsLabel() -> ParlorLabel {
        return ParlorLabel.styled(
            withTextColor: .appText,
            withFont: UIFont.custom(ofSize: Constants.FontSize.hintSize, font: UIFont.Roboto.medium),
            alignment: .left, numberOfLines: 1
        ).manualLayoutable()
    }
    
    func buildStripeButton() -> StripeButton {
        return StripeButton().manualLayoutable()
    }
    
    func buildSeperator() -> UIView {
        return UIView().manualLayoutable().apply {
            $0.heightAnchor.equalTo(constant: 1)
            $0.backgroundColor = .appBackground
        }
    }

    func buildAddPaymentButton() -> RoundedButton {
        return RoundedButton.withBackgroundStyle().manualLayoutable()
    }
    
    func buidFixedTableHeightConstraint() -> NSLayoutConstraint {
        return controller.tableView.heightAnchor.constraint(equalToConstant: 0)
    }

}
