//
//  SettingsTabContent.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 05/06/2019.
//

import Foundation
import RxSwift
import RxSwiftExt
import RxCocoa

class SettingsTabContent: BaseTabSource {
    
    private let profileRepository: ProfileRepositoryProtocol = ProfileRepository.shared
    private let disposeBag = DisposeBag()
    
    private let destinationsSubject: PublishSubject<ProfileDestination> = PublishSubject()
    private let settingItems: BehaviorRelay<[SettingItem]> = BehaviorRelay(value: [])
    
    lazy var settingItemSelectedCallback: (SettingItem) -> Void = { [weak self] item in
        self?.handleItemSelected(item)
    }
    
    lazy var didTapLogoutCallback: () -> Void = { [unowned self] in
        self.progressBarSubject.onNext(true)
        GlobalNotificationsService.shared.unregisterDeviceToken { _ in
            self.progressBarSubject.onNext(false)
            self.destinationsSubject.onNext(.login)
        }
    }
    
    lazy var calendarSyncChangedCallback: (Bool) -> Void = { [unowned self] isOn in
        CalendarUtilities.shared.changeSyncState(isEnabled: isOn, changeSource: .manual, callback: { [unowned self] (_, alertData) in
            if let alertData = alertData {
                self.alertDataSubject.onNext(alertData)
            }
        })
    }
    
    override init() {
        super.init()
        settingItems.accept(settingsItems(withCardData: nil, isCalendarSyncEnabled: CalendarUtilities.isSyncOn))
        initFlow()
    }
    
    private func handleItemSelected(_ item: SettingItem) {
        switch item {
        case .emailAddress:
            saveItemSelectionEvent(eventName: .changeEmail)
            destinationsSubject.onNext(.changeEmail)
        case .password:
            saveItemSelectionEvent(eventName: .changePassword)
            destinationsSubject.onNext(.changePassword)
        case .mapProvider:
            saveItemSelectionEvent(eventName: .mapProvider)
            destinationsSubject.onNext(.mapProvider)
        case .seeManual:
            saveItemSelectionEvent(eventName: .seeManualAgain)
            destinationsSubject.onNext(.seeManual)
        case .terms:
            saveItemSelectionEvent(eventName: .terms)
            destinationsSubject.onNext(.terms)
        case .privacyPolicy:
            saveItemSelectionEvent(eventName: .privacy)
            destinationsSubject.onNext(.privacy)
        case .creditCard:
            saveItemSelectionEvent(eventName: .creditCard)
            destinationsSubject.onNext(.cardList)
        default:
            break
        }
    }
    
    private func settingsItems(withCardData cardData: CardData?, isCalendarSyncEnabled: Bool) -> [SettingItem] {
        let isMemberShipRenewalVisible = Config.userProfile?.membershipRenewalDate != nil
        
        let membershipSlice: [SettingItem] = isMemberShipRenewalVisible ?
            [.membershipRenewal(date: Config.userProfile?.membershipRenewalDate)] :
            []
        return
            [
                .emailAddress(email: Config.userProfile?.email ?? "" ),
                .password
            ]
            +
                membershipSlice
            +
            [
                .spacer,
                .creditCard(data: cardData),
                .mapProvider(name: ""),
                .seeManual,
                .spacer,
                .calendarSync(isOn: isCalendarSyncEnabled, changeCallback: calendarSyncChangedCallback),
                .spacer,
                .privacyPolicy,
                .terms
            ]
    }
    
    private func saveItemSelectionEvent(eventName: AnalyticEventClickableElementName.Profile) {
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: eventName.rawValue)
    }
}

extension SettingsTabContent {
    
    private func initFlow() {
        let apiUpdateObs = profileRepository
            .getUserCards()
            .filterMap { response -> FilterMap<[CardData]> in
                switch response {
                case .failure:
                    return .ignore
                case .success(let data):
                    return .map(data.data)
                }
            }
        let repositoryCards = profileRepository
            .knownCardsObs
        let calendarSyncObs = CalendarUtilities.shared.isCalendarSyncEnabledObs
            .withLatestFrom(profileRepository.knownCardsObs)
        
        Observable.merge(apiUpdateObs, repositoryCards, calendarSyncObs)
            .map { $0.first(where: { $0.isDefault }) }
            .map { [unowned self] in self.settingsItems(
                withCardData: $0,
                isCalendarSyncEnabled: CalendarUtilities.isSyncOn) }
            .bind(to: settingItems)
            .disposed(by: disposeBag)
    }
    
}

extension SettingsTabContent: ProfileTabContentSource {
    
    var currentTableContent: Observable<[TableSection]> {
        return settingItems.map { [unowned self] settingItems in
            return [SettingsSection(
                items: settingItems,
                logoutCallback: self.didTapLogoutCallback,
                itemSelector: self.settingItemSelectedCallback
                )]
        }
    }
    
    var destinations: Observable<ProfileDestination> {
        return destinationsSubject.asObservable()
    }
    
}
