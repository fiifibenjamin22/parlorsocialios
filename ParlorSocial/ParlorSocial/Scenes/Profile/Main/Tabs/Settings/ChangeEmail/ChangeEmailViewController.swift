//
//  ChangeEmailViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 22/08/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

class ChangeEmailViewController: AppViewController {
    
    // MARK: - Views
    private(set) var stackBackground: UIView!
    private(set) var emailField: ParlorTextField!
    private(set) var updateButton: RoundedButton!

    // MARK: - Properties
    let analyticEventScreen: AnalyticEventScreen? = .changeEmail
    private var viewModel: ChangeEmailLogic!
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }
    
    override var observableSources: ObservableSources? {
        return viewModel
    }
    
    // MARK: - Initialization
    init() {
        super.init(nibName: nil, bundle: nil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() {
        self.viewModel = ChangeEmailViewModel()
    }

    override func bindUI() {
        super.bindUI()
        emailField.rx.text.ignoreNil().bind(to: viewModel.emailSubject).disposed(by: disposeBag)
        updateButton.rx.controlEvent(.touchUpInside).bind(to: viewModel.changeClicks).disposed(by: disposeBag)
    }
    
    override func observeViewModel() {
        super.observeViewModel()
        
        viewModel.closeObs
            .doOnNext { [unowned self] in
                self.navigationController?.popViewController(animated: true)
            }.subscribe().disposed(by: disposeBag)
    }
    
    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        adjustNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

}

// MARK: - Private methods
extension ChangeEmailViewController {

    private func setupViews() {
        let builder = ChangeEmailViewBuilder(controller: self)
        self.stackBackground = builder.buildStackBackground()
        emailField = builder.buildTextField()
        updateButton = builder.buildUpdatePasswordButton()
        builder.setupViews()
    }

    private func setupPresentationLogic() {

    }
    
    private func adjustNavigationBar() {
        navigationController?.applyProfileItemAppearance(withTitle: Strings.ChangeEmail.title.localized.uppercased())
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        statusBar?.backgroundColor = .white
        setNeedsStatusBarAppearanceUpdate()
    }

}
