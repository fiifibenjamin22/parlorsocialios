//
//  ChangeEmailViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 22/08/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class ChangeEmailViewBuilder {

    private unowned let controller: ChangeEmailViewController
    private var view: UIView! { return controller.view }

    init(controller: ChangeEmailViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension ChangeEmailViewBuilder {

    private func setupProperties() {
        view.backgroundColor = .appBackground
        
        controller.apply {
            $0.updateButton.setTitle(Strings.ChangeEmail.update.localized.uppercased(), for: .normal)
            
            $0.emailField.placeholder = Strings.ChangeEmail.newEmail.localized
            $0.emailField.autocapitalizationType = .none
            $0.emailField.autocorrectionType = .no
        }
    }

    private func setupHierarchy() {
        controller.apply {
            view.addSubviews([$0.stackBackground, $0.updateButton])
            $0.stackBackground.addSubview($0.emailField)
        }
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.stackBackground.edgesToParent(
                anchors: [.leading, .top, .trailing],
                insets: UIEdgeInsets.margins(top: 1)
            )
            
            $0.emailField.edgesToParent(
                insets: .margins(
                    top: 30,
                    left: Constants.Margin.standard,
                    bottom: 30,
                    right: Constants.Margin.standard
                )
            )
            
            $0.updateButton.edgesToParent(
                anchors: [.leading, .trailing, .bottom],
                insets: UIEdgeInsets.margins(
                    left: Constants.Margin.standard,
                    bottom: 38,
                    right: Constants.Margin.standard
                )
            )
        }
    }

}

// MARK: - Public build methods
extension ChangeEmailViewBuilder {

    func buildTextField() -> LoginTextField {
        return LoginTextField().apply {
            $0.textColor = .appText
            $0.font = UIFont.custom(ofSize: Constants.FontSize.hintSize, font: UIFont.Roboto.regular)
            $0.heightAnchor.equalTo(constant: 65)
            $0.layer.borderWidth = 1
            $0.layer.borderColor = UIColor.textVeryLight.cgColor
            $0.padding = UIEdgeInsets(top: 0, left: Constants.Margin.standard, bottom: 0, right: Constants.Margin.standard)
            }.manualLayoutable()
    }
    
    func buildUpdatePasswordButton() -> RoundedButton {
        return RoundedButton.withBackgroundStyle().manualLayoutable()
    }
    
    func buildStackBackground() -> UIView {
        return UIView().manualLayoutable().apply {
            $0.backgroundColor = .white
        }
    }

}
