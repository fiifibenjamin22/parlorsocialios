//
//  ChangeEmailViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 22/08/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol ChangeEmailLogic: BaseViewModelLogic {
    var emailSubject: BehaviorSubject<String> { get }
    var changeClicks: PublishSubject<Void> { get }
}

class ChangeEmailViewModel: BaseViewModel {
    let analyticEventReferencedId: Int? = nil
    private let profileRepository = ProfileRepository.shared
    private let changeEmailValidationHelper = ChangeEmailValidationHelper()
    private let validator = Validator()
    
    let emailSubject: BehaviorSubject<String> = BehaviorSubject(value: "")
    let changeClicks: PublishSubject<Void> = PublishSubject()
    
    override init() {
        super.init()
        initFlow()
    }
}

// MARK: Private logic
private extension ChangeEmailViewModel {
    private func initFlow() {
        changeClicks
        .withLatestFrom(emailSubject)
            .subscribe(onNext: { [unowned self] response in
                self.emailValidation(emailText: response)
            }).disposed(by: disposeBag)
    }
    
    private func sendChangeEmailRequest(newEmail: String) {
        profileRepository.changeEmail(using: ChangeEmailRequest(email: newEmail)).showingProgressBar(with: self)
            .subscribe(onNext: {[unowned self] response in
                self.handleChangeEmailResponse(response)
            }).disposed(by: disposeBag)
    }
    
    private func handleChangeEmailResponse(_ response: ApiResponse<MessageResponse>) {
        switch response {
        case .success(let data):
            alertDataSubject.onNext(AlertData(
                icon: Icons.ChangeEmail.changeEmailSuccess,
                title: data.message,
                message: nil,
                bottomButtonTitle: Strings.ok.rawValue.uppercased(),
                buttonAction: { [ unowned self] in self.closeViewSubject.emitElement() }))
        case .failure(let error):
            errorSubject.onNext(error.setErrorType(errorType: .changeEmail))
        }
    }
    
    private func emailValidation(emailText: String) {
        let result = changeEmailValidationHelper.validate(emailText: emailText, validator: validator)
        switch result {
        case .success:
            sendChangeEmailRequest(newEmail: emailText)
        case .failure(let error):
            errorSubject.onNext(error.setErrorType(errorType: .changeEmail))
        }
    }
}

// MARK: Interface logic methods
extension ChangeEmailViewModel: ChangeEmailLogic {
    
}
