//
//  ChangeEmailValidationHelper.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 04/02/2020.
//

import Foundation

class ChangeEmailValidationHelper {
    
    // MARK: - Execution
    func validate(emailText: String, validator: Validator) -> Validator.Result {
        let emailValidatedInput = ValidatedInput(rules: [StringNotBlank()], value: emailText, name: Strings.ChangeEmail.newEmail.localized)
        
        return validator.validateWithResult(for: [emailValidatedInput])
    }
    
}
