//
//  MapProviderItemViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 12/06/2019.
//

import UIKit

final class MapProviderItemViewBuilder {
    
    private unowned let view: MapProviderItemView
    
    init(view: MapProviderItemView) {
        self.view = view
    }
    
    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
    
}

// MARK: - Private methods
extension MapProviderItemViewBuilder {
    
    private func setupProperties() {
        view.backgroundColor = .white
        view.appCheckbox.apply {
            $0.backgroundColor = .white
            $0.checkedImage = Icons.GuestListRsvp.checkBoxSelectedIcon
            $0.uncheckedImage = Icons.GuestListRsvp.checkBoxIcon
            $0.isUserInteractionEnabled = false
        }
        view.bottomLineView.apply {
            $0.backgroundColor = .textVeryLight
        }
    }
    
    private func setupHierarchy() {
        view.apply {
            $0.addSubviews([$0.appCheckbox, $0.nameLabel, $0.bottomLineView])
        }
    }
    
    private func setupAutoLayout() {
        view.appCheckbox.apply {
            $0.edgesToParent(anchors: [.leading], insets: UIEdgeInsets.margins( left: 25))
            $0.widthAnchor.equalTo(constant: 30)
            $0.heightAnchor.equalTo(constant: 37)
            $0.centerYAnchor.equal(to: view.centerYAnchor)
        }
        
        view.nameLabel.apply {
            $0.edgesToParent(anchors: [.top, .trailing], insets: UIEdgeInsets.margins(top: 25, right: 25))
            $0.leadingAnchor.equal(to: view.appCheckbox.trailingAnchor, constant: 9)
        }
        
        view.bottomLineView.apply {
            $0.edgesToParent(anchors: [.bottom])
            $0.heightAnchor.equalTo(constant: 1)
            $0.topAnchor.equal(to: view.nameLabel.bottomAnchor, constant: 24)
        }
    }
    
}

// MARK: - Public methods
extension MapProviderItemViewBuilder {
    
    func buildAppCheckbox() -> AppCheckBox {
        return AppCheckBox().manualLayoutable()
    }
    
    func buildNameLabel() -> UILabel {
        return UILabel.styled(
            textColor: .appTextMediumBright,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.subbody,
                font: UIFont.Roboto.regular
            ),
            alignment: .left,
            numberOfLines: 1
        )
    }
    
    func buildBottomLineView() -> UIView {
        return UIView().manualLayoutable()
    }
    
}
