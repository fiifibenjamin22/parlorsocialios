//
//  MapProviderItemView.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 12/06/2019.
//

import UIKit

protocol MapProviderItemViewDelegate: class {
    func appCheckboxDidChangeStatus(model: MapProviderItemModel)
}

final class MapProviderItemView: UIView {
    // MARK: - Properties
    private(set) var model: MapProviderItemModel?
    private weak var delegate: MapProviderItemViewDelegate?
    
    // MARK: - Views
    private(set) var appCheckbox: AppCheckBox!
    private(set) var nameLabel: UILabel!
    private(set) var bottomLineView: UIView!
    var isChecked: Bool = false {
        didSet {
            model?.isChecked = isChecked
            isUserInteractionEnabled = !isChecked
            appCheckbox.isChecked = isChecked
        }
    }
    
    // MARK: - Constraints
    private(set) var bottomLineLeading: NSLayoutConstraint!
    private(set) var bottomLineTrailing: NSLayoutConstraint!
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - Public methods
extension MapProviderItemView {
    
    func setup(with model: MapProviderItemModel) {
        self.model = model
        delegate = model.delegate
        isChecked = model.isChecked
        nameLabel.text = model.mapName
        bottomLineLeading.constant = model.isLast ? 0 : 16
        bottomLineTrailing.constant = model.isLast ? 0 : -16
    }
    
}

// MARK: - Private methods
extension MapProviderItemView {
    
    private func initialize() {
        clipsToBounds = true
        manualLayoutable()
        setupViews()
    }
    
    private func setupViews() {
        let builder = MapProviderItemViewBuilder(view: self)
        appCheckbox = builder.buildAppCheckbox()
        nameLabel = builder.buildNameLabel()
        bottomLineView = builder.buildBottomLineView()
        
        builder.setupViews()
        bottomLineLeading = bottomLineView.leadingAnchor.equal(to: self.leadingAnchor, constant: 16)
        bottomLineTrailing = bottomLineView.trailingAnchor.equal(to: self.trailingAnchor, constant: -16)
        self.setupTapGestureRecognizer(target: self, action: #selector(didTapView))
    }
    
    @objc private func didTapView() {
        guard let model = model else { return }
        isChecked = !isChecked
        delegate?.appCheckboxDidChangeStatus(model: model)
    }
}
