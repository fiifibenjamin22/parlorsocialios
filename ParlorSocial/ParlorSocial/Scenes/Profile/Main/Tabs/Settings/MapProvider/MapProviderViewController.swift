//
//  MapProviderViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 12/06/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

protocol MapProviderViewControllerDelegate: class {
    func mapDidChange()
}

class MapProviderViewController: AppViewController {
    
    // MARK: - Properties
    let analyticEventScreen: AnalyticEventScreen? = .mapProvider
    private var viewModel: MapProviderLogic!
    weak var delegate: MapProviderViewControllerDelegate?
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }
    
    // MARK: - Views
    private(set) var mainStackView: UIStackView!
    private(set) var stackViewItems: [MapProviderItemView] = []

    // MARK: - Initialization
    init(delegate: MapProviderViewControllerDelegate? = nil) {
        super.init(nibName: nil, bundle: nil)
        initialize(delegate: delegate)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }

    private func initialize(delegate: MapProviderViewControllerDelegate?) {
        self.viewModel = MapProviderViewModel()
        self.delegate = delegate
    }
    
    override func observeViewModel() {
        super.observeViewModel()
        
        viewModel.availableMapAppsObs
            .subscribe(onNext: { [unowned self] mapProviderItemModels in
                self.setupStackView(with: mapProviderItemModels)
            }).disposed(by: disposeBag)
        
        viewModel.didSelectedMapObs
            .subscribe(onNext: { [unowned self] index in
                self.setupStackViewWithSelectedMap(index: index)
                self.delegate?.mapDidChange()
            }).disposed(by: disposeBag)
    }

    // MARK: - Lifecycle
    override func loadView() {
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        adjustNavigationBar()
    }

}

// MARK: - Private methods
extension MapProviderViewController {

    private func setupViews() {
        let builder = MapProviderViewBuilder(controller: self)
        view = builder.buildView()
        mainStackView = builder.buildMainStackView()

        builder.setupViews()
    }

    private func setupPresentationLogic() {
        viewModel.getAvailableMapApps()
    }
    
    private func adjustNavigationBar() {
        statusBar?.backgroundColor = .white
        navigationController?.applyProfileItemAppearance(withTitle: Strings.Settings.mapProvider.localized.uppercased())
        navigationController?.setNavigationBarBorderColor()
        setNeedsStatusBarAppearanceUpdate()
    }
    
    private func setupStackView(with models: [MapProviderItemModel]) {
        mainStackView.removeArrangedSubviews()
        models.forEach {
            let stackViewItem = MapProviderItemView().manualLayoutable()
            stackViewItem.setup(with: $0)
            stackViewItems.append(stackViewItem)
            mainStackView.addArrangedSubview(stackViewItem)
        }
    }
    
    private func setupStackViewWithSelectedMap(index: Int) {
        stackViewItems.enumerated().first {$0.0 != index && $0.1.isChecked == true}?.1.isChecked = false
    }

}
