//
//  MapProviderItemModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 14/06/2019.
//

import Foundation

struct MapProviderItemModel {
    var mapUrl: URL?
    let index: Int
    let mapName: String
    var isChecked: Bool
    let isLast: Bool
    weak var delegate: MapProviderItemViewDelegate?
}
