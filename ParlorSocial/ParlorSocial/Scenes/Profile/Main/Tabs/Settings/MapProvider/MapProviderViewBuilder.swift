//
//  MapProviderViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 12/06/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class MapProviderViewBuilder {

    private unowned let controller: MapProviderViewController
    private var view: UIView! { return controller.view }

    init(controller: MapProviderViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension MapProviderViewBuilder {

    private func setupProperties() {
        view.backgroundColor = .white
    }

    private func setupHierarchy() {
        view.addSubview(controller.mainStackView)
    }

    private func setupAutoLayout() {
        controller.mainStackView.apply {
            $0.edgesToParent(anchors: [.leading, .top, .trailing])
        }
    }

}

// MARK: - Public build methods
extension MapProviderViewBuilder {

    func buildView() -> UIView {
        return UIView()
    }
    
    func buildMainStackView() -> UIStackView {
        return UIStackView(axis: .vertical, alignment: .fill, distribution: .equalSpacing).manualLayoutable()
    }
    
}
