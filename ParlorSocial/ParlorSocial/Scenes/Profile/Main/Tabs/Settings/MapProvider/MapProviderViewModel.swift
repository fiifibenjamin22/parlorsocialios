//
//  MapProviderViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 12/06/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

protocol MapProviderLogic: BaseViewModelLogic {
    var availableMapAppsObs: Observable<[MapProviderItemModel]> { get }
    var didSelectedMapObs: Observable<Int> { get }
    func getAvailableMapApps()
}

class MapProviderViewModel: BaseViewModel {
    // MARK: - Properties
    let analyticEventReferencedId: Int? = nil
    private let availableMapAppsRelay = PublishRelay<[MapProviderItemModel]>()
    private let didSelectedMapRelay = PublishRelay<Int>()
}

// MARK: Interface logic methods
extension MapProviderViewModel: MapProviderLogic {
    var didSelectedMapObs: Observable<Int> {
        return didSelectedMapRelay.asObservable()
    }
    
    var availableMapAppsObs: Observable<[MapProviderItemModel]> {
        return availableMapAppsRelay.asObservable()
    }
    
    func getAvailableMapApps() {
        let availableMapApps = MapProviderEnum.allCases.filter {
            guard let url = URL(string: $0.appUrlString) else { return false }
            return UIApplication.shared.canOpenURL(url)
        }
        availableMapAppsRelay.accept(
            availableMapApps.enumerated().map { tuple in
                let (index, data) = tuple
                return MapProviderItemModel(
                    mapUrl: URL(string: data.appUrlString),
                    index: index,
                    mapName: data.appName,
                    isChecked: Config.chosenMapApp == data.appUrlString,
                    isLast: index == availableMapApps.count - 1,
                    delegate: self)
            }
        )
    }
}

// MARK: MapProviderItemViewDelegate
extension MapProviderViewModel: MapProviderItemViewDelegate {
    func appCheckboxDidChangeStatus(model: MapProviderItemModel) {
        Config.chosenMapApp = model.mapUrl?.absoluteString
        didSelectedMapRelay.accept(model.index)
    }
    
}
