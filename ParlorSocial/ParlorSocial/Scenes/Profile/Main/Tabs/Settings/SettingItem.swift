//
//  SettingItem.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 05/06/2019.
//

import Foundation
import UIKit

enum SettingItem {
    case emailAddress(email: String)
    case password
    case membershipRenewal(date: Date?)
    case creditCard(data: CardData?)
    case mapProvider(name: String)
    case seeManual
    case calendarSync(isOn: Bool, changeCallback: ((Bool) -> Void) )
    case privacyPolicy
    case terms
    case spacer
}

extension SettingItem {
    
    var name: String? {
        switch self {
        case .emailAddress:
            return Strings.Settings.email.localized
        case .password:
            return Strings.Settings.password.localized
        case .membershipRenewal:
            return Strings.Settings.membershipRenewal.localized
        case .creditCard:
            return Strings.Settings.creditCard.localized
        case .mapProvider:
            return Strings.Settings.mapProvider.localized
        case .seeManual:
            return Strings.Settings.seeManual.localized
        case .privacyPolicy:
            return Strings.Settings.privacy.localized
        case .terms:
            return Strings.Settings.terms.localized
        case .calendarSync:
            return Strings.Settings.calendarSync.localized
        default:
            return nil
        }
    }
    
    var contentText: String? {
        switch self {
        case .emailAddress(let email):
            return email
        case .password:
            return "**************"
        case .membershipRenewal(let date):
            return date?.toString(withFormat: Date.Format.membershipRenewal)
        case .mapProvider:
            guard let mapAppUrlString = Config.chosenMapApp,
                let mapAppUrl = URL(string: mapAppUrlString),
                UIApplication.shared.canOpenURL(mapAppUrl) else { return nil }
            return MapProviderEnum(stringUrl: mapAppUrlString).appName
        case .creditCard(let data):
            return data?.cardLastFour
        default:
            return nil
        }
    }
    
    var isContentVisible: Bool {
        switch self {
        case .emailAddress, .password, .creditCard, .mapProvider, .membershipRenewal, .calendarSync:
            return true
        default:
            return false
        }
    }
    
    var isArrowVisible: Bool {
        switch self {
        case .membershipRenewal, .calendarSync:
            return false
        default:
            return true
        }
    }
    
    var contentAlignement: NSTextAlignment {
        switch self {
        case .membershipRenewal, .calendarSync:
            return .right
        default:
            return .left
        }
    }
    
    var fontSize: CGFloat {
        switch self {
        case .membershipRenewal:
            return Constants.FontSize.tiny
        default:
            return Constants.FontSize.subbody
        }
    }
    
    var isSpacer: Bool {
        switch self {
        case .spacer:
            return true
        default:
            return false
        }
    }
    
    var isSwitchVisible: Bool {
        switch self {
        case .calendarSync:
            return true
        default:
            return false
        }
    }
    
}
