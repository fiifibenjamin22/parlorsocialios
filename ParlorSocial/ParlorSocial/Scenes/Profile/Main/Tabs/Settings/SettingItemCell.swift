//
//  SettingItemCell.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 05/06/2019.
//

import UIKit

class SettingItemCell: UITableViewCell {
    
    var fixedHeightConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    var switchChangedCallback: (Bool) -> Void = { _ in }
    
    // MARK: - Views
    lazy var transparentContent: UIView = {
        return UIView().manualLayoutable().apply {
            $0.backgroundColor = .clear
        }
    }()
    
    lazy var cardView: UIView = {
        return UIView().manualLayoutable().apply {
            $0.applyTouchAnimation()
        }
    }()
    
    lazy var itemName: ParlorLabel = {
        return ParlorLabel.styled(
            withFont: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.regular),
            alignment: .left,
            numberOfLines: 0)
    }()
    
    lazy var itemContent: ParlorLabel = {
        return ParlorLabel.styled(
            withFont: UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.regular),
            alignment: .left,
            numberOfLines: 1).apply {
                $0.adjustsFontSizeToFitWidth = true
                $0.minimumScaleFactor = 0.1
        }
    }()
    
    lazy var imageContainer: UIView = {
        return UIView().apply {
            $0.clipsToBounds = true
            $0.addSubview(itemImage)
            itemImage.edgesToParent(insets: .margins(right: 12))
        }
    }()
    
    lazy var itemImage: UIImageView = {
        return UIImageView().manualLayoutable().apply {
            $0.clipsToBounds = true
            $0.contentMode = .scaleAspectFit
        }
    }()
    
    lazy var arrowImage: UIImageView = {
        return UIImageView(image: Icons.RsvpDetails.rightArrow).apply {
            $0.contentMode = .scaleAspectFit
            $0.setContentHuggingPriority(.required, for: .horizontal)
        }
    }()
    
    lazy var itemsStack: UIStackView = {
        return UIStackView(
            axis: .horizontal,
            with: [itemName, imageContainer, itemContent, arrowImage, uiSwitch],
            spacing: 0,
            alignment: .fill,
            distribution: .fill
            ).manualLayoutable()
    }()
    
    lazy var uiSwitch: UISwitch = {
        return UISwitch().apply {
            $0.onTintColor = .appGreen
        }
    }()
    
    // MARK: - Properties
    var isSpacer: Bool = false {
        didSet {
            adjustUI(isSpacer: self.isSpacer)
        }
    }
    
    // MARK: - Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        itemName.translatesAutoresizingMaskIntoConstraints = true
        itemName.setContentHuggingPriority(.required, for: .horizontal)
        imageContainer.setContentHuggingPriority(.required, for: .horizontal)
        itemContent.insets.right = Constants.Margin.tiny
        uiSwitch.addTarget(self, action: #selector(didChangeSwitchValue), for: .valueChanged)
        backgroundColor = .clear
        selectionStyle = .none
        setupAutoLayout()
    }
    
    private func setupAutoLayout() {
        contentView.addSubview(transparentContent)
        transparentContent.addSubview(cardView)
        cardView.addSubview(itemsStack)
        
        transparentContent.edgesToParent()
        cardView.apply {
            $0.edgesToParent(
                anchors: [.top, .leading, .trailing],
                insets: UIEdgeInsets.margins(
                    top: -1,
                    left: Constants.Margin.standard,
                    right: Constants.Margin.standard
                )
            )
            $0.bottomAnchor.equal(to: transparentContent.bottomAnchor, withPriority: .defaultHigh)
        }
        
        itemsStack.edgesToParent(
            insets: UIEdgeInsets.margins(
                top: 23,
                left: 22,
                bottom: 23,
                right: 22
            )
        )
        
        itemName.widthAnchor.constraint(greaterThanOrEqualToConstant: 120).activate()
        arrowImage.widthAnchor.equalTo(constant: 14)
        fixedHeightConstraint = transparentContent.heightAnchor.constraint(equalToConstant: 30)
    }
    
    @objc private func didChangeSwitchValue() {
        switchChangedCallback(uiSwitch.isOn)
    }
}

extension SettingItemCell {
    
    private func adjustUI(isSpacer: Bool) {
        self.cardView.isHidden = isSpacer
        self.fixedHeightConstraint.isActive = isSpacer
    }
    
    private func showProperContent(forItem item: SettingItem) {
        imageContainer.isHidden = true
        itemContent.text = item.contentText
        switch item {
        case .creditCard(let data) where data?.cardBrand.image != nil:
            imageContainer.isHidden = false
            itemImage.image = data?.cardBrand.image
        case .calendarSync(let isOn, let callback):
            uiSwitch.isOn = isOn
            switchChangedCallback = callback
        default:
            break
        }
    }
    
}

extension SettingItemCell: ConfigurableCell {
    
    typealias Model = SettingItem
    
    func configure(with model: SettingItem) {
        isSpacer = model.isSpacer
        arrowImage.isHidden = !model.isArrowVisible
        uiSwitch.isHidden = !model.isSwitchVisible
        itemName.text = model.name
        itemName.textColor = PremiumColorManager.shared.currentTheme.profileItemNameTextColor
        itemContent.textColor = PremiumColorManager.shared.currentTheme.profileItemContentTextColor
        itemContent.font = UIFont.custom(ofSize: model.fontSize, font: UIFont.Roboto.regular)
        showProperContent(forItem: model)
        itemContent.apply {
            $0.isHidden = !model.isContentVisible
            $0.textAlignment = model.contentAlignement
        }
        configureCardViewColorTheme()
    }
}

// MARK: - Configure views with color theme methods
extension SettingItemCell {
    private func configureCardViewColorTheme() {
        cardView.apply {
            switch PremiumColorManager.shared.currentTheme {
            case .nonPremium:
                $0.addLightBorder()
            case .premium:
                $0.addDarkBorder()
            }
            $0.backgroundColor = PremiumColorManager.shared.currentTheme.profileItemBackgroundColor
        }
    }
}
