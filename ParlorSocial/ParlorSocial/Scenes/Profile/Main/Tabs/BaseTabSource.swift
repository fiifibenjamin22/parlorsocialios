//
//  BaseTabSource.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 25/07/2019.
//

import RxSwift

class BaseTabSource: NSObject {
    
    let alertDataSubject: PublishSubject<AlertData> = PublishSubject()
    let errorSubject: PublishSubject<AppError> = PublishSubject()
    let progressBarSubject: BehaviorSubject<Bool> = BehaviorSubject(value: false)
    
}

extension BaseTabSource: ObservableSources {
    
    var closeObs: Observable<Void> {
        return Observable<Void>.empty()
    }
    
    var alertDataObs: Observable<AlertData> {
        return alertDataSubject.asObservable()
    }
    
    var errorObs: Observable<AppError> {
        return errorSubject.asObservable()
    }
    
    var progressBarObs: Observable<Bool> {
        return progressBarSubject.asObservable()
    }
    
}
