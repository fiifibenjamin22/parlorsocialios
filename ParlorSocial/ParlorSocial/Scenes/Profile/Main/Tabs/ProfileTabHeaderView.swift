//
//  TabsHeaderView.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 24/05/2019.
//

import UIKit

class ProfileTabHeaderView: UITableViewHeaderFooterView {
    lazy var label: ParlorLabel = {
       return ParlorLabel.withSectionHeaderLabelStyle()
    }()

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        contentView.addSubview(label)
        label.edgesToParent()
    }

}
