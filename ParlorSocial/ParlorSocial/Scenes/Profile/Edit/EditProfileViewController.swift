//
//  EditProfileViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 14/05/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import Kingfisher
import RxSwift

class EditProfileViewController: AppViewController {
    
    // MARK: - Properties
    let analyticEventScreen: AnalyticEventScreen? = .editProfile
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        viewModel.analyticEventScreenSubject
    }
    
    private var viewModel: EditProfileLogic!
    
    private let genderPickerOptions: [Gender] = [.male, .female, .other]
    
    private lazy var genderPickerCallback: (Int) -> Void = { [unowned self] selectedGenderIndex in
        let selectedGender = self.genderPickerOptions[selectedGenderIndex]
        self.genderTextField.text = selectedGender.displayedText
        self.viewModel.profileFieldDidChangeValue(.gender, to: selectedGender.rawValue)
    }
    
    private lazy var textFieldToEditTypeMap: [UITextField: EditProfileFieldType] = {
        return [
            firstNameTextField: .firstName,
            lastNameTextField: .lastName,
            homeZipTextField: .homeAddressZip,
            workZipTextField: .workAddressZip,
            genderTextField: .gender,
            dateOfBirthTextField: .birthday,
            educationTextField: .education,
            positionTextField: .position,
            employerTextField: .company
        ]}()
    
    private lazy var textFields: [UITextField] = {
        return [firstNameTextField, lastNameTextField, homeZipTextField, workZipTextField,
                genderTextField, dateOfBirthTextField, educationTextField,
                positionTextField, employerTextField]
    }()
    
    override var observableSources: ObservableSources? {
        return viewModel
    }
    
    // MARK: - Views
    private(set) var scrollView: UIScrollView!
    private(set) var contentView: UIView!
    private(set) var photoButton: EditImageButton!
    private(set) var contentStack: UIStackView!
    private(set) var firstNameTextField: ParlorTextField!
    private(set) var lastNameTextField: ParlorTextField!
    
    private(set) var zipHorizontalStack: UIStackView!
    private(set) var homeZipTextField: ParlorTextField!
    private(set) var workZipTextField: ParlorTextField!
    
    private(set) var genderAndBirthStack: UIStackView!
    private(set) var genderTextField: ParlorTextField!
    private(set) var datePicker: UIDatePicker!
    private(set) var dateOfBirthTextField: ParlorTextField!

    private(set) var educationTextField: ParlorTextField!
    
    private(set) var positionTextField: ParlorTextField!
    private(set) var employerTextField: ParlorTextField!

    // MARK: - Initialization
    init() {
        super.init(nibName: nil, bundle: nil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() {
        self.viewModel = EditProfileViewModel()
    }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    @objc func didTapDateEdited() {
        let date = datePicker.date
        dateOfBirthTextField.text = date.toString(withFormat: Date.Format.dateOfBirthEdit)
        viewModel.profileFieldDidChangeValue(.birthday, to: date.toString(withFormat: Date.Format.dateOfBirth))
        view.endEditing(true)
    }
    
    @objc func didTapDateCancelled() {
        view.endEditing(true)
    }

}

// MARK: - Private methods
extension EditProfileViewController {

    private func setupViews() {
        let builder = EditProfileViewBuilder(controller: self)
        scrollView = builder.buildScrollView()
        contentView = builder.buildContentView()
        photoButton = builder.buildPhotoButton()
        contentStack = builder.buildContentStack()
        firstNameTextField = builder.buildEditableField()
        lastNameTextField = builder.buildEditableField()
        homeZipTextField = builder.buildEditableField()
        workZipTextField = builder.buildEditableField()
        educationTextField = builder.buildEditableField()
        positionTextField = builder.buildEditableField()
        employerTextField = builder.buildEditableField()
        genderTextField = builder.buildEditableField()
        dateOfBirthTextField = builder.buildEditableField()
        datePicker = builder.buildDatePicker()
        genderAndBirthStack = builder.buildHorizontalSpacedStack()
        zipHorizontalStack = builder.buildHorizontalSpacedStack()
        builder.setupViews()
    }
    
    private func setupNavigationBar() {
        navigationItem.rightBarButtonItem = UIBarButtonItem.createSaveItem(target: self, action: #selector(didTapSave))
        statusBar?.backgroundColor = .white
        navigationController?.applyProfileItemAppearance(withTitle: Strings.EditProfile.title.localized.uppercased())
        navigationController?.setNavigationBarBorderColor()
    }

    private func setupPresentationLogic() {
        viewModel.initBaseProfile()
        fetchInitialData()
        bindImages()
        handleDestinations()
        handleGenderSelections()
        handleCloseView()
        photoButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapEditPhoto)))
        textFields.forEach { $0.addTarget(self, action: #selector(commonTextFieldDidChange(textField:)), for: .editingChanged) }
    }
    
    private func handleGenderSelections() {
        viewModel.genderPickerObs
            .doOnNext { [weak self] currentlySelected in
                self?.showGenderSelector(withSelected: currentlySelected)
            }.subscribe().disposed(by: disposeBag)
    }
    
    private func handleDestinations() {
        viewModel.destinationObs.doOnNext { [weak self] destination in
            self?.handle(destination: destination)
            }.subscribe().disposed(by: disposeBag)
    }
    
    private func handleCloseView() {
        viewModel.closeObs
            .doOnNext { [unowned self] _ in
                self.navigationController?.popViewController(animated: true)
            }.subscribe().disposed(by: disposeBag)
    }
    
    private func handle(destination: EditProfileDestination) {
        switch destination {
        case .back:
            navigationController?.popViewController(animated: true)
        }
    }
    
    private func bindImages() {
        viewModel
            .showingImageObs
            .doOnNext { [unowned self] image in
                self.showPhotoImage(image)
            }
            .subscribe().disposed(by: disposeBag)
    }
    
    private func fetchInitialData() {
        viewModel.profileObs
            .doOnNext { [unowned self] profile in
                self.fillWithData(from: profile)
        }.subscribe().disposed(by: disposeBag)
    }
    
    private func showPhotoImage(_ image: UIImage) {
        self.photoButton.imageView.image = image
    }
    
    private func fillWithData(from profile: Profile) {
        if let birthDate = profile.birthDate {
            dateOfBirthTextField.text = birthDate.toString(withFormat: Date.Format.dateOfBirthEdit)
            datePicker.date = birthDate
        }
        firstNameTextField.text = profile.firstName
        lastNameTextField.text = profile.lastName
        positionTextField.text = profile.position
        employerTextField.text = profile.company
        educationTextField.text = profile.education
        homeZipTextField.text = profile.homeAddressZip
        workZipTextField.text = profile.workAddressZip
        genderTextField.text = profile.gender?.capitalized
        photoButton.imageView.getImage(from: profile.imageURL,
                                       placeholder: Icons.Common.placeholderPhoto,
                                       ifFailedOrNilSet: Icons.Common.placeholderPhoto)
    }
    
    private func pickImage() {
        let controller = UIImagePickerController().apply {
            $0.delegate = self
            $0.sourceType = .photoLibrary
            $0.allowsEditing = false
            $0.navigationBar.tintColor = .black
        }
        present(controller, animated: true, completion: nil)
    }
    
    private func didTouchGenderField() {
        viewModel.genderClicks.emitElement()
    }
    
    private func showGenderSelector(withSelected selected: Gender) {
        let controller = DropDownPopupViewController(
            with: DropDownData(
                icon: nil,
                title: Strings.EditProfile.selectGender.localized,
                popupTitle: Strings.EditProfile.selectGender.localized,
                options: genderPickerOptions
            ),
            selectedOption: selected).apply { $0.selectionCallback = genderPickerCallback }
        controller.modalTransitionStyle = .crossDissolve
        controller.modalPresentationStyle = .overFullScreen
        present(controller, animated: true, completion: nil)
    }
    
    @objc private func commonTextFieldDidChange(textField: UITextField) {
        let fieldType = textFieldToEditTypeMap[textField]
        if let fieldType = fieldType, let value = textField.text {
            viewModel.profileFieldDidChangeValue(fieldType, to: value)
        }
    }
    
    @objc private func didTapSave() {
        viewModel.saveClicks.emitElement()
    }
    
    @objc private func didTapEditPhoto() {
        pickImage()
    }

}

extension EditProfileViewController: UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true)
        guard let image = info[.originalImage] as? UIImage else { return  }
        viewModel.manualImageRelay.onNext(image)
    }
    
}

extension EditProfileViewController: UINavigationControllerDelegate {}

extension EditProfileViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        guard textField == genderTextField else {
            return true
        }
        didTouchGenderField()
        return false
    }
    
}
