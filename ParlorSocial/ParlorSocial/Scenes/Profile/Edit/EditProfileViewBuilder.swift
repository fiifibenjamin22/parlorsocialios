//
//  EditProfileViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 14/05/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class EditProfileViewBuilder {

    private unowned let controller: EditProfileViewController
    private var view: UIView! { return controller.view }

    init(controller: EditProfileViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension EditProfileViewBuilder {

    private func setupProperties() {
        controller.apply {
            $0.zipHorizontalStack.axis = .horizontal
            $0.zipHorizontalStack.distribution = .fillEqually
            $0.firstNameTextField.placeholder = Strings.EditProfile.firstName.localized
            $0.lastNameTextField.placeholder = Strings.EditProfile.lastName.localized
            $0.workZipTextField.placeholder = Strings.EditProfile.workZip.localized
            $0.workZipTextField.keyboardType = .numbersAndPunctuation
            $0.homeZipTextField.placeholder = Strings.EditProfile.homeZip.localized
            $0.homeZipTextField.keyboardType = .numbersAndPunctuation
            $0.educationTextField.placeholder = Strings.EditProfile.education.localized
            $0.positionTextField.placeholder = Strings.EditProfile.positionPlaceholder.localized
            $0.employerTextField.placeholder = Strings.EditProfile.employer.localized
            $0.genderTextField.placeholder = Strings.EditProfile.gender.localized
            $0.dateOfBirthTextField.placeholder = Strings.EditProfile.birthDate.localized
            $0.dateOfBirthTextField.inputView = $0.datePicker
            $0.dateOfBirthTextField.inputAccessoryView = buildDatePickerToolbar()
            $0.genderTextField.delegate = controller
        }
    }

    private func setupHierarchy() {
        controller.apply {
            view.addSubview($0.scrollView)
            $0.scrollView.addSubview($0.contentView)
            $0.contentView.addSubviews([$0.photoButton, $0.contentStack])
            
            $0.contentStack.addEditSeperatedViews([
                buildFieldLabel(withText: Strings.EditProfile.name.localized),
                $0.firstNameTextField,
                $0.lastNameTextField
                ])
            
            $0.contentStack.addArrangedSubview($0.zipHorizontalStack)
            $0.zipHorizontalStack.addArrangedSubviews([
                buildVericalStack(labelText: Strings.EditProfile.homeZip.localized, textField: $0.homeZipTextField),
                buildVericalStack(labelText: Strings.EditProfile.workZip.localized, textField: $0.workZipTextField)
            ])
            
            $0.contentStack.addArrangedSubview($0.genderAndBirthStack)
            $0.genderAndBirthStack.addArrangedSubviews([
                buildVericalStack(labelText: Strings.EditProfile.gender.localized, textField: $0.genderTextField),
                buildVericalStack(labelText: Strings.EditProfile.birthDate.localized, textField: $0.dateOfBirthTextField)
                ])
            
            $0.contentStack.addEditSeperatedViews([
                buildFieldLabel(withText: Strings.EditProfile.education.localized),
                $0.educationTextField
                ])
            
            $0.contentStack.addEditSeperatedViews([
                buildFieldLabel(withText: Strings.EditProfile.position.localized),
                $0.positionTextField,
                $0.employerTextField
                ])
        }
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.scrollView.edgesToParent()
            $0.contentView.edgesToParent()
            $0.contentView.widthAnchor.equal(to: $0.scrollView.widthAnchor)
            
            $0.photoButton.centerXAnchor.equal(to: $0.contentView.centerXAnchor)
            $0.photoButton.topAnchor.equal(to: $0.contentView.topAnchor, constant: 41)
            
            $0.contentStack.edgesToParent(anchors: [.leading, .trailing], padding: Constants.Margin.standard)
            $0.contentStack.topAnchor.equal(to: $0.photoButton.bottomAnchor, constant: 0)
            $0.contentStack.bottomAnchor.equal(to: $0.contentView.bottomAnchor, constant: -49)
        }
    }

}

// MARK: - Public build methods
extension EditProfileViewBuilder {
    
    func buildScrollView() -> UIScrollView {
        return UIScrollView().manualLayoutable().apply {
            $0.backgroundColor = .white
        }
    }
    
    func buildContentView() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildHorizontalSpacedStack() -> UIStackView {
        return UIStackView(axis: .horizontal, with: [], spacing: Constants.Margin.standard, alignment: .fill, distribution: .fillEqually)
    }
    
    func buildPhotoButton() -> EditImageButton {
        return EditImageButton().manualLayoutable()
    }
    
    func buildContentStack() -> UIStackView {
        return UIStackView(axis: .vertical, with: [], spacing: 0, alignment: .fill, distribution: .equalSpacing).manualLayoutable()
    }
    
    func buildFieldLabel(withText text: String) -> ParlorLabel {
        return ParlorLabel.styled(withTextColor: .appGreyLight, withFont: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.regular), alignment: .left, numberOfLines: 1).apply {
            $0.text = text
            $0.insets.top = 35
            $0.insets.bottom = 10
        }
    }
    
    func buildEditableField() -> ParlorTextField {
        return ParlorTextField().apply {
            $0.placeholderColor = .eventTime
            $0.font = UIFont.custom(ofSize: Constants.FontSize.hintSize, font: UIFont.Roboto.regular)
            $0.textColor = .appTextBright
            $0.tintColor = .textVeryLight
            $0.heightAnchor.equalTo(constant: 60)
            $0.sideBorderColor = .textVeryLight
            $0.padding = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        }
    }
    
    func buildVericalStack(labelText: String, textField: ParlorTextField) -> UIStackView {
        return UIStackView(axis: .vertical, with: [], spacing: 0, alignment: .fill, distribution: .fill).apply {
            $0.addEditSeperatedViews([buildFieldLabel(withText: labelText), textField])
        }
    }
    
    func buildDatePickerToolbar() -> UIToolbar {
        return UIToolbar().apply {
            $0.sizeToFit()
            let doneButton = UIBarButtonItem(title: Strings.done.localized, style: .plain, target: controller, action: #selector(controller.didTapDateEdited))
            let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: Strings.cancel.localized, style: .plain, target: controller, action: #selector(controller.didTapDateCancelled))
            $0.setItems([doneButton, spaceButton, cancelButton], animated: false)
        }
    }
    
    func buildDatePicker() -> UIDatePicker {
        return UIDatePicker().apply {
            $0.datePickerMode = .date
        }
    }
    
}

fileprivate extension UIStackView {
    
    func addEditSeperatedViews(_ views: [UIView]) {
        self.addSeperatedViews(views, withColor: .textVeryLight)
    }
}
