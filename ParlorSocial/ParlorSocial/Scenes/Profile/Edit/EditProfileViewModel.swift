//
//  EditProfileViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 14/05/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxSwift

enum EditProfileFieldType {
    case firstName
    case lastName
    case birthday
    case education
    case university
    case company
    case position
    case homeAddressStreet
    case homeAddressZip
    case homeAddressCity
    case workAddressZip
    case gender
}

protocol EditProfileLogic: BaseViewModelLogic {
    var profileObs: Observable<Profile> { get }
    var destinationObs: Observable<EditProfileDestination> { get }
    var manualImageRelay: BehaviorSubject<UIImage?> { get }
    var genderPickerObs: Observable<Gender> { get }
    var saveClicks: PublishSubject<Void> { get }
    var genderClicks: PublishSubject<Void> { get }
    
    var showingImageObs: Observable<UIImage> { get }
    
    func profileFieldDidChangeValue(_ fieldType: EditProfileFieldType, to newValue: String)
    func initBaseProfile()
}

class EditProfileViewModel: BaseViewModel {
    let analyticEventReferencedId: Int? = nil
    let maxSizeOfPhoto: CGFloat = 500
    private let profileRepository = ProfileRepository.shared
    private let validator = Validator()
    private let editProfileValidationHelper = EditProfileValidationHelper()
    
    private let destinationSubject: PublishSubject<EditProfileDestination> = PublishSubject()
    private let showingProfile: ReplaySubject<Profile> = ReplaySubject.create(bufferSize: 1)
    
    private var readyToUpdateModel: ProfileUpdateModel?
    
    private var uploadImageObs: Observable<ProfileResponse> {
        return manualImageRelay.take(1)
            .flatMap { [unowned self] image -> Observable<ProfileResponse> in
                if let image = image?.scale(toMaxSize: CGSize(width: self.maxSizeOfPhoto, height: self.maxSizeOfPhoto)) {
                return self.profileRepository.uploadNewPhoto(image).showingProgressBar(with: self)
            } else {
                return Observable.empty()
            } }
            .observeOn(MainScheduler.instance)

    }
    
    private var updateProfileDataObs: Observable<ProfileResponse> {
        guard let updateModel = self.readyToUpdateModel else { fatalError("Upload model is nil") }
        return profileRepository.updateProfile(withData: updateModel).showingProgressBar(with: self)
    }
    
    let manualImageRelay: BehaviorSubject<UIImage?> = BehaviorSubject(value: nil)
    let saveClicks: PublishSubject<Void> = PublishSubject()
    let genderClicks: PublishSubject<Void> = PublishSubject()
    let genderPickerSubject: PublishSubject<Gender> = PublishSubject()

    override init() {
        super.init()
        initFlow()
    }
    
}

// MARK: Private logic
private extension EditProfileViewModel {
    
    private func initFlow() {
        saveClicks
            .subscribe(onNext: { [unowned self] _ in
                self.validateReadyToUpdateModel()
            }).disposed(by: disposeBag)
        
        genderClicks.map { [unowned self] _ in
            Gender(rawValue: self.readyToUpdateModel?.gender ?? "") ?? Gender.other
        }.bind(to: genderPickerSubject).disposed(by: disposeBag)
        
    }
    
    private func handleUpdateResponses(updateProfile: ProfileResponse) {
        switch updateProfile {
        case (ApiResponse.success):
            self.alertDataSubject.onNext(AlertData.fromSimpleMessage(
                message: Strings.EditProfile.success.localized,
                withIcon: Icons.Common.checkedWhite,
                action: { [unowned self] in self.closeViewSubject.emitElement() }
            ))
        case (ApiResponse.failure(let error)):
            errorSubject.onNext(error)
        }
    }
    
    private func validateReadyToUpdateModel() {
        let result = editProfileValidationHelper.validate(profileUpdateModel: readyToUpdateModel, validator: validator)
        switch result {
        case .success:
            sendUpdateProfileRequest()
        case .failure(let error):
            errorSubject.onNext(error)
        }
    }
    
    private func sendUpdateProfileRequest() {
        updateProfileDataObs.concatMap { [unowned self] dataUpdateResponse -> Observable<ProfileResponse> in
            switch dataUpdateResponse {
            case ProfileResponse.success:
                return self.uploadImageObs.ifEmpty(default: dataUpdateResponse)
            case ProfileResponse.failure:
                return Observable.just(dataUpdateResponse)
            }
        }
        .doOnNext { [unowned self] response in
            self.handleUpdateResponses(updateProfile: response)
        }
        .subscribe().disposed(by: disposeBag)
    }
    
}

// MARK: Interface logic methods
extension EditProfileViewModel: EditProfileLogic {
    
    func initBaseProfile() {
        profileRepository.getMyProfileData().showingProgressBar(with: self)
            .map { (resposne: ProfileResponse) -> Profile? in
                switch resposne {
                case .failure:
                    return Config.userProfile
                case .success(let data):
                    return data.data
                }
            }
            .ignoreNil()
            .doOnNext { [ unowned self] profile in self.readyToUpdateModel = ProfileUpdateModel(profile: profile) }
            .bind(to: showingProfile)
            .disposed(by: disposeBag)
    }
    
    func profileFieldDidChangeValue(_ fieldType: EditProfileFieldType, to newValue: String) {
        switch fieldType {
        case .firstName:
            readyToUpdateModel?.firstName = newValue
        case .lastName:
            readyToUpdateModel?.lastName = newValue
        case .birthday:
            readyToUpdateModel?.birthday = newValue
        case .company:
            readyToUpdateModel?.company = newValue
        case .education:
            readyToUpdateModel?.education = newValue
        case .position:
            readyToUpdateModel?.position = newValue
        case .gender:
            readyToUpdateModel?.gender = newValue
        case .homeAddressCity:
            readyToUpdateModel?.homeAddressCity = newValue
        case .homeAddressStreet:
            readyToUpdateModel?.homeAddressStreet = newValue
        case .homeAddressZip:
            readyToUpdateModel?.homeAddressZip = newValue
        case .workAddressZip:
            readyToUpdateModel?.workAddressZip = newValue
        case .university:
            readyToUpdateModel?.university = newValue
        }
        
    }
    
    var destinationObs: Observable<EditProfileDestination> {
        return destinationSubject.asObservable()
    }
    
    var profileObs: Observable<Profile> {
        return showingProfile.asObservable()
    }
    
    var showingImageObs: Observable<UIImage> {
        return manualImageRelay.asObservable().ignoreNil()
    }
    
    var genderPickerObs: Observable<Gender> {
        return genderPickerSubject.asObservable()
    }
}
