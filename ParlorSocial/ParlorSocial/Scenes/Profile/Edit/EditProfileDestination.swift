//
//  EditProfileDestination.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 21/05/2019.
//

import Foundation
import UIKit

enum EditProfileDestination {
    case back
}

extension EditProfileDestination: Destination {
    
    var viewController: UIViewController {
        switch self {
        case .back:
            fatalError()
        }
    }
}
