//
//  EditProfileValidationHelper.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 05/02/2020.
//

import Foundation

class EditProfileValidationHelper {
    
    func validate(profileUpdateModel: ProfileUpdateModel?, validator: Validator) -> Validator.Result {
        let firstNameValidatedInput = ValidatedInput(rules: [StringNotBlank(), StringMaxLength(100)],
                                                     value: profileUpdateModel?.firstName, name: Strings.EditProfile.firstName.localized)
        let lastNameValidatedInput = ValidatedInput(rules: [StringNotBlank(), StringMaxLength(100)],
                                                    value: profileUpdateModel?.lastName, name: Strings.EditProfile.lastName.localized)
        let homeZipValidatedInput = ValidatedInput(rules: [StringNotBlank(), StringMaxLength(10), StringMinLength(3)],
                                                   value: profileUpdateModel?.homeAddressZip, name: Strings.EditProfile.homeZip.localized)
        let workZipValidatedInput = ValidatedInput(rules: [StringNotBlank(), StringMaxLength(10), StringMinLength(3)],
                                                   value: profileUpdateModel?.workAddressZip, name: Strings.EditProfile.workZip.localized)
        let genderValidatedInput = ValidatedInput(rules: [StringNotBlank(), StringMaxLength(191)],
                                                  value: profileUpdateModel?.gender, name: Strings.EditProfile.gender.localized)
        let dateOfBirthValidatedInput = ValidatedInput(rules: [DateMinAge(21)],
                                                       value: DateFormatter.dateOfBirthEditFormatter.date(from: profileUpdateModel?.birthday ?? ""),
                                                       name: "")
        let educationValidatedInput = ValidatedInput(rules: [StringNotBlank(), StringMaxLength(1000)],
                                                     value: profileUpdateModel?.education, name: Strings.EditProfile.education.localized)
        let positionValidatedInput = ValidatedInput(rules: [StringNotBlank(), StringMaxLength(100)],
                                                    value: profileUpdateModel?.position, name: Strings.EditProfile.positionPlaceholder.localized)
        let employerValidatedInput = ValidatedInput(rules: [StringNotBlank(), StringMaxLength(100)],
                                                    value: profileUpdateModel?.company, name: Strings.EditProfile.employer.localized)
        
        let beforeDateStringsValidationOutputs = validator.validate(validatedInput: [
            firstNameValidatedInput,
            lastNameValidatedInput,
            homeZipValidatedInput,
            workZipValidatedInput,
            genderValidatedInput
        ])
        let dateValidationOutput = validator.validate(validatedInput: [dateOfBirthValidatedInput])
        let afterDateStringsValidationOutputs = validator.validate(validatedInput: [
            educationValidatedInput,
            positionValidatedInput,
            employerValidatedInput
        ])
        return validator.getResultFrom(validatedOutputs: beforeDateStringsValidationOutputs + dateValidationOutput + afterDateStringsValidationOutputs)
    }
    
}
