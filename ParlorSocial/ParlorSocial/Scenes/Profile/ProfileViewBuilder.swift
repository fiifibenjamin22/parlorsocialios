//
//  ProfileViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 12/03/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class ProfileViewBuilder {

    private unowned let controller: ProfileViewController
    private var view: UIView! { return controller.view }

    init(controller: ProfileViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension ProfileViewBuilder {

    private func setupProperties() {
        controller.apply {
            $0.mainTable.tableHeaderView = $0.tableProfileHeader
            $0.mainTable.contentInsetAdjustmentBehavior = .never
        }
    }

    private func setupHierarchy() {
        view.addSubviews([controller.mainTable])
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.setupActivateMembershipView()
            $0.mainTable.edgesToParentSafeArea()
        }
    }

}

// MARK: - Public build methods
extension ProfileViewBuilder {
    
    func buildMainTable() -> UITableView {
        return UITableView(frame: .zero, style: .grouped).manualLayoutable().apply {
            $0.separatorStyle = .none
        }
    }
    
    func buildTableHeader() -> ProfileTableHeaderView {
        return ProfileTableHeaderView().manualLayoutable()
    }
    
    func buildActivateMembershipView() -> ActivateMembershipView {
        return ActivateMembershipView().manualLayoutable()
    }
}
