//
//  ProfileViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 12/03/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ProfileViewController: AppViewController, HasActivateMembershipView {
    
    // MARK: - Properties
    let analyticEventScreen: AnalyticEventScreen? = .profile
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }
    
    private var viewModel: ProfileLogic!
    private var tableManager: TableViewManager!
    private var disposeBagForStatusBar: DisposeBag?
    
    override var connectedOnboardingType: OnboardingType? {
        return .profile
    }
    
    // MARK: - Views
    private(set) var mainTable: UITableView!
    private(set) var tableProfileHeader: ProfileTableHeaderView!
    private var barStyle: UIStatusBarStyle = .default
    var activateMembershipView: ActivateMembershipView!
    
    // MARK: - Initialization
    init() {
        super.init(nibName: nil, bundle: nil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() {
        self.viewModel = ProfileViewModel()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return barStyle
    }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        adjustNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        disposeBagForStatusBar = nil
    }
        
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        mainTable.layoutTableHeaderView()
    }
    
    override func observeViewModel() {
    
        viewModel.destinationObs
            .subscribe(onNext: { [unowned self] destination in
                self.handle(destination: destination)
            }).disposed(by: disposeBag)
        
        viewModel.openMailComposerObs
            .subscribe(onNext: { [unowned self] settings in
                self.openMail(basedOn: settings)
            }).disposed(by: disposeBag)
        
        viewModel
            .errorObs
            .handleWithAlerts(using: self)
            .disposed(by: disposeBag)
        
        viewModel
            .alertDataObs
            .show(using: self)
            .disposed(by: disposeBag)
        
        viewModel
            .progressBarObs
            .showProgressBar(using: parent?.parent?.parent ?? self, isDark: Config.userProfile?.isPremium ?? false)
            .disposed(by: disposeBag)
        
        viewModel.setupActivateMembershipViewSubject
            .subscribe(onNext: { [unowned self] data in
                self.activateMembershipView.setup(with: data)
            }).disposed(by: disposeBag)
    }

    func didTapActivateMembershipMoreButton() {
        viewModel.didTapActivateMembershipMoreButton()
    }
}

// MARK: - Private methods
extension ProfileViewController {

    private func setupViews() {
        let builder = ProfileViewBuilder(controller: self)
        
        mainTable = builder.buildMainTable()
        tableProfileHeader = builder.buildTableHeader()
        activateMembershipView = builder.buildActivateMembershipView()
        
        builder.setupViews()
    }

    private func setupPresentationLogic() {
        tableManager = TableViewManager(tableView: mainTable, delegate: nil)
        tableProfileHeader.tabsView.delegate = self
        tableProfileHeader.editProfileButton.addTarget(self, action: #selector(didTapEditProfile), for: .touchUpInside)
        observeProfileChanges()
        observeContentChanges()
        viewModel.initActivateMembershipViewLogic()
    }
    
    @objc private func didTapEditProfile() {
        viewModel.didTapEditProfile()
    }
    
    private func handle(destination: ProfileDestination) {
        switch destination {
        case .editProfile, .circleDetails, .interests, .referFriend, .changeEmail,
             .changePassword, .upgrade, .cardList, .payment, .membershipPlans:
            topRouter.push(destination: destination)
        case .login:
            topRouter.replace(with: destination)
        case .emailUs:
            viewModel.prepareDataToContactEmail()
        case .suggestEventsOrVenues:
            viewModel.prepareDataToSuggestEventsOrVenues()
        case .privacy, .terms:
            topRouter.present(destination: destination, withStyle: .fullScreen)
        case .mapProvider:
            guard let vc = destination.viewController as? MapProviderViewController else {fatalError("Map provider could not be opened")}
            vc.delegate = self
            guard let parent = self.parent?.parent else {fatalError("Did you manipulate with controller hierarchy?")}
            parent.navigationController?.pushViewController(vc, animated: true)
        case .seeManual:
            viewModel.resetManualFlags()
            guard let parent = self.parent?.parent as? AppTabBarController else {fatalError("Did you manipulate with controller hierarchy?")}
            parent.selectScreen(.home)
        }
    }
    
    private func observeProfileChanges() {
        viewModel.profileObs.doOnNext { [unowned self] currentProfile in
            self.tableProfileHeader.profileData = currentProfile
        }.subscribe().disposed(by: disposeBag)
    }
    
    private func observeContentChanges() {
        viewModel
            .currentSections
            .doOnNext { [unowned self] sections in
                self.tableManager.hideLoadingFooter()
                self.tableManager.uploadWith(sections: sections)
                self.tableProfileHeader.tabsView.hideProgressBarInAllButtons()
            }.subscribe().disposed(by: disposeBag)
        
        viewModel
            .currentColorThemeObs
            .subscribe(onNext: { [unowned self] colorTheme in
                self.change(colorTheme: colorTheme)
            }).disposed(by: disposeBag)
    }
    
    private func adjustNavigationBar() {
        /* There is another disposeBag than global because `viewModel.navigationAndStatusBarVisibilityObs`
         * should be observe only when `ProfileViewController` is visibility for user.
         * This is because it sets navigation bar that is global in app and we want to change
         * appearance of navigation bar only on this view controller. So when
         * method `viewWillDisappear` will be called, `disposeBagForStatusBar` should be nil
         * to ignore `viewModel.navigationAndStatusBarVisibilityObs`
         */
        disposeBagForStatusBar = DisposeBag()
        
        viewModel.navigationAndStatusBarVisibilityObs
            .subscribe(onNext: { [unowned self] isVisible in
                self.setupNavigationAndStatusBar(isVisible: isVisible)
            }).disposed(by: disposeBagForStatusBar!)
        
        viewModel.setupNavigationAndStatusBar()
    }
    
    private func setupHiddenNavigationAndStatusBar() {
        navigationController?.setNavigationBarHidden(true, animated: false)
        statusBar?.backgroundColor = PremiumColorManager.shared.currentTheme.profileBackgroundColor
        switch PremiumColorManager.shared.currentTheme {
        case .nonPremium:
            barStyle = .default
        case .premium:
            barStyle = .lightContent
        }
        setNeedsStatusBarAppearanceUpdate()
    }
    
    private func setupVisibleNavigationAndStatusBar() {
        statusBar?.backgroundColor = .white
        barStyle = .default
        navigationController?.applyProfileItemAppearance(withTitle: Strings.Profile.title.localized.uppercased())
        navigationController?.setNavigationBarBorderColor(.clear)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    private func change(colorTheme theme: PremiumColorManager.Theme) {
        tableManager.footerStyle = theme.footerTableViewStyle
        setupHeaderViewColorTheme(theme: theme)
        mainTable.reloadData()
        statusBar?.backgroundColor = theme.profileBackgroundColor
        view.backgroundColor = theme.profileBackgroundColor
        setNeedsStatusBarAppearanceUpdate()
    }
    
    private func setupHeaderViewColorTheme(theme: PremiumColorManager.Theme) {
        tableProfileHeader.apply {
            $0.profileImageView.image = theme.placeholderPhoto
            $0.nameLabel.textColor = theme.profileNameTextColor
            $0.positionLabel.textColor = theme.profilePositionTextColor
            $0.editProfileButton.setTitleColor(theme.roundedButtonTextColor, for: .normal)
            $0.editProfileButton.layer.borderColor = theme.roundedButtonBorderColor.cgColor
            $0.seperator.backgroundColor = theme.separatorColor
            $0.bottomSeperator.backgroundColor = theme.separatorColor
            $0.backgroundColor = theme.profileBackgroundColor
        }
        setupTabsViewColorTheme(theme: theme)
        mainTable.backgroundColor = theme.profileMainTableViewBackgroundColor
    }
    
    private func setupTabsViewColorTheme(theme: PremiumColorManager.Theme) {
        let tabButtons = [
            tableProfileHeader.tabsView.circlesButton,
            tableProfileHeader.tabsView.interestsButton,
            tableProfileHeader.tabsView.contactButton,
            tableProfileHeader.tabsView.settingsButton
        ]
        tabButtons.forEach {
            $0.unselectedColor = theme.unselectedTabButtonColor
            $0.selectedColor = theme.selectedTabButtonColor
        }
        tableProfileHeader.tabsView.innerSpacerViews.forEach {
            $0.backgroundColor = theme.tabButtonsSeparator
        }
    }
    
    private func setupNavigationAndStatusBar(isVisible: Bool) {
        if isVisible {
            setupVisibleNavigationAndStatusBar()
        } else {
            setupHiddenNavigationAndStatusBar()
        }
    }
}

extension ProfileViewController: ProfileTabsViewDelegate {
    
    func profileTabsView(_ view: ProfileTabsView, didSelectTabWithType type: ProfileTabType) {
        tableManager.footerStyle = PremiumColorManager.shared.currentTheme.footerTableViewStyle
        tableManager.invalidateCache()
        viewModel.currentTabSubject.onNext(type)
    }
    
}

// MARK: - MapProviderViewControllerDelegate
extension ProfileViewController: MapProviderViewControllerDelegate {
    func mapDidChange() {
        viewModel.currentTabSubject.onNext(.settings)
    }
}

extension ProfileViewController: TabBarChild {
    
    func didTouchTabItem() {
        scrollToTop()
    }
    
    func didSelectTab() {
        viewModel.tabSelectedSubject.emitElement()
    }
    
}

extension ProfileViewController: ScrollableContent {
    
    func scrollToTop() {
        tableManager.scrollToTop()
    }
    
}
