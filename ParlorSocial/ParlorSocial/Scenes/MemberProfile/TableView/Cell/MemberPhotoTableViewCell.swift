//
//  MemberPhotoTableViewCell.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 22/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class MemberPhotoTableViewCell: UITableViewCell, ConfigurableCell {
    typealias Model = URL

    private let photoImageView = UIImageView().manualLayoutable()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configure(with model: URL) {
        photoImageView.getImage(from: model)
    }

    private func setup() {
        selectionStyle = .none

        photoImageView.contentMode = .scaleAspectFill
        photoImageView.clipsToBounds = true

        contentView.addSubview(photoImageView)

        photoImageView.heightAnchor.constraint(
            equalTo: photoImageView.widthAnchor,
            multiplier: Constants.MemberPhotoTableViewCell.imageViewHeightToWidthRatio
        ).activate()
        photoImageView.edgesToParent()
    }
}

private extension Constants {
    enum MemberPhotoTableViewCell {
        static let imageViewHeightToWidthRatio: CGFloat = 0.75
    }
}
