//
//  MemberInfoTableViewCell.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 21/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class MemberInfoTableViewCell: UITableViewCell {
    var actionButtonDidTapClosure: (() -> Void)?

    private let infoView = InfoView().manualLayoutable()
    private let separatorView = UIView().manualLayoutable()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        initializeCell()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        initializeCell()
    }
}

// MARK: - Public methods
extension MemberInfoTableViewCell: ConfigurableCell {
    typealias Model = Info

    func configure(with model: Model) {
        infoView.titleLabel.text = model.title.uppercased()
        infoView.descriptionLabel.text = model.description
    }
}

// MARK: - Private methods
extension MemberInfoTableViewCell {
    private func initializeCell() {
        selectionStyle = .none

        setupViews()
    }

    private func setupViews() {
        setupInfoView()
        setupSeparatorView()
    }

    private func setupInfoView() {
        contentView.addSubview(infoView)

        infoView.edgesToParent(anchors: [.top, .leading, .trailing], insets: Constants.MemberInfoTableViewCell.InfoView.insets)
    }

    private func setupSeparatorView() {
        separatorView.backgroundColor = .lightSeparator

        contentView.addSubview(separatorView)

        separatorView.heightAnchor.equalTo(constant: Constants.MemberInfoTableViewCell.SeparatorView.height)
        separatorView.topAnchor.equal(to: infoView.bottomAnchor, constant: Constants.MemberInfoTableViewCell.SeparatorView.insets.top)
        separatorView.edgesToParent(anchors: [.bottom, .leading, .trailing], insets: Constants.MemberInfoTableViewCell.SeparatorView.insets)
    }
}

private extension Constants {
    enum MemberInfoTableViewCell {
        enum InfoView {
            static let insets = UIEdgeInsets(vertical: 20.0, horizontal: 25.0)
        }

        enum SeparatorView {
            static let height: CGFloat = 1.0
            static let insets = UIEdgeInsets(top: 20.0, left: 25.0, bottom: 0.0, right: 25.0)
        }
    }
}
