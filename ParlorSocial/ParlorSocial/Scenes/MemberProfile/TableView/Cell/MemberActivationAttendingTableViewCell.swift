//
//  MemberActivationAttendingTableViewCell.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 22/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class MemberActivationAttendingTableViewCell: UITableViewCell {
    private let stackView = UIStackView().manualLayoutable()
    private let titleLabel = UILabel()
    private let attendingView = ActivationAttendingView()
    private let separatorView = UIView().manualLayoutable()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        initializeCell()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        initializeCell()
    }
}

// MARK: - Public methods
extension MemberActivationAttendingTableViewCell: ConfigurableCell {
    typealias Model = HomeActivation

    func configure(with model: HomeActivation) {
        attendingView.setup(with: model)
    }
}

// MARK: - Private methods
extension MemberActivationAttendingTableViewCell {
    private func initializeCell() {
        selectionStyle = .none

        setupViews()
    }

    private func setupViews() {
        setupStackView()
        setupTitleLabel()
        setupActivationAttendingView()
        setupSeparatorView()
    }

    private func setupStackView() {
        stackView.axis = .vertical
        stackView.spacing = Constants.MemberActivationAttendingTableViewCell.StackView.spacing

        contentView.addSubview(stackView)

        stackView.edgesToParent(anchors: [.top, .leading, .trailing], insets: Constants.MemberActivationAttendingTableViewCell.StackView.insets)
    }

    private func setupTitleLabel() {
        titleLabel.font = Constants.MemberActivationAttendingTableViewCell.TitleLabel.font
        titleLabel.textColor = .appTextSemiLight
        titleLabel.text = Strings.MemberProfile.attending.localized.uppercased()

        stackView.addArrangedSubview(titleLabel)
    }

    private func setupActivationAttendingView() {
        attendingView.heightAnchor.equalTo(constant: Constants.MemberActivationAttendingTableViewCell.ActivationAttendingView.height)

        stackView.addArrangedSubview(attendingView)
    }

    private func setupSeparatorView() {
        separatorView.backgroundColor = .lightSeparator

        contentView.addSubview(separatorView)

        separatorView.heightAnchor.equalTo(constant: Constants.MemberActivationAttendingTableViewCell.SeparatorView.height)
        separatorView.topAnchor.equal(to: stackView.bottomAnchor, constant: Constants.MemberActivationAttendingTableViewCell.SeparatorView.insets.top)
        separatorView.edgesToParent(anchors: [.bottom, .leading, .trailing], insets: Constants.MemberActivationAttendingTableViewCell.SeparatorView.insets)
    }
}

private extension Constants {
    enum MemberActivationAttendingTableViewCell {
        enum StackView {
            static let insets = UIEdgeInsets(vertical: 20.0, horizontal: 26.0)
            static let spacing: CGFloat = 18.0
        }

        enum TitleLabel {
            static let font = UIFont.custom(ofSize: Constants.FontSize.xTiny, font: UIFont.Roboto.regular)
        }

        enum ActivationAttendingView {
            static let height: CGFloat = 40.0
        }

        enum SeparatorView {
            static let height: CGFloat = 1.0
            static let insets = UIEdgeInsets(top: 20.0, left: 25.0, bottom: 0.0, right: 25.0)
        }
    }
}

