//
//  MemberNameTableViewCell.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 21/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class MemberNameTableViewCell: UITableViewCell {
    var actionButtonDidTapClosure: (() -> Void)?

    private let nameLabel = UILabel().manualLayoutable()
    private let connectButtonsStackView = UIStackView().manualLayoutable()
    private let declineButton = ParlorButton()
    private let actionButton = ParlorButton()
    private let separatorView = UIView().manualLayoutable()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        initializeCell()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        initializeCell()
    }
}

// MARK: - Public methods
extension MemberNameTableViewCell: ConfigurableCell {
    typealias Model = Member

    func configure(with model: Member) {
        nameLabel.text = model.name
        setupButtons(for: model.connectionStatus)
    }
}

// MARK: - Private methods
extension MemberNameTableViewCell {
    private func initializeCell() {
        selectionStyle = .none

        setupViews()
    }

    private func setupViews() {
        setupLabel()
        setupButtonsStackView()
        setupDeclineButton()
        setupButton()
        setupSeparatorView()
    }

    private func setupLabel() {
        nameLabel.font = Constants.MemberNameCell.NameLabel.font
        nameLabel.textColor = .appTextBright
        nameLabel.numberOfLines = Constants.MemberNameCell.NameLabel.numberOfLines
        nameLabel.lineBreakMode = .byTruncatingTail

        contentView.addSubview(nameLabel)

        nameLabel.leadingAnchor.equal(to: contentView.leadingAnchor, constant: Constants.MemberNameCell.NameLabel.insets.left)
        nameLabel.topAnchor.equal(to: contentView.topAnchor, constant: Constants.MemberNameCell.NameLabel.insets.top)
    }

    private func setupButtonsStackView() {
        connectButtonsStackView.axis = .horizontal
        connectButtonsStackView.spacing = Constants.MemberNameCell.ButtonsStackView.spacing

        contentView.addSubview(connectButtonsStackView)

        connectButtonsStackView.leadingAnchor.equal(to: nameLabel.trailingAnchor, constant: Constants.MemberNameCell.ButtonsStackView.insets.left)
        connectButtonsStackView.trailingAnchor.equal(to: contentView.trailingAnchor, constant: -Constants.MemberNameCell.ButtonsStackView.insets.right)
        connectButtonsStackView.heightAnchor.equalTo(constant: Constants.MemberNameCell.ButtonsStackView.height)
        connectButtonsStackView.centerYAnchor.equal(to: nameLabel.centerYAnchor)
    }

    private func setupDeclineButton() {
        declineButton.isHidden = true
        declineButton.setImage(Icons.Common.cancelButton, for: [])

        connectButtonsStackView.addArrangedSubview(declineButton)

        declineButton.heightAnchor.constraint(
            equalTo: declineButton.widthAnchor,
            multiplier: Constants.MemberNameCell.DeclineButton.ratio
        ).activate()
    }

    private func setupButton() {
        actionButton.layer.cornerRadius = Constants.MemberNameCell.ButtonsStackView.height / 2
        actionButton.applyTouchAnimation()
        actionButton.addTarget(self, action: #selector(actionButtonDidTap), for: .touchUpInside)

        connectButtonsStackView.addArrangedSubview(actionButton)

        actionButton.widthAnchor.equalTo(constant: Constants.MemberNameCell.ActionButton.width)
    }

    private func setupButtons(for status: ConnectionStatus) {
        declineButton.isHidden = status.isDeclineButtonHidden
        actionButton.style = actionButtonStyle(for: status)
        actionButton.title = status.title.uppercased()
        actionButton.isUserInteractionEnabled = status.isUserInteractionEnabled
        actionButton.setTarget(self, action: #selector(actionButtonDidTap), for: .touchUpInside)
    }

    private func actionButtonStyle(for status: ConnectionStatus) -> ParlorButtonStyle {
        return status.defaultStyle
    }

    private func setupSeparatorView() {
        separatorView.backgroundColor = .lightSeparator

        contentView.addSubview(separatorView)

        separatorView.heightAnchor.equalTo(constant: Constants.MemberNameCell.SeparatorView.height)
        separatorView.topAnchor.equal(to: nameLabel.bottomAnchor, constant: Constants.MemberNameCell.SeparatorView.insets.top)
        separatorView.edgesToParent(anchors: [.bottom, .leading, .trailing], insets: Constants.MemberNameCell.SeparatorView.insets)
    }

    @objc private func actionButtonDidTap() {
        actionButtonDidTapClosure?()
    }
}

private extension Constants {
    enum MemberNameCell {
        enum NameLabel {
            static let insets = UIEdgeInsets(top: 30, left: 25, bottom: 30, right: 0)
            static let font = UIFont.custom(ofSize: Constants.FontSize.smallTitle, font: UIFont.Editor.bold)
            static let numberOfLines = 2
        }

        enum ButtonsStackView {
            static let spacing: CGFloat = 15.0
            static let insets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 25)
            static let height: CGFloat = 30.0
        }

        enum DeclineButton {
            static let ratio: CGFloat = 1.0
        }

        enum ActionButton {
            static let width: CGFloat = 96.0
        }

        enum SeparatorView {
            static let height: CGFloat = 1.0
            static let insets = UIEdgeInsets(top: 30.0, left: 25.0, bottom: 0.0, right: 25.0)
        }
    }
}
