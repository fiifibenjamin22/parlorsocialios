//
//  ActivationAttendingView.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 21/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

class ActivationAttendingView: UIView {
    private let stackView = UIStackView().manualLayoutable()
    private let activationImageView = UIImageView()
    private let textsStackView = UIStackView()
    private let titleLabel = ParlorLabel()
    private let datesStackView = UIStackView()
    private let dateLabel = ParlorLabel()
    private let timeLabel = ParlorLabel()
    private let viewEventLabel = ParlorLabel()
    private let viewEventButton = ParlorButton(
        style: .plain(
            titleColor: .appGreyLight,
            font: UIFont.custom(ofSize: Constants.FontSize.xxxTiny, font: UIFont.Roboto.regular),
            letterSpacing: Constants.LetterSpacing.none
        )
    )

    override init(frame: CGRect) {
        super.init(frame: frame)

        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        setup()
    }

    func setup(with activation: HomeActivation) {
        activationImageView.getImage(from: activation.imageURL)
        titleLabel.text = activation.name
        dateLabel.text = activation.startAt.toMonthAndDayFormat(isApproximatedStartTime: false)
        timeLabel.text = activation.startAt.toAttendingFormat(withEndDate: activation.endAt).uppercased()
    }

    private func setup() {
        setupStackView()
        setupActivationImageView()
        setupTextsStackView()
        setupTitleLabel()
        setupDatesStackView()
        setupDateLabel()
        setupTimeLabel()
        setupViewEventButton()
    }

    private func setupStackView() {
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        stackView.spacing = Constants.ActivationAttendingView.StackView.spacing

        addSubview(stackView)

        stackView.edgesToParent()
    }

    private func setupActivationImageView() {
        activationImageView.contentMode = .scaleAspectFit

        stackView.addArrangedSubview(activationImageView)

        activationImageView.heightAnchor.constraint(
            equalTo: activationImageView.widthAnchor,
            multiplier: Constants.ActivationAttendingView.ActivationImageView.imageViewHeightToWidthRatio
        ).activate()
    }

    private func setupTextsStackView() {
        textsStackView.spacing = Constants.ActivationAttendingView.TextsStackView.spacing
        textsStackView.axis = .vertical

        stackView.addArrangedSubview(textsStackView)
        setContentHuggingPriority(.defaultLow, for: .horizontal)
    }

    private func setupTitleLabel() {
        titleLabel.font = Constants.ActivationAttendingView.TitleLabel.font
        titleLabel.textColor = .appTextMediumBright
        titleLabel.numberOfLines = Constants.ActivationAttendingView.TitleLabel.numberOfLines

        textsStackView.addArrangedSubview(titleLabel)
    }

    private func setupDatesStackView() {
        datesStackView.axis = .horizontal
        datesStackView.distribution = .equalSpacing
        datesStackView.spacing = Constants.ActivationAttendingView.DatesStackView.spacing

        textsStackView.addArrangedSubview(datesStackView)
    }

    private func setupDateLabel() {
        dateLabel.font = Constants.ActivationAttendingView.DateLabel.font
        dateLabel.textColor = .appTextMediumBright
        dateLabel.minimumScaleFactor = 0.7
        timeLabel.adjustsFontSizeToFitWidth = true

        datesStackView.addArrangedSubview(dateLabel)
    }

    private func setupTimeLabel() {
        timeLabel.font = Constants.ActivationAttendingView.TimeLabel.font
        timeLabel.textColor = .appTextMediumBright
        timeLabel.minimumScaleFactor = 0.7
        timeLabel.adjustsFontSizeToFitWidth = true

        datesStackView.addArrangedSubview(timeLabel)
    }

    private func setupViewEventButton() {
        viewEventButton.isUserInteractionEnabled = false
        viewEventButton.title = Strings.MemberProfile.viewEvent.localized

        stackView.addArrangedSubview(viewEventButton)
    }
}

private extension Constants {
    enum ActivationAttendingView {
        enum StackView {
            static let spacing: CGFloat = 12.0
        }

        enum ActivationImageView {
            static let imageViewHeightToWidthRatio: CGFloat = 1.0
        }

        enum TextsStackView {
            static let spacing: CGFloat = 8.0
        }

        enum TitleLabel {
            static let numberOfLines = 1
            static let font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Editor.bold)
        }

        enum DatesStackView {
            static let spacing: CGFloat = 18.0
        }

        enum DateLabel {
            static let font = UIFont.custom(ofSize: Constants.FontSize.xTiny, font: UIFont.Roboto.medium)
        }

        enum TimeLabel {
            static let font = UIFont.custom(ofSize: Constants.FontSize.xTiny, font: UIFont.Roboto.medium)
        }
    }
}
