//
//  InfoView.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 21/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

class InfoView: UIStackView {
    private(set) var titleLabel: ParlorLabel!
    private(set) var descriptionLabel: ParlorLabel!

    private let innerInset: CGFloat = 10

    init() {
        super.init(frame: .zero)
        initialize()
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func initialize() {
        titleLabel = ParlorLabel.styled(
            withTextColor: UIColor.appTextSemiLight,
            withFont: UIFont.custom(ofSize: Constants.FontSize.xTiny, font: UIFont.Roboto.regular),
            alignment: .left,
            numberOfLines: 1
        )

        descriptionLabel = ParlorLabel.styled(
            withTextColor: UIColor.appTextMediumBright,
            withFont: UIFont.custom(ofSize: Constants.FontSize.hintSize, font: UIFont.Roboto.regular),
            alignment: .left,
            numberOfLines: 0
        ).apply {
            $0.lineHeight = Constants.lineHeightBig
        }

        axis = .vertical
        distribution = .equalSpacing
        spacing = innerInset
        alignment = .leading
        addArrangedSubviews([titleLabel, descriptionLabel])
    }
}
