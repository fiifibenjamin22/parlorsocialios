//
//  MemberNameSection.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 22/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class MemberNameSection: BasicTableSection<Member, MemberNameTableViewCell> {
    let connectButtonSelector: () -> Void

    init(
        item: Member,
        itemSelector: @escaping ((Member) -> Void) = { _ in },
        connectButtonSelector: @escaping () -> Void = {}
    ) {
        self.connectButtonSelector = connectButtonSelector
        super.init(items: [item], itemSelector: itemSelector)
    }

    override func didSelectItem(_ tableView: UITableView, at indexPath: IndexPath) {
        super.didSelectItem(tableView, at: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension MemberNameSection {
    static func make(
        for member: Member,
        itemSelector: @escaping (Member) -> Void,
        connectButtonSelector: @escaping () -> Void = {}
    ) -> TableSection {
        return MemberNameSection(
            item: member,
            itemSelector: itemSelector,
            connectButtonSelector: connectButtonSelector
        )
    }
}
