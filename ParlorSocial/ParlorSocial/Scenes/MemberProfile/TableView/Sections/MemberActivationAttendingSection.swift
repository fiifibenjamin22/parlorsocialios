//
//  MemberActivationAttendingSection.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 22/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class MemberActivationAttendingSection: BasicTableSection<HomeActivation, MemberActivationAttendingTableViewCell> {}

extension MemberActivationAttendingSection {
    static func make(
        for activations: [HomeActivation],
        itemSelector: @escaping (HomeActivation) -> Void
    ) -> TableSection {
        return MemberActivationAttendingSection(items: activations, itemSelector: itemSelector)
    }
}
