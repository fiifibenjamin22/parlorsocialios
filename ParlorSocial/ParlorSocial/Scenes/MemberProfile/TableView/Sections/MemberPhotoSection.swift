//
//  MemberPhotoSection.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 22/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class MemberPhotoSection: BasicTableSection<URL, MemberPhotoTableViewCell> {}

extension MemberPhotoSection {
    static func make(for imageUrl: URL) -> TableSection {
        return MemberPhotoSection(items: [imageUrl])
    }
}
