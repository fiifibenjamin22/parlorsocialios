//
//  MemberProfileSectionType.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 23/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

enum MemberProfileSectionType: Int, CaseIterable {
    case photo
    case name
    case info
    case links
    case attending
}
