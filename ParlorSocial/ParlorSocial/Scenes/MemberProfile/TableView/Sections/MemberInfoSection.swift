//
//  MemberInfoSection.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 22/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

struct Info {
    let title: String
    let description: String
}

final class MemberInfoSection: BasicTableSection<Info, MemberInfoTableViewCell> {
    override func didSelectItem(_ tableView: UITableView, at indexPath: IndexPath) {
        super.didSelectItem(tableView, at: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension MemberInfoSection {
    static func make(
        for info: [Info],
        itemSelector: @escaping (Info) -> Void = { _ in }
    ) -> TableSection {
        return MemberInfoSection(items: info, itemSelector: itemSelector)
    }
}
