//
//  MemberInfoRowType.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 23/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

enum MemberInfoRowType: Int, CaseIterable {
    case about
    case position
    case company
    case education
    case interestGroup
}
