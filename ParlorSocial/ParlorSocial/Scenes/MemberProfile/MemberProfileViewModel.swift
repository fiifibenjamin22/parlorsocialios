//
//  MemberProfileViewModel.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 21/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import RxSwift
import RxCocoa

protocol MemberProfileLogic: BaseViewModelLogic {
    var titleObs: Observable<String> { get }
    var sectionsObs: Observable<[TableSection]> { get }

    func refreshMemberProfile()
}

class MemberProfileViewModel: BaseViewModel {
    private lazy var cellSelector: (Member) -> Void = { [unowned self] _ in

    }

    private lazy var linkSelector: (Info) -> Void = { [unowned self] _ in

    }

    private lazy var attendingSelector: (HomeActivation) -> Void = { [unowned self] _ in

    }

    let analyticEventReferencedId: Int?

    private let sectionsRelay = BehaviorRelay<[TableSection]>(value: [])
    private let titleRelay = BehaviorRelay<String>(value: "")

    init(member: Member) {
        self.analyticEventReferencedId = member.id

        super.init()

        setup(with: member)
    }
}

// MARK: Private logic
private extension MemberProfileViewModel {
    private func setup(with member: Member) {
        let tableSections = createTableSections(from: member)
        titleRelay.accept(member.name)
        sectionsRelay.accept(tableSections)
    }

    private func createTableSections(from member: Member) -> [TableSection] {
        return MemberProfileSectionType.allCases.map { self.createTableSection(for: $0, member: member) }
    }

    private func createTableSection(for type: MemberProfileSectionType, member: Member) -> TableSection {
        switch type {
        case .name:
            return MemberNameSection.make(for: member, itemSelector: cellSelector)
        case .photo:
            return MemberPhotoSection.make(for: member.imageUrl)
        case .info:
            let info = createInformations(from: member)
            return MemberInfoSection.make(for: info)
        case .links:
            let info = member.socialLinks.map { Info(title: $0.title, description: $0.url) }
            return MemberInfoSection.make(for: info, itemSelector: linkSelector)
        case .attending:
            return MemberActivationAttendingSection.make(for: [member.attendingActivation], itemSelector: attendingSelector)
        }
    }

    private func createInformations(from member: Member) -> [Info] {
        return MemberInfoRowType.allCases.map {
            switch $0 {
            case .about: return Info(title: Strings.MemberProfile.about.localized, description: member.about)
            case .position: return Info(title: Strings.MemberProfile.position.localized, description: member.position)
            case .company: return Info(title: Strings.MemberProfile.company.localized, description: member.company)
            case .education: return Info(title: Strings.MemberProfile.education.localized, description: member.education)
            case .interestGroup: return Info(title: Strings.MemberProfile.interestGroups.localized, description: member.interestGroups)
            }
        }
    }
}

// MARK: Interface logic methods
extension MemberProfileViewModel: MemberProfileLogic {
    var sectionsObs: Observable<[TableSection]> {
        return sectionsRelay.asObservable()
    }

    var titleObs: Observable<String> {
        return titleRelay.asObservable()
    }

    func refreshMemberProfile() {

    }
}
