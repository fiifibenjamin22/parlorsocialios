//
//  MemberProfileViewController.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 20/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

class MemberProfileViewController: AppViewController {

    // MARK: - Properties
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }

    var viewModel: MemberProfileLogic!

    private let tableView: UITableView = UITableView(frame: .zero, style: .plain).manualLayoutable()

    private var tableManager: TableViewManager!

    let analyticEventScreen: AnalyticEventScreen? = .memberProfile

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        adjustNavigationBar()
    }
}

// MARK: - Private methods
extension MemberProfileViewController {
    private func adjustNavigationBar() {
        navigationController?.removeTransparentTheme()
        statusBar?.backgroundColor = .white
        navigationController?.setNavigationBarHidden(false, animated: false)
    }

    private func setup() {
        setupTableView()
        setupPresentationLogic()
    }

    private func setupTableView() {
        tableView.rowHeight = Constants.MemberProfileViewController.TableView.rowHeight
        tableView.separatorStyle = .none

        tableManager = TableViewManager(tableView: tableView, delegate: self)
        tableManager.hideLoadingFooter()

        view.addSubview(tableView)

        tableView.edgesToParent()
    }

    private func setupPresentationLogic() {
        bindTitle()
        bindSectionsState()
    }

    private func bindTitle() {
        viewModel.titleObs
            .bind { [unowned self] title in self.title = title }
            .disposed(by: disposeBag)
    }

    private func bindSectionsState() {
        viewModel.sectionsObs
            .bind { [unowned self] elements in self.tableManager.setupWith(sections: elements) }
            .disposed(by: disposeBag)
    }
}

// MARK: - Table Manager Delegate
extension MemberProfileViewController: TableManagerDelegate {
    func didSwipeForRefresh() {
        viewModel.refreshMemberProfile()
    }

    func loadMoreData() {}

    func tableViewDidScroll(_ scrollView: UIScrollView) { }
}

private extension Constants {
    enum MemberProfileViewController {
        enum TableView {
            static let rowHeight: CGFloat = UITableView.automaticDimension
        }
    }
}
