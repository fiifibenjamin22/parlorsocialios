//
//  WaitlistInfoViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 08/05/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

class WaitlistInfoViewController: AppViewController {
    
    // MARK: - Views
    private(set) var scrollView: UIScrollView!
    private(set) var contentScrollView: UIView!
    private(set) var informationView: GuestFormActivationInfoView!
    private(set) var removeMeButton: UIButton!
    private(set) var stackBackgroundView: UIView!
    private(set) var iconImageView: UIImageView!
    private(set) var titleLabel: UILabel!
    private(set) var messageLabel: UILabel!
    private(set) var centeredStack: UIStackView!
    private(set) var messageLabelTicketSelectorSeparator: UIView!
    private(set) var confirmTransferButton: UIButton!
    private(set) var ticketPurchaseSelector: WaitlistDropDownView!
    
    private(set) var confirmButtonAndInformationSpace: NSLayoutConstraint!
    private(set) var iconTopMargin: NSLayoutConstraint!

    // MARK: - Properties
    let analyticEventScreen: AnalyticEventScreen? = .waitlist
    private var viewModel: WaitlistInfoLogic!
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }
    
    override var observableSources: ObservableSources? {
        return viewModel
    }
    
    private var confirmButtonAndInformationSpaceValue: CGFloat {
        return (scrollView.frame.height - informationView.frame.height - centeredStack.frame.height) / 2
    }
    
    // MARK: - Initialization
    init(withActivation activation: Activation) {
        super.init(nibName: nil, bundle: nil)
        setup(withActivation: activation)
    }
    
    init(forActivationId activationId: Int) {
        super.init(nibName: nil, bundle: nil)
        setup(forActivationId: activationId)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError()
    }

    private func setup(withActivation activation: Activation) {
        self.viewModel = WaitlistInfoViewModel(activation: activation, applePayPresentationController: self)
    }
    
    private func setup(forActivationId activationId: Int) {
        self.viewModel = WaitlistInfoViewModel(forActivationId: activationId, applePayPresentationController: self)
    }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupNavigationBar()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        confirmButtonAndInformationSpace.constant = min(-confirmButtonAndInformationSpaceValue, -Constants.Margin.big)
        iconTopMargin.constant = max(confirmButtonAndInformationSpaceValue, Constants.Margin.big)
    }
}

// MARK: - Private methods
extension WaitlistInfoViewController {

    private func setupViews() {
        let builder = WaitlistInfoViewBuilder(controller: self)
        self.scrollView = builder.buildScrollView()
        self.contentScrollView = builder.buildView()
        self.removeMeButton = builder.buildRemoveMeButton()
        self.stackBackgroundView = builder.buildStackBackgroundView()
        self.informationView = builder.buildInformationView()
        self.iconImageView = builder.buildIconImage()
        self.messageLabel = builder.buildSubmessageLabel()
        self.titleLabel = builder.buildMainMessageLabel()
        self.centeredStack = builder.buildCenteredStack()
        self.messageLabelTicketSelectorSeparator = builder.buildView()
        self.confirmTransferButton = builder.buildConfirmButton()
        ticketPurchaseSelector = builder.buildWaitlistDropDownView()
        builder.setupViews()
        
        self.confirmButtonAndInformationSpace = builder.buildConfirmButtonAndInformationSpace()
        self.iconTopMargin = builder.buildIconTopMargin()
    }

    private func setupPresentationLogic() {
        observeActivationChanges()
        bindDestinations()
        bindCardSelector()
        removeMeButton.addTarget(self, action: #selector(didTapRemoveButton), for: .touchUpInside)
        confirmTransferButton.addTarget(self, action: #selector(didTapConfirmTransfer), for: .touchUpInside)
        viewModel.getActivation()
    }
    
    private func bindDestinations() {
        viewModel.destinationObs
            .subscribe(onNext: { [unowned self] destination in
                self.handle(destination: destination)
            }).disposed(by: disposeBag)
    }
    
    private func handle(destination: WaitListDestination) {
        switch destination {
        case .close:
            navigationController?.dismiss(animated: true)
        case .addCard:
            router.push(destination: destination)
        case .reservationConfirmed:
            navigationController?.setViewControllers([destination.viewController], animated: true)
        }
    }
    
    private func observeActivationChanges() {
        viewModel.activationObs
            .ignoreNil()
            .doOnNext { [weak self] activation in
                self?.informationView.configure(with: activation)
                self?.messageLabel.text = activation.rsvp?.status.messageText ?? Strings.Waitlist.submessage.localized
                self?.titleLabel.text = activation.rsvp?.status.mainMessageText ?? Strings.Waitlist.mainMessage.localized
                self?.messageLabelTicketSelectorSeparator.isHidden = activation.rsvp?.status != RsvpStatus.transferableToGuestList || !(activation.isPayable ?? false)
                self?.ticketPurchaseSelector.isHidden = activation.rsvp?.status != RsvpStatus.transferableToGuestList || !(activation.isPayable ?? false)
                self?.confirmTransferButton.isHidden = activation.rsvp?.status != RsvpStatus.transferableToGuestList
                self?.ticketPurchaseSelector.currency = activation.currency
            }.subscribe().disposed(by: disposeBag)
    }
    
    private func bindCardSelector() {
        viewModel.ticketPriceObs
            .subscribe(onNext: { [unowned self] price in
                self.ticketPurchaseSelector.titleValue = price
            }).disposed(by: disposeBag)
        
        viewModel.paymentMethodsObs
            .subscribe(
                onNext: { [unowned self] (paymentMethods, selectedMethod) in
                    self.ticketPurchaseSelector.configure(
                        with: DropDownData(
                            icon: Icons.GuestsForm.card,
                            title: Strings.GuestForm.price.localized,
                            popupTitle: Strings.GuestForm.ticket.localized,
                            additionalInformation: Strings.GuestForm.ticketInfo.localized,
                            emptyOptionsButtonText: Strings.GuestForm.addCard.localized.uppercased(),
                            options: paymentMethods,
                            popupAddBottomButtonTitle: Strings.Cards.addAnother.localized.uppercased())
                    )
                    self.ticketPurchaseSelector.selectedOption = selectedMethod
                    self.ticketPurchaseSelector.addOptionCallback = { [unowned self] in self.viewModel.addCardClicks.emitElement() }
                    self.ticketPurchaseSelector.addOptionInPopupCallback = { [unowned self] in self.viewModel.addCardClicks.emitElement() }
                },
                onError: { error in print(error) }
            ).disposed(by: disposeBag)
        
        ticketPurchaseSelector.selectionChangedCallback = { [weak self] index in
            self?.viewModel.selectedPaymentMethodIndexSubject.onNext(index)
        }
    }
    
    private func setupNavigationBar() {
        statusBar?.backgroundColor = .white
        self.title = Strings.Waitlist.title.localized.uppercased()
        self.navigationController?.setNavigationBarBorderColor()
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.bold),
             NSAttributedString.Key.kern: CGFloat(1.3)]
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: Icons.NavigationBar.close, style: .plain, target: self, action: #selector(didTapClose)).apply {
            $0.tintColor = UIColor.closeTintColor
        }
    }
    
    @objc private func didTapClose() {
        navigationController?.dismiss(animated: true)
    }
    
    @objc private func didTapRemoveButton() {
        viewModel.removeMeClicks.emitElement()
    }
    
    @objc private func didTapConfirmTransfer() {
        viewModel.confirmClicks.emitElement()
    }
}

fileprivate extension RsvpStatus {
    
    var messageText: String {
        switch self {
        case .transferableToGuestList:
            return Strings.Waitlist.confirmTransfer.localized
        default:
            return Strings.Waitlist.submessage.localized
        }
    }
    
    var mainMessageText: String {
        switch self {
        case .transferableToGuestList:
            return Strings.Waitlist.mainTransferableMessage.localized
        default:
            return Strings.Waitlist.mainMessage.localized
        }
    }
    
}
