//
//  WaitlistInfoDestination.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 17/05/2019.
//

import Foundation
import UIKit

enum WaitListDestination {
    case close
    case addCard
    case reservationConfirmed
}

extension WaitListDestination: Destination {
    
    var viewController: UIViewController {
        switch self {
        case .close:
            fatalError("This destination doesn't support view controller")
        case .addCard:
            return AddCardViewController(withApplyingType: .add)
        case .reservationConfirmed:
            return RsvpConfirmedViewController()
        }
    }
    
}
