//
//  WaitlistDropDownView.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 20/08/2019.
//

import UIKit

final class WaitlistDropDownView: UIView {
    
    private(set) var headerStackView: UIStackView!
    private(set) var headerTitleLabel: ParlorLabel!
    private(set) var headerValueLabel: ParlorLabel!
    
    private(set) var optionIcon: UIImageView!
    private(set) var optionArrowDown: UIImageView!
    private(set) var optionLabel: UILabel!
    private(set) var optionStackView: UIStackView!
    private(set) var optionBackgroundView: UIView!
    private(set) var optionButton: UIButton!
    
    private(set) var addOptionButton: UIButton!
    
    private(set) var contentStackView: UIStackView!
    
    private(set) var optionLeadingConstriant: NSLayoutConstraint!
    
    private unowned var hostedController: AppViewController
    
    private var model: DropDownData?
    
    private lazy var callback: (Int) -> Void = { [weak self] index in
        guard let model = self?.model else { return }
        self?.selectedOption = model.options[index]
        self?.selectionChangedCallback(index)
    }
    
    var addOptionInPopupCallback: () -> Void = {}
    
    var addOptionCallback: () -> Void = {}
    
    var selectedOption: DropDownOption? {
        didSet {
            setupSelectedOptionUi()
        }
    }
    
    var titleValue: Double? {
        didSet {
            setupTitleValue()
        }
    }
    
    var currency: Currency? {
        didSet {
            setupTitleValue()
        }
    }
    
    var selectionChangedCallback: (Int) -> Void = { _ in  }
    
    init(controller: AppViewController) {
        self.hostedController = controller
        super.init(frame: .zero)
        initialize()
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - Public methods
extension WaitlistDropDownView {
    
    func configure(with model: DropDownData) {
        self.model = model
        let canOpenOptionsPopup = model.options.count >= 1
        
        headerTitleLabel.text = model.title
        
        addOptionButton.setTitle(model.emptyOptionsButtonText, for: .normal)
        
        optionArrowDown.isHidden = !canOpenOptionsPopup
        optionButton.isUserInteractionEnabled = canOpenOptionsPopup
        if canOpenOptionsPopup {
            optionBackgroundView.applyTouchAnimation()
        } else {
            optionBackgroundView.gestureRecognizers?.removeAll()
        }
    }
    
}

// MARK: - Private methods
extension WaitlistDropDownView {
    
    private func initialize() {
        manualLayoutable()
        setupViews()
        
        optionButton.addTarget(self, action: #selector(didTapOptionsButton), for: .touchUpInside)
        addOptionButton.addTarget(self, action: #selector(didTapAddOptionButton), for: .touchUpInside)
    }
    
    private func setupViews() {
        let builder = WaitlistDropDownViewBuilder(view: self)
        
        headerStackView = builder.buildHeaderStackView()
        headerTitleLabel = builder.buildHeaderTitleLabel()
        headerValueLabel = builder.buildHeaderValueLabel()
        
        optionIcon = builder.buildOptionIcon()
        optionArrowDown = builder.buildOptionIcon()
        optionLabel = builder.buildOptionTitle()
        optionStackView = builder.buildOptionStackView()
        optionBackgroundView = builder.buildView()
        optionButton = builder.buildDropDownButton()
        
        addOptionButton = builder.buildAddOptionButton()
        contentStackView = builder.buildContentStackView()
        
        builder.setupViews()
        optionLeadingConstriant = builder.setupOptionStackViewLeading()
    }
    
    private func setupSelectedOptionUi() {
        guard let option = selectedOption else {
            optionButton.isHidden = true
            optionStackView.isHidden = true
            addOptionButton.isHidden = false
            optionBackgroundView.layer.borderWidth = 0
            return
        }
        optionButton.isHidden = false
        optionStackView.isHidden = false
        addOptionButton.isHidden = true
        optionBackgroundView.layer.borderWidth = 1
        optionLeadingConstriant.constant = option.icon == nil ? 21 : 10
        optionLabel.text = option.title
        optionIcon.image = option.icon
        optionIcon.isHidden = option.icon == nil
    }
    
    private func setupTitleValue() {
        guard let value = titleValue,
            let currency = currency else { return }
        headerValueLabel.text = value.getCurrencyString(currency: currency)
    }
    
    @objc private func didTapOptionsButton() {
        guard let model = model, let selectedOption = selectedOption else { return }
        hostedController.router.present(
            destination: DropDownPopupDestination.popup(
                data: model,
                selectedOption: selectedOption,
                callback: self.callback,
                addOptionInPopupCallback: addOptionInPopupCallback
            ),
            animated: false,
            withStyle: .overFullScreen)
    }
    
    @objc private func didTapAddOptionButton() {
        addOptionCallback()
    }
}

