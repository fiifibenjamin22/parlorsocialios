//
//  WaitlistDropDownViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 20/08/2019.
//

import UIKit

final class WaitlistDropDownViewBuilder {
    
    private unowned let view: WaitlistDropDownView
    
    init(view: WaitlistDropDownView) {
        self.view = view
    }
    
    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
    
}

extension WaitlistDropDownViewBuilder {
    
    private func setupProperties() {
        view.apply {
            $0.backgroundColor = .white
            $0.optionBackgroundView.backgroundColor = .clear
            $0.optionBackgroundView.layer.borderColor = UIColor.textVeryLight.cgColor
            $0.optionArrowDown.image = Icons.GuestsForm.arrow
            $0.optionBackgroundView.applyTouchAnimation()
            $0.addOptionButton.setImage(Icons.GuestsForm.add, for: .normal)
            $0.optionStackView.isHidden = true
            $0.optionButton.isHidden = true
        }
    }
    
    private func setupHierarchy() {
        view.apply {
            $0.addSubview($0.contentStackView)
            $0.contentStackView.addArrangedSubviews([$0.headerStackView, $0.optionBackgroundView])
            $0.headerStackView.addArrangedSubviews([$0.headerTitleLabel, $0.headerValueLabel])
            
            [$0.optionStackView, $0.optionButton, $0.addOptionButton].addTo(parent: $0.optionBackgroundView)
            $0.optionStackView.addArrangedSubviews([$0.optionIcon, $0.optionLabel, $0.optionArrowDown])
        }
    }
    
    private func setupAutoLayout() {
        view.apply {
            $0.contentStackView.edgesToParent()
            
            $0.headerTitleLabel.setContentHuggingPriority(.required, for: .horizontal)
            
            $0.optionBackgroundView.heightAnchor.equalTo(constant: 60)
            
            $0.optionStackView.edgesToParent(anchors: [.trailing], insets: UIEdgeInsets.margins(right: 20))
            $0.optionStackView.centerYAnchor.equal(to: $0.optionBackgroundView.centerYAnchor)
            
            $0.optionIcon.heightAnchor.equalTo(constant: 30)
            $0.optionIcon.widthAnchor.equal(to: $0.optionIcon.heightAnchor)
            
            $0.optionArrowDown.widthAnchor.equalTo(constant: 18)
            
            $0.optionButton.edges(equalTo: $0.optionBackgroundView)
            $0.addOptionButton.edgesToParent(anchors: [.top, .leading, .trailing])
            $0.addOptionButton.heightAnchor.equalTo(constant: Constants.bigButtonHeight)
        }
    }
    
    func setupOptionStackViewLeading() -> NSLayoutConstraint {
        return view.optionStackView.leadingAnchor.equal(to: view.optionBackgroundView.leadingAnchor, constant: 21)
    }
    
}

extension WaitlistDropDownViewBuilder {
    
    func buildHeaderTitleLabel() -> ParlorLabel {
        return ParlorLabel.styled(
            withTextColor: .appTextMediumBright,
            withFont: UIFont.custom(ofSize: Constants.FontSize.xTiny, font: UIFont.Roboto.light),
            alignment: .left,
            numberOfLines: 1)
            .manualLayoutable()
    }
    
    func buildHeaderValueLabel() -> ParlorLabel {
        return ParlorLabel.styled(
            withTextColor: .appTextMediumBright,
            withFont: UIFont.custom(ofSize: Constants.FontSize.xTiny, font: UIFont.Roboto.bold),
            alignment: .left,
            numberOfLines: 1)
            .manualLayoutable()
    }
    
    func builAdditionalInformationView() -> InformationView {
        return InformationView().apply {
            $0.icon = Icons.GuestsForm.info
            $0.tintColor = .appTextSemiLight
            $0.font = UIFont.custom(
                ofSize: Constants.FontSize.xTiny,
                font: UIFont.Roboto.regular)
            $0.iconSize = 11
        }
    }
    
    func buildDropDownButton() -> UIButton {
        return UIButton().manualLayoutable().apply {
            $0.setTitleColor(.appTextMediumBright, for: .normal)
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.regular)
            $0.setContentHuggingPriority(.required, for: .horizontal)
            $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        }
    }
    
    func buildOptionIcon() -> UIImageView {
        return UIImageView().manualLayoutable().apply {
            $0.contentMode = .scaleAspectFit
            $0.tintColor = .appTextBright
        }
    }
    
    func buildOptionTitle() -> UILabel {
        return UILabel.styled(
            textColor: .rsvpBorder,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.hintSize,
                font: UIFont.Roboto.regular),
            alignment: .left,
            numberOfLines: 1)
    }
    
    func buildContentStackView() -> UIStackView {
        return UIStackView(axis: .vertical, spacing: 5)
    }
    
    func buildOptionStackView() -> UIStackView {
        return UIStackView(axis: .horizontal, spacing: 10)
    }
    
    func buildAddOptionButton() -> UIButton {
        return UIButton().manualLayoutable().apply {
            $0.backgroundColor = .white
            $0.tintColor = .appText
            $0.layer.cornerRadius = Constants.bigButtonHeight / 2
            $0.layer.borderColor = UIColor.textVeryLight.cgColor
            $0.layer.borderWidth = 1
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium)
            $0.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 5)
            $0.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
            $0.setTitleColor(UIColor.appText, for: .normal)
            $0.applyTouchAnimation()
        }
    }
    
    func buildHeaderStackView() -> UIStackView {
        return UIStackView(axis: .horizontal, spacing: 3)
    }
    
    func buildView() -> UIView {
        return UIView().manualLayoutable().apply {
            $0.backgroundColor = .appBackground
            $0.clipsToBounds = true
        }
    }
    
}
