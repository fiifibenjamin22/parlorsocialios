//
//  WaitlistInfoViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 08/05/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxSwiftExt
import PassKit

protocol WaitlistInfoLogic: BaseViewModelLogic {
    var activationObs: Observable<Activation?> { get }
    var removeMeClicks: PublishSubject<Void> { get }
    var confirmClicks: PublishSubject<Void> { get }
    var confirmClicksFlow: PublishSubject<Void> { get }
    var selectedPaymentMethodIndexSubject: BehaviorSubject<Int?> { get }
    var ticketPriceObs: Observable<Double> { get }
    var paymentMethodsObs: Observable<([PaymentMethodOption], PaymentMethodOption?)> { get }
    var addCardClicks: PublishSubject<Void> { get }
    
    var destinationObs: Observable<WaitListDestination> { get }
    func getActivation()
}

class WaitlistInfoViewModel: BaseViewModel {
    let analyticEventReferencedId: Int?
    private let minimumPrice = 0.001
    private let activationRelay: BehaviorRelay<Activation?>
    private let activationsRepository = ActivationsRepository.shared
    private let profileRepository = ProfileRepository.shared
    private var applePayManager: ApplePayManager!
    private let cardManagementService = CardManagementService()
    private let destinationSubject = PublishSubject<WaitListDestination>()
    private let activationId: Int?
    private let calendarSyncViewProvider = CalendarSyncViewProvider()
    private let paymentMethodsSubject: BehaviorRelay<[PaymentMethodOption]> = BehaviorRelay(value: [])
    private var transferResponseAfterApplePayPayment: ApiResponse<MessageResponse>?

    let selectedPaymentMethodIndexSubject: BehaviorSubject<Int?> = BehaviorSubject(value: nil)
    let ticketPriceRelay: BehaviorRelay<Double> = BehaviorRelay(value: 0)
    let addCardClicks = PublishSubject<Void>()
    
    let removeMeClicks: PublishSubject<Void> = PublishSubject()
    let confirmClicksFlow: PublishSubject<Void> = PublishSubject()
    let confirmClicks: PublishSubject<Void> = PublishSubject()
    var pushNotificationId: Int?
    private var incrementRsvpCountHelper = IncrementRsvpCountHelper()
    
    private var selectedPaymentMethod: Observable<PaymentMethodOption?> {
        return Observable.combineLatest(paymentMethodsSubject, selectedPaymentMethodIndexSubject) { paymentMethods, selectedIndex in
            guard let index = selectedIndex,
                index < paymentMethods.count else { return nil }
            return paymentMethods[index]
        }
    }

    init(activation: Activation, applePayPresentationController: UIViewController) {
        activationRelay = BehaviorRelay(value: activation)
        self.activationId = nil
        analyticEventReferencedId = activation.id
        super.init()
        
        applePayManager = ApplePayManager(presentationController: applePayPresentationController, delegate: self)
        initFlow()
    }
    
    init(forActivationId activationId: Int, applePayPresentationController: UIViewController) {
        analyticEventReferencedId = activationId
        activationRelay = BehaviorRelay(value: nil)
        self.activationId = activationId
        super.init()
        
        applePayManager = ApplePayManager(presentationController: applePayPresentationController, delegate: self)
        initFlow()
    }
}

// MARK: Private logic
private extension WaitlistInfoViewModel {
    
    private func initFlow() {
        removeMeClicks.withLatestFrom(activationRelay)
            .ignoreNil()
            .flatMapFirst { [unowned self] activation in self.activationsRepository.cancelRsvp(forActivation: activation).showingProgressBar(with: self) }
            .doOnNext { [unowned self] response in self.handleCancelReservation(response) }
            .subscribe().disposed(by: disposeBag)
        
        confirmClicks
            .withLatestFrom(ticketPriceRelay)
            .withLatestFrom(selectedPaymentMethod) { ($0, $1) }
            .subscribe(onNext: { [unowned self] tuple in
                let (ticketPrice, selectedPaymentMethod) = tuple
                self.handleConfirmButtonClick(ticketPrice: ticketPrice, selectedPaymentMethod: selectedPaymentMethod)
            }).disposed(by: disposeBag)
        
        confirmClicksFlow.withLatestFrom(activationRelay)
            .ignoreNil()
            .withLatestFrom(selectedPaymentMethod) { ($0, $1) }
            .subscribe(onNext: { [unowned self] tuple in
                let (activation, selectedPaymentMethod) = tuple
                self.transferRsvpToWaitlist(usingActivation: activation, andPaymentMethod: selectedPaymentMethod)
            })
            .disposed(by: disposeBag)
        
        paymentMethodsSubject
            .subscribe(onNext: { [unowned self] paymentMethods in
                self.setDefaultPaymentMethod(usingPaymentMethods: paymentMethods)
            }).disposed(by: disposeBag)
        
        loadUserCards()
        
        profileRepository.knownCardsObs
            .map { $0 as [PaymentMethodOption] }
            .bind(onNext: { [unowned self] cards in
                var paymentMethods: [PaymentMethodOption] = cards
                paymentMethods.insert(ApplePayPaymentMethodOption(), at: 0)
                self.paymentMethodsSubject.accept(paymentMethods)
            })
            .disposed(by: disposeBag)
        
        addCardClicks
            .subscribe(onNext: { [unowned self] _ in
                self.destinationSubject.onNext(.addCard)
            }).disposed(by: disposeBag)
        
        activationRelay
            .map { activation in
                (activation?.ticketPrice ?? 0) * Double((activation?.rsvp?.guests.count ?? 0) + 1)
            }.bind(to: ticketPriceRelay)
            .disposed(by: disposeBag)
        
        activationRelay
            .filter { $0 == nil || !$0!.shouldShowWaitlist }
            .subscribe(onNext: { [weak self] _ in
                self?.alertDataSubject.onNext(AlertData.fromSimpleMessage(
                    message: Strings.Waitlist.notOnWaitlist.localized,
                    withIcon: Icons.Error.general,
                    action: { [weak self] in self?.closeViewSubject.emitElement() },
                    closeButtonAction: { [weak self] in self?.closeViewSubject.emitElement() }))
            }).disposed(by: disposeBag)
    }
    
    private func loadUserCards() {
        profileRepository.getUserCards()
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    private func setDefaultPaymentMethod(usingPaymentMethods paymentMethods: [PaymentMethodOption]) {
        let defaultMethodIndex = paymentMethods.firstIndex(where: { $0.isDefault }) ?? paymentMethods.firstIndex(where: { $0.type == .applePay})
        selectedPaymentMethodIndexSubject.onNext(defaultMethodIndex)
    }
    
    private func transferRsvpToWaitlist(usingActivation activation: Activation, andPaymentMethod paymentMethod: PaymentMethodOption?) {
        if let paymentMethod = paymentMethod, paymentMethod.type == .applePay && ticketPriceRelay.value > minimumPrice {
            payUsingApplePay(usingActivation: activation)
        } else {
            var cardId = paymentMethod?.id
            if !(activation.isPayable ?? false) || cardId == ApplePayPaymentMethodOption.applePayOptionId {
                cardId = nil
            }
            let rsvpTransferRequest = RsvpTransferRequest(cardId: cardId)
            activationsRepository.transferRsvpToGuestList(forActivation: activation, data: rsvpTransferRequest)
                .showingProgressBar(with: self)
                .subscribe(onNext: { [unowned self] response in
                    self.handleFinalMessageResponse(response)
                })
                .disposed(by: disposeBag)
        }
    }
    
    private func payUsingApplePay(usingActivation activation: Activation) {
        guard let currencyCode = activation.currency?.code else {
            alertDataSubject.onNext(AlertData.applePayErrorAlert(withTitle: Strings.ApplePay.unexpectedError.localized))
            return
        }
        
        let paymentItems = ApplePayPaymentSummaryItemsProvider.createPaymentItemsForActivation(usingName: activation.name, price: activation.ticketPrice)
        let paymentData = ApplePayPaymentData(currencyCode: currencyCode, paymentItems: paymentItems)
        do {
            try applePayManager.makePayment(usingData: paymentData)
        } catch ApplePayError.notAvailable {
            alertDataSubject.onNext(AlertData.applePayErrorAlert(withTitle: ApplePayError.notAvailable.localizedDescription))
        } catch {
            alertDataSubject.onNext(AlertData.applePayErrorAlert(withTitle: Strings.ApplePay.notAvailable.localized))
        }
    }
    
    private func handleFinalMessageResponse(_ resposne: ApiResponse<MessageResponse>) {
        switch resposne {
        case .failure(let error):
            errorSubject.onNext(error)
        case .success:
            if !calendarSyncViewProvider.showCalendarSyncViewController() {
                destinationSubject.onNext(.reservationConfirmed)
            } else {
                destinationSubject.onNext(.close)
            }
            incrementRsvpCountHelper.incrementRsvpCountersAfterRsvp()
            saveRsvpCreatedEvent()
        }
    }
    
    private func handleCancelReservation(_ response: ApiResponse<MessageResponse>) {
        switch response {
        case .failure(let error):
            errorSubject.onNext(error)
        case .success:
            destinationSubject.onNext(.close)
        }
    }
    
    private func handleActivationResponse(_ response: SingleActivationResposne) {
        switch response {
        case .success(let resposne):
            activationRelay.accept(resposne.data)
        case .failure(let error):
            errorSubject.onNext(error)
        }
    }
    
    private func handleConfirmButtonClick(ticketPrice: Double, selectedPaymentMethod: PaymentMethodOption?) {
        if emptyCardAlertIsNeeded(isSelectedPaymentMethodEmpty: selectedPaymentMethod == nil) {
            showEmptyCardAlert()
        } else if payableAlertIsNeeded(price: ticketPrice, selectedPaymentMethod: selectedPaymentMethod) {
            showPayableAlert()
        } else {
            confirmClicksFlow.emitElement()
        }
    }
    
    private func payableAlertIsNeeded(price: Double, selectedPaymentMethod: PaymentMethodOption?) -> Bool {
        guard selectedPaymentMethod?.type == .card else { return false }
        return (activationRelay.value?.isPayable ?? false) && price >= minimumPrice
    }
    
    private func emptyCardAlertIsNeeded(isSelectedPaymentMethodEmpty: Bool) -> Bool {
        return (activationRelay.value?.isPayable ?? false) && isSelectedPaymentMethodEmpty
    }
    
    private func showPayableAlert() {
        alertDataSubject.onNext(AlertData(
            icon: Icons.Cards.add,
            title: Strings.GuestForm.paymentConfirmationAlert.localized,
            message: nil,
            bottomButtonTitle: Strings.yes.localized.uppercased(),
            buttonAction: { [unowned self] in self.confirmClicksFlow.emitElement()}
        ))
    }
    
    private func showEmptyCardAlert() {
        alertDataSubject.onNext(AlertData(
            icon: Icons.Cards.remove,
            title: Strings.GuestForm.emptyCardAlert.localized,
            message: nil,
            bottomButtonTitle: Strings.ok.localized.uppercased(),
            buttonAction: {}
        ))
    }
    
    private func saveRsvpCreatedEvent() {
        guard let sessionId = AnalyticService.shared.sessionId,
            let userId = Config.userProfile?.id else { return }
        let event = AnalyticEvent(
            userId: userId,
            deviceToken: Config.deviceToken,
            timestamp: Int64(Date().timeIntervalSince1970 * 1000),
            type: .rsvpCreated,
            data: .createForSpecialType(referencedId: analyticEventReferencedId),
            sessionId: sessionId
        )
        AnalyticService.shared.saveEventInDatabase(event)
    }
}

// MARK: Interface logic methods
extension WaitlistInfoViewModel: WaitlistInfoLogic {
    
    var destinationObs: Observable<WaitListDestination> {
        return destinationSubject.asObservable()
    }
    
    var activationObs: Observable<Activation?> {
        return activationRelay.asObservable()
    }
    
    func getActivation() {
        guard let activationId = activationId else { return }
        activationsRepository.getActivation(withId: activationId)
            .showingProgressBar(with: self)
            .subscribe(onNext: { [weak self] response in
                self?.handleActivationResponse(response)
            }).disposed(by: disposeBag)
    }
    
    var ticketPriceObs: Observable<Double> {
        return ticketPriceRelay
            .map { max(($0 * 100).rounded() / 100, 0) }
            .asObservable()
    }
    
    var paymentMethodsObs: Observable<([PaymentMethodOption], PaymentMethodOption?)> {
        return paymentMethodsSubject
            .withLatestFrom(selectedPaymentMethod) { ($0, $1) }
            .asObservable()
    }

}

// MARK: - ApplePayManagerDelegate

extension WaitlistInfoViewModel: ApplePayManagerDelegate {
    func applePayManager(_ manager: ApplePayManager, didFinishWith response: StripeTokenResponse, paymentCompletionHandler completion: @escaping (PKPaymentAuthorizationStatus) -> Void) {
        if let token = response.token {
            let tokenRequest = CardTokenRequest(tokenId: token.tokenId)
            cardManagementService.addCard(usingToken: tokenRequest)
                .filterMap { [unowned self] result -> FilterMap<CardData> in
                    switch result {
                    case .success(let item):
                        return .map(item.data)
                    case .failure(let error):
                        completion(.failure)
                        self.applePayManager.paymentErrorToPresent = error
                        return .ignore
                    }
                }
                .withLatestFrom(activationRelay) { ($0, $1) }
                .flatMapFirst { [unowned self] tuple -> MessageApiResponse in
                    let (card, activation) = tuple
                    let rsvpTransferRequest = RsvpTransferRequest(cardId: card.id)
                    return self.activationsRepository.transferRsvpToGuestList(forActivation: activation!, data: rsvpTransferRequest)
                }
                .subscribe(onNext: { [unowned self] response in
                    switch response {
                    case .success:
                        completion(.success)
                        self.transferResponseAfterApplePayPayment = response
                    case .failure(let error):
                        completion(.failure)
                        self.applePayManager.paymentErrorToPresent = error
                        self.loadUserCards()
                    }
                })
                .disposed(by: disposeBag)
            
        } else {
            completion(.failure)
            applePayManager.paymentErrorToPresent = AppError(with: response.error)
        }
    }
    
    func applePayManagerDidDismissPaymentController(_ manager: ApplePayManager) {
        if let error = manager.paymentErrorToPresent {
            errorSubject.onNext(error)
        } else if let response = transferResponseAfterApplePayPayment {
            handleFinalMessageResponse(response)
            transferResponseAfterApplePayPayment = nil
        } else {
            progressBarSubject.onNext(false)
        }
    }
}

fileprivate extension Activation {
    var shouldShowWaitlist: Bool {
        return rsvp?.status == .transferableToGuestList || rsvp?.status == .waitList
    }
}
