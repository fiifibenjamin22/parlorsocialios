//
//  WaitlistInfoViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 08/05/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class WaitlistInfoViewBuilder {

    private unowned let controller: WaitlistInfoViewController
    private var view: UIView! { return controller.view }

    init(controller: WaitlistInfoViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension WaitlistInfoViewBuilder {

    private func setupProperties() {
        view.backgroundColor = .white
        controller.apply {
            $0.removeMeButton.setTitle(Strings.Waitlist.removeMe.localized.uppercased(), for: .normal)
            $0.titleLabel.text = Strings.Waitlist.mainMessage.localized
            $0.messageLabel.text = Strings.Waitlist.submessage.localized
            $0.confirmTransferButton.setTitle(Strings.confirm.localized.uppercased(), for: .normal)
        }
    }

    private func setupHierarchy() {
        controller.apply {
            view.addSubviews([$0.scrollView, $0.removeMeButton])
            
            $0.scrollView.addSubview($0.contentScrollView)
            
            $0.centeredStack.addArrangedSubviews([
                $0.iconImageView,
                $0.titleLabel,
                $0.messageLabel,
                $0.messageLabelTicketSelectorSeparator,
                $0.ticketPurchaseSelector,
                $0.confirmTransferButton])
            
            $0.contentScrollView.addSubviews([$0.stackBackgroundView, $0.centeredStack, $0.informationView])
        }
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.scrollView.edgesToParent(anchors: [.top, .leading, .trailing])
            $0.scrollView.bottomAnchor.equal(to: $0.removeMeButton.topAnchor)
    
            $0.contentScrollView.edgesToParent()
            $0.contentScrollView.widthAnchor.equal(to: $0.scrollView.widthAnchor)
            
            $0.stackBackgroundView.bottomAnchor.equal(to: $0.informationView.topAnchor)
            $0.stackBackgroundView.edgesToParent(anchors: [.leading, .trailing, .top])
            
            $0.removeMeButton.edgesToParent(anchors: [.leading, .trailing, .bottom])
            $0.removeMeButton.heightAnchor.equalTo(constant: Constants.extraBigButtonHeight)
            
            $0.informationView.edgesToParent(anchors: [.leading, .trailing, .bottom])
            
            $0.centeredStack.edgesToParent(anchors: [.leading, .trailing], padding: Constants.horizontalMargin)
            
            $0.ticketPurchaseSelector.widthAnchor.equal(to: $0.centeredStack.widthAnchor)
            
            $0.messageLabelTicketSelectorSeparator.heightAnchor.equalTo(constant: 23)
            
            $0.confirmTransferButton.widthAnchor.equal(to: $0.centeredStack.widthAnchor)
        }
    }

}

// MARK: - Public build methods
extension WaitlistInfoViewBuilder {
    
    func buildScrollView() -> UIScrollView {
        return UIScrollView().manualLayoutable()
    }
    
    func buildView() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildInformationView() -> GuestFormActivationInfoView {
        return GuestFormActivationInfoView().manualLayoutable()
    }
    
    func buildRemoveMeButton() -> UIButton {
        return UIButton().manualLayoutable().apply {
            $0.backgroundColor = .white
            $0.addShadow()
            $0.tintColor = .appText
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.medium)
            $0.setTitleColor(UIColor.appText, for: .normal)
            $0.applyTouchAnimation()
        }
    }
    
    func buildMainMessageLabel() -> ParlorLabel {
        return ParlorLabel.styled(withTextColor: .appTextBright, withFont: UIFont.custom(ofSize: Constants.FontSize.hintSize, font: UIFont.Roboto.medium), alignment: .center, numberOfLines: 0)
    }
    
    func buildSubmessageLabel() -> ParlorLabel {
        return ParlorLabel.styled(withTextColor: .textFieldUnderline, withFont: UIFont.custom(ofSize: Constants.FontSize.xTiny, font: UIFont.Roboto.regular), alignment: .center, numberOfLines: 0)
    }
    
    func buildIconImage() -> UIImageView {
        return UIImageView(image: Icons.Waitlist.mainIcon).apply {
            $0.contentMode = .scaleAspectFit
            $0.tintColor = UIColor.appTextBright
        }
    }
    
    func buildCenteredStack() -> UIStackView {
        return UIStackView(
            axis: .vertical,
            spacing: Constants.Margin.standard,
            alignment: .center,
            distribution: .equalSpacing)
            .manualLayoutable()
    }
    
    func buildConfirmButton() -> UIButton {
        return RoundedButton().manualLayoutable().apply {
            $0.backgroundColor = .white
            $0.layer.borderColor = UIColor.textVeryLight.cgColor
            $0.layer.borderWidth = 1
            $0.applyTouchAnimation()
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.medium)
            $0.setTitleColor(.appText, for: .normal)
            $0.heightAnchor.equalTo(constant: Constants.bigButtonHeight)
        }
    }
    
    func buildStackBackgroundView() -> UIView {
        return UIView().manualLayoutable().apply { $0.backgroundColor = .white }
    }
    
    func buildConfirmButtonAndInformationSpace() -> NSLayoutConstraint {
        return controller.centeredStack.bottomAnchor.equal(to: controller.informationView.topAnchor, constant: -Constants.Margin.big)
    }
    
    func buildIconTopMargin() -> NSLayoutConstraint {
        return controller.centeredStack.topAnchor.equal(to: controller.contentScrollView.topAnchor, constant: Constants.Margin.big)
    }
    
    func buildWaitlistDropDownView() -> WaitlistDropDownView {
        return WaitlistDropDownView(controller: controller)
    }

}
