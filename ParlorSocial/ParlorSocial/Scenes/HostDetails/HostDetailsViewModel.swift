//
//  HostDetailsViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 17/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxSwift

protocol HostDetailsLogic: BaseViewModelLogic {
    var hostObservable: Observable<ParlorHost> { get }
}

class HostDetailsViewModel: BaseViewModel {
    let analyticEventReferencedId: Int?
    private let hostSubject: BehaviorSubject<ParlorHost>
    
    init(host: ParlorHost) {
        self.hostSubject = BehaviorSubject(value: host)
        self.analyticEventReferencedId = host.id
        super.init()
    }

}

// MARK: Private logic
private extension HostDetailsViewModel {

}

// MARK: Interface logic methods
extension HostDetailsViewModel: HostDetailsLogic {
    
    var hostObservable: Observable<ParlorHost> {
        return hostSubject.asObservable()
    }

}
