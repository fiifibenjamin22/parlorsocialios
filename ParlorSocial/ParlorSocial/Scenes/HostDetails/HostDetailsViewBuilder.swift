//
//  HostDetailsViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 17/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class HostDetailsViewBuilder {

    private unowned let controller: HostDetailsViewController
    private var view: UIView! { return controller.view }

    init(controller: HostDetailsViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension HostDetailsViewBuilder {

    private func setupProperties() {
        view.backgroundColor = .white
    }

    private func setupHierarchy() {
        controller.apply {
            view.addSubview($0.scrollView)
            
            $0.scrollView.addSubview($0.scrollContent)
            
            $0.scrollContent.addSubviews([$0.hostImage, $0.infoStack])
        }
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.scrollView.edgesToParent()
            
            $0.scrollContent.edgesToParent()
            $0.scrollContent.widthAnchor.equal(to: view.widthAnchor)
            
            $0.hostImage.edgesToParent(anchors: [.top, .leading, .trailing])
            $0.hostImage.widthAnchor.constraint(equalTo: $0.hostImage.heightAnchor, multiplier: Constants.HostDetails.hostImageRatio).activate()
            
            $0.infoStack.edgesToParent(anchors: [.leading, .trailing], padding: Constants.horizontalMargin)
            $0.infoStack.topAnchor.equal(to: $0.hostImage.bottomAnchor, constant: Constants.HostDetails.stackTop)
            $0.infoStack.edgesToParent(anchors: [.bottom], padding: Constants.HostDetails.stackBottom)
        }
    }

}

// MARK: - Public build methods
extension HostDetailsViewBuilder {

    func buildScrollView() -> UIScrollView {
        return UIScrollView().manualLayoutable()
    }
    
    func buildScrollContent() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildHostImageView() -> UIImageView {
        return UIImageView().manualLayoutable().apply {
            $0.contentMode = .scaleAspectFill
            $0.clipsToBounds = true
        }
    }
    
    func buildInfoStack() -> UIStackView {
        return UIStackView(axis: .vertical, with: [], spacing: 0, alignment: .fill, distribution: .equalSpacing).manualLayoutable()
    }
}

private extension Constants {
    
    enum HostDetails {
        static let hostImageRatio: CGFloat = 375.0 / 280.0
        static let stackTop: CGFloat = 26
        static let stackBottom: CGFloat = 26
    }
    
}
