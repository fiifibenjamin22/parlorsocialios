//
//  HostDetailsViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 17/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

class HostDetailsViewController: AppViewController {

    // MARK: - Properties
    private var viewModel: HostDetailsLogic!
    let analyticEventScreen: AnalyticEventScreen? = .hostDetails
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }

    // MARK: - Views
    private(set) var scrollView: UIScrollView!
    private(set) var scrollContent: UIView!
    private(set) var hostImage: UIImageView!
    private(set) var infoStack: UIStackView!
    
    // MARK: - Initialization
    init(withHost host: ParlorHost) {
        super.init(nibName: nil, bundle: nil)
        setup(withHost: host)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }

    private func setup(withHost host: ParlorHost) {
        self.viewModel = HostDetailsViewModel(host: host)
    }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        adjustNavigationBar()
    }
    
    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)
        cleanNavBarWhenPopToActivationDetails()
    }

}

// MARK: - Private methods
extension HostDetailsViewController {
    
    private func adjustNavigationBar() {
        navigationController?.removeTransparentTheme()
        statusBar?.backgroundColor = .white
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    private func cleanNavBarWhenPopToActivationDetails() {
        if let viewControllers = navigationController?.viewControllers {
            if viewControllers[viewControllers.count - 2] is ActivationDetailsViewController {
                navigationController?.applyTransparentTheme(withColor: .white)
            }
        }
    }

    private func setupViews() {
        let builder = HostDetailsViewBuilder(controller: self)
        self.scrollView = builder.buildScrollView()
        self.scrollContent = builder.buildScrollContent()
        self.hostImage = builder.buildHostImageView()
        self.infoStack = builder.buildInfoStack()
        builder.setupViews()
    }

    private func setupPresentationLogic() {
        viewModel.hostObservable
            .subscribe(onNext: { [weak self] host in
                self?.populate(with: host)
            }).disposed(by: disposeBag)
    }
    
    private func populate(with host: ParlorHost) {
        navigationItem.title = host.name.uppercased()
        hostImage.getImage(from: host.imageUrl, ifFailedOrNilSet: Icons.Common.hostPlaceholder)
        infoStack.removeArrangedSubviews()
        infoStack.addArrangedSubview(createHostNameInfo(host: host))
        infoStack.addDefaultSeperator(alpha: 0.44)
        if host.displayType == .about {
            addAttributedAboutInfo(text: host.info.about)
        } else {
            addInfoIfNeeded(title: Strings.HostDetails.position.localized, text: host.info.position)
            addInfoIfNeeded(title: Strings.HostDetails.company.localized, text: host.info.company)
            addInfoIfNeeded(title: Strings.HostDetails.education.localized, text: host.info.education)
            addInfoIfNeeded(title: Strings.HostDetails.interestGroups.localized, text: host.info.formattedGroups)
        }
        infoStack.removeLastSubview()
    }
    
    private func addAttributedAboutInfo(text: String?) {
        guard let about = text else { return }
        let hostInfo = HostInfoItemView().apply {
            $0.itemName.text = Strings.HostDetails.about.localized.uppercased()
            $0.itemText.attributedText = about.attributtedText(withColor: .appTextMediumBright, font: UIFont.custom(ofSize: Constants.FontSize.hintSize, font: UIFont.Roboto.regular))
        }
        infoStack.addArrangedSubview(hostInfo)
        infoStack.addDefaultSeperator(alpha: 0.44)
    }
    
    private func addInfoIfNeeded(title: String, text: String?) {
        if let text = text, !text.isEmpty {
            infoStack.addArrangedSubview(createInfo(withTitle: title.uppercased(), text: text))
            infoStack.addDefaultSeperator(alpha: 0.44)
        }
    }
    
    private func createInfo(withTitle title: String, text: String) -> HostInfoItemView {
        return HostInfoItemView().apply {
            $0.itemName.text = title
            $0.itemText.text = text
        }
    }
    
    private func createHostNameInfo(host: ParlorHost) -> HostInfoItemView {
        return HostInfoItemView().apply {
            $0.itemName.text = host.role.displayName.uppercased()
            $0.itemText.font = UIFont.custom(ofSize: 24, font: UIFont.Editor.bold)
            $0.itemText.text = host.name
            $0.itemText.lineHeight = 36
        }
    }

}
