//
//  AddProfilePhotoViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 16/10/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class AddProfilePhotoViewBuilder {

    private unowned let controller: AddProfilePhotoViewController
    private var view: UIView! { return controller.view }

    init(controller: AddProfilePhotoViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension AddProfilePhotoViewBuilder {

    private func setupProperties() {
        view.backgroundColor = .white
        controller.apply {
            $0.continueButton.setTitle(Strings.ResetPassword.continueButton.localized.uppercased(), for: .normal)
        }
    }

    private func setupHierarchy() {
        controller.apply {
            view.addSubviews([$0.iconedMessage, $0.continueButton])
        }
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.iconedMessage.edgesToParent(anchors: [.leading, .trailing],
                                           insets: .margins(left: Constants.horizontalMarginBig, right: Constants.horizontalMarginBig))
            $0.iconedMessage.topAnchor.equal(to: view.safeAreaLayoutGuide.topAnchor, constant: 70)
            
            $0.continueButton.edgesToParent(anchors: [.leading, .trailing],
                                           insets: .margins(left: Constants.horizontalMarginBig, right: Constants.horizontalMarginBig))
            $0.continueButton.bottomAnchor.equal(to: view.bottomAnchor, constant: -90)
            $0.continueButton.heightAnchor.equalTo(constant: Constants.bigButtonHeight)
        }
    }

}

// MARK: - Public build methods
extension AddProfilePhotoViewBuilder {
    func buildIconedMessageView() -> IconedMessageView {
        return IconedMessageView(icon: Icons.AddProfilePhoto.addProfileAvatar,
                                 title: Strings.AddProfilePhoto.title.localized,
                                 message: Strings.AddProfilePhoto.pictureInfoMessage.localized).manualLayoutable()
    }
    
    func buildContinueButton() -> RoundedButton {
        return RoundedButton().manualLayoutable().apply { it in
            it.backgroundColor = UIColor.black
            it.setTitleColor(UIColor.white, for: .normal)
            it.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium)
            it.applyTouchAnimation()
        }
    }
}
