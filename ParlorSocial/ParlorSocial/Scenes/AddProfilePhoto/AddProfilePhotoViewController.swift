//
//  AddProfilePhotoViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 16/10/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

class AddProfilePhotoViewController: AppViewController {
    // MARK: - Views
    private(set) var iconedMessage: IconedMessageView!
    private(set) var continueButton: RoundedButton!
    
    // MARK: - Properties
    private var viewModel: AddProfilePhotoLogic!
    let analyticEventScreen: AnalyticEventScreen? = .addProfilePhoto
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }
    
    override var observableSources: ObservableSources? {
        return viewModel
    }

    // MARK: - Initialization
    init(forActivationData data: ActivationRoutingData) {
        super.init(nibName: nil, bundle: nil)
        setup(forActivationData: data)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let className = String(describing: self)
        fatalError("Error with initialize \(className)")
    }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        adjustNavigationBar()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
    }
    
    override func observeViewModel() {
        super.observeViewModel()
        viewModel.destinationObs
            .subscribe(onNext: { [unowned self] destination in
                self.handleDestination(destination)
            }).disposed(by: disposeBag)
    }

}

// MARK: - Private methods
extension AddProfilePhotoViewController {
    
    private func setup(forActivationData data: ActivationRoutingData) {
        self.viewModel = AddProfilePhotoViewModel(forActivationData: data)
    }

    private func setupViews() {
        let builder = AddProfilePhotoViewBuilder(controller: self)
        iconedMessage = builder.buildIconedMessageView()
        continueButton = builder.buildContinueButton()
        builder.setupViews()
    }

    private func setupPresentationLogic() {
        continueButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapEditPhoto)))
    }
    
    private func adjustNavigationBar() {
        navigationController?.applyProfileItemAppearance(withTitle: "")
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        statusBar?.backgroundColor = .white
        setNeedsStatusBarAppearanceUpdate()
        guard self.presentingViewController != nil else { return }
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: Icons.NavigationBar.close, style: .plain, target: self, action: #selector(tapBackButton))
    }

    private func pickImage() {
        let controller = UIImagePickerController().apply {
            $0.delegate = self
            $0.sourceType = .photoLibrary
            $0.allowsEditing = false
            $0.navigationBar.tintColor = .black
        }
        present(controller, animated: true, completion: nil)
    }
    
    private func handleDestination(_ destination: AddProfilePhotoDestination) {
        switch destination {
        case .activationDetails:
            guard let currentController = navigationController?.viewControllers else { return }
            navigationController?.setViewControllers(currentController.dropLast() + [destination.viewController], animated: false)
        }
    }
    
    @objc private func didTapEditPhoto() {
        pickImage()
    }
    
    @objc func tapBackButton() {
        parent?.dismiss()
    }
}

extension AddProfilePhotoViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true)
        guard let image = info[.originalImage] as? UIImage else { return  }
        viewModel.manualImageSubject.onNext(image)
    }
}

extension AddProfilePhotoViewController: UINavigationControllerDelegate {}
