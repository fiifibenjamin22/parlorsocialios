//
//  AddProfilePhotoDestination.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 18/10/2019.
//  Copyright © 2019 KISSdigital. All rights reserved.
//

import Foundation
import UIKit

enum AddProfilePhotoDestination {
    case activationDetails(data: ActivationRoutingData)
}

extension AddProfilePhotoDestination: Destination {
    
    var viewController: UIViewController {
        switch self {
        case .activationDetails(let data):
            return ActivationDetailsNavigationProvider.getActivationDetailsVC(for: data)
        }
    }
    
}
