//
//  AddProfilePhotoViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 16/10/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxSwift

protocol AddProfilePhotoLogic: BaseViewModelLogic {
    var manualImageSubject: PublishSubject<UIImage?> { get }
    var destinationObs: Observable<AddProfilePhotoDestination> { get }
}

class AddProfilePhotoViewModel: BaseViewModel {
    // MARK: - Properties
    let analyticEventReferencedId: Int? = nil
    private let destinationSubject: PublishSubject<AddProfilePhotoDestination> = PublishSubject()
    let manualImageSubject: PublishSubject<UIImage?> = PublishSubject()
    let profileRepository = ProfileRepository.shared
    let maxSizeOfPhoto: CGFloat = 500
    
    let activationRoutingData: ActivationRoutingData
    
    // MARK: - Initialization
    init(forActivationData data: ActivationRoutingData) {
        self.activationRoutingData = data
        super.init()
        initFlow()
    }
    
}

// MARK: Private logic
private extension AddProfilePhotoViewModel {
    private func initFlow() {
        manualImageSubject
            .flatMap { [unowned self] image -> Observable<ProfileResponse> in
                if let image = image?.scale(toMaxSize: CGSize(width: self.maxSizeOfPhoto, height: self.maxSizeOfPhoto)) {
                    return self.profileRepository.uploadNewPhoto(image).showingProgressBar(with: self)
                } else {
                    self.alertDataSubject.onNext(AlertData.addProfilePhoto())
                    return Observable.empty()
                } }
            .subscribe(onNext: { [unowned self] response in
                self.handleUploadImage(response)
            }).disposed(by: disposeBag)
    }
    
    private func handleUploadImage(_ response: ProfileResponse) {
        switch response {
        case .success:
            destinationSubject.onNext(.activationDetails(data: activationRoutingData))
        case .failure(let error):
            errorSubject.onNext(error)
        }
    }
}

// MARK: Interface logic methods
extension AddProfilePhotoViewModel: AddProfilePhotoLogic {
    var destinationObs: Observable<AddProfilePhotoDestination> {
        destinationSubject.asObservable()
    }
}
