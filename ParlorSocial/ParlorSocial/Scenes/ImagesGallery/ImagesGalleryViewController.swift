//
//  ImagesGalleryViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 25/06/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import CHIPageControl
import RxSwift

class ImagesGalleryViewController: AppViewController {
    
    // MARK: - Views
    private(set) var scrollView: UIScrollView!
    private(set) var pageControl: CHIPageControlAleppo!
    private(set) var closeButton: UIButton!
    private(set) var contentStack: UIStackView!
    private(set) var imageViews: [UIImageView] = []

    // MARK: - Properties
    private let imageUrls: [URL]
    private var viewModel: BaseViewModelLogic!
    let analyticEventScreen: AnalyticEventScreen? = .gallery
       
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Initialization
    init(withUrls urls: [URL]) {
        self.imageUrls = urls
        super.init(nibName: nil, bundle: nil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        viewModel = BaseViewModelClass()
    }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.statusBar?.backgroundColor = .appText
    }
    
    @objc private func didTapClose() {
        dismiss(animated: true)
    }

}

// MARK: - Private methods
extension ImagesGalleryViewController {

    private func setupViews() {
        let builder = ImagesGalleryViewBuilder(controller: self)
        self.scrollView = builder.buildScrollView()
        self.closeButton = builder.buildCloseButton()
        self.pageControl = builder.buildPageControl()
        self.contentStack = builder.buildContentStack()
        self.imageViews = builder.buildImageViews(count: imageUrls.count)
        builder.setupViews()
    }

    private func setupPresentationLogic() {
        scrollView.delegate = self
        closeButton.addTarget(self, action: #selector(didTapClose), for: .touchUpInside)
        pageControl.numberOfPages = imageUrls.count
        imageViews.enumerated().forEach { number, imageView in
            imageView.getImage(from: imageUrls[number])
        }
    }

}

// MARK: - UIScrollView Delage

extension ImagesGalleryViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageControl.progress = Double(scrollView.currentPage)
    }
    
}
