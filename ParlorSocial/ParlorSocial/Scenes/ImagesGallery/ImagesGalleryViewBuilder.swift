//
//  ImagesGalleryViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 25/06/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import CHIPageControl

class ImagesGalleryViewBuilder {

    private unowned let controller: ImagesGalleryViewController
    private var view: UIView! { return controller.view }

    init(controller: ImagesGalleryViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension ImagesGalleryViewBuilder {

    private func setupProperties() {
        view.backgroundColor = .darkBackground
    }

    private func setupHierarchy() {
        controller.apply {
            view.addSubviews([$0.scrollView, $0.closeButton, $0.pageControl])
            $0.scrollView.addSubview($0.contentStack)
        }
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.scrollView.edgesToParent(anchors: [.leading, .trailing, .bottom], insets: .margins(bottom: 71))
            $0.scrollView.topAnchor.equal(to: view.safeAreaLayoutGuide.topAnchor, constant: 71)
            
            $0.closeButton.apply {
                $0.contentEdgeInsets = UIEdgeInsets.margins(
                    top: Constants.Margin.small,
                    left: Constants.Margin.small,
                    bottom: Constants.Margin.small,
                    right: Constants.Margin.small)
                $0.topAnchor.equal(to: view.safeAreaLayoutGuide.topAnchor, constant: Constants.Margin.small)
                $0.trailingAnchor.equal(to: view.trailingAnchor, constant: -Constants.Margin.small)
            }
            
            $0.pageControl.centerXAnchor.equal(to: view.centerXAnchor)
            $0.pageControl.bottomAnchor.equal(to: view.bottomAnchor, constant: -42)
            
            $0.contentStack.edgesToParent(insets: .margins(left: Constants.Margin.standard, right: Constants.Margin.standard))
            $0.contentStack.heightAnchor.equal(to: $0.scrollView.heightAnchor)
            
            $0.imageViews.forEach { imageView in
                controller.contentStack.addArrangedSubview(imageView)
                imageView.widthAnchor.equal(to: controller.scrollView.widthAnchor, constant: -Constants.Margin.standard * 2)
            }
        }
    }

}

// MARK: - Public build methods
extension ImagesGalleryViewBuilder {
    
    func buildScrollView() -> UIScrollView {
        return UIScrollView().manualLayoutable().apply {
            $0.isPagingEnabled = true
        }
    }
    
    func buildPageControl() -> CHIPageControlAleppo {
        return CHIPageControlAleppo().manualLayoutable().apply {
            $0.hidesForSinglePage = false
            $0.radius = 2
            $0.padding = Constants.Margin.standard
        }
    }
    
    func buildCloseButton() -> UIButton {
        let button = UIButton().manualLayoutable()
        button.applyTouchAnimation()
        button.setImage(Icons.NavigationBar.close, for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        return button
    }
    
    func buildContentStack() -> UIStackView {
        return UIStackView(axis: .horizontal, with: [], spacing: Constants.Margin.standard * 2, alignment: .fill, distribution: .fillEqually).manualLayoutable()
    }
    
    func buildImageViews(count: Int) -> [UIImageView] {
        return (0..<count).map { _ in
            UIImageView()
                .apply { imageView in
                    imageView.contentMode = .scaleAspectFill
                    imageView.clipsToBounds = true
                }
        }
    }
    
}
