//
//  MyConnectionsViewController.swift
//  ParlorSocialClub
//
//  Created on 20/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

final class MyConnectionsViewController: AppViewController {

    // MARK: Properties

    var analyticEventScreen: AnalyticEventScreen? = .myConnections
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }

    override var observableSources: ObservableSources? {
        return viewModel
    }

    private var tableManager: TableViewManager!

    private let viewModel: MyConnectionsLogic = MyConnectionsViewModel()
    private let tableView: UITableView = UITableView().manualLayoutable()

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.loadView()
        setup()
        bindViewModel()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        adjustNavigationBar()
    }

    // MARK: Setup

    private func setup() {
        extendedLayoutIncludesOpaqueBars = true
        setupTableView()
    }

    private func setupTableView() {
        tableView.backgroundColor = .white
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorInset = Constants.MyConnectionsViewController.tableSeparatorInsets

        tableManager = TableViewManager(tableView: tableView, delegate: self)
        tableManager.setupRefreshControl()

        view.addSubview(tableView)
        tableView.edgesToParent()
    }

    private func adjustNavigationBar() {
        title = Strings.MyConnections.title.localized.uppercased()
        statusBar?.backgroundColor = .white
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.setShadowBarEnabled(true)
        navigationController?.applyWhiteTheme()
    }

    // MARK: Data Binding

    private func bindViewModel() {

        observableSources?.alertDataObs.show(using: self).disposed(by: disposeBag)
        observableSources?.errorObs.handleWithAlerts(using: self).disposed(by: disposeBag)
        observableSources?.progressBarObs.showProgressBar(using: self, isDark: true).disposed(by: disposeBag)

        viewModel.destinationsObs
            .bind { [unowned self] in self.navigate(using: $0) }
            .disposed(by: disposeBag)

        viewModel.paginatedSectionsObs
            .bind { [unowned self] in self.handle(paginatedSectionsState: $0) }
            .disposed(by: disposeBag)
    }

    private func handle(paginatedSectionsState state: SectionPaginatedRepositoryState) {
        switch state {
        case .loading: tableManager.showLoadingFooter()
        case .paging(let elements):
            tableManager.setupWith(sections: elements)
            tableManager.showLoadingFooter()
        case .populated(let elements):
            tableManager.invalidateCache()
            tableManager.setupWith(sections: elements)
            tableManager.hideLoadingFooter()
        case .error: tableManager.hideLoadingFooter()
        }
    }

    // MARK: - Actions

    private func navigate(using destination: MyConnectionsDestination) {
        switch destination {
        case .referFriend, .memberProfile:
            router.push(destination: destination)
        }
    }

}

// MARK: TableManagerDelegate

extension MyConnectionsViewController: TableManagerDelegate {
    func didSwipeForRefresh() {
        viewModel.refresh()
    }

    func loadMoreData() {
        viewModel.loadMore()
    }

    func tableViewDidScroll(_ scrollView: UIScrollView) {
        
    }
}

// MARK: Constants

private extension Constants {
    enum MyConnectionsViewController {
        static let tableSeparatorInsets = UIEdgeInsets(side: 16)
    }
}
