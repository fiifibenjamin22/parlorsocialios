//
//  MyConnectionsShareSection.swift
//  ParlorSocialClub
//
//  Created on 20/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

final class MyConnectionsShareSection: BasicTableSection<MyConnectionsShareViewCellModel, MyConnectionsShareTableViewCell> {
    static func make(for model: MyConnectionsShareViewCellModel) -> MyConnectionsShareSection {
        return MyConnectionsShareSection(items: [model])
    }
}
