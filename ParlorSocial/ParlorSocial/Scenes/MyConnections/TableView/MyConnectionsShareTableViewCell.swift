//
//  MyConnectionsShareTableViewCell.swift
//  ParlorSocialClub
//
//  Created on 20/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

struct MyConnectionsShareViewCellModel {
    let delegate: MyConnectionsShareDelegate?
}

protocol MyConnectionsShareDelegate: class {
    func syncContactsButtonTapped()
    func referFriendButtonTapped()
}

class MyConnectionsShareTableViewCell: UITableViewCell {

    typealias Model = MyConnectionsShareViewCellModel

    private weak var delegate: MyConnectionsShareDelegate?

    // MARK: Views

    private lazy var mainStack: UIStackView = UIStackView(
        axis: .vertical,
        with: [],
        spacing: Constants.MyConnectionsShareTableViewCell.spacing,
        alignment: .fill,
        distribution: .fill
    ).manualLayoutable()
    private lazy var titleLabel: ParlorLabel = ParlorLabel()
    private lazy var syncButton: ParlorButton = ParlorButton().manualLayoutable()
    private lazy var referButton: ParlorButton = ParlorButton().manualLayoutable()

    // MARK: Initialization

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initializeCell()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: Private Methods

    private func initializeCell() {
        setupViewHierarchy()
        setupAutolayout()
        setupProperties()
        setupAnalytics()
        setupActions()
    }

    private func setupViewHierarchy() {
        addSubview(mainStack)
        mainStack.addArrangedSubviews([titleLabel, syncButton, referButton])
    }

    private func setupAutolayout() {
        mainStack.edgesToParent(anchors: [.top, .bottom], padding: Constants.MyConnectionsShareTableViewCell.padding)
        mainStack.centerXAnchor.equal(to: centerXAnchor)
        mainStack.widthAnchor.equalTo(constant: Constants.MyConnectionsShareTableViewCell.width)
        syncButton.heightAnchor.equalTo(constant: Constants.standardButtonHeight)
        referButton.heightAnchor.equalTo(constant: Constants.standardButtonHeight)
    }

    private func setupProperties() {
        backgroundColor = .appBackground
        selectionStyle = .none

        titleLabel.font = UIFont.custom(ofSize: Constants.FontSize.hintSize, font: UIFont.Roboto.regular)
        titleLabel.textColor = .appGreyLight
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 0
        titleLabel.text = Strings.MyConnections.shareTitle.localized

        syncButton.style = .filledWith(
            color: .appYellow,
            titleColor: .appTextMediumBright,
            font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium),
            letterSpacing: Constants.LetterSpacing.none
        )
        syncButton.layer.cornerRadius = Constants.MyConnectionsShareTableViewCell.buttonCornerRadius
        syncButton.title = Strings.MyConnections.shareSyncButton.localized.uppercased()
        syncButton.applyTouchAnimation()

        referButton.style = .filledWith(
            color: .appPrimary,
            titleColor: .appTextMediumBright,
            font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium),
            letterSpacing: Constants.LetterSpacing.none
        )
        referButton.layer.cornerRadius = Constants.MyConnectionsShareTableViewCell.buttonCornerRadius
        referButton.title = Strings.MyConnections.shareReferButton.localized.uppercased()
        referButton.applyTouchAnimation()
    }

    private func setupAnalytics() {
        syncButton.trackingId = AnalyticEventClickableElementName.MyConnections.sync.rawValue
        referButton.trackingId = AnalyticEventClickableElementName.MyConnections.refer.rawValue
    }

    private func setupActions() {
        syncButton.addTarget(self, action: #selector(didTapSyncButton(_:)), for: .touchUpInside)
        referButton.addTarget(self, action: #selector(didTapReferButton(_:)), for: .touchUpInside)
    }

    // MARK: Actions

    @objc func didTapSyncButton(_ sender: UIButton) {
        delegate?.syncContactsButtonTapped()
    }

    @objc func didTapReferButton(_ sender: UIButton) {
        delegate?.referFriendButtonTapped()
    }

}

// MARK: ConfigurableCell

extension MyConnectionsShareTableViewCell: ConfigurableCell {
    func configure(with model: MyConnectionsShareViewCellModel) {
        delegate = model.delegate
    }
}

// MARK: Constants

private extension Constants {
    enum MyConnectionsShareTableViewCell {
        static let spacing: CGFloat = 20
        static let padding: CGFloat = 20
        static let width: CGFloat = 200
        static let buttonHeignt: CGFloat = 40
        static let buttonCornerRadius: CGFloat = 20
    }
}
