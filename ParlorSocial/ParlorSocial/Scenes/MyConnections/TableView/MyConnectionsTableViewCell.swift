//
//  MyConnectionsTableViewCell.swift
//  ParlorSocialClub
//
//  Created on 20/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

class MyConnectionsTableViewCell: UITableViewCell {

    typealias Model = MyConnection

    // MARK: Views

    private lazy var mainStack: UIStackView = UIStackView(
        axis: .horizontal,
        with: [],
        spacing: Constants.MyConnectionsTableViewCell.spacing,
        alignment: .center,
        distribution: .fill
    ).manualLayoutable()
    
    private lazy var mainImageView: UIImageView = UIImageView().apply {
        $0.setContentHuggingPriority(.required, for: .horizontal)
        $0.setContentCompressionResistancePriority(.required, for: .horizontal)
    }.manualLayoutable()
    
    private lazy var contentStack: UIStackView = UIStackView(
        axis: .vertical,
        with: [],
        spacing: Constants.MyConnectionsTableViewCell.contentSpacing,
        alignment: .leading,
        distribution: .fill
    )
    
    private lazy var titleLabel: ParlorLabel = ParlorLabel()
    
    private lazy var descriptionStack: UIStackView = UIStackView(
        axis: .vertical,
        with: [],
        spacing: Constants.MyConnectionsTableViewCell.descriptionSpacing,
        alignment: .leading,
        distribution: .fill
    )
    
    private var descriptionLabels: [ParlorLabel] = [] {
        didSet {
            descriptionStack.removeArrangedSubviews()
            descriptionStack.addArrangedSubviews(descriptionLabels)
        }
    }

    // MARK: Initialization

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: Private Methods

    private func initialize() {
        setupViewHierarchy()
        setupAutolayout()
        setupProperties()
    }

    private func setupViewHierarchy() {
        addSubview(mainStack)
        mainStack.addArrangedSubviews([mainImageView, contentStack])
        contentStack.addArrangedSubviews([titleLabel, descriptionStack])
    }

    private func setupAutolayout() {
        mainStack.edgesToParent(insets: Constants.MyConnectionsTableViewCell.insets)
        mainImageView.widthAnchor.equalTo(constant: Constants.MyConnectionsTableViewCell.imageSize)
        mainImageView.heightAnchor.equalTo(constant: Constants.MyConnectionsTableViewCell.imageSize)
    }

    private func setupProperties() {
        selectionStyle = .none

        mainImageView.backgroundColor = .appBackground
        mainImageView.clipsToBounds = true
        mainImageView.contentMode = .scaleAspectFill
        mainImageView.layer.cornerRadius = Constants.MyConnectionsTableViewCell.imageCornerRadius

        titleLabel.font = UIFont.custom(ofSize: Constants.FontSize.hintSize, font: UIFont.Roboto.medium)
        titleLabel.textColor = .appTextMediumBright
    }

    private func createDescriptionLabel(with text: String?) -> ParlorLabel? {
        guard let text = text else { return nil }
        return ParlorLabel().apply {
            $0.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.regular)
            $0.textColor = .appTextMediumBright
            $0.text = text
        }
    }

}

// MARK: ConfigurableCell

extension MyConnectionsTableViewCell: ConfigurableCell {
    func configure(with model: MyConnection) {
        mainImageView.getImage(from: model.imageURL)
        titleLabel.text = model.name
        descriptionLabels = [model.position, model.company].compactMap { createDescriptionLabel(with: $0) }
    }
}

// MARK: Constants

private extension Constants {
    enum MyConnectionsTableViewCell {
        static let insets: UIEdgeInsets = UIEdgeInsets(top: 18, bottom: 18, horizontal: 16)
        static let spacing: CGFloat = 13
        static let contentSpacing: CGFloat = 5
        static let descriptionSpacing: CGFloat = 2
        static let imageSize: CGFloat = 60
        static let imageCornerRadius: CGFloat = 30
    }
}
