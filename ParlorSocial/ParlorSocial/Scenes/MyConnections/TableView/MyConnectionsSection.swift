//
//  MyConnectionsSection.swift
//  ParlorSocialClub
//
//  Created on 20/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

final class MyConnectionsSection: BasicTableSection<MyConnection, MyConnectionsTableViewCell> {
    static func make(
        for connections: [MyConnection],
        itemSelector: @escaping (MyConnection) -> Void
    ) -> MyConnectionsSection {
        return MyConnectionsSection(items: connections, itemSelector: itemSelector)
    }
}
