//
//  MyConnectionsViewModel.swift
//  ParlorSocialClub
//
//  Created on 20/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import RxSwift
import RxCocoa

protocol MyConnectionsLogic: BaseViewModelLogic {
    var destinationsObs: Observable<MyConnectionsDestination> { get }
    var paginatedSectionsObs: Observable<SectionPaginatedRepositoryState> { get }
    func refresh()
    func loadMore()
}

final class MyConnectionsViewModel: BaseViewModel {

    typealias ObservableApiResponse = Observable<ApiResponse<MyConnections.Get.Response>>

    var analyticEventReferencedId: Int?

    private var myConnectionsRequestModel: MyConnections.Get.Request = MyConnections.Get.Request.initial

    private let connectionsRepository: ConnectionsRepositoryProtocol = ConnectionsRepository.shared
    private let destinationRelay: PublishRelay<MyConnectionsDestination> = PublishRelay()
    private let loadDataRelay = BehaviorRelay(value: 0)
    private let paginatedDataRelay = BehaviorRelay<PaginatedData>(value: PaginatedData(sections: [], metadata: ListMetadata.initial))
    private let paginatedSectionsRelay = BehaviorRelay<SectionPaginatedRepositoryState>(value: .loading)

    override init() {
        super.init()
        self.initFlow()
    }

    private func initFlow() {
        bindLoadData()
    }

    private func bindLoadData() {
        loadDataRelay
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .userInteractive))
            .setLoadingStateOnSubscribe(with: paginatedSectionsRelay)
            .skipOnListPopulated(with: paginatedDataRelay)
            .flatMapLatest { [unowned self] _ -> ObservableApiResponse in
                return self.connectionsRepository.getMyConnections(
                    using: self.myConnectionsRequestModel
                )
            }
            .mapResponseToTableSection(
                with: paginatedDataRelay,
                sectionStateRelay: paginatedSectionsRelay,
                sectionMaker: { [unowned self] in self.makeSections(for: $0) }
            )
            .changePaginatedState(with: paginatedSectionsRelay)
            .doOnNext { [unowned self] _ in self.myConnectionsRequestModel.increasePage() }
            .bind(to: paginatedDataRelay)
            .disposed(by: disposeBag)
    }

    private func makeSections(for myConnections: [MyConnection]) -> [TableSection] {
        return [
            MyConnectionsShareSection.make(
                for: MyConnectionsShareViewCellModel(delegate: self)
            ),
            MyConnectionsSection.make(
                for: myConnections,
                itemSelector: { [unowned self] in
                    self.destinationRelay.accept(.memberProfile(id: $0.memberId))
                }
            )
        ]
    }

}

extension MyConnectionsViewModel: MyConnectionsLogic {
    var destinationsObs: Observable<MyConnectionsDestination> {
        return destinationRelay.asObservable()
    }

    var paginatedSectionsObs: Observable<SectionPaginatedRepositoryState> {
        return paginatedSectionsRelay.asObservable()
    }

    func refresh() {
        paginatedDataRelay.accept(PaginatedData(sections: [], metadata: ListMetadata.initial))
        myConnectionsRequestModel.set(page: 1)
        loadMore()
    }

    func loadMore() {
        loadDataRelay.accept(0)
    }
}

extension MyConnectionsViewModel: MyConnectionsShareDelegate {
    func syncContactsButtonTapped() {
        // TODO: navigate to sync contact
    }

    func referFriendButtonTapped() {
        destinationRelay.accept(.referFriend)
    }
}
