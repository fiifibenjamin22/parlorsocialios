//
//  MyConnectionsDestination.swift
//  ParlorSocialClub
//
//  Created on 20/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

enum MyConnectionsDestination {
    case referFriend
    case memberProfile(id: Int)
}

extension MyConnectionsDestination: Destination {
    var viewController: UIViewController {
        switch self {
        case .referFriend:
            return ReferFriendViewController()
        case .memberProfile:
            let viewController = MemberProfileViewController()
            viewController.viewModel = MemberProfileViewModel(member: .mock)

            return viewController
        }
    }
}
