//
//  ActivationDetailsInfoViewModel.swift
//  ParlorSocialClub
//
//  Created on 12/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

protocol ActivationDetailsInfoLogic: BaseViewModelLogic {
    var destinationsObs: Observable<ActivationDetailsInfoDestination> { get }
    var sectionsObs: Observable<SectionPaginatedRepositoryState> { get }
    func refresh()
    func loadMore()
}

class ActivationDetailsInfoViewModel: BaseViewModel {

    var analyticEventReferencedId: Int?

    private let activationId: Int
    private var activationUpdatesRequestModel = ActivationUpdates.Get.Request.initialAll

    private let activationRepository = ActivationsRepository.shared
    private let guestListRepository = GuestListRsvpRepository.shared

    private let destinationRelay: PublishRelay<ActivationDetailsInfoDestination> = PublishRelay()
    private let activationRelay: BehaviorRelay<Activation?> = BehaviorRelay(value: nil)
    private let activationInfoRelay: BehaviorRelay<ActivationInfo?> = BehaviorRelay(value: nil)
    private let activationUpdatesRelay: BehaviorRelay<ActivationUpdate?> = BehaviorRelay(value: nil)

    private let loadDataRelay = BehaviorRelay(value: 0)
    private let paginatedDataRelay = BehaviorRelay<PaginatedData>(value: PaginatedData(sections: [], metadata: ListMetadata.initial))
    private let paginatedSectionsRelay = BehaviorRelay<SectionPaginatedRepositoryState>(value: .loading)

    lazy var linkHandler: TextViewLinkHandler = {
        DefaultTextViewLinkHandler()
    }()

    private lazy var activityViewControllerCompletion: UIActivityViewController.CompletionWithItemsHandler = {
        activityType, completed, returnedItems, error in
        guard completed, let type = activityType else { return }
        AnalyticService.shared.saveNativeAnalyticShareActivation(with: type)
    }

    lazy var checkInService: CheckInService = {
        let checkInService = CheckInService(
            locationService: LocationService(),
            guestListRepository: guestListRepository,
            activationRepository: activationRepository
        )
        checkInService.delegate = self
        return checkInService
    }()

    init(activationId: Int) {
        self.activationId = activationId
        super.init()
        self.initFlow()
    }

    private func initFlow() {
        activationInfoRelay
            .ignoreNil()
            .bind { [weak self] in self?.bindLoadData(with: $0) }
            .disposed(by: disposeBag)
        fetchActivationData()
    }

    private func navigateToActivationDetails() {
        guard let activation = activationRelay.value else { return }
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.ActivationDetailsInfo.activationName.rawValue)
        let openingType = ActivationDetailsNavigationProvider.getOpeningActivationDetailsType(with: activation)
        switch openingType {
        case .activationDetailsVC, .addProfilePhotoVC, .upgradeToPremiumForStandard:
            self.destinationRelay.accept(.activationDetails(activation: activation, openingType: openingType, navigationSource: .parlorPassDetails))
        case .startedAlert:
            self.alertDataSubject.onNext(
                AlertData.fromSimpleMessage(message: Strings.Activation.startedAlert.localized, sourceName: AnalyticEventAlertSourceName.activationHasAlreadyStarted)
            )
        }
    }

    private func navigateToGuestList() {
        guard let activation = activationRelay.value else { return }
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.ActivationDetailsInfo.guestList.rawValue)
        self.checkInService.navigateToGuestList(activationId: activationId, checkInStrategy: activation.checkInStrategy)
    }

    private func navigateToLink(_ url: URL?) {
        guard let url = url else { return }
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.ActivationDetailsInfo.linkJoin.rawValue)
        linkHandler.openUrl(url)
    }

    private func shareLink(_ url: URL?) {
        guard let url = url else { return }
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.ActivationDetailsInfo.linkShare.rawValue)
        let shareData = ShareData(url: url.absoluteString, message: "", emailTitle: "")
        self.destinationRelay.accept(.share(shareData: shareData, delegate: self))
    }

}

// MARK: ActivationDetailsInfoLogic

extension ActivationDetailsInfoViewModel: ActivationDetailsInfoLogic {

    var destinationsObs: Observable<ActivationDetailsInfoDestination> {
        return destinationRelay.asObservable()
    }

    var sectionsObs: Observable<SectionPaginatedRepositoryState> {
        return paginatedSectionsRelay.asObservable()
    }

    func refresh() {
        paginatedDataRelay.accept(PaginatedData(sections: [], metadata: ListMetadata.initial))
        activationUpdatesRequestModel.set(page: 1)
        loadMore()
    }

    func loadMore() {
        loadDataRelay.accept(0)
    }

}

// MARK: Fetch Data

private extension ActivationDetailsInfoViewModel {

    func fetchActivationData() {
        Observable.combineLatest(
            activationRepository.getActivation(withId: activationId),
            activationRepository.getActivationInfo(forActivationWithId: activationId)
        )
            .showingProgressBar(with: self)
            .subscribe(onNext: { [weak self] in
                self?.handleActivationResponse($0.0)
                self?.handleActivationInfoResponse($0.1)
            })
            .disposed(by: disposeBag)
    }

    private func handleActivationResponse(_ response: SingleActivationResposne) {
        switch response {
        case .success(let successResponse):
            activationRelay.accept(successResponse.data)
        case .failure(let error):
            errorSubject.onNext(error)
        }
    }

    private func handleActivationInfoResponse(_ response: ActivationInfoResposne) {
        switch response {
        case .success(let successResponse):
            activationInfoRelay.accept(successResponse.data)
        case .failure(let error):
            errorSubject.onNext(error)
        }
    }

}

// MARK: Bind Data

private extension ActivationDetailsInfoViewModel {

    typealias ObservableApiResponse = Observable<ApiResponse<ActivationUpdates.Get.Response>>

    private func bindLoadData(with activationInfo: ActivationInfo) {
        loadDataRelay
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .userInteractive))
            .setLoadingStateOnSubscribe(with: paginatedSectionsRelay)
            .skipOnListPopulated(with: paginatedDataRelay)
            .flatMapLatest { [unowned self] _ -> ObservableApiResponse in
                return self.activationRepository.getActivationUpdates(
                    forActivationWithId: self.activationId,
                    using: self.activationUpdatesRequestModel
                )
            }
            .mapResponseToTableSection(
                with: paginatedDataRelay,
                sectionStateRelay: paginatedSectionsRelay,
                sectionMaker: { [unowned self] in self.makeSections(for: $0, with: activationInfo) }
            )
            .changePaginatedState(with: paginatedSectionsRelay)
            .doOnNext { [unowned self] _ in self.activationUpdatesRequestModel.increasePage() }
            .bind(to: paginatedDataRelay)
            .disposed(by: disposeBag)
    }

}

// MARK: Sections

private extension ActivationDetailsInfoViewModel {

    func makeSections(for activationUpdates: [ActivationUpdate], with activationInfo: ActivationInfo) -> [TableSection] {
        let sections: [TableSection?] = [
            ImageHeaderSection.make(for: activationInfo),
            SeparatorSection.makeBlank(),
            InfoSection.makeTitle(
                for: activationInfo,
                itemSelector: { [unowned self] _ in self.navigateToActivationDetails() }
            ),
            activationInfo.links.isEmpty ? nil : SeparatorSection.makeLine(),
            activationInfo.links.isEmpty ? nil : LinkSection.make(
                for: activationInfo,
                selectCallback: { [unowned self] in self.navigateToLink($0.url) },
                shareCallback: { [unowned self] in self.shareLink($0.url) }
            ),
            SeparatorSection.makeLine(),
            InfoSection.makeGuestList(
                for: activationInfo,
                itemSelector: { [unowned self] _ in self.navigateToGuestList() }
            ),
            activationUpdates.isEmpty ? nil : SeparatorSection.makeLine(),
            activationUpdates.isEmpty ? nil : UpdateSection(items: activationUpdates)
        ]
        return sections.compactMap { $0 }
    }

}

// MARK: CheckInService

extension ActivationDetailsInfoViewModel: ShareActivationDelegate {
    func presentMoreShareOptions(with data: ShareData) {
        destinationRelay.accept(.moreShareOptions(shareData: data, completion: activityViewControllerCompletion))
    }
}

// MARK: CheckInService

extension ActivationDetailsInfoViewModel: CheckInServiceDelegate {

    func didEndCheckInSuccess(_ checkInService: CheckInService, activationId: Int) {
        progressBarSubject.onNext(false)
        destinationRelay.accept(.guestList(activationId: activationId))
    }

}
