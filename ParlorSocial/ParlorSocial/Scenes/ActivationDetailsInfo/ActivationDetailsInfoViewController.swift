//
//  ActivationDetailsInfoViewController.swift
//  ParlorSocialClub
//
//  Created on 12/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

class ActivationDetailsInfoViewController: AppViewController {

    // MARK: - Properties

    let analyticEventScreen: AnalyticEventScreen? = .activationDetailsInfo
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>?

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override var observableSources: ObservableSources? {
        return viewModel
    }

    private var viewModel: ActivationDetailsInfoLogic!

    // MARK: - Views

    private(set) var tableView: UITableView!
    private var tableManager: TableViewManager!

    // MARK: - Initialization

    init(activation: Activation) {
        super.init(nibName: nil, bundle: nil)
        initialize(with: activation)
    }

    init(activationId: Int) {
        super.init(nibName: nil, bundle: nil)
        initialize(forActivationId: activationId)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }

    override func observeViewModel() {
        super.observeViewModel()

        observableSources?.alertDataObs.show(using: self).disposed(by: disposeBag)
        observableSources?.errorObs.handleWithAlerts(using: self).disposed(by: disposeBag)
        observableSources?.progressBarObs.showProgressBar(using: self, isDark: true).disposed(by: disposeBag)

        viewModel.destinationsObs
            .bind { [unowned self] destination in self.handle(destination: destination) }
            .disposed(by: disposeBag)

        viewModel.sectionsObs
            .bind { [unowned self] state in self.handle(state: state) }
            .disposed(by: disposeBag)
    }

    private func handle(state: SectionPaginatedRepositoryState) {
        tableManager.invalidateCache()
        switch state {
        case .loading:
            tableManager.showLoadingFooter()
        case .paging(let elements):
            tableManager.setupWith(sections: elements)
            tableManager.showLoadingFooter()
        case .populated(let elements):
            tableManager.setupWith(sections: elements)
            tableManager.hideLoadingFooter()
        case .error: tableManager.hideLoadingFooter()
        }
    }

    // MARK: - Lifecycle

    override func loadView() {
        super.loadView()
        setupViews()
        setupTableManager()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        adjustNavigationBar()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        changeNavigationBarAlphaWithAnimation()
    }

}

// MARK: - Private methods

extension ActivationDetailsInfoViewController {

    private func initialize(with activation: Activation) {
        self.viewModel = ActivationDetailsInfoViewModel(activationId: activation.id)
    }

    private func initialize(forActivationId activationId: Int) {
        self.viewModel = ActivationDetailsInfoViewModel(activationId: activationId)
    }

    private func adjustNavigationBar() {
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.applyTransparentTheme(withColor: .white)
        statusBar?.backgroundColor = .clear
        setNeedsStatusBarAppearanceUpdate()
        guard self.presentingViewController != nil else { return }
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: Icons.NavigationBar.close, style: .plain, target: self, action: #selector(tapCloseButton))
    }

    private func setupViews() {
        let builder = ActivationDetailsInfoViewBuilder(controller: self)
        tableView = builder.buildTableView()
        builder.setupViews()
    }

    private func setupTableManager() {
        tableManager = TableViewManager(tableView: tableView, delegate: self)
        tableManager.setupRefreshControl()
    }

    private func handle(destination: ActivationDetailsInfoDestination) {
        switch destination {
        case .activationDetails:
            router.push(destination: destination)
        case .guestList:
            router.present(destination: destination, animated: true, withStyle: .fullScreen)
        case .share, .moreShareOptions:
            router.present(destination: destination, animated: true, withStyle: .overCurrentContext)
        }
    }

    private func changeNavigationBarAlphaWithAnimation() {
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.tableViewDidScroll(self.tableView)
        }
    }

    @objc func tapCloseButton() {
        parent?.dismiss()
    }

}

// MARK: - Table Manager Delegate

extension ActivationDetailsInfoViewController: TableManagerDelegate {

    func didSwipeForRefresh() {
        viewModel.refresh()
    }

    func loadMoreData() {
        viewModel.loadMore()
    }

    func tableViewDidScroll(_ scrollView: UIScrollView) {
        guard let firstCell = tableView.cellForRow(at: IndexPath(item: 0, section: 0)) else { return }
        scrollView.changeNavigationBarAlpha(using: firstCell, to: self.navigationController?.navigationBar)
    }

}
