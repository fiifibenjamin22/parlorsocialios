//
//  UpdateSectionHeader.swift
//  ParlorSocialClub
//
//  Created on 19/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

class UpdateSectionHeader: UITableViewHeaderFooterView {

    lazy var label: ParlorLabel = ParlorLabel.styled(
        withFont: UIFont.custom(ofSize: Constants.FontSize.body, font: UIFont.Roboto.regular),
        alignment: .left
    ).apply {
        $0.insets = Constants.UpdateSectionHeader.insets
    }

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        initializeHeader()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func initializeHeader() {
        contentView.backgroundColor = .appBackground
        contentView.addSubview(label)
        label.edgesToParent()
        label.text = Strings.ActivationDetailsInfo.updates.localized
    }

}

fileprivate extension Constants {

    enum UpdateSectionHeader {
        static let insets: UIEdgeInsets = UIEdgeInsets(top: 20, bottom: 16, horizontal: 32)
    }

}
