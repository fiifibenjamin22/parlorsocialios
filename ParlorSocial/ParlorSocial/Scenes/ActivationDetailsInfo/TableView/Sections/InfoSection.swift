//
//  InfoSection.swift
//  ParlorSocialClub
//
//  Created on 19/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class InfoSection: BasicTableSection<InfoTableViewCellModel, InfoTableViewCell> {

    static func makeTitle(for activationInfo: ActivationInfo, itemSelector: @escaping (InfoTableViewCellModel) -> Void) -> InfoSection {
        let model = InfoTableViewCellModel(
            type: .title,
            text: activationInfo.name
        )
        return InfoSection(items: [model], itemSelector: itemSelector)
    }

    static func makeGuestList(for activationInfo: ActivationInfo, itemSelector: @escaping (InfoTableViewCellModel) -> Void) -> InfoSection {
        let isGuestlistAvailable = activationInfo.guestListAccess?.guestlistState == .available
        let model = InfoTableViewCellModel(
            type: isGuestlistAvailable ? .guestListActive : .guestListInactive,
            text: Strings.ActivationDetailsInfo.guestList.localized
        )
        return InfoSection(
            items: [model],
            itemSelector: isGuestlistAvailable ? itemSelector : { _ in }
        )
    }

}
