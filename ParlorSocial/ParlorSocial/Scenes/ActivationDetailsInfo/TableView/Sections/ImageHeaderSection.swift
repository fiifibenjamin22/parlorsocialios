//
//  ImageHeaderSection.swift
//  ParlorSocialClub
//
//  Created on 19/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class ImageHeaderSection: BasicTableSection<ImageHeaderTableViewCellModel, ImageHeaderTableViewCell> {

    static func make(for activationInfo: ActivationInfo) -> ImageHeaderSection {
        let model = ImageHeaderTableViewCellModel(
            title: activationInfo.timeSegment.startAt.toUpcomingRsvpsDate(),
            subtitle: activationInfo.timeSegment.startAt.toString(withFormat: Date.Format.activationDetails).uppercased(),
            imageUrl: URL(string: activationInfo.imageUrl)!
        )
        return ImageHeaderSection(items: [model])
    }

}
