//
//  SeparatorSection.swift
//  ParlorSocialClub
//
//  Created on 19/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class SeparatorSection: BasicTableSection<SeparatorTableViewCellModel, SeparatorTableViewCell> {

    static func makeBlank() -> SeparatorSection {
        let model = SeparatorTableViewCellModel(
            isLineHidden: true
        )
        return SeparatorSection(items: [model])
    }

    static func makeLine() -> SeparatorSection {
        let model = SeparatorTableViewCellModel(
            isLineHidden: false
        )
        return SeparatorSection(items: [model])
    }

}
