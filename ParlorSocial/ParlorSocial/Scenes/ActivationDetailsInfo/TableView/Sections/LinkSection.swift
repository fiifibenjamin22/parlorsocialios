//
//  LinkSection.swift
//  ParlorSocialClub
//
//  Created on 29/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class LinkSection: BasicTableSection<LinkTableViewCellModel, LinkTableViewCell> {

    init(items: [LinkTableViewCellModel]) {
        super.init(items: items, itemSelector: { _ in })
    }

    static func make(for activationInfo: ActivationInfo, selectCallback: @escaping (_ model: LinkTableViewCellModel) -> Void, shareCallback: @escaping (_ model: LinkTableViewCellModel) -> Void) -> LinkSection {
        let model = activationInfo.links.map { activationInfoLink in
            LinkTableViewCellModel(
                text: activationInfoLink.name,
                url: URL(string: activationInfoLink.url),
                selectCallback: selectCallback,
                shareCallback: shareCallback
            )
        }
        return LinkSection(items: model)
    }

}
