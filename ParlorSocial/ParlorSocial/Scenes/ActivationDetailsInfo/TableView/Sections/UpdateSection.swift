//
//  UpdateSection.swift
//  ParlorSocialClub
//
//  Created on 19/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class UpdateSection: BasicTableSection<ActivationUpdate, UpdateTableViewCell> {

    override var headerType: UITableViewHeaderFooterView.Type? { return UpdateSectionHeader.self }

}
