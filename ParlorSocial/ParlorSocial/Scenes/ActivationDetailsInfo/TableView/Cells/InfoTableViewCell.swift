//
//  InfoTableViewCell.swift
//  ParlorSocialClub
//
//  Created on 15/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

struct InfoTableViewCellModel {
    let type: InfoTableViewCellType
    let text: String
}

final class InfoTableViewCell: UITableViewCell {

    typealias Model = InfoTableViewCellModel

    private lazy var stackView: UIStackView = UIStackView(
        axis: .horizontal,
        with: [],
        spacing: 16,
        alignment: .center,
        distribution: .fill
    )

    private lazy var titleLabel: UILabel = ParlorLabel.styled(
        withTextColor: UIColor.appTextBright,
        withFont: UIFont.custom(ofSize: Constants.FontSize.body, font: UIFont.Roboto.regular),
        alignment: .left,
        numberOfLines: 0
    )

    private lazy var additionalLabel: UILabel = ParlorLabel.styled(
        withTextColor: UIColor.appTextBright,
        withFont: UIFont.custom(ofSize: Constants.FontSize.xxTiny, font: UIFont.Roboto.regular),
        alignment: .right,
        numberOfLines: 0
    ).apply {
        $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        $0.setContentHuggingPriority(.required, for: .horizontal)
    }

    private lazy var leftIcon: UIImageView = UIImageView().apply {
        $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        $0.setContentHuggingPriority(.required, for: .horizontal)
    }

    private lazy var rightIcon: UIImageView = UIImageView().apply {
        $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        $0.setContentHuggingPriority(.required, for: .horizontal)
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initializeCell()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func initializeCell() {
        setupViewHierarchy()
        setupAutolayout()
        setupProperties()
    }

    private func setupViewHierarchy() {
        contentView.addSubview(stackView)
        stackView.addArrangedSubviews([leftIcon, titleLabel, additionalLabel, rightIcon])
    }

    private func setupAutolayout() {
        stackView.edgesToParent(insets: Constants.InfoTableViewCell.insets)
    }

    private func setupProperties() {
        backgroundColor = .clear
        selectedBackgroundView = nil
        selectionStyle = .none
    }

}

extension InfoTableViewCell: ConfigurableCell {

    func configure(with model: InfoTableViewCellModel) {
        titleLabel.text = model.text
        additionalLabel.text = model.type.additionalText
        leftIcon.image = model.type.leftIcon
        rightIcon.image = model.type.rightIcon
        additionalLabel.isHidden = model.type.additionalText == nil
        leftIcon.isHidden = model.type.leftIcon == nil
        rightIcon.isHidden = model.type.rightIcon == nil
    }

}

fileprivate extension Constants {

    enum InfoTableViewCell {
        static let insets: UIEdgeInsets = UIEdgeInsets(top: 16, bottom: 16, horizontal: 28)
    }

}
