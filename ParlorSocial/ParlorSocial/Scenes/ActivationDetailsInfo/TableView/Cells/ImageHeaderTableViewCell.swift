//
//  ImageHeaderTableViewCell.swift
//  ParlorSocialClub
//
//  Created on 15/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

struct ImageHeaderTableViewCellModel {
    let title: String
    let subtitle: String
    let imageUrl: URL?
}

final class ImageHeaderTableViewCell: UITableViewCell {

    typealias Model = ImageHeaderTableViewCellModel

    private lazy var backgroundImageView: UIImageView = UIImageView().manualLayoutable().apply {
        $0.contentMode = .scaleAspectFill
        $0.clipsToBounds = true
    }

    private lazy var backgroundImageViewOverlay: UIView =  UIView().manualLayoutable().apply {
        $0.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }

    private lazy var stackView: UIStackView = UIStackView(
        axis: .vertical,
        spacing: 10,
        alignment: .center,
        distribution: .fill
    ).manualLayoutable()

    private lazy var titleLabel: UILabel = UILabel.styled(
        textColor: .white,
        withFont: UIFont.custom(ofSize: Constants.FontSize.largeDate, font: UIFont.Editor.bold),
        alignment: .center,
        numberOfLines: 1
    )

    private lazy var subtitleLabel: UILabel = UILabel.styled(
        textColor: .white,
        withFont: UIFont.custom(ofSize: Constants.FontSize.hintSize, font: UIFont.Roboto.bold),
        alignment: .center
    )

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initializeCell()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func initializeCell() {
        setupViewHierarchy()
        setupAutolayout()
        setupProperties()
    }

    private func setupViewHierarchy() {
        contentView.addSubview(backgroundImageView)
        contentView.addSubview(backgroundImageViewOverlay)
        contentView.addSubview(stackView)
        stackView.addArrangedSubviews([titleLabel, subtitleLabel])
    }

    private func setupAutolayout() {
        backgroundImageView.edgesToParent()
        backgroundImageViewOverlay.edgesToParent()
        stackView.apply {
            $0.leadingAnchor.equal(to: contentView.leadingAnchor, constant: 20)
            $0.trailingAnchor.equal(to: contentView.trailingAnchor, constant: -20)
            $0.centerYAnchor.equal(to: contentView.centerYAnchor)
        }

        contentView.heightAnchor.constraint(
            equalTo: contentView.widthAnchor,
            multiplier: Constants.ImageHeaderTableViewCell.ratio
        ).activate()
    }

    private func setupProperties() {
        backgroundColor = .clear
        isUserInteractionEnabled = false
    }

}

extension ImageHeaderTableViewCell: ConfigurableCell {

    func configure(with model: ImageHeaderTableViewCellModel) {
        titleLabel.text = model.title
        subtitleLabel.text = model.subtitle
        if let imageUrl = model.imageUrl {
            backgroundImageView.getImage(from: imageUrl)
        }
    }

}

fileprivate extension Constants {

    enum ImageHeaderTableViewCell {
        static let ratio: CGFloat = 460.0 / 375.0
    }

}
