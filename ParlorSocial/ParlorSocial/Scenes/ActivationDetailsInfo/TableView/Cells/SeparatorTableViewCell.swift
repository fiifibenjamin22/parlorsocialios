//
//  SeparatorTableViewCell.swift
//  ParlorSocialClub
//
//  Created on 15/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

struct SeparatorTableViewCellModel {
    let isLineHidden: Bool
}

final class SeparatorTableViewCell: UITableViewCell {

    typealias Model = SeparatorTableViewCellModel

    private lazy var lineView: UIView = UIView().manualLayoutable().apply {
        $0.backgroundColor = UIColor.lightBackgroundSeperator.withAlphaComponent(0.44)
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initializeCell()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func initializeCell() {
        setupViewHierarchy()
        setupAutolayout()
        setupProperties()
    }

    private func setupViewHierarchy() {
        contentView.addSubview(lineView)
    }

    private func setupAutolayout() {
        lineView.heightAnchor.equalTo(constant: 1)
        lineView.edgesToParent(insets: Constants.SeparatorTableViewCell.insets)
    }

    private func setupProperties() {
        backgroundColor = .clear
        isUserInteractionEnabled = false
    }

}

extension SeparatorTableViewCell: ConfigurableCell {

    func configure(with model: SeparatorTableViewCellModel) {
        lineView.isHidden = model.isLineHidden
    }

}

fileprivate extension Constants {

    enum SeparatorTableViewCell {
        static let insets: UIEdgeInsets = UIEdgeInsets(top: 8, bottom: 8, horizontal: 28)
    }

}
