//
//  InfoTableViewCellType.swift
//  ParlorSocialClub
//
//  Created on 17/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

enum InfoTableViewCellType {

    case title
    case guestListActive
    case guestListInactive

    var additionalText: String? {
        switch self {
        case .title, .guestListActive:
            return nil
        case .guestListInactive:
            return Strings.ActivationDetailsInfo.comingSoon.localized
        }
    }

    var leftIcon: UIImage? {
        switch self {
        case .title:
            return Icons.ActivationDetailsInfo.event
        case .guestListActive, .guestListInactive:
            return Icons.ActivationDetailsInfo.guestList
        }
    }

    var rightIcon: UIImage? {
        switch self {
        case .title, .guestListActive:
            return Icons.ActivationDetailsInfo.rightArrow
        case .guestListInactive:
            return nil
        }
    }

}
