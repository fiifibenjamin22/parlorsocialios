//
//  LinkTableViewCell.swift
//  ParlorSocialClub
//
//  Created on 29/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

struct LinkTableViewCellModel {
    let text: String
    let url: URL?
    let selectCallback: (_ model: Self) -> Void
    let shareCallback: (_ model: Self) -> Void
}

final class LinkTableViewCell: UITableViewCell {

    typealias Model = LinkTableViewCellModel

    private let disposeBag = DisposeBag()

    private var selectCallback: (() -> Void)?
    private var shareCallback: (() -> Void)?

    private lazy var stackView: UIStackView = UIStackView(
        axis: .horizontal,
        with: [],
        spacing: 16,
        alignment: .center,
        distribution: .fill
    )

    private lazy var titleLabel: UILabel = ParlorLabel.styled(
        withTextColor: UIColor.appTextBright,
        withFont: UIFont.custom(ofSize: Constants.FontSize.body, font: UIFont.Roboto.regular),
        alignment: .left,
        numberOfLines: 0
    )

    private lazy var leftIcon: UIImageView = UIImageView().apply {
        $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        $0.setContentHuggingPriority(.required, for: .horizontal)
    }

    private lazy var selectButton: ParlorButton = ParlorButton(style: .filledWith(
        color: .appGreenDark,
        titleColor: .appText,
        font: UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.regular),
        letterSpacing: Constants.LetterSpacing.none
    )).apply {
        $0.layer.cornerRadius = Constants.LinkTableViewCell.selectButtonCornerRadius
        $0.contentEdgeInsets = Constants.LinkTableViewCell.selectButtonInsets
        $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        $0.setContentHuggingPriority(.required, for: .horizontal)
    }

    private lazy var shareButton: ParlorButton = ParlorButton(style: .plain(
        titleColor: .appText,
        font: UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.regular),
        letterSpacing: Constants.LetterSpacing.none
    )).apply {
        $0.contentEdgeInsets = Constants.LinkTableViewCell.shareButtonInsets
        $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        $0.setContentHuggingPriority(.required, for: .horizontal)
        $0.setImage(Icons.ActivationDetailsInfo.share, for: .normal)
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initializeCell()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func initializeCell() {
        setupViewHierarchy()
        setupAutolayout()
        setupProperties()
        setupActions()
    }

    private func setupViewHierarchy() {
        contentView.addSubview(stackView)
        stackView.addArrangedSubviews([leftIcon, titleLabel, selectButton, shareButton])
    }

    private func setupAutolayout() {
        stackView.edgesToParent(insets: Constants.LinkTableViewCell.insets)
    }

    private func setupProperties() {
        backgroundColor = .clear
        selectedBackgroundView = nil
        selectionStyle = .none
    }

    private func setupActions() {
        selectButton.rx.tap
            .bind { [unowned self] in self.selectCallback?() }
            .disposed(by: disposeBag)
        shareButton.rx.tap
            .bind { [unowned self] in self.shareCallback?() }
            .disposed(by: disposeBag)
    }

}

extension LinkTableViewCell: ConfigurableCell {

    func configure(with model: LinkTableViewCellModel) {
        titleLabel.text = model.text
        leftIcon.image = Icons.ActivationDetailsInfo.link
        selectButton.title = Strings.ActivationDetailsInfo.join.localized.uppercased()
        selectCallback = { model.selectCallback(model) }
        shareCallback = { model.shareCallback(model) }
    }

}

fileprivate extension Constants {

    enum LinkTableViewCell {
        static let insets: UIEdgeInsets = UIEdgeInsets(top: 10, bottom: 10, horizontal: 28)
        static let selectButtonInsets: UIEdgeInsets = UIEdgeInsets(top: 6, bottom: 6, horizontal: 20)
        static let selectButtonCornerRadius: CGFloat = 14
        static let shareButtonInsets: UIEdgeInsets = UIEdgeInsets(top: 6, bottom: 6, horizontal: 0)
    }

}
