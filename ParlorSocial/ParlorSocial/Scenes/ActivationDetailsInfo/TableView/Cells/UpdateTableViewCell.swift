//
//  UpdateTableViewCell.swift
//  ParlorSocialClub
//
//  Created on 18/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class UpdateTableViewCell: UITableViewCell {

    typealias Model = ActivationUpdate

    private lazy var mainView: UIView = UIView().manualLayoutable().apply {
        $0.backgroundColor = .white
    }

    private lazy var stackView: UIStackView = UIStackView(
        axis: .vertical,
        with: [],
        spacing: Constants.UpdateTableViewCell.stackSpacing,
        alignment: .leading,
        distribution: .fill
    ).manualLayoutable()

    private lazy var datetimeLabel: UITextView = ParlorTextView().manualLayoutable().apply {
        $0.font = UIFont.custom(ofSize: Constants.FontSize.small, font: UIFont.Roboto.regular)
        $0.lineHeight = Constants.UpdateTableViewCell.lineHeight
        $0.tintColor = .appText
        $0.textColor = .appText
    }

    private lazy var textView: UITextView = ParlorTextView().manualLayoutable().apply {
        $0.font = UIFont.custom(ofSize: Constants.FontSize.body, font: UIFont.Roboto.regular)
        $0.lineHeight = Constants.UpdateTableViewCell.lineHeight
        $0.tintColor = .appGreyLight
        $0.textColor = .appGreyLight
        $0.linkClickTrackingId = AnalyticEventClickableElementName.ActivationDetailsInfo.linkInUpdateContent.rawValue
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initializeCell()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func initializeCell() {
        setupViewHierarchy()
        setupAutolayout()
        setupProperties()
    }

    private func setupViewHierarchy() {
        contentView.addSubview(mainView)
        mainView.addSubview(stackView)
        stackView.addArrangedSubviews([datetimeLabel, textView])
    }

    private func setupAutolayout() {
        mainView.edgesToParent(insets: Constants.UpdateTableViewCell.mainInsets)
        stackView.edgesToParent(insets: Constants.UpdateTableViewCell.contentInsets)
    }

    private func setupProperties() {
        backgroundColor = .clear
        selectionStyle = .none
    }

}

extension UpdateTableViewCell: ConfigurableCell {

    func configure(with model: ActivationUpdate) {
        datetimeLabel.text = model.createdAt.toActivationUpdateFormat()
        textView.text = model.content
    }

}

fileprivate extension Constants {

    enum UpdateTableViewCell {
        static let stackSpacing: CGFloat = 4
        static let lineHeight: CGFloat = 20
        static let mainInsets: UIEdgeInsets = UIEdgeInsets(top: 0, bottom: 10, horizontal: 28)
        static let contentInsets: UIEdgeInsets = UIEdgeInsets(top: 8, bottom: 22, horizontal: 22)
    }

}
