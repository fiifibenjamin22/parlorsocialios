//
//  ActivationDetailsInfoDestination.swift
//  ParlorSocialClub
//
//  Created on 18/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

enum ActivationDetailsInfoDestination {
    case activationDetails(activation: Activation, openingType: OpeningActivationDetailsType, navigationSource: ActivationDetailsNavigationSource)
    case guestList(activationId: Int)
    case share(shareData: ShareData, delegate: ShareActivationDelegate)
    case moreShareOptions(shareData: ShareData, completion: UIActivityViewController.CompletionWithItemsHandler)
}

extension ActivationDetailsInfoDestination: Destination {
    var viewController: UIViewController {
        switch self {
        case .activationDetails(let activation, let openingType, let navigationSource):
            return ActivationDetailsNavigationProvider.getVCByAvailabilityOfOpeningActivationDetails(
                forActivation: activation, openingType: openingType, navigationSource: navigationSource)
        case .guestList(let activationId):
            return GuestListRsvpViewController(activationId: activationId).embedInNavigationController()
        case .share(let shareData, let delegate):
            let viewModel = ShareActivationViewModel(shareData: shareData, pasteboard: UIPasteboard.general)
            let viewController = ShareActivationViewController(viewModel: viewModel, delegate: delegate)
            viewController.title = Strings.ShareActivation.titleLink.localized
            return viewController
        case .moreShareOptions(let shareData, let completion):
            let controller = UIActivityViewController(
                activityItems: [shareData.url, shareData.message],
                applicationActivities: nil
            )
            UIButton.appearance().tintColor = .black
            controller.completionWithItemsHandler = completion
            return controller
        }
    }
}
