//
//  ActivationDetailsInfoViewBuilder.swift
//  ParlorSocialClub
//
//  Created on 12/06/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

class ActivationDetailsInfoViewBuilder {

    private unowned let controller: ActivationDetailsInfoViewController
    private var view: UIView! { return controller.view }

    init(controller: ActivationDetailsInfoViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }

    private func setupProperties() {
        view.backgroundColor = .appBackground
    }

    private func setupHierarchy() {
        controller.apply {
            $0.view.addSubview($0.tableView)
        }
   }

    private func setupAutoLayout() {
        controller.apply {
            // HACK: fix tableView unwanted top space caused by nonzero section header height
            $0.tableView.edgesToParent(insets: UIEdgeInsets(top: -UIScreen.main.scale, bottom: 0, horizontal: 0))
        }
   }

}

extension ActivationDetailsInfoViewBuilder {

    func buildTableView() -> UITableView {
        return UITableView().manualLayoutable().apply {
            $0.backgroundColor = .appBackground
            $0.separatorStyle = .none
            $0.showsVerticalScrollIndicator = false
            $0.contentInsetAdjustmentBehavior = .never
        }
    }

}
