//  ConnectionsActionButtonsSection.swift
//  ParlorSocialClub
//
//  Created on 16/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class ConnectionsActionButtonsSection: BasicTableSection<ConnectionsActionButtonsCellModel, ConnectionsActionButtonsCell> {
    override var headerType: UITableViewHeaderFooterView.Type? { return ConnectionsDummyHeaderFooterView.self }
    override var footerType: UITableViewHeaderFooterView.Type? { return ConnectionsDummyHeaderFooterView.self }
}
