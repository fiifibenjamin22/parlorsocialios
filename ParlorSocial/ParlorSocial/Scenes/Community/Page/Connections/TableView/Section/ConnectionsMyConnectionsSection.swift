//  ConnectionsMyConnectionsSection.swift
//  ParlorSocialClub
//
//  Created on 17/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class ConnectionsMyConnectionsSection: BasicTableSection<ConnectionsMyConnectionsCellModel, ConnectionsMyConnectionsCell> {
    override var headerType: UITableViewHeaderFooterView.Type? { return ConnectionsDummyHeaderFooterView.self }
    override var footerType: UITableViewHeaderFooterView.Type? { return ConnectionsDummyHeaderFooterView.self }
}
