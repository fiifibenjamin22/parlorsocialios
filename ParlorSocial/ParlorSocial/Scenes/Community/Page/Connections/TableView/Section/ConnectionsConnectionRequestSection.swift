//
//  ConnectionsConnectionRequestSection.swift
//  ParlorSocialClub
//
//  Created by Piotr Olech on 20/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class ConnectionsConnectionRequestSection: BasicTableSection<ConnectionsConnectionRequestCellModel, ConnectionsConnectionRequestCell> {
    override var headerType: UITableViewHeaderFooterView.Type? { return ConnectionsTitleSectionHeader.self }
    private let sectionTitle: String
    
    init(sectionTitle: String, items: [ConnectionsConnectionRequestCellModel], itemSelector: @escaping ((ConnectionsConnectionRequestCellModel) -> Void)) {
        self.sectionTitle = sectionTitle
        super.init(items: items, itemSelector: itemSelector)
    }

    override func configure(header: UITableViewHeaderFooterView, in section: Int) {
        guard let sectionHeader = header as? ConnectionsTitleSectionHeader else {
            fatalError("Header is wrong type! Needed: \(ConnectionsTitleSectionHeader.name) but get instead \(String(describing: header))")
        }
        sectionHeader.titleLabel.text = sectionTitle
    }
}
