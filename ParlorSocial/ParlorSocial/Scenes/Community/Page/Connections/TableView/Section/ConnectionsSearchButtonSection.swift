//  ConnectionsSearchButtonSection.swift
//  ParlorSocialClub
//
//  Created on 16/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

final class ConnectionsSearchButtonSection: BasicTableSection<ConnectionsSearchButtonCellModel, ConnectionsSearchButtonCell> {}
