//  ConnectWithFriendSection.swift
//  ParlorSocialClub
//
//  Created on 16/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class ConnectWithFriendsSection: BasicTableSection<ConnectWithFriendsCellModel, ConnectWithFriendsCell> {}
