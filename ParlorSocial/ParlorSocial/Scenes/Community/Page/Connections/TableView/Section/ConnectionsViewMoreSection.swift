//  ConnectionsViewMoreSection.swift
//  ParlorSocialClub
//
//  Created on 22/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class ConnectionsViewMoreSection: BasicTableSection<ConnectionsViewMoreCellModel, ConnectionsViewMoreCell> {
    override var headerType: UITableViewHeaderFooterView.Type? { return ConnectionsDummyHeaderFooterView.self }
    override var footerType: UITableViewHeaderFooterView.Type? { return ConnectionsDummyHeaderFooterView.self }
}
