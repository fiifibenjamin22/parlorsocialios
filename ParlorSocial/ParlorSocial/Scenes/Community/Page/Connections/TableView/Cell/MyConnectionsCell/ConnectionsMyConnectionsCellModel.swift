//  ConnectionsMyConnectionsCellModel.swift
//  ParlorSocialClub
//
//  Created on 17/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

struct ConnectionsMyConnectionsCellModel {
    private(set) weak var delegate: ConnectionsMyConnectionsCellDelegate?
    let connections: [MyConnectionCellModel]
    let viewMoreButtonHidden: Bool
}

struct MyConnectionCellModel {
    let memberId: Int?
    let imageURL: URL?
    let isAllConnectionsCell: Bool
    let allConnectionsCellImage: UIImage?
}
