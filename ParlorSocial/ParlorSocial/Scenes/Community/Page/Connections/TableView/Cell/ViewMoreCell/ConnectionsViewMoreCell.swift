//  ConnectionsViewMoreCell.swift
//  ParlorSocialClub
//
//  Created on 22/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class ConnectionsViewMoreCell: UITableViewCell {
    
    // MARK: - Properties
    
    private let actionButton: ParlorButton = ParlorButton().manualLayoutable()
    private var action: (() -> Void)?

    // MARK: - Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setup
    
    private func setup() {
        selectionStyle = .none
        contentView.backgroundColor = .white
        setupTitleLabel()
    }
    
    private func setupTitleLabel() {
        actionButton.apply {
            $0.title = Strings.Connections.viewMore.localized
            $0.style = .selectable
            $0.setTitleColor(.appGreyLight, for: .normal)
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.xTiny, font: UIFont.Roboto.regular)
            $0.applyTouchAnimation()
            $0.addTarget(self, action: #selector(actionButtonTapped), for: .touchUpInside)
        }
        
        contentView.addSubview(actionButton)
        actionButton.edgesToParent(anchors: [.top, .bottom], insets: Constants.ActionButton.insets)
        actionButton.centerXAnchor.equal(to: contentView.centerXAnchor)
        actionButton.heightAnchor.equalTo(constant: Constants.ActionButton.height)
    }
    
    // MARK: - Actions
    
    @objc private func actionButtonTapped() {
        action?()
    }
}

extension ConnectionsViewMoreCell: ConfigurableCell {
    func configure(with model: ConnectionsViewMoreCellModel) {
        action = model.buttonAction
    }
}

private extension Constants {
    enum ActionButton {
        static let insets: UIEdgeInsets = UIEdgeInsets(top: -5, bottom: 25, left: 0, right: 0)
        static let height: CGFloat = 25
    }
}
