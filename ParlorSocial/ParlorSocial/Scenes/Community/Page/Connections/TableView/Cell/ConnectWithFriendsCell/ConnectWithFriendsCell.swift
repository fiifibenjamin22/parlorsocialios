//  ConnectWithFriendsCell.swift
//  ParlorSocialClub
//
//  Created on 16/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

protocol ConnectWithFriendsCellDelegate: ConnectionsActionButtonsCellDelegate {
    func viewGuestListsButtonTapped()
}

final class ConnectWithFriendsCell: UITableViewCell {
    
    // MARK: - Properties
    
    weak var delegate: ConnectWithFriendsCellDelegate?
    
    private let buttonsStackView = UIStackView().manualLayoutable()
    private let titleLabel = ParlorLabel().manualLayoutable()
    private let syncContactsButton = ParlorButton()
    private let referFriendButton = ParlorButton()
    private let viewGuestListsButton = ParlorButton()
    
    // MARK: - Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setup
    
    private func setup() {
        selectionStyle = .none
        contentView.backgroundColor = .appBackground
        
        setupTitleLabel()
        setupButtonsStackView()
        setupSyncContactsButton()
        setupReferFriendButton()
        setupViewGuestListsButton()
    }
    
    private func setupTitleLabel() {
        titleLabel.apply {
            $0.font = UIFont.custom(ofSize: Constants.FontSize.small, font: UIFont.Roboto.regular)
            $0.textColor = UIColor.appGreyLight
            $0.textAlignment = .center
            $0.numberOfLines = Constants.TitleLabel.numberOfLines
            $0.text = Strings.Connections.connectWithFriends.localized
        }

        contentView.addSubview(titleLabel)
        titleLabel.edgesToParent(anchors: [.top, .leading, .trailing], insets: Constants.TitleLabel.insets)
    }
    
    private func setupButtonsStackView() {
        buttonsStackView.apply {
            $0.spacing = Constants.ButtonsStackView.spacing
            $0.axis = .vertical
        }

        contentView.addSubview(buttonsStackView)
        buttonsStackView.edgesToParent(anchors: [.leading, .trailing, .bottom], insets: Constants.ButtonsStackView.insets)
        buttonsStackView.topAnchor.equal(to: titleLabel.bottomAnchor, constant: Constants.ButtonsStackView.topMargin)
        buttonsStackView.addArrangedSubviews([syncContactsButton, referFriendButton, viewGuestListsButton])
    }
    
    private func setupSyncContactsButton() {
        syncContactsButton.apply {
            $0.style = .filledWith(
                color: .appYellow,
                titleColor: .appTextMediumBright,
                font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium),
                letterSpacing: Constants.LetterSpacing.none
            )
            $0.layer.cornerRadius = Constants.standardButtonHeight / 2
            $0.addTarget(self, action: #selector(syncContactsButtonTapped), for: .touchUpInside)
            $0.title = Strings.Connections.syncContacts.localized.uppercased()
            $0.applyTouchAnimation()
            $0.heightAnchor.equalTo(constant: Constants.standardButtonHeight)
        }
    }
    
    private func setupReferFriendButton() {
        referFriendButton.apply {
            $0.style = .filledWith(
                color: .white,
                titleColor: .appTextMediumBright,
                font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium),
                letterSpacing: Constants.LetterSpacing.none
            )
            $0.layer.cornerRadius = Constants.standardButtonHeight / 2
            $0.addTarget(self, action: #selector(referFriendButtonTapped), for: .touchUpInside)
            $0.title = Strings.Connections.referFriend.localized.uppercased()
            $0.applyTouchAnimation()
            $0.heightAnchor.equalTo(constant: Constants.standardButtonHeight)
        }
    }
    
    private func setupViewGuestListsButton() {
        viewGuestListsButton.apply {
            $0.style = .outlined(
                titleColor: .appTextMediumBright,
                borderColor: .appSeparator,
                font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium),
                letterSpacing: Constants.LetterSpacing.none
            )
            $0.layer.cornerRadius = Constants.standardButtonHeight / 2
            $0.addTarget(self, action: #selector(viewGuestListsButtonTapped), for: .touchUpInside)
            $0.title = Strings.Connections.viewGuestLists.localized.uppercased()
            $0.applyTouchAnimation()
            $0.heightAnchor.equalTo(constant: Constants.standardButtonHeight)
        }
    }
    
    // MARK: - Actions
    
    @objc private func syncContactsButtonTapped() {
        delegate?.syncContactsButtonTapped()
    }
    
    @objc private func referFriendButtonTapped() {
        delegate?.referFriendButtonTapped()
    }
    
    @objc private func viewGuestListsButtonTapped() {
        delegate?.viewGuestListsButtonTapped()
    }
}

extension ConnectWithFriendsCell: ConfigurableCell {
    func configure(with model: ConnectWithFriendsCellModel) {
        delegate = model.delegate
    }
}

private extension Constants {
    enum TitleLabel {
        static let insets: UIEdgeInsets = UIEdgeInsets(top: 29, left: 49, bottom: 0, right: 49)
        static let numberOfLines: Int = 2
    }
    enum ButtonsStackView {
        static let topMargin: CGFloat = 20
        static let insets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 87, bottom: 30, right: 87)
        static let spacing: CGFloat = 20
    }
}
