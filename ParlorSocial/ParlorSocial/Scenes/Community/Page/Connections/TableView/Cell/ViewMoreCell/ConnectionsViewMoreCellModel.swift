//  ConnectionsViewMoreCellModel.swift
//  ParlorSocialClub
//
//  Created on 22/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

struct ConnectionsViewMoreCellModel {
    let buttonAction: (() -> Void)
}
