//  ConnectionsActionButtonsCell.swift
//  ParlorSocialClub
//
//  Created on 16/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

protocol ConnectionsActionButtonsCellDelegate: class {
    func syncContactsButtonTapped()
    func referFriendButtonTapped()
}

final class ConnectionsActionButtonsCell: UITableViewCell {
    
    // MARK: - Properties
    
    weak var delegate: ConnectionsActionButtonsCellDelegate?
    
    private let buttonsStackView = UIStackView().manualLayoutable()
    private let syncContactsButton = ParlorButton()
    private let referFriendButton = ParlorButton()
    
    // MARK: - Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setup
    
    private func setup() {
        selectionStyle = .none

        setupButtonsStackView()
        setupSyncContactsButton()
        setupReferFriendButton()
    }
    
    private func setupButtonsStackView() {
        buttonsStackView.apply {
            $0.spacing = Constants.ButtonsStackView.spacing
            $0.axis = .vertical
        }
        
        contentView.addSubview(buttonsStackView)
        buttonsStackView.edgesToParent(insets: Constants.ButtonsStackView.insets)
        buttonsStackView.addArrangedSubviews([syncContactsButton, referFriendButton])
    }
    
    private func setupSyncContactsButton() {
        syncContactsButton.apply {
            $0.style = .filledWith(
                color: .appYellow,
                titleColor: .appTextMediumBright,
                font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium),
                letterSpacing: Constants.LetterSpacing.none
            )
            $0.layer.cornerRadius = Constants.standardButtonHeight / 2
            $0.addTarget(self, action: #selector(syncContactsButtonTapped), for: .touchUpInside)
            $0.title = Strings.Connections.syncContacts.localized.uppercased()
            $0.applyTouchAnimation()
            $0.heightAnchor.equalTo(constant: Constants.standardButtonHeight)
        }
    }
    
    private func setupReferFriendButton() {
        referFriendButton.apply {
            $0.style = .outlined(
                titleColor: .appTextMediumBright,
                borderColor: .appSeparator,
                font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium),
                letterSpacing: Constants.LetterSpacing.none
            )
            $0.layer.cornerRadius = Constants.standardButtonHeight / 2
            $0.addTarget(self, action: #selector(referFriendButtonTapped), for: .touchUpInside)
            $0.title = Strings.Connections.referFriend.localized.uppercased()
            $0.applyTouchAnimation()
            $0.heightAnchor.equalTo(constant: Constants.standardButtonHeight)
        }
    }
    
    // MARK: - Actions
    
    @objc private func syncContactsButtonTapped() {
        delegate?.syncContactsButtonTapped()
    }
    
    @objc private func referFriendButtonTapped() {
        delegate?.referFriendButtonTapped()
    }
}

extension ConnectionsActionButtonsCell: ConfigurableCell {
    func configure(with model: ConnectionsActionButtonsCellModel) {
        delegate = model.delegate
    }
}

private extension Constants {
    enum ButtonsStackView {
        static let insets: UIEdgeInsets = UIEdgeInsets(top: 30, left: 87, bottom: 30, right: 87)
        static let spacing: CGFloat = 20
    }
}
