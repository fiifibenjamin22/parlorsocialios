//  ConnectionsFriendCollectionViewCell.swift
//  ParlorSocialClub
//
//  Created on 17/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class ConnectionsFriendCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Properties
    
    private let avatarImageView = UIImageView().manualLayoutable()
    
    // MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        applyTouchAnimation()
        setupAvatarImageView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setup
    
    private func setupAvatarImageView() {
        avatarImageView.apply {
            $0.clipsToBounds = true
            $0.layer.cornerRadius = bounds.height / 2
        }
        
        addSubview(avatarImageView)
        avatarImageView.edgesToParent()
    }
    
    func setup(with data: MyConnectionCellModel) {
        if data.isAllConnectionsCell {
            avatarImageView.image = data.allConnectionsCellImage
        } else {
            avatarImageView.getImage(from: data.imageURL, placeholder: Icons.Common.placeholderPhoto)
        }
    }
}
