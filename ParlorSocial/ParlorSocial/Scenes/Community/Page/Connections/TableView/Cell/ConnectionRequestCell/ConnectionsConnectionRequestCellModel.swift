//
//  ConnectionsConnectionRequestCellModel.swift
//  ParlorSocialClub
//
//  Created by Piotr Olech on 20/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

struct ConnectionsConnectionRequestCellModel {
    let requestId: Int
    let memberId: Int
    let imageURL: URL?
    let name: String
    let shouldHideSeparator: Bool
    private(set) weak var delegate: ConnectionsConnectionRequestCellDelegate?
}
