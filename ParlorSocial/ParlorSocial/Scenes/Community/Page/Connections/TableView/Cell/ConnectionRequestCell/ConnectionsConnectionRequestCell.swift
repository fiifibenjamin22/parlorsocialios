//
//  ConnectionsConnectionRequestCell.swift
//  ParlorSocialClub
//
//  Created by Piotr Olech on 20/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

protocol ConnectionsConnectionRequestCellDelegate: class {
    func rejectButtonTapped(inCell cell: UITableViewCell, requestId: Int)
    func acceptButtonTapped(inCell cell: UITableViewCell, requestId: Int)
}

final class ConnectionsConnectionRequestCell: UITableViewCell {
    
    // MARK: - Properties
    
    private weak var delegate: ConnectionsConnectionRequestCellDelegate?
    private var requestId: Int?
    
    private let avatarImageView = UIImageView().manualLayoutable()
    private let nameLabel = ParlorLabel().manualLayoutable()
    private let rejectButton = ParlorButton().manualLayoutable()
    private let acceptButton = ParlorButton().manualLayoutable()
    private let separatorView = UIView().manualLayoutable()
    
    // MARK: - Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        contentView.backgroundColor = .white
    }
    
    // MARK: - Setup
    
    private func setup() {
        selectionStyle = .none
        contentView.backgroundColor = .white
        
        setupAvatarImageView()
        setupNameLabel()
        setupAcceptButton()
        setupRejectButton()
        setupSeparatorView()
    }
    
    private func setupAvatarImageView() {
        avatarImageView.apply {
            $0.clipsToBounds = true
            $0.layer.cornerRadius = Constants.AvatarImageView.height / 2
        }
        
        contentView.addSubview(avatarImageView)
        avatarImageView.edgesToParent(anchors: [.top, .leading, .bottom], insets: Constants.AvatarImageView.insets)
        avatarImageView.heightAnchor.equalTo(constant: Constants.AvatarImageView.height)
        avatarImageView.widthAnchor.equal(to: avatarImageView.heightAnchor)
    }
    
    private func setupNameLabel() {
        nameLabel.apply {
            $0.font = UIFont.custom(ofSize: Constants.FontSize.small, font: UIFont.Roboto.medium)
            $0.lineHeight = Constants.NameLabel.lineHeight
            $0.textColor = UIColor.appTextMediumBright
            $0.numberOfLines = Constants.NameLabel.numberOfLines
        }
        
        contentView.addSubview(nameLabel)
        nameLabel.centerYAnchor.equal(to: avatarImageView.centerYAnchor, constant: Constants.NameLabel.centerYMargin)
        nameLabel.bottomAnchor.greaterThanOrEqual(to: contentView.bottomAnchor, constant: Constants.NameLabel.bottomMargin)
        nameLabel.leftAnchor.equal(to: avatarImageView.rightAnchor, constant: Constants.NameLabel.leftMargin)
    }
    
    private func setupAcceptButton() {
        acceptButton.apply {
            $0.style = .filledWith(
                color: .appTextBright,
                titleColor: .white,
                font: UIFont.custom(ofSize: Constants.FontSize.xTiny, font: UIFont.Roboto.medium),
                letterSpacing: Constants.LetterSpacing.none
            )
            $0.layer.cornerRadius = Constants.AcceptButton.height / 2
            $0.title = Strings.Connections.accept.localized.uppercased()
            $0.applyTouchAnimation()
            $0.addTarget(self, action: #selector(acceptButtonTapped), for: .touchUpInside)
        }
        
        contentView.addSubview(acceptButton)
        acceptButton.heightAnchor.equalTo(constant: Constants.AcceptButton.height)
        acceptButton.widthAnchor.equalTo(constant: Constants.AcceptButton.width)
        acceptButton.centerYAnchor.equal(to: avatarImageView.centerYAnchor)
        acceptButton.rightAnchor.equal(to: contentView.rightAnchor, constant: Constants.AcceptButton.rightMargin)
    }
    
    private func setupRejectButton() {
        rejectButton.apply {
            $0.style = .plain(
                titleColor: .black,
                font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.bold),
                letterSpacing: Constants.LetterSpacing.small
            )
            $0.setImage(Icons.Connections.rejectIcon, for: .normal)
            $0.layer.cornerRadius = Constants.RejectButton.height / 2
            $0.applyTouchAnimation()
            $0.addTarget(self, action: #selector(rejectButtonTapped), for: .touchUpInside)
        }
        
        contentView.addSubview(rejectButton)
        rejectButton.heightAnchor.equalTo(constant: Constants.RejectButton.height)
        rejectButton.widthAnchor.equal(to: rejectButton.heightAnchor)
        rejectButton.centerYAnchor.equal(to: acceptButton.centerYAnchor)
        rejectButton.leftAnchor.equal(to: nameLabel.rightAnchor, constant: Constants.RejectButton.leftMargin)
        rejectButton.rightAnchor.equal(to: acceptButton.leftAnchor, constant: Constants.RejectButton.rightMargin)
    }
    
    private func setupSeparatorView() {
        separatorView.backgroundColor = .textVeryLight
        
        contentView.addSubview(separatorView)
        separatorView.edgesToParent(anchors: [.leading, .trailing, .bottom], insets: Constants.SeparatorView.insets)
        separatorView.heightAnchor.equalTo(constant: 1)
    }
    
    // MARK: - Actions
    
    @objc private func rejectButtonTapped() {
        guard let requestId = requestId else { return }
        delegate?.rejectButtonTapped(inCell: self, requestId: requestId)
    }
    
    @objc private func acceptButtonTapped() {
        guard let requestId = requestId else { return }
        delegate?.acceptButtonTapped(inCell: self, requestId: requestId)
    }
}

// MARK: - ConfigurableCell
extension ConnectionsConnectionRequestCell: ConfigurableCell {
    func configure(with model: ConnectionsConnectionRequestCellModel) {
        avatarImageView.getImage(from: model.imageURL, placeholder: Icons.Common.placeholderPhoto)
        nameLabel.text = model.name
        separatorView.isHidden = model.shouldHideSeparator
        delegate = model.delegate
        requestId = model.requestId
    }
}

private extension Constants {
    enum AcceptButton {
        static let height: CGFloat = 30
        static let width: CGFloat = 94
        static let rightMargin: CGFloat = -16
    }
    enum AvatarImageView {
        static let height: CGFloat = 60
        static let insets: UIEdgeInsets = UIEdgeInsets(top: 20, left: 16, bottom: 20, right: 0)
    }
    enum RejectButton {
        static let height: CGFloat = 30
        static let leftMargin: CGFloat = 15
        static let rightMargin: CGFloat = -15
    }
    enum SeparatorView {
        static let insets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
    }
    enum NameLabel {
        static let leftMargin: CGFloat = 13
        static let bottomMargin: CGFloat = -20
        static let centerYMargin: CGFloat = -8
        static let lineHeight: CGFloat = 22
        static let numberOfLines: Int = 2
    }
}
