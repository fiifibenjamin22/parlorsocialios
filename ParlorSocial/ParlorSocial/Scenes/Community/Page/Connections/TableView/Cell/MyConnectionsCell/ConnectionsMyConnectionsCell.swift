//  ConnectionsMyConnectionsCell.swift
//  ParlorSocialClub
//
//  Created on 17/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

protocol ConnectionsMyConnectionsCellDelegate: class {
    func selectedConnection(_ connection: MyConnectionCellModel)
    func viewMoreMyConnections()
}

final class ConnectionsMyConnectionsCell: UITableViewCell {
    
    // MARK: - Properties
    
    private var collectionView: UICollectionView!
    private var collectionViewHeightConstraint: NSLayoutConstraint!
    private var viewMoreButtonBottomConstraint: NSLayoutConstraint!
    private var connections: [MyConnectionCellModel] = []
    private weak var delegate: ConnectionsMyConnectionsCellDelegate?
    
    private let titleLabel: ParlorLabel = ParlorLabel().manualLayoutable()
    private let viewMoreButton: ParlorButton = ParlorButton().manualLayoutable()
    
    // MARK: - Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Setup
    
    private func setup() {
        selectionStyle = .none
        contentView.backgroundColor = .appBackground

        setupTitleLabel()
        setupCollectionView()
        setupViewMoreButton()
    }
    
    private func setupTitleLabel() {
        titleLabel.apply {
            $0.font = UIFont.custom(ofSize: Constants.FontSize.xTiny, font: UIFont.Roboto.regular)
            $0.textColor = UIColor.appTextSemiLight
            $0.textAlignment = .left
            $0.text = Strings.Connections.myConnections.localized
        }

        contentView.addSubview(titleLabel)
        titleLabel.edgesToParent(anchors: [.top, .leading, .trailing], insets: Constants.TitleLabel.edgeInsets)
    }
    
    private func setupCollectionView() {
        let flowLayout = UICollectionViewFlowLayout().apply {
            $0.minimumLineSpacing = Constants.CollectionView.lineSpacing
            $0.minimumInteritemSpacing = Constants.CollectionView.itemSpacing
            $0.itemSize = CGSize(width: Constants.CollectionView.itemWidth, height: Constants.CollectionView.itemHeight)
        }
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout).manualLayoutable()
        collectionView.apply {
            $0.showsVerticalScrollIndicator = false
            $0.isScrollEnabled = false
            $0.allowsMultipleSelection = false
            $0.dataSource = self
            $0.delegate = self
            $0.backgroundColor = .clear
            $0.register(ConnectionsFriendCollectionViewCell.self, forCellWithReuseIdentifier: ConnectionsFriendCollectionViewCell.name)
        }
        
        contentView.addSubview(collectionView)
        collectionViewHeightConstraint = collectionView.heightAnchor.constraint(equalToConstant: Constants.CollectionView.itemHeight).activate()
        collectionView.topAnchor.equal(to: titleLabel.bottomAnchor, constant: 10)
        collectionView.edgesToParent(
            anchors: [.leading, .trailing],
            insets: .margins(left: Constants.CollectionView.leftMargin, right: Constants.CollectionView.rightMargin)
        )
    }
    
    private func setupViewMoreButton() {
        viewMoreButton.apply {
            $0.title = Strings.Connections.viewMore.localized
            $0.style = .selectable
            $0.setTitleColor(.appGreyLight, for: .normal)
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.xTiny, font: UIFont.Roboto.regular)
            $0.applyTouchAnimation()
            $0.addTarget(self, action: #selector(viewMoreTapped), for: .touchUpInside)
        }
        
        contentView.addSubview(viewMoreButton)
        viewMoreButton.topAnchor.equal(to: collectionView.bottomAnchor, constant: Constants.ViewMoreButton.topMargin)
        viewMoreButton.centerXAnchor.equal(to: contentView.centerXAnchor)
        viewMoreButton.heightAnchor.equalTo(constant: Constants.ViewMoreButton.height)
        viewMoreButtonBottomConstraint = viewMoreButton.bottomAnchor.equal(to: contentView.bottomAnchor, constant: Constants.ViewMoreButton.bottomMarginWhenVisible).activate()
    }
    
    // MARK: - Actions
    
    @objc private func viewMoreTapped() {
        delegate?.viewMoreMyConnections()
    }
}

// MARK: - ConfigurableCell
extension ConnectionsMyConnectionsCell: ConfigurableCell {
    func configure(with model: ConnectionsMyConnectionsCellModel) {
        delegate = model.delegate
        connections = model.connections
        setupCollectionViewHeight(numberOfItems: model.connections.count)
        viewMoreButton.isHidden = model.viewMoreButtonHidden
        viewMoreButtonBottomConstraint.constant = model.viewMoreButtonHidden ? Constants.ViewMoreButton.bottomMarginWhenHidden : Constants.ViewMoreButton.bottomMarginWhenVisible
        collectionView.reloadData()
    }
    
    private func setupCollectionViewHeight(numberOfItems: Int) {
        let collectionViewWidth = UIScreen.main.bounds.width - Constants.CollectionView.leftMargin - Constants.CollectionView.rightMargin
        let numberOfElementsInRow = Int(collectionViewWidth / Constants.CollectionView.itemWidth)
        let numberOfRows = CGFloat(numberOfItems / numberOfElementsInRow) + 1
        let height: CGFloat = numberOfRows * Constants.CollectionView.itemHeight + (numberOfRows - 1) * Constants.CollectionView.lineSpacing
        collectionViewHeightConstraint.constant = height > 0 ? height : Constants.CollectionView.itemHeight
    }
}

// MARK: - UICollectionViewDelegate
extension ConnectionsMyConnectionsCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.selectedConnection(connections[indexPath.item])
    }
}

// MARK: - UICollectionViewDataSource
extension ConnectionsMyConnectionsCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return connections.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ConnectionsFriendCollectionViewCell.name, for: indexPath) as? ConnectionsFriendCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.setup(with: connections[indexPath.item])
        return cell
    }
}

private extension Constants {
    enum CollectionView {
        static let itemSpacing: CGFloat = 34
        static let lineSpacing: CGFloat = 20
        static let itemWidth: CGFloat = 60
        static let itemHeight: CGFloat = 60
        static let leftMargin: CGFloat = 16
        static let rightMargin: CGFloat = 16
    }
    
    enum ViewMoreButton {
        static let bottomMarginWhenVisible: CGFloat = -20
        static let bottomMarginWhenHidden: CGFloat = 26
        static let height: CGFloat = 30
        static let topMargin: CGFloat = 12
    }
    
    enum TitleLabel {
        static let edgeInsets = UIEdgeInsets(top: 17, left: 16, bottom: 0, right: 16)
    }
}
