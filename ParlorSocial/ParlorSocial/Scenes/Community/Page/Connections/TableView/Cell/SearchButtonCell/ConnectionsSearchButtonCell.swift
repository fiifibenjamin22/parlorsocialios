//  ConnectionsSearchButtonCell.swift
//  ParlorSocialClub
//
//  Created on 16/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

protocol ConnectionsSearchButtonCellDelegate: class {
    func searchTapped()
}

final class ConnectionsSearchButtonCell: UITableViewCell {
    
    // MARK: - Properties
    
    weak var delegate: ConnectionsSearchButtonCellDelegate?
    
    private let containerView = UIView().manualLayoutable()
    private let searchImageView = UIImageView().manualLayoutable()
    private let titleLabel = ParlorLabel().manualLayoutable()
    
    // MARK: - Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setup
    
    private func setup() {
        selectionStyle = .none
        
        setupContainerView()
        setupSearchImageView()
        setupTitleLabel()
    }
    
    private func setupContainerView() {
        containerView.apply {
            $0.layer.cornerRadius = Constants.ContainerView.height / 2
            $0.layer.borderWidth = 1
            $0.layer.borderColor = UIColor.appSeparator.cgColor
            $0.applyTouchAnimation()
            $0.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(searchTapped)))
        }
        
        contentView.addSubview(containerView)
        containerView.heightAnchor.equalTo(constant: Constants.ContainerView.height)
        containerView.edgesToParent(insets: Constants.ContainerView.insets)
    }
    
    private func setupSearchImageView() {
        searchImageView.contentMode = .scaleAspectFit
        searchImageView.image = Icons.NavigationBar.search
        
        containerView.addSubview(searchImageView)
        searchImageView.edgesToParent(anchors: [.leading], insets: Constants.SearchImageView.insets)
        searchImageView.centerYAnchor.equal(to: containerView.centerYAnchor)
        searchImageView.heightAnchor.equalTo(constant: Constants.SearchImageView.height)
        searchImageView.widthAnchor.equal(to: searchImageView.heightAnchor)
    }
    
    private func setupTitleLabel() {
        titleLabel.apply {
            $0.font = UIFont.custom(ofSize: Constants.FontSize.xTiny, font: UIFont.Roboto.regular)
            $0.textColor = UIColor.appGreyLight
            $0.textAlignment = .left
            $0.text = Strings.Connections.searchPeople.localized
        }
        
        containerView.addSubview(titleLabel)
        titleLabel.centerYAnchor.equal(to: searchImageView.centerYAnchor)
        titleLabel.leftAnchor.equal(to: searchImageView.rightAnchor, constant: Constants.TitleLabel.leftMargin)
        titleLabel.rightAnchor.greaterThanOrEqual(to: containerView.rightAnchor, constant: Constants.TitleLabel.rightMargin)
    }
    
    // MARK: - Actions
    
    @objc private func searchTapped() {
        delegate?.searchTapped()
    }
}

extension ConnectionsSearchButtonCell: ConfigurableCell {
    func configure(with model: ConnectionsSearchButtonCellModel) {
        delegate = model.delegate
    }
}

private extension Constants {
    enum ContainerView {
        static let height: CGFloat = 30
        static let insets: UIEdgeInsets = UIEdgeInsets(top: 20, left: 16, bottom: 20, right: 16)
    }
    enum SearchImageView {
        static let height: CGFloat = 13
        static let insets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 13, bottom: 0, right: 0)
    }
    enum TitleLabel {
        static let leftMargin: CGFloat = 7
        static let rightMargin: CGFloat = 10
    }
}
