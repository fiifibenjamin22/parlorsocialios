//  ConnectionsDummyHeaderFooterView.swift
//  ParlorSocialClub
//
//  Created on 28/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

//replaces table view header/footer default views for specified sections due insert/delete view animations
final class ConnectionsDummyHeaderFooterView: UITableViewHeaderFooterView {
    private let view: UIView = UIView().manualLayoutable()

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        contentView.backgroundColor = .white
        contentView.addSubview(view)
        view.edgesToParent()
        view.heightAnchor.equalTo(constant: 1)
    }
}
