//  ConnectionsTitleSectionHeader.swift
//  ParlorSocialClub
//
//  Created on 22/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class ConnectionsTitleSectionHeader: UITableViewHeaderFooterView {
    lazy var titleLabel: ParlorLabel = ParlorLabel().manualLayoutable()

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setup() {
        contentView.backgroundColor = .white
        setupTitleLabel()
    }

    private func setupTitleLabel() {
        titleLabel.apply {
            $0.font = UIFont.custom(ofSize: Constants.FontSize.xTiny, font: UIFont.Roboto.regular)
            $0.textColor = .appTextSemiLight
        }
        
        contentView.addSubview(titleLabel)
        titleLabel.edgesToParent(insets: Constants.titleLabelInsets)
    }
}

private extension Constants {
    static let titleLabelInsets: UIEdgeInsets = UIEdgeInsets(top: 20, bottom: 0, left: 16, right: 10)
}
