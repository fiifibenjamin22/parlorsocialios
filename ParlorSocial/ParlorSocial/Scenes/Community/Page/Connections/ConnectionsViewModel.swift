//  ConnectionsViewModel.swift
//  ParlorSocialClub
//
//  Created on 15/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import RxSwift
import RxCocoa

protocol ConnectionsViewLogic: BaseViewModelLogic {
    var sectionsObs: Observable<[TableSection]> { get }
    var destinationObs: Observable<CommunityDestination> { get }
    var connectionRequestTableUpdateObs: Observable<ConnectionRequestUpdateTableModel> { get }

    func loadData()
    func refreshData()
}

final class ConnectionsViewModel: BaseViewModel {
    
    // MARK: - Properties
    
    let analyticEventReferencedId: Int? = nil
    
    private lazy var connectionRequestCellModels: [ConnectionsConnectionRequestCellModel] = []
    private lazy var connectionRequestSelector: (ConnectionsConnectionRequestCellModel) -> Void = { [unowned self] request in
        self.navigateToMemberDetails(using: request.memberId)
    }
    
    private let sectionsRelay = BehaviorRelay<[TableSection]?>(value: nil)
    private let destinationSubject = PublishSubject<CommunityDestination>()
    private let connectionRequestTableUpdateRelay = PublishRelay<ConnectionRequestUpdateTableModel>()
    private let connectionsRepository = ConnectionsRepository.shared

    // MARK: - Init
    
    override init() {
        super.init()
    }
    
    // MARK: - Data loading
    
    func loadData() {
        loadMyConnectionsAndConnectionRequests()
            .showingProgressBar(with: self)
            .bind { [unowned self] (myConnectionsResponse, connectionRequestsResponse) in
                self.handleMyConnectionsAndConnectionRequestsResponses(myConnectionsResponse: myConnectionsResponse, connectionRequestsResponse: connectionRequestsResponse)
            }
            .disposed(by: disposeBag)
    }
    
    private func loadMyConnectionsAndConnectionRequests() -> Observable<(ApiResponse<MyConnections.Get.Response>, ApiResponse<ConnectionRequests.Get.Response>)> {
        let myConnectionsReuqestData = MyConnections.Get.Request(page: 0)
        let connectionRequestsRequestData = ConnectionRequests.Get.Request(page: 0)
        
        return Observable.combineLatest(
            connectionsRepository.getMyConnections(using: myConnectionsReuqestData),
            connectionsRepository.getConnectionRequests(using: connectionRequestsRequestData)
        )
    }
    
    private func handleMyConnectionsAndConnectionRequestsResponses(myConnectionsResponse: ApiResponse<MyConnections.Get.Response>, connectionRequestsResponse: ApiResponse<ConnectionRequests.Get.Response>) {
        switch (myConnectionsResponse, connectionRequestsResponse) {
        case (.failure(let error), .failure):
            errorSubject.onNext(error)
        case (.success(let myConnections), .success(let connectionRequests)):
            buildSections(usingMyConnections: myConnections.data, andConnectionRequests: connectionRequests.data)
        case (.success(let myConnections), _):
            buildSections(usingMyConnections: myConnections.data, andConnectionRequests: [])
        case (_, .success(let connectionRequests)):
            buildSections(usingMyConnections: [], andConnectionRequests: connectionRequests.data)
        }
    }
    
    // MARK: - Sections building
    
    private func buildSections(usingMyConnections myConnections: [MyConnection], andConnectionRequests connectionRequests: [ConnectionRequest]) {
        var sections: [TableSection] = []
        if !connectionRequests.isEmpty {
            sections.append(createConnectionRequestsSection(using: connectionRequests))
            if connectionRequests.count > Constants.maxVisibleConnectionRequests {
                sections.append(createViewMoreRequestsSection())
            }
        }
        if !myConnections.isEmpty {
            sections.append(createMyConnectionsSection(using: myConnections))
        }
        if myConnections.isEmpty {
            sections.append(createConnectWithFriendsSection())
        } else {
            sections.append(createConnectionsActionButtonsSection())
        }
        
        sectionsRelay.accept(sections)
    }
    
    private func createConnectWithFriendsSection() -> TableSection {
        return ConnectWithFriendsSection(items: [ConnectWithFriendsCellModel(delegate: self)])
    }
    
    private func createConnectionsActionButtonsSection() -> TableSection {
        return ConnectionsActionButtonsSection(items: [ConnectionsActionButtonsCellModel(delegate: self)])
    }
    
    private func createConnectionsSearchButtonSection() -> TableSection {
        return ConnectionsSearchButtonSection(items: [ConnectionsSearchButtonCellModel(delegate: self)])
    }
    
    private func createMyConnectionsSection(using connections: [MyConnection]) -> TableSection {
        var viewMoreButtonHidden = true
        var connectionModels: [MyConnectionCellModel] = connections.map { MyConnectionCellModel(memberId: $0.memberId, imageURL: $0.imageURL, isAllConnectionsCell: false, allConnectionsCellImage: nil) }
        
        if connectionModels.count < Constants.maxVisibleMyConnections {
            connectionModels.append(MyConnectionCellModel(memberId: nil, imageURL: nil, isAllConnectionsCell: true, allConnectionsCellImage: Icons.Connections.moreConnectionsIcon))
        } else {
            viewMoreButtonHidden = false
        }
        let limitedConnections = Array(connectionModels.prefix(Constants.maxVisibleMyConnections))

        return ConnectionsMyConnectionsSection(items:
            [ConnectionsMyConnectionsCellModel(delegate: self, connections: limitedConnections, viewMoreButtonHidden: viewMoreButtonHidden)
        ])
    }
    
    private func createConnectionRequestsSection(using connectionRequests: [ConnectionRequest]) -> TableSection {
        connectionRequestCellModels = connectionRequests.enumerated().map { (index, request) in
            let shouldHideSeparator = index >= (Constants.maxVisibleConnectionRequests - 1)
            return ConnectionsConnectionRequestCellModel(requestId: request.requestId, memberId: request.memberId ?? 1, imageURL: request.imageURL,
                                                         name: request.name, shouldHideSeparator: shouldHideSeparator, delegate: self)
        }
        let requests = Array(connectionRequestCellModels.prefix(Constants.maxVisibleConnectionRequests))
        return ConnectionsConnectionRequestSection(sectionTitle: Strings.Connections.connectionRequests.localized, items: requests, itemSelector: connectionRequestSelector)
    }
    
    private func createViewMoreRequestsSection() -> TableSection {
        let cellModel = ConnectionsViewMoreCellModel(buttonAction: { [unowned self] in
            AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.Connections.viewMoreConnectionRequests.rawValue)
            self.destinationSubject.onNext(.connectionRequests)
        })
        return ConnectionsViewMoreSection(items: [cellModel])
    }
    
    // MARK: - Actions
    
    private func navigateToMemberDetails(using memberId: Int) {
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.Connections.connectionRequest.rawValue)
        destinationSubject.onNext(.memberProfile(id: memberId))
    }
    
    private func changeConnectionRequestState(with id: Int, accept: Bool) {
        let requestData = UpdateConnectionRequest(accept: accept)
        connectionsRepository.updateConnection(with: id, using: requestData).subscribe().disposed(by: disposeBag)
    }
}

// MARK: - ConnectionsViewLogic
extension ConnectionsViewModel: ConnectionsViewLogic {
    var destinationObs: Observable<CommunityDestination> {
        return destinationSubject.asObservable()
    }
    
    var sectionsObs: Observable<[TableSection]> {
        return sectionsRelay.asObservable().ignoreNil()
    }
    
    var connectionRequestTableUpdateObs: Observable<ConnectionRequestUpdateTableModel> {
        return connectionRequestTableUpdateRelay.asObservable()
    }
    
    func refreshData() {
        loadMyConnectionsAndConnectionRequests()
            .bind { [unowned self] (myConnectionsResponse, connectionRequestsResponse) in
                self.handleMyConnectionsAndConnectionRequestsResponses(myConnectionsResponse: myConnectionsResponse, connectionRequestsResponse: connectionRequestsResponse)
            }
            .disposed(by: disposeBag)
    }
}

// MARK: - ConnectWithFriendsCellDelegate
extension ConnectionsViewModel: ConnectWithFriendsCellDelegate {
    func syncContactsButtonTapped() {
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.Connections.syncContacts.rawValue)
        // TODO: add action
    }
    
    func referFriendButtonTapped() {
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.Connections.referFriend.rawValue)
        destinationSubject.onNext(.referFriend)
    }
    
    func viewGuestListsButtonTapped() {
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.Connections.viewGuestLists.rawValue)
        destinationSubject.onNext(.homeRsvpsTab)
    }
}

// MARK: - ConnectionsSearchButtonCellDelegate
extension ConnectionsViewModel: ConnectionsSearchButtonCellDelegate {
    func searchTapped() {
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.Connections.search.rawValue)
        // TODO: add action
    }
}

// MARK: - ConnectionsMyConnectionsCellDelegate
extension ConnectionsViewModel: ConnectionsMyConnectionsCellDelegate {
    func selectedConnection(_ connection: MyConnectionCellModel) {
        if connection.isAllConnectionsCell {
            AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.Connections.myConnectionViewMore.rawValue)
            destinationSubject.onNext(.myConnections)
        } else if let memberId = connection.memberId {
            AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.Connections.myConnection.rawValue)
            destinationSubject.onNext(.memberProfile(id: memberId))
        }
    }
    
    func viewMoreMyConnections() {
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.Connections.viewMoreMyConnections.rawValue)
        destinationSubject.onNext(.myConnections)
    }
}

// MARK: - ConnectionsConnectionRequestCellDelegate
extension ConnectionsViewModel: ConnectionsConnectionRequestCellDelegate {
    func rejectButtonTapped(inCell cell: UITableViewCell, requestId: Int) {
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.Connections.rejectRequest.rawValue)
        changeConnectionRequestState(with: requestId, accept: false)
        completeConnectionRequestStateChange(actionType: .reject, cell: cell, requestId: requestId)
    }
    
    func acceptButtonTapped(inCell cell: UITableViewCell, requestId: Int) {
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.Connections.acceptRequest.rawValue)
        changeConnectionRequestState(with: requestId, accept: true)
        completeConnectionRequestStateChange(actionType: .accept, cell: cell, requestId: requestId)
    }
    
    private func completeConnectionRequestStateChange(actionType: ConnectionRequestUpdateTableModel.ActionType, cell: UITableViewCell, requestId: Int) {
        guard var sections = sectionsRelay.value else { return }
        guard let sectionIndex = sections.firstIndex(where: { $0.cellType == ConnectionsConnectionRequestCell.self }) else { return }
        guard var items: [ConnectionsConnectionRequestCellModel] = (sections[sectionIndex].getItems()),
            let dataIndex = items.firstIndex(where: { $0.requestId == requestId }) else {
                return
        }

        let lastVisibleConnectionRequestId = items.last?.requestId
        let nextItem = getNextConnectionRequestItem(after: lastVisibleConnectionRequestId)
        
        items.remove(at: dataIndex)
        removeConnectionRequest(with: requestId)
        
        var addedNewItem = false
        if let item = nextItem {
            items = items.map { ConnectionsConnectionRequestCellModel(requestId: $0.requestId, memberId: $0.memberId, imageURL: $0.imageURL,
                                                                      name: $0.name, shouldHideSeparator: false, delegate: $0.delegate)
            }
            items.append(item)
            addedNewItem = true
        }
        sections[sectionIndex].setItems(items)
        
        let updateModel = ConnectionRequestUpdateTableModel(
            cellToRemove: cell,
            actionType: actionType,
            sectionIndex: sectionIndex,
            itemIndex: dataIndex,
            updatedSections: sections,
            addedNewItem: addedNewItem,
            completionHandler: { [unowned self] in
                self.removeViewMoreConnectionRequestSectionIfNeeded(from: &sections)
                self.removeConnectionRequestsSectionIfNeeded(from: &sections, sectionIndex: sectionIndex)
                self.sectionsRelay.accept(sections)
            }
        )
        connectionRequestTableUpdateRelay.accept(updateModel)
    }
    
    private func removeConnectionRequest(with id: Int) {
        guard let index = connectionRequestCellModels.firstIndex(where: { $0.requestId == id }) else { return }
        connectionRequestCellModels.remove(at: index)
    }
    
    private func getNextConnectionRequestItem(after requestId: Int?) -> ConnectionsConnectionRequestCellModel? {
        guard let currentItemIndex = connectionRequestCellModels.firstIndex(where: { $0.requestId == requestId }) else { return nil }
        let nextItemIndex = currentItemIndex + 1
        if nextItemIndex < connectionRequestCellModels.count {
            return connectionRequestCellModels[nextItemIndex]
        } else {
            return nil
        }
    }
    
    private func removeViewMoreConnectionRequestSectionIfNeeded(from sections: inout [TableSection]) {
        guard connectionRequestCellModels.count <= Constants.maxVisibleConnectionRequests else { return }
        guard let sectionIndex = sections.firstIndex(where: { $0.cellType == ConnectionsViewMoreCell.self }) else { return }
        sections.remove(at: sectionIndex)
    }
    
    private func removeConnectionRequestsSectionIfNeeded(from sections: inout [TableSection], sectionIndex: Int) {
        guard connectionRequestCellModels.count == 0 else { return }
        sections.remove(at: sectionIndex)
    }
}

private extension Constants {
    static let maxVisibleMyConnections = 12
    static let maxVisibleConnectionRequests = 3
}
