//  ConnectionRequestUpdateTableModel.swift
//  ParlorSocialClub
//
//  Created on 23/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

struct ConnectionRequestUpdateTableModel {
    enum ActionType {
        case accept
        case reject
    }
    let cellToRemove: UITableViewCell
    let actionType: ActionType
    let sectionIndex: Int
    let itemIndex: Int
    let updatedSections: [TableSection]
    let addedNewItem: Bool
    let completionHandler: () -> Void
}
