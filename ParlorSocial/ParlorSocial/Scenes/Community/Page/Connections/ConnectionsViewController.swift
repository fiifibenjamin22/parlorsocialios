//  ConnectionsViewController.swift
//  ParlorSocialClub
//
//  Created on 15/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class ConnectionsViewController: AppViewController {

    // MARK: - Properties

    override var observableSources: ObservableSources? { return viewModel }
    override var connectedOnboardingType: OnboardingType? { return nil } // TODO: wait for designs for onboarding
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? { return viewModel.analyticEventScreenSubject }

    private var tableManager: TableViewManager!

    private lazy var viewModel: ConnectionsViewLogic = {
        return ConnectionsViewModel()
    }()

    let analyticEventScreen: AnalyticEventScreen? = .connections

    private let tableView: UITableView = UITableView(frame: .zero, style: .grouped).manualLayoutable()
    
    // MARK: - Init
    
    init(router: Router) {
        super.init(nibName: nil, bundle: nil)

        self.router = router
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        viewModel.loadData()
    }
    
    // MARK: - Setup
    
    private func setup() {
        setupTableView()
    }
    
    private func setupTableView() {
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.backgroundColor = .white
        
        tableManager = TableViewManager(tableView: tableView, delegate: self)
        tableManager.setupRefreshControl()
        
        view.addSubview(tableView)
        tableView.edgesToParent()
    }
    
    // MARK: - Data binding
    
    override func observeViewModel() {
        super.observeViewModel()
        
        viewModel.sectionsObs
            .bind { [unowned self] sections in self.setNewSections(sections) }
            .disposed(by: disposeBag)
        
        viewModel.destinationObs
            .bind { [unowned self] destination in self.navigate(using: destination) }
            .disposed(by: disposeBag)
        
        viewModel.connectionRequestTableUpdateObs
            .bind { [unowned self] data in
                self.updateTableViewAfterConnectionRequestChange(data: data)
            }
            .disposed(by: disposeBag)
    }
    
    // MARK: - Actions
    
    private func setNewSections(_ sections: [TableSection]) {
        tableManager.invalidateCache()
        tableManager.setupWith(sections: sections)
        tableManager.hideLoadingFooter()
    }
    
    private func navigate(using destination: CommunityDestination) {
        switch destination {
        case .homeRsvpsTab:
            guard let tabBarController = findAppTabBarInHierarchy(startingWith: self) else { return }
            tabBarController.selectHomeTab(withSelectedSubtab: .rsvps)
        case .referFriend, .memberProfile, .myConnections, .connectionRequests:
            router.push(destination: destination)
        }
    }
    
    private func updateTableViewAfterConnectionRequestChange(data: ConnectionRequestUpdateTableModel) {
        let indexPath = IndexPath(item: data.itemIndex, section: data.sectionIndex)
        let newIndexPath = IndexPath(item: tableView.numberOfRows(inSection: data.sectionIndex) - 1, section: data.sectionIndex)
        var newFrame: CGRect
        var backgroundColor: UIColor
        var deleteRowAnimation: UITableView.RowAnimation
        
        switch data.actionType {
        case .accept:
            newFrame = CGRect(x: data.cellToRemove.bounds.width, y: data.cellToRemove.frame.origin.y, width: data.cellToRemove.bounds.width, height: data.cellToRemove.bounds.height)
            backgroundColor = .appYellow
            deleteRowAnimation = .right
        case .reject:
            newFrame = CGRect(x: -data.cellToRemove.bounds.width, y: data.cellToRemove.frame.origin.y, width: data.cellToRemove.bounds.width, height: data.cellToRemove.bounds.height)
            backgroundColor = .appGrey
            deleteRowAnimation = .left
        }
        
        animateBackgroundChange(forCell: data.cellToRemove, backgroundColor: backgroundColor) {
            UIView.animate(withDuration: 0.35, animations: {
                data.cellToRemove.frame = newFrame
            }) { _ in
                self.tableView.performBatchUpdates({
                    self.tableView.deleteRows(at: [indexPath], with: deleteRowAnimation)
                    if data.addedNewItem {
                        self.tableView.insertRows(at: [newIndexPath], with: .bottom)
                    }
                    self.tableManager.setSections(data.updatedSections)
                }, completion: { _ in
                    data.completionHandler()
                })
            }
        }
    }
    
    private func animateBackgroundChange(forCell cell: UITableViewCell, backgroundColor: UIColor, completion: @escaping () -> Void) {
        UIView.animate(withDuration: 0.1, animations: {
            cell.contentView.backgroundColor = backgroundColor
        }) { _ in
            completion()
        }
    }
}

// MARK: TableManagerDelegate
extension ConnectionsViewController: TableManagerDelegate {
    func didSwipeForRefresh() {
        viewModel.refreshData()
    }
    
    func loadMoreData() {
    }
    
    func tableViewDidScroll(_ scrollView: UIScrollView) {
    }
}

// MARK: - ScrollableContent
extension ConnectionsViewController: ScrollableContent {
    func scrollToTop() {
        tableManager.scrollToTop()
    }
}
