//  CommunityViewModel.swift
//  ParlorSocialClub
//
//  Created on 15/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import RxSwift

protocol CommunityViewLogic: BaseViewModelLogic, HasActivateMembershipViewLogic {
    
}

final class CommunityViewModel: BaseViewModel {
    var activateMembershipViewLogic: ActivateMembershipViewLogic?
    var setupActivateMembershipViewSubject: PublishSubject<ActivateMembershipViewModel> = PublishSubject<ActivateMembershipViewModel>()

    let analyticEventReferencedId: Int? = nil
    
    func initActivateMembershipViewLogic() {
        activateMembershipViewLogic = ActivateMembershipViewLogic(profileRepository: ProfileRepository.shared)

        activateMembershipViewLogic?.setupViewRelay
            .ignoreNil()
            .bind(to: setupActivateMembershipViewSubject)
            .disposed(by: disposeBag)

        activateMembershipViewLogic?.buttonClickedSubject
            .subscribe(onNext: { [unowned self] _ in
                // TODO: add navigation
            }).disposed(by: disposeBag)
    }
}

extension CommunityViewModel: CommunityViewLogic {
    func didTapActivateMembershipMoreButton() {
        activateMembershipViewLogic?.buttonClicked()
    }
}
