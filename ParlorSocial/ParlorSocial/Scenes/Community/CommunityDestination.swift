//  CommunityDestination.swift
//  ParlorSocialClub
//
//  Created on 16/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

enum CommunityDestination {
    case homeRsvpsTab
    case referFriend
    case memberProfile(id: Int)
    case myConnections
    case connectionRequests
}

extension CommunityDestination: Destination {
    var viewController: UIViewController {
        switch self {
        case .homeRsvpsTab:
            return UIViewController()
        case .referFriend:
            let vc = ReferFriendViewController()
            return vc
        case .memberProfile:
            let viewController = MemberProfileViewController()
            viewController.viewModel = MemberProfileViewModel(member: .mock)

            return viewController
        case .myConnections:
            return MyConnectionsViewController()
        case .connectionRequests:
            return ConnectionRequestsViewController()
        }
    }
}
