//  CommunityViewController.swift
//  ParlorSocialClub
//
//  Created on 15/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

class CommunityViewController: NewFilterableViewController {

    // MARK: - Properties

    override var observableSources: ObservableSources? { return viewModel }
    override var controllerTitle: String? { return Strings.Community.title.localized }
    // TODO: - Message of this onboarding type is not final 
    override var connectedOnboardingType: OnboardingType? { return .community }

    var activateMembershipView: ActivateMembershipView = ActivateMembershipView().manualLayoutable()

    private(set) lazy var viewModel: CommunityViewLogic = {
        return CommunityViewModel()
    }()

    private var pageViewControllers: [UIViewController]! // in future releases that tab will have tabs as page controllers
    
    private let pageController: UIPageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    private let containerView: UIView = UIView().manualLayoutable()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        statusBar?.backgroundColor = .clear
    }
    
    // MARK: - Setup
    
    private func setup() {
        setupActivateMembershipView()
        setupContainerView()
        setupPageViewController()
        setupTitleButton()
        viewModel.initActivateMembershipViewLogic()
    }
    
    private func setupContainerView() {
        view.addSubview(containerView)
        containerView.edgesToParent(anchors: [.leading, .trailing, .bottom])
        containerView.topAnchor.equal(to: activateMembershipView.bottomAnchor)
    }
    
    private func setupPageViewController() {
        addChild(viewController: pageController, inside: containerView)
        pageViewControllers = [ConnectionsViewController(router: topRouter)]
        changePage(to: CommunityPageType.connections.pageIndex)
    }
    
    private func setupTitleButton() {
        titleButton?.isSelected = true
    }
    
    // MARK: - Data binding
    
    override func observeViewModel() {
        super.observeViewModel()
        bindActivateMembershipView()
    }
    
    private func bindActivateMembershipView() {
        viewModel.setupActivateMembershipViewSubject
            .subscribe(onNext: { [unowned self] data in
                self.activateMembershipView.setup(with: data)
            }).disposed(by: disposeBag)
    }
    
    // MARK: - Actions
    
    private func changePage(to index: Int) {
        pageController.setViewControllers(
            [pageViewControllers[index]],
            direction: .forward,
            animated: true,
            completion: nil
        )
    }
}

extension CommunityViewController: TabBarChild {
    func didTouchTabItem() {
        if let controller = children.first as? UIPageViewController {
            (controller.viewControllers?.first as? ScrollableContent)?.scrollToTop()
            (controller.viewControllers?.first as? RefreshableView)?.refreshData()
        } else {
            (children.first { $0 is ScrollableContent } as? ScrollableContent)?.scrollToTop()
            (children.first { $0 is RefreshableView } as? RefreshableView)?.refreshData()
        }
    }
}

extension CommunityViewController: HasActivateMembershipView {
    func didTapActivateMembershipMoreButton() {
        viewModel.didTapActivateMembershipMoreButton()
    }
}
