//  CommunityPageType.swift
//  ParlorSocialClub
//
//  Created on 15/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

enum CommunityPageType: Int, CaseIterable {
    case connections = 0
    
    var pageIndex: Int { return self.rawValue }
}
