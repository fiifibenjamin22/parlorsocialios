//
//  ConnectionsAttendingVC.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 26/08/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

class ConnectionsAttendingVC: AppViewController {
    
    var analyticEventScreen: AnalyticEventScreen? = .myConnections
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }

    override var observableSources: ObservableSources? {
        return viewModel
    }

    private var tableManager: TableViewManager!

    private let viewModel: ConnectionsAttendingLogic = ConnectionsAttendingViewModel()
    private let tableView: UITableView = UITableView().manualLayoutable()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        bindViewModel()
        viewModel.loadData()
    }
    
    private func setup() {
        extendedLayoutIncludesOpaqueBars = true
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        adjustNavigationBar()
    }
    
    private func setupTableView() {
        tableView.backgroundColor = .white
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorInset = Constants.ConnectionsAttendingVC.tableSeparatorInsets

        tableManager = TableViewManager(tableView: tableView, delegate: self)
        tableManager.setupRefreshControl()

        view.addSubview(tableView)
        tableView.edgesToParent()
    }
    
    private func adjustNavigationBar() {
        title = "connections attending".localized.uppercased()
        statusBar?.backgroundColor = .white
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.setShadowBarEnabled(true)
        navigationController?.applyWhiteTheme()
    }
    
    // MARK: Data Binding

    private func bindViewModel() {

        observableSources?.alertDataObs.show(using: self).disposed(by: disposeBag)
        observableSources?.errorObs.handleWithAlerts(using: self).disposed(by: disposeBag)
        observableSources?.progressBarObs.showProgressBar(using: self, isDark: true).disposed(by: disposeBag)

        viewModel.destinationsObs
            .bind { [unowned self] in self.navigate(using: $0) }
            .disposed(by: disposeBag)

        viewModel.paginatedSectionsObs
            .bind { [unowned self] in self.handle(paginatedSectionsState: $0) }
            .disposed(by: disposeBag)
    }
    
    private func handle(paginatedSectionsState state: SectionPaginatedRepositoryState) {
        switch state {
        case .loading:
            tableManager.showLoadingFooter()
        case .paging(let elements):
            tableManager.setupWith(sections: elements)
            tableManager.showLoadingFooter()
        case .populated(let elements):
            tableManager.invalidateCache()
            tableManager.setupWith(sections: elements)
            tableManager.hideLoadingFooter()
        case .error:
            tableManager.hideLoadingFooter()
        }
    }
    
    private func navigate(using destination: ConnectionsAttendingDestination) {
        switch destination {
        case .referFriend, .memberProfile:
            router.push(destination: destination)
        }
    }
     
}

extension ConnectionsAttendingVC: TableManagerDelegate {
    
    func didSwipeForRefresh() {
        
    }
    
    func loadMoreData() {
        
    }
    
    func tableViewDidScroll(_ scrollView: UIScrollView) {
        
    }
}

// MARK: Constants

private extension Constants {
    enum ConnectionsAttendingVC {
        static let tableSeparatorInsets = UIEdgeInsets(side: 16)
    }
}
