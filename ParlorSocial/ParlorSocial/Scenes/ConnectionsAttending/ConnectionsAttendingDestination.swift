//
//  ConnectionsAttendingDestination.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 26/08/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

enum ConnectionsAttendingDestination {
    case referFriend
    case memberProfile(id: Int)
}

extension ConnectionsAttendingDestination: Destination {
    var viewController: UIViewController {
        switch self {
        case .referFriend:
            return ReferFriendViewController()
        case .memberProfile:
            let viewController = MemberProfileViewController()
            viewController.viewModel = MemberProfileViewModel(member: .mock)

            return viewController
        }
    }
}
