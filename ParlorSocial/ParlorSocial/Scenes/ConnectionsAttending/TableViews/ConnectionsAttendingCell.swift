//
//  ConnectionsAttendingCell.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 26/08/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

class ConnectionsAttendingCell: UITableViewCell {
    
    // MARK: - Properties

    private var requestId: Int?

    private var avatarImageView = UIImageView().manualLayoutable()
    private let nameLabel = ParlorLabel().manualLayoutable()
    private let titleLabel = ParlorLabel().manualLayoutable()
    private let descriptionLabel = ParlorLabel().manualLayoutable()
    private var detailImageView = UIImageView().manualLayoutable()

    // MARK: - Init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        contentView.backgroundColor = .white
    }

    // MARK: - Setup

    private func setup() {
        selectionStyle = .none
        contentView.backgroundColor = .white

        setupAvatarImageView()
        setupNameLabel()
    }

    private func setupAvatarImageView() {
        avatarImageView.apply {
            $0.clipsToBounds = true
            $0.layer.cornerRadius = Constants.AvatarImageView.height / 2
        }

        contentView.addSubview(avatarImageView)
        avatarImageView.edgesToParent(anchors: [.top, .leading, .bottom], insets: Constants.AvatarImageView.insets)
        avatarImageView.heightAnchor.equalTo(constant: Constants.AvatarImageView.height)
        avatarImageView.widthAnchor.equal(to: avatarImageView.heightAnchor)
    }

    private func setupNameLabel() {
        nameLabel.apply {
            $0.font = UIFont.custom(ofSize: Constants.FontSize.small, font: UIFont.Roboto.regular)
            $0.lineHeight = Constants.NameLabel.lineHeight
            $0.textColor = UIColor.appTextMediumBright
            $0.numberOfLines = Constants.NameLabel.numberOfLines
        }

        contentView.addSubview(nameLabel)
        contentView.addSubview(titleLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(detailImageView)
        
        nameLabel.topAnchor.constraint(equalTo: avatarImageView.topAnchor, constant: 0).isActive = true
        nameLabel.leftAnchor.equal(to: avatarImageView.rightAnchor, constant: Constants.NameLabel.leftMargin)
        nameLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
                
        titleLabel.apply {
            $0.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.light)
            $0.lineHeight = Constants.NameLabel.lineHeight
            $0.textColor = UIColor.appTextMediumBright
            $0.numberOfLines = Constants.NameLabel.numberOfLines
        }
        
        titleLabel.text = "Producer"
        titleLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 0).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: nameLabel.leftAnchor).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: nameLabel.rightAnchor).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        descriptionLabel.apply {
            $0.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.light)
            $0.lineHeight = Constants.NameLabel.lineHeight
            $0.textColor = UIColor.appTextMediumBright
            $0.numberOfLines = Constants.NameLabel.numberOfLines
        }
        
        descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 0).isActive = true
        descriptionLabel.leftAnchor.constraint(equalTo: titleLabel.leftAnchor).isActive = true
        descriptionLabel.rightAnchor.constraint(equalTo: titleLabel.rightAnchor).isActive = true
        descriptionLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        detailImageView.image = UIImage(named: "right-arrow")
        detailImageView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -16).isActive = true
        detailImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        detailImageView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        detailImageView.widthAnchor.constraint(equalToConstant: 20).isActive = true
    }

}

// MARK: - ConfigurableCell
extension ConnectionsAttendingCell: ConfigurableCell {
    func configure(with model: ConnectionsAttending) {
        requestId = model.requestId
        avatarImageView.image = UIImage(named: "\(model.imageUrl ?? "")")//.getImage(from: model.imageURL)
        nameLabel.text = model.name
        titleLabel.text = model.title
        descriptionLabel.text = model.description
    }
}

private extension Constants {
    enum AcceptButton {
        static let height: CGFloat = 30
        static let width: CGFloat = 94
        static let rightMargin: CGFloat = -16
    }
    enum AvatarImageView {
        static let height: CGFloat = 50
        static let insets: UIEdgeInsets = UIEdgeInsets(top: 20, left: 16, bottom: 40, right: 0)
    }
    enum RejectButton {
        static let height: CGFloat = 30
        static let leftMargin: CGFloat = 15
        static let rightMargin: CGFloat = -15
    }
    enum SeparatorView {
        static let insets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
    }
    enum NameLabel {
        static let leftMargin: CGFloat = 13
        static let bottomMargin: CGFloat = -20
        static let centerYMargin: CGFloat = -8
        static let lineHeight: CGFloat = 22
        static let numberOfLines: Int = 2
    }
}


import Foundation

struct ConnectionsAttending: Codable, Equatable {
    let requestId: Int
    let memberId: Int
    let name: String
    let title: String
    let description: String
    let imageUrl: String?

    var imageURL: URL? {
        return imageUrl?.url
    }
}
