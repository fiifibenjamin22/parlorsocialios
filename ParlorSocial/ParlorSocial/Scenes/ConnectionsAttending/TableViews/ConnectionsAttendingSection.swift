//
//  ConnectionsAttendingHeader.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 26/08/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit
import Foundation

final class ConnectionsAttendingSection: BasicTableSection<ConnectionsAttending, ConnectionsAttendingCell> {

//    override init(
//        items: [ConnectionRequest],
//        itemSelector: @escaping ((ConnectionRequest) -> Void)
//    ) {
//        super.init(items: items, itemSelector: itemSelector)
//    }
    
    override var headerType: UITableViewHeaderFooterView.Type? { return ConnectionsAttendingHeader.self }
    
    override func configure(header: UITableViewHeaderFooterView, in section: Int) {
        guard let sectionHeader = header as? ConnectionsAttendingHeader else {return}
        
        sectionHeader.apply {_ in }
    }

    override func configure(cell: UITableViewCell, at indexPath: IndexPath) {
        guard let cell = cell as? ConnectionsAttendingCell else {return}
        super.configure(cell: cell, at: indexPath)
    }

    static func make(
        for connections: [ConnectionsAttending],
        itemSelector: @escaping (ConnectionsAttending) -> Void
    ) -> ConnectionsAttendingSection {
        return ConnectionsAttendingSection(
            items: connections,
            itemSelector: itemSelector
        )
    }
}
