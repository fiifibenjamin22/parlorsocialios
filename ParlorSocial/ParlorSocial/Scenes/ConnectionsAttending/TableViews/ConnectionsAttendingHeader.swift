//
//  ConnectionsAttendingHeader.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 28/08/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

class ConnectionsAttendingHeader: UITableViewHeaderFooterView {
    
    private var avatarImageView = UIImageView().manualLayoutable()
    private let title = ParlorLabel().manualLayoutable()
    private let date = ParlorLabel().manualLayoutable()
    private let time = ParlorLabel().manualLayoutable()

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        self.contentView.backgroundColor = .black
        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func initialize() {
        setupAvatarImageView()
        setupNameLabel()
    }
    
    //TODO: Remove all static strings
    private func setupAvatarImageView() {
        avatarImageView.apply {
            $0.clipsToBounds = true
        }

        contentView.addSubview(avatarImageView)
        avatarImageView.edgesToParent(anchors: [.top, .leading, .bottom], insets: Constants.AvatarImageView.insets)
        avatarImageView.heightAnchor.equalTo(constant: Constants.AvatarImageView.height)
        avatarImageView.widthAnchor.equal(to: avatarImageView.heightAnchor)
        avatarImageView.image = UIImage(named: "happenings-empty-data")
    }

    private func setupNameLabel() {
        title.apply {
            $0.font = UIFont.custom(ofSize: Constants.FontSize.small, font: UIFont.Editor.bold)
            $0.lineHeight = Constants.NameLabel.lineHeight
            $0.textColor = UIColor.white
            $0.numberOfLines = Constants.NameLabel.numberOfLines
        }

        contentView.addSubview(title)
        contentView.addSubview(date)
        contentView.addSubview(time)
        
        title.text = "Author Conversation with Yuval Noah Harari"
        title.topAnchor.constraint(equalTo: avatarImageView.topAnchor, constant: 0).isActive = true
        title.leftAnchor.equal(to: avatarImageView.rightAnchor, constant: Constants.NameLabel.leftMargin)
        title.heightAnchor.constraint(equalToConstant: 20).isActive = true
                
        date.apply {
            $0.font = UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.regular)
            $0.lineHeight = Constants.NameLabel.lineHeight
            $0.textColor = UIColor.white
            $0.numberOfLines = Constants.NameLabel.numberOfLines
        }
        
        date.text = "FEB 3rd"
        date.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 8).isActive = true
        date.leftAnchor.constraint(equalTo: title.leftAnchor).isActive = true
        date.widthAnchor.constraint(equalToConstant: 100).isActive = true
        date.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        time.apply {
            $0.font = UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.regular)
            $0.lineHeight = Constants.NameLabel.lineHeight
            $0.textColor = UIColor.white
            $0.numberOfLines = Constants.NameLabel.numberOfLines
        }
        
        time.text = "9:00PM -11:00PM"
        time.topAnchor.constraint(equalTo: date.topAnchor, constant: 0).isActive = true
        time.leftAnchor.constraint(equalTo: date.rightAnchor).isActive = true
        time.rightAnchor.constraint(equalTo: title.rightAnchor).isActive = true
        time.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
}

private extension Constants {
    enum AcceptButton {
        static let height: CGFloat = 30
        static let width: CGFloat = 94
        static let rightMargin: CGFloat = -16
    }
    enum AvatarImageView {
        static let height: CGFloat = 60
        static let insets: UIEdgeInsets = UIEdgeInsets(top: 20, left: 16, bottom: 20, right: 0)
    }
    enum RejectButton {
        static let height: CGFloat = 30
        static let leftMargin: CGFloat = 15
        static let rightMargin: CGFloat = -15
    }
    enum SeparatorView {
        static let insets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
    }
    enum NameLabel {
        static let leftMargin: CGFloat = 16
        static let bottomMargin: CGFloat = -20
        static let centerYMargin: CGFloat = -8
        static let lineHeight: CGFloat = 22
        static let numberOfLines: Int = 2
    }
}
