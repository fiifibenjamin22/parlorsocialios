//
//  ConnectionsAttendingViewModel.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 26/08/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import RxSwift
import RxCocoa

protocol ConnectionsAttendingLogic: BaseViewModelLogic {
    var destinationsObs: Observable<ConnectionsAttendingDestination> { get }
    var paginatedSectionsObs: Observable<SectionPaginatedRepositoryState> { get }
    func refresh()
    func loadMore()
    func loadData()
}

final class ConnectionsAttendingViewModel: BaseViewModel {

    typealias ObservableApiResponse = Observable<ApiResponse<ConnectionsAttendings.Get.Response>>

    var analyticEventReferencedId: Int?
    
    private var connectionsAttendingModel: ConnectionsAttendings.Get.Request = ConnectionsAttendings.Get.Request.initial
        
    private let connectionsAttendingRepository: AttendingRepositoryProtocol = AttendingsRepository.shared
    private let destinationRelay: PublishRelay<ConnectionsAttendingDestination> = PublishRelay()
    private let loadDataRelay = BehaviorRelay(value: 0)
    private let paginatedDataRelay = BehaviorRelay<PaginatedData>(value: PaginatedData(sections: [], metadata: ListMetadata.initial))
    private let paginatedSectionsRelay = BehaviorRelay<SectionPaginatedRepositoryState>(value: .loading)

    override init() {
        super.init()
        self.initFlow()
    }
    
    private func initFlow() {
        bindLoadData()
    }
    
    //TODO: Remove all static strings
    public func loadData() {
        
        let dataObject = ConnectionsAttending(requestId: 1, memberId: 5, name: "Sarah Kim", title: "Producer Director", description: "Group PMX", imageUrl: "happenings-empty-data")
        let dataObject1 = ConnectionsAttending(requestId: 1, memberId: 5, name: "Jahmil Eady", title: "Clinical Laboratory Scientist", description: "Columbia University Medical Center", imageUrl: "happenings-empty-data")
        let dataObject2 = ConnectionsAttending(requestId: 1, memberId: 5, name: "Kimberly Hamroff", title: "Samantha Plesser", description: "Group PMX", imageUrl: "happenings-empty-data")
        let connectionSection = ConnectionsAttendingSection(items: [dataObject,dataObject1,dataObject2])
        paginatedSectionsRelay.accept(.populated(elements: [connectionSection]))
    }
    
    private func bindLoadData() {
        
        //TODO: Uncomment for apiservice yet to implement
        /*
        loadDataRelay
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .userInteractive))
            .setLoadingStateOnSubscribe(with: paginatedSectionsRelay)
            .skipOnListPopulated(with: paginatedDataRelay)
            .flatMapLatest { [unowned self] _ -> ObservableApiResponse in
                return self.connectionsAttendingRepository.getConnectionRequests(
                    using: self.connectionsAttendingModel
                )
            }
            .mapResponseToTableSection(
                with: paginatedDataRelay,
                sectionStateRelay: paginatedSectionsRelay,
                sectionMaker: { [unowned self] in self.makeSections(for: $0) }
            )
            .changePaginatedState(with: paginatedSectionsRelay)
            .doOnNext { [unowned self] _ in self.connectionsAttendingModel.increasePage() }
            .bind(to: paginatedDataRelay)
            .disposed(by: disposeBag)
        */
    }

    private func makeSections(for connectionRequests: [ConnectionsAttending]) -> [TableSection] {
        return [
            ConnectionsAttendingSection.make(
                for: connectionRequests,
                itemSelector: { [unowned self] in self.didSelectConnectionRequest($0) }
            )
        ]
    }

    private func didSelectConnectionRequest(_ model: ConnectionsAttending) {
        self.destinationRelay.accept(.memberProfile(id: model.memberId ))
    }
}

extension ConnectionsAttendingViewModel: ConnectionsAttendingLogic {
    
    var destinationsObs: Observable<ConnectionsAttendingDestination> {
        return destinationRelay.asObservable()
    }
    
    var paginatedSectionsObs: Observable<SectionPaginatedRepositoryState> {
        return paginatedSectionsRelay.asObservable()
    }
    
    func refresh() {
        
    }
    
    func loadMore() {
        
    }
}
