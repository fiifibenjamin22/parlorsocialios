//
//  OnboardingViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 18/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

@objc protocol OnboardingViewControllerDelegate: class {
    func onboardingWasClosed()
}

class OnboardingViewController: AppViewController {

    // MARK: - Views
    private(set) var containerView: UIView!
    private(set) var iconView: UIImageView!
    private(set) var titleLabel: UILabel!
    private(set) var messageLabel: UILabel!
    private(set) var bottomButtonsStack: UIStackView!
    private(set) var gotItButton: UIButton!
    
    // MARK: - Properties
    let analyticEventScreen: AnalyticEventScreen? = .tutorial
    private var type: OnboardingType!
    private var viewModel: BaseViewModelLogic!
    weak var delegate: OnboardingViewControllerDelegate?
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }

    // MARK: - Initialization
    init(witih type: OnboardingType) {
        self.type = type
        super.init(nibName: nil, bundle: nil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    private func setup() {
        modalPresentationStyle = .overFullScreen
        viewModel = BaseViewModelClass()
    }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        statusBar?.backgroundColor = .clear
    }
    
    @objc private func didTapGotIt() {
        Config.setOnboardingShown(for: type)
        dismiss(animated: true, completion: delegate?.onboardingWasClosed)
    }
}

// MARK: - Private methods
extension OnboardingViewController {

    private func setupViews() {
        let builder = OnboardingViewBuilder(controller: self)
        containerView = builder.buildContainerView()
        iconView = builder.buildIconImage()
        titleLabel = builder.buildTitleLabel()
        messageLabel = builder.buildMessageLabel()
        bottomButtonsStack = builder.buildBottomButtonsStackView()
        gotItButton = builder.buildGotItButton()
        builder.setupViews()
    }

    private func setupPresentationLogic() {
        iconView.image = type.appearance.icon
        titleLabel.text = type.appearance.title
        messageLabel.text = type.appearance.message
        gotItButton.addTarget(self, action: #selector(didTapGotIt), for: .touchUpInside)
    }

}
