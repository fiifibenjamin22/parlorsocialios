//
//  OnboardingType.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 18/04/2019.
//

import Foundation
import UIKit

enum OnboardingType: String, CaseIterable {
    case home
    case announcements
    case profile
    case guestList
    case mixers
    case happenings
    case rsvps
    case community

    struct Appearance {
        let icon: UIImage
        let title: String
        let message: String
    }

    var appearance: Appearance {
        switch self {
        case .home:
            return Appearance(icon: Icons.Onboarding.home, title: Strings.Onboarding.Home.title.localized, message: Strings.Onboarding.Home.message.localized)
        case .announcements:
            return Appearance(icon: Icons.Onboarding.announcements, title: Strings.Onboarding.Announcements.title.localized, message: Strings.Onboarding.Announcements.message.localized)
        case .profile:
            return Appearance(icon: Icons.Onboarding.profile, title: Strings.Onboarding.Profile.title.localized, message: Strings.Onboarding.Profile.message.localized)
        case .guestList:
            return Appearance(icon: Icons.Onboarding.guestList, title: Strings.Onboarding.GuestList.title.localized, message: Strings.Onboarding.GuestList.message.localized)
        case .mixers:
            return Appearance(icon: Icons.Onboarding.mixers, title: Strings.Onboarding.Mixers.title.localized, message: Strings.Onboarding.Mixers.message.localized)
        case .happenings:
            return Appearance(icon: Icons.Onboarding.happenings, title: Strings.Onboarding.Happenings.title.localized, message: Strings.Onboarding.Happenings.message.localized)
        case .rsvps:
            return Appearance(icon: Icons.Onboarding.rsvps, title: Strings.Onboarding.Rsvps.title.localized, message: Strings.Onboarding.Rsvps.message.localized)
        case .community:
            return Appearance(icon: Icons.Onboarding.community, title: Strings.Onboarding.Community.title.localized, message: Strings.Onboarding.Community.message.localized)
        }
    }
}
