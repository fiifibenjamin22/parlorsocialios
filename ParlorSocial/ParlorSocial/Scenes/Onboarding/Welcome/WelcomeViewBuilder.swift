//
//  WelcomeViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 19/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class WelcomeViewBuilder {

    private unowned let controller: WelcomeViewController
    private var view: UIView! { return controller.view }

    init(controller: WelcomeViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension WelcomeViewBuilder {

    private func setupProperties() {
        controller.apply {
            $0.getStartedButton.setTitle(Strings.Welcome.getStarted.localized.uppercased(), for: .normal)
            $0.titleLabel.text = Strings.Welcome.title.localized
            $0.messageLabel.text = Strings.Welcome.message.localized
        }
    }

    private func setupHierarchy() {
        controller.apply {
            view.addSubviews([$0.backgroundImage, $0.middleContent])
            
            $0.middleContent.addSubviews([
                $0.titleLabel,
                $0.messageLabel,
                $0.getStartedButton
                ])
        }
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.backgroundImage.edgesToParent()
            
            $0.middleContent.centerXAnchor.equal(to: view.centerXAnchor)
            $0.middleContent.centerYAnchor.equal(to: view.centerYAnchor)
            $0.middleContent.edgesToParent(anchors: [.leading, .trailing], padding: Constants.horizontalMargin)
            
            $0.titleLabel.edgesToParent(anchors: [.top], padding: Constants.Welcome.titleTop)
            $0.titleLabel.edgesToParent(anchors: [.leading, .trailing])
            
            $0.messageLabel.topAnchor.equal(to: $0.titleLabel.bottomAnchor, constant: Constants.Margin.tiny * 2)
            $0.messageLabel.edgesToParent(anchors: [.leading, .trailing])
            
            $0.getStartedButton.topAnchor.equal(to: $0.messageLabel.bottomAnchor, constant: Constants.Welcome.buttonTop)
            $0.getStartedButton.bottomAnchor.equal(to: $0.middleContent.bottomAnchor, constant: -Constants.Welcome.buttonBottom)
            $0.getStartedButton.centerXAnchor.equal(to: $0.middleContent.centerXAnchor)
            $0.getStartedButton.widthAnchor.equalTo(constant: Constants.buttonDefaultWidth)
            $0.getStartedButton.heightAnchor.equalTo(constant: Constants.buttonHeight)
        }
    }

}

// MARK: - Public build methods
extension WelcomeViewBuilder {

    func buildBackgroundImageView() -> UIImageView {
        return UIImageView(image: Icons.Welcome.background).manualLayoutable().apply {
            $0.contentMode = .scaleAspectFill
            $0.clipsToBounds = true
        }
    }
    
    func buildContentView() -> UIView {
        return UIView().manualLayoutable().apply {
            $0.backgroundColor = .white
        }
    }
    
    func buildTitleLabel() -> ParlorLabel {
        return ParlorLabel.styled(withTextColor: .black, withFont: UIFont.custom(ofSize: 28, font: UIFont.Editor.medium), alignment: .center, numberOfLines: 1)
    }
    
    func buildMessageLabel() -> ParlorLabel {
        return ParlorLabel.styled(withTextColor: UIColor.black.withAlphaComponent(0.72), withFont: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.regular), alignment: .center, numberOfLines: 1)
    }
    
    func buildGetStartedButton() -> UIButton {
        return RoundedButton().manualLayoutable().apply {
            $0.layer.borderColor = UIColor.appSeparator.cgColor
            $0.layer.borderWidth = 1
            $0.backgroundColor = .white
            $0.setTitleColor(.appTextMediumBright, for: .normal)
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium)
            $0.applyTouchAnimation()
        }
    }

}

extension Constants {
    
    enum Welcome {
        static let titleTop: CGFloat = 77
        static let buttonTop: CGFloat = 43
        static let buttonBottom: CGFloat = 71
    }
    
}
