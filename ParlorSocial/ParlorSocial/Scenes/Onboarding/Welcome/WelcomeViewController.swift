//
//  WelcomeViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 19/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {
    
    private(set) var backgroundImage: UIImageView!
    private(set) var middleContent: UIView!
    private(set) var titleLabel: ParlorLabel!
    private(set) var messageLabel: ParlorLabel!
    private(set) var getStartedButton: UIButton!

    // MARK: - Initialization
    init() {
        super.init(nibName: nil, bundle: nil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() { }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
    }

}

// MARK: - Private methods
extension WelcomeViewController {

    private func setupViews() {
        let builder = WelcomeViewBuilder(controller: self)
        self.backgroundImage = builder.buildBackgroundImageView()
        self.middleContent = builder.buildContentView()
        self.titleLabel = builder.buildTitleLabel()
        self.messageLabel = builder.buildMessageLabel()
        self.getStartedButton = builder.buildGetStartedButton()
        builder.setupViews()
    }

    private func setupPresentationLogic() {

    }

}
