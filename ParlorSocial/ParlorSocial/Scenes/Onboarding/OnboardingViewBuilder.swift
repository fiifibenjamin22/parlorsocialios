//
//  OnboardingViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 18/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class OnboardingViewBuilder {

    private unowned let controller: OnboardingViewController
    private var view: UIView! { return controller.view }

    init(controller: OnboardingViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension OnboardingViewBuilder {

    private func setupProperties() {
        view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        controller.gotItButton.setTitle(Strings.Onboarding.gotIt.localized.uppercased(), for: .normal)
    }

    private func setupHierarchy() {
        controller.apply {
            view.addSubviews([$0.containerView, $0.bottomButtonsStack])
            $0.containerView.addSubviews([
                $0.iconView,
                $0.titleLabel,
                $0.messageLabel
                ])
            $0.bottomButtonsStack.addArrangedSubviews([$0.gotItButton])
        }
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.containerView.constraintByCenteringVerticallyBeetween(view.safeAreaLayoutGuide.topAnchor, and: $0.bottomButtonsStack.topAnchor)
            $0.containerView.edgesToParent(anchors: [.leading, .trailing])
            
            $0.iconView.edgesToParent(anchors: [.top])
            $0.iconView.centerXAnchor.equal(to: $0.containerView.centerXAnchor)
            
            $0.titleLabel.edgesToParent(anchors: [.leading, .trailing], padding: Constants.horizontalMargin)
            $0.titleLabel.topAnchor.equal(to: $0.iconView.bottomAnchor, constant: Constants.Onboarding.titleTop)
            
            $0.messageLabel.edgesToParent(anchors: [.leading, .trailing], padding: Constants.horizontalMargin)
            $0.messageLabel.edgesToParent(anchors: [.bottom])
            $0.messageLabel.topAnchor.equal(to: $0.titleLabel.bottomAnchor, constant: Constants.Onboarding.messageTop)
            
            $0.bottomButtonsStack.centerXAnchor.equal(to: view.centerXAnchor)
            $0.bottomButtonsStack.bottomAnchor.equal(to: view.safeAreaLayoutGuide.bottomAnchor, constant: -Constants.Onboarding.buttonBottom)
            
            $0.gotItButton.heightAnchor.equalTo(constant: Constants.Onboarding.buttonHeight)
            $0.gotItButton.widthAnchor.equalTo(constant: Constants.Onboarding.buttonWidth)
        }
    }

}

// MARK: - Public build methods
extension OnboardingViewBuilder {
    
    func buildContainerView() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildIconImage() -> UIImageView {
        return UIImageView().manualLayoutable().apply {
            $0.contentMode = .scaleAspectFit
        }
    }
    
    func buildTitleLabel() -> ParlorLabel {
        return ParlorLabel.styled(withTextColor: UIColor.white, withFont: UIFont.custom(ofSize: 24, font: UIFont.Roboto.medium), alignment: .center, numberOfLines: 1).manualLayoutable()
    }
    
    func buildMessageLabel() -> ParlorLabel {
        return ParlorLabel.styled(withTextColor: UIColor.white, withFont: UIFont.custom(ofSize: Constants.FontSize.hintSize, font: UIFont.Roboto.regular), alignment: .center, numberOfLines: 0)
            .manualLayoutable()
            .apply {
                $0.lineHeight = Constants.lineHeightBig
            }
    }
    
    func buildBottomButtonsStackView() -> UIStackView {
        return UIStackView(
            axis: .vertical,
            spacing: 13,
            alignment: .fill,
            distribution: .fillEqually
        )
    }
    
    func buildGotItButton() -> UIButton {
        return RoundedButton().manualLayoutable().apply {
            $0.backgroundColor = .white
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium)
            $0.setTitleColor(UIColor.rsvpBorder, for: .normal)
            $0.applyTouchAnimation()
        }
    }
    
    func buildRemindMeLaterButton() -> UIButton {
        return RoundedButton().manualLayoutable().apply {
            $0.backgroundColor = .clear
            $0.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium)
            $0.setTitleColor(.white, for: .normal)
            $0.applyTouchAnimation()
        }
    }
}

private extension Constants {
    
    enum Onboarding {
        static let titleTop: CGFloat = 45
        static let messageTop: CGFloat = 18
        static let buttonBottom: CGFloat = 55
        static let buttonWidth: CGFloat = 196
        static let buttonHeight: CGFloat = 50
    }
    
}
