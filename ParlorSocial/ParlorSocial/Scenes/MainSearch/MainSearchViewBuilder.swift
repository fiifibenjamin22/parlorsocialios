//
//  MainSearchViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 18/07/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class MainSearchViewBuilder {

    private unowned let controller: MainSearchViewController
    private var view: UIView! { return controller.view }

    init(controller: MainSearchViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension MainSearchViewBuilder {
    
    private func setupProperties() {
        view.backgroundColor = .white
        controller.searchTopBar.addShadow(withOpacity: 0.1, radius: 10)
    }
    
    private func setupHierarchy() {
        controller.apply {
            [$0.tableView, $0.historyView, $0.searchTopBar].addTo(parent: view)
        }
    }
    
    private func setupAutoLayout() {
        controller.searchTopBar.apply {
            $0.topAnchor.equal(to: view.safeAreaLayoutGuide.topAnchor)
            $0.edgesToParent(anchors: [.leading, .trailing])
            $0.heightAnchor.equalTo(constant: Constants.MainSearch.searchBarHeight)
        }
        
        controller.tableView.apply {
            $0.edgesToParent()
        }
        
        controller.historyView.apply {
            $0.topAnchor.equal(to: controller.searchTopBar.bottomAnchor)
            $0.edgesToParent(anchors: [.leading, .trailing, .bottom])
        }
    }
    
}

// MARK: - Public build methods
extension MainSearchViewBuilder {
    
    func buildView() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildSearchTopBar(_ delegate: SearchTopBarViewDelegate) -> SearchTopBarView {
        return SearchTopBarView(delegate: delegate).manualLayoutable()
    }
    
    func buildTableView() -> UITableView {
        return UITableView().manualLayoutable().apply {
            $0.separatorStyle = .none
            var topInset: CGFloat = 0
            topInset = 20 + Constants.MainSearch.searchBarHeight

            $0.contentInset = UIEdgeInsets.margins(top: topInset, bottom: 20)
        }
    }
    
    func buildSearchHistoryView(_ delegate: SearchHistoryViewDelegate) -> SearchHistoryView {
        return SearchHistoryView(delegate: delegate).manualLayoutable()
    }
    
}

fileprivate extension Constants {
    enum MainSearch {
        static let searchBarHeight: CGFloat = 44.0
    }
}
