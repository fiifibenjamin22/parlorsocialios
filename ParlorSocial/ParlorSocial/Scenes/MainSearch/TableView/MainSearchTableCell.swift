//
//  MainSearchTableCell.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 19/07/2019.
//

import UIKit

class MainSearchTableCell: UITableViewCell {
    
    // MARK: - Views
    lazy var nameLabel: ParlorLabel = {
        return ParlorLabel.styled(
            withTextColor: .textFieldUnderline,
            withFont: UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.regular),
            alignment: .left,
            numberOfLines: 1)
            .manualLayoutable()
    }()
    
    // MARK: - Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        selectionStyle = .none
        contentView.addSubview(nameLabel)
        setupAutoLayout()
    }
    
    private func setupAutoLayout() {
        nameLabel.apply {
            $0.edgesToParent(insets: UIEdgeInsets.margins(
                top: 12,
                left: Constants.Margin.standard,
                bottom: 12,
                right: Constants.Margin.standard)
            )
        }
    }
}

extension MainSearchTableCell: ConfigurableCell {
    
    typealias Model = SearchData
    
    func configure(with model: SearchData) {
        nameLabel.text = model.name
        nameLabel.textColor = model.type == .notFound ? .appTextLight : .textFieldUnderline
        nameLabel.textAlignment = model.type == .notFound ? .center : .left
    }
}

