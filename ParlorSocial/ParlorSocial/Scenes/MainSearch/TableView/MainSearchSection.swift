//
//  MainSearchSection.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 19/07/2019.
//

import UIKit
import RxSwift

class MainSearchSection: BasicTableSection<SearchData, MainSearchTableCell> {
    
    override init(items: [SearchData], itemSelector: @escaping ((SearchData) -> Void) = { _ in }) {
        super.init(items: items, itemSelector: itemSelector)
    }
    
}
