//
//  MainSearchViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 18/07/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol MainSearchLogic: BaseViewModelLogic {
    var loadSearchRelay: PublishRelay<String> { get }
    var sectionsObs: Observable<[TableSection]> { get }
    var tableViewStateObs: Observable<MainSearchState> { get }
    var destinationsObs: Observable<SearchDestination> { get }
}

class MainSearchViewModel: BaseViewModel {
    
    // MARK: - Properties
    let analyticEventReferencedId: Int? = nil
    static private let minSearchCount: Int = 3
    static private let debounceInterval: RxTimeInterval = .milliseconds(300)
    
    private let searchRepository = SearchRepository.shared
    private let activationsRepository = ActivationsRepository.shared
    private let announcementsRepository = AnnouncementsRepository.shared

    private let destinationsSubject: PublishSubject<SearchDestination> = PublishSubject()
    private let searchDataSubject: ReplaySubject<[SearchData]> = ReplaySubject.create(bufferSize: 1)
    private let tableViewState = PublishRelay<MainSearchState>()

    let loadSearchRelay = PublishRelay<String>()
    
    var requestDisposeBag = DisposeBag()

    // MARK: - Initialization
    override init() {
        super.init()
        initFlow()
    }
}

// MARK: Private logic
private extension MainSearchViewModel {
    private func initFlow() {
        subscribeToSearchRequest()
        
        loadSearchRelay
            .filter { $0.isEmpty }
            .doOnNext { [unowned self] _ in self.subscribeToSearchRequest() }
            .changeSearchState(on: tableViewState, searchState: .history)
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    // MARK: - It is needed in this case: when user click clean text button during api request, the request should be stopped
    func subscribeToSearchRequest() {
        requestDisposeBag = DisposeBag()
        
        loadSearchRelay
            .filter { !$0.isEmpty }
            .filter { $0.count >= MainSearchViewModel.minSearchCount}
            .debounce(MainSearchViewModel.debounceInterval, scheduler: MainScheduler.asyncInstance)
            .doOnNext { text in Config.addToSearchHistory(item: text) }
            .changeSearchState(on: tableViewState, searchState: .loading)
            .flatMapFirst { [unowned self] searchText in
                self.searchRepository.search(withPhrase: searchText)
            }
            .changeSearchState(on: tableViewState, searchState: .results)
            .subscribe(onNext: { [weak self] response in
                self?.handleSearchResponse(response)
            }).disposed(by: requestDisposeBag)
    }
    
    private func handleSearchResponse(_ response: SearchApiResponse) {
        switch response {
        case .success(let response):
            if !response.data.isEmpty {
                searchDataSubject.onNext(response.data)
            } else {
                searchDataSubject.onNext([SearchData(
                    id: 0,
                    name: Strings.Search.notFound.localized,
                    type: .notFound)
                    ])
            }
        case .failure(let error):
            errorSubject.onNext(error)
        }
    }
    
    private func openRsvpDetails(forActivationWithId id: Int) {
        activationsRepository.getActivation(withId: id)
            .showingProgressBar(with: self)
            .filterMapWithErrorHandling(errorSubject: self.errorSubject)
            .doOnNext { [unowned self] activation in
                self.destinationsSubject.onNext(.rsvpDetails(activation: activation))
            }.subscribe().disposed(by: disposeBag)
    }
    
    private func openAnnouncement(withId id: Int) {
        destinationsSubject.onNext(.announcementDetails(announcementId: id))
    }
    
    private func openActivationDetails(withId id: Int) {
        activationsRepository.getActivation(withId: id)
            .showingProgressBar(with: self)
            .filterMapWithErrorHandling(errorSubject: self.errorSubject)
            .doOnNext { [unowned self] activation in
                self.checkActivationDetailsCanBeOpened(activation)
            }.subscribe().disposed(by: disposeBag)
    }
    
    private func checkActivationDetailsCanBeOpened(_ activation: Activation) {
        let openingType = ActivationDetailsNavigationProvider.getOpeningActivationDetailsType(with: activation)
        switch openingType {
        case .activationDetailsVC, .addProfilePhotoVC, .upgradeToPremiumForStandard:
            self.destinationsSubject.onNext(.activationDetails(activation: activation, openingType: openingType, navigationSource: .others))
        case .startedAlert:
            let alertData = AlertData.fromSimpleMessage(message: Strings.Activation.startedAlert.localized, sourceName: .activationHasAlreadyStarted)
            self.alertDataSubject.onNext(alertData)
        }
    }
}

// MARK: Interface logic methods
extension MainSearchViewModel: MainSearchLogic {
    
    var destinationsObs: Observable<SearchDestination> {
        return destinationsSubject.asObservable()
    }
    
    var tableViewStateObs: Observable<MainSearchState> {
        return tableViewState.asObservable()
    }
    
    var sectionsObs: Observable<[TableSection]> {
        return searchDataSubject.map { [unowned self] in [MainSearchSection(
            items: $0,
            itemSelector: self.searchResultSelector)] }
    }
    
    func searchResultSelector(data: SearchData) {
        switch data.type {
        case .activation:
            openActivationDetails(withId: data.id)
        case .rsvpdActivation:
            openRsvpDetails(forActivationWithId: data.id)
        case .announcement:
            openAnnouncement(withId: data.id)
        case .notFound:
            break
        }
        
        if data.type != .notFound {
            AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.SearchScreen.listItem.rawValue)
        }
    }
    
}
