//
//  MainSearchViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 18/07/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

class MainSearchViewController: AppViewController {
    
    // MARK: - Views
    private(set) var searchTopBar: SearchTopBarView!
    private(set) var contentView: UIView!
    private(set) var tableView: UITableView!
    private(set) var historyView: SearchHistoryView!

    // MARK: - Properties
    let analyticEventScreen: AnalyticEventScreen? = .search
    private var viewModel: MainSearchLogic!
    private var tableManager: TableViewManager!
    
    override var observableSources: ObservableSources? {
        return viewModel
    }
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }
    
    // MARK: - Initialization
    init() {
        super.init(nibName: nil, bundle: nil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() {
        modalTransitionStyle = .crossDissolve
        self.viewModel = MainSearchViewModel()
    }
    
    override func observeViewModel() {
        super.observeViewModel()
        
        viewModel
            .tableViewStateObs
            .subscribe(onNext: { [unowned self] searchState in
                self.handle(searchState: searchState)
            })
            .disposed(by: disposeBag)
        
        viewModel
            .sectionsObs
            .doOnNext { [unowned self] sections in
                self.tableManager.setupWith(sections: sections)
            }.subscribe().disposed(by: disposeBag)
        
        viewModel
            .destinationsObs
            .doOnNext { [unowned self] destintaion in
                self.handleDestination(destintaion)
            }.subscribe().disposed(by: disposeBag)
    }
    
    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        adjustStatusBar()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
}

// MARK: - Private methods
extension MainSearchViewController {

    private func setupViews() {
        let builder = MainSearchViewBuilder(controller: self)
        searchTopBar = builder.buildSearchTopBar(self)
        contentView = builder.buildView()
        tableView = builder.buildTableView()
        historyView = builder.buildSearchHistoryView(self)

        builder.setupViews()
    }

    private func setupPresentationLogic() {
        tableManager = TableViewManager(tableView: tableView, delegate: nil)
        tableManager.hideLoadingFooter()
        historyView.setup(with: Config.searchHistory)
    }
    
    private func handle(searchState: MainSearchState) {
        switch searchState {
        case .loading:
            historyView.isHidden = true
            tableManager.removeAllItems()
            tableManager.showLoadingFooter()
        case .history:
            historyView.isHidden = false
            tableManager.removeAllItems()
            tableManager.hideLoadingFooter()
            historyView.setup(with: Config.searchHistory)
        case .results:
            historyView.isHidden = true
            tableManager.hideLoadingFooter()
        }
    }
    
    private func adjustStatusBar() {
        statusBar?.backgroundColor = .white
    }
    
    private func handleDestination(_ destination: SearchDestination) {
        router.push(destination: destination)
    }
 
}

// MARK: - SearchTopBarViewDelegate
extension MainSearchViewController: SearchTopBarViewDelegate {
    func didTapCancel() {
        navigationController?.dismiss()
    }
    
    func didSearchTextChange(text: String) {
        viewModel.loadSearchRelay.accept(text)
    }
}

// MARK: - SearchHistoryViewDelegate
extension MainSearchViewController: SearchHistoryViewDelegate {
    
    func didTapItem(withQuery query: String) {
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.SearchScreen.historyItem.rawValue)
        searchTopBar.searchTextField.text = query
        searchTopBar.cleanTextButton.isHidden = false
        viewModel.loadSearchRelay.accept(query)
    }
    
}
