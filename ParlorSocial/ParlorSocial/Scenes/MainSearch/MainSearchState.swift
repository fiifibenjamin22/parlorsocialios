//
//  MainSearchState.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 22/07/2019.
//

import Foundation

enum MainSearchState {
    case loading
    case history
    case results
}
