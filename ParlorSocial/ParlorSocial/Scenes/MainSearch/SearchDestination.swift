//
//  SearchDestination.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 31/07/2019.
//

import Foundation
import UIKit

enum SearchDestination {
    case activationDetails(activation: Activation, openingType: OpeningActivationDetailsType, navigationSource: ActivationDetailsNavigationSource)
    case rsvpDetails(activation: Activation)
    case announcementDetails(announcementId: Int)
}

extension SearchDestination: Destination {
    var viewController: UIViewController {
        switch self {
        case .activationDetails(let activation, let openingType, let navigationSource):
            return ActivationDetailsNavigationProvider.getVCByAvailabilityOfOpeningActivationDetails(
                forActivation: activation,
                openingType: openingType,
                navigationSource: navigationSource
            )
        case .rsvpDetails(let activation):
            return RsvpDetailsViewController(activation: activation)
        case .announcementDetails(let announcementId):
            return AnnouncementDetailsViewController(withAnnouncementId: announcementId)
        }
    }
}
