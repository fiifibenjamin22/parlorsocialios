//
//  ParlorCalendarView.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 15/07/2019.
//

import UIKit
import JTAppleCalendar

class ParlorCalendarView: UIView {
    
    private let reusableCellId = "dateCell"
    private let reusableHeaderId = "reuseHeader"
    
    // MARK: Properties
    var dateSelectionCallback: (Date) -> Void = { _ in }
    var datesWithRsvps: Set<Date> = Set() {
        didSet {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) { [unowned self] in
                self.calendarView.reloadData()
            }
        }
    }
    
    // MARK: Views
    lazy var monthPickerView: MonthPickerView = {
        return MonthPickerView().manualLayoutable().apply {
            $0.leftArrowButton.addTarget(self, action: #selector(didTapPreviousMonth), for: .touchUpInside)
            $0.rightArrowButton.addTarget(self, action: #selector(didTapNextMonth), for: .touchUpInside)
        }
    }()
    
    lazy var calendarView: JTAppleCalendarView = {
        return JTAppleCalendarView().manualLayoutable().apply {
            $0.scrollingMode = .stopAtEachCalendarFrame
            $0.scrollDirection = .horizontal
            $0.backgroundColor = .white
            $0.ibCalendarDelegate = self
            $0.ibCalendarDataSource = self
            $0.register(DateCell.self, forCellWithReuseIdentifier: reusableCellId)
            $0.showsHorizontalScrollIndicator = false
            $0.minimumLineSpacing = 0
            $0.minimumInteritemSpacing = 0
            $0.scrollToDate(Date(), animateScroll: false)
            $0.allowsMultipleSelection = false
            $0.register(WeekdaysHeader.self,
                        forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                        withReuseIdentifier: reusableHeaderId)
        }
    }()
    
    // MARK: Initialization
    init() {
        super.init(frame: .zero)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        backgroundColor = .white
        autoresizingMask = .flexibleHeight
        addSubviews([monthPickerView, calendarView])
        calendarView.heightAnchor.constraint(equalTo: calendarView.widthAnchor).activate()
        
        monthPickerView.edgesToParent(anchors: [.leading, .trailing, .top], insets:
            .margins(top: 30, left: Constants.Margin.standard, right: Constants.Margin.standard))
        
        calendarView.topAnchor.equal(to: monthPickerView.bottomAnchor, constant: 12)
        calendarView.edgesToParent(anchors: [.leading, .trailing, .bottom], insets:
            .margins(left: Constants.Margin.standard, bottom: Constants.Margin.standard, right: Constants.Margin.standard))
    }
    
    @objc private func didTapPreviousMonth() {
        calendarView.scrollToSegment(.previous)
    }
    
    @objc private func didTapNextMonth() {
        calendarView.scrollToSegment(.next)
    }
    
}

extension ParlorCalendarView: JTAppleCalendarViewDataSource {
    
    func calendar(_ calendar: JTAppleCalendarView, headerViewForDateRange range: (start: Date, end: Date), at indexPath: IndexPath) -> JTAppleCollectionReusableView {
        return calendar.dequeueReusableJTAppleSupplementaryView(withReuseIdentifier: reusableHeaderId, for: indexPath)
    }
    
    func calendarSizeForMonths(_ calendar: JTAppleCalendarView?) -> MonthSize? {
        return MonthSize(defaultSize: 50)
    }
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        let currentDate = Date()
        var startComponents = DateComponents()
        var endComponents = DateComponents()
        startComponents.year = -1
        endComponents.year = 1
        let startDate = Calendar.current.date(byAdding: startComponents, to: currentDate)
        let endDate = Calendar.current.date(byAdding: endComponents, to: currentDate)
        return ConfigurationParameters(
            startDate: startDate ?? Date(),
            endDate: endDate ?? Date(),
            generateInDates: .forAllMonths,
            generateOutDates: .off
        )
    }
    
    private func configureCell(_ cell: JTAppleCell?, cellState: CellState, date: Date) {
        guard let cell = cell as? DateCell else {
            return
        }
        guard cellState.dateBelongsTo == .thisMonth else {
            cell.isHidden = true
            return
        }
        
        cell.apply {
            $0.isHidden = false
            $0.dateLabel.text = cellState.text
            $0.backgroundSelectionView.backgroundColor = cellColor(forDate: date, andState: cellState)
            $0.dateLabel.textColor = cellState.isSelected && !date.isToday ? .white : .appTextMediumBright
            $0.dotIndicator.backgroundColor = indicatorColor(forDate: date, andState: cellState)
        }
    }
    
    private func updateMonthLabel(basedOn date: Date) {
        let monthOrdinal = Calendar.current.component(.month, from: date)
        let month = Calendar.current.standaloneMonthSymbols[monthOrdinal - 1]
        let currentYear = Calendar.current.component(.year, from: Date())
        let dateYear = Calendar.current.component(.year, from: date)
        
        let monthLabelText: String = currentYear != dateYear ? "\(month) \(dateYear)" : month
        monthPickerView.monthLabel.text = monthLabelText
    }
    
    private func cellColor(forDate date: Date, andState state: CellState) -> UIColor {
        switch (date.isToday, state.isSelected) {
        case (false, true):
            return .appGreen
        case (true, _):
            return .textVeryLight
        default:
            return .clear
        }
    }
    
    private func indicatorColor(forDate date: Date, andState state: CellState) -> UIColor {
        guard datesWithRsvps.contains(date) else { return .clear }
        return state.isSelected && !date.isToday ? .white : .appGreenDark
    }
    
}

extension ParlorCalendarView: JTAppleCalendarViewDelegate {
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        configureCell(cell, cellState: cellState, date: date)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        guard let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: reusableCellId, for: indexPath) as? DateCell else {
            fatalError()
        }
        configureCell(cell, cellState: cellState, date: date)
        return cell
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        dateSelectionCallback(date)
        configureCell(cell, cellState: cellState, date: date)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        configureCell(cell, cellState: cellState, date: date)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, shouldSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) -> Bool {
        return true
    }
    
    func calendar(_ calendar: JTAppleCalendarView, shouldDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) -> Bool {
        return true
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        if let firstVisibleDate = visibleDates.monthDates.first?.date {
            updateMonthLabel(basedOn: firstVisibleDate)
        }
        
    }
    
}
