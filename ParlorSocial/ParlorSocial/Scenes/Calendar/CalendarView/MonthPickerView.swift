//
//  MonthPickerView.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 15/07/2019.
//

import UIKit

class MonthPickerView: UIView {
    
    private let arrowMultiplier: CGFloat = 1.0 / 8.0

    // MARK: Views
    lazy var leftArrowButton: TrackableButton = {
        return TrackableButton(type: .custom).manualLayoutable().apply {
            $0.setImage(Icons.RsvpDetails.rightArrow.rotated(byRadians: -Float.pi), for: .normal)
            $0.trackingId = AnalyticEventClickableElementName.CalendarScreen.showPreviousMonthArrow.rawValue
        }
    }()
    
    lazy var rightArrowButton: TrackableButton = {
        return TrackableButton(type: .custom).manualLayoutable().apply {
            $0.setImage(Icons.RsvpDetails.rightArrow, for: .normal)
            $0.trackingId = AnalyticEventClickableElementName.CalendarScreen.showNextMonthArrow.rawValue
        }
    }()
    
    lazy var monthLabel: ParlorLabel = {
        return ParlorLabel.styled(
            withFont: UIFont.custom(ofSize: Constants.FontSize.largeTitle, font: UIFont.Editor.bold),
            numberOfLines: 1
            ).apply {
                $0.adjustsFontSizeToFitWidth = true
                $0.minimumScaleFactor = CGFloat.leastNonzeroMagnitude
            }
    }()
    
    // MARK: Initalization
    init() {
        super.init(frame: .zero)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        addSubviews([leftArrowButton, rightArrowButton, monthLabel])
        
        monthLabel.apply {
            $0.edgesToParent(anchors: [.top, .bottom])
            $0.centerXAnchor.equal(to: self.centerXAnchor)
            $0.leadingAnchor.greaterThanOrEqual(to: leftArrowButton.trailingAnchor, constant: Constants.Margin.small)
            $0.trailingAnchor.lessThanOrEqual(to: rightArrowButton.leadingAnchor, constant: -Constants.Margin.small)
        }
        
        leftArrowButton.leadingAnchor.equal(to: self.leadingAnchor)
        
        rightArrowButton.trailingAnchor.equal(to: self.trailingAnchor)
        
        [rightArrowButton, leftArrowButton].forEach {
            $0.centerYAnchor.equal(to: self.centerYAnchor)
            $0.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: arrowMultiplier).activate()
        }

    }
}
