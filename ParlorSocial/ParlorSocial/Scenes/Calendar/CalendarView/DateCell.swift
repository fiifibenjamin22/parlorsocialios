//
//  DateCell.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 15/07/2019.
//

import UIKit
import JTAppleCalendar

class DateCell: JTAppleCell {
    
    // MARK: Views
    lazy var dateLabel: ParlorLabel = {
        return ParlorLabel.styled(
            withTextColor: .appTextMediumBright,
            withFont: UIFont.custom(ofSize: Constants.FontSize.hintSize, font: UIFont.Roboto.regular),
            alignment: .center,
            numberOfLines: 1
        )
    }()
    
    lazy var backgroundSelectionView: UIView = {
        return UIView().manualLayoutable()
    }()
    
    lazy var dotIndicator: UIView = {
        return UIView().manualLayoutable().apply {
            $0.layer.cornerRadius = 3
        }
    }()
    
    // MARK: Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        setContentHuggingPriority(.defaultLow, for: .horizontal)
        setContentCompressionResistancePriority(.required, for: .horizontal)
        initializeCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initializeCell() {
        contentView.addSubviews([backgroundSelectionView, dateLabel, dotIndicator])

        dateLabel.apply {
            $0.centerXAnchor.equal(to: contentView.centerXAnchor)
            $0.centerYAnchor.equal(to: contentView.centerYAnchor)
        }
        
        backgroundSelectionView.apply {
            $0.widthAnchor.equal(to: contentView.widthAnchor, constant: -4)
            $0.widthAnchor.equal(to: $0.heightAnchor)
            $0.centerXAnchor.equal(to: contentView.centerXAnchor)
            $0.centerYAnchor.equal(to: contentView.centerYAnchor)
        }
        
        dotIndicator.apply {
            $0.centerXAnchor.equal(to: dateLabel.centerXAnchor)
            $0.topAnchor.equal(to: dateLabel.bottomAnchor, constant: 2)
            $0.widthAnchor.equalTo(constant: 6)
            $0.heightAnchor.equalTo(constant: 6)
        }

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        backgroundSelectionView.layer.cornerRadius = (self.contentView.frame.size.width / 2) - 2
    }
    
}
