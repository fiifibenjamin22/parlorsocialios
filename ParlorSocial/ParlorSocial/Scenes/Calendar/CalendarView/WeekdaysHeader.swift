//
//  WeekdaysHeader.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 17/07/2019.
//

import UIKit
import JTAppleCalendar

class WeekdaysHeader: JTAppleCollectionReusableView {
    
    // MARK: Properties
    private let numberOfWeekdays = 6
    
    // MARK: Views
    lazy var weekdaysStack: UIStackView = {
        return UIStackView(axis: .horizontal, with: [], spacing: 0, alignment: .fill, distribution: .fillEqually)
    }()
    
    // MARK: Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        addSubview(weekdaysStack)
        weekdaysStack.edgesToParent()
        weekdaysStack.addArrangedSubviews((0...numberOfWeekdays).map(labelForWeekday))
    }
    
    private func labelForWeekday(_ number: Int) -> ParlorLabel {
        return ParlorLabel.styled(
            withTextColor: .eventTime,
            withFont: UIFont.custom(ofSize: Constants.FontSize.small, font: UIFont.Roboto.medium),
            alignment: .center
            ).apply {
                let calendar = Calendar.current
                let index = (number + calendar.firstWeekday - 1) % 7
                $0.text = String(calendar.veryShortStandaloneWeekdaySymbols[index])
            }
    }
}
