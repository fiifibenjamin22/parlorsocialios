//
//  CalendarItemsSection.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 17/07/2019.
//

import UIKit

final class CalendarItemsSection: BasicTableSection<Activation, CalendarItemCell> {
    
    private let firstActivation: Activation?
    
    override init(items: [Activation], itemSelector: @escaping ((Activation) -> Void) = { _ in }) {
        self.firstActivation = items.first
        super.init(items: items, itemSelector: itemSelector)
    }
    
    override var headerType: UITableViewHeaderFooterView.Type? { return CalendarSectionHeader.self }
    
    override func configure(header: UITableViewHeaderFooterView, in section: Int) {
        guard let sectionHeader = header as? CalendarSectionHeader else {
            fatalError("Header is wrong type! Needed: \(CalendarSectionHeader.name) but get instead \(String(describing: header))")
        }
        sectionHeader.firstActivation = firstActivation
    }
}

extension CalendarItemsSection {

    static func make(for activations: [Activation], itemSelector: @escaping ((Activation) -> Void) = { _ in }) -> [TableSection] {
        return [CalendarItemsSection(items: activations, itemSelector: itemSelector)]
    }

}
