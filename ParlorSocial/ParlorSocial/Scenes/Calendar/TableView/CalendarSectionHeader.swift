//
//  CalendarSectionHeader.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 17/07/2019.
//

import UIKit

class CalendarSectionHeader: UITableViewHeaderFooterView {
    
    // MARK: Views
    lazy var label: ParlorLabel = {
        return ParlorLabel.styled(
            withFont: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.bold),
            alignment: .left
            ).apply {
                $0.insets.top = 46
                $0.insets.left = Constants.Margin.standard
                $0.insets.bottom = 19
            }
    }()
    
    // MARK: Properties
    var firstActivation: Activation? {
        didSet {
            updateLabel()
        }
    }
    
    // MARK: Initialization
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        contentView.addSubview(label)
        label.edgesToParent()
    }
    
    private func updateLabel() {
        guard let date = firstActivation?.startAt else {
                label.text = nil
                return
        }

        if date.isToday {
            label.text = Strings.Announcements.today.localized.uppercased()
        } else {
            label.text = date.toCalendarHeaderDate()
        }
    }
}
