//
//  CalendarItemCell.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 17/07/2019.
//

import UIKit

class CalendarItemCell: UITableViewCell {
    
    lazy var mainContent: UIView = {
        return UIView().manualLayoutable().apply {
            $0.backgroundColor = .white
            $0.applyTouchAnimation()
        }
    }()
    
    lazy var bottomSeperator: UIView = {
        return UIView().manualLayoutable()
    }()
    
    lazy var activationIconImageView: UIImageView = {
        return UIImageView().manualLayoutable().apply {
            $0.clipsToBounds = true
            $0.contentMode = .scaleAspectFit
        }
    }()
    
    lazy var labelsStack: UIStackView = {
        return UIStackView(axis: .vertical, spacing: 11, alignment: .leading, distribution: .equalSpacing)
    }()
    
    lazy var bottomStack: UIStackView = {
        return UIStackView(axis: .horizontal, spacing: 10, alignment: .leading, distribution: .equalSpacing)
    }()
    
    lazy var titleLabel: ParlorLabel = {
        return ParlorLabel.styled(
            withTextColor: .appTextMediumBright,
            withFont: UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.medium),
            alignment: .left).apply {
                $0.lineHeight = 22
            }
    }()
    
    lazy var timeLabel: ParlorLabel = {
        return ParlorLabel.styled(
            withTextColor: .appTextMediumBright,
            withFont: UIFont.custom(ofSize: Constants.FontSize.small, font: UIFont.Roboto.regular),
            alignment: .left).apply {
                $0.setContentCompressionResistancePriority(.required, for: .horizontal)
                $0.lineHeight = 18
            }
    }()
    
    lazy var rsvpdLabel: ParlorLabel = {
        return ParlorLabel.styled(
            withTextColor: .white,
            withFont: UIFont.custom(ofSize: Constants.FontSize.xTiny, font: UIFont.Roboto.bold),
            alignment: .center).apply {
                $0.backgroundColor = .appGreenDark
                $0.textColor = .white
                $0.text = Strings.Activation.rsvpd.localized
                $0.layer.cornerRadius = 10
                $0.clipsToBounds = true
            }
    }()
    
    // MARK: Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        selectionStyle  = .none
        backgroundColor = .clear
        setupHierarchy()
        setupAutoLayout()
    }
    
    private func setupHierarchy() {
        contentView.addSubviews([mainContent, bottomSeperator])
        mainContent.addSubviews([activationIconImageView, labelsStack])
        labelsStack.addArrangedSubviews([titleLabel, bottomStack])
        bottomStack.addArrangedSubviews([timeLabel, rsvpdLabel])
    }
    
    private func setupAutoLayout() {
        mainContent.edgesToParent(
            anchors: [.leading, .top, .trailing],
            insets: .margins(left: Constants.Margin.standard, right: Constants.Margin.standard)
        )
        
        bottomSeperator.apply {
            $0.heightAnchor.equalTo(constant: 10)
            $0.edgesToParent(anchors: [.leading, .bottom, .trailing])
            $0.topAnchor.equal(to: mainContent.bottomAnchor)
        }
        
        activationIconImageView.apply {
            $0.edgesToParent(anchors: [.leading, .top], insets: .margins(top: 25, left: 20))
            $0.widthAnchor.equalTo(constant: 19)
            $0.heightAnchor.equalTo(constant: 19)
        }
        
        labelsStack.apply {
            $0.topAnchor.equal(to: activationIconImageView.topAnchor, constant: -3)
            $0.trailingAnchor.equal(to: mainContent.trailingAnchor)
            $0.bottomAnchor.equal(to: mainContent.bottomAnchor, constant: -25)
            $0.leadingAnchor.equal(to: activationIconImageView.trailingAnchor, constant: 15)
        }
        
        rsvpdLabel.apply {
            $0.heightAnchor.equalTo(constant: 20)
            $0.widthAnchor.equalTo(constant: 60)
        }
    }
    
}

extension CalendarItemCell: ConfigurableCell {
    
    typealias Model = Activation
    
    func configure(with model: Activation) {
        activationIconImageView.image = model.type.iconDark
        titleLabel.text = model.name
        timeLabel.text = model.startAt.toCalendarItemFormat(withEndDate: model.endAt)
        rsvpdLabel.isHidden = !model.isRsvpd
    }
}
