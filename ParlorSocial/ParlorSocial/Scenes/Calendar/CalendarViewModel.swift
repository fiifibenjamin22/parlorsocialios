//
//  CalendarViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 15/07/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import RxSwift
import RxSwiftExt
import RxCocoa

protocol CalendarLogic: BaseViewModelLogic {
    var sectionsObs: Observable<[TableSection]> { get }
    var selectedDateRelay: BehaviorSubject<Date> { get }
    var loadMoreRelay: PublishSubject<Void> { get }
    var showsLoadingFooter: Observable<Bool> { get }
    var destinationsObs: Observable<CalendarDestination> { get }
    var startDatesObs: Observable<Set<Date>> { get }
    func fetchRsvpsStartDates()
}

class CalendarViewModel: BaseViewModel {   
    let analyticEventReferencedId: Int? = nil
    private let activationsRepository = ActivationsRepository.shared
    private let activationsDataRelay = BehaviorRelay<[Activations.Get.Response]>(value: [])
    private let destinationsSubject = PublishSubject<CalendarDestination>()
    private let allRsvpsDates = BehaviorRelay<[Date]>(value: [])

    let selectedDateRelay: BehaviorSubject<Date> = BehaviorSubject(value: Date())
    let loadMoreRelay: PublishSubject<Void> = PublishSubject()
    
    fileprivate lazy var repositoryChangesMapper = {
        (newActivation: Activation, latestData: [Activations.Get.Response]) -> Observable<[Activations.Get.Response]> in
        var newActivationGetResponseData = latestData
        guard let latestDataIndex = latestData.firstIndex(where: {$0.data.firstIndex { $0.id == newActivation.id } != nil }),
            let activationIndex = latestData[latestDataIndex].data.firstIndex(where: { $0.id == newActivation.id }),
            latestData[latestDataIndex].data[activationIndex] != newActivation else {
                return Observable.empty()
        }
        
        var activations = latestData[latestDataIndex].data
        activations[activationIndex] = newActivation
        newActivationGetResponseData[latestDataIndex] = newActivationGetResponseData[latestDataIndex].withActivations(activations)
        return Observable.just(newActivationGetResponseData)
    }
    
    private var activationsObs: Observable<[Activation]> {
        return activationsDataRelay.map { responses in responses.flatMap { $0.data }.uniqueElements }
    }
    
    private var nextPageToLoadObs: Observable<Int> {
        return activationsDataRelay.map { currentData in
            return currentData.count + 1
        }
    }
    
    private lazy var dateSelectedObs: Observable<Date> = {
        self.selectedDateRelay.asObservable()
    }()
    
    override init() {
        super.init()
        initFlow()
    }
}

// MARK: Private logic
private extension CalendarViewModel {
    
    private func initFlow() {
        
        selectedDateRelay.doOnNext { [unowned self] _ in
            self.activationsDataRelay.accept([])
            }.subscribe().disposed(by: disposeBag)
        
        selectedDateRelay
            .flatMapLatest { [unowned self] selectedDate in
                return self.loadMoreRelay.startWithElement()
                    .withLatestFrom(self.activationsDataRelay)
                    .filter { !($0.last?.meta.isLastPage ?? false ) }
                    .withLatestFrom(self.nextPageToLoadObs)
                    .flatMapFirst { pageToLoad in
                        return self.activationsRepository.getActivations(
                        using: Activations.Get.Request.calendar(forDate: selectedDate, page: pageToLoad)
                        )
                    }
            }
            .mergeWithCurrentPaginatableData(from: activationsDataRelay, errorSubject: errorSubject)
            .bind(to: activationsDataRelay)
            .disposed(by: disposeBag)
        
        activationsRepository.newActivationDataObs
            .withLatestFrom(activationsDataRelay) { return ($0, $1) }
            .flatMap(repositoryChangesMapper)
            .bind(to: activationsDataRelay)
            .disposed(by: disposeBag)
    }
    
    private func handleRsvpsStartDates(_ response: DatesApiResponse) {
        switch response {
        case .success(let response):
            allRsvpsDates.accept(response.data)
        case .failure:
            break
        }
    }
}

// MARK: Interface logic methods
extension CalendarViewModel: CalendarLogic {
    
    var startDatesObs: Observable<Set<Date>> {
        return allRsvpsDates.map { allDates in
            Set(allDates.map { $0.startOfDay })
        }
    }
    
    var destinationsObs: Observable<CalendarDestination> {
        return destinationsSubject.asObservable()
    }
    
    var showsLoadingFooter: Observable<Bool> {
        return activationsDataRelay.map { data in
            guard let last = data.last else { return data.isEmpty }
            return last.meta.currentPage < last.meta.lastPage
        }
    }
    
    var sectionsObs: Observable<[TableSection]> {
        return Observable.combineLatest(selectedDateRelay, activationsObs).map { date, activations in
            return CalendarItemsSection.make(for: activations, itemSelector: { [unowned self] activation in
                if activation.isRsvpd {
                    self.destinationsSubject.onNext(.rsvpDetails(activation: activation))
                } else {
                    let openingType = ActivationDetailsNavigationProvider.getOpeningActivationDetailsType(with: activation)
                    switch openingType {
                    case .activationDetailsVC, .addProfilePhotoVC, .upgradeToPremiumForStandard:
                        self.destinationsSubject.onNext(.activationDetails(activation: activation, openingType: openingType, navigationSource: .others))
                    case .startedAlert:
                        let alertData = AlertData.fromSimpleMessage(message: Strings.Activation.startedAlert.localized, sourceName: .activationHasAlreadyStarted)
                        self.alertDataSubject.onNext(alertData)
                    }
                }
                AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.CalendarScreen.activationItem.rawValue)
            })
        }
    }
    
    func fetchRsvpsStartDates() {
        activationsRepository
            .getRsvpsStartDates()
            .showingProgressBar(with: self)
            .subscribe(onNext: { [unowned self] response in
                self.handleRsvpsStartDates(response)
            })
            .disposed(by: disposeBag)
    }

}
