//
//  CalendarDestination.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 18/07/2019.
//

import UIKit

enum CalendarDestination {
    case activationDetails(activation: Activation, openingType: OpeningActivationDetailsType, navigationSource: ActivationDetailsNavigationSource)
    case rsvpDetails(activation: Activation)
}

extension CalendarDestination: Destination {
    
    var viewController: UIViewController {
        switch self {
        case .activationDetails(let activation, let openingType, let navigationSource):
            return ActivationDetailsNavigationProvider.getVCByAvailabilityOfOpeningActivationDetails(
                forActivation: activation, openingType: openingType, navigationSource: navigationSource)
        case .rsvpDetails(let activation):
            return RsvpDetailsViewController(activation: activation)
        }
    }
    
}
