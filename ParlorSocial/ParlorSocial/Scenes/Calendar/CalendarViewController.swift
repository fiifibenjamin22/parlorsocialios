//
//  CalendarViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 15/07/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift
import JTAppleCalendar

class CalendarViewController: AppViewController {
    
    // MARK: - Views
    private(set) var tableView: UITableView!
    private(set) var parlorCalendarView: ParlorCalendarView!

    // MARK: - Properties
    let analyticEventScreen: AnalyticEventScreen? = .calendar
    private var viewModel: CalendarLogic!
    private var tableViewManager: TableViewManager!
    
    override var observableSources: ObservableSources? {
        return viewModel
    }
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }

    // MARK: - Initialization
    init() {
        super.init(nibName: nil, bundle: nil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() {
        self.viewModel = CalendarViewModel()
    }
    
    override func observeViewModel() {
        super.observeViewModel()
        viewModel.sectionsObs.doOnNext { [unowned self] sections in
            self.tableViewManager.setupWith(sections: sections)
        }.subscribe().disposed(by: disposeBag)
        
        viewModel.showsLoadingFooter.doOnNext { [unowned self] isVisible in
            if isVisible {
                self.tableViewManager.showLoadingFooter()
            } else {
                self.tableViewManager.hideLoadingFooter()
            }
        }.subscribe().disposed(by: disposeBag)
        
        viewModel.destinationsObs.doOnNext { [unowned self] destination in
            self.handleDestination(destination)
        }.subscribe().disposed(by: disposeBag)
        
        viewModel.startDatesObs.doOnNext { [unowned self] rsvpsDates in
            self.parlorCalendarView.datesWithRsvps = rsvpsDates
        }.subscribe().disposed(by: disposeBag)
        
    }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }
    
    override func viewDidLoad() {
        setupPresentationLogic()
        super.viewDidLoad()
        viewModel?.fetchRsvpsStartDates()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.layoutTableHeaderView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        adjustNavBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        adjustNavBar()
    }
    
    @objc private func didTapClose() {
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.CalendarScreen.close.rawValue)
        self.navigationController?.dismiss(animated: true)
    }
}

// MARK: - Private methods
extension CalendarViewController {

    private func setupViews() {
        let builder = CalendarViewBuilder(controller: self)
        self.tableView = builder.buildTableView()
        self.parlorCalendarView = builder.buildParlorCalendarView()
        builder.setupViews()
    }

    private func setupPresentationLogic() {
        self.tableViewManager = TableViewManager(tableView: tableView, delegate: self)
        self.tableView.tableHeaderView = parlorCalendarView
        self.parlorCalendarView.dateSelectionCallback = { [unowned self] selectedDate in
            AnalyticService.shared.saveViewClickAnalyticEvent(withName: AnalyticEventClickableElementName.CalendarScreen.dayItem.rawValue)
            self.scrollAndChangeDate(to: selectedDate)
        }
    }
    
    private func scrollAndChangeDate(to date: Date) {
        self.tableViewManager.scrollToTop()
        let isScrolledToTop = tableView.contentOffset.y <= 0
        if isScrolledToTop {
            self.viewModel.selectedDateRelay.onNext(date)
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                self.viewModel.selectedDateRelay.onNext(date)
            })
        }
    }
    
    private func adjustNavBar() {
        title = Strings.Calendar.title.localized.uppercased()
        navigationController?.removeTransparentTheme()
        navigationController?.setNavigationBarBorderColor()
        statusBar?.backgroundColor = .white
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: Icons.NavigationBar.close,
            style: .done,
            target: self,
            action: #selector(didTapClose)
            ).apply {
                $0.tintColor = .lightBackArrowColor
        }
    }
    
    private func handleDestination(_ destination: CalendarDestination) {
        router.push(destination: destination)
    }

}

extension CalendarViewController: TableManagerDelegate {
    
    func tableViewDidScroll(_ scrollView: UIScrollView) { }
    
    func didSwipeForRefresh() { }
    
    func loadMoreData() {
        viewModel.loadMoreRelay.emitElement()
    }
    
}
