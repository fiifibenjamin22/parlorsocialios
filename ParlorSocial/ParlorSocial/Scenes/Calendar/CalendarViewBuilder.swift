//
//  CalendarViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 15/07/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CalendarViewBuilder {

    private unowned let controller: CalendarViewController
    private var view: UIView! { return controller.view }

    init(controller: CalendarViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension CalendarViewBuilder {

    private func setupProperties() { }

    private func setupHierarchy() {
        controller.apply {
            view.addSubview($0.tableView)
        }
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.tableView.edgesToParent()
        }
    }

}

// MARK: - Public build methods
extension CalendarViewBuilder {
    
    func buildTableView() -> UITableView {
        return UITableView(frame: .zero, style: .grouped).manualLayoutable().apply {
            $0.backgroundColor = .appBackground
            $0.separatorStyle = .none
            $0.bounces = false
        }
    }
    
    func buildParlorCalendarView() -> ParlorCalendarView {
        return ParlorCalendarView().manualLayoutable()
    }
}
