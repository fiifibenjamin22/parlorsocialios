//
//  CalendarSyncViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 19/07/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import EventKit
import RxSwift

class CalendarSyncViewController: AppViewController {

    // MARK: - Views
    private(set) var iconedMessage: IconedMessageView!
    private(set) var confirmButton: RoundedButton!
    private(set) var syncSwitch: ParlorLabeledSwitch!

    // MARK: - Properties
    let analyticEventScreen: AnalyticEventScreen? = .calendarSync
    var viewModel: BaseViewModelLogic!
    private let activationToSync: Activation?
    private let settingsChangeSource: CalendarSyncChangedEventSource
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }

    // MARK: - Initialization
    init(activationToSync: Activation? = nil, settingsChangeSource: CalendarSyncChangedEventSource) {
        self.activationToSync = activationToSync
        self.settingsChangeSource = settingsChangeSource
        
        super.init(nibName: nil, bundle: nil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
       fatalError()
    }

    private func setup() {
        self.viewModel = BaseViewModelClass()
    }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPresentationLogic()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        adjustNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        statusBar?.backgroundColor = .white
    }
    
    // MARK: - Callback
    @objc private func didTapContinue() {
        handleCalendarSyncStateChange(isCalendarSyncEnabled: syncSwitch.isOn) { [weak self] isEnabled in
            if let activationToSync = self?.activationToSync, let rsvp = activationToSync.rsvp, isEnabled {
                CalendarUtilities.shared.addRsvp(rsvp, forActivation: activationToSync)
            }
            self?.navigationController?.setViewControllers([CalendarSyncDestination.reservationConfirmed.viewController], animated: true)
        }
    }

}

// MARK: - Private methods
extension CalendarSyncViewController {
    
    private func handleCalendarSyncStateChange(isCalendarSyncEnabled: Bool, _ completionHandler: @escaping (Bool) -> Void) {
        CalendarUtilities.shared.changeSyncState(isEnabled: isCalendarSyncEnabled, changeSource: settingsChangeSource) { [weak self] (isEnabled, alertData) in
            if let alertData = alertData {
                self?.present(ParlorAlertViewController(alertData: alertData), animated: true, completion: nil)
            }
            completionHandler(isEnabled)
        }
    }

    private func setupViews() {
        let builder = CalendarSyncViewBuilder(controller: self)
        self.iconedMessage = builder.buildCalendarSyncMessageView()
        self.confirmButton = builder.buildContinueButton()
        self.syncSwitch = builder.buildLabeledSwitch()
        builder.setupViews()
    }

    private func setupPresentationLogic() {
        syncSwitch.isOn = true
        confirmButton.addTarget(self, action: #selector(didTapContinue), for: .touchUpInside)
    }
    
    private func adjustNavigationBar() {
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

}
