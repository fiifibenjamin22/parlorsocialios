//
//  CalendarSyncDestination.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 09/09/2019.
//

import UIKit

enum CalendarSyncDestination {
    case reservationConfirmed
}

extension CalendarSyncDestination: Destination {
    
    var viewController: UIViewController {
        switch self {
        case .reservationConfirmed:
            return RsvpConfirmedViewController()
        }
    }
    
}
