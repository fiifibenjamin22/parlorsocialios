//
//  CalendarSyncViewProvider.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 13/03/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import Foundation

final class CalendarSyncViewProvider {
    // MARK: - Properties
    // Incrementing is done after showing calendar sync
    let rsvpsCountWhenShowCalendarSync = [0, 4]
    
    func showCalendarSyncViewController() -> Bool {
        guard rsvpsCountWhenShowCalendarSync.contains(CalendarSyncConfig.rsvpsCount),
            CalendarSyncConfig.isSyncEnabled != true else { return false }
        return true
    }
    
    func isFirstRsvp() -> Bool {
        return Config.rsvpsCount == 0
    }
}
