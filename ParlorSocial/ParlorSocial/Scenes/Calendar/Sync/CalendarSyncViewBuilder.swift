//
//  CalendarSyncViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 19/07/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class CalendarSyncViewBuilder {

    private unowned let controller: CalendarSyncViewController
    private var view: UIView! { return controller.view }

    init(controller: CalendarSyncViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension CalendarSyncViewBuilder {

    private func setupProperties() {
        view.backgroundColor = .white
        
        controller.apply {
            $0.confirmButton.setTitle(Strings.ResetPassword.continueButton.localized.uppercased(), for: .normal)
            $0.syncSwitch.label.text = Strings.Calendar.syncTitle.localized
        }
    }

    private func setupHierarchy() {
        controller.apply {
            view.addSubviews([$0.iconedMessage, $0.syncSwitch, $0.confirmButton])
        }
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.iconedMessage.edgesToParent(anchors: [.leading, .trailing],
                                           insets: .margins(left: Constants.horizontalMarginBig, right: Constants.horizontalMarginBig))
            $0.iconedMessage.topAnchor.equal(to: view.safeAreaLayoutGuide.topAnchor, constant: 70)
            
            $0.confirmButton.edgesToParent(anchors: [.leading, .trailing],
                                           insets: .margins(left: Constants.horizontalMarginBig, right: Constants.horizontalMarginBig))
            $0.confirmButton.bottomAnchor.equal(to: view.bottomAnchor, constant: -90)
            $0.confirmButton.heightAnchor.equalTo(constant: Constants.bigButtonHeight)

            $0.syncSwitch.edgesToParent(anchors: [.leading, .trailing],
                                        insets: .margins(left: Constants.horizontalMarginBig, right: Constants.horizontalMarginBig))
            $0.syncSwitch.centerYAnchor.equal(to: view.centerYAnchor, constant: 80)
        }
    }

}

// MARK: - Public build methods
extension CalendarSyncViewBuilder {

    func buildContinueButton() -> RoundedButton {
        return RoundedButton().manualLayoutable().apply { it in
            it.backgroundColor = UIColor.black
            it.setTitleColor(UIColor.white, for: .normal)
            it.titleLabel?.font = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium)
            it.applyTouchAnimation()
        }
    }
    
    func buildCalendarSyncMessageView() -> IconedMessageView {
        return IconedMessageView(icon: Icons.Profile.calendarSetttings,
                                 title: Strings.Calendar.syncTitle.localized,
                                 message: Strings.Calendar.syncMessage.localized).manualLayoutable()
    }
    
    func buildLabeledSwitch() -> ParlorLabeledSwitch {
        return ParlorLabeledSwitch().manualLayoutable().apply {
            $0.uiSwitch.onTintColor = .appGreen
        }
    }

}
