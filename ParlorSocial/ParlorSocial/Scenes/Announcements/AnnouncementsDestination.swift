//
//  AnnouncementsDestination.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 05/07/2019.
//

import UIKit

enum AnnouncementDestination {
    case imageGallery(images: [URL])
    case announcementDetails(announcementId: Int)
    case membershipPlans
}

extension AnnouncementDestination: Destination {
    
    var viewController: UIViewController {
        switch self {
        case .imageGallery(let urls):
            return ImagesGalleryViewController(withUrls: urls)
        case .announcementDetails(let announcementId):
            return AnnouncementDetailsViewController(withAnnouncementId: announcementId)
        case .membershipPlans:
            return MembershipPlansViewController()
        }
    }
}
