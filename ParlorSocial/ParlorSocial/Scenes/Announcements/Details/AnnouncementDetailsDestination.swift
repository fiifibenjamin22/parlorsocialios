//  AnnouncementDetailsDestination.swift
//  ParlorSocialClub
//
//  Created on 01/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

enum AnnouncementDetailsDestination: Destination {
    case imageGallery(imageUrls: [URL])
    
    var viewController: UIViewController {
        switch self {
        case .imageGallery(let imageUrls):
            return ImagesGalleryViewController(withUrls: imageUrls).apply {
                $0.modalPresentationStyle = .fullScreen
            }
        }
    }
}
