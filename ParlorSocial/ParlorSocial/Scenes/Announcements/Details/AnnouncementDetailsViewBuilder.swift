//
//  AnnouncementsDetailsViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 05/07/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//
import UIKit

class AnnouncementsDetailsViewBuilder {
    
    private unowned let controller: AnnouncementDetailsViewController
    private var view: UIView! { return controller.view }
    
    init(controller: AnnouncementDetailsViewController) {
        self.controller = controller
    }
    
    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension AnnouncementsDetailsViewBuilder {
    
    private func setupProperties() {
        controller.apply {
            $0.extendedLayoutIncludesOpaqueBars = true
            $0.announcementMessage.lineHeight = 24
        }
    }
    
    private func setupHierarchy() {
        controller.apply {
            view.addSubview($0.scrollView)
            $0.scrollView.addSubview($0.scrollContentView)
            $0.scrollContentView.addSubviews([$0.announcementImage, $0.labelsStack])
        }
    }
    
    private func setupAutoLayout() {
        controller.apply {
            $0.scrollView.edgesToParent()
            
            $0.scrollContentView.edgesToParent()
            $0.scrollContentView.widthAnchor.equal(to: $0.scrollView.widthAnchor)
            
            $0.announcementImage.edgesToParent(anchors: [.leading, .trailing, .top])

            $0.labelsStack.edgesToParent(anchors: [.leading, .trailing], padding: 25)
            $0.labelsStack.topAnchor.equal(to: $0.announcementImage.bottomAnchor, constant: 42)
            $0.labelsStack.edgesToParent(anchors: [.bottom], padding: 31)
        }
    }
    
}

// MARK: - Public build methods
extension AnnouncementsDetailsViewBuilder {
    
    func buildScrollView() -> UIScrollView {
        return UIScrollView().manualLayoutable().apply {
            $0.backgroundColor = .white
        }
    }
    
    func buildScrollContent() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildImageView() -> UIImageView {
        return UIImageView().manualLayoutable().apply {
            $0.contentMode = .scaleAspectFill
            $0.clipsToBounds = true
        }
    }
    
    func buildLablesStack() -> UIStackView {
        return UIStackView(axis: .vertical,
                           with: [controller.titleLabel, controller.announcementMessage],
                           spacing: 15, alignment: .fill, distribution: .equalSpacing).manualLayoutable()
    }
    
    func buildTitleLabel() -> ParlorLabel {
        return ParlorLabel.styled(withTextColor: .appTextMediumBright,
                                  withFont: UIFont.custom(ofSize: Constants.FontSize.title, font: UIFont.Editor.bold),
                                  alignment: .left, numberOfLines: 0)
    }
    
    func buildMessageTextView() -> ParlorTextView {
        return ParlorTextView().apply {
            $0.font = UIFont.custom(ofSize: Constants.FontSize.body, font: UIFont.Roboto.light)
            $0.textColor = .appTextMediumBright
            $0.tintColor = .appTextMediumBright
            $0.lineHeight = Constants.lineHeightBig
        }
    }
    
    func buildImageGalleryNavigationItem() -> UIBarButtonItem {
        return UIBarButtonItem(image: Icons.Common.moreImages, style: .plain, target: controller, action: nil)
    }
    
    func buildImageViewVisibleConstraint() -> NSLayoutConstraint {
        return controller.announcementImage.widthAnchor.constraint(equalTo: controller.announcementImage.heightAnchor, multiplier: 1.1)
    }
    
    func buildImageViewEmptyConstraint() -> NSLayoutConstraint {
        return controller.announcementImage.heightAnchor.constraint(equalToConstant: 64)
    }
}

