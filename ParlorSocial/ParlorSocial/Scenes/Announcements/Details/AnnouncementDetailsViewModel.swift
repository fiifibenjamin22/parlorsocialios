//  AnnouncementDetailsViewModel.swift
//  ParlorSocialClub
//
//  Created on 01/07/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import RxCocoa
import RxSwift

protocol AnnouncementDetailsViewModelLogic: BaseViewModelLogic {
    var announcementObs: Observable<Announcement> { get }
    var destinationObs: Observable<AnnouncementDetailsDestination> { get }

    func navigateToImageGallery()
    func loadData()
}

final class AnnouncementDetailsViewModel: BaseViewModel {
    
    // MARK: - Properties
    
    var analyticEventReferencedId: Int?
    
    private let announcementId: Int
    private let announcementsRepository: AnnouncementsRepositoryProtocol
    private let announcementRelay: BehaviorRelay<Announcement?> = BehaviorRelay(value: nil)
    private let destinationRelay: PublishRelay<AnnouncementDetailsDestination> = PublishRelay()

    // MARK: - Init
    
    init(withAnnouncementId announcementId: Int, announcementsRepository: AnnouncementsRepositoryProtocol = AnnouncementsRepository.shared) {
        self.announcementId = announcementId
        self.analyticEventReferencedId = announcementId
        self.announcementsRepository = announcementsRepository
        
        super.init()
    }
}

extension AnnouncementDetailsViewModel: AnnouncementDetailsViewModelLogic {
    var announcementObs: Observable<Announcement> {
        return announcementRelay.ignoreNil().asObservable()
    }
    
    var destinationObs: Observable<AnnouncementDetailsDestination> {
        return destinationRelay.asObservable()
    }
    
    func navigateToImageGallery() {
        guard let imageUrls = announcementRelay.value?.imageUrls.imageUrls2x3 else { return }
        destinationRelay.accept(.imageGallery(imageUrls: imageUrls))
    }
    
    func loadData() {
        announcementsRepository.getAnnouncement(withId: announcementId)
            .showingProgressBar(with: self)
            .filterMap { [unowned self] response in
                switch response {
                case .success(let responseData):
                    return .map(responseData.data)
                case .failure(let error):
                    self.alertDataSubject.onNext(AlertData.fromAppError(
                        appError: error,
                        bottomButtonAction: { [unowned self] in self.closeViewSubject.emitElement() },
                        closeButtonAction: { [unowned self] in  self.closeViewSubject.emitElement() }
                    ))
                    return .ignore
                }
            }
            .bind(to: announcementRelay)
            .disposed(by: disposeBag)
    }
}
