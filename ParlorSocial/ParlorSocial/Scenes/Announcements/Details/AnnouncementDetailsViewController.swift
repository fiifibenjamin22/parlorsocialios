//
//  AnnouncementsDetailsViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 05/07/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

class AnnouncementDetailsViewController: AppViewController {
    
    // MARK: - Views
    private(set) var scrollView: UIScrollView!
    private(set) var scrollContentView: UIView!
    private(set) var announcementImage: UIImageView!
    private(set) var labelsStack: UIStackView!
    private(set) var titleLabel: ParlorLabel!
    private(set) var announcementMessage: ParlorTextView!
    private(set) var moreImagesBarItem: UIBarButtonItem!
    
    private(set) var imageViewWithImagesConstraint: NSLayoutConstraint!
    private(set) var imageViewEmptyConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    private var viewModel: AnnouncementDetailsViewModelLogic!
    private var isNoImageMode = true
    
    let analyticEventScreen: AnalyticEventScreen? = .announcementDetails
    
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }
    
    override var observableSources: ObservableSources? {
        return viewModel
    }
    
    override var includeNavBarSafeAreaInsets: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return isNoImageMode ? .default : .lightContent
    }
    
    // MARK: - Initialization
    init(withAnnouncementId announcementId: Int) {
        super.init(nibName: nil, bundle: nil)
        
        setup(withAnnouncementId: announcementId)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    private func setup(withAnnouncementId announcementId: Int) {
        viewModel = AnnouncementDetailsViewModel(withAnnouncementId: announcementId)
    }
    
    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        disableScrollInsets(for: scrollView)
        bindView()
        viewModel.loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        adjustNavigationBar()
        scrollView.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !isNoImageMode {
            UIView.animate(withDuration: 0.2) { [unowned self] in
                self.scrollView.changeNavigationBarAlpha(using: self.announcementImage, to: self.navigationController?.navigationBar)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        scrollView.delegate = nil
        scrollView.stopScrolling()
    }
    
    // MARK: - Binding
    
    private func bindView() {        
        viewModel.announcementObs
            .bind { [unowned self] announcement in
                self.isNoImageMode = announcement.imageUrls.imageUrl11x10 == nil
                self.adjustNavigationBar()
                self.configure(with: announcement)
            }
            .disposed(by: disposeBag)
        
        viewModel.destinationObs
            .bind { [unowned self] destination in
                switch destination {
                case .imageGallery:
                    self.present(destination.viewController, animated: true)
                }
            }
            .disposed(by: disposeBag)
        
        viewModel.closeObs
            .bind { [unowned self] _ in self.dismiss() }
            .disposed(by: disposeBag)
    }
    
    // MARK: - Setup
    
    private func adjustNavigationBar() {
        navigationController?.setNavigationBarHidden(false, animated: false)
        if presentingViewController != nil {
            navigationItem.rightBarButtonItems = [UIBarButtonItem(image: Icons.NavigationBar.close, style: .plain, target: self, action: #selector(didTapClose))]
        } else {
            navigationItem.rightBarButtonItems = []
        }
        
        if isNoImageMode {
            navigationController?.removeTransparentTheme()
            navigationController?.setNavigationBarBorderColor()
            statusBar?.backgroundColor = .white
        } else {
            navigationController?.applyTransparentTheme(withColor: .white)
            statusBar?.backgroundColor = .clear
        }
        setNeedsStatusBarAppearanceUpdate()
    }
    
    private func configure(with announcement: Announcement) {
        announcementImage.getImage(from: announcement.imageUrls.imageUrl11x10)
        announcementMessage.text = announcement.message
        imageViewEmptyConstraint.isActive = isNoImageMode
        imageViewWithImagesConstraint.isActive = !isNoImageMode
        titleLabel.text = announcement.title
        
        if announcement.imageUrls.imageUrls2x3.count > 1 {
            navigationItem.rightBarButtonItems?.append(moreImagesBarItem)
        }
        title = announcement.title.uppercased()
    }
    
    private func setupViews() {
        let builder = AnnouncementsDetailsViewBuilder(controller: self)
        self.scrollView = builder.buildScrollView()
        self.scrollView.delegate = self
        self.scrollContentView = builder.buildScrollContent()
        self.announcementImage = builder.buildImageView()
        self.titleLabel = builder.buildTitleLabel()
        self.announcementMessage = builder.buildMessageTextView()
        self.labelsStack = builder.buildLablesStack()
        self.moreImagesBarItem = builder.buildImageGalleryNavigationItem()
        self.moreImagesBarItem.action = #selector(didTapGallery)
        self.imageViewWithImagesConstraint = builder.buildImageViewVisibleConstraint()
        self.imageViewEmptyConstraint = builder.buildImageViewEmptyConstraint()
        builder.setupViews()
    }

    // MARK: - Actions
    
    @objc private func didTapGallery() {
        viewModel.navigateToImageGallery()
    }
    
    @objc private func didTapClose() {
        dismiss()
    }
}

// MARK: - UIScrollViewDelegate
extension AnnouncementDetailsViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if !isNoImageMode {
            scrollView.changeNavigationBarAlpha(using: announcementImage, to: navigationController?.navigationBar)
        }
    }
    
}
