//
//  AnnouncementsViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 12/03/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class AnnouncementsViewBuilder {

    private unowned let controller: AnnouncementsViewController
    private var view: UIView! { return controller.view }

    init(controller: AnnouncementsViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }

}

// MARK: - Private methods
extension AnnouncementsViewBuilder {
    
    private func setupProperties() {
        
    }
    
    private func setupHierarchy() {
        view.addSubviews([controller.tableView])
        
    }
    
    private func setupAutoLayout() {
        controller.setupActivateMembershipView()
        controller.apply {
            $0.tableView.edgesToParent()
        }
    }
    
}

// MARK: - Public build methods
extension AnnouncementsViewBuilder {
    
    func buildTableView() -> UITableView {
        return UITableView().manualLayoutable().apply {
            $0.separatorStyle = .none
            $0.showsVerticalScrollIndicator = false
            $0.contentInset = UIEdgeInsets(top: 40, left: 0, bottom: 0, right: 0)
        }
    }
    
    func buildActivateMembershipView() -> ActivateMembershipView {
        return ActivateMembershipView().manualLayoutable()
    }
    
}
