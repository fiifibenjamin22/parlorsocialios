//
//  AnnouncementsViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 12/03/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

class AnnouncementsViewController: FilterableViewController, HasActivateMembershipView {
        
    // MARK: - Vews
    private(set) var tableView: UITableView!
    var activateMembershipView: ActivateMembershipView!

    // MARK: - Properties
    private var viewModel: AnnouncementsLogic!
    private var tableManager: TableViewManager!
    
    override var analyticEventScreen: AnalyticEventScreen {
        return .announcementsList
    }
    
    override var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }

    override var connectedOnboardingType: OnboardingType? {
        return .announcements
    }
    
    override var observableSources: ObservableSources? {
        return viewModel
    }
    
    override var tabBarController: AppTabBarController {
        guard let parent = self.parent?.parent as? AppTabBarController else {
            fatalError("Did you manipulate with controller hierarchy?") }
        return parent
    }
    
    // MARK: - Initialization
    init() {
        super.init(nibName: nil, bundle: nil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() {
        self.viewModel = AnnouncementsViewModel()
    }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }

    override func viewDidLoad() {
        setupTableManager()
        super.viewDidLoad()
        viewModel?.refreshSubject.emitElement()
        setupPresentationLogic()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        statusBar?.backgroundColor = .white
        updateUnreadAnnouncements()
    }
    
    override func observeViewModel() {
        super.observeViewModel()
        
        viewModel.sections
            .doOnNext { [unowned self] sections in
                self.tableManager.setupWith(sections: sections)
            }.subscribe().disposed(by: disposeBag)
        
        viewModel.isDataLoaded.distinctUntilChanged()
            .doOnNext { [weak self] isLoaded in
                if isLoaded {
                    self?.tableManager.hideLoadingFooter()
                } else {
                    self?.tableManager.showLoadingFooter()
                }
            }.subscribe().disposed(by: disposeBag)
        
        viewModel.destinationsObs
            .doOnNext { [weak self] destination in
                self?.handleDestination(destination)
            }.subscribe().disposed(by: disposeBag)
        
        viewModel.unreadAnnouncementsObs
            .subscribe(onNext: { [unowned self] isUnread in
                if isUnread && self.tabBarController.selectedIndex == MainTabBarScreen.announcements.rawValue {
                    self.updateUnreadAnnouncements()
                }
            }).disposed(by: disposeBag)
        
        viewModel.setupActivateMembershipViewSubject
            .subscribe(onNext: { [unowned self] data in
                self.activateMembershipView.setup(with: data)
                self.tableView.contentInset = .margins(top: data.isHidden ? 40 : 83)
            }).disposed(by: disposeBag)
    }
        
    override var controllerTitle: String? { return Strings.Announcements.title.localized }
    
    func updateUnreadAnnouncements() {
        viewModel.updateUnreadAnnouncementsSubject.onNext(tableView.isInAroundOfTop)
    }
    
    func didTapActivateMembershipMoreButton() {
        viewModel.didTapActivateMembershipMoreButton()
    }
  
}

// MARK: - Private methods
extension AnnouncementsViewController {

    private func setupViews() {
        let builder = AnnouncementsViewBuilder(controller: self)
        self.tableView = builder.buildTableView()
        self.activateMembershipView = builder.buildActivateMembershipView()
        builder.setupViews()
    }
    
    private func setupTableManager() {
        tableManager = TableViewManager(tableView: tableView, delegate: viewModel)
        tableManager.setupRefreshControl()
    }

    private func setupPresentationLogic() {
        viewModel.initActivateMembershipViewLogic()
    }
    
    private func handleDestination(_ destination: AnnouncementDestination) {
        switch destination {
        case .imageGallery:
            self.router.present(destination: destination, animated: true, withStyle: .fullScreen)
        case .announcementDetails, .membershipPlans:
            topRouter.push(destination: destination)
        }
    }
    
}

extension AnnouncementsViewController: TabBarChild {
    
    func didTouchTabItem() {
        scrollToTop()
        viewModel.refreshSubject.emitElement()
    }
    
}

extension AnnouncementsViewController: ScrollableContent {
    
    func scrollToTop() {
        tableManager.scrollToTop()
    }
    
}
