//
//  AnnouncementsViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 12/03/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxSwift
import RxSwiftExt

protocol AnnouncementsLogic: BaseViewModelLogic & TableManagerDelegate, HasActivateMembershipViewLogic {
    var sections: Observable<[TableSection]> { get }
    var isDataLoaded: Observable<Bool> { get }
    var destinationsObs: Observable<AnnouncementDestination> { get }
    var refreshSubject: PublishSubject<Void> { get }
    var loadMoreNeeded: PublishSubject<Void> { get }
    var updateUnreadAnnouncementsSubject: PublishSubject<Bool> { get }
    var unreadAnnouncementsObs: Observable<Bool> { get }
}

class AnnouncementsViewModel: BaseViewModel {
    
    // MARK: Properties
    let analyticEventReferencedId: Int? = nil
    var activateMembershipViewLogic: ActivateMembershipViewLogic?
    var setupActivateMembershipViewSubject: PublishSubject<ActivateMembershipViewModel> = PublishSubject<ActivateMembershipViewModel>()
    
    private let announcementsRepository = AnnouncementsRepository.shared
    
    private let apiDataSubject: BehaviorSubject<[PaginatedResponse<Announcement>]> = BehaviorSubject(value: [])
    
    let loadMoreNeeded: PublishSubject<Void> = PublishSubject()
    let refreshSubject: PublishSubject<Void> = PublishSubject()
    let updateUnreadAnnouncementsSubject: PublishSubject<Bool> = PublishSubject()
    let unreadAnnouncementsSubject: PublishSubject<Bool> = PublishSubject()
    private let destinationsSubject: PublishSubject<AnnouncementDestination> = PublishSubject()
    
    private lazy var moreImageSelector: (Announcement) -> Void = { [unowned self] announcement in
        self.destinationsSubject.onNext(.imageGallery(images: announcement.imageUrls.imageUrls2x3))
    }
    
    private lazy var detailsSelector: (Announcement) -> Void = { [unowned self] announcement in
        self.destinationsSubject.onNext(.announcementDetails(announcementId: announcement.id))
    }

    private var announcementsObs: Observable<[Announcement]> {
        return apiDataSubject
            .map { paginatedResponses in return paginatedResponses.flatMap { $0.data }.uniqueElements }
    }
    
    override init() {
        super.init()
        initFlow()
    }
    
}

// MARK: Private logic
private extension AnnouncementsViewModel {
    
    private func initFlow() {
        loadMoreNeeded.withLatestFrom(apiDataSubject)
            .filter { !($0.last?.meta.isLastPage ?? false ) }
            .flatMapFirst { [unowned self] (currentItems: [PaginatedResponse<Announcement>]) -> Observable<AnnouncementsApiResponse> in
                let nextPageToLoad = ((currentItems.last?.meta.currentPage) ?? 0) + 1
                return self.announcementsRepository.getAnnouncements(atPage: nextPageToLoad) }
            .mapSuccessfullResponses(errorObserver: errorSubject)
            .withLatestFrom(apiDataSubject) { $1 + [$0] }
            .observeOn(MainScheduler.asyncInstance)
            .bind(to: apiDataSubject)
            .disposed(by: disposeBag)
        
        refreshSubject
            .flatMapFirst { [unowned self] _ -> Observable<AnnouncementsApiResponse> in
                return self.announcementsRepository.getAnnouncements(atPage: 1)
            }
            .mapSuccessfullResponses(errorObserver: errorSubject)
            .map { return [$0] }
            .bind(to: apiDataSubject)
            .disposed(by: disposeBag)
        
        updateUnreadAnnouncementsSubject.withLatestFrom(announcementsRepository.unreadAnnouncementsObs) { ($0, $1) }
            .filter { (shouldUpdated, isUnread) in
                return shouldUpdated && isUnread
            }.subscribe(onNext: { [unowned self] _ in
                self.refreshSubject.emitElement()
            }).disposed(by: disposeBag)
        
        announcementsRepository.unreadAnnouncementsObs
            .bind(to: unreadAnnouncementsSubject)
            .disposed(by: disposeBag)
    }
}

// MARK: Interface logic methods

extension AnnouncementsViewModel: AnnouncementsLogic {
    var unreadAnnouncementsObs: Observable<Bool> {
        return unreadAnnouncementsSubject.asObservable()
    }
    
    var isDataLoaded: Observable<Bool> {
        return apiDataSubject.map { $0.last?.meta.isLastPage ?? false }
    }
    
    var sections: Observable<[TableSection]> {
        return announcementsObs.map { [unowned self] in
            return [AnnouncementTableSection(items: $0, moreImagesSelector: self.moreImageSelector, itemSelector: self.detailsSelector)]
        }
    }
    
    var destinationsObs: Observable<AnnouncementDestination> {
        return destinationsSubject.asObservable()
    }
    
    func didTapActivateMembershipMoreButton() {
        activateMembershipViewLogic?.buttonClicked()
    }
    
    func initActivateMembershipViewLogic() {
        activateMembershipViewLogic = ActivateMembershipViewLogic(profileRepository: ProfileRepository.shared)
        
        activateMembershipViewLogic?.setupViewRelay
            .ignoreNil()
            .bind(to: setupActivateMembershipViewSubject)
            .disposed(by: disposeBag)
        
        activateMembershipViewLogic?.buttonClickedSubject
            .subscribe(onNext: { [unowned self] _ in
                self.destinationsSubject.onNext(.membershipPlans)
            }).disposed(by: disposeBag)
    }
}

// MARK: TableManagerDelegate
extension AnnouncementsViewModel: TableManagerDelegate {
    
    func didSwipeForRefresh() {
        refreshSubject.emitElement()
    }
    
    func loadMoreData() {
        loadMoreNeeded.emitElement()
    }
    
    func tableViewDidScroll(_ scrollView: UIScrollView) {
        updateUnreadAnnouncementsSubject.onNext(scrollView.isInAroundOfTop)
    }
    
}

fileprivate extension ObservableType where Element == AnnouncementsApiResponse {
    
    func mapSuccessfullResponses(errorObserver: PublishSubject<AppError>) -> Observable<PaginatedResponse<Announcement>> {
        return filterMap { [unowned errorObserver] response -> FilterMap<PaginatedResponse<Announcement>> in
            switch response {
            case .success(let successResponse):
                return .map(successResponse)
            case .failure(let error):
                errorObserver.onNext(error)
                return .ignore
            } }
    }
    
}
