//
//  AnnouncementTableViewCell.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 03/07/2019.
//

import UIKit

final class AnnouncementTableViewCell: UITableViewCell {
    
    private let maxNoTruncationLines: Int = 5
    private let minTruncation: Int = 3
    private let letterLabelSize: CGFloat = 35
    private let letterRightMargin: CGFloat = 28
    private let labelsStackHorizontalMargin: CGFloat = 21
    
    private var expectedContentLabelWidth: CGFloat {
        return UIScreen.main.bounds.width - (Constants.Margin.standard * 2)
            - letterLabelSize - letterRightMargin
            - (labelsStackHorizontalMargin * 2)
    }
    
    // MARK: Properties
    var moreImagesCallback: Function = { }
    
    // MARK: Views
    lazy var letterLabel: ParlorLabel = {
        return ParlorLabel.styled(
            withTextColor: .white,
            withFont: UIFont.custom(ofSize: Constants.FontSize.subheading, font: UIFont.Editor.bold),
            alignment: .center,
            numberOfLines: 1 ).apply {
                $0.layer.cornerRadius = letterLabelSize / 2
                $0.clipsToBounds = true
                $0.backgroundColor = .black
                $0.adjustsFontSizeToFitWidth = true
                $0.minimumScaleFactor = CGFloat.leastNonzeroMagnitude
                $0.baselineAdjustment = .alignCenters
            }
    }()
    
    lazy var cardView: UIView = {
        return UIView().manualLayoutable().apply {
            $0.backgroundColor = .textVeryLight
            $0.layer.cornerRadius = 7
        }
    }()
    
    lazy var cardStack: UIStackView = {
        return UIStackView(
            axis: .vertical,
            spacing: 0, alignment: .fill, distribution: .equalSpacing
            )
    }()
    
    lazy var labelsStackContainer: UIView = {
        return UIView()
    }()
    
    lazy var labelsStack: UIStackView = {
        return UIStackView(
            axis: .vertical,
            spacing: 0, alignment: .fill, distribution: .equalSpacing).manualLayoutable()
    }()
    
    lazy var announcementsImage: UIImageView = {
        return UIImageView().manualLayoutable().apply {
            $0.contentMode = .scaleAspectFill
            $0.clipsToBounds = true
            $0.isUserInteractionEnabled = true
        }
    }()
    
    lazy var moreImagesButton: UIButton = {
        return UIButton(type: .custom).apply {
            $0.setImage(Icons.Common.moreImages, for: .normal)
            $0.isUserInteractionEnabled = false
        }.manualLayoutable()
    }()
    
    lazy var topInfoLabel: ParlorLabel = {
       return ParlorLabel.styled(
        withTextColor: .appTextSemiLight,
        withFont: UIFont.custom(ofSize: Constants.FontSize.xxTiny, font: UIFont.Roboto.regular),
        alignment: .left,
        numberOfLines: 0)
    }()
    
    lazy var titleLabel: ParlorLabel = {
        return ParlorLabel.styled(
            withTextColor: .appTextBright,
            withFont: UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.medium),
            alignment: .left,
            numberOfLines: 0).apply {
                $0.insets.top = 6
        }
    }()
    
    lazy var contentTextView: ParlorTextView = {
        return ParlorTextView().manualLayoutable().apply {
            $0.font = UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.regular)
            $0.textColor = .appTextSemiLight
            $0.tintColor = .appTextSemiLight
            $0.letterSpacing = -0.09
            $0.lineHeight = 21
            $0.textContainerInset = UIEdgeInsets.margins(top: 10)
        }
    }()
    
    lazy var timeAgoLabel: ParlorLabel = {
        return ParlorLabel.styled(
            withTextColor: .appGreyLight,
            withFont: UIFont.custom(ofSize: Constants.FontSize.xxTiny, font: UIFont.Roboto.regular),
            alignment: .left,
            numberOfLines: 1).apply {
                $0.lineHeight = 22
        }
    }()
    
    lazy var readMoreLabel: ParlorLabel = {
        return ParlorLabel.styled(
            withTextColor: .appGreyLight,
            withFont: UIFont.custom(ofSize: Constants.FontSize.xxTiny, font: UIFont.Roboto.regular),
            alignment: .left,
            numberOfLines: 1
        )
    }()
    
    // MARK: Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        cardView.clipsToBounds = true
        selectionStyle  = .none
        announcementsImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapMoreImages)))
        readMoreLabel.text = Strings.Announcements.readMore.localized
        setupHierarchy()
        setupAutoLayout()
    }
    
    private func setupHierarchy() {
        contentView.addSubviews([letterLabel, cardView, timeAgoLabel, readMoreLabel])
        cardView.addSubviews([cardStack])
        cardStack.addArrangedSubviews([announcementsImage, labelsStackContainer])
        announcementsImage.addSubview(moreImagesButton)
        labelsStackContainer.addSubview(labelsStack)
        labelsStack.addArrangedSubviews([topInfoLabel, titleLabel, contentTextView])
    }
    
    private func setupAutoLayout() {
        
        letterLabel.apply {
            $0.widthAnchor.equalTo(constant: letterLabelSize)
            $0.heightAnchor.equalTo(constant: letterLabelSize)
            $0.leadingAnchor.equal(to: contentView.leadingAnchor, constant: Constants.Margin.standard)
            $0.topAnchor.equal(to: cardView.topAnchor)
        }
        
        announcementsImage.widthAnchor.constraint(equalTo: announcementsImage.heightAnchor, multiplier: 280.0 / 120.0).activate()
        moreImagesButton.edgesToParent(anchors: [.top, .trailing], padding: Constants.Margin.small)
        
        cardView.apply {
            $0.leadingAnchor.equal(to: letterLabel.trailingAnchor, constant: letterRightMargin)
            $0.topAnchor.equal(to: contentView.topAnchor)
            $0.trailingAnchor.equal(to: contentView.trailingAnchor, constant: -Constants.Margin.standard)
        }
        
        cardStack.edgesToParent()
        
        labelsStack.edgesToParent(insets: .margins(
            top: Constants.Margin.standard,
            left: labelsStackHorizontalMargin,
            bottom: Constants.Margin.standard,
            right: labelsStackHorizontalMargin)
        )
        
        timeAgoLabel.apply {
            $0.topAnchor.equal(to: cardView.bottomAnchor, constant: 11)
            $0.leadingAnchor.equal(to: cardView.leadingAnchor)
            $0.bottomAnchor.equal(to: contentView.bottomAnchor, constant: -34)
        }
        
        readMoreLabel.apply {
            $0.trailingAnchor.equal(to: cardView.trailingAnchor)
            $0.bottomAnchor.equal(to: timeAgoLabel.bottomAnchor)
        }
    }
    
    @objc private func didTapMoreImages() {
        moreImagesCallback()
    }
}

// MARK: Configurable Cell
extension AnnouncementTableViewCell: ConfigurableCell {
    
    typealias Model = Announcement
    
    func configure(with model: Announcement) {
        let expectedLinesCount = contentTextView.font?.expectedLinesCount(
            forUILabelWithWidth: expectedContentLabelWidth,
            text: model.message
        ) ?? maxNoTruncationLines
        
        let messageLines = numberOfLines(forTextWithExpectedLines: expectedLinesCount)
        let isGalleryHidden = model.imageUrls.imageUrls2x3.isEmpty
        
        topInfoLabel.apply {
            $0.isHidden = model.authorType == .parlor
            $0.text = model.topInfoCardText
            $0.textColor = model.colors.messageColor
        }
        
        letterLabel.apply {
            $0.text = model.initials
            $0.backgroundColor = (model.authorType == .parlor) ? UIColor.appText : model.colors.backgroundColor
            $0.textColor = (model.authorType == .parlor) ? UIColor.white : model.colors.titleColor
        }
        
        titleLabel.apply {
            $0.text = model.title
            $0.textColor = model.colors.titleColor
        }
        
        announcementsImage.apply {
            guard let imageUrl = model.imageUrls.imageUrl8x3 else {
                $0.isHidden = true
                return
            }

            $0.isHidden = false
            $0.getImage(from: imageUrl)
        }
        
        contentTextView.apply {
            $0.textColor = model.colors.messageColor
            $0.tintColor = model.colors.messageColor
            $0.textContainer.maximumNumberOfLines = messageLines
            $0.text = model.message
        }
        
        readMoreLabel.isHidden = expectedLinesCount <= maxNoTruncationLines
        cardView.backgroundColor = model.colors.backgroundColor
        timeAgoLabel.text = model.timeAgoText
        moreImagesButton.isHidden = isGalleryHidden
        announcementsImage.isUserInteractionEnabled = !isGalleryHidden
    }
    
    func numberOfLines(forTextWithExpectedLines expected: Int) -> Int {
        if expected <= maxNoTruncationLines {
            return expected
        } else if expected == maxNoTruncationLines + 1 {
            return minTruncation
        } else {
            return minTruncation + 1
        }
    }
    
}
