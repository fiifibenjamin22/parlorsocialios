//
//  AnnouncementTableSection.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 03/07/2019.
//

import UIKit

final class AnnouncementTableSection: BasicTableSection<Announcement, AnnouncementTableViewCell> {
    
    let moreImagesSelector: (Announcement) -> Void
    
    init(items: [Announcement],
         moreImagesSelector: @escaping (Announcement) -> Void,
         itemSelector: @escaping ((Announcement) -> Void) = { _ in }) {
        self.moreImagesSelector = moreImagesSelector
        super.init(items: items, itemSelector: itemSelector)
    }
    
    override func configure(cell: UITableViewCell, at indexPath: IndexPath) {
        super.configure(cell: cell, at: indexPath)
        if let cell = cell as? AnnouncementTableViewCell {
            cell.moreImagesCallback = { [unowned self] in
                self.moreImagesSelector(self.items[indexPath.row])
            }
        }
    }
    
    override func willSelectItem(at indexPath: IndexPath, cell: UITableViewCell) -> IndexPath? {
        if let cell = cell as? AnnouncementTableViewCell, cell.readMoreLabel.isHidden {
            return nil
        } else {
            return indexPath
        }
    }
    
}
