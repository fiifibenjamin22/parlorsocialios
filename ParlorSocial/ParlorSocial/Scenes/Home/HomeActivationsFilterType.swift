//
//  HomeActivationsFilterType.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 12/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

enum HomeActivationsFilterType: Int, CaseIterable {
    case all = 0
    case mixers
    case happenings
    case rsvps

    var pageIndex: Int { return self.rawValue }

    var eventName: String {
        let events = AnalyticEventClickableElementName.HomeScreen.self
        switch self {
        case .all: return events.All.tab.rawValue
        case .mixers: return events.Mixers.tab.rawValue
        case .happenings: return events.Happenings.tab.rawValue
        case .rsvps: return events.Rsvps.tab.rawValue
        }
    }
}
