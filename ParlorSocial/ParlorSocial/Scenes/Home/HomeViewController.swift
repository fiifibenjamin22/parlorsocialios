//
//  HomeViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 03/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

protocol HomeViewControllerProtocol: AnyObject {
    var isWelcomeViewHidden: Bool { get }
    func changeSelectedPage(to page: HomeActivationsFilterType)
}

class HomeViewController: NewFilterableViewController, TabBarChild, HasActivateMembershipView {

    // MARK: - Properties

    override var observableSources: ObservableSources? { return viewModel }
    override var setBottomShadow: Bool { return false }
    override var controllerTitle: String? { return Strings.Activation.all.localized.uppercased() }
    override var setTitleAsButton: Bool { return true }
    override var analyticsSearchButtonEvent: String? { return AnalyticEventClickableElementName.HomeScreen.search.rawValue }
    override var analyticsCalendarButtonEvent: String? { return AnalyticEventClickableElementName.HomeScreen.calendar.rawValue }

    var activateMembershipView: ActivateMembershipView!

    private(set) var pageController: UIPageViewController!
    private(set) var containerView: UIView!
    private(set) var topTabBarStackViewContainer: UIView!
    private(set) var topTabBarStackView: UIStackView!
    private(set) var mixersItem: TopTabBarItemView!
    private(set) var happeningsItem: TopTabBarItemView!
    private(set) var rsvpsItem: TopTabBarItemView!
    private(set) var homeWelcomeView: HomeWelcomeView!

    private(set) lazy var viewModel: HomeLogic = {
        return HomeViewModel()
    }()

    private lazy var pageItemIndicators: [UIControl?] = [titleButton, mixersItem, happeningsItem, rsvpsItem]

    private var pageViewDataSource: PageViewControllerDataSource!
    private var pageViewControllers: [UIViewController]!
    private var currentPageIndex: Int = 0

    private let pageViewDelegate: HomePageViewControllerDelegateProtocol = HomePageViewControllerDelegate() // swiftlint:disable:this weak_delegate
    private let homePageViewControllersBuilder: HomePageViewControllersBuilderProtocol = HomePageViewControllersBuilder()
    
    // MARK: - Lifecycle
    override func loadView() {
        setupViews()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTitleButton()
        setupInitialScene()
        setupWelcomeViewIfNeeded()
        setupPresentationLogic()
        viewModel.fetchScreenData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        statusBar?.backgroundColor = .clear
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.showEnableNotificationsSettingsIfNeeded(when: .beforeDashboard)
    }

    override func didTapCategoriesBarButtonItem(sender: UIControl) {
        AnalyticService.shared.saveViewClickAnalyticEvent(withName: HomeActivationsFilterType.all.eventName)
        changePage(to: HomeActivationsFilterType.all.pageIndex)
    }
    
    override func observeViewModel() {
        super.observeViewModel()
        viewModel.screenData
            .subscribe(onNext: { [weak self] screenData in
                self?.homeWelcomeView.welcomeData = screenData
            }).disposed(by: disposeBag)
        
        viewModel.profileObs
            .subscribe(onNext: { [weak self] profile in
                self?.homeWelcomeView.displayProfile = profile
            }).disposed(by: disposeBag)
        
        viewModel.destinationObs
            .subscribe(onNext: { [weak self] destination in
                self?.handle(destination)
            }).disposed(by: disposeBag)
    }

    func didTouchTabItem() {
        (children.first { $0 is ScrollableContent } as? ScrollableContent)?.scrollToTop()
        (children.first { $0 is RefreshableView } as? RefreshableView)?.refreshData()
    }
    
    override func onboardingWasClosed() {
        super.onboardingWasClosed()
        
        viewModel.showEnableNotificationsSettingsIfNeeded(when: .beforeDashboard)
    }
}

// MARK: - Private methods
extension HomeViewController {
    private func setupViews() {
        let builder = HomeViewBuilder(controller: self)
        self.view = builder.buildView()

        containerView = builder.buildView().manualLayoutable()
        topTabBarStackViewContainer = builder.buildView().manualLayoutable()
        topTabBarStackView = builder.buildTopTabBarStackView()
        mixersItem = builder.buildTopBarItem()
        happeningsItem = builder.buildTopBarItem()
        rsvpsItem = builder.buildTopBarItem()
        homeWelcomeView = builder.buildHomeWelcomeView()
        activateMembershipView = builder.buildActivateMembershipView()
        setupActivateMembershipView()

        builder.setupViews()
    }

    private func setupPresentationLogic() {
        setupPageViewController()
        setupTopBarButtons()
        bindActivateMembershipView()
        viewModel.initActivateMembershipViewLogic()
    }

    private func setupPageViewController() {
        pageViewControllers = createPageViewControllers()
        pageViewDataSource = PageViewControllerDataSource(viewControllers: pageViewControllers)
        pageViewDelegate.delegate = self

        pageController.dataSource = pageViewDataSource
        pageController.delegate = pageViewDelegate

        changePage(to: HomeActivationsFilterType.all.pageIndex)
    }

    private func setupTitleButton() {
        titleButton?.isSelected = true
    }

    private func setupTopBarButtons() {
        mixersItem.delegate = self
        happeningsItem.delegate = self
        rsvpsItem.delegate = self
    }

    private func bindActivateMembershipView() {
        viewModel.setupActivateMembershipViewSubject
            .subscribe(onNext: { [unowned self] data in
                self.activateMembershipView.setup(with: data)
            }).disposed(by: disposeBag)
    }

    private func setupInitialScene() {
        pageController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        addChild(viewController: pageController, inside: containerView)
    }

    private func setupWelcomeViewIfNeeded() {
        navigationController?.setNavigationBarHidden(true, animated: true)
        homeWelcomeView.didStartFadingOut = { [weak self] in
            self?.navigationController?.setNavigationBarHidden(false, animated: true)
        }
    }

    private func handle(_ destination: HomeDestination) {
        switch destination {
        case .notificationsReminder:
            if let onboardingVC = onboardingViewController {
                onboardingVC.router.present(destination: destination)
            } else {
                router.present(destination: destination)
            }
        case .membershipPlans:
            topRouter.push(destination: destination)
        default:
            break
        }
    }

    @objc private func didTouchWelcomeMessage() {
        homeWelcomeView.isHidden = true
    }

    private func createPageViewControllers() -> [UIViewController] {
        return HomeActivationsFilterType.allCases.map {
            switch $0 {
            case .all: return homePageViewControllersBuilder.buildAllActivationsModule(
                    router: topRouter,
                    delegate: self
                )
            case .mixers: return homePageViewControllersBuilder.buildMixersModule(
                    router: topRouter,
                    delegate: self
                )
            case .happenings: return homePageViewControllersBuilder.buildHappeningsModule(
                    router: topRouter,
                    delegate: self
                )
            case .rsvps: return homePageViewControllersBuilder.buildRSVPSModule(
                    router: topRouter,
                    delegate: self
                )
            }
        }
    }

    private func changePage(to index: Int) {
        pageController.setViewControllers(
            [pageViewControllers[index]],
            direction: currentPageIndex <= index ? .forward : .reverse,
            animated: true,
            completion: nil
        )
        currentPageIndex = index
        updatePageIndicators(for: index)
    }

    private func updatePageIndicators(for index: Int) {
        deselectAllPageIndicators()
        pageItemIndicators[index]?.isSelected = true
    }

    private func deselectAllPageIndicators() {
        pageItemIndicators.forEach { $0?.isSelected = false }
    }
}

extension HomeViewController: HomeViewControllerProtocol {
    var isWelcomeViewHidden: Bool { return homeWelcomeView.isHidden }

    func changeSelectedPage(to page: HomeActivationsFilterType) {
        changePage(to: page.pageIndex)
    }
    
    func didTapActivateMembershipMoreButton() {
        viewModel.didTapActivateMembershipMoreButton()
    }
}

extension HomeViewController: HomePageViewControllerDelegateDelegate {
    func index(of viewController: UIViewController) -> Int? {
        return pageViewControllers.firstIndex(of: viewController)
    }

    func currentPageChanged(to index: Int) {
        updatePageIndicators(for: index)
    }
}

extension HomeViewController: TopTabBarItemViewDelegate {
    func topTabBarItemView(_ topTabBarItemView: TopTabBarItemView, selected: Bool) {
        guard let index = topTabBarStackView.arrangedSubviews.firstIndex(of: topTabBarItemView) else { return }
        let pageIndex = index + 1

        guard let filterTypeTabEventName = HomeActivationsFilterType(rawValue: pageIndex)?.eventName else { return }

        AnalyticService.shared.saveViewClickAnalyticEvent(withName: filterTypeTabEventName)
        changePage(to: pageIndex)
    }
}
