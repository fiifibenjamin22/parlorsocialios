//
//  MixersViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 13.05.2020.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

final class MixersViewBuilder {
    private unowned let controller: MixersViewController
    private var view: UIView! { return controller.view }

    init(controller: MixersViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }

    private func setupProperties() {
        controller.view.backgroundColor = .darkBackground
    }

    private func setupHierarchy() {
        controller.view.addSubviews([controller.tableView])
    }

    private func setupAutoLayout() {
        controller.tableView.edgesToParent()
    }
}

extension MixersViewBuilder {
    func buildTableView() -> UITableView {
        let tableView = UITableView(frame: .zero, style: .grouped).manualLayoutable()
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.backgroundColor = .clear

        return tableView
    }

    func buildActivateMembershipView() -> ActivateMembershipView {
        return ActivateMembershipView().manualLayoutable()
    }
}
