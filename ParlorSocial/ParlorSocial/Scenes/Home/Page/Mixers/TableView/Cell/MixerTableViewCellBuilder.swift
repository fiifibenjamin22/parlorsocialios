//
//  MixerTableViewCellBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 28/02/2019.
//

import UIKit

final class MixerTableViewCellBuilder {
    private unowned let cell: MixerTableViewCell
    private var view: UIView! { return cell.contentView }

    init(cell: MixerTableViewCell) {
        self.cell = cell
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

extension MixerTableViewCellBuilder {
    private func setupProperties() {
        view.backgroundColor = .darkBackground
        cell.apply {
            $0.backgroundColor = .darkBackground
            
            [$0.locationInformationView, $0.hostInformationView, $0.timeInformationView].forEach { informationView in
                informationView.tintColor = .appGreyLight
            }

            $0.locationInformationView.icon = Icons.AllActivations.location
            $0.timeInformationView.icon = nil
            $0.hostInformationView.icon = Icons.AllActivations.host

            $0.premiumLabel.text = Strings.Activation.premium.localized
            $0.premiumLabel.setContentCompressionResistancePriority(.required, for: .horizontal)

            // TODO - PSC-6063 Hidden for 2.0 release
            $0.friendsListView.isHidden = true
        }
    }

    private func setupHierarchy() {
        cell.apply {
            [$0.activationImageView, $0.gradientView, $0.contentStackView].addTo(parent: view)

            $0.contentStackView.addArrangedSubviews([$0.informationStackView, $0.actionButton])
            $0.informationStackView.addArrangedSubviews([$0.titlesStackView, $0.subtitlesStackView])
            $0.titlesStackView.addArrangedSubviews([$0.activationType, $0.activationTitle])

            $0.subtitlesStackView.addArrangedSubviews([
                $0.locationInformationView,
                $0.hostInformationView,
                $0.timeInformationView,
                $0.premiumStackView,
                $0.friendsListView
            ])

            $0.premiumStackView.addArrangedSubviews([$0.premiumImageView, $0.premiumLabel])
        }
    }

    private func setupAutoLayout() {
        cell.apply {
            $0.activationImageView.edgesToParent(anchors: [.leading, .top, .trailing])
            $0.activationImageView.widthAnchor.constraint(equalTo: $0.activationImageView.heightAnchor, multiplier: 1).activate()

            $0.contentStackView.edgesToParent(anchors: [.leading, .trailing, .bottom], insets: UIEdgeInsets.margins(left: Constants.Margin.standard, bottom: 42, right: Constants.Margin.standard))
            $0.contentStackView.topAnchor.equal(to: $0.activationImageView.bottomAnchor, constant: -53)

            $0.gradientView.edgesToParent(anchors: [.leading, .trailing])
            $0.gradientView.bottomAnchor.equal(to: $0.activationImageView.bottomAnchor)
            $0.gradientView.heightAnchor.constraint(equalTo: $0.activationImageView.heightAnchor, multiplier: 2.0 / 3).activate()
            
            $0.premiumImageView.widthAnchor.equalTo(constant: Constants.MixerTableViewCell.premiumImageSize)

            $0.friendsListView.heightAnchor.equalTo(constant: Constants.MixerTableViewCell.FriendsListView.height)
            
            $0.actionButton.widthAnchor.equalTo(constant: Constants.MixerTableViewCell.ActionButton.width)
            $0.actionButton.heightAnchor.equalTo(constant: Constants.MixerTableViewCell.ActionButton.height)
        }
    }
}

extension MixerTableViewCellBuilder {
    func buildActivationImageView() -> UIImageView {
        let imageView = UIImageView().manualLayoutable()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true

        return imageView
    }

    func buildGradientView() -> GradientView {
        return GradientView()
    }

    func buildActivationTypeLabel() -> UILabel {
        let label = UILabel.styled(
            textColor: .white,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.subheading,
                font: UIFont.Dalmatins.regular))
        
        label.setContentCompressionResistancePriority(.required, for: .horizontal)
        return label
    }

    func buildTitleLabel() -> UILabel {
        let label =  UILabel.styled(
            textColor: .white,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.largeTitle,
                font: UIFont.Editor.medium))
        
        label.setContentCompressionResistancePriority(.required, for: .horizontal)
        return label
    }

    func buildInforamtionView() -> InformationView {
        let informationView = InformationView()

        return informationView
    }

    func buildView() -> UIView {
        return UIView().manualLayoutable()
    }

    func buildTitlesStackView() -> UIStackView {
        return UIStackView(axis: .vertical, spacing: 40, alignment: .center)
    }

    func buildDetailsStackView() -> UIStackView {
        return UIStackView(axis: .vertical, spacing: 10, alignment: .center)
    }

    func buildPremiumStackView() -> UIStackView {
        return UIStackView(axis: .horizontal, spacing: 5)
    }

    func buildInformationStackView() -> UIStackView {
        return UIStackView(axis: .vertical, spacing: 20)
    }

    func buildContentStackView() -> UIStackView {
        return UIStackView(axis: .vertical, spacing: 40, alignment: .center)
    }

    func buildPremiumLabel() -> UILabel {
        return UILabel.styled(
            textColor: .appGold,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.small,
                font: UIFont.Roboto.regular),
            alignment: .left)
    }

    func buildPremiumImageView() -> UIImageView {
        let imageView = UIImageView().manualLayoutable()
        imageView.contentMode = .scaleAspectFit
        imageView.image = Icons.AllActivations.premium
        imageView.tintColor = .appGold
        imageView.clipsToBounds = true

        return imageView
    }

    func buildActionButton() -> ParlorButton {
        let button = ParlorButton()
        button.layer.cornerRadius = Constants.MixerTableViewCell.ActionButton.height / 2
        return button.manualLayoutable()
    }

    func buildFriendsListView() -> FriendsListView {
        return FriendsListView()
    }
}

private extension Constants {
    enum MixerTableViewCell {
        enum FriendsListView {
            static let height: CGFloat = 30
        }

        enum ActionButton {
            static let height: CGFloat = 50
            static let width: CGFloat = 197
        }

        static let premiumImageSize: CGFloat = 11
    }
}
