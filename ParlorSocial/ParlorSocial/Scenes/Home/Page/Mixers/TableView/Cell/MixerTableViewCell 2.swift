//
//  MixerTableViewCell.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 28/02/2019.
//

import UIKit

final class MixerTableViewCell: UITableViewCell {

    // MARK: Properties
    private(set) var actionButtonDidTapClosure: (() -> Void)?

    private(set) var activationImageView: UIImageView!
    private(set) var activationTitle: UILabel!
    private(set) var activationType: UILabel!

    private(set) var hostInformationView: InformationView!
    private(set) var locationInformationView: InformationView!
    private(set) var timeInformationView: InformationView!

    private(set) var premiumStackView: UIStackView!
    private(set) var premiumImageView: UIImageView!
    private(set) var premiumLabel: UILabel!

    private(set) var titlesStackView: UIStackView!
    private(set) var subtitlesStackView: UIStackView!
    private(set) var informationStackView: UIStackView!
    private(set) var contentStackView: UIStackView!

    private(set) var actionButtonContainer: UIView!
    private(set) var actionButton: ParlorButton!

    private(set) var friendsListView: FriendsListView!
    
    private(set) var gradientView: GradientView!

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initializeCell()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.autoresizingMask = .flexibleHeight
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        actionButtonDidTapClosure = nil
        actionButton.resetLayout()
    }
}

// MARK: - Public methods
extension MixerTableViewCell: ConfigurableCell {
    typealias Model = HomeActivation
    
    func configure(with model: HomeActivation) {
        activationType.text = model.summary
        activationTitle.text = model.name
        activationImageView.getImage(from: model.imageURL)
        
        locationInformationView.informationLabel.text = model.location?.name
        hostInformationView.isHidden = model.mainHost == nil
        hostInformationView.informationLabel.text = "\(Strings.Activation.hostedBy.localized) \(model.mainHost?.fullName ?? "")"
        timeInformationView.informationLabel.text = model.startAt
            .toMixerStartDateFormat(isApproximatedStartTime: model.isApproximatedStartTime)
            .checkIfVersionAppIsPreview()

        premiumStackView.isHidden = !model.isPremium

        setupButton(for: model.actionButtonType)

        switch model.actionButtonType {
        case .rsvpd: setupButtonImageForRsvpdType()
        case .guestList: setupButtonImageForGuestListType()
        default: break
        }

        premiumStackView.isHidden = !model.isPremium

        friendsListView.set(imageUrls: model.attendingFriendsInfo.imageUrls, title: model.attendingFriendsInfo.text)
    }

    private func setupButtonImageForGuestListType() {
        actionButton.semanticContentAttribute = .forceRightToLeft
        actionButton.setImage(Icons.AllActivations.guestList, for: .normal)
        actionButton.imageEdgeInsets = Constants.MixerTableViewCell.ActionButton.GuestList.imageEdgeInsets
        actionButton.titleEdgeInsets = Constants.MixerTableViewCell.ActionButton.GuestList.titleEdgeInsets
    }

    private func setupButtonImageForRsvpdType() {
        actionButton.setImage(Icons.AllActivations.check, for: .normal)
        actionButton.titleEdgeInsets = Constants.MixerTableViewCell.ActionButton.Rsvpd.titleEdgeInsets
        actionButton.imageEdgeInsets = Constants.MixerTableViewCell.ActionButton.Rsvpd.imageEdgeInsets
    }

    private func setupButton(for type: ActionButtonType) {
        actionButton.style = buttonStyle(for: type)
        actionButton.title = type.title
    }

    private func buttonStyle(for type: ActionButtonType) -> ParlorButtonStyle {
        switch type {
        case .guestList:
            return .white(font: UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.medium))
        case .live:
            return .filledWith(
                color: UIColor.backgroundGreen,
                titleColor: UIColor.appText,
                font: UIFont.custom(ofSize: Constants.FontSize.subbody, font: UIFont.Roboto.medium),
                letterSpacing: Constants.LetterSpacing.none
            )
        default: return type.defaultStyle
        }
    }

    func setActionButtonDidTapClosure(_ closure: @escaping (() -> Void)) {
        actionButtonDidTapClosure = closure
        actionButton.setTarget(self, action: #selector(actionButtonDidTap), for: .touchUpInside)
    }

    @objc private func actionButtonDidTap() {
        actionButtonDidTapClosure?()
    }
}

// MARK: - Private methods
extension MixerTableViewCell {

    private func initializeCell() {
        clipsToBounds = true
        selectionStyle = .none
        setupViews()
    }

    private func setupViews() {
        let builder = MixerTableViewCellBuilder(cell: self)

        activationImageView = builder.buildActivationImageView()
        activationTitle = builder.buildTitleLabel()
        activationType = builder.buildActivationTypeLabel()
        
        locationInformationView = builder.buildInforamtionView()
        timeInformationView = builder.buildInforamtionView()
        hostInformationView = builder.buildInforamtionView()
        
        premiumStackView = builder.buildPremiumStackView()
        premiumImageView = builder.buildPremiumImageView()
        premiumLabel = builder.buildPremiumLabel()

        friendsListView = builder.buildFriendsListView()

        titlesStackView = builder.buildTitlesStackView()
        subtitlesStackView = builder.buildDetailsStackView()
        informationStackView = builder.buildInformationStackView()
        contentStackView = builder.buildContentStackView()

        gradientView = builder.buildGradientView()

        actionButton = builder.buildActionButton()
        actionButtonContainer = builder.buildView()
        
        builder.setupViews()
    }
}

private extension Constants {
    enum MixerTableViewCell {
        enum ActionButton {
            enum Rsvpd {
                static let imageEdgeInsets = UIEdgeInsets(top: 0, left: -1, bottom: 0, right: 3)
                static let titleEdgeInsets = UIEdgeInsets(top: 0, left: 3, bottom: 0, right: -3)
            }

            enum GuestList {
                static let titleEdgeInsets = UIEdgeInsets(top: 0, left: 17, bottom: 0, right: 0)
                static let imageEdgeInsets = UIEdgeInsets(top: 0, left: 55, bottom: 0, right: 0)
            }
        }
    }
}
