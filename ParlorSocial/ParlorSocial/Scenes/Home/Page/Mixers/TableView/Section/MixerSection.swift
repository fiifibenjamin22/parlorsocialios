//
//  MixerSection.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 13/05/2020.
//

import UIKit

final class MixerSection: BasicTableSection<HomeActivation, MixerTableViewCell> {
    private let buttonSelector: ((HomeActivation) -> Void)

    init(
        items: [HomeActivation],
        itemSelector: @escaping (HomeActivation) -> Void,
        buttonSelector: @escaping ((HomeActivation) -> Void) = { _ in }
    ) {
        self.buttonSelector = buttonSelector
        super.init(items: items, itemSelector: itemSelector)
    }

    override func configure(cell: UITableViewCell, at indexPath: IndexPath) {
        if let cell = cell as? MixerTableViewCell {
            cell.setActionButtonDidTapClosure { [unowned self] in
                self.buttonSelector(self.items[indexPath.item])
            }
        }
        super.configure(cell: cell, at: indexPath)
    }
}

extension MixerSection {
    static func make(
        for activations: [HomeActivation],
        itemSelector: @escaping (HomeActivation) -> Void,
        buttonSelector: @escaping ((HomeActivation) -> Void)
    ) -> [TableSection] {
        var mutableActivations = activations

        mutableActivations.sort {
            return $0.startAt.compare($1.startAt) == .orderedAscending
        }

        return [MixerSection(items: mutableActivations, itemSelector: itemSelector, buttonSelector: buttonSelector)]
    }

}
