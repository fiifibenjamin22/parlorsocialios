//
//  HomeEmptyDataTableViewCell.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 28/02/2019.
//

import UIKit

final class HomeEmptyDataTableViewCell: UITableViewCell {

    // MARK: Properties
    var allActivationsButtonDidTapClosure: (() -> Void)?
    private(set) var emptyDataView: HomeEmptyDataView!

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initializeCell()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.autoresizingMask = .flexibleHeight
    }
}

// MARK: - Public methods
extension HomeEmptyDataTableViewCell: ConfigurableCell {
    typealias Model = HomeEmptyDataViewStyle

    func configure(with model: HomeEmptyDataViewStyle) {
        let builder = HomeEmptyDataTableViewCellBuilder(cell: self)
        emptyDataView = builder.buildEmptyDataView(with: model)
        emptyDataView.allActivationsButton.addTarget(
            self,
            action: #selector(actionButtonDidTap),
            for: .touchUpInside
        )
        builder.setupViews()
    }

    @objc private func actionButtonDidTap() {
        allActivationsButtonDidTapClosure?()
    }
}

// MARK: - Private methods
extension HomeEmptyDataTableViewCell {
    private func initializeCell() {
        clipsToBounds = true
        selectionStyle = .none
    }
}
