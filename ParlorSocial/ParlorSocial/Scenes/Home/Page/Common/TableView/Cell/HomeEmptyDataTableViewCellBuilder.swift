//
//  HomeEmptyDataTableViewCellBuilder.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 27/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class HomeEmptyDataTableViewCellBuilder {
    private unowned let cell: HomeEmptyDataTableViewCell
    private var view: UIView! { return cell.contentView }

    init(cell: HomeEmptyDataTableViewCell) {
        self.cell = cell
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

extension HomeEmptyDataTableViewCellBuilder {
    private func setupProperties() {
        view.backgroundColor = .clear
    }

    private func setupHierarchy() {
        view.addSubview(cell.emptyDataView)
    }

    private func setupAutoLayout() {
        cell.emptyDataView.edgesToParent()
    }
}

extension HomeEmptyDataTableViewCellBuilder {
    func buildEmptyDataView(with style: HomeEmptyDataViewStyle) -> HomeEmptyDataView {
        return HomeEmptyDataView(style: style).manualLayoutable()
    }
}
