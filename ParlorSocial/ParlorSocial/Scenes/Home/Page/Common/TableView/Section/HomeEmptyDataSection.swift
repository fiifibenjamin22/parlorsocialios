//
//  HomeEmptyDataSection.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 27/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class HomeEmptyDataSection: BasicTableSection<HomeEmptyDataViewStyle, HomeEmptyDataTableViewCell> {
    private let buttonSelector: (() -> Void)

    init(item: HomeEmptyDataViewStyle, buttonSelector: @escaping (() -> Void)) {
        self.buttonSelector = buttonSelector

        super.init(items: [item])
    }

    override func configure(cell: UITableViewCell, at indexPath: IndexPath) {
        if let cell = cell as? HomeEmptyDataTableViewCell {
            cell.allActivationsButtonDidTapClosure = buttonSelector
        }

        super.configure(cell: cell, at: indexPath)
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.height
    }
}

extension HomeEmptyDataSection {
    static func make(for style: HomeEmptyDataViewStyle, buttonSelector: @escaping (() -> Void)) -> [TableSection] {
        return [HomeEmptyDataSection(item: style, buttonSelector: buttonSelector)]
    }
}
