//
//  HomeEmptyDataView.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 26/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class HomeEmptyDataView: UIView {
    private(set) var backgroundImageView: UIImageView!
    private(set) var stackView: UIStackView!
    private(set) var emptyActivationsLabel: UILabel!
    private(set) var allActivationsButtonContainer: UIView!
    private(set) var allActivationsButton: ParlorButton!

    init(style: HomeEmptyDataViewStyle) {
        super.init(frame: .zero)

        initialize(with: style)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Public methods
extension HomeEmptyDataView {
    func setAllActivationsButtonTarget(_ target: Any?, action: Selector, for events: UIControl.Event) {
        allActivationsButton.addTarget(target, action: action, for: events)
    }
}

// MARK: - Private methods
extension HomeEmptyDataView {
    private func initialize(with style: HomeEmptyDataViewStyle) {
        setupViews()
        setup(with: style.appearance)
    }

    private func setupViews() {
        let builder = HomeEmptyDataViewBuilder(view: self)

        backgroundImageView = builder.buildImageView()
        stackView = builder.buildStackView()
        emptyActivationsLabel = builder.buildLabel()
        allActivationsButtonContainer = builder.buildView()
        allActivationsButton = builder.buildButton()

        builder.setupViews()
    }

    private func setup(with appearance: HomeEmptyDataViewAppearance) {
        backgroundColor = appearance.backgroundColor
        backgroundImageView.image = appearance.backgroundImage
        emptyActivationsLabel.text = appearance.emptyActivationsLabelText
        emptyActivationsLabel.textColor = appearance.emptyActivationsLabelColor
        allActivationsButton.style = appearance.allActivationsButtonStyle
        allActivationsButton.title = appearance.allActivationsButtonTitle
    }
}
