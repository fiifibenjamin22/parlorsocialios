//
//  HomeEmptyDataViewStyle.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 26/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

struct HomeEmptyDataViewAppearance {
    let backgroundColor: UIColor
    let backgroundImage: UIImage?
    let emptyActivationsLabelText: String
    let emptyActivationsLabelColor: UIColor
    let allActivationsButtonTitle: String
    let allActivationsButtonStyle: ParlorButtonStyle
}

enum HomeEmptyDataViewStyle {
    case mixers, happenings, rsvps

    var appearance: HomeEmptyDataViewAppearance {
        let strings = Strings.Home.EmptyDataView.self
        switch self {
        case .mixers:
            return HomeEmptyDataViewAppearance(
                backgroundColor: UIColor.black,
                backgroundImage: Icons.Home.mixersEmptyData,
                emptyActivationsLabelText: strings.Mixers.label.localized,
                emptyActivationsLabelColor: UIColor.white,
                allActivationsButtonTitle: strings.Mixers.button.localized,
                allActivationsButtonStyle: .white(font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium))
            )
        case .happenings:
            return HomeEmptyDataViewAppearance(
                backgroundColor: UIColor.black,
                backgroundImage: Icons.Home.happeningsEmptyData,
                emptyActivationsLabelText: strings.Happenings.label.localized,
                emptyActivationsLabelColor: UIColor.white,
                allActivationsButtonTitle: strings.Happenings.button.localized,
                allActivationsButtonStyle: .white(font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium))
            )
        case .rsvps:
            return HomeEmptyDataViewAppearance(
                backgroundColor: UIColor.appBackground,
                backgroundImage: nil,
                emptyActivationsLabelText: strings.Rsvps.label.localized,
                emptyActivationsLabelColor: UIColor.appGreyLight,
                allActivationsButtonTitle: strings.Rsvps.button.localized,
                allActivationsButtonStyle: .outlined(
                    titleColor: UIColor.appTextMediumBright,
                    borderColor: UIColor.emptyDataRsvpBorder,
                    font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium),
                    letterSpacing: Constants.LetterSpacing.medium
                )
            )
        }
    }
}
