//
//  HomeEmptyDataViewBuilder.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 26/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class HomeEmptyDataViewBuilder {
    private unowned let view: HomeEmptyDataView

    init(view: HomeEmptyDataView) {
        self.view = view
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

extension HomeEmptyDataViewBuilder {
    private func setupProperties() {
        view.backgroundColor = .black
        view.backgroundImageView.contentMode = .scaleAspectFill
    }

    private func setupHierarchy() {
        [view.backgroundImageView, view.stackView].addTo(parent: view)
        view.stackView.addArrangedSubviews([view.emptyActivationsLabel, view.allActivationsButtonContainer])
        view.allActivationsButtonContainer.addSubview(view.allActivationsButton)
    }

    private func setupAutoLayout() {
        view.apply {
            $0.backgroundImageView.edgesToParent()

            $0.stackView.centerXAnchor.equal(to: $0.centerXAnchor)
            $0.stackView.centerYAnchor.equal(to: $0.centerYAnchor)

            $0.allActivationsButton.centerXAnchor.equal(to: $0.allActivationsButtonContainer.centerXAnchor)
            $0.allActivationsButton.heightAnchor.equalTo(constant: Constants.HomeEmptyDataView.AllActivationsButton.height)
            $0.allActivationsButton.widthAnchor.equalTo(constant: Constants.HomeEmptyDataView.AllActivationsButton.width)
            $0.allActivationsButton.edgesToParent(anchors: [.top, .bottom])
        }
    }
}

extension HomeEmptyDataViewBuilder {
    func buildLabel() -> UILabel {
        return ParlorLabel.styled(
            withTextColor: .white,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.small,
                font: UIFont.Roboto.regular
            ),
            letterSpacing: Constants.LetterSpacing.small
        )
    }

    func buildStackView() -> UIStackView {
        return UIStackView(axis: .vertical, spacing: 20, distribution: .fill)
    }

    func buildImageView() -> UIImageView {
        return UIImageView().manualLayoutable()
    }

    func buildView() -> UIView {
        return UIView().manualLayoutable()
    }

    func buildButton() -> ParlorButton {
        return ParlorButton().apply {
            $0.layer.cornerRadius = Constants.HomeEmptyDataView.AllActivationsButton.height / 2
        }.manualLayoutable()
    }
}

private extension Constants {
    enum HomeEmptyDataView {
        enum AllActivationsButton {
            static let height: CGFloat = 40
            static let width: CGFloat = 200

        }
    }
}
