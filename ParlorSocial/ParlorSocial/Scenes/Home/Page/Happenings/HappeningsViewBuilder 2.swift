//
//  HappeningsViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 13.05.2020.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

final class HappeningsViewBuilder {
    private unowned let controller: HappeningsViewController
    private var view: UIView! { return controller.view }

    init(controller: HappeningsViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }

    private func setupProperties() {
        controller.view.backgroundColor = .darkBackground
    }

    private func setupHierarchy() {
        controller.view.addSubviews([controller.tableView])
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.tableView.edgesToParent()
        }
    }
}

extension HappeningsViewBuilder {
    func buildTableView() -> UITableView {
        let tableView = UITableView(frame: .zero, style: .grouped).manualLayoutable()
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.backgroundColor = .clear
        tableView.contentInset = UIEdgeInsets.margins(top: -CGFloat.leastPositiveForTableView)

        return tableView
    }

    func buildActivateMembershipView() -> ActivateMembershipView {
        return ActivateMembershipView().manualLayoutable()
    }
}
