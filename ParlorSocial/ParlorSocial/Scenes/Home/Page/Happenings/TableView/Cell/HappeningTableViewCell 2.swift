//
//  HappeningTableViewCell.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 13/05/2020.
//

import UIKit

final class HappeningTableViewCell: UITableViewCell {
    // MARK: Properties
    private(set) var actionButtonDidTapClosure: (() -> Void)?

    private(set) var activationImageView: UIImageView!
    private(set) var activationTitle: UILabel!

    private(set) var locationInformationView: InformationView!
    private(set) var timeInformationView: InformationView!
    private(set) var hostInformationView: InformationView!
    private(set) var premiumInformationView: InformationView!
    private(set) var friendsListView: FriendsListView!

    private(set) var titlesStackView: UIStackView!
    private(set) var descriptionStackView: UIStackView!
    private(set) var subtitlesStackView: UIStackView!
    private(set) var informationStackView: UIStackView!
    private(set) var contentStackView: UIStackView!

    private(set) var actionButtonContainer: UIView!
    private(set) var actionButton: ParlorButton!
    
    private(set) var gradientView: GradientView!

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initializeCell()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.autoresizingMask = .flexibleHeight
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        actionButton.setImage(nil, for: .normal)
        actionButton.titleEdgeInsets = .zero
        actionButton.imageEdgeInsets = .zero
    }
}

// MARK: - Public methods
extension HappeningTableViewCell: ConfigurableCell {
    typealias Model = HomeActivation
    
    func configure(with model: HomeActivation) {
        activationTitle.text = model.name
        activationImageView.getImage(from: model.imageURL)
        
        locationInformationView.title = model.location?.name
        hostInformationView.isHidden = model.mainHost == nil
        hostInformationView.title = "\(Strings.Activation.hostedBy.localized) \(model.mainHost?.fullName ?? "")"
        
        timeInformationView.informationLabel.text = model.startAt
            .toMixerStartDateFormat(isApproximatedStartTime: model.isApproximatedStartTime)
            .checkIfVersionAppIsPreview()

        premiumInformationView.isHidden = !model.isPremium

        setupButton(for: model.actionButtonType)

        switch model.actionButtonType {
        case .rsvpd: setupButtonImageForRsvpdType()
        default: break
        }

        premiumInformationView.isHidden = !model.isPremium

        friendsListView.set(imageUrls: model.attendingFriendsInfo.imageUrls, title: model.attendingFriendsInfo.text)
    }

    private func setupButton(for type: ActionButtonType) {
        actionButton.style = type.defaultStyle
        actionButton.title = type.title
    }

    private func setupButtonImageForRsvpdType() {
        actionButton.setImage(Icons.AllActivations.check, for: .normal)
        actionButton.titleEdgeInsets = Constants.HappeningTableViewCell.ActionButton.Rsvpd.titleEdgeInsets
        actionButton.imageEdgeInsets = Constants.HappeningTableViewCell.ActionButton.Rsvpd.imageEdgeInsets
    }

    func setActionButtonDidTapClosure(_ closure: @escaping (() -> Void)) {
        actionButtonDidTapClosure = closure
        actionButton.setTarget(self, action: #selector(actionButtonDidTap), for: .touchUpInside)
    }

    @objc private func actionButtonDidTap() {
        actionButtonDidTapClosure?()
    }
}

// MARK: - Private methods
extension HappeningTableViewCell {

    private func initializeCell() {
        clipsToBounds = true
        selectionStyle = .none
        setupViews()
    }

    private func setupViews() {
        let builder = HappeningTableViewCellBuilder(cell: self)

        activationImageView = builder.buildActivationImageView()
        activationTitle = builder.buildTitleLabel()
        
        locationInformationView = builder.buildInforamtionView()
        timeInformationView = builder.buildInforamtionView()
        hostInformationView = builder.buildInforamtionView()
        premiumInformationView = builder.buildInforamtionView()

        friendsListView = builder.buildFriendsListView()

        titlesStackView = builder.buildTitlesStackView()
        descriptionStackView = builder.buildDescriptionStackView()
        subtitlesStackView = builder.buildSubtitlesStackView()
        informationStackView = builder.buildInformationStackView()
        contentStackView = builder.buildContentStackView()
        
        gradientView = builder.buildGradientView()

        actionButton = builder.buildActionButton()
        actionButtonContainer = builder.buildView()
        
        builder.setupViews()
    }
}

private extension Constants {
    enum HappeningTableViewCell {
        enum ActionButton {
            enum Rsvpd {
                static let imageEdgeInsets = UIEdgeInsets(top: 0, left: -1, bottom: 0, right: 3)
                static let titleEdgeInsets = UIEdgeInsets(top: 0, left: 3, bottom: 0, right: -3)
            }
        }
    }
}
