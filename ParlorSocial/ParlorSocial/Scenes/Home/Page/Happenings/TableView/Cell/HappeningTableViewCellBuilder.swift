//
//  HappeningTableViewCellBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 13/05/2020.
//

import UIKit

final class HappeningTableViewCellBuilder {
    private unowned let cell: HappeningTableViewCell
    private var view: UIView! { return cell.contentView }

    init(cell: HappeningTableViewCell) {
        self.cell = cell
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }

}

extension HappeningTableViewCellBuilder {
    private func setupProperties() {
        view.backgroundColor = .darkBackground
        cell.apply {
            $0.backgroundColor = .darkBackground
            $0.locationInformationView.tintColor = .appGreyLight
            $0.hostInformationView.tintColor = .appGreyLight
            $0.premiumInformationView.tintColor = .appGold
            $0.timeInformationView.tintColor = .eventTime
            
            let informationFont = UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.regular)
            $0.locationInformationView.font = informationFont
            $0.hostInformationView.font = informationFont
            $0.premiumInformationView.font = informationFont
            $0.timeInformationView.font = informationFont

            $0.locationInformationView.icon = Icons.AllActivations.location
            $0.hostInformationView.icon = Icons.AllActivations.host
            $0.premiumInformationView.icon = Icons.AllActivations.premium
            $0.timeInformationView.icon = nil

            $0.premiumInformationView.title = Strings.Activation.premium.localized

            // TODO - PSC-6063 Hidden for 2.0 release
            $0.friendsListView.isHidden = true
        }
    }

    private func setupHierarchy() {
        cell.apply {
            [$0.activationImageView, $0.gradientView, $0.contentStackView].addTo(parent: view)

            $0.contentStackView.addArrangedSubviews([$0.titlesStackView, $0.informationStackView, $0.actionButtonContainer])
            $0.titlesStackView.addArrangedSubviews([$0.timeInformationView, $0.activationTitle])
            $0.informationStackView.addArrangedSubviews([$0.descriptionStackView, $0.actionButtonContainer])
            $0.descriptionStackView.addArrangedSubviews([
                $0.subtitlesStackView,
                $0.friendsListView
            ])
            $0.subtitlesStackView.addArrangedSubviews([
                $0.locationInformationView,
                $0.hostInformationView,
                $0.premiumInformationView
            ])

            $0.actionButtonContainer.addSubview($0.actionButton)
        }
    }

    private func setupAutoLayout() {
        cell.apply {
            $0.activationImageView.edgesToParent(anchors: [.leading, .top, .trailing])
            $0.activationImageView.heightAnchor.constraint(equalTo: $0.activationImageView.widthAnchor, multiplier: 0.75).activate()

            $0.contentStackView.edgesToParent(anchors: [.leading, .trailing, .bottom], insets: UIEdgeInsets.margins(left: 25, bottom: 65, right: 29))
            $0.contentStackView.topAnchor.equal(to: $0.activationImageView.bottomAnchor, constant: 35)

            $0.gradientView.edgesToParent(anchors: [.leading, .trailing])
            $0.gradientView.bottomAnchor.equal(to: $0.activationImageView.bottomAnchor)
            $0.gradientView.heightAnchor.constraint(equalTo: $0.activationImageView.heightAnchor, multiplier: 2.0 / 3).activate()
            
            $0.actionButton.edgesToParent(anchors: [.leading, .top, .trailing])
            $0.actionButton.bottomAnchor.lessThanOrEqual(to: $0.actionButtonContainer.bottomAnchor)
            $0.actionButton.widthAnchor.equalTo(constant: Constants.HappeningTableViewCell.ActionButton.width)
            $0.actionButton.heightAnchor.equalTo(constant: Constants.HappeningTableViewCell.ActionButton.height)

            $0.friendsListView.heightAnchor.equalTo(constant: Constants.HappeningsCell.FriendsListView.height)
        }
    }
}

extension HappeningTableViewCellBuilder {
    func buildActivationImageView() -> UIImageView {
        let imageView = UIImageView().manualLayoutable()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true

        return imageView
    }

    func buildGradientView() -> GradientView {
        return GradientView()
    }

    func buildTitleLabel() -> UILabel {
        let label = UILabel.styled(
            textColor: .white,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.title,
                font: UIFont.Editor.medium),
            alignment: .left)

        label.setContentCompressionResistancePriority(.required, for: .horizontal)
        return label
    }

    func buildInforamtionView() -> InformationView {
        let informationView = InformationView()
        informationView.clipIconToTop = false
        return informationView
    }

    func buildView() -> UIView {
        return UIView().manualLayoutable()
    }

    func buildTitlesStackView() -> UIStackView {
        return UIStackView(axis: .vertical, spacing: 7, alignment: .leading)
    }

    func buildDescriptionStackView() -> UIStackView {
        return UIStackView(axis: .vertical, spacing: Constants.HappeningsCell.DescriptionStackView.spacing, alignment: .leading)
    }

    func buildSubtitlesStackView() -> UIStackView {
        return UIStackView(axis: .vertical, spacing: 6, alignment: .leading)
    }

    func buildInformationStackView() -> UIStackView {
        return UIStackView(axis: .horizontal, spacing: Constants.Margin.standard)
    }

    func buildContentStackView() -> UIStackView {
        return UIStackView(axis: .vertical, spacing: 20)
    }

    func buildFriendsListView() -> FriendsListView {
        return FriendsListView()
    }

    func buildActionButton() -> ParlorButton {
        let button = ParlorButton()
        button.layer.cornerRadius = Constants.HappeningTableViewCell.ActionButton.height / 2
        return button.manualLayoutable()
    }
}

private extension Constants {
    enum HappeningTableViewCell {
        enum ActionButton {
            static let height: CGFloat = 40
            static let width: CGFloat = 100
        }
    }
}

private extension Constants {
    enum HappeningsCell {
        enum FriendsListView {
            static let height: CGFloat = 30.0
        }

        enum DescriptionStackView {
            static let spacing: CGFloat = 14.0
        }
    }
}
