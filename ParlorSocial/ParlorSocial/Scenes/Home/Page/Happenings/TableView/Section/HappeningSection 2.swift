//
//  HappeningSection.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 02/04/2019.
//

import UIKit

final class HappeningSection: BasicTableSection<HomeActivation, HappeningTableViewCell> {
    private let buttonSelector: ((HomeActivation) -> Void)

    init(
        items: [HomeActivation],
        itemSelector: @escaping (HomeActivation) -> Void,
        buttonSelector: @escaping ((HomeActivation) -> Void) = { _ in }
    ) {
        self.buttonSelector = buttonSelector
        super.init(items: items, itemSelector: itemSelector)
    }

    override func configure(cell: UITableViewCell, at indexPath: IndexPath) {
        if let cell = cell as? HappeningTableViewCell {
            cell.setActionButtonDidTapClosure { [unowned self] in
                self.buttonSelector(self.items[indexPath.item])
            }
        }

        super.configure(cell: cell, at: indexPath)
    }
}

extension HappeningSection {
    
    static func make(
        for activations: [HomeActivation],
        itemSelector: @escaping (HomeActivation) -> Void,
        buttonSelector: @escaping ((HomeActivation) -> Void)
    ) -> [TableSection] {
        var mutableHomeActivations = activations
        
        mutableHomeActivations.sort {
            return $0.startAt.compare($1.startAt) == .orderedAscending
        }
        
        return [HappeningSection(items: mutableHomeActivations, itemSelector: itemSelector, buttonSelector: buttonSelector)]
    }
    
}
