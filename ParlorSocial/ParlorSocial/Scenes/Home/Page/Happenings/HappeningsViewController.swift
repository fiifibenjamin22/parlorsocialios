//
//  HappeningsViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 27.02.2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

final class HappeningsViewController: AppViewController, ScrollableContent {
    
    // MARK: - Properties
    let analyticEventScreen: AnalyticEventScreen? = .happeningsList

    override var observableSources: ObservableSources? { return self }
    override var connectedOnboardingType: OnboardingType? { return .happenings }

    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }

    var viewModel: HappeningsViewModelType!

    weak var delegate: HomeViewControllerProtocol?

    private(set) var containerView: UIView!
    private(set) var tableView: UITableView!

    private var tableManager: TableViewManager!
    private var isWelcomeViewHidden: Bool { return delegate?.isWelcomeViewHidden ?? true }

    // MARK: - Lifecycle

    init(router: Router) {
        super.init(nibName: nil, bundle: nil)

        self.router = router
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()

        tableManager = TableViewManager(tableView: tableView, delegate: self)
        tableManager.setupRefreshControl()
        setupPresentationLogic()
    }
}

// MARK: - Public methods
extension HappeningsViewController {
    func scrollToTop() {
        tableManager.scrollToTop()
    }
}

// MARK: - Private methods
extension HappeningsViewController {
    private func setupViews() {
        let builder = HappeningsViewBuilder(controller: self)

        self.tableView = builder.buildTableView()
        builder.setupViews()
    }

    private func setupPresentationLogic() {
        setupTableView()
        bindActivationsState()
        bindDestinationObservable()
        bindChangeToAllActivationsObservable()
    }

    private func setupTableView() {
        tableManager.footerStyle = .dark
        tableManager.refreshControl?.tintColor = .white
    }

    private func bindDestinationObservable() {
        viewModel.destinationsObs
            .filter { [unowned self] _ in self.isWelcomeViewHidden }
            .subscribe(onNext: { [unowned self] destination in
                self.router.push(destination: destination)
            }).disposed(by: disposeBag)
    }

    private func bindActivationsState() {
        viewModel.activationSectionsRelay
            .subscribe(
                onNext: { [unowned self] state in
                    self.handle(activationsState: state)
                },
                onError: { error in
                    print(error)
                }
            ).disposed(by: disposeBag)
    }

    private func bindChangeToAllActivationsObservable() {
        viewModel.changeToAllActivationsObs
            .subscribe(onNext: { [unowned self] in
                self.delegate?.changeSelectedPage(to: .all)
            }).disposed(by: disposeBag)
    }

    private func handle(activationsState state: SectionPaginatedRepositoryState) {
        switch state {
        case .loading: tableManager.showLoadingFooter()
        case .paging(let elements):
            tableManager.setupWith(sections: elements)
            tableManager.showLoadingFooter()
        case .populated(let elements):
            tableManager.invalidateCache()
            tableManager.setupWith(sections: elements)
            tableManager.hideLoadingFooter()
        case .error: tableManager.hideLoadingFooter()
        }
    }
}

// MARK: - Table Manager Delegate
extension HappeningsViewController: TableManagerDelegate {
    func didSwipeForRefresh() {
        viewModel.refreshActivities()
    }

    func loadMoreData() {
        viewModel.loadMoreActivities()
    }
    
    func tableViewDidScroll(_ scrollView: UIScrollView) { }
}

// MARK: - Observable sources

extension HappeningsViewController: ObservableSources {
    var closeObs: Observable<Void> {
        return viewModel.closeObs
    }
    
    var alertDataObs: Observable<AlertData> {
        return viewModel.alertDataObs.filter { [unowned self] _ in
            self.isWelcomeViewHidden
        }
    }
    
    var errorObs: Observable<AppError> {
        return viewModel.errorObs
    }
    
    var progressBarObs: Observable<Bool> {
        return viewModel.progressBarObs
    }
}

// MARK: - Refreshable view implementation
extension HappeningsViewController: RefreshableView {
    func refreshData() {
        viewModel.refreshActivities()
    }
}
