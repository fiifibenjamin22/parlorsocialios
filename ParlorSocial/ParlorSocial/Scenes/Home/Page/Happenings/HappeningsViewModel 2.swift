//
//  HappeningsViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 27.02.2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

typealias HappeningsViewModelType = ActivationsLogic & HomeSubPageViewModelProtocol & HomeSubPageViewModelSelectors

final class HappeningsViewModel: BaseViewModel {
    lazy var checkInService: CheckInService = {
        let checkInService = CheckInService(
            locationService: LocationService(),
            guestListRepository: guestListRepository,
            activationRepository: activationsRepository
        )
        checkInService.delegate = self

        return checkInService
    }()

    private lazy var activationSelector: (HomeActivation) -> Void = { [unowned self] activation in
        let openingType = ActivationDetailsNavigationProvider.getOpeningActivationDetailsType(with: activation)
        switch openingType {
        case .activationDetailsVC, .addProfilePhotoVC, .upgradeToPremiumForStandard:
            self.destinationsRelay.accept(
                .activationDetails(
                    activationId: activation.id,
                    openingType: openingType,
                    navigationSource: .activationsHappenings
                )
            )
        case .startedAlert:
            self.alertDataSubject.onNext(AlertData.fromSimpleMessage(message: Strings.Activation.startedAlert.localized))
        }

        AnalyticService.shared.saveViewClickAnalyticEvent(
            withName: AnalyticEventClickableElementName.HomeScreen.Happenings.item.rawValue,
            referenceId: activation.id
        )
    }

    private lazy var activationInfoSelector: (HomeActivation) -> Void = { [unowned self] activation in
        self.destinationsRelay.accept(.activationDetailsInfo(activationId: activation.id))
    }

    private lazy var allActivationsButtonSelector: () -> Void = { [unowned self] in
        AnalyticService.shared.saveViewClickAnalyticEvent(
            withName: AnalyticEventClickableElementName.HomeScreen.Happenings.emptyViewButton.rawValue
        )
        self.changeToAllActivationsSubject.emitElement()
    }

    private lazy var cellActionButtonSelector: (HomeActivation) -> Void = { [unowned self] activation in
        switch activation.actionButtonType {
        case .rsvp, .rsvpd: self.activationSelector(activation)
        case .updates, .live: self.activationInfoSelector(activation)
        case .guestList:
            self.checkInService.navigateToGuestList(
                activationId: activation.id,
                checkInStrategy: activation.guestListAccess.checkInStrategy
            )
        default: break
        }

        AnalyticService.shared.saveViewClickAnalyticEvent(
            withName: activation.actionButtonType.eventName,
            referenceId: activation.id
        )
    }

    fileprivate lazy var repositoryChangesMapper: (SingleHomeActivationResposne, PaginatedData) -> Observable<PaginatedData> = { [unowned self] result, latestData -> Observable<PaginatedData> in
        guard case let .success(data) = result else {
            if case let .failure(error) = result {
                self.activationSectionsRelay.accept(.error(error))
            }

            return Observable.empty()
        }

        let newActivation = data.data
        var allActivations: [HomeActivation] = latestData.sections.flatMap { $0.getItems() ?? [] }
        guard let firstIndex = allActivations.firstIndex(where: { $0.id == newActivation.id }) else { return Observable.empty() }

        if allActivations[firstIndex] != newActivation {
            allActivations[firstIndex] = newActivation
            return Observable.just(
                PaginatedData(
                    HappeningSection.make(
                        for: allActivations,
                        itemSelector: self.activationSelector,
                        buttonSelector: self.cellActionButtonSelector
                    ),
                    latestData.metadata
                )
            )
        } else {
            return Observable.empty()
        }
    }

    private var activationsRequestModel = HomeActivations.Get.Request.initialHappenings

    let analyticEventReferencedId: Int? = nil
    let activationSectionsRelay = BehaviorRelay<SectionPaginatedRepositoryState>(value: .loading)
    
    private let changeToAllActivationsSubject: PublishSubject<Void> = PublishSubject()
    private let showEmptyDataSubject: BehaviorSubject<Bool> = BehaviorSubject(value: true)
    private let activationsDataRelay = BehaviorRelay<PaginatedData>(value: PaginatedData(sections: [], metadata: ListMetadata.initial))
    private let destinationsRelay: PublishRelay<HomeDestination> = PublishRelay()
    private let activationsRepository = ActivationsRepository.shared
    private let homeRepository = HomeRepository.shared
    private let guestListRepository = GuestListRsvpRepository.shared
    private let loadDataRelay = BehaviorRelay(value: 0)

    override init() {
        super.init()

        bindLoadDataRelay()
        loadMoreActivities()
    }
}

// MARK: Interface logic methods
extension HappeningsViewModel: ActivationsLogic {
    var destinationsObs: Observable<HomeDestination> {
        return destinationsRelay.asObservable()
    }

    func loadMoreActivities() {
        loadDataRelay.accept(0)
    }

    func refreshActivities() {
        activationsDataRelay.accept(PaginatedData(sections: [], metadata: ListMetadata.initial))
        activationsRequestModel.set(page: 1)

        loadMoreActivities()
    }

    func loadActivations() {
        invalidateRequestModel()
        clearData()
        loadMoreActivities()
    }
}

extension HappeningsViewModel: HomeSubPageViewModelProtocol {
    var showEmptyDataObs: Observable<Bool> {
        return showEmptyDataSubject.asObservable()
    }

    var changeToAllActivationsObs: Observable<Void> {
        return changeToAllActivationsSubject.asObservable()
    }
}

extension HappeningsViewModel: HomeSubPageViewModelSelectors {
    func allActivationsButtonDidTap() {
        changeToAllActivationsSubject.emitElement()
    }
}

// MARK: - Private methods
extension HappeningsViewModel {
    private func invalidateRequestModel() {
        activationsRequestModel.set(page: 1)
    }

    private func clearData() {
        activationsDataRelay.accept(PaginatedData(sections: [], metadata: ListMetadata.initial))
        activationSectionsRelay.accept(.paging(elements: []))
    }

    private func bindLoadDataRelay() {
        activationSectionsRelay
            .subscribe(onNext: { [unowned self] state in
                self.handle(activationState: state)
            }).disposed(by: disposeBag)
        
        loadDataRelay
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .userInteractive))
            .setLoadingStateOnSubscribe(with: activationSectionsRelay)
            .skipOnListPopulated(with: activationsDataRelay)
            .flatMapLatest { [unowned self] _ -> Observable<ApiResponse<HomeActivations.Get.Response>> in
                return self.homeRepository.getHomeActivations(using: self.activationsRequestModel)
            }
            .mapResponseToTableSection(
                with: activationsDataRelay,
                sectionStateRelay: activationSectionsRelay,
                sectionMaker: { [unowned self] in self.createTableSections(from: $0) }
            )
            .changePaginatedState(with: activationSectionsRelay)
            .doOnNext { [unowned self] _ in self.activationsRequestModel.increasePage() }
            .doOnNext { [unowned self] sections, _ in self.showEmptyDataSubject.onNext(sections.isEmpty) }
            .bind(to: activationsDataRelay)
            .disposed(by: disposeBag)
        
        activationsRepository.activationWithIdChangedObs
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .userInteractive))
            .flatMapFirst { [unowned self] id -> Observable<SingleHomeActivationResposne> in
                return self.fetchActivation(with: id)
            }
            .withLatestFrom(activationsDataRelay) { return ($0, $1) }
            .flatMap(repositoryChangesMapper)
            .changePaginatedState(with: activationSectionsRelay)
            .bind(to: activationsDataRelay)
            .disposed(by: disposeBag)
    }

    private func createTableSections(from activations: [HomeActivation]) -> [TableSection] {
        return activations.isEmpty ?
            HomeEmptyDataSection.make(for: .happenings, buttonSelector: self.allActivationsButtonSelector) :
            HappeningSection.make(
                for: activations,
                itemSelector: self.activationSelector,
                buttonSelector: self.cellActionButtonSelector
            )
    }

    private func handle(activationState state: SectionPaginatedRepositoryState) {
        switch state {
        case .error(let err):
            errorSubject.onNext(err)
        default:
            return
        }
    }

    private func fetchActivation(with id: Int) -> Observable<SingleHomeActivationResposne> {
        return homeRepository.getHomeActivation(withId: id)
            .showingProgressBar(with: self)
    }
}

// MARK: - Check-in service
extension HappeningsViewModel: CheckInServiceDelegate {
    func didEndCheckInSuccess(_ checkInService: CheckInService, activationId: Int) {
        progressBarSubject.onNext(false)
        destinationsRelay.accept(.guestList(activationId: activationId))
    }
}

private extension ActionButtonType {
    var eventName: String {
        let events = AnalyticEventClickableElementName.HomeScreen.Happenings.self
        switch self {
        case .guestList: return events.guestListActionButton.rawValue
        case .live: return events.liveActionButton.rawValue
        case .rsvp: return events.rsvpActionButton.rawValue
        case .rsvpd: return events.rsvpdActionButton.rawValue
        case .updates: return events.updatesActionButton.rawValue
        }
    }
}
