//
//  RsvpsViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 21.05.2020.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class RsvpsViewController: AppViewController {

    // MARK: - Properties

    weak var delegate: HomeViewControllerProtocol?

    override var connectedOnboardingType: OnboardingType? { return .rsvps }

    var viewModel: RsvpsViewModelType!
    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? { return viewModel.analyticEventScreenSubject }

    private(set) var tableView: UITableView!

    private var isWelcomeViewHidden: Bool { return delegate?.isWelcomeViewHidden ?? true }
    private var tableManager: TableViewManager!

    let analyticEventScreen: AnalyticEventScreen? = .rsvpsList

    // MARK: - Initialization
    init(router: Router) {
        super.init(nibName: nil, bundle: nil)

        self.router = router
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
        tableManager = TableViewManager(tableView: tableView, delegate: self)
        tableManager.setupRefreshControl()
        setupPresentationLogic()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if tableView.isInAroundOfTop {
            viewModel.loadRsvps()
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func observeViewModel() {
        super.observeViewModel()

        viewModel
            .errorObs
            .handleWithAlerts(using: self)
            .disposed(by: disposeBag)

        viewModel
            .alertDataObs
            .show(using: self)
            .disposed(by: disposeBag)

        // TODO: - implement more suitable way of displaying loader + refactor naming of progressBar to loader
        viewModel
            .progressBarObs
            .showProgressBar(using: parent?.parent?.parent ?? self)
            .disposed(by: disposeBag)
    }
}

// MARK: - Private methods
extension RsvpsViewController {
    private func setupViews() {
        let builder = RsvpsViewBuilder(controller: self)

        self.tableView = builder.buildTableView()

        builder.setupViews()
    }

    private func setupPresentationLogic() {
        bindDestinationObservable()
        bindRsvpsUpcomingState()
        bindChangeToAllActivationsObservable()
    }

    private func bindDestinationObservable() {
        viewModel.destinationsObs
            .filter { [unowned self] _ in self.isWelcomeViewHidden }
            .subscribe(onNext: { [unowned self] destination in
                self.router.push(destination: destination)
            }).disposed(by: disposeBag)
    }

    private func bindRsvpsUpcomingState() {
        viewModel.rsvpsSectionsRelay
            .subscribe(
                onNext: { [unowned self] state in
                    self.handle(rsvpsState: state)
                }
        ).disposed(by: disposeBag)
    }

    private func bindChangeToAllActivationsObservable() {
        viewModel.changeToAllActivationsObs
            .subscribe(onNext: { [unowned self] in
                self.delegate?.changeSelectedPage(to: .all)
            }).disposed(by: disposeBag)
    }

    private func handle(rsvpsState state: SectionPaginatedRepositoryState) {
        switch state {
        case .loading: tableManager.showLoadingFooter()
        case .paging(let elements):
            tableManager.setupWith(sections: elements)
            tableManager.showLoadingFooter()
        case .populated(let elements):
            tableManager.invalidateCache()
            tableManager.setupWith(sections: elements)
            tableManager.hideLoadingFooter()
        case .error: tableManager.hideLoadingFooter()
        }
    }
}

// MARK: - Scrollable content
extension RsvpsViewController: ScrollableContent {
    func scrollToTop() {
        tableManager.scrollToTop()
    }
}

// MARK: - Table Manager Delegate
extension RsvpsViewController: TableManagerDelegate {
    func didSwipeForRefresh() {
        viewModel.loadRsvps()
    }

    func loadMoreData() {
        viewModel.loadMoreRsvps()
    }

    func tableViewDidScroll(_ scrollView: UIScrollView) { }
}

// MARK: - Observable sources
extension RsvpsViewController {
    var alertDataObs: Observable<AlertData> {
        return viewModel.alertDataObs.filter { [unowned self] _ in
            self.isWelcomeViewHidden
        }
    }
}
