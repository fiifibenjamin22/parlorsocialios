//
//  RsvpsViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 21.05.2020.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class RsvpsViewBuilder {
    private unowned let controller: RsvpsViewController
    private var view: UIView! { return controller.view }

    init(controller: RsvpsViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension RsvpsViewBuilder {
    private func setupHierarchy() {
        controller.apply {
            view.addSubviews([$0.tableView])
        }
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.tableView.edges(equalTo: view)
        }
    }

}

// MARK: - Public build methods
extension RsvpsViewBuilder {
    func buildView() -> UIView {
        return UIView()
    }

    func buildTableView() -> UITableView {
        let tableView = UITableView(frame: .zero, style: .grouped).manualLayoutable().apply {
            $0.separatorStyle = .none
            $0.showsVerticalScrollIndicator = false
            $0.backgroundColor = .appBackground
        }

        return tableView
    }

}
