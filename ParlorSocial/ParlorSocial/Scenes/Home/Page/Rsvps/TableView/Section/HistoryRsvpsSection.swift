//
//  HistoryRsvpsSection.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 21/05/2019.
//

import UIKit

final class HistoryRsvpsSection: BasicTableSection<HomeRsvp, HistoryRsvpsTableViewCell> {
    override var headerType: UITableViewHeaderFooterView.Type? { return HistoryRsvpsHeaderView.self }

    override func configure(header: UITableViewHeaderFooterView, in section: Int) {
        guard let sectionHeader = header as? HistoryRsvpsHeaderView else {
            fatalError("Header is wrong type! Needed: \(ProfileTabHeaderView.name) but get instead \(String(describing: header))")
        }

        sectionHeader.apply {
            $0.label.text = Strings.Rsvps.pastEvents.localized
            $0.label.textColor = UIColor.appTextSemiLight
        }
    }
}

extension HistoryRsvpsSection {
    static func make(
        for activations: [HomeRsvp],
        itemSelector: @escaping (HomeRsvp) -> Void
    ) -> [TableSection] {
        return [self.init(items: activations, itemSelector: itemSelector)]
    }
}

