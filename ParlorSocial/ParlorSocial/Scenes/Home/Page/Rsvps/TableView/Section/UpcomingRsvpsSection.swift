//
//  UpcomingRsvpsSection.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 10/05/2019.
//

import UIKit

final class UpcomingRsvpsSection: BasicTableSection<HomeRsvp, UpcomingRsvpsTableViewCell> {
    private let buttonSelector: ((HomeRsvp) -> Void)

    init(
        items: [HomeRsvp],
        itemSelector: @escaping (HomeRsvp) -> Void,
        buttonSelector: @escaping ((HomeRsvp) -> Void)
    ) {
        self.buttonSelector = buttonSelector
        super.init(items: items, itemSelector: itemSelector)
    }

    override var headerType: UITableViewHeaderFooterView.Type? { return UpcomingRsvpsHeaderView.self }

    override func configure(header: UITableViewHeaderFooterView, in section: Int) {
        guard let sectionHeader = header as? UpcomingRsvpsHeaderView else {
            fatalError("Header is wrong type! Needed: \(ProfileTabHeaderView.name) but get instead \(String(describing: header))")
        }

        sectionHeader.apply {
            $0.label.text = Strings.Rsvps.upcomingEvents.localized
            $0.label.textColor = UIColor.appTextSemiLight
        }
    }

    override func configure(cell: UITableViewCell, at indexPath: IndexPath) {
        if let cell = cell as? UpcomingRsvpsTableViewCell {
            cell.actionButtonDidTapClosure = { [unowned self] in
                self.buttonSelector(self.items[indexPath.item])
            }
        }

        super.configure(cell: cell, at: indexPath)
    }
}

extension UpcomingRsvpsSection {
    static func make(
        for items: [HomeRsvp],
        itemSelector: @escaping (HomeRsvp) -> Void,
        buttonSelector: @escaping (HomeRsvp) -> Void
    ) -> [TableSection] {
        return [UpcomingRsvpsSection(items: items, itemSelector: itemSelector, buttonSelector: buttonSelector)]
    }
}
