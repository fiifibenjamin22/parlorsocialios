//
//  HistoryRsvpHeaderView.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 21/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

final class HistoryRsvpsHeaderView: UITableViewHeaderFooterView {
    private let separatorView = UIView().apply {
        $0.backgroundColor = .appSemiTransparentSeparator
    }.manualLayoutable()

    lazy var label: ParlorLabel = {
        return ParlorLabel.withSectionHeaderLabelStyle(insets: UIEdgeInsets(top: 20, left: 16, bottom: 10, right: 0))
    }()

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setup() {
        setupSeparatorView()
        setupLabel()
    }

    private func setupSeparatorView() {
        contentView.addSubview(separatorView)
        separatorView.edgesToParent(anchors: [.leading, .top, .trailing], insets: UIEdgeInsets(horizontal: 16.0))
        separatorView.heightAnchor.equalTo(constant: Constants.separatorSize)
    }

    private func setupLabel() {
        contentView.addSubview(label)
        label.edgesToParent(anchors: [.leading, .bottom, .trailing])
        label.topAnchor.equal(to: separatorView.bottomAnchor, constant: 0.0)
    }
}
