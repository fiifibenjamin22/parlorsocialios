//
//  RsvpsHeaderView.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 11/05/2019.
//

import UIKit

final class UpcomingRsvpsHeaderView: UITableViewHeaderFooterView {
    lazy var label: ParlorLabel = {
        return ParlorLabel.withSectionHeaderLabelStyle(insets: UIEdgeInsets(top: 15, left: 30, bottom: 0, right: 0))
    }()

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func initialize() {
        contentView.addSubview(label)
        label.edgesToParent()
    }
}
