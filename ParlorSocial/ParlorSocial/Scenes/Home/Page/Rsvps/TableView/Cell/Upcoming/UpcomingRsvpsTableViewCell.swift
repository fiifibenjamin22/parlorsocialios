//
//  UpcomingRsvpsTableViewCell.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 09/05/2019.
//

import UIKit

final class UpcomingRsvpsTableViewCell: UITableViewCell, ConfigurableCell {
    typealias Model = HomeRsvp
    
    // MARK: - Views
    private(set) var contentStackView: UIStackView!
    private(set) var containerView: UIView!
    private(set) var topContentView: UIView!
    private(set) var activationImageView: UIImageView!
    private(set) var darkAlphaView: UIView!
    private(set) var activationTitleLabel: UILabel!
    private(set) var headerStackView: UIStackView!
    private(set) var headerTextLabel: UILabel!
    private(set) var headerLeftLineView: UIView!
    private(set) var headerRightLineView: UIView!
    private(set) var dateTimeStackView: UIStackView!
    private(set) var activationDateLabel: UILabel!
    private(set) var activationTimeLabel: UILabel!
    private(set) var bottomStackView: UIStackView!
    private(set) var activationGuestsLabel: UILabel!
    private(set) var activationPlaceLabel: UILabel!
    private(set) var activationWaitListView: UIView!
    private(set) var activationWaitingStackView: UIStackView!
    private(set) var activationWaitingIconImageView: UIImageView!
    private(set) var activationWaitListLabel: UILabel!
    private(set) var actionButtonView: UIView!
    private(set) var actionButton: ParlorButton!
    
    // MARK: - Properties
    var actionButtonDidTapClosure: (() -> Void)?
    
    private var model: Model!
 
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initializeCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        actionButtonDidTapClosure = nil
        actionButton.resetLayout()
    }
}

// MARK: - Public methods
extension UpcomingRsvpsTableViewCell {
    func configure(with model: Model) {
        self.model = model
        activationImageView.getImage(from: model.imageURL)
        activationTitleLabel.text = model.name
        activationDateLabel.text = model.timeSegment.startAt.toUpcomingRsvpsDate()
        activationTimeLabel.text = model.timeSegment.startAt
            .toString(withFormat: Date.Format.activationDetails).uppercased()
        activationGuestsLabel.text = model.formattedGuestCountText
        activationPlaceLabel.text = model.location?.name
        activationWaitListView.isHidden = !model.isWaiting
        activationWaitListLabel.text = Strings.MyRsvps.upcomingWaitlist.localized

        let isActionButtonHidden = model.actionButtonType == .rsvp || model.actionButtonType == .rsvpd
        actionButtonView.isHidden = isActionButtonHidden
        roundCorners(for: isActionButtonHidden)
        setupButton(for: model.actionButtonType)

        actionButton.setupTapGestureRecognizer(target: self, action: #selector(didTapGuestList))
    }

    private func setupButton(for type: ActionButtonType) {
        actionButton.style = buttonStyle(for: type)
        actionButton.title = type.title

        switch type {
        case .guestList: setupButtonForGuestListType()
        default: break
        }
    }

    private func setupButtonForGuestListType() {
        actionButton.semanticContentAttribute = .forceRightToLeft
        actionButton.setImage(Icons.AllActivations.guestList, for: .normal)
        actionButton.imageEdgeInsets = Constants.UpcomingRsvps.ActionButton.GuestList.imageEdgeInsets
    }

    private func buttonStyle(for type: ActionButtonType) -> ParlorButtonStyle {
        switch type {
        case .guestList, .updates:
            return .white(font: UIFont.custom(ofSize: Constants.FontSize.small, font: UIFont.Roboto.regular))
        case .live:
            return .filledWith(
                color: UIColor.backgroundGreen,
                titleColor: UIColor.appText,
                font: UIFont.custom(ofSize: Constants.FontSize.small, font: UIFont.Roboto.regular),
                letterSpacing: Constants.LetterSpacing.none
            )
        default: return type.defaultStyle
        }
    }
}

// MARK: - Private methods
extension UpcomingRsvpsTableViewCell {
    private func initializeCell() {
        selectionStyle = .none
        backgroundColor = UIColor.appBackground
        setupViews()
    }
    
    private func setupViews() {
        let builder = UpcomingRsvpsTableViewCellBuilder(cell: self)

        containerView = builder.buildView()
        contentStackView = builder.buildStackView()
        topContentView = builder.buildView()
        activationImageView = builder.buildActivationImageView()
        darkAlphaView = builder.buildDarkAlphaView()
        activationTitleLabel = builder.buildActivationTitleLabel()
        headerStackView = builder.buildHeaderStackView()
        headerTextLabel = builder.buildHeaderTextLabel()
        headerLeftLineView = builder.buildHeaderLeftLineView()
        headerRightLineView = builder.buildHeaderRightLineView()
        dateTimeStackView = builder.buildDateTimeStackView()
        activationDateLabel = builder.buildActivationDateLabel()
        activationTimeLabel = builder.buildActivationTimeLabel()
        bottomStackView = builder.buildBottomStackView()
        activationGuestsLabel = builder.buildActivationGuestsLabel()
        activationPlaceLabel = builder.buildActivationPlaceLabel()
        activationWaitListView = builder.buildActivationWaitListView()
        activationWaitingStackView = builder.buildActivationWaitingStackView()
        activationWaitingIconImageView = builder.buildActivationWaitingIconImageView()
        activationWaitListLabel = builder.buildActivationWaitListLabel()
        actionButton = builder.buildActionButton()
        actionButtonView = builder.buildActionButtonView()
        
        builder.setupViews()
    }
    
    @objc private func didTapGuestList() {
        actionButtonDidTapClosure?()
    }

    private func roundCorners(for actionButtonHidden: Bool) {
        let corners: CACornerMask
        if actionButtonHidden {
            corners = [.layerMinXMinYCorner,
                       .layerMaxXMinYCorner,
                       .layerMinXMaxYCorner,
                       .layerMaxXMaxYCorner]
        } else {
            corners = [.layerMinXMinYCorner,
                       .layerMaxXMinYCorner]
        }

        activationImageView.apply {
            $0.roundCorners(with: Constants.UpcomingRsvps.cellCornerRadius, and: corners)
        }

        darkAlphaView.apply {
            $0.roundCorners(with: Constants.UpcomingRsvps.cellCornerRadius, and: corners)
        }
    }
}

private extension Constants {
    enum UpcomingRsvps {
        enum ActionButton {
            enum GuestList {
                static let imageEdgeInsets = UIEdgeInsets(top: 0, left: 60, bottom: 0, right: -60)
            }
        }

        static let cellCornerRadius: CGFloat = 20
    }
}
