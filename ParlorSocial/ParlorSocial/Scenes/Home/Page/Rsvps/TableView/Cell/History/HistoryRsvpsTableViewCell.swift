//
//  HistoryRsvpsTableViewCell.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 20/05/2019.
//

import UIKit

final class HistoryRsvpsTableViewCell: UITableViewCell, ConfigurableCell {
    typealias Model = HomeRsvp
    
    // MARK: - Views
    private(set) var contentCellView: UIView!
    private(set) var activationImageView: UIImageView!
    private(set) var titleLabel: UILabel!
    private(set) var dateLabel: UILabel!
    private(set) var guestListIconImageView: UIImageView!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initializeCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - Public methods
extension HistoryRsvpsTableViewCell {
    func configure(with model: Model) {
        activationImageView.getImage(from: model.imageURL)
        titleLabel.text = model.name
        dateLabel.text = model.timeSegment.startAt.toHistoryRsvpsDate()
        guestListIconImageView.isHidden = !model.isGuestListAvailable
        if model.isGuestListAvailable {
            contentCellView.applyTouchAnimation()
        } else {
            contentCellView.gestureRecognizers?.removeAll()
        }
    }
}

// MARK: - Private methods
extension HistoryRsvpsTableViewCell {
    
    private func initializeCell() {
        clipsToBounds = true
        selectionStyle = .none
        backgroundColor = UIColor.appBackground
        setupViews()
    }
    
    private func setupViews() {
        let builder = HistoryRsvpsTableViewCellBuilder(cell: self)
        contentCellView = builder.buildContentCellView()
        activationImageView = builder.buildActivationImageView()
        titleLabel = builder.buildTitleLabel()
        dateLabel = builder.buildDateLabel()
        guestListIconImageView = builder.buildGuestListIconImageView()
        
        builder.setupViews()
    }
    
}

