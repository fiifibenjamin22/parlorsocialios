//
//  HistoryRsvpsTableViewCellBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 20/05/2019.
//

import UIKit

final class HistoryRsvpsTableViewCellBuilder {
    private unowned let cell: HistoryRsvpsTableViewCell
    private var view: UIView! { return cell.contentView }
    
    init(cell: HistoryRsvpsTableViewCell) {
        self.cell = cell
    }
    
    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension HistoryRsvpsTableViewCellBuilder {
    
    private func setupProperties() {
        cell.apply {
            $0.contentCellView.backgroundColor = .white
            $0.activationImageView.clipsToBounds = true
            $0.activationImageView.contentMode = .scaleAspectFill
        }
    }
    
    private func setupHierarchy() {
        cell.apply {
            view.addSubview($0.contentCellView)
            
            [$0.activationImageView,
             $0.titleLabel,
             $0.guestListIconImageView,
             $0.dateLabel].addTo(parent: $0.contentCellView)
        }
    }
    
    private func setupAutoLayout() {
        cell.contentCellView.apply {
            $0.edgesToParent(
                anchors: [.top, .leading, .bottom, .trailing],
                insets: UIEdgeInsets.margins(
                    top: Constants.HistoryRsvpsCell.contentVerticalMargin,
                    left: Constants.HistoryRsvpsCell.contentHorizontalMargin,
                    bottom: Constants.HistoryRsvpsCell.contentVerticalMargin,
                    right: Constants.HistoryRsvpsCell.contentHorizontalMargin
                )
            )
        }
        
        cell.activationImageView.apply {
            $0.edgesToParent(
                anchors: [.leading, .top, .bottom],
                insets: UIEdgeInsets.margins(
                    top: Constants.HistoryRsvpsCell.activationImageViewMargin,
                    left: Constants.HistoryRsvpsCell.activationImageViewMargin,
                    bottom: Constants.HistoryRsvpsCell.activationImageViewMargin,
                    right: 0)
            )
            $0.heightAnchor.equalTo(constant: Constants.HistoryRsvpsCell.activationImageHeight)
            $0.widthAnchor.equalTo(constant: Constants.HistoryRsvpsCell.activationImageWidth)
        }
        
        cell.titleLabel.apply {
            $0.leadingAnchor.equal(to: cell.activationImageView.trailingAnchor, constant: 20)
            $0.topAnchor.equal(to: cell.contentCellView.topAnchor, constant: 21)
            $0.trailingAnchor.equal(to: cell.guestListIconImageView.leadingAnchor, constant: -10)
        }
        
        cell.dateLabel.apply {
            $0.leadingAnchor.equal(to: cell.activationImageView.trailingAnchor, constant: 18)
            $0.bottomAnchor.equal(to: cell.contentCellView.bottomAnchor, constant: -19)
        }
        
        cell.guestListIconImageView.apply {
            $0.trailingAnchor.equal(to: cell.contentCellView.trailingAnchor, constant: -25)
            $0.centerYAnchor.equal(to: cell.contentCellView.centerYAnchor)
            $0.heightAnchor.equalTo(constant: Constants.HistoryRsvpsCell.guestListIconSize)
            $0.widthAnchor.equalTo(constant: Constants.HistoryRsvpsCell.guestListIconSize)
        }
    }
    
}

// MARK: - Public methods
extension HistoryRsvpsTableViewCellBuilder {
    func buildView() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildContentCellView() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildActivationImageView() -> UIImageView {
        return UIImageView().manualLayoutable()
    }
    
    func buildTitleLabel() -> UILabel {
        return UILabel.styled(
            textColor: .appTextMediumBright,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.subheading,
                font: UIFont.Roboto.medium),
            alignment: .left,
            numberOfLines: 2
        )
    }
    
    func buildDateLabel() -> UILabel {
        return UILabel.styled(
            textColor: .appTextSemiLight,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.tiny,
                font: UIFont.Roboto.regular),
            alignment: .left,
            numberOfLines: 1
        )
    }
    
    func buildGuestListIconImageView() -> UIImageView {
        return UIImageView(image: Icons.MyRsvps.guestListIcon).manualLayoutable()
    }
}

extension Constants {
    
    enum HistoryRsvpsCell {
        static let contentHorizontalMargin: CGFloat = 16.0
        static let contentVerticalMargin: CGFloat = 5.0
        static let activationImageViewMargin: CGFloat = 6.0
        static let activationImageHeight: CGFloat = 88.0
        static let activationImageWidth: CGFloat = 68.0
        static let guestListIconSize: CGFloat = 19.0
    }
    
}
