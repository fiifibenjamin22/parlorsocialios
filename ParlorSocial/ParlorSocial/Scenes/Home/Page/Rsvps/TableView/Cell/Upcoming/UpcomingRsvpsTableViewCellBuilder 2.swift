//
//  UpcomingRsvpsTableViewCellBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 09/05/2019.
//

import UIKit

final class UpcomingRsvpsTableViewCellBuilder {
    private unowned let cell: UpcomingRsvpsTableViewCell
    private var view: UIView! { return cell.contentView }
    
    init(cell: UpcomingRsvpsTableViewCell) {
        self.cell = cell
    }
    
    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension UpcomingRsvpsTableViewCellBuilder {
    
    private func setupProperties() {
        cell.apply {
            $0.activationImageView.apply {
                $0.clipsToBounds = true
                $0.backgroundColor = UIColor.appBackground
                $0.contentMode = .scaleAspectFill
            }
            
            $0.darkAlphaView.apply {
                $0.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            }
            
            $0.activationDateLabel.adjustsFontSizeToFitWidth = true
            
            $0.headerTextLabel.text = Strings.parlorSymbol.rawValue
            $0.headerLeftLineView.backgroundColor = .white
            $0.headerRightLineView.backgroundColor = .white

            $0.containerView.apply {
                $0.layer.set(shadow: Constants.UpcomingRsvps.shadow, rasterize: false)
                $0.layer.masksToBounds = false
                $0.layer.cornerRadius = Constants.UpcomingRsvps.cellCornerRadius
                $0.backgroundColor = .white
            }

            $0.actionButton.apply {
                $0.roundCorners(with: Constants.UpcomingRsvps.cellCornerRadius, and: [.layerMaxXMaxYCorner, .layerMinXMaxYCorner])
            }
        }
    }
    
    private func setupHierarchy() {
        cell.apply {
            view.addSubview($0.containerView)

            $0.containerView.addSubview($0.contentStackView)

            [$0.activationImageView,
             $0.darkAlphaView,
             $0.headerStackView,
             $0.dateTimeStackView,
             $0.bottomStackView].addTo(parent: $0.topContentView)

            $0.contentStackView.addArrangedSubviews([$0.topContentView, $0.actionButtonView])

            $0.headerStackView.addArrangedSubviews([$0.headerLeftLineView,
                                                    $0.headerTextLabel,
                                                    $0.headerRightLineView])

            $0.dateTimeStackView.addArrangedSubviews([$0.activationDateLabel,
                                                      $0.activationTimeLabel])

            $0.bottomStackView.addArrangedSubviews([$0.activationGuestsLabel,
                                                    $0.activationTitleLabel,
                                                    $0.activationPlaceLabel,
                                                    $0.activationWaitListView])

            $0.actionButtonView.addSubview($0.actionButton)

            $0.activationWaitListView.addSubview($0.activationWaitingStackView)

            $0.activationWaitingStackView.addArrangedSubviews([$0.activationWaitListLabel,
                                                               $0.activationWaitingIconImageView])
        }
    }
    
    private func setupAutoLayout() {
        cell.containerView.apply {
            $0.edgesToParent(
                anchors: [.leading, .top, .trailing, .bottom],
                insets: UIEdgeInsets.margins(
                    top: 10,
                    left: 30,
                    bottom: 20,
                    right: 30
                )
            )
        }

        cell.contentStackView.edgesToParent()

        cell.activationImageView.edgesToParent()
        cell.activationImageView.heightAnchor.constraint(
            equalTo: cell.activationImageView.widthAnchor,
            multiplier: Constants.UpcomingRsvps.ImageView.ratio
        ).activate()

        cell.darkAlphaView.edges(equalTo: cell.activationImageView)

        cell.headerStackView.apply {
            $0.topAnchor.equal(to: cell.activationImageView.topAnchor, constant: Constants.UpcomingRsvps.cellContentMargin)
            $0.centerXAnchor.equal(to: cell.activationImageView.centerXAnchor)
        }

        cell.headerLeftLineView.apply {
            $0.heightAnchor.equalTo(constant: 1)
            $0.widthAnchor.equalTo(constant: 16)
        }

        cell.headerRightLineView.apply {
            $0.heightAnchor.equalTo(constant: 1)
            $0.widthAnchor.equalTo(constant: 16)
        }

        cell.dateTimeStackView.apply {
            $0.leadingAnchor.equal(to: cell.activationImageView.leadingAnchor,
                                   constant: Constants.UpcomingRsvps.cellContentMargin)
            $0.trailingAnchor.equal(to: cell.activationImageView.trailingAnchor,
                                    constant: -Constants.UpcomingRsvps.cellContentMargin)
             $0.constraintByCenteringVerticallyBeetween(cell.headerStackView, and: cell.activationGuestsLabel)
        }

        cell.bottomStackView.apply {
            $0.leadingAnchor.equal(to: cell.activationImageView.leadingAnchor,
                                   constant: Constants.UpcomingRsvps.cellContentMargin)
            $0.trailingAnchor.equal(to: cell.activationImageView.trailingAnchor,
                                    constant: -Constants.UpcomingRsvps.cellContentMargin)
            $0.bottomAnchor.equal(to: cell.activationImageView.bottomAnchor, constant: -24)
        }

        cell.activationWaitingStackView.apply {
            $0.edgesToParent(
                anchors: [.top, .bottom],
                insets: UIEdgeInsets.margins(top: 15, bottom: -5))
            $0.centerXAnchor.equal(to: cell.activationWaitListView.centerXAnchor)
        }

        cell.activationWaitingIconImageView.setContentHuggingPriority(.required, for: .horizontal)
        cell.activationWaitListLabel.setContentCompressionResistancePriority(.required, for: .horizontal)

        cell.actionButton.apply {
            $0.edgesToParent()
            $0.heightAnchor.equalTo(constant: 50)
        }
    }
}

// MARK: - Public methods
extension UpcomingRsvpsTableViewCellBuilder {
    func buildView() -> UIView {
        return UIView().manualLayoutable()
    }

    func buildStackView() -> UIStackView {
        return UIStackView().apply {
            $0.axis = .vertical
        }.manualLayoutable()
    }

    func buildActivationImageView() -> UIImageView {
        return UIImageView().manualLayoutable()
    }
    
    func buildDarkAlphaView() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildHeaderStackView() -> UIStackView {
        return UIStackView(axis: .horizontal, spacing: 2, alignment: .center, distribution: .fillEqually)
    }
    
    func buildDateTimeStackView() -> UIStackView {
        return UIStackView(axis: .vertical, spacing: 10, alignment: .center, distribution: .fill)
    }
    
    func buildActivationDateLabel() -> UILabel {
        return UILabel.styled(textColor: .white,
                              withFont: UIFont.custom(
                                ofSize: Constants.FontSize.largeDate,
                                font: UIFont.Editor.bold),
                              alignment: .center,
                              numberOfLines: 1)
    }
    
    func buildActivationTimeLabel() -> UILabel {
        return UILabel.styled(textColor: .white,
                              withFont: UIFont.custom(
                                ofSize: Constants.FontSize.hintSize,
                                font: UIFont.Roboto.bold),
                              alignment: .center)
    }
    
    func buildActivationTypeIconImageView() -> UIImageView {
        return UIImageView().manualLayoutable()
    }
    
    func buildBottomStackView() -> UIStackView {
        return UIStackView(axis: .vertical, spacing: 5, alignment: .center, distribution: .fill)
    }
    
    func buildHeaderLeftLineView() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildHeaderTextLabel() -> UILabel {
        return UILabel.styled(textColor: .white,
                              withFont: UIFont.custom(
                                ofSize: Constants.FontSize.heading,
                                font: UIFont.Editor.medium),
                              alignment: .center)
    }
    
    func buildHeaderRightLineView() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildActivationGuestsLabel() -> UILabel {
        return UILabel.styled(textColor: .white,
                              withFont: UIFont.custom(
                                ofSize: Constants.FontSize.subbody,
                                font: UIFont.Roboto.regular),
                              alignment: .center,
                              numberOfLines: 1)
    }
    
    func buildActivationTitleLabel() -> UILabel {
        return UILabel.styled(textColor: .white,
                              withFont: UIFont.custom(
                                ofSize: Constants.FontSize.subbody,
                                font: UIFont.Roboto.regular),
                              alignment: .center,
                              numberOfLines: 2)
    }
    
    func buildActivationPlaceLabel() -> UILabel {
        return UILabel.styled(textColor: UIColor.ActivationUpcomingCell.cellPlace,
                              withFont: UIFont.custom(
                                ofSize: Constants.FontSize.tiny,
                                font: UIFont.Roboto.regular),
                              alignment: .center,
                              numberOfLines: 2)
    }
    
    func buildActivationWaitListView() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildActivationWaitingStackView() -> UIStackView {
        return UIStackView(axis: .horizontal, spacing: 10, alignment: .center, distribution: .fill).manualLayoutable()
    }
    
    func buildActivationWaitingIconImageView() -> UIImageView {
        return UIImageView(image: Icons.MyRsvps.waitlistIcon).manualLayoutable()
    }
    
    func buildActivationWaitListLabel() -> UILabel {
        return UILabel.styled(
            textColor: .white,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.tiny,
                font: UIFont.Roboto.regular
            ),
            alignment: .center)
    }
    
    func buildActionButtonView() -> UIView {
        return UIView().manualLayoutable()
    }
    
    func buildActionButton() -> ParlorButton {
        return ParlorButton().manualLayoutable()
    }
}

private extension Constants {
    enum UpcomingRsvps {
        enum ImageView {
            static let ratio: CGFloat = 4/3
        }

        static let cellCornerRadius: CGFloat = 20
        static let cellContentMargin: CGFloat = 20

        static let shadow = Shadow(offset: CGPoint(x: 0.0, y: 1.0), opacity: 0.2, radius: 2.0)
    }
}
