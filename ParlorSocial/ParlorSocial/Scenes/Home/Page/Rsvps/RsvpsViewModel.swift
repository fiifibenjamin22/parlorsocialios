//
//  RsvpsViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 21.05.2020.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CoreLocation

protocol RsvpsLogic: BaseViewModelLogic {
    var rsvpsSectionsRelay: BehaviorRelay<SectionPaginatedRepositoryState> { get }
    var destinationsObs: Observable<HomeDestination> { get }

    func loadRsvps()
    func loadMoreRsvps()
}

typealias RsvpsViewModelType = RsvpsLogic & HomeSubPageViewModelProtocol & HomeSubPageViewModelSelectors

final class RsvpsViewModel: BaseViewModel {
    lazy var checkInService: CheckInService = {
        let checkInService = CheckInService(
            locationService: LocationService(),
            guestListRepository: guestListRepository,
            activationRepository: activationsRepository
        )
        checkInService.delegate = self

        return checkInService
    }()

    fileprivate lazy var rsvpCanceledMapper: (Int, PaginatedData) -> Observable<PaginatedData> = { [unowned self] id, latestData -> Observable<PaginatedData> in
        var allItems: [HomeRsvp] = latestData.sections.flatMap { $0.getItems() ?? [] }
        guard let firstIndex = allItems.firstIndex(where: { $0.id == id }) else { return Observable.empty() }

        allItems.remove(at: firstIndex)

        return Observable.just(
            PaginatedData(self.createTableSections(from: allItems), latestData.metadata)
        )
    }

    fileprivate lazy var repositoryChangesMapper: (SingleHomeRsvpResposne, PaginatedData) -> Observable<PaginatedData> = {
        [unowned self] result, latestData -> Observable<PaginatedData> in
        guard case let .success(data) = result else {
            if case let .failure(error) = result {
                self.rsvpsSectionsRelay.accept(.error(error))
            }

            return Observable.empty()
        }

        let newRsvp = data.data
        var allItems: [HomeRsvp] = latestData.sections.flatMap { $0.getItems() ?? []}

        if let firstIndex = allItems.firstIndex(where: { $0.id == newRsvp.id }) {
            allItems[firstIndex] = newRsvp
        } else if let appendIndex = allItems.firstIndex(where: { $0.timeSegment.startAt > newRsvp.timeSegment.startAt }) {
            allItems.insert(newRsvp, at: appendIndex)
        } else {
            allItems.append(newRsvp)
        }

        return Observable.just(
            PaginatedData(self.createTableSections(from: allItems), latestData.metadata)
        )
    }

    private lazy var rsvpSelector: (HomeRsvp) -> Void = { [unowned self] rsvp in
        self.destinationsRelay.accept(
            .activationDetails(
                activationId: rsvp.id,
                openingType: .activationDetailsVC,
                navigationSource: .rsvps
            )
        )

        AnalyticService.shared.saveViewClickAnalyticEvent(
            withName: AnalyticEventClickableElementName.HomeScreen.Rsvps.upcomingItem.rawValue,
            referenceId: rsvp.id
        )
    }

    private lazy var cellActionButtonSelector: (HomeRsvp) -> Void = { [unowned self] rsvp in
        switch rsvp.actionButtonType {
        case .guestList:
            self.checkInService.navigateToGuestList(
                activationId: rsvp.id,
                checkInStrategy: rsvp.guestListAccess.checkInStrategy
            )
        case .updates, .live:
            self.activationDetailsInfoSelector(rsvp)
        default: break
        }
        guard let eventName = rsvp.actionButtonType.eventName else { return }

        AnalyticService.shared.saveViewClickAnalyticEvent(
            withName: eventName,
            referenceId: rsvp.id
        )
    }

    private lazy var activationDetailsInfoSelector: (HomeRsvp) -> Void = { [weak self] rsvp in
        self?.destinationsRelay.accept(.activationDetailsInfo(activationId: rsvp.id))
    }

    private lazy var rsvpHistorySelector: (HomeRsvp) -> Void = { [weak self] rsvp in
        if rsvp.isGuestListAvailable {
            self?.destinationsRelay.accept(.guestList(activationId: rsvp.id))
        }

        AnalyticService.shared.saveViewClickAnalyticEvent(
            withName: AnalyticEventClickableElementName.HomeScreen.Rsvps.historyItem.rawValue,
            referenceId: rsvp.id
        )
    }

    private lazy var allActivationsButtonSelector: () -> Void = { [unowned self] in
        AnalyticService.shared.saveViewClickAnalyticEvent(
            withName: AnalyticEventClickableElementName.HomeScreen.Rsvps.emptyViewButton.rawValue
        )
        self.changeToAllActivationsSubject.emitElement()
    }

    let analyticEventReferencedId: Int? = nil
    let rsvpsSectionsRelay = BehaviorRelay<SectionPaginatedRepositoryState>(value: .loading)

    private let changeToAllActivationsSubject: PublishSubject<Void> = PublishSubject()

    private let rsvpsDataRelay = BehaviorRelay<PaginatedData>(value: PaginatedData(sections: [], metadata: ListMetadata.initial))
    private let destinationsRelay: PublishRelay<HomeDestination> = PublishRelay()
    private let guestListRepository = GuestListRsvpRepository.shared
    private let homeRepository = HomeRepository.shared
    private let activationsRepository = ActivationsRepository.shared
    private var rsvpsRequestModel = HomeRsvps.Get.Request.initial
    private let loadDataSubject = PublishSubject<Void>()

    // MARK: - Initialization
    override init() {
        super.init()
        bindLoadDataRelay()
    }
}

// MARK: Private methods
extension RsvpsViewModel {
    private func bindLoadDataRelay() {
        rsvpsSectionsRelay
            .subscribe(onNext: { [unowned self] state in
                self.handle(rsvpsState: state)
            }).disposed(by: disposeBag)

        loadDataSubject
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .userInteractive))
            .setLoadingStateOnSubscribe(with: rsvpsSectionsRelay)
            .skipOnListPopulated(with: rsvpsDataRelay)
            .flatMapFirst { [unowned self] _ -> Observable<ApiResponse<HomeRsvps.Get.Response>> in
                return self.homeRepository.getRsvps(using: self.rsvpsRequestModel)
            }
            .mapResponseToTableSection(
                with: rsvpsDataRelay,
                sectionStateRelay: rsvpsSectionsRelay,
                sectionMaker: { [unowned self] in self.createTableSections(from: $0) }
            )
            .changePaginatedState(with: rsvpsSectionsRelay)
            .doOnNext { [unowned self] _ in self.rsvpsRequestModel.increasePage() }
            .bind(to: rsvpsDataRelay)
            .disposed(by: disposeBag)

        activationsRepository.rsvpWithIdCanceledObs
            .withLatestFrom(rsvpsDataRelay) { return ($0, $1) }
            .flatMap(rsvpCanceledMapper)
            .changePaginatedState(with: rsvpsSectionsRelay)
            .bind(to: rsvpsDataRelay)
            .disposed(by: disposeBag)

        activationsRepository.activationWithIdChangedObs
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .userInteractive))
            .flatMapFirst { [unowned self] id -> Observable<SingleHomeRsvpResposne> in
                return self.fetchRsvp(with: id)
            }
            .withLatestFrom(rsvpsDataRelay) { return ($0, $1) }
            .flatMap(repositoryChangesMapper)
            .changePaginatedState(with: rsvpsSectionsRelay)
            .bind(to: rsvpsDataRelay)
            .disposed(by: disposeBag)
    }

    private func createTableSections(from rsvps: [HomeRsvp]) -> [TableSection] {
        var upcomingRsvps: [HomeRsvp] = []
        var historyRsvps: [HomeRsvp] = []

        for rsvp in rsvps {
            if rsvp.status == .upcoming {
                upcomingRsvps.append(rsvp)
            } else {
                historyRsvps.append(rsvp)
            }
        }

        let sections = [
            createUpcomingRsvpsSection(for: upcomingRsvps),
            createHistoryRsvpsSection(for: historyRsvps)
        ].flatMap { $0 }

        let emptyDataSection = HomeEmptyDataSection.make(for: .rsvps, buttonSelector: self.allActivationsButtonSelector)

        return rsvps.isEmpty ? emptyDataSection : sections
    }

    private func createUpcomingRsvpsSection(for rsvps: [HomeRsvp]) -> [TableSection] {
        guard !rsvps.isEmpty else { return [] }

        return UpcomingRsvpsSection.make(
            for: rsvps,
            itemSelector: rsvpSelector,
            buttonSelector: cellActionButtonSelector
        )
    }

    private func createHistoryRsvpsSection(for rsvps: [HomeRsvp]) -> [TableSection] {
        guard !rsvps.isEmpty else { return [] }

        return HistoryRsvpsSection.make(
            for: rsvps,
            itemSelector: rsvpHistorySelector
        )
    }

    private func fetchRsvp(with id: Int) -> Observable<SingleHomeRsvpResposne> {
        return homeRepository.getRsvp(withId: id)
            .showingProgressBar(with: self)
    }

    private func handle(rsvpsState state: SectionPaginatedRepositoryState) {
        switch state {
        case .error(let err):
            errorSubject.onNext(err)
        default:
            return
        }
    }
}

extension RsvpsViewModel: HomeSubPageViewModelProtocol {
    var changeToAllActivationsObs: Observable<Void> {
        return changeToAllActivationsSubject.asObservable()
    }
}

extension RsvpsViewModel: HomeSubPageViewModelSelectors {
    func allActivationsButtonDidTap() {
        changeToAllActivationsSubject.emitElement()
    }
}

// MARK: Interface logic methods
extension RsvpsViewModel: RsvpsLogic {
    var destinationsObs: Observable<HomeDestination> {
        return destinationsRelay.asObservable()
    }

    func loadRsvps() {
        rsvpsDataRelay.accept(PaginatedData(sections: [], metadata: ListMetadata.initial))
        rsvpsRequestModel.set(page: 1)
        loadMoreRsvps()
    }

    func loadMoreRsvps() {
        loadDataSubject.emitElement()
    }
}

// MARK: - Check-in service
extension RsvpsViewModel: CheckInServiceDelegate {
    func didEndCheckInSuccess(_ checkInService: CheckInService, activationId: Int) {
        progressBarSubject.onNext(false)
        destinationsRelay.accept(.guestList(activationId: activationId))
    }
}

private extension ActionButtonType {
    var eventName: String? {
        let events = AnalyticEventClickableElementName.HomeScreen.Rsvps.self
        switch self {
        case .rsvp, .rsvpd: return nil
        case .guestList: return events.guestListActionButton.rawValue
        case .live: return events.liveActionButton.rawValue
        case .updates: return events.updatesActionButton.rawValue
        }
    }
}
