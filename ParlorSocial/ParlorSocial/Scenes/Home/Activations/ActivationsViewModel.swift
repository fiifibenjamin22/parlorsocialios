//
//  ActivationsViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 27.02.2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol ActivationsLogic: BaseViewModelLogic {
    var activationSectionsRelay: BehaviorRelay<SectionPaginatedRepositoryState> { get }
    var destinationsObs: Observable<HomeDestination> { get }

    func refreshActivities()
    func loadMoreActivities()

    func loadActivations()
}

final class ActivationsViewModel: BaseViewModel {
    lazy var checkInService: CheckInService = {
        let checkInService = CheckInService(
            locationService: LocationService(),
            guestListRepository: guestListRepository,
            activationRepository: activationsRepository
        )
        checkInService.delegate = self

        return checkInService
    }()

    private lazy var activationSelector: (HomeActivation) -> Void = { [unowned self] activation in
        let openingType = ActivationDetailsNavigationProvider.getOpeningActivationDetailsType(with: activation)
        switch openingType {
        case .activationDetailsVC, .addProfilePhotoVC, .upgradeToPremiumForStandard:
            let navigationSource = self.getActivationDetailsNavigationSource(usingActivationType: self.activationsRequestModel.type)
            self.destinationsRelay.accept(.activationDetails(activationId: activation.id, openingType: openingType, navigationSource: navigationSource))
        case .startedAlert:
            self.alertDataSubject.onNext(AlertData.fromSimpleMessage(message: Strings.Activation.startedAlert.localized))
        }

        AnalyticService.shared.saveViewClickAnalyticEvent(
            withName: AnalyticEventClickableElementName.HomeScreen.All.item.rawValue,
            referenceId: activation.id
        )
    }

    private lazy var activationInfoSelector: (HomeActivation) -> Void = { [unowned self] activation in
        self.destinationsRelay.accept(.activationDetailsInfo(activationId: activation.id))
    }

    private lazy var cellActionButtonSelector: (HomeActivation) -> Void = { [unowned self] activation in
        switch activation.actionButtonType {
        case .rsvp, .rsvpd: self.activationSelector(activation)
        case .updates, .live: self.activationInfoSelector(activation)
        case .guestList:
            self.checkInService.navigateToGuestList(
                activationId: activation.id,
                checkInStrategy: activation.guestListAccess.checkInStrategy
            )
        default: break
        }

        AnalyticService.shared.saveViewClickAnalyticEvent(
            withName: activation.actionButtonType.eventName,
            referenceId: activation.id
        )
    }

    fileprivate lazy var repositoryChangesMapper: (SingleHomeActivationResposne, PaginatedData) -> Observable<PaginatedData> = { [unowned self] result, latestData -> Observable<PaginatedData> in
        guard case let .success(data) = result else {
            if case let .failure(error) = result {
                self.activationSectionsRelay.accept(.error(error))
            }

            return Observable.empty()
        }

        let newActivation = data.data
        var allActivations: [HomeActivation] = latestData.sections.flatMap { $0.getItems() ?? [] }
        guard let firstIndex = allActivations.firstIndex(where: { $0.id == newActivation.id }) else { return Observable.empty() }

        if allActivations[firstIndex] != newActivation {
            allActivations[firstIndex] = newActivation
            return Observable.just(PaginatedData(
                DefaultActivationSection.make(
                    for: allActivations,
                    itemSelector: self.activationSelector,
                    buttonSelector: self.cellActionButtonSelector
                ),
                latestData.metadata
            ))
        } else {
            return Observable.empty()
        }
    }

    private var activationsRequestModel = HomeActivations.Get.Request.initialAll

    let analyticEventReferencedId: Int? = nil
    let activationSectionsRelay = BehaviorRelay<SectionPaginatedRepositoryState>(value: .loading)

    private let activationsDataRelay = BehaviorRelay<PaginatedData>(value: PaginatedData(sections: [], metadata: ListMetadata.initial))
    private let destinationsRelay: PublishRelay<HomeDestination> = PublishRelay()
    private let activationsRepository = ActivationsRepository.shared
    private let guestListRepository = GuestListRsvpRepository.shared
    private let homeRepository = HomeRepository.shared
    private let loadDataRelay = BehaviorRelay(value: 0)

    override init() {
        super.init()

        bindLoadDataRelay()
        loadMoreActivities()
    }
}

// MARK: Private logic
private extension ActivationsViewModel {
    private func getActivationDetailsNavigationSource(usingActivationType type: Activation.ActivationType) -> ActivationDetailsNavigationSource {
        switch type {
        case .all: return .activationsAll
        case .happening: return .activationsHappenings
        case .mixer: return .activationsMixers
        }
    }
}

// MARK: Interface logic methods
extension ActivationsViewModel: ActivationsLogic {
    var destinationsObs: Observable<HomeDestination> {
        return destinationsRelay.asObservable()
    }

    func refreshActivities() {
        activationsDataRelay.accept(PaginatedData(sections: [], metadata: ListMetadata.initial))
        activationsRequestModel.set(page: 1)

        loadMoreActivities()
    }

    func loadMoreActivities() {
        loadDataRelay.accept(0)
    }

    func loadActivations() {
        invalidateRequestModel()
        clearData()
        loadMoreActivities()
    }
}

// MARK: - Private methods
extension ActivationsViewModel {
    private func invalidateRequestModel() {
        activationsRequestModel.set(page: 1)
    }

    private func clearData() {
        activationsDataRelay.accept(PaginatedData(sections: [], metadata: ListMetadata.initial))
        activationSectionsRelay.accept(.paging(elements: []))
    }

    private func bindLoadDataRelay() {
        activationSectionsRelay
            .subscribe(onNext: { [unowned self] state in
                self.handle(activationState: state)
            }).disposed(by: disposeBag)
        
        loadDataRelay
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .userInteractive))
            .setLoadingStateOnSubscribe(with: activationSectionsRelay)
            .skipOnListPopulated(with: activationsDataRelay)
            .flatMapLatest { [unowned self] _ -> Observable<ApiResponse<HomeActivations.Get.Response>> in
                return self.homeRepository.getHomeActivations(using: self.activationsRequestModel)
            }
            .mapResponseToTableSection(
                with: activationsDataRelay,
                sectionStateRelay: activationSectionsRelay,
                sectionMaker: { [unowned self] in
                    DefaultActivationSection.make(
                        for: $0,
                        itemSelector: self.activationSelector,
                        buttonSelector: self.cellActionButtonSelector
                    )
            })
            .changePaginatedState(with: activationSectionsRelay)
            .doOnNext { [unowned self] _ in self.activationsRequestModel.increasePage() }
            .bind(to: activationsDataRelay)
            .disposed(by: disposeBag)

        activationsRepository.activationWithIdChangedObs
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .userInteractive))
            .flatMapFirst { [unowned self] id -> Observable<SingleHomeActivationResposne> in
                return self.fetchActivation(with: id)
            }
            .withLatestFrom(activationsDataRelay) { return ($0, $1) }
            .flatMap(repositoryChangesMapper)
            .changePaginatedState(with: activationSectionsRelay)
            .bind(to: activationsDataRelay)
            .disposed(by: disposeBag)
    }
    
    private func handle(activationState state: SectionPaginatedRepositoryState) {
        switch state {
        case .error(let err):
            errorSubject.onNext(err)
        default:
            return
        }
    }

    private func fetchActivation(with id: Int) -> Observable<SingleHomeActivationResposne> {
        return homeRepository.getHomeActivation(withId: id)
            .showingProgressBar(with: self)
    }
}

// MARK: - Check-in service
extension ActivationsViewModel: CheckInServiceDelegate {
    func didEndCheckInSuccess(_ checkInService: CheckInService, activationId: Int) {
        progressBarSubject.onNext(false)
        destinationsRelay.accept(.guestList(activationId: activationId))
    }
}

private extension ActionButtonType {
    var eventName: String {
        let events = AnalyticEventClickableElementName.HomeScreen.All.self
        switch self {
        case .guestList: return events.guestListActionButton.rawValue
        case .live: return events.liveActionButton.rawValue
        case .rsvp: return events.rsvpActionButton.rawValue
        case .rsvpd: return events.rsvpdActionButton.rawValue
        case .updates: return events.updatesActionButton.rawValue
        }
    }
}
