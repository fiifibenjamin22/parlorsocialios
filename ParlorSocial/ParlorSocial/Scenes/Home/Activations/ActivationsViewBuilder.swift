//
//  ActivationsViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 27.02.2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

final class ActivationsViewBuilder {
    private unowned let controller: ActivationsViewController
    private var view: UIView! { return controller.view }

    init(controller: ActivationsViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }

    private func setupProperties() {
        controller.view.backgroundColor = .appPrimary
    }

    private func setupHierarchy() {
        controller.view.addSubview(controller.tableView)
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.tableView.edgesToParent()
        }
    }
}

extension ActivationsViewBuilder {
    func buildTableView() -> UITableView {
        let tableView = UITableView(frame: .zero, style: .grouped).manualLayoutable()
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.backgroundColor = .clear
        tableView.contentInset = UIEdgeInsets.margins(top: -CGFloat.leastPositiveForTableView)

        return tableView
    }
}
