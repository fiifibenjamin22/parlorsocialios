//
//  BasicActivationSection.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 29/03/2019.
//

import UIKit

final class DefaultActivationSection: BasicTableSection<HomeActivation, ActivationTableViewCell> {
    private let buttonSelector: ((HomeActivation) -> Void)

    init(
        items: [HomeActivation],
        itemSelector: @escaping ((HomeActivation) -> Void) = { _ in },
        buttonSelector: @escaping ((HomeActivation) -> Void) = { _ in }
    ) {
        self.buttonSelector = buttonSelector
        super.init(items: items, itemSelector: itemSelector)
    }

    override var headerType: UITableViewHeaderFooterView.Type? { return ActivationHeaderView.self }

    override func configure(header: UITableViewHeaderFooterView, in section: Int) {
        guard let activationHeader = header as? ActivationHeaderView else {
            fatalError("Header is wrong type! Needed: \(ActivationHeaderView.name) but get instead \(String(describing: header))")
        }

        activationHeader.setup(with: items.first)
        activationHeader.showTopLine = section != 0
    }

    override func configure(cell: UITableViewCell, at indexPath: IndexPath) {
        if let cell = cell as? ActivationTableViewCell {
            cell.setActionButtonDidTapClosure { [unowned self] in
                self.buttonSelector(self.items[indexPath.item])
            }
        }

        super.configure(cell: cell, at: indexPath)
    }
}

extension DefaultActivationSection {

    static func make(
        for activations: [HomeActivation],
        itemSelector: @escaping ((HomeActivation) -> Void) = { _ in },
        buttonSelector: @escaping ((HomeActivation) -> Void)
    ) -> [TableSection] {
        var mutableActivations = activations

        mutableActivations.sort {
            return $0.startAt.compare($1.startAt) == .orderedAscending
        }

        guard Config.userProfile?.type != .preview else {
            return [
                DefaultActivationSection(
                    items: mutableActivations,
                    itemSelector: itemSelector,
                    buttonSelector: buttonSelector
                )
            ]
        }

        var slicedActivations: [[HomeActivation]] = []

        var nextDate: Date?
        var nextApproximateStartTimeStatus: Bool?
        for activation in mutableActivations {
            if nextDate?.isTheSameDay(as: activation.startAt) == true && activation.isApproximatedStartTime == nextApproximateStartTimeStatus {
                slicedActivations[slicedActivations.count - 1].append(activation)
            } else {
                slicedActivations.append([activation])
                nextDate = activation.startAt
                nextApproximateStartTimeStatus = activation.isApproximatedStartTime
            }
        }

        return slicedActivations.compactMap {
            DefaultActivationSection(
                items: $0,
                itemSelector: itemSelector,
                buttonSelector: buttonSelector
            )
        }
    }
}

