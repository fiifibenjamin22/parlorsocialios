//
//  ActivationHeaderViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 04.03.2019.
//

import UIKit

final class ActivationHeaderViewBuilder {

    private unowned let header: ActivationHeaderView
    private var view: UIView! { return header.contentView }

    init(header: ActivationHeaderView) {
        self.header = header
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }

}

extension ActivationHeaderViewBuilder {

    private func setupProperties() {
        header.dividerView.backgroundColor = .appSeparator
    }

    private func setupHierarchy() {
        [header.dateLabel, header.dividerView].addTo(parent: view)
    }

    private func setupAutoLayout() {
        header.dateLabel.edgesToParent(
            anchors: [.leading, .trailing],
            insets: UIEdgeInsets.margins(
                left: Constants.Margin.standard,
                right: Constants.Margin.standard))
        header.dateLabel.bottomAnchor.equal(to: view.bottomAnchor, constant: -Constants.ActivationHeader.labelBottomMargin, withPriority: .defaultHigh)
        
        header.dividerView.edgesToParent(anchors: [.leading, .top, .trailing])
        header.dividerView.heightAnchor.equalTo(constant: Constants.separatorSize)
    }

}

extension ActivationHeaderViewBuilder {

    func buildLabel() -> UILabel {
        return ParlorLabel.styled(
            withTextColor: .black,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.small,
                font: UIFont.Roboto.bold),
            letterSpacing: Constants.LetterSpacing.small
        )
    }
    
    func buildDateLabelTopAnchor() -> NSLayoutConstraint {
        return header.dateLabel.topAnchor.equal(to: view.topAnchor, constant: Constants.ActivationHeader.labelTopMargin, withPriority: .defaultHigh)
    }

    func buildView() -> UIView {
        return UIView().manualLayoutable()
    }

}

private extension Constants {

    enum ActivationHeader {
        static let labelTopMargin: CGFloat = 25
        static let labelBottomMargin: CGFloat = 3
    }
}
