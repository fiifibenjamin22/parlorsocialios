//
//  ActivationHeaderView.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 04.03.2019.
//

import UIKit

final class ActivationHeaderView: UITableViewHeaderFooterView {
    private(set) var dateLabel: UILabel!
    private(set) var dividerView: UIView!
    private(set) var dateLabelTopAnchor: NSLayoutConstraint!

    var showTopLine: Bool = true {
        didSet { dividerView.isHidden = !showTopLine }
    }
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Public methods
extension ActivationHeaderView {
    func setup(with firstActivation: HomeActivation?) {
        guard let activation = firstActivation else { return }
        dateLabel.text = activation.startAt
            .toAllActivationsHeaderFormat(isApproximatedStartTime: activation.isApproximatedStartTime)
            .checkIfVersionAppIsPreview(with: Strings.Activation.upcomingEvents.localized.uppercased())
        dateLabelTopAnchor.constant = dateLabel.text.isEmptyOrNil ? Constants.Margin.standard : Constants.ActivationHeader.labelTopMargin
    }
}

// MARK: - Private methods
extension ActivationHeaderView {
    private func initialize() {
        setupViews()
    }

    private func setupViews() {
        let builder = ActivationHeaderViewBuilder(header: self)

        dateLabel = builder.buildLabel()
        dividerView = builder.buildView()

        builder.setupViews()
        dateLabelTopAnchor = builder.buildDateLabelTopAnchor()
    }
}

fileprivate extension Constants {
    enum ActivationHeader {
        static let labelTopMargin: CGFloat = 25
    }
}
