//
//  ActivationsTableViewCell.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 28/02/2019.
//

import UIKit

final class ActivationTableViewCell: UITableViewCell, ConfigurableCell {
    typealias Model = HomeActivation

    // MARK: Views

    private(set) var activationSummary: UILabel!
    private(set) var activationImageView: UIImageView!
    private(set) var activationTitle: UILabel!
    private(set) var activationInformationView: ActivationInformationView!
    private(set) var premiumStackView: UIStackView!
    private(set) var premiumImageView: UIImageView!
    private(set) var premiumLabel: UILabel!

    private(set) var titlesStackView: UIStackView!
    private(set) var friendsListView: FriendsListView!

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initializeCell()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.autoresizingMask = .flexibleHeight
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        activationInformationView.actionButtonDidTapClosure = nil
        activationInformationView.actionButton.resetLayout()
    }
}

// MARK: - Public methods
extension ActivationTableViewCell {
    func configure(with model: HomeActivation) {
        activationSummary.text = model.summary
        activationTitle.text = model.name
        activationImageView.getImage(from: model.imageURL)

        activationInformationView.configure(with: model)

        premiumStackView.isHidden = !model.isPremium

        friendsListView.set(imageUrls: model.attendingFriendsInfo.imageUrls, title: model.attendingFriendsInfo.text)
    }

    func setActionButtonDidTapClosure(_ closure: @escaping (() -> Void)) {
        activationInformationView.actionButtonDidTapClosure = closure
    }
}

// MARK: - Private methods
extension ActivationTableViewCell {
    private func initializeCell() {
        selectionStyle = .none

        setupViews()
    }

    private func setupViews() {
        let builder = ActivationTableViewCellBuilder(cell: self)

        activationSummary = builder.buildSummaryLabel()
        activationImageView = builder.buildActivationImageView()
        activationTitle = builder.buildTitleLabel()

        activationInformationView = builder.buildActivationInformationView()

        premiumStackView = builder.buildPremiumStackView()
        premiumImageView = builder.buildPremiumImageView()
        premiumLabel = builder.buildPremiumLabel()

        titlesStackView = builder.buildTitlesStackView()

        friendsListView = builder.buildFriendsListView()

        builder.setupViews()
    }
}
