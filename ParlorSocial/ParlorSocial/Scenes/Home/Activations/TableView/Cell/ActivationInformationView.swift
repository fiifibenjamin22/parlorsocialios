//
//  ActivationInformationView.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 28/04/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

class ActivationInformationView: UIView {
    var actionButtonDidTapClosure: (() -> Void)?

    // MARK: - Views

    private(set) var rsvpHeaderContainer: UIView!
    private(set) var actionButton: ParlorButton!
    private(set) var locationInformationView: InformationView!
    private(set) var timeInformationView: InformationView!

    // MARK: - Initalization

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Public methods
extension ActivationInformationView {
    func configure(with model: HomeActivation) {
        setupButton(for: model.actionButtonType)

        switch model.actionButtonType {
        case .rsvpd: setupButtonImageForRsvpdType()
        default: break
        }

        locationInformationView.informationLabel.attributedText = model.location?.neighborhood.hyphenAttributed()

        timeInformationView.informationLabel.text =
            model.startAt.toString(withFormat: Date.Format.allActivations).uppercased()
        timeInformationView.informationIcon.image = Icons.AllActivations.clock
    }

    private func setupButtonImageForRsvpdType() {
        actionButton.setImage(Icons.AllActivations.check, for: .normal)
        actionButton.titleEdgeInsets = Constants.ActivationInformationView.ActionButton.Rsvpd.titleEdgeInsets
        actionButton.imageEdgeInsets = Constants.ActivationInformationView.ActionButton.Rsvpd.imageEdgeInsets
    }

    private func setupButton(for type: ActionButtonType) {
        actionButton.style = actionButtonStyle(for: type)
        actionButton.title = type.title
        actionButton.setTarget(self, action: #selector(actionButtonDidTap), for: .touchUpInside)
    }

    private func actionButtonStyle(for type: ActionButtonType) -> ParlorButtonStyle {
        switch type {
        case .rsvpd, .rsvp:
            return .filledWith(
                color: UIColor.appTextBright,
                titleColor: UIColor.white,
                font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Editor.medium),
                letterSpacing: Constants.LetterSpacing.medium
            )
        default: return type.defaultStyle
        }
    }
}

// MARK: - Private methods
extension ActivationInformationView {
    private func initialize() {
        manualLayoutable()
        setupViews()
    }

    private func setupViews() {
        let builder = ActivationInformationViewBuilder(view: self)

        rsvpHeaderContainer = builder.buildView()
        actionButton = builder.buildButton()
        locationInformationView = builder.buildInforamtionView()
        timeInformationView = builder.buildInforamtionView()

        builder.setupViews()
    }

    @objc private func actionButtonDidTap() {
        actionButtonDidTapClosure?()
    }
}

fileprivate extension String {

    func hyphenAttributed() -> NSAttributedString {
        let paragraphStyle = NSMutableParagraphStyle().apply { $0.hyphenationFactor = 1.0 }
        let hyphenAttribute = [NSAttributedString.Key.paragraphStyle: paragraphStyle] as [NSAttributedString.Key: Any]
        return NSAttributedString(string: self, attributes: hyphenAttribute)
    }

}

private extension Constants {
    enum ActivationInformationView {
        enum ActionButton {
            enum Rsvpd {
                static let imageEdgeInsets = UIEdgeInsets(top: 0, left: -1, bottom: 0, right: 3)
                static let titleEdgeInsets = UIEdgeInsets(top: 0, left: 3, bottom: 0, right: -3)
            }
        }
    }
}
