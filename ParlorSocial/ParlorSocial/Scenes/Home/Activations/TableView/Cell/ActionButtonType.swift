//
//  ActionButtonType.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 30/04/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

enum ActionButtonType: String, Codable {
    case rsvp
    case rsvpd
    case guestList = "guest_list"
    case updates
    case live

    var title: String {
        switch self {
        case .rsvp: return Strings.Activation.rsvp.localized
        case .rsvpd: return Strings.Activation.rsvpd.localized
        case .guestList: return Strings.Activation.guestList.localized
        case .updates: return Strings.Activation.updates.localized
        case .live: return Strings.Activation.live.localized
        }
    }

    var defaultStyle: ParlorButtonStyle {
        switch self {
        case .rsvp:
            return .outlined(
                titleColor: .white,
                borderColor: UIColor.rsvpBorder,
                font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Editor.medium),
                letterSpacing: Constants.LetterSpacing.medium
            )
        case .rsvpd:
            return .filledWith(
                color: UIColor.clear,
                titleColor: UIColor.white,
                font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Editor.medium),
                letterSpacing: Constants.LetterSpacing.medium
            )
        case .live:
            return .filledWith(
                color: UIColor.backgroundGreen,
                titleColor: UIColor.appText,
                font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium),
                letterSpacing: Constants.LetterSpacing.none
            )
        case .guestList, .updates: return .white(font: UIFont.custom(ofSize: Constants.FontSize.tiny, font: UIFont.Roboto.medium))
        }
    }
}
