//
//  ActivationTableViewCellBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 28/02/2019.
//

import UIKit

final class ActivationTableViewCellBuilder {
    private unowned let cell: ActivationTableViewCell
    private var view: UIView! { return cell.contentView }

    init(cell: ActivationTableViewCell) {
        self.cell = cell
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

extension ActivationTableViewCellBuilder {
    private func setupProperties() {
        cell.activationImageView.clipsToBounds = true

        cell.activationInformationView.backgroundColor = .appText
        cell.activationInformationView.roundCorners()

        cell.premiumLabel.text = Strings.Activation.premium.localized
        cell.premiumLabel.setContentHuggingPriority(UILayoutPriority.defaultHigh.increase(), for: .vertical)

        // TODO - PSC-6063 Hidden for 2.0 release
        cell.friendsListView.isHidden = true
    }

    private func setupHierarchy() {
        [cell.activationImageView,
            cell.titlesStackView,
            cell.activationInformationView,
            cell.activationSummary].addTo(parent: view)

        cell.titlesStackView.addArrangedSubviews(
            [cell.activationTitle, cell.premiumStackView, cell.friendsListView])
        cell.premiumStackView.addArrangedSubviews(
            [cell.premiumImageView, cell.premiumLabel])
    }

    private func setupAutoLayout() {
        cell.activationSummary.edgesToParent(anchors: [.leading, .top, .trailing])

        cell.activationImageView.edgesToParent(
            anchors: [.leading, .trailing],
            insets: UIEdgeInsets.margins(left: Constants.Margin.standard)
        )
        cell.activationImageView.topAnchor.equal(to: cell.activationSummary.bottomAnchor, constant: Constants.AllActivationsCell.activationImageViewTopOffset)
        cell.activationImageView.widthAnchor.constraint(equalTo: cell.activationImageView.heightAnchor, multiplier: 2).activate()

        cell.titlesStackView.leadingAnchor.equal(to: view.leadingAnchor, constant: Constants.Margin.large)
        cell.titlesStackView.topAnchor.equal(to: cell.activationImageView.bottomAnchor, constant: Constants.Margin.standard)
        cell.titlesStackView.trailingAnchor.equal(to: cell.activationInformationView.leadingAnchor, constant: -Constants.Margin.standard)
        cell.titlesStackView.bottomAnchor.lessThanOrEqual(to: view.bottomAnchor, constant: -Constants.AllActivationsCell.cellBottomMargin)

        cell.activationInformationView.topAnchor.equal(to: cell.activationImageView.bottomAnchor, constant: -Constants.AllActivationsCell.rsvpHeaderHeight)
        cell.activationInformationView.trailingAnchor.equal(to: view.trailingAnchor, constant: -Constants.Margin.standard)
        cell.activationInformationView.bottomAnchor.lessThanOrEqual(to: view.bottomAnchor, constant: -Constants.AllActivationsCell.cellBottomMargin)
        cell.activationInformationView.widthAnchor.equalTo(constant: Constants.AllActivationsCell.informationWidth)

        cell.premiumImageView.widthAnchor.equalTo(constant: Constants.AllActivationsCell.premiumImageSize)

        cell.friendsListView.heightAnchor.equalTo(constant: Constants.AllActivationsCell.friendsListViewHeight)
    }
}

extension ActivationTableViewCellBuilder {
    func buildSummaryLabel() -> UILabel {
        let label = UILabel.styled(
            textColor: .appText,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.subheading,
                font: UIFont.Dalmatins.regular
            )
        )

        label.setContentCompressionResistancePriority(.required, for: .horizontal)
        return label
    }

    func buildActivationImageView() -> UIImageView {
        let imageView = UIImageView().manualLayoutable()
        imageView.contentMode = .scaleAspectFill

        return imageView
    }

    func buildTitleLabel() -> UILabel {
        return UILabel.styled(
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.heading,
                font: UIFont.Editor.medium),
            alignment: .left)
    }

    func buildActivationInformationView() -> ActivationInformationView {
        return ActivationInformationView()
    }

    func buildTitlesStackView() -> UIStackView {
        return UIStackView(axis: .vertical, spacing: 10)
    }

    func buildPremiumStackView() -> UIStackView {
        return UIStackView(axis: .horizontal, spacing: 5)
    }

    func buildPremiumLabel() -> UILabel {
        return UILabel.styled(
            textColor: .appGold,
            withFont: UIFont.custom(
                ofSize: Constants.FontSize.small,
                font: UIFont.Roboto.regular),
            alignment: .left)
    }

    func buildPremiumImageView() -> UIImageView {
        let imageView = UIImageView().manualLayoutable()
        imageView.contentMode = .scaleAspectFit
        imageView.image = Icons.AllActivations.premium
        imageView.tintColor = .appGold
        return imageView
    }

    func buildFriendsListView() -> FriendsListView {
        return FriendsListView()
    }
}

private extension Constants {
    enum AllActivationsCell {
        static let activationImageViewTopOffset: CGFloat = -32
        static let informationWidth: CGFloat = 115
        static let rsvpHeaderHeight: CGFloat = 40
        static let premiumImageSize: CGFloat = 12
        static let friendsListViewHeight: CGFloat = 30
        static let cellBottomMargin: CGFloat = 60
    }
}
