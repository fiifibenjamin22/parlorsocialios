//
//  ActivationInformationViewBuilder.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 28/04/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

class ActivationInformationViewBuilder {
    private unowned let view: ActivationInformationView

    init(view: ActivationInformationView) {
        self.view = view
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension ActivationInformationViewBuilder {
    private func setupProperties() {
        view.rsvpHeaderContainer.backgroundColor = .appGrey

        view.locationInformationView.tintColor = .appTextLight
        view.timeInformationView.tintColor = .appTextLight

        view.locationInformationView.informationIcon.image = Icons.AllActivations.location
    }

    private func setupHierarchy() {
        [view.rsvpHeaderContainer,
         view.locationInformationView,
         view.timeInformationView].addTo(parent: view)

        view.rsvpHeaderContainer.addSubview(view.actionButton)
    }

    private func setupAutoLayout() {
        view.rsvpHeaderContainer.edgesToParent(anchors: [.leading, .top, .trailing])
        view.rsvpHeaderContainer.heightAnchor.equalTo(constant: Constants.AllActivationsView.rsvpHeaderHeight)

        view.actionButton.edgesToParent()

        view.locationInformationView.leadingAnchor.equal(to: view.leadingAnchor, constant: Constants.Margin.standard)
        view.locationInformationView.topAnchor.equal(to: view.rsvpHeaderContainer.bottomAnchor, constant: Constants.Margin.standard)
        view.locationInformationView.trailingAnchor.lessThanOrEqual(to: view.trailingAnchor, constant: -Constants.Margin.standard)

        view.timeInformationView.leadingAnchor.equal(to: view.leadingAnchor, constant: Constants.Margin.standard)
        view.timeInformationView.topAnchor.equal(to: view.locationInformationView.bottomAnchor, constant: Constants.AllActivationsView.timeTopMargin)
        view.timeInformationView.bottomAnchor.equal(to: view.bottomAnchor, constant: -Constants.AllActivationsView.timeBottomMargin)
        view.timeInformationView.trailingAnchor.lessThanOrEqual(to: view.trailingAnchor, constant: -Constants.Margin.standard)
    }
}

// MARK: - Public build methods
extension ActivationInformationViewBuilder {
    func buildView() -> UIView {
        return UIView().manualLayoutable()
    }

    func buildButton() -> ParlorButton {
        return ParlorButton().manualLayoutable()
    }

    func buildInforamtionView() -> InformationView {
        return InformationView().manualLayoutable()
    }
}

private extension Constants {
    enum AllActivationsView {
        static let rsvpHeaderHeight: CGFloat = 40
        static let timeTopMargin: CGFloat = 10
        static let timeBottomMargin: CGFloat = 20
    }
}
