//
//  ActivationsViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 27.02.2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import RxSwift

final class ActivationsViewController: AppViewController, ScrollableContent {
    
    // MARK: - Properties
    let analyticEventScreen: AnalyticEventScreen? = .allList

    var analyticEventScreenSubject: PublishSubject<AnalyticEventViewControllerData>? {
        return viewModel.analyticEventScreenSubject
    }

    var viewModel: ActivationsLogic!

    override var connectedOnboardingType: OnboardingType? { return .home }

    weak var delegate: HomeViewControllerProtocol?

    private var tableManager: TableViewManager!
    
    private var isWelcomeViewHidden: Bool {
        return delegate?.isWelcomeViewHidden ?? true
    }

    override var observableSources: ObservableSources? {
        return self
    }

    // MARK: - Views
    private(set) var containerView: UIView!
    private(set) var tableView: UITableView!

    // MARK: - Lifecycle

    init(router: Router) {
        super.init(nibName: nil, bundle: nil)

        self.router = router
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()

        tableManager = TableViewManager(tableView: tableView, delegate: self)
        tableManager.setupRefreshControl()

        setupPresentationLogic()
    }
}

// MARK: - Public methods
extension ActivationsViewController {
    func scrollToTop() {
        tableManager.scrollToTop()
    }
}

// MARK: - Private methods
extension ActivationsViewController {
    private func setupViews() {
        let builder = ActivationsViewBuilder(controller: self)

        self.tableView = builder.buildTableView()
        
        builder.setupViews()
    }

    private func setupPresentationLogic() {
        setupTableView()
        bindActivationsState()
        bindDestinationObservable()
    }

    private func setupTableView() {
        tableManager.footerStyle = .light
        tableManager.refreshControl?.tintColor = .appGrey
    }
    
    private func bindDestinationObservable() {
        viewModel.destinationsObs
            .filter { [unowned self] _ in self.isWelcomeViewHidden }
            .subscribe(onNext: { [unowned self] destination in
                self.router.push(destination: destination)
            }).disposed(by: disposeBag)
    }

    private func bindActivationsState() {
        viewModel.activationSectionsRelay
            .subscribe(
                onNext: { [unowned self] state in
                    self.handle(activationsState: state)
                },
                onError: { error in
                    print(error)
                }
            ).disposed(by: disposeBag)
    }

    private func handle(activationsState state: SectionPaginatedRepositoryState) {
        switch state {
        case .loading: tableManager.showLoadingFooter()
        case .paging(let elements):
            tableManager.setupWith(sections: elements)
            tableManager.showLoadingFooter()
        case .populated(let elements):
            tableManager.invalidateCache()
            tableManager.setupWith(sections: elements)
            tableManager.hideLoadingFooter()
        case .error: tableManager.hideLoadingFooter()
        }
    }
}

// MARK: - Table Manager Delegate
extension ActivationsViewController: TableManagerDelegate {
    func didSwipeForRefresh() {
        viewModel.refreshActivities()
    }

    func loadMoreData() {
        viewModel.loadMoreActivities()
    }
    
    func tableViewDidScroll(_ scrollView: UIScrollView) { }
}

// MARK: - Refreshable view implementation
extension ActivationsViewController: RefreshableView {
    func refreshData() {
        viewModel.refreshActivities()
    }
}

// MARK: - Observable sources
extension ActivationsViewController: ObservableSources {
    var closeObs: Observable<Void> {
        return viewModel.closeObs
    }
    
    var alertDataObs: Observable<AlertData> {
        return viewModel.alertDataObs.filter { [unowned self] _ in
            self.isWelcomeViewHidden
        }
    }
    
    var errorObs: Observable<AppError> {
        return viewModel.errorObs
    }
    
    var progressBarObs: Observable<Bool> {
        return viewModel.progressBarObs
    }
}
