//
//  ActivationsRouter.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 27.02.2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

enum HomeDestination {
    case guestList(activationId: Int)
    case activationDetails(activationId: Int, openingType: OpeningActivationDetailsType, navigationSource: ActivationDetailsNavigationSource)
    case activationDetailsInfo(activationId: Int)
    case membershipPlans
    case notificationsReminder(NotificationsSettingsChangedEventSource)
    case rsvpDetails(activationId: Int)
}

extension HomeDestination: Destination {
    var viewController: UIViewController {
        switch self {
        case .activationDetails(let activationId, let openingType, let navigationSource):
            return ActivationDetailsNavigationProvider.getVCByAvailabilityOfOpeningActivationDetails(
                forActivationId: activationId,
                openingType: openingType,
                navigationSource: navigationSource
            )
        case .activationDetailsInfo(let activationId):
            return ActivationDetailsInfoViewController(activationId: activationId)
        case .guestList(let activationId):
            return GuestListRsvpViewController(activationId: activationId)
        case .membershipPlans:
            return MembershipPlansViewController()
        case .notificationsReminder(let source):
            return EnableNotificationsReminderViewController(notificationsSettingsChangeSource: source)
        case .rsvpDetails(let activationId):
            return RsvpDetailsViewController(activationId: activationId)
        }
    }
}
