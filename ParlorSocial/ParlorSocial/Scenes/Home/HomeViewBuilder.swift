//
//  HomeViewBuilder.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 03/04/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit

class HomeViewBuilder {

    private unowned let controller: HomeViewController
    private var view: UIView! { return controller.view }

    init(controller: HomeViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension HomeViewBuilder {
    private func setupProperties() {
        controller.apply {
            $0.topTabBarStackViewContainer.addShadow(withOpacity: 0.1, radius: 10)
            $0.mixersItem.title = Strings.Activation.mixersButton.localized
            $0.happeningsItem.title = Strings.Activation.happeningsButton.localized
            $0.rsvpsItem.title = Strings.Activation.rsvpsButton.localized
        }
    }

    private func setupHierarchy() {
        controller.apply {
            view.addSubviews([$0.containerView, $0.topTabBarStackViewContainer, $0.homeWelcomeView])
            $0.topTabBarStackViewContainer.addSubview($0.topTabBarStackView)
            $0.topTabBarStackView.addArrangedSubviews([$0.mixersItem, $0.happeningsItem, $0.rsvpsItem])
        }
    }

    private func setupAutoLayout() {
        controller.apply {
            $0.topTabBarStackViewContainer.apply {
                $0.edgesToParent(anchors: [.trailing, .leading])
                $0.topAnchor.equal(to: controller.activateMembershipView.bottomAnchor)
                $0.heightAnchor.equalTo(constant: 46)
            }

            $0.topTabBarStackView.edgesToParent()

            $0.containerView.apply {
                $0.edgesToParent(anchors: [.trailing, .leading, .bottom])
                $0.topAnchor.equal(to: controller.topTabBarStackViewContainer.bottomAnchor)
            }

            $0.homeWelcomeView.edgesToParent()
        }
    }

}

// MARK: - Public build methods
extension HomeViewBuilder {
    func buildView() -> UIView {
        return UIView()
    }

    func buildTopBarItem() -> TopTabBarItemView {
        return TopTabBarItemView(bottomLineStyle: .fixedWidthMultiplier(multiplier: 0.72)).manualLayoutable()
    }

    func buildTopTabBarStackView() -> UIStackView {
        return UIStackView(
            axis: .horizontal
        ).apply {
            $0.backgroundColor = UIColor.white
            $0.distribution = .fillEqually
        }
    }

    func buildHomeWelcomeView() -> HomeWelcomeView {
        return HomeWelcomeView().manualLayoutable()
    }

    func buildActivateMembershipView() -> ActivateMembershipView {
        return ActivateMembershipView().manualLayoutable()
    }
}
