//
//  HomePageViewControllersBuilder.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 13/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

protocol HomePageViewControllersBuilderProtocol {
    func buildAllActivationsModule(router: Router, delegate: HomeViewControllerProtocol) -> UIViewController
    func buildMixersModule(router: Router, delegate: HomeViewControllerProtocol) -> UIViewController
    func buildHappeningsModule(router: Router, delegate: HomeViewControllerProtocol) -> UIViewController
    func buildRSVPSModule(router: Router, delegate: HomeViewControllerProtocol) -> UIViewController
}

final class HomePageViewControllersBuilder: HomePageViewControllersBuilderProtocol {
    func buildAllActivationsModule(router: Router, delegate: HomeViewControllerProtocol) -> UIViewController {
        let viewController = ActivationsViewController(router: router)
        viewController.viewModel = ActivationsViewModel()
        viewController.delegate = delegate

        return viewController
    }

    func buildMixersModule(router: Router, delegate: HomeViewControllerProtocol) -> UIViewController {
        let viewController = MixersViewController(router: router)
        viewController.viewModel = MixersViewModel()
        viewController.delegate = delegate

        return viewController
    }

    func buildHappeningsModule(router: Router, delegate: HomeViewControllerProtocol) -> UIViewController {
        let viewController = HappeningsViewController(router: router)
        viewController.viewModel = HappeningsViewModel()
        viewController.delegate = delegate

        return viewController
    }

    func buildRSVPSModule(router: Router, delegate: HomeViewControllerProtocol) -> UIViewController {
        let viewController = RsvpsViewController(router: router)
        viewController.viewModel = RsvpsViewModel()
        viewController.delegate = delegate

        return viewController
    }
}
