//
//  PageViewControllerDataSource.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 12/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

class PageViewControllerDataSource: NSObject, UIPageViewControllerDataSource {
    private let viewControllers: [UIViewController]

    init(viewControllers: [UIViewController]) {
        self.viewControllers = viewControllers
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = viewControllers.firstIndex(of: viewController) else { return nil }
        var previousIndex = currentIndex
        if currentIndex == 0 { previousIndex = viewControllers.count }
        previousIndex = previousIndex - 1

        return viewControllers[previousIndex]
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = viewControllers.firstIndex(of: viewController) else { return nil }
        var nextIndex = currentIndex + 1
        if nextIndex == viewControllers.count { nextIndex = 0 }

        return viewControllers[nextIndex]
    }
}
