//
//  HomePageViewControllerDelegate.swift
//  ParlorSocialClub
//
//  Created by Benjamin Acquah on 12/05/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import UIKit

protocol HomePageViewControllerDelegateDelegate: class {
    func index(of viewController: UIViewController) -> Int?
    func currentPageChanged(to index: Int)
}

protocol HomePageViewControllerDelegateProtocol: UIPageViewControllerDelegate {
    var delegate: HomePageViewControllerDelegateDelegate? { get set }
}

final class HomePageViewControllerDelegate: NSObject, HomePageViewControllerDelegateProtocol {
    weak var delegate: HomePageViewControllerDelegateDelegate?

    func pageViewController(
        _ pageViewController: UIPageViewController,
        didFinishAnimating finished: Bool,
        previousViewControllers: [UIViewController],
        transitionCompleted completed: Bool
    ) {
        guard let currenViewController = pageViewController.viewControllers?.first,
            let currentPageIndex = delegate?.index(of: currenViewController) else { return }

        delegate?.currentPageChanged(to: currentPageIndex)
    }
}
