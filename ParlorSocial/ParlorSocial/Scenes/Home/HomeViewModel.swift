//
//  HomeViewModel.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 24/04/2019.
//

import Foundation
import RxSwift
import RxCocoa
import UserNotifications

protocol HomeLogic: BaseViewModelLogic, HasActivateMembershipViewLogic {
    var screenData: Observable<WelcomeScreenData> { get }
    var destinationObs: Observable<HomeDestination> { get }
    var profileObs: Observable<Profile> { get }

    func fetchScreenData()
    func showEnableNotificationsSettingsIfNeeded(when viewType: EnableNotificationsReminderViewType)
    func didTapActivateMembershipMoreButton()
}

class HomeViewModel: BaseViewModel {
    var activateMembershipViewLogic: ActivateMembershipViewLogic?
    var setupActivateMembershipViewSubject: PublishSubject<ActivateMembershipViewModel> = PublishSubject<ActivateMembershipViewModel>()

    let analyticEventReferencedId: Int? = nil

    private let destinationSubject = PublishSubject<HomeDestination>()
    private let welcomeDataSubject: ReplaySubject<WelcomeScreenData> = ReplaySubject.create(bufferSize: 1)
    private let profileRepository = ProfileRepository.shared
    private let notificationService = GlobalNotificationsService.shared
    private let notificationsReminderProvider = EnableNotificationsReminderViewProvider()
}

// MARK: Private logic
private extension HomeViewModel {
    private func updateWelcomeMessage(from data: [WelcomeScreenData]) {
        if let currentData = data.first(where: { welcomeData in
            welcomeData.category == WelcomeCategory.currentTimeCategory
        }) {
            welcomeDataSubject.onNext(currentData)
        }
    }
    
    private func handleScreenData(response: WelcomeScreensResposne) {
        switch response {
        case .success(let data):
            updateWelcomeMessage(from: data.data)
        case .failure(let error):
            errorSubject.onNext(error)
        }
    }
    
    private func setNotificationsEnabledIfAllowed() {
        notificationService.arePermissionsAuthorized { isGranted in
            guard isGranted else { return }
            self.notificationService.setEnabledPushNotifications()
        }
    }
}

// MARK: Interface logic methods
extension HomeViewModel: HomeLogic {
    var profileObs: Observable<Profile> {
        return profileRepository.currentProfileObs
    }
    
    var screenData: Observable<WelcomeScreenData> {
        return welcomeDataSubject.asObservable()
    }
    
    var destinationObs: Observable<HomeDestination> {
        return destinationSubject.asObservable()
    }
    
    func fetchScreenData() {
        profileRepository.getWelcomeScreens()
            .subscribe(onNext: { [weak self] response in
                self?.handleScreenData(response: response)
                self?.setNotificationsEnabledIfAllowed()
            }).disposed(by: disposeBag)
    }
    
    func showEnableNotificationsSettingsIfNeeded(when viewType: EnableNotificationsReminderViewType) {
        guard Config.wasOnboardingShown(for: .home) else { return }
        notificationsReminderProvider.showNotificationsReminder(when: viewType) { [weak self] shouldShow, notificationsSettingsChangeSource in
            guard shouldShow else { return }
            DispatchQueue.main.async { [weak self] in
                self?.destinationSubject.onNext(.notificationsReminder(notificationsSettingsChangeSource))
            }
        }
    }

    func didTapActivateMembershipMoreButton() {
        activateMembershipViewLogic?.buttonClicked()
    }

    func initActivateMembershipViewLogic() {
        activateMembershipViewLogic = ActivateMembershipViewLogic(profileRepository: ProfileRepository.shared)

        activateMembershipViewLogic?.setupViewRelay
            .ignoreNil()
            .bind(to: setupActivateMembershipViewSubject)
            .disposed(by: disposeBag)

        activateMembershipViewLogic?.buttonClickedSubject
            .subscribe(onNext: { [unowned self] _ in
                self.destinationSubject.onNext(.membershipPlans)
            }).disposed(by: disposeBag)
    }
}
