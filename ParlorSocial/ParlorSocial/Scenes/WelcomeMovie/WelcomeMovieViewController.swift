//
//  WelcomeMovieViewController.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 04/07/2019.
//  Copyright (c) 2019 KISSdigital. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import RxSwift

class WelcomeMovieViewController: AVPlayerViewController {

    // MARK: - Properties
    private let movieFileName = "WelcomeMovie"
    private let movieFileType = "mp4"
    private var avPlayer: AVPlayer?
    private let disposeBag = DisposeBag()
    lazy var router = Router(viewController: self)

    // MARK: - Initialization
    init() {
        super.init(nibName: nil, bundle: nil)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("WelcomeMovieViewController: init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAVPlayerViewController()
        setupNotificationCenter()
    }

}

// MARK: - Private methods
extension WelcomeMovieViewController {
    
    private func setupView() {
        view.backgroundColor = .white
    }

    private func setupAVPlayerViewController() {
        guard let urlMainPath = Bundle.main.path(forResource: movieFileName, ofType: movieFileType) else { return }
        let movieUrl = URL(fileURLWithPath: urlMainPath)
        avPlayer = AVPlayer(url: movieUrl)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(playerDidFinishPlaying),
            name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
            object: player?.currentItem)
        showsPlaybackControls = false
        player = avPlayer
        player?.seek(to: CMTimeMake(value: 12, timescale: 10))
        player?.isMuted = true
        player?.play()
    }
    
    private func setupNotificationCenter() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToForeground), name: UIApplication.didBecomeActiveNotification, object: nil)
        
        notificationCenter.addObserver(self, selector: #selector(appMovedToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    @objc private func playerDidFinishPlaying() {
        if KeychainWorker().userToken == nil {
            handleDestination(destination: .login)
            return
        }
        if Config.userProfile == nil {
            ProfileRepository.shared.getMyProfileData()
                .subscribe(onNext: { [unowned self] _ in
                    self.navigateToNextScreenForLoggedInUser()
                })
            .disposed(by: disposeBag)
        } else {
            self.navigateToNextScreenForLoggedInUser()
        }
    }
    
    @objc private func appMovedToForeground() {
        player?.play()
    }
    
    private func navigateToNextScreenForLoggedInUser() {
        let initialSceneDestination: WelcomeMovieDestination = {
            if Config.shouldInterceptWithPaymentScreen {
                return .payment
            } else if Config.shouldInterceptWithMembershipPlansScreen {
                return .membershipPlans
            } else if Config.shouldInterceptWithUserNotApprovedScreen {
                return .notApproved
            } else {
                return .appTabBar
            }
        }()
        handleDestination(destination: initialSceneDestination)
    }
    
    private func handleDestination(destination: WelcomeMovieDestination) {
        switch destination {
        case .login, .appTabBar, .payment, .membershipPlans, .notApproved:
            router.replace(with: destination)
        }
    }
}
