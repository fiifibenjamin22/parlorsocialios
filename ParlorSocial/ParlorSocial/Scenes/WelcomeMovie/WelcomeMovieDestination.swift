//
//  WelcomeMovieDestination.swift
//  ParlorSocial
//
//  Created by Benjamin Acquah on 04/07/2019.
//

import UIKit

enum WelcomeMovieDestination {
    case login
    case appTabBar
    case payment
    case membershipPlans
    case notApproved
}

extension WelcomeMovieDestination: Destination {
    
    var viewController: UIViewController {
        switch self {
        case .login:
            return LoginViewController().embedInNavigationController()
        case .appTabBar:
            return AppTabBarController().embedInNavigationController()
        case .payment:
            return InitialViewControllerUtility.getViewControllerWithLoginAsRoot(viewController: PremiumPaymentViewController())
        case .membershipPlans:
            return InitialViewControllerUtility.getViewControllerWithLoginAsRoot(viewController: MembershipPlansViewController())
        case .notApproved:
            return UserNotApprovedViewController()
        }
    }

}
