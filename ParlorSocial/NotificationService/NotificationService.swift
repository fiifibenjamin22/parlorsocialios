//
//  NotificationService.swift
//  NotificationService
//
//  Created by Benjamin Acquah on 07/01/2020.
//

import UserNotifications

class NotificationService: UNNotificationServiceExtension {

    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?

    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        guard let bestAttemptContent = bestAttemptContent,
            let urlString = request.content.userInfo["image_url"] as? String,
            let fileFormat = urlString.split(separator: ".").last else { return }
        
        bestAttemptContent.title = "\(bestAttemptContent.title)"
        let fileName = "\(ProcessInfo.processInfo.globallyUniqueString).\(fileFormat)"
        
        guard let fileUrl = URL(string: urlString),
            let imageData = NSData(contentsOf: fileUrl),
            let attachment = UNNotificationAttachment.saveNotificationImageToDisk(
                fileIdentifier: fileName, data: imageData, options: nil) else {
                    print("Error with notification attachment.")
                    contentHandler(bestAttemptContent)
                    return
        }
        
        bestAttemptContent.attachments = [attachment]
        contentHandler(bestAttemptContent)
    }
    
    override func serviceExtensionTimeWillExpire() {
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }

}
