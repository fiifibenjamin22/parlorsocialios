//
//  EnableNotificationsReminderProfileRepositoryMock.swift
//  ParlorSocialTests
//
//  Created by Benjamin Acquah on 10/03/2020.
//

import Foundation
import RxSwift
@testable import Debug_ParlorSocialClub

class EnableNotificationsReminderProfileRepositoryMock: ProfileRepositoryProtocol {
    // MARK: - Properties
    var enableNotificationsSettingsRemindModel: EnableNotificationsSettingsRemindModel?
    
    // MARK: - Overriden methods
    func getEnableNotificationsSettingsRemindModel(_ completionHandler: @escaping (EnableNotificationsSettingsRemindModel) -> Void) {
        guard let enableNotificationsSettingsRemindModel = enableNotificationsSettingsRemindModel else { fatalError("enableNotificationsSettingsRemindModel should not be nil") }
        completionHandler(enableNotificationsSettingsRemindModel)
    }
    
    var knownCircles: Observable<[Circle]> { return Observable.empty() }
    var knownInterests: Observable<[InterestGroup]> { return Observable.empty() }
    var knownCardsObs: Observable<[CardData]> { return Observable.empty() }
    var currentProfileObs: Observable<Profile> { return Observable.empty() }
    var currentColorThemeObs: Observable<PremiumColorManager.Theme> { return Observable.empty() }
    func subscribeToPremium(usingCardWithId id: Int, planId: Int) -> MessageApiResponse { return Observable.empty() }
    func addCard(withData data: CardUploadData) -> Observable<SingleCardResponse> { return Observable.empty() }
    func editCard(withData data: EditCardRequest, cardId: Int) -> Observable<SingleCardResponse> { return Observable.empty() }
    func removeCard(withId id: Int) -> Observable<EmptyResponse> { return Observable.empty() }
    func getCardDetails(withId id: Int) -> Observable<CardDetailsResponse> { return Observable.empty() }
    func changeDefaultCard(to newCardId: Int) -> Observable<SingleCardResponse> { return Observable.empty() }
    func changePassword(using data: ChangePasswordRequest) -> Observable<ChangePasswordResponse> { return Observable.empty() }
    func changeEmail(using data: ChangeEmailRequest) -> MessageApiResponse { return Observable.empty() }
    func createNewPassword(forUserWithEmail email: String) -> Observable<ApiResponse<MessageResponse>> { return Observable.empty() }
    func resetPassword(using requestData: ResetPasswordRequest) -> Observable<ApiResponse<MessageResponse>> { return Observable.empty() }
    func getWelcomeScreens() -> Observable<WelcomeScreensResposne> { return Observable.empty() }
    func getUserCards() -> Observable<CardsResponse> { return Observable.empty() }
    func getMyProfileData() -> Observable<ProfileResponse> { return Observable.empty() }
    func updateProfile(withData data: ProfileUpdateModel) -> Observable<ProfileResponse> { return Observable.empty() }
    func uploadNewPhoto(_ image: UIImage) -> Observable<ProfileResponse> { return Observable.empty() }
    func getMyCircles() -> Observable<CirclesResposne> { return Observable.empty() }
    func removeMyCircle(withId id: Int) -> Observable<EmptyResponse> { return Observable.empty() }
    func getMyInterestGroups() -> Observable<MyInterestGroupsResponse> { return Observable.empty() }
    func updateKnownInterestsGroups(basedOn data: [InterestGroup]) {}
    func getSettingsInfo() -> Observable<SettingsResponse> { return Observable.empty() }
    func getPlanInfo() -> Observable<PlanInfoResponse> { return Observable.empty() }
    func inviteFriend(with data: ReferFriendData) -> MessageApiResponse { return Observable.empty() }
    func requestPremium() -> Observable<ProfileMessageResponse> { return Observable.empty() }
    func getMembershipPlans(using model: MembershipPlans.Get.Request) -> Observable<MembershipPlansResponse> { return Observable.empty() }
    func cancelPremiumMembershipRequest() -> Observable<ProfileMessageResponse> { return Observable.empty() }
    func getPremiumPlanInfo() -> Observable<PlanInfoResponse> { return Observable.empty() }
    func addCard(usingToken token: CardTokenRequest) -> Observable<SingleCardResponse> { return Observable.empty() }
}
