//
//  EnableNotificationsSettingsRemindModelTestExtension.swift
//  ParlorSocialTests
//
//  Created by Benjamin Acquah on 11/03/2020.
//

import Foundation
import UserNotifications
@testable import Debug_ParlorSocialClub

extension EnableNotificationsSettingsRemindModel {
    static func createDefaultWith(
        enableNotificationsRemindLevel: EnableNotificationsSettingsRemindLevel = .zero,
        rsvpsCount: Int = 0,
        lastRejectNotificationsPermissionsDate: Date? = nil,
        appEntryCount: Int = 0,
        notificationsPermissions: UNAuthorizationStatus = .denied) -> EnableNotificationsSettingsRemindModel {
        return EnableNotificationsSettingsRemindModel(
            enableNotificationsRemindLevel: enableNotificationsRemindLevel,
            rsvpsCount: rsvpsCount,
            lastRejectNotificationsPermissionsDate: lastRejectNotificationsPermissionsDate,
            appEntryCount: appEntryCount,
            notificationsPermissions: notificationsPermissions
        )
    }
}
