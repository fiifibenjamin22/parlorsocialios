//
//  EnableNotificationsReminderViewProviderTests.swift
//  ParlorSocialTests
//
//  Created by Benjamin Acquah on 10/03/2020.
//

import XCTest
@testable import Debug_ParlorSocialClub

class EnableNotificationsReminderViewProviderTests: XCTestCase {
    
    // MARK: - Properties
    var sut: EnableNotificationsReminderViewProvider!
    var mockProfileRepository: EnableNotificationsReminderProfileRepositoryMock!
    var showNotificationsReminderExpectation: XCTestExpectation!

    override func setUp() {
        super.setUp()
        mockProfileRepository = EnableNotificationsReminderProfileRepositoryMock()
        sut = EnableNotificationsReminderViewProvider(profileRepository: mockProfileRepository)
        showNotificationsReminderExpectation = self.expectation(description: "ShowNotificationsReminder")
    }

    override func tearDown() {
        sut = nil
        mockProfileRepository = nil
        showNotificationsReminderExpectation = nil
        super.tearDown()
    }

    // MARK: - ShouldShowReminderAfterLogin tests
    func testShowReminderAfterLogin_reminderLevelIsZeroAndPermissionsAreDenied_returnTrue() {
        mockProfileRepository.enableNotificationsSettingsRemindModel = EnableNotificationsSettingsRemindModel.createDefaultWith(
            enableNotificationsRemindLevel: .zero,
            notificationsPermissions: .denied)
        
        sut.showNotificationsReminder(when: .beforeDashboard) { [unowned self] isShown, _ in
            self.showNotificationsReminderExpectation.fulfill()
            XCTAssertTrue(isShown)
        }
        waitForExpectations(timeout: 2, handler: nil)
    }
    
    func testShowReminderAfterLogin_reminderLevelIsZeroAndPermissionsAreNotDetermined_returnTrue() {
        mockProfileRepository.enableNotificationsSettingsRemindModel = EnableNotificationsSettingsRemindModel.createDefaultWith(
            enableNotificationsRemindLevel: .zero,
            notificationsPermissions: .notDetermined)
        
        sut.showNotificationsReminder(when: .beforeDashboard) { [unowned self] isShown, _ in
            self.showNotificationsReminderExpectation.fulfill()
            XCTAssertTrue(isShown)
        }
        waitForExpectations(timeout: 2, handler: nil)
    }
    
    func testShowReminderAfterLogin_reminderLevelIsZeroAndPermissionsAreAuthorized_returnFalse() {
        mockProfileRepository.enableNotificationsSettingsRemindModel = EnableNotificationsSettingsRemindModel.createDefaultWith(
            enableNotificationsRemindLevel: .zero,
            notificationsPermissions: .authorized)
        
        sut.showNotificationsReminder(when: .beforeDashboard) { [unowned self] isShown, _ in
            self.showNotificationsReminderExpectation.fulfill()
            XCTAssertFalse(isShown)
        }
        waitForExpectations(timeout: 2, handler: nil)
    }
    
    func testShowReminderAfterLogin_reminderLevelIsZeroAndPermissionsAreProvisional_returnTrue() {
        mockProfileRepository.enableNotificationsSettingsRemindModel = EnableNotificationsSettingsRemindModel.createDefaultWith(
            enableNotificationsRemindLevel: .zero,
            notificationsPermissions: .provisional)
        
        sut.showNotificationsReminder(when: .beforeDashboard) { [unowned self] isShown, _ in
            self.showNotificationsReminderExpectation.fulfill()
            XCTAssertTrue(isShown)
        }
        waitForExpectations(timeout: 2, handler: nil)
    }
    
    func testShowReminderAfterLogin_reminderLevelIsFirstAndPermissionsAreDenied_returnFalse() {
        mockProfileRepository.enableNotificationsSettingsRemindModel = EnableNotificationsSettingsRemindModel.createDefaultWith(
            enableNotificationsRemindLevel: .first,
            notificationsPermissions: .denied)
        
        sut.showNotificationsReminder(when: .beforeDashboard) { [unowned self] isShown, _ in
            self.showNotificationsReminderExpectation.fulfill()
            XCTAssertFalse(isShown)
        }
        waitForExpectations(timeout: 2, handler: nil)
    }
    
    func testShowReminderAfterLogin_reminderLevelIsSecondAndPermissionsAreDenied_returnFalse() {
        mockProfileRepository.enableNotificationsSettingsRemindModel = EnableNotificationsSettingsRemindModel.createDefaultWith(
            enableNotificationsRemindLevel: .second,
            notificationsPermissions: .denied)
        
        sut.showNotificationsReminder(when: .beforeDashboard) { [unowned self] isShown, _ in
            self.showNotificationsReminderExpectation.fulfill()
            XCTAssertFalse(isShown)
        }
        waitForExpectations(timeout: 2, handler: nil)
    }
    
    func testShowReminderAfterLogin_reminderLevelIsThirdAndPermissionsAreDenied_returnFalse() {
        mockProfileRepository.enableNotificationsSettingsRemindModel = EnableNotificationsSettingsRemindModel.createDefaultWith(
            enableNotificationsRemindLevel: .third,
            notificationsPermissions: .denied)
        
        sut.showNotificationsReminder(when: .beforeDashboard) { [unowned self] isShown, _ in
            self.showNotificationsReminderExpectation.fulfill()
            XCTAssertFalse(isShown)
        }
        waitForExpectations(timeout: 2, handler: nil)
    }
    
    // MARK: - ShowNotificationsReminderAfterFirstRsvp tests
    func testShowReminderAfterFirstRsvp_reminderLevelIsFirstAndRsvpsCountEqualOneAndLastRejectionDateIsYesterday_returnTrue() {
        mockProfileRepository.enableNotificationsSettingsRemindModel = EnableNotificationsSettingsRemindModel.createDefaultWith(
            enableNotificationsRemindLevel: .first,
            rsvpsCount: 1,
            lastRejectNotificationsPermissionsDate: Calendar.current.date(byAdding: .day, value: -1, to: Date())
        )
        
        sut.showNotificationsReminder(when: .afterFirstRsvp) { [unowned self] isShown, _ in
            self.showNotificationsReminderExpectation.fulfill()
            XCTAssertTrue(isShown)
        }
        waitForExpectations(timeout: 2, handler: nil)
    }
    
    func testShowReminderAfterFirstRsvp_reminderLevelIsFirstAndRsvpsCountLessThanOneAndLastRejectionDateIsYesterday_returnFalse() {
        mockProfileRepository.enableNotificationsSettingsRemindModel = EnableNotificationsSettingsRemindModel.createDefaultWith(
            enableNotificationsRemindLevel: .first,
            rsvpsCount: 0,
            lastRejectNotificationsPermissionsDate: Calendar.current.date(byAdding: .day, value: -1, to: Date())
        )
        
        sut.showNotificationsReminder(when: .afterFirstRsvp) { [unowned self] isShown, _ in
            self.showNotificationsReminderExpectation.fulfill()
            XCTAssertFalse(isShown)
        }
        waitForExpectations(timeout: 2, handler: nil)
    }
    
    func testShowReminderAfterFirstRsvp_reminderLevelIsFirstAndLastRejectionDateIsToday_returnFalse() {
        mockProfileRepository.enableNotificationsSettingsRemindModel = EnableNotificationsSettingsRemindModel.createDefaultWith(
            enableNotificationsRemindLevel: .first,
            rsvpsCount: 2,
            lastRejectNotificationsPermissionsDate: Date()
        )
        
        sut.showNotificationsReminder(when: .afterFirstRsvp) { [unowned self] isShown, _ in
            self.showNotificationsReminderExpectation.fulfill()
            XCTAssertFalse(isShown)
        }
        waitForExpectations(timeout: 2, handler: nil)
    }
    
    func testShowReminderAfterFirstRsvp_reminderLevelIsFirstAndAppEntryCountEqualOne_returnFalse() {
        mockProfileRepository.enableNotificationsSettingsRemindModel = EnableNotificationsSettingsRemindModel.createDefaultWith(
            enableNotificationsRemindLevel: .first,
            rsvpsCount: 2,
            appEntryCount: 1)
        
        sut.showNotificationsReminder(when: .afterFirstRsvp) { [unowned self] isShown, _ in
            self.showNotificationsReminderExpectation.fulfill()
            XCTAssertFalse(isShown)
        }
        waitForExpectations(timeout: 2, handler: nil)
    }
    
    func testShowReminderAfterFirstRsvp_reminderLevelIsZero_returnFalse() {
        mockProfileRepository.enableNotificationsSettingsRemindModel = EnableNotificationsSettingsRemindModel.createDefaultWith(
            enableNotificationsRemindLevel: .zero)
        
        sut.showNotificationsReminder(when: .afterFirstRsvp) { [unowned self] isShown, _ in
            self.showNotificationsReminderExpectation.fulfill()
            XCTAssertFalse(isShown)
        }
        waitForExpectations(timeout: 2, handler: nil)
    }

    func testShowReminderAfterFirstRsvp_reminderLevelIsSecond_returnFalse() {
        mockProfileRepository.enableNotificationsSettingsRemindModel = EnableNotificationsSettingsRemindModel.createDefaultWith(
            enableNotificationsRemindLevel: .second)
        
        sut.showNotificationsReminder(when: .afterFirstRsvp) { [unowned self] isShown, _ in
            self.showNotificationsReminderExpectation.fulfill()
            XCTAssertFalse(isShown)
        }
        waitForExpectations(timeout: 2, handler: nil)
    }
    
    func testShowReminderAfterFirstRsvp_reminderLevelIsThird_returnFalse() {
        mockProfileRepository.enableNotificationsSettingsRemindModel = EnableNotificationsSettingsRemindModel.createDefaultWith(
            enableNotificationsRemindLevel: .third)
        
        sut.showNotificationsReminder(when: .afterFirstRsvp) { [unowned self] isShown, _ in
            self.showNotificationsReminderExpectation.fulfill()
            XCTAssertFalse(isShown)
        }
        waitForExpectations(timeout: 2, handler: nil)
    }
    
    // MARK: - ShowNotificationsReminderOnDashboard tests
    func testShowReminderOnDashboard_reminderLevelIsZero_returnFalse() {
        mockProfileRepository.enableNotificationsSettingsRemindModel = EnableNotificationsSettingsRemindModel.createDefaultWith(
            enableNotificationsRemindLevel: .zero)
        
        sut.showNotificationsReminder(when: .onDashboard) { [unowned self] isShown, _ in
            self.showNotificationsReminderExpectation.fulfill()
            XCTAssertFalse(isShown)
        }
        waitForExpectations(timeout: 2, handler: nil)
    }
    
    func testShowReminderOnDashboard_reminderLevelIsFirstANDRejectDateLessThanTwoWeeksANDAppEntryCountLessThanFive_returnFalse() {
        let currentDate = Date()
        var dayComponent = DateComponents()
        dayComponent.day = -13
        let calendar = Calendar.current
        guard let lessThanTwoWeeksBeforeCurrent = calendar.date(byAdding: dayComponent, to: currentDate) else {
            XCTFail("TestShowReminderOnDashboard: date is null")
            return
        }
        
        mockProfileRepository.enableNotificationsSettingsRemindModel = EnableNotificationsSettingsRemindModel.createDefaultWith(
            enableNotificationsRemindLevel: .first,
            lastRejectNotificationsPermissionsDate: lessThanTwoWeeksBeforeCurrent,
            appEntryCount: 4)
        
        sut.showNotificationsReminder(when: .onDashboard) { [unowned self] isShown, _ in
            self.showNotificationsReminderExpectation.fulfill()
            XCTAssertFalse(isShown)
        }
        waitForExpectations(timeout: 2, handler: nil)
    }
    
    func testShowReminderOnDashboard_reminderLevelIsFirstANDRejectDateEqualToTwoWeeksANDAppEntryCountLessThanFive_returnTrue() {
        let currentDate = Date()
        var dayComponent = DateComponents()
        dayComponent.day = -14
        let calendar = Calendar.current
        guard let twoWeeksBeforeCurrent = calendar.date(byAdding: dayComponent, to: currentDate) else {
            XCTFail("TestShowReminderOnDashboard: date is null")
            return
        }

        mockProfileRepository.enableNotificationsSettingsRemindModel = EnableNotificationsSettingsRemindModel.createDefaultWith(
            enableNotificationsRemindLevel: .first,
            lastRejectNotificationsPermissionsDate: twoWeeksBeforeCurrent,
            appEntryCount: 4)

        sut.showNotificationsReminder(when: .onDashboard) { [unowned self] isShown, _ in
            self.showNotificationsReminderExpectation.fulfill()
            XCTAssertTrue(isShown)
        }
        waitForExpectations(timeout: 2, handler: nil)
    }
    
    func testShowReminderOnDashboard_reminderLevelIsFirstANDRejectDateGreatherThanToTwoWeeksANDAppEntryCountLessThanFive_returnTrue() {
        let currentDate = Date()
        var dayComponent = DateComponents()
        dayComponent.day = -15
        let calendar = Calendar.current
        guard let greatherThanTwoWeeksBeforeCurrent = calendar.date(byAdding: dayComponent, to: currentDate) else {
            XCTFail("TestShowReminderOnDashboard: date is null")
            return
        }

        mockProfileRepository.enableNotificationsSettingsRemindModel = EnableNotificationsSettingsRemindModel.createDefaultWith(
            enableNotificationsRemindLevel: .first,
            lastRejectNotificationsPermissionsDate: greatherThanTwoWeeksBeforeCurrent,
            appEntryCount: 4)

        sut.showNotificationsReminder(when: .onDashboard) { [unowned self] isShown, _ in
            self.showNotificationsReminderExpectation.fulfill()
            XCTAssertTrue(isShown)
        }
        waitForExpectations(timeout: 2, handler: nil)
    }
    
    func testShowReminderOnDashboard_reminderLevelIsFirstANDRejectDateLessThanTwoWeeksANDAppEntryCountEqualToFive_returnTrue() {
        let currentDate = Date()
        var dayComponent = DateComponents()
        dayComponent.day = -13
        let calendar = Calendar.current
        guard let lessThanTwoWeeksBeforeCurrent = calendar.date(byAdding: dayComponent, to: currentDate) else {
            XCTFail("TestShowReminderOnDashboard: date is null")
            return
        }
        
        mockProfileRepository.enableNotificationsSettingsRemindModel = EnableNotificationsSettingsRemindModel.createDefaultWith(
            enableNotificationsRemindLevel: .first,
            lastRejectNotificationsPermissionsDate: lessThanTwoWeeksBeforeCurrent,
            appEntryCount: 5)
        
        sut.showNotificationsReminder(when: .onDashboard) { [unowned self] isShown, _ in
            self.showNotificationsReminderExpectation.fulfill()
            XCTAssertTrue(isShown)
        }
        waitForExpectations(timeout: 2, handler: nil)
    }
    
    func testShowReminderOnDashboard_reminderLevelIsFirstANDRejectDateEqualToTwoWeeksANDAppEntryCountEqualToFive_returnTrue() {
        let currentDate = Date()
        var dayComponent = DateComponents()
        dayComponent.day = -14
        let calendar = Calendar.current
        guard let twoWeeksBeforeCurrent = calendar.date(byAdding: dayComponent, to: currentDate) else {
            XCTFail("TestShowReminderOnDashboard: date is null")
            return
        }

        mockProfileRepository.enableNotificationsSettingsRemindModel = EnableNotificationsSettingsRemindModel.createDefaultWith(
            enableNotificationsRemindLevel: .first,
            lastRejectNotificationsPermissionsDate: twoWeeksBeforeCurrent,
            appEntryCount: 5)

        sut.showNotificationsReminder(when: .onDashboard) { [unowned self] isShown, _ in
            self.showNotificationsReminderExpectation.fulfill()
            XCTAssertTrue(isShown)
        }
        waitForExpectations(timeout: 2, handler: nil)
    }
    
    func testShowReminderOnDashboard_reminderLevelIsFirstANDRejectDateGreatherThanToTwoWeeksANDAppEntryCountEqualToFive_returnTrue() {
        let currentDate = Date()
        var dayComponent = DateComponents()
        dayComponent.day = -15
        let calendar = Calendar.current
        guard let greatherThanTwoWeeksBeforeCurrent = calendar.date(byAdding: dayComponent, to: currentDate) else {
            XCTFail("TestShowReminderOnDashboard: date is null")
            return
        }

        mockProfileRepository.enableNotificationsSettingsRemindModel = EnableNotificationsSettingsRemindModel.createDefaultWith(
            enableNotificationsRemindLevel: .first,
            lastRejectNotificationsPermissionsDate: greatherThanTwoWeeksBeforeCurrent,
            appEntryCount: 5)

        sut.showNotificationsReminder(when: .onDashboard) { [unowned self] isShown, _ in
            self.showNotificationsReminderExpectation.fulfill()
            XCTAssertTrue(isShown)
        }
        waitForExpectations(timeout: 2, handler: nil)
    }
    
    func testShowReminderOnDashboard_reminderLevelIsFirstANDRejectDateLessThanTwoWeeksANDAppEntryCountGreatherThanFive_returnTrue() {
        let currentDate = Date()
        var dayComponent = DateComponents()
        dayComponent.day = -13
        let calendar = Calendar.current
        guard let lessThanTwoWeeksBeforeCurrent = calendar.date(byAdding: dayComponent, to: currentDate) else {
            XCTFail("TestShowReminderOnDashboard: date is null")
            return
        }
        
        mockProfileRepository.enableNotificationsSettingsRemindModel = EnableNotificationsSettingsRemindModel.createDefaultWith(
            enableNotificationsRemindLevel: .first,
            lastRejectNotificationsPermissionsDate: lessThanTwoWeeksBeforeCurrent,
            appEntryCount: 6)
        
        sut.showNotificationsReminder(when: .onDashboard) { [unowned self] isShown, _ in
            self.showNotificationsReminderExpectation.fulfill()
            XCTAssertTrue(isShown)
        }
        waitForExpectations(timeout: 2, handler: nil)
    }
    
    func testShowReminderOnDashboard_reminderLevelIsFirstANDRejectDateEqualToTwoWeeksANDAppEntryCountGreatherThanFive_returnTrue() {
        let currentDate = Date()
        var dayComponent = DateComponents()
        dayComponent.day = -14
        let calendar = Calendar.current
        guard let twoWeeksBeforeCurrent = calendar.date(byAdding: dayComponent, to: currentDate) else {
            XCTFail("TestShowReminderOnDashboard: date is null")
            return
        }

        mockProfileRepository.enableNotificationsSettingsRemindModel = EnableNotificationsSettingsRemindModel.createDefaultWith(
            enableNotificationsRemindLevel: .first,
            lastRejectNotificationsPermissionsDate: twoWeeksBeforeCurrent,
            appEntryCount: 6)

        sut.showNotificationsReminder(when: .onDashboard) { [unowned self] isShown, _ in
            self.showNotificationsReminderExpectation.fulfill()
            XCTAssertTrue(isShown)
        }
        waitForExpectations(timeout: 2, handler: nil)
    }
    
    func testShowReminderOnDashboard_reminderLevelIsFirstANDRejectDateGreatherThanToTwoWeeksANDAppEntryCountGreatherThanFive_returnTrue() {
        let currentDate = Date()
        var dayComponent = DateComponents()
        dayComponent.day = -15
        let calendar = Calendar.current
        guard let greatherThanTwoWeeksBeforeCurrent = calendar.date(byAdding: dayComponent, to: currentDate) else {
            XCTFail("TestShowReminderOnDashboard: date is null")
            return
        }

        mockProfileRepository.enableNotificationsSettingsRemindModel = EnableNotificationsSettingsRemindModel.createDefaultWith(
            enableNotificationsRemindLevel: .first,
            lastRejectNotificationsPermissionsDate: greatherThanTwoWeeksBeforeCurrent,
            appEntryCount: 6)

        sut.showNotificationsReminder(when: .onDashboard) { [unowned self] isShown, _ in
            self.showNotificationsReminderExpectation.fulfill()
            XCTAssertTrue(isShown)
        }
        waitForExpectations(timeout: 2, handler: nil)
    }
}
