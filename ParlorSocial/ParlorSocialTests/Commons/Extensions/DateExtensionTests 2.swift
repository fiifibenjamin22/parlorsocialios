//
//  DateExtensionTests.swift
//  ParlorSocialTests
//
//  Created by Benjamin Acquah on 23/04/2020.
//  Copyright © 2020 KISSdigital. All rights reserved.
//

import XCTest
@testable import Debug_ParlorSocialClub

final class DateProviderTests: XCTestCase {

    func testToUpcomingActivationDateReturnsCorrectDateAndAbbreviationForYesterdayDate() {
        let yestarday = Calendar.current.date(byAdding: .day, value: -1, to: Date())!
        let noonDate = Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: yestarday)!
        let result = "12:00 PM"

        XCTAssertEqual(noonDate.toUpcomingActivationDate(), result)
    }

    func testToPrivateActivationStartDateFormatReturnsCorrectlyFormattedDateWhenDateIsNotApproximatedAndCurrentDateIsNotEarlyMorning() {
        let date = self.date(from: "2020-01-01 12:00")
        let currentDate = self.currentDate(isEarlyMorning: false)
        let result = "Wednesday, Jan 1st 12:00pm"

        XCTAssertEqual(date.toPrivateActivationStartDateFormat(isApproximatedStartTime: false, currentDate: currentDate), result)
    }
    
    func testToPrivateActivationStartDateFormatReturnsCorrectlyFormattedDateWhenDateIsNotApproximatedAndCurrentDateIsEarlyMorning() {
        let date = self.date(from: "2020-01-01 12:00")
        let currentDate = self.currentDate(isEarlyMorning: true)
        let result = "Wednesday, Jan 1st 12:00pm"

        XCTAssertEqual(date.toPrivateActivationStartDateFormat(isApproximatedStartTime: false, currentDate: currentDate), result)
    }

    func testToPrivateActivationStartDateFormatReturnsTBDLocalizedStringWhenDateIsApproximatedAndCurrentDateIsNotEarlyMorning() {
        let date = self.date(from: "2020-01-01 12:00")
        let currentDate = self.currentDate(isEarlyMorning: false)

        XCTAssertEqual(date.toPrivateActivationStartDateFormat(isApproximatedStartTime: true, currentDate: currentDate), Strings.Activation.dateTBD.localized)
    }
    
    func testToPrivateActivationStartDateFormatReturnsTBDLocalizedStringWhenDateIsApproximatedAndCurrentDateIsEarlyMorning() {
        let date = self.date(from: "2020-01-01 12:00")
        let currentDate = self.currentDate(isEarlyMorning: true)

        XCTAssertEqual(date.toPrivateActivationStartDateFormat(isApproximatedStartTime: true, currentDate: currentDate), Strings.Activation.dateTBD.localized)
    }

    func testToPrivateActivationStartDateFormatReturnsCorrectlyFormattedStringForDateInTheSameDayAsCurrentAndWithTimeBefore6PMAndCurrentDateIsNotEarlyMorning() {
        let date = Calendar.current.date(bySettingHour: 17, minute: 59, second: 59, of: Date())!
        let currentDate = self.currentDate(isEarlyMorning: false)
        let formatter = privateActivationFormatter(for: date)
        let result = "\(Strings.Activation.today.localized) " + formatter.string(from: date)

        XCTAssertEqual(date.toPrivateActivationStartDateFormat(isApproximatedStartTime: false, currentDate: currentDate), result)
    }

    func testToPrivateActivationStartDateFormatReturnsCorrectlyFormattedStringForDateInTheSameDayAsCurrentAndWithTimeAfter6PMAndCurrentDateIsNotEarlyMorning() {
        let date = Calendar.current.date(bySettingHour: 18, minute: 00, second: 00, of: Date())!
        let currentDate = self.currentDate(isEarlyMorning: false)
        let formatter = privateActivationFormatter(for: date)
        let result = "\(Strings.Activation.tonight.localized) " + formatter.string(from: date)

        XCTAssertEqual(date.toPrivateActivationStartDateFormat(isApproximatedStartTime: false, currentDate: currentDate), result)
    }

    func testToPrivateActivationStartDateFormatReturnsCorrectlyFormattedStringForNextDayDateAndCurrentDateIsNotEarlyMorning() {
        let date = Calendar.current.date(byAdding: .day, value: 1, to: Date())!
        let currentDate = self.currentDate(isEarlyMorning: false)
        let formatter = privateActivationFormatter(for: date)
        let result = "\(Strings.Activation.tomorrow.localized) " + formatter.string(from: date)

        XCTAssertEqual(date.toPrivateActivationStartDateFormat(isApproximatedStartTime: false, currentDate: currentDate), result)
    }

    func testToUpcomingRsvpsDateReturnsTodayLocalizedStringForDateInTheSameDayAsCurrentAndWithTimeBefore6PMAndCurrentDateIsNotEarlyMorning() {
        let date = Calendar.current.date(bySettingHour: 17, minute: 59, second: 59, of: Date())!
        let currentDate = self.currentDate(isEarlyMorning: false)

        XCTAssertEqual(date.toUpcomingRsvpsDate(currentDate: currentDate), Strings.MyRsvps.upcomingToday.localized)
    }

    func testToUpcomingRsvpsDateReturnsCorrectlyFormattedStringForDateInTheSameDayAsCurrentAndWithTimeAfter6PMAndCurrentDateIsNotEarlyMorning() {
        let date = Calendar.current.date(bySettingHour: 18, minute: 00, second: 00, of: Date())!
        let currentDate = self.currentDate(isEarlyMorning: false)
        
        XCTAssertEqual(date.toUpcomingRsvpsDate(currentDate: currentDate), Strings.MyRsvps.upcomingTonight.localized)
    }

    func testToUpcomingRsvpsDateReturnsCorrectlyFormattedStringForNextDayDateAndCurrentDateIsNotEarlyMorning() {
        let date = Calendar.current.date(byAdding: .day, value: 1, to: Date())!
        let currentDate = self.currentDate(isEarlyMorning: false)
        
        XCTAssertEqual(date.toUpcomingRsvpsDate(currentDate: currentDate), Strings.MyRsvps.upcomingTomorrow.localized)
    }

    func testToAllActivationsHeaderFormatTodayLocalizedStringForDateInTheSameDayAsCurrentAndWithTimeBefore6PMAndCurrentDateIsNotEarlyMorning() {
        let date = Calendar.current.date(bySettingHour: 17, minute: 59, second: 59, of: Date())!
        let currentDate = self.currentDate(isEarlyMorning: false)

        XCTAssertEqual(date.toAllActivationsHeaderFormat(isApproximatedStartTime: false, currentDate: currentDate), Strings.Activation.today.localized.uppercased())
    }

    func testToAllActivationsHeaderFormatTodayLocalizedStringForDateInTheSameDayAsCurrentAndWithTimeAfter6PMAndCurrentDateIsNotEarlyMorning() {
        let date = Calendar.current.date(bySettingHour: 18, minute: 00, second: 00, of: Date())!
        let currentDate = self.currentDate(isEarlyMorning: false)
        
        XCTAssertEqual(date.toAllActivationsHeaderFormat(isApproximatedStartTime: false, currentDate: currentDate), Strings.Activation.today.localized.uppercased())
    }

    func testToAllActivationsHeaderFormatTomorrowtLocalizedStringForNextDayDateAndCurrentDateIsNotEarlyMorning() {
        let date = Calendar.current.date(byAdding: .day, value: 1, to: Date())!
        let currentDate = self.currentDate(isEarlyMorning: false)
        
        XCTAssertEqual(date.toAllActivationsHeaderFormat(isApproximatedStartTime: false, currentDate: currentDate), Strings.Activation.tomorrow.localized.uppercased())
    }

    func testToAllActivationsHeaderFormatReturnsCorrectlyFormattedDateWhenDateIsNotApproximatedAndCurrentDateIsNotEarlyMorning() {
        let date = self.date(from: "2020-01-01 12:00")
        let currentDate = self.currentDate(isEarlyMorning: false)
        let result = "WEDNESDAY, JAN 1ST"

        XCTAssertEqual(date.toAllActivationsHeaderFormat(isApproximatedStartTime: false, currentDate: currentDate), result)
    }
    
    func testToAllActivationsHeaderFormatReturnsCorrectlyFormattedDateWhenDateIsNotApproximatedAndCurrentDateIsEarlyMorning() {
        let date = self.date(from: "2020-01-01 12:00")
        let currentDate = self.currentDate(isEarlyMorning: true)
        let result = "WEDNESDAY, JAN 1ST"

        XCTAssertEqual(date.toAllActivationsHeaderFormat(isApproximatedStartTime: false, currentDate: currentDate), result)
    }

    func testToAllActivationsHeaderFormatReturnsTBDLocalizedStringWhenDateIsApproximatedAndCurrentDateIsNotEarlyMorning() {
        let date = self.date(from: "2020-01-01 12:00")
        let currentDate = self.currentDate(isEarlyMorning: false)
        
        XCTAssertEqual(date.toAllActivationsHeaderFormat(isApproximatedStartTime: true, currentDate: currentDate), Strings.Activation.dateTBD.localized.uppercased())
    }
    
    func testToAllActivationsHeaderFormatReturnsTBDLocalizedStringWhenDateIsApproximatedAndCurrentDateIsEarlyMorning() {
        let date = self.date(from: "2020-01-01 12:00")
        let currentDate = self.currentDate(isEarlyMorning: true)
        
        XCTAssertEqual(date.toAllActivationsHeaderFormat(isApproximatedStartTime: true, currentDate: currentDate), Strings.Activation.dateTBD.localized.uppercased())
    }

    func date(from string: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        dateFormatter.timeZone = .current
        let date = dateFormatter.date(from: string)!
        return date
    }

    func privateActivationFormatter(for date: Date) -> DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mma"
        formatter.amSymbol = "am"
        formatter.pmSymbol = "pm"

        return formatter
    }
    
    func currentDate(isEarlyMorning: Bool) -> Date {
        let date = Date()
        if isEarlyMorning {
            return Calendar.current.startOfDay(for: date).addingTimeInterval(3600)
        } else {
            return Calendar.current.date(bySetting: .hour, value: 12, of: date)!
        }
    }
}
