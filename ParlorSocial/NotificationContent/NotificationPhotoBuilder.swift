//
//  NotificationPhotoBuilder.swift
//  NotificationContent
//
//  Created by Benjamin Acquah on 07/01/2020.
//

import UIKit

class NotificationPhotoBuilder {

    private unowned let controller: NotificationPhotoViewController
    private var view: UIView! { return controller.view }

    init(controller: NotificationPhotoViewController) {
        self.controller = controller
    }

    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
}

// MARK: - Private methods
extension NotificationPhotoBuilder {

    private func setupProperties() {
        view.backgroundColor = .clear
        controller.imageView.contentMode = .scaleAspectFill
    }

    private func setupHierarchy() {
        [controller.imageView].forEach {
            view.addSubview($0)
        }
    }

    private func setupAutoLayout() {
        controller.imageView.apply {
            NSLayoutConstraint.activate([
                $0.topAnchor.constraint(equalTo: view.topAnchor),
                $0.bottomAnchor.constraint(equalTo: view.bottomAnchor),
                $0.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                $0.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                $0.heightAnchor.constraint(equalToConstant: 300).apply { $0.priority = .defaultHigh }
            ])
        }
    }

}

// MARK: - Public build methods
extension NotificationPhotoBuilder {
    func buildImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }
}
