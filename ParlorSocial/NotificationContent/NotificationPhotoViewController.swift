//
//  NotificationPhotoViewController.swift
//  NotificationContent
//
//  Created by Benjamin Acquah on 30/12/2019.
//

import UIKit
import UserNotifications
import UserNotificationsUI

class NotificationPhotoViewController: UIViewController, UNNotificationContentExtension {
    
    // MARK: - Views
    private(set) var imageView: UIImageView!
    
    // MARK: - Initialization
    init() {
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupViews()
    }
    
    func didReceive(_ notification: UNNotification) {
        let content = notification.request.content

        guard let urlImageString = content.userInfo["image_url"] as? String,
            let imageUrl = URL(string: urlImageString) else { return }
            
        URLSession.downloadImage(atURL: imageUrl) { [weak self] (data, error) in
            guard error == nil, let data = data else { return }
            DispatchQueue.main.async {
                self?.imageView.image = UIImage(data: data)
            }
        }
    }

    // MARK: - Private methods
    private func setupViews() {
        let builder = NotificationPhotoBuilder(controller: self)
        imageView = builder.buildImageView()
        builder.setupViews()
    }

}
